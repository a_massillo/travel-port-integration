/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.universal.api.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;

import com.safarifone.waafitravels.common.Itinerary;
import com.safarifone.waafitravels.common.PaymentInfo;
import com.safarifone.waafitravels.common.ReservationRequest;
import com.safarifone.waafitravels.common.ReservationResponse;
import com.safarifone.waafitravels.common.ResponseTrip;
import com.safarifone.waafitravels.common.TravellerInfo;
import com.safarifone.waafitravels.common.Trip;
import com.safarifone.waafitravels.customer.Customer;
import com.travelport.support.request.enums.AirProvider;
import com.universal.api.service.imp.travelport.AirAvailabilityServiceConnector;
import com.universal.api.service.imp.travelport.AirBookingServiceConnector;
import com.universal.api.service.imp.travelport.AirLowfareServiceConnector;
import com.universal.api.service.imp.travelport.ETicketMailer;
import com.universal.api.service.response.AirAvailabilityResponse;
import com.universal.api.service.response.BookingCustomer;
/**
 * 
 * @author amassillo
 *
 */

public class ServicesTest {

	private AirAvailabilityServiceConnector airAvailabilityService;
	
	private AirBookingServiceConnector bookingService;

	private AirLowfareServiceConnector lowfareService;

	
	private ETicketMailer eTicketProcessor = new ETicketMailer();

	private static List<AirAvailabilityResponse> lOutItinerary;
	
	private String departure = "EZE";
	private String arrival = "DCA"; 

	public ServicesTest() {
		lowfareService = new AirLowfareServiceConnector();
		bookingService = new AirBookingServiceConnector();
	}
	
	public static void main(String[] args) {
		ServicesTest l = new ServicesTest();
		switch (args[0]) {
			case "book":{
				l.airBookingTest();
				break;
			}
			case "resend": {
				if (args.length > 1) {
					String lReservationCode = args[1];
					l.pnrRetrieveandEmailTest(lReservationCode);
				}
				break;
			}
			case "email": {
				l.emailTest();
				break;
			}
		}
	}

	public void pnrRetrieveandEmailTest(String pReservationCode) {
		ReservationRequest req = new ReservationRequest(); 
		
		Customer c1 = this.createCustomer("Alejandra", "Massillo", "amassillo@gmail.com", "123456788");
		c1.setGender("Female");
		c1.setAddressLine1("Street 1");
		c1.setCity("Tandil");
		c1.setZipCode("7000");
		c1.setPassportNumber("123456789");
		TravellerInfo lTrvInfo = new TravellerInfo(); 
		lTrvInfo.setAdults(Arrays.asList(c1));
		req.setTravellerInfo(lTrvInfo);
		
		PaymentInfo lInfo = new PaymentInfo();
		lInfo.setPaymentName("ZAAD");
		lInfo.setPaymentType("Credit Card");
		lInfo.setBillingInfo(c1);
		req.setPaymentInfo(lInfo);
		// from req I use the payment info only
		bookingService.reSendEmailReservationRequest(req,pReservationCode);
	}
	
    public void airLowFareTestAsycn() {
		Itinerary lItinerary = new Itinerary();
		lItinerary.setSearchId("airLowFareTestAsycn");
		lItinerary.setNumberAdults(1);
		ArrayList<Trip> lTrips = new ArrayList<Trip>(); 
		//one way trip
		Trip l1 = new Trip();
		l1.setDeparture_city(this.departure);
		l1.setArrival_city(this.arrival);
		l1.setDeparture_date("2018-03-03");
		lTrips.add(l1);
		lItinerary.setTrips(lTrips);
		//call service
		lowfareService.check(lItinerary);
		//only for testing purposes, do not use, check inner comment
		List<AirAvailabilityResponse> lOutSegments = lowfareService.getAllOutSegments();
		if (lOutSegments.isEmpty()) {
			System.out.println("An error hs occurred during low fare search");
			return;
		}
		lOutItinerary = new ArrayList<AirAvailabilityResponse>();
		for (int i = 0; i< lOutSegments.size(); i++) {
			//search for a non-stop itinerary
			//if (lOutSegments.get(i).getStop()==0) {
			
			lOutItinerary.add(lOutSegments.get(i));
			if (lOutSegments.get(i).getArrAirport().compareTo("IAD")==0) {
				break;
			}
		}
	}
	
    public void airAvailabilityAsycn() {
		Itinerary lItinerary = new Itinerary();
		lItinerary.setSearchId("airAvailabilityAsycn");
		lItinerary.setNumberAdults(1);
		ArrayList<Trip> lTrips = new ArrayList<Trip>(); 
		//one way trip
		Trip l1 = new Trip();
		l1.setDeparture_city(this.departure);
		l1.setArrival_city(this.arrival);
		l1.setDeparture_date("2018-02-01T14:35:00.000-06:00");
		lTrips.add(l1);
		lItinerary.setTrips(lTrips);
		//call service
		airAvailabilityService.check(lItinerary);
	}
	
	public void emailTest() {
		ReservationResponse lOutput = new ReservationResponse();
		Customer c1 = this.createCustomer("Alejandra", "Massillo", "amassillo@gmail.com", "123456788");
		Customer c2 = this.createCustomer("Evanlegina", "Veron", "amassillo@gmail.com", "123456788");
		Customer c3 = this.createCustomer("Elias", "Veron", "amassillo@gmail.com", "123456788");
		Customer c4 = this.createCustomer("Pablo", "Veron", "amassillo@gmail.com", "123456788");
		TravellerInfo lTrvInfo = new TravellerInfo(); 
		lTrvInfo.setAdults(Arrays.asList(c1,c4));
		lTrvInfo.setChildren(Arrays.asList(c2));
		lTrvInfo.setInfants(Arrays.asList(c3));
		lOutput.setTraveller_info(lTrvInfo);
		PaymentInfo lInfo = new PaymentInfo();
		lInfo.setPaymentName("ZAAD");
		lInfo.setPaymentType("Credit Card");
		//billing
		c1.setAddressLine1("Street address 1");
		c1.setCity("Tandil");
		c1.setState("BA");
		c1.setZipCode("7000");
		lInfo.setBillingInfo(c1);
		
		lOutput.setTraveller_info(lTrvInfo);
		lOutput.setPayment_info(lInfo);
		lOutput.setBooking_id("1234567890");
		lOutput.setNumber_adults(1);
		lOutput.setTotal_price("$2345");
		lOutput.setCurrency_code("USD");
		ResponseTrip outbound =  new ResponseTrip();
		outbound.setAirline_name("AA");
		outbound.setArriavel_airport("CHI");
		outbound.setArrival_city("Chicago");
		outbound.setArrival_date("2 Jul");
		outbound.setDeparture_terminal("1");
		outbound.setArrival_time("10:35");
		outbound.setFlight_time(2);
		outbound.setEquipment("737");
		outbound.setCabin("Economy");
		outbound.setCurrency("USD");
		outbound.setDistance(245);
		outbound.setDeparture_airport("DCA");
		outbound.setDeparture_city("Washington");
		outbound.setDeparture_date("2 Jul");
		outbound.setDeparture_time("3:00");
		outbound.setCabin("Economy");
		outbound.setFlight_no("980");
		
		ResponseTrip inbound =  new ResponseTrip();
		inbound.setAirline_name("AA");
		inbound.setArriavel_airport("CHI");
		inbound.setArrival_city("Chicago");
		inbound.setArrival_date("2 Jul");
		inbound.setArrival_terminal("1");
		inbound.setArrival_time("11:00");
		inbound.setCabin("Economy");
		inbound.setStop(1);
		inbound.setCurrency("USD");
		inbound.setDeparture_airport("DCA");
		inbound.setDeparture_city("Washington");
		inbound.setDeparture_date("2 Jul");
		inbound.setDeparture_time("3:00");
		inbound.setCabin("Economy");
		inbound.setFlight_time(1);
		inbound.setEquipment("320");
		inbound.setFlight_no("210");
		inbound.setDeparture_terminal("C");
		outbound.setConnections(Arrays.asList(inbound));
		lOutput.setOutbound(outbound);
		eTicketProcessor.sendEticketEmail(lOutput);
	}

    public void airBookingTest() {
		this.airLowFareTestAsycn();
		ReservationRequest req = new ReservationRequest(); 
		if (lOutItinerary.isEmpty()) {
			return;
		};
//		AirAvailabilityResponse lResponse = lOutItinerary.get(0);
//		Map<String, Object> outbound = createReqParam(lResponse.getDepAirport(), lResponse.getArrAirport(),
//													  lResponse.getFlightNumber(), lResponse.getDepDateTimeTravelPort(), lResponse.getArrDateTimeTravelPort(),
//													  lResponse.getAirlineId(),lResponse.getCabinCode());
//		
		Customer c1 = this.createCustomer("Alejandra", "Massillo", "amassillo@gmail.com", "123456788");
		c1.setGender("Female");
		c1.setAddressLine1("Street 1");
		c1.setCity("Tandil");
		c1.setZipCode("7000");
		c1.setPassportNumber("123456789");
		TravellerInfo lTrvInfo = new TravellerInfo(); 
		lTrvInfo.setAdults(Arrays.asList(c1));
		req.setTravellerInfo(lTrvInfo);
		PaymentInfo lInfo = new PaymentInfo();
		lInfo.setPaymentName("ZAAD");
		lInfo.setPaymentType("Credit Card");
		lInfo.setBillingInfo(c1);
		req.setPaymentInfo(lInfo);
		ReservationResponse lRsv = bookingService.makeReservation(req, lOutItinerary);
		System.out.println(lRsv.getBooking_id());
	}

   
	public Map<String, Object> createReqParam(String pOutbound, String pInBound, String pFlightNbr,
												String pDepartureD, String pArrivalD, String pAirline,
												String pCabinCode){
		Map<String, Object> lBound = new Hashtable<String, Object>();
		lBound.put("departure_airport",pOutbound);
		lBound.put("arrival_airport",pInBound);
		lBound.put("flight_no",pFlightNbr);
		lBound.put("cabin_code",pCabinCode);
		lBound.put("departure_date",pDepartureD);
		lBound.put("arrival_date", pArrivalD);//TODO VERIFY THIS IS AVAILABLE
		lBound.put("airline_id",pAirline);
		lBound.put("provider_code", AirProvider.GALILEO.getCode());// TODO CHECK IF THIS WOULD BE PART OF THE INSERT STATM
		return lBound;

	}
	private Customer createCustomer(String pFirstName, String pLastName, String pEmail, String pPhone ) {
		BookingCustomer c1 = new BookingCustomer();
		c1.setFirstName(pFirstName);
		c1.setLastName(pLastName);
		c1.setEmail(pEmail);
		c1.setPhone(pPhone);
		c1.setTicketNumber(RandomStringUtils.randomAlphabetic(10));
		c1.addSpecialRequest("Meal ", " Veggie");
		return c1;
	}
}
