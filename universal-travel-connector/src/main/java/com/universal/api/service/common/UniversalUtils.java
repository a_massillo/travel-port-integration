/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.universal.api.service.common;

import java.text.ParseException;
import java.util.Date;

/**
 * 
 * @author amassillo
 *
 */
public class UniversalUtils {
    // turn a travel port date from a response back into a java object
    // not as easy to do because java gets confused by the iso 8601 timezone
    public static Date dateFromISO8601(String iso) {
        try {
            String noColon = iso.substring(0, 26) + iso.substring(27);
            return UniversalConstants.tportResultFormat.parse(noColon);
        } catch (ParseException e) {
            throw new RuntimeException("Really unlikely, but it looks like travelport is not using ISO dates anymore! "
                    + e.getMessage());
        }
    }
}
