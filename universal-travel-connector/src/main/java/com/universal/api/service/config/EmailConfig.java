/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.universal.api.service.config;

import lombok.Data;

/**
 * 
 * @author amassillo
 * @date 02/06/2018
 */
@Data
public class EmailConfig {
	private String hostName;
	private Integer smtpPort;
	private String username;
	private String password;
	private Boolean sSLOnConnec;
	private String fromEmail;
	private String bounceEmailAddress;	
}
