/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.universal.api.service.imp.travelport;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.mapstruct.factory.Mappers;

import com.safarifone.waafitravels.common.Itinerary;
import com.safarifone.waafitravels.common.Trip;
import com.travelport.schema.air_v43_0.AirPricePoint;
import com.travelport.schema.air_v43_0.AirPricingInfo;
import com.travelport.schema.air_v43_0.BookingInfo;
import com.travelport.schema.air_v43_0.Connection;
import com.travelport.schema.air_v43_0.FareInfo;
import com.travelport.schema.air_v43_0.FlightDetails;
import com.travelport.schema.air_v43_0.FlightDetailsRef;
import com.travelport.schema.air_v43_0.FlightOption;
import com.travelport.schema.air_v43_0.LowFareSearchReq;
import com.travelport.schema.air_v43_0.LowFareSearchRsp;
import com.travelport.schema.air_v43_0.Option;
import com.travelport.schema.air_v43_0.TypeBaseAirSegment;
import com.travelport.service.air_v43_0.AirFaultMessage;
import com.travelport.support.request.AirBaseLowFareReq;
import com.travelport.support.request.SearchAirLegExt;
import com.travelport.support.request.enums.PassengerType;
import com.universal.api.dto.mapper.AirSearchMapper;
import com.universal.api.service.UniversalAirAvailabilityTravelConnector;
import com.universal.api.service.config.AppConfig;
import com.universal.api.service.response.AirAvailabilityResponse;
import com.travelport.support.Helper;
import com.travelport.support.TravelPortServicesHelper;
import com.travelport.support.Helper.AirSegmentMap;
import com.travelport.support.Helper.FareMap;
import com.travelport.support.Helper.FlightDetailsMap;

/**
 * https://support.travelport.com/webhelp/uapi/Content/Air/Low_Fare_Shopping/Low_Fare_Shopping_(Synchronous).htm
 * Low Fare Shopping functionality combines air availability and a fare quote request to return the lowest available fares for a specified itinerary
 * In order to book, need to call air availability and air price
 * @author amassillo
 * @date 01/18/2018
 */

public class AirLowfareServiceConnector implements UniversalAirAvailabilityTravelConnector{
	
	final static Logger logger = Logger.getLogger(AirLowfareServiceConnector.class);
	
	private TravelPortServicesHelper services;
	
	private AirSearchMapper mapper;
	
	private String searchId;
	
	//collects responses from threads
	private ArrayList<AirAvailabilityResponse> allOutSegments;
	private ArrayList<AirAvailabilityResponse> allInSegments;
	
	public AirLowfareServiceConnector() {
		mapper 	 = Mappers.getMapper(AirSearchMapper.class);
		services = AppConfig.getTravelPortConnector();
	}
	
	/**
	 * Provides availability check based in low fare query 
	 * @param reqItinerary
	 */
	public void check(Itinerary reqItinerary) {		
		//save search-id for later on
		this.searchId = reqItinerary.getSearchId();
		
		allOutSegments = new ArrayList<AirAvailabilityResponse>();
		allInSegments  = new ArrayList<AirAvailabilityResponse>();
		//parse input for the service request parameter
		List<Trip> trips  = reqItinerary.getTrips();
		for(Trip trip : trips ) {
			//setup request
			LowFareSearchReq lOutboundRequest = this.setUpRequest(reqItinerary);	
			SearchAirLegExt outbound = new SearchAirLegExt(trip.getDeparture_city(), 
														   trip.getArrival_city(),trip.getDeparture_date());
			lOutboundRequest.getSearchAirLeg().add(outbound);
			//one or two threads according return date
			int poolSize = trip.getReturn_date()!=null ? 2: 1;
			ExecutorService exec = Executors.newFixedThreadPool(poolSize);
			Future<ArrayList<AirAvailabilityResponse>> leaveSegFuture = exec.submit(new Worker(lOutboundRequest,"outbound"));
			Future<ArrayList<AirAvailabilityResponse>> returnSegFuture = null;
			//clone and execute in different thread
			if (trip.getReturn_date()!=null) {
				LowFareSearchReq lInboundRequest = this.setUpRequest(reqItinerary);	
				//clone and execute in different thread
				SearchAirLegExt inboud = new SearchAirLegExt(trip.getArrival_city(), 
														     trip.getDeparture_city(), trip.getReturn_date());
				lInboundRequest.getSearchAirLeg().add(inboud);
				returnSegFuture = exec.submit(new Worker(lInboundRequest,"inbound"));
			}
			// Wait for segment workers to complete
			try {
					while(!leaveSegFuture.isDone()) {
						Thread.sleep(10);
					}
					allOutSegments.addAll(leaveSegFuture.get());
					if(poolSize == 2) {
						while(returnSegFuture !=null && !returnSegFuture.isDone()) {
							Thread.sleep(10);
						}
						allInSegments.addAll(returnSegFuture.get());
					}
			} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			}
			exec.shutdown();
		}
		
		
	}
	
	/**
	 * Setup request, would have used clone, but it's not implemented by default in axis2 clients schemas
	 * @param reqItinerary
	 * @return
	 */
	private LowFareSearchReq setUpRequest(Itinerary reqItinerary) {
		//setup request
		LowFareSearchReq lRequest = new LowFareSearchReq();
		//travelers
		int numAdults = reqItinerary.getNumberAdults();
		numAdults = numAdults < 0 ? 1 : numAdults;
		
		int numChildren = reqItinerary.getNumberChildren();
		numChildren =  numChildren < 0 ? 0 : numChildren;
		
		//helper
		AirBaseLowFareReq h1 = new AirBaseLowFareReq();
		h1.addPassengers(numAdults + numChildren, PassengerType.ADT);
		//those are always necessary
		lRequest.setTargetBranch(services.getTargetBranch());
		lRequest.setBillingPointOfSaleInfo(services.getBillingPointOfSaleInfo());
		
		lRequest.getSearchPassenger().addAll(h1.getSearchPassenger());
		return lRequest;

	}

	//getters for the accumulated responses
	public ArrayList<AirAvailabilityResponse> getAllOutSegments() {
		return allOutSegments;
	}

	public ArrayList<AirAvailabilityResponse> getAllInSegments() {
		return allInSegments;
	}

	/**
	 * 
	 * @author amassillo
	 *
	 */
	private class Worker implements Callable<ArrayList<AirAvailabilityResponse>> {
		private LowFareSearchReq r;
		private String segment;
		
		public Worker(LowFareSearchReq pRequest,String pSegment) {
			this.r = pRequest;
			this.segment = pSegment;
		}

		@Override
		public ArrayList<AirAvailabilityResponse> call() throws Exception {
			ArrayList<AirAvailabilityResponse> responses = new ArrayList<AirAvailabilityResponse>();
			//execute request for this itinerary	
			try {
				//services.getAirShop().showXML(true);
				LowFareSearchRsp l = services.getAirShop().get().service(r);
				String lCurrencyType = l.getCurrencyType();
				//aux parsing before merging all info
				AirSegmentMap lSegmentMap = Helper.createAirSegmentMap(l.getAirSegmentList().getAirSegment());
				FareMap lFaresMap 		  = Helper.createFareInfoMap(l.getFareInfoList().getFareInfo());
				FlightDetailsMap lFlightDetailsMap = Helper.createFlightDetailsMap(l.getFlightDetailsList().getFlightDetails());
			
				//using point list entrance point due to the need of returning prices, otherwise you can use the segments which have 
				//most of the details
				for (AirPricePoint lPricePoint : l.getAirPricePointList().getAirPricePoint())
				for (AirPricingInfo lPriceInfo : lPricePoint.getAirPricingInfo()) {	
					//one way flight option, segments might contain connections
					for (FlightOption lFlightOption: lPriceInfo.getFlightOptionsList().getFlightOption()) {
						String lFinalDestination = lFlightOption.getDestination();
						for (Option lOption: lFlightOption.getOption()) {	
							//connections refers to BookingInfo indexes
							HashSet<Integer> lIndexes = new HashSet<Integer>() ;
							//connection nodes indicates flight which destination is a connection
							//https://support.travelport.com/webhelp/uAPI/Content/Air/Shared_Air_Topics/AirSegmentConnectionLogic.htm
							for (Connection lConnection: lOption.getConnection()) {
//								//only connecting flight is stopOver = false
//								if (!lConnection.isStopOver())
									lIndexes.add(lConnection.getSegmentIndex());
							}
							int lSegmentIndex = 0;
							for (BookingInfo lInfo: lOption.getBookingInfo()) {
								TypeBaseAirSegment lSegment = lSegmentMap.getByRef(lInfo.getSegmentRef());
								if (lSegment.getGroup()==0 && segment.compareTo("outbound") !=0) {
									//this is not an out-bound itinerary
									logger.warn("found an inbound flight in an outbound search !" + lSegment.getKey());
								}
								if (lSegment.getGroup()==1 && segment.compareTo("inbound") !=0) {
									//this is not an out-bound itinerary
									logger.warn("found an outound flight in an inbound search !" + lSegment.getKey());
								}
								//if no availability info continue
								if (lSegment.getAirAvailInfo().size() <1) continue;
								//fare and booking info references are translated into the lists								
								FareInfo lFareInfo = lFaresMap.getByRef(lInfo.getFareInfoRef());
								if (lFareInfo.getAmount()==null) {
									//no fee found, continue
									continue;
								}
								//use as necessary later on
								AirAvailabilityResponse lResponse = new AirAvailabilityResponse();
								lResponse.setSegment(segment);
								lResponse.setSearchId(searchId);
								//set fields extracted from fare and booking information -------------------------------------
								lResponse.setTax(Helper.parseNumberWithCurrency(lPriceInfo.getTaxes()));
								lResponse.setPrice(Helper.parseNumberWithCurrency(lFareInfo.getAmount()));
								lResponse.setCabinCode(lInfo.getCabinClass());
								lResponse.setCurrency(lCurrencyType);
								
								mapper.fromTypeBaseAirSegmentToReservationResponse(lSegment, lResponse);
								
								//segment has connection flights
								if(lIndexes.contains(lSegmentIndex)) {
									int lStopNbr = 0;
									for (FlightDetailsRef lFlightRef : lSegment.getFlightDetailsRef()) {
										FlightDetails lConnFlight = lFlightDetailsMap.getByRef(lFlightRef);
										//map connecting flight, need to add many, so I clone
										AirAvailabilityResponse lResponseClon = lResponse.clone();
										mapper.fromFlightDetailsToReservationResponse(lConnFlight, lResponseClon);
										lResponseClon.setStop(++lStopNbr);
										lResponseClon.setFinalDest(lFinalDestination);//this is city, not airport);
										responses.add(lResponseClon);
									}
								}else {
									responses.add(lResponse);
								}
								lSegmentIndex++;
								//---------------------------------------------------------------------------------------------
								
							}
						}
					}
				}				
			} catch (AirFaultMessage e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.error(searchId+ "/" +e.getMessage());
			}
			return responses;
		}
	}	
	
	
}
