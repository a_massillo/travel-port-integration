/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.universal.api.service.response;

import java.util.ArrayList;
import java.util.List;

import com.safarifone.waafitravels.customer.Customer;

/**
 * 
 * @author amassillo
 *
 */
public class BookingCustomer extends Customer {
	public class SpecialRequest{
		private String key;
		private String value;
		public SpecialRequest() {	
		}
		public SpecialRequest(String pKey, String pValue) {
			this.key = pKey;
			this.value = pValue;
		}
		public String getKey() {
			return key;
		}
		public void setKey(String key) {
			this.key = key;
		}
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
	}
	
	private String genBookingRef;
	private String ticketNumber;
	private String seatPreference; //TODO per flight if necessary
	private List<SpecialRequest> specialRequests;
	
	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public List<SpecialRequest> getSpecialRequests() {
		return specialRequests;
	}

	public void setSpecialRequests(List<SpecialRequest> specialRequests) {
		this.specialRequests = specialRequests;
	}
	
	public void addSpecialRequest(String pKey, String pValue) {
		SpecialRequest pRequest = new SpecialRequest (pKey, pValue);
		if (specialRequests ==null) {
			specialRequests = new ArrayList<SpecialRequest>();
		}
		specialRequests.add(pRequest);
	}

	public String getGenBookingRef() {
		return genBookingRef;
	}

	public void setGenBookingRef(String genBookingRef) {
		this.genBookingRef = genBookingRef;
	}

	public String getSeatPreference() {
		return seatPreference;
	}

	public void setSeatPreference(String seatPreference) {
		this.seatPreference = seatPreference;
	}

}
