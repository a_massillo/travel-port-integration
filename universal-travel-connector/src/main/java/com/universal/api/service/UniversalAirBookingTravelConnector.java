/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.universal.api.service;

import java.util.Map;

import com.safarifone.waafitravels.common.ReservationRequest;
import com.safarifone.waafitravels.common.ReservationResponse;

/**
 * 
 * @author amassillo
 *
 */
public interface UniversalAirBookingTravelConnector {

	public ReservationResponse makeReservation(ReservationRequest req, 
			Map<String, Object> outbound,Map<String, Object> inbound, 
			Map<String, Object> outboundSecondSegment, Map<String, Object> inboundSSecondSegment);
}
