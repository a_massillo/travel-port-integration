/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.universal.api.service.imp.travelport;

import java.util.Iterator;
import java.util.Map;


import com.safarifone.waafitravels.common.ServiceResponse;
import com.safarifone.waafitravels.common.WaafiTravelsException;
import com.travelport.schema.air_v43_0.AirRefundQuoteReq;
import com.travelport.schema.air_v43_0.AirRefundQuoteRsp;
import com.travelport.schema.air_v43_0.AirRefundReq;
import com.travelport.schema.air_v43_0.AirRefundRsp;
import com.travelport.schema.universal_v43_0.ProviderReservationStatus;
import com.travelport.schema.universal_v43_0.UniversalRecordCancelReq;
import com.travelport.schema.universal_v43_0.UniversalRecordCancelRsp;
import com.travelport.schema.universal_v43_0.UniversalRecordRetrieveRsp;
import com.travelport.service.air_v43_0.AirFaultMessage;
import com.travelport.service.universal_v43_0.UniversalRecordFaultMessage;
import com.travelport.support.TravelPortServicesHelper;
import com.universal.api.service.UniversalAirCancellationTravelConnector;
import com.universal.api.service.config.AppConfig;

/**
 * To cancel booking
 * @author amassillo
 * @date 01/18/2018
 */

public class AirCancellationServiceConnector implements UniversalAirCancellationTravelConnector{
	
	private TravelPortServicesHelper services = AppConfig.getTravelPortConnector();

	private UniversalRetrieveService universalRetrieveSrv;

	public AirCancellationServiceConnector() {
		universalRetrieveSrv = new UniversalRetrieveService();
	}
	/**
	 * 
	 */
	public ServiceResponse cancel(Map<String, Object> resMap, String reservationId) throws WaafiTravelsException{
		ServiceResponse lCancelBookingResponse = new ServiceResponse();
		
		UniversalRecordCancelReq lCancelReq = new UniversalRecordCancelReq();
		try {
			UniversalRecordRetrieveRsp lUnvRecord = universalRetrieveSrv.retrieveUniversalRecordLocator(reservationId);
			lCancelReq.setVersion(lUnvRecord.getUniversalRecord().getVersion());
			lCancelReq.setUniversalRecordLocatorCode(lUnvRecord.getUniversalRecord().getLocatorCode());
			//those are always necessary
			lCancelReq.setTargetBranch(services.getTargetBranch());
			lCancelReq.setBillingPointOfSaleInfo(services.getBillingPointOfSaleInfo());

			UniversalRecordCancelRsp lCancelRsp = services.getCancelJourney().get().service(lCancelReq);
			
			boolean allBoundsCancelled = true;
			for (Iterator<ProviderReservationStatus> lIterator = lCancelRsp.getProviderReservationStatus().iterator();
													 lIterator.hasNext();) {
				ProviderReservationStatus pcStatus = lIterator.next();
				pcStatus.getProviderCode();
				//this might be different in current provider, need to check
				if (!pcStatus.isCancelled())
					allBoundsCancelled = false;
			}
			
			if (!allBoundsCancelled)
				throw new WaafiTravelsException("Unable to reverse reservation.");
			lCancelRsp.getTransactionId();
			// ask for refund, if any, TODO is this necessary?
			// this.refundPayment(reservationId);
		} catch (UniversalRecordFaultMessage e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return lCancelBookingResponse;
	}

	public void refundPayment(String pBookingId) {
		try {
			//query how much can be refunded
			AirRefundQuoteReq lReqQuote = new AirRefundQuoteReq();
			lReqQuote.getTicketNumber().add(pBookingId);
			//those are always needed
			lReqQuote.setTargetBranch(services.getTargetBranch());
			lReqQuote.setBillingPointOfSaleInfo(services.getBillingPointOfSaleInfo());
			AirRefundQuoteRsp lRespQuote = services.getAirRefundQuote().get().service(lReqQuote);
			
			if (lRespQuote.getAirRefundBundle().isEmpty()) {
				//nothing to refund
				return;
			}
			//ask, if any provided result from previous quote, the refunds
			AirRefundReq lRefundReq = new AirRefundReq();
			//those are always needed
			lRefundReq.setTargetBranch(services.getTargetBranch());
			lRefundReq.setBillingPointOfSaleInfo(services.getBillingPointOfSaleInfo());
			lRefundReq.getAirRefundBundle().addAll(lRespQuote.getAirRefundBundle());
			AirRefundRsp lRefundRsp = services.getAirRefund().get().service(lRefundReq);
			System.out.println(lRefundRsp.getTransactionId());
		} catch (AirFaultMessage e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
