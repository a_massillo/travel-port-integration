/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.universal.api.service.imp.travelport;

import com.travelport.schema.universal_v43_0.UniversalRecordRetrieveReq;
import com.travelport.schema.universal_v43_0.UniversalRecordRetrieveRsp;
import com.travelport.service.universal_v43_0.UniversalRecordArchivedFaultMessage;
import com.travelport.service.universal_v43_0.UniversalRecordFaultMessage;
import com.travelport.support.TravelPortServicesHelper;
import com.universal.api.service.config.AppConfig;

/**
 * 
 * @author amassillo
 *
 */

public class UniversalRetrieveService {
	
	private TravelPortServicesHelper services = AppConfig.getTravelPortConnector();
	
	public UniversalRecordRetrieveRsp retrieveUniversalRecordLocator(String pCode) {
		UniversalRecordRetrieveReq lRequest = new UniversalRecordRetrieveReq();
		//those are always required
		lRequest.setBillingPointOfSaleInfo(services.getBillingPointOfSaleInfo());
		lRequest.setTargetBranch(services.getTargetBranch());
		
		lRequest.setUniversalRecordLocatorCode(pCode);
		UniversalRecordRetrieveRsp lResp = null;
		try {
			lResp = services.getUniversalRetrieve().get().service(lRequest);
		} catch (UniversalRecordArchivedFaultMessage e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UniversalRecordFaultMessage e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lResp;
	}
}
