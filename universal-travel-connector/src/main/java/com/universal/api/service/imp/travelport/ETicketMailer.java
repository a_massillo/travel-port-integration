/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.universal.api.service.imp.travelport;

import java.util.Hashtable;

import org.apache.log4j.Logger;
import com.safarifone.waafitravels.common.ReservationResponse;
import com.universal.api.service.EmailService;
import com.universal.api.service.EmailService.EmailTemplate;
import com.universal.api.service.config.AppConfig;

/**
 * 
 * @author amassillo
 * @date 01/31/2018
 */

public class ETicketMailer {
	final static Logger logger = Logger.getLogger(ETicketMailer.class);

	private EmailService emailService  = new EmailService(AppConfig.getEmailConfig());
	 
	public void sendEticketEmail(ReservationResponse lResponse){
		Hashtable<String, Object> leMapping = new Hashtable<String, Object>();
		//DIVIDE IF NEEDED
		leMapping.put("reservation_response", lResponse);
		emailService.sendMail(lResponse.getPayment_info().getBillingInfo().getEmail(), 
				 			 "Flight confirmation", leMapping, EmailTemplate.FLIGHT_CONFIRMATION);
	}
	//others
	public void cancelReservationEmail() {
		
	}
}
