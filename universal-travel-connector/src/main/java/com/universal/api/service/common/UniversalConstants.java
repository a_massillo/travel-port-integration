/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.universal.api.service.common;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 * 
 * @author amassillo
 *
 */
public class UniversalConstants {

	// this is not *quite* a travel port date because tport puts a colon in
    // the timezone which is not ok with RFC822 timezones
    public static SimpleDateFormat tportResultFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    public static SimpleDateFormat timeLocalFormat = new SimpleDateFormat("HH:mm:ss");
    public static SimpleDateFormat dateLocalFormat = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat offsetLocalFormat = new SimpleDateFormat("Z");
    
    public static SimpleDateFormat timeLocalFormatGMT = (SimpleDateFormat) timeLocalFormat.clone();
    public static SimpleDateFormat dateLocalFormatGMT = (SimpleDateFormat) dateLocalFormat.clone();
    public static SimpleDateFormat offsetLocalFormatGMT = (SimpleDateFormat) offsetLocalFormat.clone();
    public static SimpleDateFormat dateTimeFormatGMT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    {
    	TimeZone lGMTTz = TimeZone.getTimeZone("GMT");
    	timeLocalFormatGMT.setTimeZone(lGMTTz);
    	dateLocalFormatGMT.setTimeZone(lGMTTz);	   
    	
    	dateTimeFormatGMT.setTimeZone(lGMTTz);
    }
}
