/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.universal.api.service.imp.travelport;

import java.math.BigInteger;
import java.util.Hashtable;

import org.apache.log4j.Logger;

import com.travelport.schema.util_v43_0.ReferenceDataSearchItem;
import com.travelport.schema.util_v43_0.ReferenceDataSearchModifiers;
import com.travelport.schema.util_v43_0.ReferenceDataSearchReq;
import com.travelport.schema.util_v43_0.ReferenceDataSearchRsp;
import com.travelport.schema.util_v43_0.ReferenceDataSearchRsp.Airport;
import com.travelport.schema.util_v43_0.ReferenceDataSearchRsp.City;
import com.travelport.support.TravelPortServicesHelper;
import com.universal.api.service.config.AppConfig;

/**
 * Returns Itineraries with focus in availability  
 * @author amassillo
 * @date 02/07/2018
 */

public class ReferenceDataRetrieveServiceConnector {

	final static Logger logger = Logger.getLogger(ReferenceDataRetrieveServiceConnector.class);
	private TravelPortServicesHelper services = AppConfig.getTravelPortConnector();
	
	/**
	 * 
	 * @param pAirportList
	 */
	public Hashtable<String, String> getCitiesByAirportList(String[] pAirportList) {
		Hashtable<String, String> lAirportCity = new Hashtable <String, String>();	
		ReferenceDataSearchReq lRequest = new ReferenceDataSearchReq();
		
		//those are always necessary
		lRequest.setTargetBranch(services.getTargetBranch());
		lRequest.setBillingPointOfSaleInfo(services.getBillingPointOfSaleInfo());
		
		for (String lAirportCode: pAirportList) {
			ReferenceDataSearchItem lSearchItem = new ReferenceDataSearchItem();
			lSearchItem.setType("Airport");
			lSearchItem.setCode(lAirportCode);
			lRequest.getReferenceDataSearchItem().add(lSearchItem);
		}
		
		ReferenceDataSearchModifiers lModifiers = new ReferenceDataSearchModifiers();
		lModifiers.setProviderCode("1G");
		lModifiers.setMaxResults(BigInteger.valueOf(pAirportList.length));
		lModifiers.setStartFromResult(BigInteger.valueOf(0));
		lRequest.setReferenceDataSearchModifiers(lModifiers);
		try {
			services.getReferenceLookupRetrieve().showXML(true);
			ReferenceDataSearchRsp lResponse = services.getReferenceLookupRetrieve().get().service(lRequest);
			
			lRequest.getReferenceDataSearchItem().clear(); //using for new query
			
			for (Airport lAirport :lResponse.getAirport()) {
				ReferenceDataSearchItem lSearchItem = new ReferenceDataSearchItem();
				lSearchItem.setType("City");
				lSearchItem.setCode(lAirport.getCityCode());
				lRequest.getReferenceDataSearchItem().add(lSearchItem);
			}
			
			ReferenceDataSearchRsp lResponse2 = services.getReferenceLookupRetrieve().get().service(lRequest);
			Hashtable<String, String> lCitiesMap = new Hashtable <String, String>();
			for (City lCity: lResponse2.getCity()) {
				lCitiesMap.put(lCity.getCode(),lCity.getName());
			}
			
			for (Airport lAirport :lResponse.getAirport()) {
				lAirportCity.put(lAirport.getCode(), lCitiesMap.get(lAirport.getCityCode()));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			logger.error(e.getMessage());
		}
		return lAirportCity;
	}
}
