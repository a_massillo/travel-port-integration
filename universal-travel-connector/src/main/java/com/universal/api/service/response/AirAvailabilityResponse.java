/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.universal.api.service.response;

import java.math.BigDecimal;
import java.util.Date;

import com.universal.api.service.common.UniversalConstants;
import com.universal.api.service.common.UniversalUtils;



/**
 * 
 * @author amassillo
 * this class has all parameters needed to insert record in DB as per defined in
 * NonGDSAvailability insertItinerary method
 */

public class AirAvailabilityResponse implements Cloneable{ 
	private String searchId;
	private String depAirport;
	private String arrAirport;
	//custom fields----------
	private Date depDateTimeLocal;
	private Date arrDateTimeLocal;
	//end of custom fields---
	private String depDayLocal;
	private String depDayGmt;
	private int  depDayOffset;
	private String depTimeLocal;
	private String depTimeGmt;
	private String arrDayLocal;
	private String arrDayGmt;
	private String arrTimeLocal;
	private String arrTimeGmt;
	private int arrDayOffset;
	private int duration;
	private String airlineId;
	private String flightNumber;
	private int stop;
	private int cabinId;
	private String cabinCode;
	private String currency;

	private BigDecimal price;
	private BigDecimal tax;
	private Integer miles;
	private String segment;
	private int line;
	private String finalDest;
	public String getSearchId() {
		return searchId;
	}
	public void setSearchId(String searchId) {
		this.searchId = searchId;
	}
	public String getDepAirport() {
		return depAirport;
	}
	public void setDepAirport(String depAirport) {
		this.depAirport = depAirport;
	}
	public String getArrAirport() {
		return arrAirport;
	}
	public void setArrAirport(String arrAirport) {
		this.arrAirport = arrAirport;
	}
	public String getDepDayLocal() {
		return depDayLocal;
	}

	public String getDepDayGmt() {
		return depDayGmt;
	}
	public int getDepDayOffset() {
		return depDayOffset;
	}
	public String getDepTimeLocal() {
		return depTimeLocal;
	}
	public String getDepTimeGmt() {
		return depTimeGmt;
	}
	public String getArrDayLocal() {
		return arrDayLocal;
	}
	public String getArrDayGmt() {
		return arrDayGmt;
	}
	public String getArrTimeLocal() {
		return arrTimeLocal;
	}
	public String getArrTimeGmt() {
		return arrTimeGmt;
	}
	public int getArrDayOffset() {
		return arrDayOffset;
	}

	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public String getAirlineId() {
		return airlineId;
	}
	public void setAirlineId(String airlineId) {
		this.airlineId = airlineId;
	}
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	public int getStop() {
		return stop;
	}
	public void setStop(int stop) {
		this.stop = stop;
	}
	public int getCabinId() {
		return cabinId;
	}
	public void setCabinId(int cabinId) {
		this.cabinId = cabinId;
	}
	public String getCabinCode() {
		return cabinCode;
	}
	public void setCabinCode(String cabinCode) {
		this.cabinCode = cabinCode;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getTax() {
		return tax;
	}
	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}
	public Integer getMiles() {
		return miles;
	}
	public void setMiles(Integer miles) {
		this.miles = miles;
	}
	public String getSegment() {
		return segment;
	}
	public void setSegment(String segment) {
		this.segment = segment;
	}
	public int getLine() {
		return line;
	}
	public void setLine(int line) {
		this.line = line;
	}
	public String getFinalDest() {
		return finalDest;
	}
	public void setFinalDest(String finalDest) {
		this.finalDest = finalDest;
	}
	public void setDepDateTimeIso(String depDateTimeLocal) {
		
	}
	
	public void setDepDateTimeLocal(String depDateTimeLocal) {
		this.depDateTimeLocal = UniversalUtils.dateFromISO8601(depDateTimeLocal);
		this.depDayLocal = UniversalConstants.dateLocalFormat.format(this.depDateTimeLocal);
		this.depTimeLocal = UniversalConstants.timeLocalFormat.format(this.depDateTimeLocal);
		//this.depDayOffset = Integer.valueOf(offsetLocalFormat.format(this.depDateTimeLocal));
		this.depDayGmt = UniversalConstants.dateLocalFormatGMT.format(this.depDateTimeLocal);
		this.depTimeGmt = UniversalConstants.timeLocalFormatGMT.format(this.depDateTimeLocal);
	}

	public void setArrDateTimeLocal(String arrDateTimeLocal) {		
		this.arrDateTimeLocal = UniversalUtils.dateFromISO8601(arrDateTimeLocal);
		this.arrDayLocal = UniversalConstants.dateLocalFormat.format(this.arrDateTimeLocal);
		this.arrTimeLocal = UniversalConstants.timeLocalFormat.format(this.arrDateTimeLocal);
		// this.arrDayOffset = Integer.valueOf(offsetLocalFormat.format(this.arrDateTimeLocal));
		this.arrDayGmt = UniversalConstants.dateLocalFormatGMT.format(this.arrDateTimeLocal);
		this.arrTimeGmt = UniversalConstants.timeLocalFormatGMT.format(this.arrDateTimeLocal);

	}
	
	public String getDepDateTimeTravelPort() {
		return UniversalConstants.tportResultFormat.format(this.depDateTimeLocal);
	}
	public String getArrDateTimeTravelPort() {
		return UniversalConstants.tportResultFormat.format(this.arrDateTimeLocal);
	}	

	public AirAvailabilityResponse clone() throws CloneNotSupportedException {
		return (AirAvailabilityResponse) super.clone();
	}
}
