/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/

package com.universal.api.service;

import java.util.Iterator;
import java.util.Map;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.HtmlEmail;
import org.apache.log4j.Logger;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import com.universal.api.service.config.EmailConfig;

/**
 * 
 * @author amassillo
 *
 */
public class EmailService {

	final static Logger logger = Logger.getLogger(EmailService.class);
	
	private EmailConfig emailConfig;

	private TemplateEngine templateEngine;
	
	public EmailService(EmailConfig pEmailConfig) {
		this.emailConfig =  pEmailConfig;
		this.templateEngine = new TemplateEngine(); 
		ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
		templateResolver.setSuffix(".html");
	    templateResolver.setPrefix("/templates/");
	    templateResolver.setCacheable(false);
	    templateEngine.setTemplateResolver(templateResolver);
	}
	
	public enum EmailTemplate{
		FLIGHT_CONFIRMATION("flightConfirmationTemplate");
		
		private String templateName;
		//private String templateType;
		
		EmailTemplate(String pTemplateName){
			this.templateName = pTemplateName;
		}

		public String getTemplateName() {
			return templateName;
		}
		
	}
	
	/**
	 * 
	 * @param pVariables
	 * @param pTemplateName
	 * @return
	 */
	private String buildFromTemplate(Map<String, Object> pVariables, String pTemplateName) {
	  Context context = new Context();
	  //each variable
	  for (Iterator<String> lKeys = pVariables.keySet().iterator();lKeys.hasNext();) {
		  String lKey = lKeys.next();
		  context.setVariable(lKey, pVariables.get(lKey));
	  }
	  return templateEngine.process(pTemplateName, context);
	}
	
	/**
	 * 
	 * @param toEmail
	 * @param subject
	 * @param pVariables
	 * @param pTemplateName
	 */
    public void sendMail(String toEmail, String subject, Map<String, Object> pVariables, EmailTemplate pTemplateName) {
        try {
        	HtmlEmail lEmail = new HtmlEmail();
            String content = this.buildFromTemplate(pVariables,pTemplateName.getTemplateName());
            //connection data
            lEmail.setHostName(this.emailConfig.getHostName());
            lEmail.setSmtpPort(this.emailConfig.getSmtpPort());
            lEmail.setAuthenticator(new DefaultAuthenticator(this.emailConfig.getUsername(), 
            												 this.emailConfig.getPassword()));
            lEmail.setSSLOnConnect(true);
            //message specifics
            lEmail.setHtmlMsg(content);
            lEmail.setFrom(this.emailConfig.getFromEmail());
            lEmail.addTo(toEmail);
            lEmail.setSubject(subject);
            lEmail.setBounceAddress(this.emailConfig.getBounceEmailAddress());
            lEmail.send();
        }catch (Exception e) {
        	e.printStackTrace();
        	logger.error("-------- there has been an error in sending email:----- " + e.getMessage());
        }
    }
}
