/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/

package com.universal.api.service.imp.travelport;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.mapstruct.factory.Mappers;

import com.safarifone.waafitravels.common.BaggageAllowance;
import com.safarifone.waafitravels.common.ReservationRequest;
import com.safarifone.waafitravels.common.ReservationResponse;
import com.safarifone.waafitravels.common.ResponseTrip;
import com.safarifone.waafitravels.common.TravellerInfo;
import com.safarifone.waafitravels.common.WaafiTravelsException;
import com.safarifone.waafitravels.customer.Customer;
import com.safarifone.waafitravels.util.Util;
import com.travelport.schema.air_v43_0.AirPriceRsp;
import com.travelport.schema.air_v43_0.AirPricingInfo;
import com.travelport.schema.air_v43_0.AirPricingSolution;
import com.travelport.schema.air_v43_0.AirReservation;
import com.travelport.schema.air_v43_0.AirReservationLocatorCode;
import com.travelport.schema.air_v43_0.AirSolutionChangedInfo;
import com.travelport.schema.air_v43_0.AirTicketingReq;
import com.travelport.schema.air_v43_0.AirTicketingRsp;
import com.travelport.schema.air_v43_0.BookingInfo;
import com.travelport.schema.air_v43_0.DocumentInfo;
import com.travelport.schema.air_v43_0.FareInfo;
import com.travelport.schema.air_v43_0.FareNote;
import com.travelport.schema.air_v43_0.FlightDetails;
import com.travelport.schema.air_v43_0.TicketInfo;
import com.travelport.schema.air_v43_0.TypeBaseAirSegment;
import com.travelport.schema.common_v43_0.TypeStructuredAddress;
import com.travelport.schema.common_v43_0.ActionStatus;
import com.travelport.schema.common_v43_0.AirSeatAssignment;
import com.travelport.schema.common_v43_0.BookingTraveler;
import com.travelport.schema.common_v43_0.ResponseMessage;
import com.travelport.schema.common_v43_0.SSR;
import com.travelport.schema.universal_v43_0.AirCreateReservationReq;
import com.travelport.schema.universal_v43_0.AirCreateReservationRsp;
import com.travelport.schema.universal_v43_0.UniversalRecord;
import com.travelport.schema.universal_v43_0.UniversalRecordRetrieveRsp;
import com.travelport.service.air_v43_0.AirFaultMessage;
import com.travelport.support.Helper;
import com.travelport.support.TravelPortServicesHelper;
import com.travelport.support.Helper.AirSegmentMap;
import com.travelport.support.Helper.FareMap;
import com.travelport.support.Helper.TicketInforByTravelerRefMap;
import com.travelport.support.request.enums.AirProvider;
import com.travelport.support.request.enums.EmailType;
import com.travelport.support.request.enums.PassengerType;
import com.travelport.support.request.enums.PhoneType;
import com.travelport.support.request.enums.TicketingType;
import com.travelport.support.request.params.TravelPortTraveler;
import com.universal.api.dto.mapper.AirBookingMapper;
import com.universal.api.service.UniversalAirBookingTravelConnector;
import com.universal.api.service.config.AppConfig;
import com.universal.api.service.response.AirAvailabilityResponse;
import com.universal.api.service.response.BookingCustomer;
/**
 * 
 * @author amassillo
 *
 */

public class AirBookingServiceConnector implements UniversalAirBookingTravelConnector{
	
	final static Logger logger = Logger.getLogger(AirBookingServiceConnector.class);
	
	private TravelPortServicesHelper services = AppConfig.getTravelPortConnector();
	
	private AirPriceServiceConnector airPriceService;
	private UniversalRetrieveService universalService;
	private ReferenceDataRetrieveServiceConnector referenceRetrieveService;
	private ETicketMailer eTicketProcessor;

	private AirBookingMapper mapper;

	public AirBookingServiceConnector() {
		airPriceService = new AirPriceServiceConnector();
		universalService = new UniversalRetrieveService();
		referenceRetrieveService = new ReferenceDataRetrieveServiceConnector();
		eTicketProcessor = new ETicketMailer();
		mapper = Mappers.getMapper(AirBookingMapper.class);
	}
	/**
	 * for any given previously calculated AirPriceRsp
	 * @param req
	 * @param pAirPriceResult
	 * @return
	 */
	private ReservationResponse makeReservation(ReservationRequest req, AirPriceRsp pAirPriceResult) {
		return this.airReservation(req, pAirPriceResult);
	}
	
	public ReservationResponse makeReservation(ReservationRequest req, 
			List<AirAvailabilityResponse> itinerary) {
		ReservationResponse resp = new ReservationResponse();
		//travelers
		int numAdults = req.getTravellerInfo().getAdults().size();
		numAdults = numAdults < 0 ? 1 : numAdults;
				
		int numChildren = req.getTravellerInfo().getChildren() != null ? req.getTravellerInfo().getChildren().size() : 0;
		numChildren =  numChildren < 0 ? 0 : numChildren;
	
		//Call to confirm price
		AirPriceRsp airPriceResult = null;
		try {
			airPriceResult = airPriceService.checkPrice(numAdults + numChildren, itinerary);
			resp = this.makeReservation(req, airPriceResult);

		} catch (WaafiTravelsException e1) {
			logger.warn(e1.getMessage());
			resp.setStatus("error");//TODO need to check safarifone error types
		}
		return resp;	
	}
	/**
	 * consider Air segments were mapped in the maps parameters
	 * price not available in the request, calling airPrice to confirm
	 */
	public ReservationResponse makeReservation(ReservationRequest req, 
					Map<String, Object> outbound,Map<String, Object> inbound, 
					Map<String, Object> outboundSecondSegment, Map<String, Object> inboundSSecondSegment) {
		
		ReservationResponse resp = new ReservationResponse();
		//travelers
		int numAdults = req.getTravellerInfo().getAdults().size();
		numAdults = numAdults < 0 ? 1 : numAdults;
				
		int numChildren = req.getTravellerInfo().getChildren() != null ? req.getTravellerInfo().getChildren().size() : 0;
		numChildren =  numChildren < 0 ? 0 : numChildren;
	
		//Call to confirm price
		AirPriceRsp airPriceResult = null;
		try {
			airPriceResult = airPriceService.checkPrice(numAdults + numChildren, outbound, inbound, 
														outboundSecondSegment, inboundSSecondSegment);
			resp = this.makeReservation(req, airPriceResult);

		} catch (WaafiTravelsException e1) {
			logger.warn(e1.getMessage());
			resp.setStatus("error");//TODO need to check safarifone error types
		}
		return resp;
	}
	/**
	 * Processes reservation request
	 * @param req
	 * @return
	 */
	private ReservationResponse airReservation(ReservationRequest req, AirPriceRsp airPriceResult) {
		
		ReservationResponse resp = new ReservationResponse();
		resp.setStatus("error"); // default status
		//get all possible itinerary segments for this specific air-pricing
		AirSegmentMap lSegments = Helper.createAirSegmentMap(airPriceResult.getAirItinerary().getAirSegment());
		
		//{one air:AirPriceResult node, probably many air:AirPricingSolution, depending on requested optional services}	
		//TODO if optional services provided, you need to update the way of selecting air price 
		AirPricingSolution airPricing = airPriceResult.getAirPriceResult().get(0).getAirPricingSolution().get(0);
		/**
		 * This is s required fix according to:
		 * https://github.com/Travelport/travelport-uapi-tutorial/issues/232
		 * Seems in current versions, this attribute is not used, but still mapped
		 */
		airPricing.getAirPricingInfo().get(0).setPlatingCarrier(null);
		//links segments refs
		airPricing.getAirSegment().addAll(lSegments.getByRef(airPricing.getAirSegmentRef()));
		airPricing.getAirSegmentRef().clear(); //clean refs
		airPricing.setOptionalServices(null); // clear refs			
		//create reservation request using air price result
		AirCreateReservationReq lResrvReq = new AirCreateReservationReq();
		lResrvReq.getAirPricingSolution().add(airPricing);
				
		//those are always necessary
		lResrvReq.setTargetBranch(services.getTargetBranch());
		lResrvReq.setBillingPointOfSaleInfo(services.getBillingPointOfSaleInfo());
		//add traveler info
		this.addCustomer(lResrvReq, req.getTravellerInfo().getAdults(), PassengerType.ADT);
		this.addCustomer(lResrvReq, req.getTravellerInfo().getChildren(), PassengerType.CNN);
		this.addCustomer(lResrvReq, req.getTravellerInfo().getInfants(), PassengerType.INS);// infant with seat
		//action status
		ActionStatus lActionStatus = new ActionStatus();
		lActionStatus.setType(TicketingType.ACTIVE.name());
		lActionStatus.setProviderCode(AirProvider.GALILEO.getCode()); //TODO check
		lActionStatus.setTicketDate("T*");
		lResrvReq.getActionStatus().add(lActionStatus);
		//not doing ticketing here, so form of payment is not necessary
		try {
			services.getAirReserve().showXML(true);
			AirCreateReservationRsp tpAirRvs = services.getAirReserve().get().service(lResrvReq);
			//------------------------------- process response ------------------------------------------------------------------
			//universal record was not found, some error happened
			if (tpAirRvs.getUniversalRecord()==null) {
				//some changes happened during the request
				if (!tpAirRvs.getAirSolutionChangedInfo().isEmpty()) {
					for (AirSolutionChangedInfo lInfo : tpAirRvs.getAirSolutionChangedInfo()) {
						resp.setMessage(resp.getMessage() + "\n" + lInfo.getReasonCode());
					}
				}
				return resp;
			}
			
			//log any messages to check possible issues
			for(ResponseMessage lMesage: tpAirRvs.getResponseMessage()) {
				logger.info(lMesage.getValue());
			}
			//set universal locator code
			resp.setBooking_id(tpAirRvs.getUniversalRecord().getLocatorCode());
			AirReservation lReservation = tpAirRvs.getUniversalRecord().getAirReservation().get(0);
			if (lReservation.getAirPricingInfo().isEmpty()) {
				logger.info("Reservation incomplete:  "+ resp.getBooking_id());
				return resp;
			}
			// load traveler and payment info
			TravellerInfo lInfo = req.getTravellerInfo();
			resp.setTraveller_info(lInfo);
			resp.setPayment_info(req.getPaymentInfo());

			//parse output
			this.fillResponse(tpAirRvs.getUniversalRecord(), resp);
			 
			
			/**
			 AirCreate -> After creating you can update commission levels using URModify
			 AirTicket -> Commission Level can be updated
			 */
			//TODO process payment here, please note this reservation still pending till payment is complete and ticketing performed
			resp.setMessage("Flight Confirmation | ...");
			resp.setStatus("success");
			eTicketProcessor.sendEticketEmail(resp);
		}catch (Exception e) {
			logger.error(e);
		}
		return resp;
	}
	
	/**
	 * 
	 * @param pRequest
	 * @param pCustomers
	 * @param pType
	 */
	private void addCustomer(AirCreateReservationReq pRequest, List<Customer> pCustomers, PassengerType pType) {
		
		if (pCustomers == null) return;
		//traveler info, iterate for all passengers
		for (Customer lCustomer : pCustomers) {
			TravelPortTraveler lTraveler = new TravelPortTraveler(lCustomer.getFirstName(),
																  lCustomer.getLastName(),
																  this.getTitle(lCustomer.getGender()));
			lTraveler.setGender(this.getTravelPortGender(lCustomer.getGender()));
			lTraveler.setTravelerType(pType.name());
			if (lCustomer.getPhone()!=null)
				lTraveler.setPhone(lCustomer.getPhone(),PhoneType.Mobile);
			if (lCustomer.getEmail()!=null)
				lTraveler.setEmail(lCustomer.getEmail(),EmailType.Home);
			if (lCustomer.getDateOfBirth()!=null)
				lTraveler.setAge(BigInteger.valueOf(Util.getAgeFromDoB(lCustomer.getDateOfBirth())));
			pRequest.getBookingTraveler().add(lTraveler);
			//https://support.travelport.com/webhelp/uapi/Content/Air/Shared_Air_Topics/SSRs_(Special_Service_Requests).htm
			//passport, if any provided
			if (lCustomer.getPassportNumber() !=null) {
				SSR lSSr = new SSR(); 
				lSSr.setType("DOCS");
				lSSr.setFreeText("P/"+lCustomer.getPassportNumber());
				pRequest.getSSR().add(lSSr);
			}
		}
	}
	
	/**
	 * 
	 * @param gender
	 * @return
	 */
	private String getTitle(String gender) {
		String title = null;
		if(gender != null) {
			switch(gender.toUpperCase()) {
			case "MALE":
				title = "Mr";
				break;
			case "FEMALE":
				title = "Mrs";
				break;
			default:
					title = "Mr";
					break;
			}
		}
		return title;
	}

	/**
	 * 
	 * @param gender
	 * @return
	 */
	private String getTravelPortGender(String gender) {
		String title = null;
		if(gender != null) {
			switch(gender.toUpperCase()) {
			case "MALE":
				title = "M";
				break;
			case "FEMALE":
				title = "F";
				break;
			default:
					title = "M";
					break;
			}
		}
		return title;
	}
	
	/**
	 * 
	 * @param lReservation
	 * @param resp
	 */
	public void fillResponse(UniversalRecord pUniversalRecord, ReservationResponse resp) {
		AirReservation lReservation = pUniversalRecord.getAirReservation().get(0); //get the instance
		AirPricingInfo lPriceInfo = lReservation.getAirPricingInfo().get(0);
		//Helpers----------------------------------------------------------------------------------------------
		FareMap lFaresMap 		  = Helper.createFareInfoMap(lPriceInfo.getFareInfo());
		AirSegmentMap lSegmentMap = Helper.createAirSegmentMap(lReservation.getAirSegment());
		
		//this is not null if ticketing has been done !!!!!
		DocumentInfo lDocumentInfo = lReservation.getDocumentInfo();
		TicketInforByTravelerRefMap lTravelerMap = null;
		if (lDocumentInfo!=null) 
			lTravelerMap = Helper.createTicketInfoByTravelerRefMap(lDocumentInfo.getTicketInfo());
		
		Hashtable<PassengerType, List<Customer>> lTravelers = new Hashtable<PassengerType, List<Customer>>();
		for (BookingTraveler lTraveler: pUniversalRecord.getBookingTraveler()) {
			BookingCustomer lCustomer = mapper.fromBookingTravelerToCustomer(lTraveler);
			//if ticketing ha been done , thre's a document number to assign
			if (lTravelerMap != null) {
				TicketInfo lTicketInfo  = lTravelerMap.getByRef(lTraveler.getKey());
				lCustomer.setTicketNumber(lTicketInfo.getNumber());
			}
			//ticketing should be done, this info is per flight, so it would be better to calculate accordingly
			if (lTraveler.getAirSeatAssignment()!=null) {
				String lSeats ="";
				for (AirSeatAssignment lSeat:lTraveler.getAirSeatAssignment()) {
					lSeats +=lSeat.getSeatTypeCode() + "/"+lSeat.getSeat()+ " ";
				}
				lCustomer.setSeatPreference(lSeats);
			}
			//special requests
			for (SSR lSSr: lTraveler.getSSR()) {
				//if this is displayed in template, you need to provide some text for each ssr type
				//values are SsrType
				lCustomer.addSpecialRequest(lSSr.getType(), lSSr.getFreeText());
			}
			//as street addresses are separated we only parse other date
			if (lTraveler.getAddress() !=null && !lTraveler.getAddress().isEmpty()) {
				TypeStructuredAddress lAddress = lTraveler.getAddress().get(0);
				mapper.fromTypeStructuredAddressToCustomerAddress(lAddress, lCustomer);
				lCustomer.setAddressLine1(lAddress.getStreet().get(0));
				if (lAddress.getStreet().size() >1)
					lCustomer.setAddressLine2(lAddress.getStreet().get(1));
			}
			//phone info
			if (lTraveler.getPhoneNumber()!=null && !lTraveler.getPhoneNumber().isEmpty())
				mapper.fromPhoneNumberToPhoneNumber(lTraveler.getPhoneNumber().get(0), lCustomer);
			
			PassengerType lType = PassengerType.valueOf(lTraveler.getTravelerType()); 
			//add to specific list
			List<Customer> lExistent = lTravelers.get(lType);
			lExistent = lExistent == null ? new ArrayList<Customer>() : lExistent;
			lExistent.add(lCustomer);
			lTravelers.put(lType, lExistent);
		}
		TravellerInfo lTravellerInfo = new TravellerInfo();
		lTravellerInfo.setAdults(lTravelers.get(PassengerType.ADT));
		lTravellerInfo.setChildren(lTravelers.get(PassengerType.CNN));
		lTravellerInfo.setInfants(lTravelers.get(PassengerType.INS));
		resp.setTraveller_info(lTravellerInfo);
		//TODO this hash-set collects airport codes to obtain cities names (*), if you have your own DB, you can remove and get from there
		HashSet<String> lAirports = new HashSet<String>();
		
		//parse fares and segments
		for (BookingInfo lInfo : lPriceInfo.getBookingInfo()) {
			FareInfo lFareInfo = lFaresMap.getByRef(lInfo.getFareInfoRef());
			TypeBaseAirSegment lSegment  = lSegmentMap.getByRef(lInfo.getSegmentRef());
		
			//map segment
			ResponseTrip lTrip = mapper.fromTypeBaseAirSegmentToResponseTrip(lSegment);
			//map price
			mapper.fromFareInfoToResponseTrip(lFareInfo, lTrip);
			
			//baggage allowance
			if (lFareInfo.getBaggageAllowance()!=null) {
				List<BaggageAllowance> lBaggageInfo =  Arrays.asList(new BaggageAllowance(lFareInfo.getBaggageAllowance().getNumberOfPieces().intValue()));
				lTrip.setBaggage_allowances(lBaggageInfo);
			}
			//to search city names by airport later on (*1)----------------------
			lAirports.add(lSegment.getOrigin());
			lAirports.add(lSegment.getDestination());
			//-------------------------------------------------------------------
			lTrip.setTax(lPriceInfo.getTaxes()); //TODO is this ok? or need to check each origin airport tax?
			//out-bounds and inbounds or connections----------------------------------------------------------
		
			//if 1st segment is the tagged as connection and next ones are in same group
			//those segments are connections too!!, check sample https://support.travelport.com/webhelp/uapi/Content/SampleWeb/SampleFiles/035-03_1G_AirBook_Rs.xml
			boolean isConnection = false;
			if (lSegment.getGroup() == 0) {
				if (resp.getOutbound() == null)
					resp.setOutbound(lTrip);
				else
					isConnection = true;
			}else{
				if (resp.getInbound() == null)
					resp.setInbound(lTrip);
				else
					isConnection = true;	
			}
			
			if (lSegment.getConnection()!=null || isConnection) {
				//more than one flight details means chain of plane
				for (FlightDetails lConnection: lSegment.getFlightDetails()) {
					//map details
					ResponseTrip lTripConnection;
					try {
						lTripConnection = lTrip.clone();
						mapper.fromFlightDetailsToResponseTrip(lConnection, lTripConnection);
						//to search city names by airport later on (*2)-----------
						lAirports.add(lConnection.getOrigin());
						lAirports.add(lConnection.getDestination());
						//--------------------------------------------------------
						if (lSegment.getGroup() == 0) {//outbound
							resp.getOutbound().addConnection(lTripConnection);
						}else{//inbound
							resp.getInbound().addConnection(lTripConnection);
						}
					} catch (CloneNotSupportedException e) {
						logger.error(e.getMessage());
						e.printStackTrace();
					}
				}
				if (lSegment.getConnection()!=null) {//if 1st connection, set segment as "Many"
					lTrip.setFlight_no("Many");
				}
				// end of out-bounds and in-bounds or connections----------------------------------------------------	
			}else {
				//map its flight detail
				mapper.fromFlightDetailsToResponseTrip(lSegment.getFlightDetails().get(0),lTrip);
			}
		}
		//fare notes
		if (lReservation.getFareNote()!=null)
		for (FareNote lNote : lReservation.getFareNote()) {
			resp.addRule(lNote.getValue());
		}
		//query city names for all airport found (*3)------------------------------------------------------------
		Hashtable<String, String> lAirportCity = referenceRetrieveService.getCitiesByAirportList(lAirports.toArray(new String[lAirports.size()]));
		this.setCityNames(resp.getOutbound(), lAirportCity);
		if (resp.getOutbound().getConnections()!=null)
			for (ResponseTrip lConnection : resp.getOutbound().getConnections()) {
				this.setCityNames(lConnection, lAirportCity);
			}
		if(resp.getInbound() !=null) {
			this.setCityNames(resp.getInbound(), lAirportCity);
			for (ResponseTrip lConnection : resp.getInbound().getConnections()) {
				this.setCityNames(lConnection, lAirportCity);
			}		
		}
		//-------------------------------------------------------------------------------------------------------
		// check final fares
		resp.setTotal_price(lReservation.getAirPricingInfo().get(0).getTotalPrice());
	}
	/**
	 * 
	 * @param lTrip
	 * @param lAirportCity
	 */
	private void setCityNames(ResponseTrip lTrip, Hashtable<String, String> lAirportCity ) {
		String lDepCity = lAirportCity.get(lTrip.getDeparture_airport());
		String lArrCity = lAirportCity.get(lTrip.getArriavel_airport());
		lTrip.setDeparture_city(lDepCity);
		lTrip.setArrival_city(lArrCity);
	}
	
	/**
	 * 
	 * @param pLocatorCode
	 */
	public void doTicketing(String pLocatorCode, String pAirPricingRef) {
		AirTicketingReq lAirTicketingReq = new AirTicketingReq();
		try {
			//reservation code parameter
			AirReservationLocatorCode lCode = new AirReservationLocatorCode();
			lCode.setValue(pLocatorCode);
			lAirTicketingReq.setAirReservationLocatorCode(lCode);
			//price reference parameter
			AirTicketingReq.AirPricingInfoRef lAirPricingRef = new AirTicketingReq.AirPricingInfoRef();
			lAirPricingRef.setKey(pAirPricingRef);
			lAirTicketingReq.getAirPricingInfoRef().add(lAirPricingRef);
			//modify commissions if necessary
			//lAirTicketingReq.getCommission().add(...)
			
			//update payment stuff
			
			//those are always necessary
			lAirTicketingReq.setTargetBranch(services.getTargetBranch());
			lAirTicketingReq.setBillingPointOfSaleInfo(services.getBillingPointOfSaleInfo());

			AirTicketingRsp lAirTicketingRsp = services.getAirTicket().get().service(lAirTicketingReq);
//			for (ETR lETR: lAirTicketingRsp.getETR()) {
//				//if you need to do something with it
//			}
			System.out.println(lAirTicketingRsp.getETR().get(0).getProviderCode());
			//after ticketing call universal record and resend email
			
		} catch (AirFaultMessage e) {
			logger.error(e.getMessage());
		}
	}
	
	/**
	 * 
	 * @param pReservationCode
	 */
	public void reSendEmailReservationRequest(ReservationRequest lReq,String pReservationCode) {
		UniversalRecordRetrieveRsp lResp = universalService.retrieveUniversalRecordLocator(pReservationCode);
		//get universal record if any
		UniversalRecord resp = lResp.getUniversalRecord();
		//parse
		ReservationResponse lOutput = new ReservationResponse();
		lOutput.setBooking_id(pReservationCode);
		this.fillResponse(resp, lOutput);
		lOutput.setTraveller_info(lReq.getTravellerInfo());
		lOutput.setPayment_info(lReq.getPaymentInfo());
		//send email
		eTicketProcessor.sendEticketEmail(lOutput);
	}
	
}
