/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.universal.api.service.config;

import com.travelport.support.TravelPortServicesHelper;
import com.travelport.support.config.PropertiesLoader;
import com.travelport.support.config.TravelPortConfig;

/**
 * 
 * @author amassillo
 *
 */
public class AppConfig {

	private static TravelPortServicesHelper services;
	private static EmailConfig emailConfig;
	static {
		TravelPortConfig lConfig = new PropertiesLoader<TravelPortConfig>().loadForInstance("travelport-helper.yml", TravelPortConfig.class);
		services 	= new TravelPortServicesHelper(lConfig);
		emailConfig = new PropertiesLoader<EmailConfig>().loadForInstance("email-properties.yml", EmailConfig.class);
	}
	
	public static TravelPortServicesHelper getTravelPortConnector() {
		return services;
	}
	
	public static EmailConfig getEmailConfig() {
		return emailConfig;
	}
}
