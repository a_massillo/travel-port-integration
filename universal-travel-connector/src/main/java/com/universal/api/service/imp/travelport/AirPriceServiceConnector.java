/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.universal.api.service.imp.travelport;


import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;
import com.safarifone.waafitravels.common.WaafiTravelsException;
import com.travelport.schema.air_v43_0.AirItinerary;
import com.travelport.schema.air_v43_0.AirPriceReq;
import com.travelport.schema.air_v43_0.AirPriceRsp;
import com.travelport.schema.air_v43_0.AirPricingCommand;
import com.travelport.schema.air_v43_0.TypeBaseAirSegment;
import com.travelport.schema.common_v43_0.SearchPassenger;
import com.travelport.support.TravelPortServicesHelper;
import com.travelport.support.request.enums.PassengerType;
import com.universal.api.service.config.AppConfig;
import com.universal.api.service.response.AirAvailabilityResponse;

/**
 * Returns pricing solution for specified itineraries  
 * @author amassillo
 * @date 01/18/2018
 */

public class AirPriceServiceConnector{
	
	private TravelPortServicesHelper services = AppConfig.getTravelPortConnector();

	
	private TypeBaseAirSegment buildSearchSegment(Map<String, Object> pDeparture, int pGroup) {
		TypeBaseAirSegment lReq = new TypeBaseAirSegment();
		lReq.setKey(RandomStringUtils.randomAlphabetic(10));
		lReq.setOrigin(pDeparture.get("departure_airport").toString());
		lReq.setDestination(pDeparture.get("arrival_airport").toString());
		lReq.setFlightNumber(pDeparture.get("flight_no").toString());
		lReq.setCabinClass(pDeparture.get("cabin_code").toString());
		lReq.setDepartureTime(pDeparture.get("departure_date").toString());
		lReq.setArrivalTime(pDeparture.get("arrival_date").toString());
		lReq.setCarrier(pDeparture.get("airline_id").toString());
		lReq.setProviderCode(pDeparture.get("provider_code").toString());// TOCDO check if I can use extra parameters
		lReq.setGroup(pGroup);
		return lReq;
	}

	public AirPriceRsp checkPrice(int pSeatCount,
			Map<String, Object> outbound,Map<String, Object> inbound, 
			Map<String, Object> outboundSecondSegment, Map<String, Object> inboundSSecondSegment) throws WaafiTravelsException {
		AirItinerary lItinerary = new AirItinerary();
		//set air itinerary
		lItinerary.getAirSegment().add(this.buildSearchSegment(outbound,0));
		if (inbound !=null)
			lItinerary.getAirSegment().add(this.buildSearchSegment(inbound,1));
		if (outboundSecondSegment !=null)
			lItinerary.getAirSegment().add(this.buildSearchSegment(outboundSecondSegment,0));
		if (inboundSSecondSegment !=null)
			lItinerary.getAirSegment().add(this.buildSearchSegment(inboundSSecondSegment,1));
		//not setting cabin class, since each is going in its segment
		return this.checkPrice(pSeatCount,null,lItinerary);	
	}
	
	public AirPriceRsp checkPrice(int pSeatCount,List<AirAvailabilityResponse> itinerary) throws WaafiTravelsException {
		AirItinerary lItinerary = new AirItinerary();
		//set air itinerary
		for (AirAvailabilityResponse lSegment : itinerary) {
			TypeBaseAirSegment lReq = new TypeBaseAirSegment();
			lReq.setKey(RandomStringUtils.randomAlphabetic(10));
			lReq.setOrigin(lSegment.getDepAirport());
			lReq.setDestination(lSegment.getArrAirport());
			lReq.setFlightNumber(lSegment.getFlightNumber());
			lReq.setCabinClass(lSegment.getCabinCode());
			lReq.setDepartureTime(lSegment.getDepDateTimeTravelPort());
			lReq.setArrivalTime(lSegment.getArrDateTimeTravelPort());
			lReq.setCarrier(lSegment.getAirlineId());
			lReq.setProviderCode("1G");// TOCDO check if I can use extra parameters
			lReq.setGroup(0);
			lItinerary.getAirSegment().add(lReq);
		}
		//not setting cabin class, since each is going in its segment
		return this.checkPrice(pSeatCount,null,lItinerary);	
	}
	/**
	 * Provides availability check based in low fare query 
	 * @param reqItinerary
	 * @throws WaafiTravelsException 
	 */
	public AirPriceRsp checkPrice(int pSeatCount, String pCabinClass,
								  AirItinerary pItinerary) throws WaafiTravelsException {
	
		AirPriceReq lReq = new AirPriceReq();
		//all segments to query 
		lReq.setAirItinerary(pItinerary);		
		AirPriceRsp airPricing = null;
		try {
			//add additional required parameters
			for (int i = 0; i< pSeatCount; i++) {
				SearchPassenger adult = new SearchPassenger();
				adult.setCode(PassengerType.ADT.name());
				adult.setBookingTravelerRef(RandomStringUtils.randomAlphabetic(10));
				lReq.getSearchPassenger().add(adult);
			}
			//set cabin
			AirPricingCommand command = new AirPricingCommand();
			if (pCabinClass !=null)
				command.setCabinClass(pCabinClass);
			lReq.getAirPricingCommand().add(command);
			//those are always necessary-----------------------------------------
			lReq.setTargetBranch(services.getTargetBranch());
			lReq.setBillingPointOfSaleInfo(services.getBillingPointOfSaleInfo());	
			services.getAirPrice().showXML(true);
			airPricing = services.getAirPrice().get().service(lReq);
			if (airPricing.getAirPriceResult().isEmpty()) {
				throw new WaafiTravelsException("Price not available");
			}
			//ideally we just send this variable as return and save it for air booking service
		} catch (Exception e) {
			throw new WaafiTravelsException(e.getMessage());
		}
		return airPricing;
	}
}
