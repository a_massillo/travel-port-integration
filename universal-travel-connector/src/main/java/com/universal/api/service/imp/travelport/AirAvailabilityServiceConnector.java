/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/

package com.universal.api.service.imp.travelport;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.mapstruct.factory.Mappers;

import com.safarifone.waafitravels.common.Itinerary;
import com.safarifone.waafitravels.common.Trip;
import com.safarifone.waafitravels.common.WaafiTravelsException;
import com.travelport.schema.air_v43_0.AirAvailInfo;
import com.travelport.schema.air_v43_0.AirItinerary;
import com.travelport.schema.air_v43_0.AirPriceRsp;
import com.travelport.schema.air_v43_0.AirPricingSolution;
import com.travelport.schema.air_v43_0.AvailabilitySearchReq;
import com.travelport.schema.air_v43_0.AvailabilitySearchRsp;
import com.travelport.schema.air_v43_0.BookingCodeInfo;
import com.travelport.schema.air_v43_0.Connection;
import com.travelport.schema.air_v43_0.TypeBaseAirSegment;
import com.travelport.service.air_v43_0.AirFaultMessage;
import com.travelport.support.request.AirBaseLowFareReq;
import com.travelport.support.request.SearchAirLegExt;
import com.travelport.support.request.enums.PassengerType;
import com.universal.api.dto.mapper.AirSearchMapper;
import com.universal.api.service.UniversalAirAvailabilityTravelConnector;
import com.universal.api.service.config.AppConfig;
import com.universal.api.service.response.AirAvailabilityResponse;
import com.travelport.support.Helper;
import com.travelport.support.TravelPortServicesHelper;

/**
 * https://support.travelport.com/webhelp/uapi/uAPI.htm#Air/Air_Availability/Air_Availability.htm%3FTocPath%3DAir%7CAir%2520Shopping%2520and%2520Booking%7CAir%2520Availability%7C_____0
 * Returns Itineraries with focus in availability, doesn't provide any information about prices, if needed need to call AirPrice servcie
 * @author amassillo
 * @date 01/18/2018
 */

public class AirAvailabilityServiceConnector implements UniversalAirAvailabilityTravelConnector{

	final static Logger logger = Logger.getLogger(AirAvailabilityServiceConnector.class);

	private TravelPortServicesHelper services;
	private AirSearchMapper mapper;
	private AirPriceServiceConnector airPriceService;
	private String searchId;
	
	//collects responses from threads
	private ArrayList<AirAvailabilityResponse> allOutSegments;
	private ArrayList<AirAvailabilityResponse> allInSegments;

	public AirAvailabilityServiceConnector() {
		airPriceService = new AirPriceServiceConnector();
		mapper	 = Mappers.getMapper(AirSearchMapper.class);
		services = AppConfig.getTravelPortConnector();
	}
	
	/**
	 * Provides availability check based in low fare query 
	 * @param reqItinerary
	 */
	public void check(Itinerary reqItinerary) {
		//save searchid for later on
		this.searchId = reqItinerary.getSearchId();
		//setup request
		AvailabilitySearchReq lOutboundRequest = this.setUpRequest(reqItinerary);	
		List<Trip> trips  = reqItinerary.getTrips();
		for(Trip trip : trips ) {
			SearchAirLegExt outbound = new SearchAirLegExt(trip.getDeparture_city(), 
														   trip.getArrival_city(),trip.getDeparture_date());
			lOutboundRequest.getSearchAirLeg().add(outbound);

			//one or two threads according return date
			int poolSize = trip.getReturn_date()!=null ? 2: 1;
			ExecutorService exec = Executors.newFixedThreadPool(poolSize);
			Future<ArrayList<AirAvailabilityResponse>> leaveSegFuture = exec.submit(new Worker(lOutboundRequest,"outbound"));
			Future<ArrayList<AirAvailabilityResponse>> returnSegFuture = null;

			if (trip.getReturn_date()!=null) {
				AvailabilitySearchReq lInboundRequest = this.setUpRequest(reqItinerary);	
				SearchAirLegExt inboud = new SearchAirLegExt(trip.getArrival_city(), 
														     trip.getDeparture_city(), trip.getReturn_date());
				lInboundRequest.getSearchAirLeg().add(inboud);
				returnSegFuture = exec.submit(new Worker(lInboundRequest,"inbound"));
			}
			// Wait for segment workers to complete
			try {
					while(!leaveSegFuture.isDone()) {
						Thread.sleep(10);
					}
					allOutSegments.addAll(leaveSegFuture.get());
					if(poolSize == 2) {
						while(returnSegFuture !=null && !returnSegFuture.isDone()) {
							Thread.sleep(10);
						}
						allInSegments.addAll(returnSegFuture.get());
					}
			} catch (InterruptedException e) {
					logger.error(e.getMessage());
			} catch (ExecutionException e) {
					logger.error(e.getMessage());
			}
			exec.shutdown();
		}
	}

	/**
	 * Setup request, would have used clone, but it's not implemented by default in axis2 clients schemas
	 * @param reqItinerary
	 * @return
	 */
	private AvailabilitySearchReq setUpRequest(Itinerary reqItinerary) {
		//setup request
		AvailabilitySearchReq lRequest = new AvailabilitySearchReq();
		//travelers
		int numAdults = reqItinerary.getNumberAdults();
		numAdults = numAdults < 0 ? 1 : numAdults;
		
		int numChildren = reqItinerary.getNumberChildren();
		numChildren =  numChildren < 0 ? 0 : numChildren;
		
		//helper
		AirBaseLowFareReq h1 = new AirBaseLowFareReq();
		h1.addPassengers(numAdults + numChildren, PassengerType.ADT);
		
		lRequest.getSearchPassenger().addAll(h1.getSearchPassenger());
		return lRequest;

	}

	//getters for the accumulated responses
	public ArrayList<AirAvailabilityResponse> getAllOutSegments() {
		return allOutSegments;
	}

	public ArrayList<AirAvailabilityResponse> getAllInSegments() {
		return allInSegments;
	}

	/**
	 * 
	 * @author amassillo
	 *
	 */
	private class Worker implements Callable<ArrayList<AirAvailabilityResponse>> {
		//request
		private AvailabilitySearchReq request;
		private String segment;
		
		public Worker(AvailabilitySearchReq pRequest,String pSegment) {
			this.request = pRequest;
			this.segment = pSegment;
		}

		@Override
		public ArrayList<AirAvailabilityResponse> call() throws WaafiTravelsException{
			ArrayList<AirAvailabilityResponse> responses = new ArrayList<AirAvailabilityResponse>();
			//those are always necessary
			request.setTargetBranch(services.getTargetBranch());
			request.setBillingPointOfSaleInfo(services.getBillingPointOfSaleInfo());
			//execute request for this trip			
			AvailabilitySearchRsp lResp;
			try {
				lResp = services.getAirAvailability().get().service(request);
			} catch (AirFaultMessage e) {
				throw new WaafiTravelsException("Not able to request availability: " + e.getMessage());
			}
			//collect connections and flight details
			//FlightDetailsMap lMap = Helper.createFlightDetailsMap(lResp.getFlightDetailsList().getFlightDetails());
			//https://support.travelport.com/webhelp/uAPI/Content/Air/Shared_Air_Topics/AirSegmentConnectionLogic.htm
			List<Connection> lConnections = lResp.getAirItinerarySolution().get(0).getConnection();
			HashSet<Integer> lIndexes = new HashSet<Integer>() ;
			for (Connection lConnection: lConnections) {
				//only connecting flight is stopOver = false
				if (!lConnection.isStopOver())
					lIndexes.add(lConnection.getSegmentIndex());
			}
			int lSeatCount = request.getSearchPassenger().size();
			//parse response
			int lSegmentIndex = 0;
			for(TypeBaseAirSegment lAirSegment : lResp.getAirSegmentList().getAirSegment()) {

				//if no availability info continue
				if (lAirSegment.getAirAvailInfo().size() <1) continue;
				
				//lAirSegment.getFlightDetails().addAll(lMap.getByRef(lAirSegment.getFlightDetailsRef()));
				lAirSegment.getFlightDetailsRef().clear(); //clear reference, to avoid conflicts with references
				
				//price this segment---- for each available cabin class ----------------------------------------------------------
				for (AirAvailInfo lInfo : lAirSegment.getAirAvailInfo()) {
					for (BookingCodeInfo lBookinginfo : lInfo.getBookingCodeInfo()) {
						lAirSegment.setProviderCode(lInfo.getProviderCode());
						AirItinerary lItinerary = new AirItinerary();
						lItinerary.getAirSegment().add(lAirSegment);
						AirAvailabilityResponse lResponse 	= new AirAvailabilityResponse();
						try {
							AirPriceRsp airPriceResult 		= airPriceService.checkPrice(lSeatCount,lBookinginfo.getCabinClass(), lItinerary);
							AirPricingSolution airPricing 	= airPriceResult.getAirPriceResult().get(0)
																		    .getAirPricingSolution().get(0);	
							//start response mapping !!!
							mapper.fromTypeBaseAirSegmentToReservationResponse(lAirSegment, lResponse);
							//segment is inbound or outbound in NonGDSAvailability because availability is queried in different calls
							lResponse.setSegment(segment);
							lResponse.setSearchId(searchId);
							lResponse.setPrice(Helper.parseNumberWithCurrency(airPricing.getBasePrice()));
							lResponse.setTax(Helper.parseNumberWithCurrency(airPricing.getTaxes()));
							lResponse.setCabinCode(lBookinginfo.getCabinClass());
							String lCurrency = airPricing.getBasePrice().substring(0,3);
							lResponse.setCurrency(lCurrency); //no currency code in air price response
						}catch(Exception ex) {
							System.out.println("Could not fetch price for segment: " + lAirSegment.getKey() + " in booking class: "+ lBookinginfo.getCabinClass());
							continue;
						}
						//is this a connecting flight?
						if (lIndexes.contains(lSegmentIndex)) {
							lResponse.setStop(1);
							lResponse.setLine(lSegmentIndex);//connecting flight to the segment key, is this ok?
							lResponse.setFinalDest(Helper.getFinalDestination(lResp.getAirSegmentList().getAirSegment(), lSegmentIndex));// how to get it, if no final destination present in response?,
						}
						lSegmentIndex++;
						//collect data
						responses.add(lResponse);
					}
				}
			}
			return responses;
		}	
	}
}
