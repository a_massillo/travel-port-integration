/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.universal.api.service;

import java.util.Map;

import com.safarifone.waafitravels.common.ServiceResponse;
import com.safarifone.waafitravels.common.WaafiTravelsException;

/**
 * 
 * @author amassillo
 *
 */
public interface UniversalAirCancellationTravelConnector {

	public ServiceResponse cancel(Map<String, Object> resMap, String reservationId) throws WaafiTravelsException;
}
