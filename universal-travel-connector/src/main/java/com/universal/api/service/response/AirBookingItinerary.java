/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.universal.api.service.response;

import java.math.BigDecimal;
import java.util.Date;

import com.universal.api.service.common.UniversalConstants;
import com.universal.api.service.common.UniversalUtils;



/**
 * 
 * @author amassillo
 * this class has all parameters needed to insert record in DB as per defined in
 * NonGDSReservation createReservation method
 * 
 */

public class AirBookingItinerary {

	private String departureCity;
	private String arrivalCity;
	//custom fields----------
	private Date depDateTimeLocal;
	private Date arrDateTimeLocal;
	//end of custom fields---
	private String departureDate;
	private String departureTime;
	private String arrivalDate;
	private String arrivalTime;
	private int flightNo;
	private String airlineName;
	private BigDecimal price;
	private BigDecimal tax;
	private BigDecimal segmentTotalPrice;
	private String rloc;
	private String cabin;
	private String status;
	private String airlineId;
	private String numSegments;
	private String seatNumber;
	private Integer numAdults;
	private Integer numChildren;
	private Integer numInfants;
	private String searchId;
	private String validTo;

	private String utcDepDatetime;
	private String utcArrDatetime;
	private Integer segmentSeg;
	
	public String getDepartureCity() {
		return departureCity;
	}
	public void setDepartureCity(String departureCity) {
		this.departureCity = departureCity;
	}
	public String getArrivalCity() {
		return arrivalCity;
	}
	public void setArrivalCity(String arrivalCity) {
		this.arrivalCity = arrivalCity;
	}
	public String getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}
	public String getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}
	public String getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public String getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public int getFlightNo() {
		return flightNo;
	}
	public void setFlightNo(int flightNo) {
		this.flightNo = flightNo;
	}
	public String getAirlineName() {
		return airlineName;
	}
	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getTax() {
		return tax;
	}
	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}
	public BigDecimal getSegmentTotalPrice() {
		return segmentTotalPrice;
	}
	public void setSegmentTotalPrice(BigDecimal segmentTotalPrice) {
		this.segmentTotalPrice = segmentTotalPrice;
	}
	public String getRloc() {
		return rloc;
	}
	public void setRloc(String rloc) {
		this.rloc = rloc;
	}
	public String getCabin() {
		return cabin;
	}
	public void setCabin(String cabin) {
		this.cabin = cabin;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAirlineId() {
		return airlineId;
	}
	public void setAirlineId(String airlineId) {
		this.airlineId = airlineId;
	}
	public String getNumSegments() {
		return numSegments;
	}
	public void setNumSegments(String numSegments) {
		this.numSegments = numSegments;
	}
	public String getSeatNumber() {
		return seatNumber;
	}
	public void setSeatNumber(String seatNumber) {
		this.seatNumber = seatNumber;
	}
	public Integer getNumAdults() {
		return numAdults;
	}
	public void setNumAdults(Integer numAdults) {
		this.numAdults = numAdults;
	}
	public Integer getNumChildren() {
		return numChildren;
	}
	public void setNumChildren(Integer numChildren) {
		this.numChildren = numChildren;
	}
	public Integer getNumInfants() {
		return numInfants;
	}
	public void setNumInfants(Integer numInfants) {
		this.numInfants = numInfants;
	}
	public String getSearchId() {
		return searchId;
	}
	public void setSearchId(String searchId) {
		this.searchId = searchId;
	}
	public String getValidTo() {
		return validTo;
	}
	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}
	public String getUtcDepDatetime() {
		return utcDepDatetime;
	}
	public void setUtcDepDatetime(String utcDepDatetime) {
		this.utcDepDatetime = utcDepDatetime;
	}
	public String getUtcArrDatetime() {
		return utcArrDatetime;
	}
	public void setUtcArrDatetime(String utcArrDatetime) {
		this.utcArrDatetime = utcArrDatetime;
	}
	public Integer getSegmentSeg() {
		return segmentSeg;
	}
	public void setSegmentSeg(Integer segmentSeg) {
		this.segmentSeg = segmentSeg;
	}

	public void setDepDateTimeLocal(String depDateTimeLocal) {
		this.depDateTimeLocal = UniversalUtils.dateFromISO8601(depDateTimeLocal);
		this.departureDate = UniversalConstants.dateLocalFormat.format(this.depDateTimeLocal);
		this.departureTime = UniversalConstants.timeLocalFormat.format(this.depDateTimeLocal);
		this.utcDepDatetime = UniversalConstants.dateTimeFormatGMT.format(this.depDateTimeLocal);
	}

	public void setArrDateTimeLocal(String arrDateTimeLocal) {		
		this.arrDateTimeLocal = UniversalUtils.dateFromISO8601(arrDateTimeLocal);
		this.arrivalDate = UniversalConstants.dateLocalFormat.format(this.arrDateTimeLocal);
		this.arrivalTime = UniversalConstants.timeLocalFormat.format(this.arrDateTimeLocal);
		this.utcArrDatetime = UniversalConstants.dateTimeFormatGMT.format(this.arrDateTimeLocal);
	}
	
}
