/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.universal.api.service;


import com.safarifone.waafitravels.common.Itinerary;

/**
 * 
 * @author amassillo
 *
 */
public interface UniversalAirAvailabilityTravelConnector {

	public void check(Itinerary reqItinerary);
}
