/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.universal.api.dto.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

import com.travelport.schema.air_v43_0.FlightDetails;
import com.travelport.schema.air_v43_0.TypeBaseAirSegment;
import com.universal.api.service.response.AirAvailabilityResponse;

/**
 * 
 * @author amassillo
 * @date	01/17/2018
 *
 */
@Mapper
public interface AirSearchMapper {
	
	
	  @Mappings({
	        @Mapping(source = "origin", target = "depAirport"),
	        @Mapping(source = "destination", target = "arrAirport"),
	        @Mapping(source = "departureTime", target = "depDateTimeLocal"),
	        @Mapping(source = "arrivalTime", target = "arrDateTimeLocal"),
	        @Mapping(source = "flightTime", target = "duration"),
	        @Mapping(source = "carrier", target = "airlineId"),
	        @Mapping(source = "flightNumber", target = "flightNumber"),
	        @Mapping(source = "numberOfStops", target = "stop"),
	        @Mapping(source = "distance", target = "miles"), //in response node "DistanceUnits"
	        @Mapping(source = "destination", target = "finalDest"),    
	    })
	public void fromTypeBaseAirSegmentToReservationResponse(TypeBaseAirSegment pTravelPortRresponse, @MappingTarget AirAvailabilityResponse pResponse);
	  
	  @Mappings({
	        @Mapping(source = "origin", target = "depAirport"),
	        @Mapping(source = "destination", target = "arrAirport"),
	        @Mapping(source = "departureTime", target = "depDateTimeLocal"),
	        @Mapping(source = "arrivalTime", target = "arrDateTimeLocal"),
	        @Mapping(source = "flightTime", target = "duration"),
	       // @Mapping(source = "carrier", target = "airlineId"),
	        //@Mapping(source = "flightNumber", target = "flightNumber"),
	      //  @Mapping(source = "numberOfStops", target = "stop"),
	        @Mapping(source = "distance", target = "miles"), //in response node "DistanceUnits"
	       // @Mapping(source = "destination", target = "finalDest"),    
	    })
	public void fromFlightDetailsToReservationResponse(FlightDetails pFlightDetail, @MappingTarget AirAvailabilityResponse pResponse);
	  
	
}
