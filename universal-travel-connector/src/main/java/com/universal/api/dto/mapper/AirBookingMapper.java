/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.universal.api.dto.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

import com.safarifone.waafitravels.common.ResponseTrip;
import com.safarifone.waafitravels.customer.Customer;
import com.travelport.schema.air_v43_0.FareInfo;
import com.travelport.schema.air_v43_0.FlightDetails;
import com.travelport.schema.air_v43_0.TypeBaseAirSegment;
import com.travelport.schema.common_v43_0.BookingTraveler;
import com.travelport.schema.common_v43_0.PhoneNumber;
import com.travelport.schema.common_v43_0.TypeStructuredAddress;
import com.universal.api.service.response.AirBookingItinerary;
import com.universal.api.service.response.BookingCustomer;


/**
 * 
 * @author amassillo
 * @date	01/30/2018
 *
 */
@Mapper
public interface AirBookingMapper {
	
	
	  @Mappings({
	        @Mapping(source = "origin", target = "departureCity"),
	        @Mapping(source = "destination", target = "arrivalCity"),
	        @Mapping(source = "departureTime", target = "depDateTimeLocal"),
	        @Mapping(source = "arrivalTime", target = "arrDateTimeLocal"),
	        @Mapping(source = "carrier", target = "airlineName"),
	        @Mapping(source = "flightNumber", target = "flightNo"),
	        @Mapping(source = "cabinClass", target = "cabin"),
//	        @Mapping(source = "", target = "rloc"), //TODO
	        
	        //TODO probably needs parsing, those are the travelport statuses
	        ////https://support.travelport.com/webhelp/uapi/Content/Air/Shared_Air_Topics/Air_Status_Codes.htm
	        @Mapping(source = "status", target = "status"),
//	        @Mapping(source = "", target = "airlineId"),
	        
//	        @Mapping(source = "", target = "numSegments"),
//	        @Mapping(source = "", target = "seatNumber"),
//	        @Mapping(source = "", target = "numAdults"),
//	        @Mapping(source = "", target = "numChildren"),
//	        @Mapping(source = "", target = "numInfants"),
//	        @Mapping(source = "", target = "validTo"),
//	        @Mapping(source = "", target = "segmentSeg"), //TODO
	    })
	public AirBookingItinerary fromTypeBaseAirSegmentToAirBookingItinerary(TypeBaseAirSegment pTravelPortRresponse);

	  @Mappings({
		  @Mapping(source = "amount", target = "price"),//TODO
//	      @Mapping(source = "", target = "segmentTotalPrice"),
	  })
	public void fromTypeBaseAirSegmentToAirBookingItinerary(FareInfo pTravelPortRresponse, @MappingTarget AirBookingItinerary pAirBookingItinerary);  
	  
	  @Mappings({
	        @Mapping(source = "origin", target = "departure_airport"),
	        @Mapping(source = "destination", target = "arriavel_airport"),
	        //TODO cities
	        @Mapping(source = "departureTime", target = "departure_date"),
	        @Mapping(source = "arrivalTime", target = "arrival_date"),
	        
	        //@Mapping(source = "flightTime", target = "flight_time"),
	        @Mapping(source = "carrier", target = "airline_name"),
	       // @Mapping(source = "", target = "airlineId"),
	        @Mapping(source = "flightNumber", target = "flight_no"),
	        
	        @Mapping(source = "cabinClass", target = "cabin"),
	        //@Mapping(source = "", target = "stop_city"), //TODO
	        @Mapping(source = "distance", target = "distance"),
	        @Mapping(constant = "GDS", target = "provider"),
	        @Mapping(source = "equipment", target = "equipment"),
	        
	    })
	public ResponseTrip fromTypeBaseAirSegmentToResponseTrip(TypeBaseAirSegment pTravelPortRresponse);
	  
	  @Mappings({
	        @Mapping(source = "amount", target = "price"),//TODO
	      //@Mapping(source = "", target = "currency"),
	      //  @Mapping(source = "endorsement", target = "rules"),
	    })  
	public void fromFareInfoToResponseTrip(FareInfo pFareInfo, @MappingTarget ResponseTrip pResponseTrip);

	  @Mappings({
		  @Mapping(source = "originTerminal", target =  "departure_terminal", defaultValue="NA"),
	      @Mapping(source = "destinationTerminal", target = "arrival_terminal", defaultValue="NA"),
	      @Mapping(source = "departureTime", target = "departure_date"),
	      @Mapping(source = "arrivalTime", target = "arrival_date"),
	      @Mapping(source = "flightTime", target = "flight_time"),
	    })  
	public void fromFlightDetailsToResponseTrip(FlightDetails pFlightDetails, @MappingTarget ResponseTrip pResponseTrip);
  
	  @Mappings({
		  @Mapping(source = "bookingTravelerName.first", target = "firstName"),
	      @Mapping(source = "bookingTravelerName.last", target = "lastName"),
	      @Mapping(source = "gender", target = "gender"),
	      @Mapping(target = "email", ignore = true),
	    })  
	public BookingCustomer fromBookingTravelerToCustomer(BookingTraveler pTraveler);
	
	  @Mappings({
//		  @Mapping(source = "street (0)", target = "addressLine1"),
//	      @Mapping(source = "street (1)", target = "addressLine2"),
	      @Mapping(source = "postalCode", target = "zipCode"),
	      @Mapping(source = "country", target = "country"),
	      @Mapping(source = "state.value", target = "state"), // State is a class in tp
	      @Mapping(source = "city", target = "city"),
	    })  
	public void fromTypeStructuredAddressToCustomerAddress(TypeStructuredAddress pAddress, @MappingTarget Customer pCustomer);  
	  
	  @Mappings({
	      @Mapping(expression = "java(source.getCountryCode()+ source.getAreaCode()+ source.getNumber())", target = "phone"),
	    })  
	public void fromPhoneNumberToPhoneNumber(PhoneNumber source, @MappingTarget Customer pCustomer);
}
