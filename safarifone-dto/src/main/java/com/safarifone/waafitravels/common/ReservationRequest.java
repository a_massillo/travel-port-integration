/***********************************************************************************
 * CopyRight (c) Sep 16, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.safarifone.waafitravels.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReservationRequest {

	@JsonProperty("search_id")
	private String searchId;
	
	@JsonProperty("outbound_trip_id")
	private int outboundTripId;
	
	@JsonProperty("inbound_trip_id")
	private int inboundTripId;
	
	@JsonProperty("agent_id")
	private String agentId;
	
	@JsonProperty("traveller_info")
	private TravellerInfo travellerInfo;
	
	@JsonProperty("payment_info")
	private PaymentInfo paymentInfo;
	
	@JsonProperty("token_id")
	private String tokenId;
	
	@JsonProperty("callback_url")
	private String callbackUrl;
	
	public ReservationRequest() {
		
	}

	public String getSearchId() {
		return searchId;
	}

	public void setSearchId(String searchId) {
		this.searchId = searchId;
	}

	public int getOutboundTripId() {
		return outboundTripId;
	}

	public void setOutboundTripId(int outboundTripId) {
		this.outboundTripId = outboundTripId;
	}

	public int getInboundTripId() {
		return inboundTripId;
	}

	public void setInboundTripId(int inboundTripId) {
		this.inboundTripId = inboundTripId;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public TravellerInfo getTravellerInfo() {
		return travellerInfo;
	}

	public void setTravellerInfo(TravellerInfo travellerInfo) {
		this.travellerInfo = travellerInfo;
	}

	public PaymentInfo getPaymentInfo() {
		return paymentInfo;
	}

	public void setPaymentInfo(PaymentInfo paymentInfo) {
		this.paymentInfo = paymentInfo;
	}

	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}


}
