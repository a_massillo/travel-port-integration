package com.safarifone.waafitravels.common;

public class WaafiTravelsException extends Exception{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public WaafiTravelsException(String message) {
		super(message);
	}
}
