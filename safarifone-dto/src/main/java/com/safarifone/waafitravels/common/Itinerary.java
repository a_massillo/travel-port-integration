/***********************************************************************************
 * CopyRight (c) Aug 28, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.safarifone.waafitravels.common;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Itinerary {

	@JsonProperty("trips")
	private List<Trip> trips;
	
	@JsonProperty("number_adults")
	private int numberAdults;
	
	@JsonProperty("number_children")
	private int numberChildren;
	
	@JsonProperty("number_infants")
	private int numberInfants;
	
	@JsonProperty("cabin")
	private String cabin;
	
	@JsonProperty("sort")
	private String sort;
	
	@JsonProperty("order")
	private String order;
	
	@JsonProperty("currency_code")
	private String currencyCode;
	
	@JsonProperty("price_min_usd")
	private double priceMinUsd;
	
	@JsonProperty("price_max_usd")
	private double priceMaxUsd;
	
	@JsonProperty("stop_types")
	private List<String> stopTypes;
	
	@JsonProperty("airline_code")
	private String airlineCode;
	
	@JsonProperty("search_id")
	private String searchId;
	
	@JsonProperty("token_id")
	private String tokenId;
	
	@JsonProperty("include_m_class")
	private boolean includeMClass;
	
	public Itinerary() {
		
	}

	public List<Trip> getTrips() {
		return trips;
	}

	public void setTrips(List<Trip> trips) {
		this.trips = trips;
	}

	public String getCabin() {
		return cabin;
	}

	public void setCabin(String cabin) {
		this.cabin = cabin;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public int getNumberAdults() {
		return numberAdults;
	}

	public void setNumberAdults(int numberAdults) {
		this.numberAdults = numberAdults;
	}

	public int getNumberChildren() {
		return numberChildren;
	}

	public void setNumberChildren(int numberChildren) {
		this.numberChildren = numberChildren;
	}

	public int getNumberInfants() {
		return numberInfants;
	}

	public void setNumberInfants(int numberInfants) {
		this.numberInfants = numberInfants;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public double getPriceMinUsd() {
		return priceMinUsd;
	}

	public void setPriceMinUsd(double priceMinUsd) {
		this.priceMinUsd = priceMinUsd;
	}

	public double getPriceMaxUsd() {
		return priceMaxUsd;
	}

	public void setPriceMaxUsd(double priceMaxUsd) {
		this.priceMaxUsd = priceMaxUsd;
	}

	public List<String> getStopTypes() {
		return stopTypes;
	}

	public void setStopTypes(List<String> stopTypes) {
		this.stopTypes = stopTypes;
	}

	public String getAirlineCode() {
		return airlineCode;
	}

	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}

	public String getSearchId() {
		return searchId;
	}

	public void setSearchId(String searchId) {
		this.searchId = searchId;
	}

	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

	public boolean isIncludeMClass() {
		return includeMClass;
	}

	public void setIncludeMClass(boolean includeMClass) {
		this.includeMClass = includeMClass;
	}
}
