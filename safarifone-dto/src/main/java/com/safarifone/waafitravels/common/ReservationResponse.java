/***********************************************************************************
 * CopyRight (c) Sep 16, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.safarifone.waafitravels.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ReservationResponse {

	private String status;
	private String message;
	private String booking_id;
	private ResponseTrip outbound;
	private ResponseTrip inbound;
	private int number_adults;
	private int number_children;
	private int number_infants;
	private String currency_code;
	private TravellerInfo traveller_info;
	private PaymentInfo payment_info;
	private String total_price;
	private Map<String, Object> handoff;
	private List<String> rules;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getBooking_id() {
		return booking_id;
	}

	public void setBooking_id(String booking_id) {
		this.booking_id = booking_id;
	}

	public int getNumber_adults() {
		return number_adults;
	}

	public void setNumber_adults(int number_adults) {
		this.number_adults = number_adults;
	}

	public int getNumber_children() {
		return number_children;
	}

	public void setNumber_children(int number_children) {
		this.number_children = number_children;
	}

	public int getNumber_infants() {
		return number_infants;
	}

	public void setNumber_infants(int number_infants) {
		this.number_infants = number_infants;
	}

	public String getCurrency_code() {
		return currency_code;
	}

	public void setCurrency_code(String currency_code) {
		this.currency_code = currency_code;
	}

	public TravellerInfo getTraveller_info() {
		return traveller_info;
	}

	public void setTraveller_info(TravellerInfo traveller_info) {
		this.traveller_info = traveller_info;
		this.number_adults = this.traveller_info.getAdults()!=null ? this.traveller_info.getAdults().size():0;
		this.number_children = this.traveller_info.getChildren() !=null ? this.traveller_info.getChildren().size():0;
		this.number_infants = this.traveller_info.getInfants()!=null ? this.traveller_info.getInfants().size():0;
	}

	public PaymentInfo getPayment_info() {
		return payment_info;
	}

	public void setPayment_info(PaymentInfo payment_info) {
		this.payment_info = payment_info;
	}

	public ResponseTrip getOutbound() {
		return outbound;
	}

	public void setOutbound(ResponseTrip outbound) {
		this.outbound = outbound;
	}

	public ResponseTrip getInbound() {
		return inbound;
	}

	public void setInbound(ResponseTrip inbound) {
		this.inbound = inbound;
	}

	public String getTotal_price() {
		return total_price;
	}

	public void setTotal_price(String total_price) {
		this.total_price = total_price;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Map<String, Object> getHandoff() {
		return handoff;
	}

	public void setHandoff(Map<String, Object> handoff) {
		this.handoff = handoff;
	}
	//added amassillo
	public List<String> getRules() {
		return rules;
	}

	public void setRules(List<String> rules) {
		this.rules = rules;
	}
	
	public void addRule(String pRule) {
		if (this.rules == null){
			this.rules = new ArrayList<String>();
		}
		this.rules.add(pRule);
	}
}
