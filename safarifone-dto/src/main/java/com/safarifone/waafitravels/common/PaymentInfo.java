/***********************************************************************************
 * CopyRight (c) Sep 16, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.safarifone.waafitravels.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.safarifone.waafitravels.customer.Customer;

public class PaymentInfo {

	@JsonProperty("payment_type")
	private String paymentType;
	
	@JsonProperty("payment_name")
	private String paymentName;
	
	@JsonProperty("billing_info")
	private Customer billingInfo;
	
	public PaymentInfo() {
		
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getPaymentName() {
		return paymentName;
	}

	public void setPaymentName(String paymentName) {
		this.paymentName = paymentName;
	}

	public Customer getBillingInfo() {
		return billingInfo;
	}

	public void setBillingInfo(Customer billingInfo) {
		this.billingInfo = billingInfo;
	}

}
