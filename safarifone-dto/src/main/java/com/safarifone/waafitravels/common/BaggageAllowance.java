package com.safarifone.waafitravels.common;


/**
 * 
 * @author amassillo
 *
 */
public class BaggageAllowance {
	private int numberOfPieces;

	public BaggageAllowance() {
		
	}
	
	public BaggageAllowance(int pNofP) {
		this.setNumberOfPieces(pNofP);
	}
	
	public int getNumberOfPieces() {
		return numberOfPieces;
	}

	public void setNumberOfPieces(int numberOfPieces) {
		this.numberOfPieces = numberOfPieces;
	}
}
