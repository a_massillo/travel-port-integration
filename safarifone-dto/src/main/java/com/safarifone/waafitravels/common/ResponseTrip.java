/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.safarifone.waafitravels.common;

import java.util.ArrayList;
import java.util.List;


public class ResponseTrip implements Cloneable{ 
	private long id;
	private String departure_city;
	private String departure_airport;
	private String arrival_city;
	private String arriavel_airport;
	private String departure_date;
	private String arrival_date;
	private String flight_no;
	private String cabin;
	private String price;
	private String tax;
	private String total_price;
	private String airline_name;
	private int stop;
	private String stop_city;
	private String departure_time;
	private String arrival_time;
	private int flight_time;
	private int distance;
	private String currency = "USD";
	private String provider = "GDS";
	private List<ResponseTrip> connections;
	private List<Penalty> penalties;
	//private List<CabinClass> cabinClasses;
	private List<BaggageAllowance> baggage_allowances;
	private List<CarryOnAllowance> carry_on_allowances;
	private List<String> rules;
	//added amassillo
	private String departure_terminal;
	private String arrival_terminal;
	private String equipment;
	private Boolean flightDetailsOn = true;
	
	public ResponseTrip() {
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDeparture_city() {
		return departure_city;
	}

	public void setDeparture_city(String departure_city) {
		this.departure_city = departure_city;
	}

	public String getArrival_city() {
		return arrival_city;
	}

	public void setArrival_city(String arrival_city) {
		this.arrival_city = arrival_city;
	}

	public String getDeparture_date() {
		return departure_date;
	}

	public void setDeparture_date(String departure_date) {
		this.departure_date = departure_date;
	}

	public String getArrival_date() {
		return arrival_date;
	}

	public void setArrival_date(String arrival_date) {
		this.arrival_date = arrival_date;
	}

	public String getFlight_no() {
		return flight_no;
	}

	public void setFlight_no(String flight_no) {
		this.flight_no = flight_no;
	}

	public String getCabin() {
		return cabin;
	}

	public void setCabin(String cabin) {
		this.cabin = cabin;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getAirline_name() {
		return airline_name;
	}

	public void setAirline_name(String airline_name) {
		this.airline_name = airline_name;
	}

	public int getStop() {
		return stop;
	}

	public void setStop(int stop) {
		this.stop = stop;
	}

	public String getTotal_price() {
		return total_price;
	}

	public void setTotal_price(String total_price) {
		this.total_price = total_price;
	}
	
	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public String getStop_city() {
		return stop_city;
	}

	public void setStop_city(String stop_city) {
		this.stop_city = stop_city;
	}

	public String getDeparture_time() {
		return departure_time;
	}

	public void setDeparture_time(String departure_time) {
		this.departure_time = departure_time;
	}

	public String getArrival_time() {
		return arrival_time;
	}

	public void setArrival_time(String arrival_time) {
		this.arrival_time = arrival_time;
	}

	public List<ResponseTrip> getConnections() {
		return connections;
	}

	public void setConnections(List<ResponseTrip> connections) {
		this.connections = connections;
	}
	/**
	 * added amassillo
	 * @param connection
	 */
	public void addConnection(ResponseTrip connection) {
		if (this.connections == null)
			this.connections = new ArrayList<ResponseTrip>();
		else {
			connection.setStop(this.connections.size()); // 1st flight not tagged with connection number
			if (this.connections.get(this.connections.size()-1).getFlight_no().compareTo(connection.getFlight_no()) ==0){
				connection.setFlightDetailsOn(false);
			}
		}
		this.setFlight_time(this.getFlight_time() + connection.getFlight_time());
		this.connections.add(connection);
	}
	
	public List<Penalty> getPenalties() {
		return penalties;
	}

	public void setPenalties(List<Penalty> penalties) {
		this.penalties = penalties;
	}

//	public List<CabinClass> getCabinClasses() {
//		return cabinClasses;
//	}
//
//	public void setCabinClasses(List<CabinClass> cabinClasses) {
//		this.cabinClasses = cabinClasses;
//	}


	public List<String> getRules() {
		return rules;
	}

	public void setRules(List<String> rules) {
		this.rules = rules;
	}

	public List<BaggageAllowance> getBaggage_allowances() {
		return baggage_allowances;
	}

	public void setBaggage_allowances(List<BaggageAllowance> baggage_allowances) {
		this.baggage_allowances = baggage_allowances;
	}

	public List<CarryOnAllowance> getCarry_on_allowances() {
		return carry_on_allowances;
	}

	public void setCarry_on_allowances(List<CarryOnAllowance> carry_on_allowances) {
		this.carry_on_allowances = carry_on_allowances;
	}

	public int getFlight_time() {
		return flight_time;
	}

	public void setFlight_time(int flight_time) {
		this.flight_time = flight_time;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String privider) {
		this.provider = privider;
	}

	public String getDeparture_airport() {
		return departure_airport;
	}

	public void setDeparture_airport(String departure_airport) {
		this.departure_airport = departure_airport;
	}

	public String getArriavel_airport() {
		return arriavel_airport;
	}

	public void setArriavel_airport(String arriavel_airport) {
		this.arriavel_airport = arriavel_airport;
	}

	public String getDeparture_terminal() {
		return departure_terminal;
	}

	public void setDeparture_terminal(String departure_terminal) {
		this.departure_terminal = departure_terminal;
	}

	public String getArrival_terminal() {
		return arrival_terminal;
	}

	public void setArrival_terminal(String arrival_terminal) {
		this.arrival_terminal = arrival_terminal;
	}

	public String getEquipment() {
		return equipment;
	}

	public void setEquipment(String equipment) {
		this.equipment = equipment;
	}

	public Boolean getFlightDetailsOn() {
		return flightDetailsOn;
	}

	public void setFlightDetailsOn(Boolean flightDetailsOn) {
		this.flightDetailsOn = flightDetailsOn;
	}

	public ResponseTrip clone() throws CloneNotSupportedException {
		return (ResponseTrip) super.clone();
	}

	
}
