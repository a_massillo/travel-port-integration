/***********************************************************************************
 * CopyRight (c) Sep 16, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.safarifone.waafitravels.common;

import java.util.List;

import com.safarifone.waafitravels.customer.Customer;

public class TravellerInfo {

	private List<Customer> adults;
	private List<Customer> children;
	private List<Customer> infants;
	
	public TravellerInfo() {
		
	}

	public List<Customer> getAdults() {
		return adults;
	}

	public void setAdults(List<Customer> adults) {
		this.adults = adults;
	}

	public List<Customer> getChildren() {
		return children;
	}

	public void setChildren(List<Customer> children) {
		this.children = children;
	}

	public List<Customer> getInfants() {
		return infants;
	}

	public void setInfants(List<Customer> infants) {
		this.infants = infants;
	}
}
