/***********************************************************************************
 * CopyRight (c) Feb 6, 2017 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.safarifone.waafitravels.common;

import java.util.Map;

public class ServiceResponse {

	public String status;
	public String message;
	public String data;
	public Map<String, Object> handoff;
}
