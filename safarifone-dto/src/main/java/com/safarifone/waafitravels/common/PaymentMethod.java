package com.safarifone.waafitravels.common;

import java.util.List;

public class PaymentMethod {
	private List<PaymentType> paymentTypes;

	public List<PaymentType> getPaymentTypes() {
		return paymentTypes;
	}

	public void setPaymentTypes(List<PaymentType> paymentTypes) {
		this.paymentTypes = paymentTypes;
	}
	
	
	
}
