package com.safarifone.waafitravels.common;

import java.math.BigDecimal;

/**
 * generated test dto
 * @author Alejandra
 *
 */
public class Transaction {

	private BigDecimal amountDue;
	private BigDecimal amount;
	private BigDecimal commission;
	private BigDecimal agentCommission;
	private BigDecimal BillingPhone;
	private String pinNumber;
	private String paymentMethod;
	private String pgStatus;
	private String pgTxId;
	private String reservationId;
	private String airlineName;
	private String txType;
	public BigDecimal getAmountDue() {
		return amountDue;
	}
	public void setAmountDue(BigDecimal amountDue) {
		this.amountDue = amountDue;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getCommission() {
		return commission;
	}
	public void setCommission(BigDecimal commission) {
		this.commission = commission;
	}
	public BigDecimal getAgentCommission() {
		return agentCommission;
	}
	public void setAgentCommission(BigDecimal agentCommission) {
		this.agentCommission = agentCommission;
	}
	public BigDecimal getBillingPhone() {
		return BillingPhone;
	}
	public void setBillingPhone(BigDecimal billingPhone) {
		BillingPhone = billingPhone;
	}
	public String getPinNumber() {
		return pinNumber;
	}
	public void setPinNumber(String pinNumber) {
		this.pinNumber = pinNumber;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getPgStatus() {
		return pgStatus;
	}
	public void setPgStatus(String pgStatus) {
		this.pgStatus = pgStatus;
	}
	public String getPgTxId() {
		return pgTxId;
	}
	public void setPgTxId(String pgTxId) {
		this.pgTxId = pgTxId;
	}
	public String getReservationId() {
		return reservationId;
	}
	public void setReservationId(String reservationId) {
		this.reservationId = reservationId;
	}
	public String getAirlineName() {
		return airlineName;
	}
	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}
	public String getTxType() {
		return txType;
	}
	public void setTxType(String txType) {
		this.txType = txType;
	}
	
	
	
	
}
