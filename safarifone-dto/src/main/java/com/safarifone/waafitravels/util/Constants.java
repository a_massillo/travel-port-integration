package com.safarifone.waafitravels.util;

public class Constants {
	public static final int AIRLINE_DETERMINED_COMMISSION = 0;
	public static final String DEFAULT_COMMISSION = null;
	public static final String DATE_FORMAT ="MM/dd/yyyy";
	public static final String ERROR_VRS_APP_IP_MISSING_FROM_AGENT_TABLE="";
	public static final String ERROR_VRS_INVALID_IP="";
	public static final String ERROR_VRS_INVALID_TOKEN="";
	public static final String ERROR_VRS_NO_IP_CONFIGURED_FOR_AGENT=""; 
	public static final String ERROR_VRS_NO_TOKEN="";
	public static final String ERROR_VRS_NOT_HTTPS="";
	public static final String ERROR_VRS_INVALID_AGENT_SINE="";
	public static final String CACHE_CABINS_KEY="";
	public static final String NONGDS_DEFAULT_CABIN="";
	public static final String SEARCH_DATE_FORMAT = null;
	public static final long LOGIN_TIMEOUT = 0;
	public static final Throwable ERROR_PAYMENT_LOGIN_REQUEST_ERROR = null;

}
