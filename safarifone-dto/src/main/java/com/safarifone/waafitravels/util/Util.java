/***********************************************************************************
 * CopyRight (c) Aug 28, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.safarifone.waafitravels.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
//import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Future;
import javax.naming.NamingException;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Years;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.safarifone.waafitravels.customer.Customer;
import com.safarifone.waafitravels.common.Itinerary;
import com.safarifone.waafitravels.common.PaymentInfo;
import com.safarifone.waafitravels.common.PaymentMethod;
import com.safarifone.waafitravels.common.PaymentType;
import com.safarifone.waafitravels.common.ReservationRequest;
import com.safarifone.waafitravels.common.ServiceResponse;
import com.safarifone.waafitravels.common.Trip;
import com.safarifone.waafitravels.common.WaafiTravelsException;

public class Util {

	public static String createUnknownError() {
		return "Unknow error occurred";
	}
	
	public static String createErrorResponse(String message) {
		ServiceResponse response = new ServiceResponse();
	
		String error = "";
		
		return error;
	}
	
	public static String createCCPaymentErrorResponse(String message, int errorCode) {
	
		
		return message;
	}
	
	public static String createCCPaymentSuccessResponse(String message) {
	
		return message;
	}
	
	public static String validItinerary(Itinerary itinerary) {
		
		String error = null;
		Map<String,Object> errorMap = new HashMap<String,Object>();

		if(itinerary.getTrips().size() <= 0) {
			errorMap.put("trips", "Trips is empty. Must provide trip.");
		}
		else {
			
			// Check trips
			int index = 0;
			for(Trip trip : itinerary.getTrips()) {
				if(trip.getArrival_city() == null || trip.getArrival_city().length() <= 0) {
					errorMap.put("trips["+index+"].arrival_city","City is empty. Must provide city.");
				}
				
				if(trip.getDeparture_city() == null || trip.getDeparture_city().length() <= 0) {
					errorMap.put("trips["+index+"].departure_city","City is empty. Must provide city.");
				}
				
				if(trip.getDeparture_city() != null && trip.getDeparture_city().equalsIgnoreCase(trip.getArrival_city())) {
					errorMap.put("trips["+index+"].departure_city","Departure city must not be same as arrival city");
				}
				
				
				if(trip.getDeparture_date() == null || trip.getDeparture_date().length() <= 0) {
					errorMap.put("trips["+index+"].departure_date","Date is empty. Must provide date.");
				}
				else {
					if(!isValidDate(trip.getDeparture_date(), Constants.DATE_FORMAT)) {
						errorMap.put("trips["+index+"].departure_date","Invalid date format. Valid format:MM#dd#yyy. e.g.08#30#2016");
					}
				}
				
//				if(trip.getArrival_date() == null || trip.getArrival_date().length() <= 0) {
//					errorMap.put("trips["+index+"].arrival_date","Date is empty. Must provide date.");
//				}
//				else {
//					if(!isValidDate(trip.getArrival_date(), Constants.DATE_FORMAT)) {
//						errorMap.put("trips["+index+"].arrival_date","Invalid date format. Valid format:MM#dd#yyy. e.g.08#30#2016");
//					}
//					
//				}
				
				if(trip.getReturn_date() != null && !isValidDate(trip.getReturn_date(), Constants.DATE_FORMAT)) {
					errorMap.put("trips["+index+"].return_date","Invalid date format. Valid format:MM#dd#yyy. e.g.08#30#2016");
				}
				else if(trip.getDeparture_date() != null && trip.getReturn_date() != null) {
					// Check return date vs departure date
					if(compareDates(trip.getReturn_date(), trip.getDeparture_date()) <= 0) {
						errorMap.put("trips["+index+"].return_date","Must be after departure date.");
					}
				}
				
				// Check arrival date
//				if(trip.getDeparture_date() != null && trip.getArrival_date() != null) {
//					if(compareDates(trip.getArrival_date(), trip.getDeparture_date()) < 0) {
//						errorMap.put("trips["+index+"].arrival_date","Must not be before departure date.");
//					}
//				}

				// Check return date vs arrival date
//				if(trip.getArrival_date() != null && trip.getReturn_date() != null) {
//					if(compareDates(trip.getReturn_date(), trip.getArrival_date()) <= 0) {
//						errorMap.put("trips["+index+"].return_date","Must be after arrival date.");
//					}
//				}
			}
			
			// Check others
			if(itinerary.getNumberAdults() <= 0) {
				errorMap.put("number_adults", "Number of adults must greater than 0.");
			}
		}
		
//		if(!errorMap.isEmpty()) {
//			ObjectMapper mapper = new ObjectMapper();
//			try {
//				errorMap.put("message", "Invalid request. Please correct errors and try again.");
//				errorMap.put("status", Constants.REQUEST_STATUS.failed.name());
//				error = mapper.writeValueAsString(errorMap);
//			} catch (JsonProcessingException e) {
//				e.printStackTrace();
//				error = e.getMessage();
//			}
//		}

		return error;
	}
	
	/**
	 * Return value is 0 if both dates are equal.
	 * Return value is greater than 0 , if Date is after the date argument.
	 * Return value is less than 0, if Date is before the date argument.
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static int compareDates(String date1, String date2) {
    	try {

    		SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
        	Date date1Obj = sdf.parse(date1);
        	Date date2Obj = sdf.parse(date2);
        	
        	return date1Obj.compareTo(date2Obj);

    	} catch(ParseException ex){
    		ex.printStackTrace();
    	}
    	
    	return -1;
	}
	
	public static String validReservationRequest(ReservationRequest resRequest) {
		String error = null;
		Map<String,Object> errorMap = new HashMap<String,Object>();
		
		if(resRequest.getSearchId() == null || resRequest.getSearchId().length() <= 0) {
			errorMap.put("search_id", "Search ID must not be empty.");
		}
		
		if(resRequest.getOutboundTripId() <= 0) {
			errorMap.put("outbound_trip_id", "Trip ID must not be 0.");
		}
		
		if(resRequest.getPaymentInfo() == null) {
			errorMap.put("payment_info", "Payment info must not be empty.");
		}
		else {
			if(resRequest.getPaymentInfo().getBillingInfo() == null) {
				errorMap.put("payment_info.billing_info", "Billing info must not be empty.");
			}
			else {
				if(resRequest.getPaymentInfo().getBillingInfo().getEmail() == null) {
					errorMap.put("payment_info.billing_info.email_address", "Email address must not be empty.");
				}
				
				if(resRequest.getPaymentInfo().getBillingInfo().getPhone() == null) {
					errorMap.put("payment_info.billing_info.phone_number", "Phone number must not be empty.");
				}
				
				if(resRequest.getPaymentInfo().getPaymentName() == null) {
					errorMap.put("payment_info.payment_name", "Payment name must not be empty.");
				}
				else {
					String paymentName = resRequest.getPaymentInfo().getPaymentName();
					if(!isValidPaymentMethod(resRequest.getPaymentInfo())) {
						errorMap.put("payment_info.payment_name","Invalid payment name:"+paymentName);
					}
					else if("mwallet_account".equalsIgnoreCase(resRequest.getPaymentInfo().getPaymentType()) 
							|| "debit_card".equalsIgnoreCase(resRequest.getPaymentInfo().getPaymentType())) {
						if(resRequest.getPaymentInfo().getBillingInfo().getPinNumber() == null 
								|| resRequest.getPaymentInfo().getBillingInfo().getPinNumber().length() <= 0)
							errorMap.put("payment_info.billing_info.pin_number", "Pin number must not be empty.");
					}
				}
			}
		}
		
		if(resRequest.getTravellerInfo() == null) {
			errorMap.put("traveller_info", "Traveller info must not be empty.");
		}
		
		if(resRequest.getTravellerInfo() != null) {
			// Adults
			List<Customer> adults = resRequest.getTravellerInfo().getAdults();
			if(adults != null) {
				int i = 0;
				for(Customer adult : adults) {
					if(adult.getFirstName() == null || adult.getFirstName().length() <= 0) {
						errorMap.put("traveller_info.adults["+i+"].fist_name", "Must not be empty.");
					}
					
					if(adult.getLastName() == null || adult.getLastName().length() <= 0) {
						errorMap.put("traveller_info.adults["+i+"].last_name", "Must not be empty.");
					}
					
//					if(adult.getEmail_address() == null || adult.getEmail_address().length() <= 0) {
//						errorMap.put("traveller_info.adults["+i+"].email_address", "Must not be empty.");
//					}
					
					if(adult.getGender() == null || adult.getGender().length() <= 0) {
						errorMap.put("traveller_info.adults["+i+"].gender", "Must not be empty.");
					}
					
					if(adult.getPhone() == null || adult.getPhone().length() <= 0) {
						errorMap.put("traveller_info.adults["+i+"].phone_number", "Must not be empty.");
					}
					
					/*if(adult.getPassport_number() == null || adult.getPassport_number().length() <= 0) {
						errorMap.put("traveller_info.adults["+i+"].passport_number", "Must not be empty.");
					}
					
					if(adult.getAddress_line1() == null || adult.getAddress_line1().length() <= 0) {
						errorMap.put("traveller_info.adults["+i+"].address_line1", "Must not be empty.");
					}
					
					if(adult.getCountry() == null || adult.getCountry().length() <= 0) {
						errorMap.put("traveller_info.adults["+i+"].country", "Must not be empty.");
					}*/
					
					if(adult.getDateOfBirth() == null || adult.getDateOfBirth().length() <= 0) {
						errorMap.put("traveller_info.adults["+i+"].date_of_birth", "Must not be empty.");
					}
					else {
						if(!validAdultDOB(adult.getDateOfBirth())) {
							errorMap.put("traveller_info.adults["+i+"].date_of_birth", "Not valid adult date of birth:"+adult.getDateOfBirth());
						}
					}
				}
			}
			else {
				errorMap.put("traveller_info.adults", "Adults must not be empty.");
			}
			
			// Children
			List<Customer> children = resRequest.getTravellerInfo().getChildren();
			if(children != null) {
				int i = 0;
				
				for(Customer child : children) {
					if(child.getFirstName() == null || child.getFirstName().length() <= 0) {
						errorMap.put("traveller_info.children["+i+"].fist_name", "Must not be empty.");
					}
					
					if(child.getLastName() == null || child.getLastName().length() <= 0) {
						errorMap.put("traveller_info.children["+i+"].last_name", "Must not be empty.");
					}
					
					if(child.getGender() == null || child.getGender().length() <= 0) {
						errorMap.put("traveller_info.children["+i+"].gender", "Must not be empty.");
					}
					
					if(child.getDateOfBirth() == null || child.getDateOfBirth().length() <= 0) {
						errorMap.put("traveller_info.children["+i+"].date_of_birth", "Must not be empty.");
					}
					else {
						if(!validChildDOB(child.getDateOfBirth())) {
							errorMap.put("traveller_info.children["+i+"].date_of_birth", "Not valid child date of birth:"+child.getDateOfBirth());
						}
					}
				}
			}
			
			// Infants
			List<Customer> infants = resRequest.getTravellerInfo().getInfants();
			if(infants != null) {
				int i = 0;
				
				for(Customer infant : infants) {
					if(infant.getFirstName() == null || infant.getFirstName().length() <= 0) {
						errorMap.put("traveller_info.infants["+i+"].fist_name", "Must not be empty.");
					}
					
					if(infant.getLastName() == null || infant.getLastName().length() <= 0) {
						errorMap.put("traveller_info.infants["+i+"].last_name", "Must not be empty.");
					}
					
					if(infant.getGender() == null || infant.getGender().length() <= 0) {
						errorMap.put("traveller_info.infants["+i+"].gender", "Must not be empty.");
					}
					
					if(infant.getDateOfBirth() == null || infant.getDateOfBirth().length() <= 0) {
						errorMap.put("traveller_info.infants["+i+"].date_of_birth", "Must not be empty.");
					}
					else {
						if(!validInfantDOB(infant.getDateOfBirth())) {
							errorMap.put("traveller_info.infants["+i+"].date_of_birth", "Not valid infant date of birth:"+infant.getDateOfBirth());
						}
					}
				}
			}
		}
		else {
			errorMap.put("traveller_info", "Traveller info must not be empty.");
		}
		
		if(!errorMap.isEmpty()) {

				error = "Invalid request. Please correct errors and try again.";
		}
		
		return error;
	}
	
	private static boolean isValidPaymentMethod(PaymentInfo paymentInfo) {
//		if(paymentInfo == null) return false;
//		
//		@SuppressWarnings("unchecked")
//		List<PaymentMethod> paymentMethods = (List<PaymentMethod>) StartupManager.getInstance().getCache().get(Constants.CACHE_PAYMENTMETHODS_KEY);
//		
//		if(paymentMethods == null|| paymentMethods.size() <= 0) return false;
//		
//		for(PaymentMethod paymentMethod : paymentMethods) {
//			if(paymentMethod == null) continue;
//			
//			for(PaymentType paymentType : paymentMethod.getPaymentTypes()) {
//				if(paymentType == null) continue;
//				
//				if(paymentInfo.getPaymentName().equalsIgnoreCase(paymentType.getPaymentName())) {
//					paymentInfo.setPaymentType(paymentType.getPaymentType());
//					return true;
//				}
//			}
//		}
		return true;
	}

	private static boolean validAdultDOB(String date_of_birth) {
//		Date dateObj = strToDate(date_of_birth, Constants.DATE_FORMAT);
//		
//		if(dateObj == null) return false;
//		
//		LocalDate birthdate = new LocalDate (dateObj);
//		LocalDate now = new LocalDate();
//		Days age = Days.daysBetween(birthdate, now);
//		
//		if(age.getDays() > Constants.ADULT_AGE) {
//			return true;
//		}
		return true;
	}
	
	private static boolean validChildDOB(String date_of_birth) {
//		Date dateObj = strToDate(date_of_birth, Constants.DATE_FORMAT);
//		
//		if(dateObj == null) return false;
//		
//		LocalDate birthdate = new LocalDate (dateObj);
//		LocalDate now = new LocalDate();
//		Days age = Days.daysBetween(birthdate, now);
//		
//		// 2-12
//		if(age.getDays() >= Constants.CHILD_LOWER_BOUND_AGE && age.getDays() <= Constants.CHILD_UPPER_BOUND_AGE) {
//			return true;
//		}
		return true;
	}
	
	private static boolean validInfantDOB(String date_of_birth) {
//		Date dateObj = strToDate(date_of_birth, Constants.DATE_FORMAT);
//		
//		if(dateObj == null) return false;
//		
//		LocalDate birthdate = new LocalDate (dateObj);
//		LocalDate now = new LocalDate();
//		Days age = Days.daysBetween(birthdate, now);
//		
//		//0-23 months
//		if(age.getDays() >= 0 && age.getDays() < Constants.INFANT_UPPER_BOUND_AGE) {
//			return true;
//		}
		return true;
	}
	
	
//	/**
//	 * Ticket expires midnight of the issued day
//	 * @param timestampObj
//	 * @return
//	 */
//	public static boolean isCancellationTimeExpired(DateTime timestampObj) {
//				
//		LocalDate ticketdate = timestampObj.toLocalDate();
//
//		String expiresStr    = ticketdate.toString()+" 23:59:59.999";
//
//		DateTime expires = toUTC(expiresStr);
//
//		DateTime now = new DateTime(DateTimeZone.UTC);
//		expires = expires.plusMinutes(Constants.NONGDS_RESERVATION_CANCELLATION_PERIOD);
//		
//		// We are past midnight
//		if(now.isAfter(expires)) {
//			return true;
//		}
//
//		
//		return false;
//	}
	
//	public static boolean isBeforeToday(String dateStr) {
//		if(dateStr == null || dateStr.length() <= 0) return false;
//
//		DateTime givenDate = toUTC(dateStr);
//		
//		DateTime now = new DateTime(DateTimeZone.UTC);
//		
//		if(givenDate.isBefore(now)) {
//			return true;
//		}
//		
//		return false;
//	}
	
	public static boolean isWithin24Hrs(String depDateStr) {
//		if(depDateStr == null || depDateStr.length() <= 0) return false;
//
//		DateTime givenDate  = toUTC(depDateStr);
//		
//		DateTime now        = new DateTime(DateTimeZone.UTC);
//		
//		long nowMillis      =  now.getMillis();
//		long depMillis      = givenDate.getMillis();
//		long diff           = depMillis - nowMillis;
//		long millisFor24hrs = 86400000;
//		
//		if(diff < millisFor24hrs) {
//			return true;
//		}
		
		return false;
	}
	
//	public static String getDatePart(LocalDate date) {
//		if(date == null) return null;
//		
//		String dateStr = date.toString();
//		
//		String dateParts[] = dateStr.split(" ");
//		String datePart = dateParts[0];
//		
//		return datePart;
//	}

//	public static boolean validSession(String sessionId) {
//		
//		if(sessionId == null || sessionId.length() <= 0) return false;
//
//		Object cachedClient        =  StartupManager.getInstance().getCache().get(sessionId);
//		
//		if(cachedClient == null) {
//			return false;
//		}
//		WaafiTravelsClient sClient = (WaafiTravelsClient) cachedClient;
//	
//		// Check session
//		if(sClient.getSessionId().equals(sessionId) == false) {
//			return false;
//		}
//		
//		return true;
//	}
	
//	public static boolean validAdminSession(HttpSession httpSession, String sessionId) {
//
//		Object sAdminObj         = StartupManager.getInstance().getCache().get(sessionId);
//		
//		if(sAdminObj == null) {
//			return false;
//		}
//		
//		WaafiTravelsAdmin sAdmin = (WaafiTravelsAdmin) sAdminObj;
//	
//		// Check session
//		if(sAdmin.getSessionId().equals(sessionId) == false) {
//			return false;
//		}
//		
//		return true;
//	}

	public static String getShortMonthName(String date) {
		
		if(date == null || date.length() <= 0) {
			return null;
		}
		
		Date dateObj = strToDate(date, Constants.DATE_FORMAT);
		
		if(dateObj == null) return null;
		
		Format formatter = new SimpleDateFormat("MMM");
		String shortName = formatter.format(dateObj);
		
		if(shortName != null) shortName = shortName.toUpperCase();
	    
		return shortName;
	}
	
	public static String getShortMonthName(String date, String format) {
		
		if(date == null || date.length() <= 0) {
			return null;
		}
		
		Date dateObj = strToDate(date, format);
		
		if(dateObj == null) return null;
		
		Format formatter = new SimpleDateFormat("MMM");
		String shortName = formatter.format(dateObj);
		
		if(shortName != null) shortName = shortName.toUpperCase();
	    
		return shortName;
	}
	
	public static Date strToDate(String date, String format){

		if(date == null || date.length() <= 0 || format == null || format.length() <= 0){
			return null;
		}

		SimpleDateFormat sdf = new SimpleDateFormat(format);
		sdf.setLenient(false);
		Date dateObj = null;
		
		try {

			//if not valid, it will throw ParseException
			dateObj = sdf.parse(date);

		} catch (ParseException e) {

			//e.printStackTrace();
			return null;
		}

		return dateObj;
	}

	public static boolean isValidDate(String date, String format){

		if(date == null || date.length() <= 0 || format == null || format.length() <= 0){
			return false;
		}

		SimpleDateFormat sdf = new SimpleDateFormat(format);
		sdf.setLenient(false);

		try {

			//if not valid, it will throw ParseException
			sdf.parse(date);
			//System.out.println(dateObj);

		} catch (ParseException e) {

			//e.printStackTrace();
			return false;
		}

		return true;
	}

	public static String createSuccess(String message) {
	
		
		return message;
	}
	
	public static String createResponse(String key, Object value) {
		Map<String, Object> response = new HashMap<String, Object>();
		String jsonSuccess = "";
		
//		response.put(key, value);
//		response.put("status", Constants.REQUEST_STATUS.success.name());
//		response.put("message", "");
//		
//		try {
//			ObjectMapper mapper = new ObjectMapper();
//			jsonSuccess = mapper.writeValueAsString(response);
//		} catch (JsonProcessingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		return jsonSuccess;
	}
	
	public static String createError(String error, int errorCode) {
		
		
		return error;
	}
	
//	public static String createError(WaafiTravelsError wtError) {
//		String error = "";
//		
//		try {
//			ObjectMapper mapper = new ObjectMapper();
//			error = mapper.writeValueAsString(wtError);
//		} catch (JsonProcessingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		return error;
//	}

//	public static Airline findAirline(List<Airline> airlines, String code) {
//		if(airlines == null || airlines.size() <= 0 || code == null || code.length() <= 0) return null;
//		
//		for(Airline airline : airlines) {
//			if(code.equalsIgnoreCase(airline.getCode())) {
//				return airline;
//			}
//		}
//		
//		return null;
//	}
	
	public static String getAirlineNameByCode(String code) {
		return code;
	}

	public static String getAirportByCity(String city) {
		return city;
	}
	
	public static String getCityByAirportCode(String airportCode) {	
		return airportCode;
	}

	public static String getDepartureDay(String date) {
		String[] dateParts = date.split("#");
		
		if(dateParts.length > 1) return dateParts[1];
		
		return null;
	}
	
	public static String getDepartureDay(String date, String sep) {
		String[] dateParts = date.split(sep);
		
		if(dateParts.length > 1) return dateParts[2];
		
		return null;
	}
	
	public static String getDepartureDayForReport(String date, String sep) {
		String[] dateParts = date.split(sep);
		
		if(dateParts.length > 1) return dateParts[1];
		
		return null;
	}

	public static String getCityCode(String city) {
		
		
		return city;
	}
	
	public static String fixFlightAvail(String xml) {
		String newXml         = null;
		XMLEventWriter writer = null;
		
		try {
			XMLInputFactory inFactory = XMLInputFactory.newInstance();
			XMLEventReader eventReader = inFactory.createXMLEventReader(new ByteArrayInputStream(xml.getBytes()));
			XMLOutputFactory factory = XMLOutputFactory.newInstance();

			ByteArrayOutputStream output = new ByteArrayOutputStream();

			writer = factory.createXMLEventWriter(output);
			XMLEventFactory eventFactory = XMLEventFactory.newInstance();
			while (eventReader.hasNext()) {
				XMLEvent event = eventReader.nextEvent();
				writer.add(event);
				if (event.getEventType() == XMLEvent.END_ELEMENT) {
					if (event.asEndElement().getName().toString().equalsIgnoreCase("finf")) {
						writer.add(eventFactory.createEndElement("", null, "fltav"));
						writer.add(eventFactory.createStartElement("", null, "fltav"));
					}
				}
			}
			
			byte[] newXmlArray = output.toByteArray();
			newXml = new String(newXmlArray);
		} catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
				if(writer != null) writer.close();
			} catch (XMLStreamException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
		}
		
		return newXml;
	}

	public static String readJsonInput(BufferedReader br) {
		StringBuffer buf = new StringBuffer();
		String line;
		
		try {
			while((line = br.readLine()) != null) {
				buf.append(line);
			}
			if(buf.length() > 0) {
				return buf.toString();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static boolean nonGDSReponseError(String responseXml) {
		if(responseXml.endsWith(""+Constants.ERROR_VRS_APP_IP_MISSING_FROM_AGENT_TABLE)
				|| responseXml.endsWith(""+Constants.ERROR_VRS_INVALID_IP)
				|| responseXml.endsWith(""+Constants.ERROR_VRS_INVALID_TOKEN)
				|| responseXml.endsWith(""+Constants.ERROR_VRS_NO_IP_CONFIGURED_FOR_AGENT)
				|| responseXml.endsWith(""+Constants.ERROR_VRS_NO_TOKEN)
				|| responseXml.endsWith(""+Constants.ERROR_VRS_NOT_HTTPS)
				|| responseXml.endsWith(""+Constants.ERROR_VRS_INVALID_AGENT_SINE)
				|| responseXml.startsWith("Error")) {
			return true;
		}
		return false;
	}

//	public static String validClientLogin(WaafiTravelsClient wtClient) {
//
//		@SuppressWarnings({ "unchecked" })
//		List<WaafiTravelsClient> clients = (List<WaafiTravelsClient>) StartupManager.getInstance().getCache().get(Constants.CACHE_CLIENTS_KEY);
//		
//		if(clients == null || clients.size() <= 0) {
//			return "Unable to retrieve clients.";
//		}
//		
//		for(WaafiTravelsClient client : clients) {
//
//			if(client.getClient_type().equals(wtClient.getClient_type()) == false) {
//				
//				if(client.getUsername().equals(wtClient.getUsername()) == false
//						|| client.getPassword().equals(wtClient.getPassword()) == false) {
//					return "Invalid client username or password.";
//				}
//				else {
//					return null;
//				}
//				
////				if(client.getIp().equals(wtClient.getIp()) == false) {
////					return "Invalid client IP address.";
////				}
//				
//			}
//		}
//		
//		return "Unkown client type.";
//	}
//	
//	public static WaafiTravelsClient getClientByType(String clientType) {
//
//		@SuppressWarnings({ "unchecked" })
//		List<WaafiTravelsClient> clients = (List<WaafiTravelsClient>) StartupManager.getInstance().getCache().get(Constants.CACHE_CLIENTS_KEY);
//
//		if(clients == null || clients.size() <= 0) {
//			return null;
//		}
//
//		for(WaafiTravelsClient client : clients) {
//
//			if(client.getClient_type().equalsIgnoreCase(clientType) == true) {
//
//				return client;
//
//			}
//		}
//
//		return null;
//	}
//	
//	public static String createClientLogin(String clientType) throws WaafiTravelsException {
//		WaafiTravelsClient wtClient = getClientByType(clientType);
//		
//		if(wtClient == null) {
//			throw new WaafiTravelsException("Unable to find client type:"+clientType);
//		}
//		
//		String tokenId    = UUID.randomUUID().toString();
//        wtClient.setAccessTime(new Date());
//        wtClient.setSessionId(tokenId);
//
//        saveClientSession(wtClient);
//
//        StartupManager.getInstance().getCache().put(tokenId, wtClient);
//        
//        return tokenId;
//	}
//	
//	public static String getClientType(String tokenId) {
//
//		if(tokenId == null || tokenId.length() <= 0) return null;
//
//		Object cachedClient        =  StartupManager.getInstance().getCache().get(tokenId);
//		
//		if(cachedClient == null) {
//			return null;
//		}
//		
//		WaafiTravelsClient sClient = (WaafiTravelsClient) cachedClient;
//		
//		
//		return sClient.getClient_type();
//	}
//	
//	public static WaafiTravelsAdmin validAdminLogin(String username, String password) {
//
//		@SuppressWarnings({ "unchecked" })
//		List<WaafiTravelsAdmin> admins = (List<WaafiTravelsAdmin>) StartupManager.getInstance().getCache().get(Constants.CACHE_ADMINS_KEY);
//		
//		if(admins == null || admins.size() <= 0) {
//			return null;
//		}
//		
//		for(WaafiTravelsAdmin admin : admins) {
//
//			if(admin.getUsername().equals(username) == true
//					&& admin.getPassword().equals(password) == true) {
//				return admin;
//			}
//
//		}
//		
//		return null;
//	}

	public static String getNonGDSCabin(String givenCabin) {
		if(givenCabin == null || givenCabin.length() <= 0) {
			return Constants.NONGDS_DEFAULT_CABIN;
		}
		
		givenCabin   = givenCabin.trim();
		String cabin = givenCabin;
		String code  = null;
		
		// e.g. 'Y Economy'
		if(givenCabin.contains(" ")) {
			String[] cabinParts = givenCabin.split(" ");
			cabin = cabinParts[1].trim();
			code  = cabinParts[0].trim();
			
			return code.toUpperCase();
		}
		
//		@SuppressWarnings("unchecked")
//		List<Cabin> cabins = (List<Cabin>) StartupManager.getInstance().getCache().get(Constants.CACHE_CABINS_KEY);
//		if(cabins == null || cabins.size() <= 0) {
//			return null;
//		}
//		
		String cabinCodes = "";
		String comma      = "";
//		for(Cabin cab : cabins) {
//			if(cab.getCabinName().equalsIgnoreCase(cabin)) {
//				cabinCodes = cabinCodes+comma+cab.getCabinCode();
//				comma      = ",";
//			}
//		}
//		
		if(cabinCodes.length() <= 0) {
			return Constants.NONGDS_DEFAULT_CABIN;
		}
		return cabinCodes;
	}
	
	
	public static String getCabinNameByCode(String code) {

	

		return code;
	}
	
	public static String getCabinName(String name) {
	

		return name;
	}
	
	public static String translateItinStatusCode(String code) {

		
		return code;
	}

	public static String getOrder(String order) {
		String newSort = (order == null || order.length() <= 0 ? "ASC" : order);
		if(newSort.equalsIgnoreCase("ASC") == false && newSort.equalsIgnoreCase("DESC")) {
			newSort = "ASC";
		}
		return newSort.toUpperCase();
	}

	public static String getSort(String sort) {
		String newSort = (sort == null || sort.length() <= 0 ? "price" : sort);
		if(newSort.equalsIgnoreCase("price") == false && newSort.equalsIgnoreCase("duration") == false) {
			newSort = "price";
		}
		return newSort.toLowerCase();
	}

	public static String getNonDGDSPaxPlaceHolders(int numAdults, int numChildren, int numInfants) {
		String pax = "";
		
		char[] paxLetters = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 
								'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 
								'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
		int paxCount = 0;
		
		// Adults
		for(; paxCount < numAdults; paxCount++ ) {
			pax += "/"+paxLetters[paxCount]+"MR";
		}
		
		// Children
		for(; paxCount < (numAdults+numChildren); paxCount++ ) {
			pax += "/"+paxLetters[paxCount]+"MR.CH10";
		}
		
		// Infants
		for(; paxCount < (numAdults+numChildren+numInfants); paxCount++ ) {
			pax += "/"+paxLetters[paxCount]+"MR.IN06";
		}
		
		if(pax.length() > 0) {
			pax = "-"+(numAdults+numChildren+numInfants)+"Pax"+pax;
		}
		
		return pax;
	}
	
	public static String getNonGDSPaxNames(ReservationRequest request) {
		String paxNames = "";
		
		List<Customer> adults = request.getTravellerInfo().getAdults();
		int paxCount          = 0;
		String adultsPax      = "";
		String childrenPax    = "";
		String infantsPax     = "";
		
		for(Customer adult : adults) {
			String title = "";
			String gender = adult.getGender();
			paxCount++;
			
//			if(gender != null) {
//				switch(gender.toUpperCase()) {
//				case "MALE":
//					title = "MR";
//					break;
//				case "FEMALE":
//					title = "MRS";
//					break;
//					default:
//						title = "MR";
//						break;
//				}
//			}
			
			String firstName = adult.getFirstName();
			String lastName  = adult.getLastName();
			
			adultsPax += lastName+"/"+firstName+title;
		}
		
		List<Customer> children = request.getTravellerInfo().getChildren();
		if(children != null) {
			for(Customer child : children) {
				String title = "";
				String gender = child.getGender();
				paxCount++;
//
//				if(gender != null) {
//					switch(gender.toUpperCase()) {
//					case "MALE":
//						title = "Mstr";
//						break;
//					case "FEMALE":
//						title = "Ms";
//						break;
//					default:
//						title = "Mstr";
//						break;
//					}
//				}

				String firstName = child.getFirstName();

				int age = getAge(child.getDateOfBirth());

				childrenPax += "/"+firstName+title+".CH"+(age < 10 ? "0"+age : age);
			}
		}
		
		List<Customer> infants = request.getTravellerInfo().getInfants();
		if(infants != null) {
			for(Customer infant : infants) {
				String title = "";
				String gender = infant.getGender();
				paxCount++;

//				if(gender != null) {
//					switch(gender.toUpperCase()) {
//					case "MALE":
//						title = "Mstr";
//						break;
//					case "FEMALE":
//						title = "Ms";
//						break;
//					default:
//						title = "Mstr";
//						break;
//					}
//				}

				String firstName = infant.getFirstName();

				int age = getAge(infant.getDateOfBirth());
				infantsPax += "/"+firstName+title+".IN0"+age;
			}
		}
		
		paxNames = "-"+paxCount+adultsPax+childrenPax+infantsPax;
		
		return paxNames;
	}

	private static int getAge(String date_of_birth) {
		Date dateObj = strToDate(date_of_birth, Constants.DATE_FORMAT);
		
		if(dateObj == null) return 0;
		
		LocalDate birthdate = new LocalDate (dateObj);
		LocalDate now = new LocalDate();
		Years age = Years.yearsBetween(birthdate, now);
		return age.getYears();
	}

	public static String dateToDateTime(String date) {
		Date dateObj = strToDate(date, Constants.DATE_FORMAT);
		
		DateTime dt = new DateTime(dateObj);
		return dt.toString();
	}

	public static boolean isRoundTrip(Itinerary reqItinerary) {
		List<Trip> trips = reqItinerary.getTrips();
		if(trips != null) {
			for(Trip trip : trips) {
				if(trip.getReturn_date() != null && trip.getReturn_date().length() > 0) {
					return true;
				}
			}
		}
		return false;
	}

	public static String buildTripSegment(String date, String depAirport, 
			String arrAirport, String flightNo, String AirlineId, String cabinCode) {
		
		// Build: 0FlightNumberDayMonthDepAirportArrAirport
		// e.g. 0P00204Y20MAYDXBKIH
		
		String month = getShortMonthName(date, Constants.SEARCH_DATE_FORMAT);
		String day   = getDepartureDay(date, "-");
		
		String segment = "0"+AirlineId+"0"+flightNo+cabinCode+day+month+depAirport+arrAirport;
		return segment;
	}
	
	public static String buildUpdateTripSegment(String date, String depAirport, String arrAirport) {
		
		// Build: ADayMonthDepAirportArrAirport
		// e.g. A10DECDXBMGQ
		
		String month = getShortMonthName(date, Constants.SEARCH_DATE_FORMAT);
		String day   = getDepartureDay(date, "-");
		
		String segment = "A"+day+month+depAirport+arrAirport;
		return segment;
	}

	public static BigDecimal calculateFinalTicketPrice(BigDecimal price, String airlineCode) {

		int commissionType = getAirlineCommissionType(airlineCode);
		
		if(commissionType == Constants.AIRLINE_DETERMINED_COMMISSION) {
			return price;
		}

		price = price.add(calculateCommisssion(price, airlineCode));

		return price;
	}
	
	public static BigDecimal calculateAirlinePortion(BigDecimal price, String airlineCode) {

		int commissionType = getAirlineCommissionType(airlineCode);
		
		if(commissionType == Constants.AIRLINE_DETERMINED_COMMISSION) {
			price = price.subtract(calculateCommisssion(price, airlineCode));
		}

		return price;
	}

	public static BigDecimal calculateCommisssion(BigDecimal price, String airlineCode) {
		BigDecimal commission = null;
		
		int rate = getAirlineCommissionRate(airlineCode);
		if(rate != 0) {
			double percent = rate / 100.00;
			double commissionPortion = price.doubleValue() * percent;
			
			commission = new BigDecimal(commissionPortion).setScale(3, BigDecimal.ROUND_HALF_UP);

		}
		else {
			commission = new BigDecimal("0.00");
		}
		return commission;
	}
	
	public static BigDecimal calculateDefaultCommisssion(BigDecimal price) {
		BigDecimal commission = null;
		
		int rate = getDefaultCommissionRate();
		if(rate != 0) {
			double percent = rate / 100.00;
			double commissionPortion = price.doubleValue() * percent;
			
			commission = new BigDecimal(commissionPortion).setScale(3, BigDecimal.ROUND_HALF_UP);

		}
		else {
			commission = new BigDecimal("0.00");
		}
		return commission;
	}
	
	public static int getDefaultCommissionRate() {
//		@SuppressWarnings("unchecked")
//		List<Commission> commissions = (List<Commission>) StartupManager.getInstance().getCache().get(Constants.CACHE_COMMISSIONS_KEY);
//		
//		if(commissions != null) {
//			
//			for(Commission commission : commissions) {
//				if(Constants.DEFAULT_COMMISSION.equalsIgnoreCase(commission.getName())) {
//					return commission.getRate();
//				}
//			}
//		}
//		
		return 0;
	}
	
	public static BigDecimal calculateCCSurcharge(BigDecimal totalPrice) {
		BigDecimal surcharge = new BigDecimal(0.0);
		
//		@SuppressWarnings("unchecked")
//		List<Tariff> tariffs = (List<Tariff>) StartupManager.getInstance().getCache().get(Constants.CACHE_TARIFFS_KEY);
//		
//		if(tariffs != null) {
//			for(Tariff tariff : tariffs) {
//				if(tariff.getId() == 3 && "percent".equalsIgnoreCase(tariff.getRateType())) {
//					int rate = tariff.getRate();
//					double percent = rate / 100.00;
//					double scd = totalPrice.doubleValue() * percent;
//					surcharge =  new BigDecimal(scd).setScale(3, BigDecimal.ROUND_HALF_UP);
//					return surcharge;
//				}
//			}
//		}

		return surcharge;
	}
	
	public static int getAirlineCommissionRate(String airlineCode) {
		@SuppressWarnings("unchecked")
		//List<Map<String, Object>> airlineCommissions = (List<Map<String, Object>>) StartupManager.getInstance().getCache().get(Constants.CACHE_AIRLINECOMMISSION_KEY);
		List<Map<String, Object>> airlineCommissions = new ArrayList<Map<String, Object>>();
		
		if(airlineCommissions != null) {
			
			for(Map<String, Object> airlineCommission : airlineCommissions) {
				if(airlineCommission.get("airline_id") != null && airlineCode.equalsIgnoreCase(airlineCommission.get("airline_id").toString()) == true) {
					if(airlineCommission.get("commission_rate") != null) {
						return (Integer) airlineCommission.get("commission_rate");
					}
					else {
						return 0;
					}
				}
			}
		}
		
		return 0;
	}
	
	public static int getAirlineCommissionType(String airlineCode) {
		@SuppressWarnings("unchecked")
		//List<Map<String, Object>> airlineCommissions = (List<Map<String, Object>>) StartupManager.getInstance().getCache().get(Constants.CACHE_AIRLINECOMMISSION_KEY);
		List<Map<String, Object>> airlineCommissions = new ArrayList<Map<String, Object>>();

		
		if(airlineCommissions != null) {
			
			for(Map<String, Object> airlineCommission : airlineCommissions) {
				if(airlineCommission.get("airline_id") != null && airlineCode.equalsIgnoreCase(airlineCommission.get("airline_id").toString()) == true) {
					if(airlineCommission.get("commission_type") != null) {
						return (Integer) airlineCommission.get("commission_type");
					}
					else {
						return 0;
					}
				}
			}
		}
		
		return 0;
	}

	public static BigDecimal calculateAgentCommission(String agentId, BigDecimal commission) {
		// TODO Auto-generated method stub
		return new BigDecimal("0.00");
	}

	public static Map<String, Object> buildReservationResponse(Map<String, Object> segment) {
		Map<String, Object> trip = new HashMap<String, Object>();

		trip.put("departure_city", segment.get("departure_city"));
		
		String arrivalCity = (segment.get("arrival_city") != null ? segment.get("arrival_city").toString() : "");
		String finalDestCity = (segment.get("final_dest_city") != null ? segment.get("final_dest_city").toString() : "");
		
		if(finalDestCity.equalsIgnoreCase(arrivalCity) == false) {
			arrivalCity = finalDestCity;
		}
		
		String cabinCode = (segment.get("cabin_code") != null ? segment.get("cabin_code").toString() : "");
		String cabin     = (segment.get("cabin") != null ? segment.get("cabin").toString() : "");
		
		trip.put("arrival_city", arrivalCity);
		trip.put("departure_date", segment.get("departure_date"));
		trip.put("departure_time", segment.get("departure_time"));
		trip.put("arrival_date", segment.get("arrival_date"));
		trip.put("arrival_time", segment.get("arrival_time"));
		trip.put("flight_no", segment.get("flight_no"));
		trip.put("airline_name", segment.get("airline_name"));
		trip.put("cabin", cabinCode+" "+cabin);
		
		return trip;
	}

	public static void waitForlogin(Future<String> futureLogin) throws WaafiTravelsException {
		long counter    = 0;
		long waitTimeMs = 100; 
		
		while(!futureLogin.isDone() && counter < Constants.LOGIN_TIMEOUT) {
			try {
				Thread.sleep(waitTimeMs);
				counter += waitTimeMs;
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
		}
		
		if(!futureLogin.isDone()) {
//			throw new WaafiTravelsException("Payment gateway login timeout", 
//					Constants.ERROR_PAYMENT_LOGIN_REQUEST_ERROR);
		}
	}

	public static String buildCancellationSegmets(String reservationId, int numSegments) {
		String segments = "";
		
		for(int i = 1; i <= numSegments; i++) {
			// ^X1^X2
			segments = segments+"^X"+i;
		}
		return segments;
	}

//	public static String validCreateSettlementRequest(SettlementRequest setRequest) {
//		// Check start date
//		// Check end date
//		// Party type
//		// Party Id
//		
//		String error = null;
//		Map<String,String> errorMap = new HashMap<String,String>();
//		
//		if(setRequest.start_date == null || setRequest.start_date.length() <= 0 
//				|| strToDate(setRequest.start_date, Constants.DATE_FORMAT) == null) {
//			errorMap.put("start_date", "Must be contain valid date format: dd#mm#yyyy");
//		}
//		
//		if(setRequest.end_date == null || setRequest.end_date.length() <= 0 
//				|| strToDate(setRequest.end_date, Constants.DATE_FORMAT) == null) {
//			errorMap.put("end_date", "Must be contain valid date format: dd#mm#yyyy");
//		}
//		
//		if(setRequest.party_type == null || setRequest.party_type.length() <= 0) {
//			errorMap.put("party_type", "Must not be empty.");
//		}
//		
//		if(setRequest.party_id == null || setRequest.party_id.length() <= 0) {
//			errorMap.put("party_id", "Must not be empty.");
//		}
//		
//		if("agent".equalsIgnoreCase(setRequest.party_type)) {
//			if(!validAgent(setRequest.party_id)) {
//				errorMap.put("party_id", "Agent not supported yet.");
//			}
//		}
//		else if("airline".equalsIgnoreCase(setRequest.party_type)) {
//			if(getAirlineNameByCode(setRequest.party_id) == null) {
//				errorMap.put("party_id", "Airline not found. Please provide valid airline ID.");
//			}
//		}
//		else {
//			errorMap.put("party_type", "Must be one of the following: ['agent', 'airline']");
//		}
//		
//		if(!errorMap.isEmpty()) {
//			ObjectMapper mapper = new ObjectMapper();
//			try {
//				error = mapper.writeValueAsString(errorMap);
//			} catch (JsonProcessingException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//				error = e.getMessage();
//			}
//		}
//		
//		return error;
//	}
	
//	public static String validJubbaSalesRequest(JubbaSalesReportRequest salesReprotRequest) {
//		// Check start date
//		// Check end date
//		
//		String error = null;
//		Map<String,String> errorMap = new HashMap<String,String>();
//		
//		if(salesReprotRequest.start_date == null || salesReprotRequest.start_date.length() <= 0 
//				|| strToDate(salesReprotRequest.start_date, Constants.DATE_FORMAT) == null) {
//			errorMap.put("start_date", "Must be contain valid date format: dd#mm#yyyy");
//		}
//		
//		if(salesReprotRequest.end_date == null || salesReprotRequest.end_date.length() <= 0 
//				|| strToDate(salesReprotRequest.end_date, Constants.DATE_FORMAT) == null) {
//			errorMap.put("end_date", "Must be contain valid date format: dd#mm#yyyy");
//		}
//		
//		if(!errorMap.isEmpty()) {
//			ObjectMapper mapper = new ObjectMapper();
//			try {
//				error = mapper.writeValueAsString(errorMap);
//			} catch (JsonProcessingException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//				error = e.getMessage();
//			}
//		}
//		
//		return error;
//	}

//	public static String validContactRequest(ContactUsRequest contactRequest) {
//		String error = null;
//		Map<String,Object> errorMap = new HashMap<String,Object>();
//		
//		if(contactRequest.name == null || contactRequest.name.length() <= 0) {
//			errorMap.put("name", "Must not be empty");
//		}
//		
//		if(contactRequest.phone == null || contactRequest.phone.length() <= 0) {
//			errorMap.put("phone", "Must not be empty");
//		}
//		
//		if(contactRequest.email == null || contactRequest.email.length() <= 0 || contactRequest.email.contains("@") == false) {
//			errorMap.put("email", "Must be contain valid email address");
//		}
//
//		if(contactRequest.subject == null || contactRequest.subject.length() <= 0) {
//			errorMap.put("subject", "Must be not be empty");
//		}
//
//		if(contactRequest.message == null || contactRequest.message.length() <= 0) {
//			errorMap.put("subject", "Must be not be empty");
//		}
//		
//		if(!errorMap.isEmpty()) {
//			ObjectMapper mapper = new ObjectMapper();
//			try {
//				errorMap.put("message", "Invalid request.");
//				errorMap.put("status", Constants.REQUEST_STATUS.failed.name());
//				error = mapper.writeValueAsString(errorMap);
//			} catch (JsonProcessingException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//				error = e.getMessage();
//			}
//		}
//		
//		return error;
//	}

	private static boolean validAgent(String party_id) {
		// TODO Auto-generated method stub
		return false;
	}

	public static String convertToSqlDate(String date) {
		//10#30#2016
		String[] dateParts = date.split("#");
		
		//2016-10-30
		String sqlDate = dateParts[2]+"-"+dateParts[0]+"-"+dateParts[1];
		
		return sqlDate;
	}
	
	public static String convertToSearchlDate(String date) {
		//10#30#2016
		String[] dateParts = date.split("#");
		
		//2016-10-30
		String sqlDate = dateParts[2]+"-"+dateParts[0]+"-"+dateParts[1];
		
		return sqlDate;
	}
	
	public static Map<String, Object> getSearchRequest(String searchId) {
//		DBConnector dbConnector  = null;
		CallableStatement cStmt  = null;
		ResultSet resultSet      = null;
		Map<String, Object> trip = null;
		
//		try {
//			
//			dbConnector = new DBConnector();
//			String sql  = "{CALL spGetSearchRequest(?)}";
//			cStmt       = dbConnector.getConnection().prepareCall(sql);
//			
//			cStmt.setString(1, searchId);
//
//			resultSet = cStmt.executeQuery();
//
//			if(resultSet == null) {
//				return null;
//			}
//
//			if(resultSet.next()) {
//				trip = new HashMap<String, Object>();
//				
//				trip.put("currency", (resultSet.getString("currency") == null ? "" : resultSet.getString("currency")));
//				trip.put("num_adults", resultSet.getInt("num_adults"));
//				trip.put("num_children", resultSet.getInt("num_children"));
//				trip.put("num_infants", resultSet.getInt("num_infants"));
//				trip.put("cabin", (resultSet.getString("cabin") == null ? "" : resultSet.getString("cabin")));
//			}
//
//		} catch (SQLException | NamingException e) {
//			e.printStackTrace();
//		}
//		finally {
//			if(resultSet != null)
//				try {
//					resultSet.close();
//				} catch (SQLException e1) {
//
//				}
//			
//			if(cStmt != null)
//				try {
//					cStmt.close();
//				} catch (SQLException e) {
//
//				}
//			if(dbConnector != null) dbConnector.close();
//		}
		
		return trip;
	}

	public static String buildJubbaReportDate(String date) {
		//10#30#2016

		String month = getShortMonthName(date, Constants.DATE_FORMAT);
		String day   = getDepartureDayForReport(date, "#");
		
		String reportDate = day+month;
		return reportDate;
	}

//	public static void saveClientSession(WaafiTravelsClient wtClient) throws WaafiTravelsException {
//		DBConnector dbConnector = null;
//		CallableStatement cStmt = null;
//
//		try {
//
//			dbConnector = new DBConnector();
//			String customerSql  = "{CALL spCreateClientSession(?,?,?)}";
//			cStmt               = dbConnector.getConnection().prepareCall(customerSql);
//			
//			// Insert flight
//			cStmt.setString(1, wtClient.getClient_type());
//			cStmt.setString(2, wtClient.getSessionId());
//			cStmt.setString(3, wtClient.getIp());
//			
//			cStmt.execute();
//			
//		} catch (SQLException | NamingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			throw new WaafiTravelsException(e.getMessage(), Constants.ERROR_INVALID_CLIENT_LOGIN);
//		}
//		finally {
//			if(cStmt != null)
//				try {
//					cStmt.close();
//				} catch (SQLException e) {
//					// TODO Auto-generated catch block
//					//e.printStackTrace();
//				}
//
//			if(dbConnector != null) dbConnector.close();
//		}
//		
//	}

	public static String getTicketValidToDate(String cabin) {
		return cabin;
	}

//	public static Charge getChargeById(int refundInternational) {
//		@SuppressWarnings("unchecked")
//		List<Charge> charges = (List<Charge>) StartupManager.getInstance().getCache().get(Constants.CACHE_CHARGES_KEY);
//		
//		if(charges == null || charges.size() <= 0) {
//			return null;
//		}
//		
//		for(Charge charge : charges) {
//			if(charge.getId() == refundInternational) {
//				return charge;
//			}
//		}
//		return null;
//	}

	public static boolean isInternationalFlight(String city) {
		
		return false;
	}

//	public static DateTime toUTC(String createdTime) {
//		
//		// Append millis
//		if(createdTime != null && createdTime.contains(".") == false) createdTime = createdTime+".000";
//		
//		LocalDateTime ldt = LocalDateTime.parse(createdTime, DateTimeFormat.forPattern(Constants.NONGDS_DATETIME_FORMAT));
//		DateTime utcDatetime = ldt.toDateTime(DateTimeZone.UTC);
//		return utcDatetime;
//	}

	//-------------------------------------------------------------------------------------------------------------------
	//TODO this method was added because classes references for date parsing  were not given
	//you can replace its implementation	
	//added amassillo
	public static int getAgeFromDoB(String date_of_birth) {
		return getAge(date_of_birth);
	}
	//-------------------------------------------------------------------------------------------------------------------
}
