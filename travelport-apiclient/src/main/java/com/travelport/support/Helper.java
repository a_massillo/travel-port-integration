/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.travelport.support;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.travelport.schema.air_v43_0.*;
import com.travelport.schema.common_v43_0.BookingTravelerRef;
import com.travelport.schema.common_v43_0.VendorLocation;

import com.travelport.schema.universal_v43_0.SavedTripActivity.VendorLocationRef;

/**
 * 
 * @author amassillo
 *
 */
public class Helper {
    /**
     * Utility class for building a map that knows about all the segments in the
     * response.
     */
    public static class AirSegmentMap extends HashMap<String, TypeBaseAirSegment> {
        /**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public void add(TypeBaseAirSegment segment) {
            put(segment.getKey(), segment);
        }

        @Override
        public TypeBaseAirSegment get(Object wontWork) {
            throw new RuntimeException("This is disallowed because it was a " + "common mistake to pass a AirSegmentRef here instead "
                    + "of the string contained in the AirSegmentRef");
        }

        public TypeBaseAirSegment getByRef(AirSegmentRef ref) {
            return super.get(ref.getKey());
        }

        public TypeBaseAirSegment getByRef(String ref) {
        	AirSegmentRef lRef = new AirSegmentRef();
        	lRef.setKey(ref);
            return super.get(lRef.getKey());
        }
        public List<TypeBaseAirSegment> getByRef(List<AirSegmentRef> ref) {
        	ArrayList<TypeBaseAirSegment> lArrayOfFTypeBaseAirSegment = new ArrayList<TypeBaseAirSegment>();
            for (Iterator<AirSegmentRef> iterator = ref.iterator(); iterator.hasNext();) {
            	AirSegmentRef airSegment = iterator.next();
            	lArrayOfFTypeBaseAirSegment.add(super.get(airSegment.getKey()));
            }        	
            return lArrayOfFTypeBaseAirSegment;
        }
    }

    public static class FareMap extends HashMap<String, FareInfo> {
        /**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public void add(FareInfo segment) {
            put(segment.getKey(), segment);
        }

        @Override
        public FareInfo get(Object wontWork) {
            throw new RuntimeException("This is disallowed because it was a " + "common mistake to pass a AirSegmentRef here instead "
                    + "of the string contained in the FareInfo");
        }

        public FareInfo getByRef(String ref) {
            return super.get(ref);
        }

        public FareInfo getByRef(FareInfo ref) {
            return super.get(ref.getKey());
        }
        public List<FareInfo> getByRef(List<FareInfo> ref) {
        	ArrayList<FareInfo> lArrayOfFTypeBaseAirSegment = new ArrayList<FareInfo>();
            for (Iterator<FareInfo> iterator = ref.iterator(); iterator.hasNext();) {
            	FareInfo airSegment = iterator.next();
            	lArrayOfFTypeBaseAirSegment.add(super.get(airSegment.getKey()));
            }        	
            return lArrayOfFTypeBaseAirSegment;
        }
    }
    /**
     * Utility class for building a map that knows all the flight details
     * objects and can look them up by their key.
     */
    public static class FlightDetailsMap extends HashMap<String, FlightDetails> {
        /**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public void add(FlightDetails detail) {
            put(detail.getKey(), detail);
        }

        @Override
        public FlightDetails get(Object wontWork) {
            throw new RuntimeException("This is disallowed because it was a " + "common mistake to pass a FlightSegmentRef here instead "
                    + "of the string contained in the FlightSegmentRef");
        }

        public FlightDetails getByRef(FlightDetailsRef ref) {
            return super.get(ref.getKey());
        }

        public List<FlightDetails> getByRef(List<FlightDetailsRef> ref) {
        	ArrayList<FlightDetails> lArrayOfFlightDetails = new ArrayList<FlightDetails>();
            for (Iterator<FlightDetailsRef> iterator = ref.iterator(); iterator.hasNext();) {
            	FlightDetailsRef airSegment = iterator.next();
            	lArrayOfFlightDetails.add(super.get(airSegment.getKey()));
            }        	
            return lArrayOfFlightDetails;
        }
    }

    public static class TicketInforByTravelerRefMap extends HashMap<String, TicketInfo> {
        /**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public void add(TicketInfo segment) {
            put(segment.getBookingTravelerRef(), segment);
        }

        @Override
        public TicketInfo get(Object wontWork) {
            throw new RuntimeException("This is disallowed because it was a " + "common mistake to pass a AirSegmentRef here instead "
                    + "of the string contained in the BookingTravelerRef");
        }

        public TicketInfo getByRef(BookingTravelerRef ref) {
            return super.get(ref.getKey());
        }

        public TicketInfo getByRef(String ref) {
        	BookingTravelerRef lRef = new BookingTravelerRef();
        	lRef.setKey(ref);
            return super.get(lRef.getKey());
        }
    }

    // this is the format we SEND to travelport
    public static SimpleDateFormat searchFormat = new SimpleDateFormat("yyyy-MM-dd");

    // return a date that is n days in future
    public static String daysInFuture(int n) {
        Date now = new Date(), future;
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(now);
        calendar.add(Calendar.DATE, n);
        future = calendar.getTime();
        return searchFormat.format(future);
    }

    /**
     * Build the map from references to flight details to to real flight
     * details.
     */
    public static FlightDetailsMap createFlightDetailsMap(List<FlightDetails> details) {
        FlightDetailsMap result = new FlightDetailsMap();
        for (Iterator<FlightDetails> iterator = details.iterator(); iterator.hasNext();) {
            FlightDetails deet = (FlightDetails) iterator.next();
            result.add(deet);
        }
        return result;
    }

    /**
     * Take a air segment list and construct a map of all the segments into a
     * segment map. This makes other parts of the work easier.
     */
    public static FareMap createFareInfoMap(List<FareInfo> segments) {
        // construct a map with all the segments and their keys
    	FareMap segmentMap = new FareMap();

        for (Iterator<FareInfo> iterator = segments.iterator(); iterator.hasNext();) {
        	FareInfo airSegment = (FareInfo) iterator.next();
            segmentMap.add(airSegment);
        }

        return segmentMap;
    }

    /**
     * Take a air segment list and construct a map of all the segments into a
     * segment map. This makes other parts of the work easier.
     */
    public static AirSegmentMap createAirSegmentMap(List<TypeBaseAirSegment> segments) {
        // construct a map with all the segments and their keys
        AirSegmentMap segmentMap = new AirSegmentMap();

        for (Iterator<TypeBaseAirSegment> iterator = segments.iterator(); iterator.hasNext();) {
        	TypeBaseAirSegment airSegment = (TypeBaseAirSegment) iterator.next();
            segmentMap.add(airSegment);
        }

        return segmentMap;
    }

    /**
     * Take a air segment list and construct a map of all the segments into a
     * segment map. This makes other parts of the work easier.
     */
    public static TicketInforByTravelerRefMap createTicketInfoByTravelerRefMap(List<TicketInfo> ticketInfo) {
        // construct a map with all the segments and their keys
    	TicketInforByTravelerRefMap segmentMap = new TicketInforByTravelerRefMap();

        for (Iterator<TicketInfo> iterator = ticketInfo.iterator(); iterator.hasNext();) {
        	TicketInfo airSegment = (TicketInfo) iterator.next();
            segmentMap.add(airSegment);
        }
        return segmentMap;
    }
    
    public static final String RAIL_PROVIDER = "RCH";
    
    public static final String LOW_COST_PROVIDER = "ACH";
    // this is not *quite* a travel port date because tport puts a colon in
    // the timezone which is not ok with RFC822 timezones
    public static SimpleDateFormat tportResultFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

    // turn a travel port date from a response back into a java object
    // not as easy to do because java gets confused by the iso 8601 timezone
    public static Date dateFromISO8601(String iso) {
        try {
            String noColon = iso.substring(0, 26) + iso.substring(27);
            return tportResultFormat.parse(noColon);
        } catch (ParseException e) {
            throw new RuntimeException("Really unlikely, but it looks like " + "travelport is not using ISO dates anymore! "
                    + e.getMessage());
        }
    }

    /**
     * Map from a key to an actual Vendor location
     */
    public static class VendorLocMap extends HashMap<String, VendorLocation> {
        /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public void add(VendorLocation location) {
            put(location.getKey(),location);
        }

        @Override
        public VendorLocation get(Object wontWork) {
            throw new RuntimeException("This is disallowed because it was a " + "common mistake to pass a AirSegmentRef here instead "
                    + "of the string contained in the VendorLocationRef");
        }

        /**
         * Much more type safe than the get above
         * 
         * @param ref the reference object
         * @return the vendor location being referenced
         */
        public VendorLocation getByLocationRef(VendorLocationRef ref) {
            return super.get(ref.getKey());
        }
        
   
      
//        /**
//         * Merge all the locations into this object.A non-zero return is probably
//         * very bad.
//         */
//        public int mergeAll(List<HotelSearchResult> list) {
//            int result = 0;
//            for (Iterator<HotelSearchResult> sourceIter = list.iterator(); sourceIter.hasNext();) {
//                HotelSearchResult loc = (HotelSearchResult) sourceIter.next();
//                
//                Iterator<VendorLocation> it = loc.getVendorLocation().iterator();                         
//                while(it.hasNext()) {
//                    ++result;
//                    VendorLocation vl = it.next();
//                    add(vl);
//                }
//                
//            }
//            
//            return result;
//        }
        
    }


    /**
     * Parse a number from something with a currency code on the front.  Aborts
     * if the number cannot be understood.
     */
    public static BigDecimal parseNumberWithCurrency(String numberWithCurrency) {
        // first 3 chars are currency code
        String price = numberWithCurrency.substring(3);
        return BigDecimal.valueOf(Double.parseDouble(price));
    }

    /**
     * Parse a number from something with a currency code on the front.  Aborts
     * if the number cannot be understood.
     */
    public static BigDecimal parseNumberWithCurrency(String numberWithCurrency, String currenrcySymbol) {
        // removes the currency symbol
        String price = numberWithCurrency.replace(currenrcySymbol, "");
        return BigDecimal.valueOf(Double.parseDouble(price));
    }
    
	/**
	 * 
	 * @param pAirSegments
	 * @param pCurrentIndex
	 * @return
	 */
	public static String getFinalDestination (List<TypeBaseAirSegment> pAirSegments, int pCurrentIndex) {
		String pCurrentDestination  =  pAirSegments.get(pCurrentIndex).getDestination();
		//search among air segments until destination (i) != origin (i+1)
		for (int i = pCurrentIndex + 1; i< pAirSegments.size() ; i++) {
			if(pAirSegments.get(i).getOrigin().compareTo(pCurrentDestination) != 0)
				return pAirSegments.get(i-1).getDestination();
		}
		//throws?
		return null;
	}
}