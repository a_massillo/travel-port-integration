/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.travelport.support;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.ws.Service;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.travelport.schema.common_v43_0.BillingPointOfSaleInfo;
import com.travelport.service.air_v43_0.AirAvailabilitySearchPortType;
import com.travelport.service.air_v43_0.AirLowFareSearchAsynchPortType;
import com.travelport.service.air_v43_0.AirLowFareSearchPortType;
import com.travelport.service.air_v43_0.AirPricePortType;
import com.travelport.service.air_v43_0.AirRefundQuotePortType;
import com.travelport.service.air_v43_0.AirRefundTicketPortType;
import com.travelport.service.air_v43_0.AirRetrieveLowFareSearchPortType;
import com.travelport.service.air_v43_0.AirService;
import com.travelport.service.air_v43_0.AirTicketingPortType;
//import com.travelport.service.system_v32_0.SystemPingPortType;
//import com.travelport.service.system_v32_0.SystemService;
//import com.travelport.service.system_v32_0.SystemTimePortType;

import com.travelport.service.universal_v43_0.AirCreateReservationPortType;
import com.travelport.service.universal_v43_0.UniversalRecordCancelService;
import com.travelport.service.universal_v43_0.UniversalRecordCancelServicePortType;
import com.travelport.service.universal_v43_0.UniversalRecordRetrieveServicePortType;
import com.travelport.service.universal_v43_0.UniversalRecordService;
import com.travelport.service.util_v43_0.ReferenceDataLookupPortType;
import com.travelport.service.util_v43_0.ReferenceDataLookupService;
import com.travelport.service.util_v43_0.ReferenceDataRetrievePortType;
import com.travelport.service.util_v43_0.ReferenceDataRetrieveService;
import com.travelport.support.config.TravelPortConfig;

import lombok.Data;



/**
 * Convenience class for getting access to the WSDL services without needing to
 * mess with the parameters and such. This hides some CXF specific things that
 * most people don't need to care about.
 * @author amassillo
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TravelPortServicesHelper {
	
	final static Logger logger = Logger.getLogger(TravelPortServicesHelper.class);
	
	// make this point to the right path for you
	private TravelPortConfig $config;
	
	//services you want to use, add or remove as needed
	private PortWrapper<AirLowFareSearchPortType> airShop;
	private PortWrapper<AirLowFareSearchAsynchPortType> airShopAsync;
	private PortWrapper<AirAvailabilitySearchPortType> airAvailability;
	private PortWrapper<AirRetrieveLowFareSearchPortType> airRetrieve;
	 
	private PortWrapper<AirPricePortType> airPrice;
	private PortWrapper<AirCreateReservationPortType> airReserve;
	private PortWrapper<AirTicketingPortType> airTicket;

//	private PortWrapper<SystemPingPortType> sysPing;
//	private PortWrapper<SystemInfoPortType> sysInfo;
//	private PortWrapper<SystemTimePortType> sysTime;

	private PortWrapper<UniversalRecordRetrieveServicePortType> universalRetrieve ;
	private PortWrapper<ReferenceDataRetrievePortType> referenceRetrieve;
	private PortWrapper<ReferenceDataLookupPortType> referenceLookupRetrieve;
	private PortWrapper<UniversalRecordCancelServicePortType> cancelJourney;
	private PortWrapper<AirRefundQuotePortType> airRefundQuote;
	private PortWrapper<AirRefundTicketPortType> airRefund;
	
	public TravelPortServicesHelper(TravelPortConfig pConfig) {
		this.$config = pConfig;
		this.init();
	}
	
	private void init() {
		// location of the WSDL files within this repository
//		String SYSTEM_WSDL = "system_v32_0/System.wsdl";
		String AIR_WSDL = "air_v43_0/Air.wsdl";
		String UNIVERSAL_WSDL = "universal_v43_0/UniversalRecord.wsdl";
		String UTIL_WSDL = "util_v43_0/Util.wsdl";
		
		String endPointPrefix = this.$config.getEndPointPrefix();
		String username = this.$config.getUsername();
		String password = this.$config.getPassword();
//		String SYSTEM_ENDPOINT = endPointPrefix + "SystemService";
		String AIR_ENDPOINT = endPointPrefix 	+ "AirService";
		String UNIVERSAL_ENDPOINT = endPointPrefix + "UniversalRecordService";
		String UTIL_ENDPOINT = endPointPrefix 	+ "UtilService";
		String REF_DATA_ENDPOINT = endPointPrefix + "ReferenceDataLookupService";

		//SERVICES YOU WANT TO USE
		// air ports-------------------------------------------------------------------------------------------------------------------------
		 Service air = new AirService(this.getURLForWSDL(AIR_WSDL));
		 airShop 				= new PortWrapper<AirLowFareSearchPortType>(air, AirLowFareSearchPortType.class, AIR_ENDPOINT, username, password);
		 airShopAsync 			= new PortWrapper<AirLowFareSearchAsynchPortType>(air, AirLowFareSearchAsynchPortType.class, AIR_ENDPOINT, username, password);
		 airAvailability 		= new PortWrapper<AirAvailabilitySearchPortType>(air, AirAvailabilitySearchPortType.class, AIR_ENDPOINT, username, password);
		 airRetrieve 			= new PortWrapper<AirRetrieveLowFareSearchPortType>(air, AirRetrieveLowFareSearchPortType.class, AIR_ENDPOINT, username, password);
		 airPrice 				= new PortWrapper<AirPricePortType>(air, AirPricePortType.class, AIR_ENDPOINT, username, password);
		 airTicket 				= new PortWrapper<AirTicketingPortType>(air, AirTicketingPortType.class, AIR_ENDPOINT, username, password);
		 airRefund				= new PortWrapper<AirRefundTicketPortType>(air, AirRefundTicketPortType.class, AIR_ENDPOINT, username, password);
		 airRefundQuote			= new PortWrapper<AirRefundQuotePortType>(air, AirRefundQuotePortType.class, AIR_ENDPOINT, username, password);
		 Service universalAir 	= new com.travelport.service.universal_v43_0.AirService(this.getURLForWSDL(UNIVERSAL_WSDL)); 
		 airReserve 			= new PortWrapper<AirCreateReservationPortType>(universalAir, AirCreateReservationPortType.class, AIR_ENDPOINT, username, password);
				
		 // system ports---------------------------------------------------------------------------------------------------------------------
//		 Service sys = new SystemService(this.getURLForWSDL(SYSTEM_WSDL));
//		 sysPing = new PortWrapper<SystemPingPortType>(sys, SystemPingPortType.class, SYSTEM_ENDPOINT, username, password);
//		 sysInfo = new PortWrapper<SystemInfoPortType>(sys, SystemInfoPortType.class, SYSTEM_ENDPOINT, username, password);
//		 sysTime = new PortWrapper<SystemTimePortType>(sys, SystemTimePortType.class, SYSTEM_ENDPOINT, username, password); // please check generated port type has getSystemtimePortType method instead of getSystemTimePortType, manually updated
		 
		 // universal record ports------------------------------------------------------------------------------------------------------------
		 Service universal = new UniversalRecordService(this.getURLForWSDL(UNIVERSAL_WSDL));
		 universalRetrieve = new PortWrapper<UniversalRecordRetrieveServicePortType>(universal, UniversalRecordRetrieveServicePortType.class, UNIVERSAL_ENDPOINT, username, password);
		 Service cancelUniversal = new UniversalRecordCancelService(this.getURLForWSDL(UNIVERSAL_WSDL));
		 cancelJourney	   = new PortWrapper<UniversalRecordCancelServicePortType>(cancelUniversal,UniversalRecordCancelServicePortType.class, UNIVERSAL_ENDPOINT, username, password);
		 // util ports-----------------------------------------------------------------------------------------------------------------------
		 Service referenceDataRetrievePort = new ReferenceDataRetrieveService(this.getURLForWSDL(UTIL_WSDL));
		 referenceRetrieve = new PortWrapper<ReferenceDataRetrievePortType>(referenceDataRetrievePort, ReferenceDataRetrievePortType.class, UTIL_ENDPOINT, username, password);
		 Service referenceDataLookupPort = new ReferenceDataLookupService(this.getURLForWSDL(UTIL_WSDL));
		 referenceLookupRetrieve = new PortWrapper<ReferenceDataLookupPortType>(referenceDataLookupPort, ReferenceDataLookupPortType.class, REF_DATA_ENDPOINT, username, password);
	}

		



	/**
	 * This checks that the path given by a URL is well formed as URL despite
	 * the fact that this must be a pointer to a file.
	 * 
	 * @param wsdlFileInThisProject
	 *            name of the wsdl file, with dir prefix
	 * @return the URL that points to the wsdl
	 */
	private URL getURLForWSDL(String wsdlFileInThisProject) {
		try {
			URL url = new URL(this.$config.getLocalWsdlPath() + wsdlFileInThisProject);
			return url;
		} catch (MalformedURLException e) {
			throw new RuntimeException("The URL to access the WSDL was not "
					+ "well-formed! Check the URLPREFIX value in the class "
					+ "WSDLService in the file Helper.java.  We tried to "
					+ "to use this url:\n" + this.$config.getLocalWsdlPath() + wsdlFileInThisProject + "\n");

		}
	}
	
	private BillingPointOfSaleInfo billingPointOfSaleInfo;
	public BillingPointOfSaleInfo getBillingPointOfSaleInfo() {
		if (null == billingPointOfSaleInfo) {
			billingPointOfSaleInfo = new BillingPointOfSaleInfo();
			billingPointOfSaleInfo.setOriginApplication(this.$config.getBillingPointOfSale());
		}
		return (billingPointOfSaleInfo);
	}
	
	public String getTargetBranch() {
		return this.$config.getTargetBranch();
	}
}