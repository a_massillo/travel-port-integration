/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.travelport.support.request.enums;

/**
 * 
 * @author amassillo
 *
 */
public enum PassengerType {
	CNN ("Child"),
	ADT ("Adult"),
	INS ("Infant"),
	INF ("INF");// INFANT WITHOUT SEAT
	
	private String label;
	
	PassengerType(String pLabel) {
		this.label = pLabel;
	}
	
	public String getLabel() {
		return this.label;
	}
}
