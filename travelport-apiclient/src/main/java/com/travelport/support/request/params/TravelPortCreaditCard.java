package com.travelport.support.request.params;

import java.util.Arrays;


import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.travelport.schema.common_v43_0.CreditCard;
import com.travelport.schema.common_v43_0.State;
import com.travelport.schema.common_v43_0.TypeStructuredAddress;

/**
 * 
 * @author amassillo
 *
 */
public class TravelPortCreaditCard  extends CreditCard{

	public TravelPortCreaditCard() {}

	/**
	 * 
	 * @param pCardType
	 * @param pNumber
	 * @param pExpDateYear
	 * @param pExpDateMonth
	 * @param pName
	 * @param pCvv
	 * @param pBankCountryCode
	 * @throws DatatypeConfigurationException
	 */
	public TravelPortCreaditCard(String pCardType, String pNumber, int pExpDateYear,  int pExpDateMonth,
								 String pName, String pCvv, String pBankCountryCode){
		CreditCard cc = new CreditCard();
		cc.setType(pCardType);
		cc.setNumber(pNumber);
		cc.setBankCountryCode(pBankCountryCode);
		DatatypeFactory dataTypeFactory;
		try {
			dataTypeFactory = DatatypeFactory.newInstance();
			XMLGregorianCalendar lCalendarDate  = dataTypeFactory.newXMLGregorianCalendarDate(pExpDateYear, pExpDateMonth,
			DatatypeConstants.FIELD_UNDEFINED,DatatypeConstants.FIELD_UNDEFINED);
			cc.setExpDate(lCalendarDate);
			cc.setName(pName);
			cc.setCVV(pCvv);
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	/**
	 * 
	 * @param pName
	 * @param pCountry
	 * @param pState
	 * @param pCity
	 * @param pZipCode
	 * @param pStreet
	 * @param pAddStreets
	 */
	public void setTravelPortddress(String pName, String pCountry, String pState,
									String pCity, String pZipCode, String pStreet, String... pAddStreets) {
		 TypeStructuredAddress addr = new TypeStructuredAddress();
    	 addr.setAddressName(pName);
    	 addr.setCountry(pCountry);
    	 State vt = new State();
		 vt.setValue(pState);
		 addr.setState(vt);
		 addr.setCity(pCity);
		 addr.setPostalCode(pZipCode);
		 addr.getStreet().add(pStreet);
		 //additional streets
		 if (null != pAddStreets)
			 addr.getStreet().addAll(Arrays.asList(pAddStreets));
		 this.setBillingAddress(addr);  
	}
}
