package com.travelport.support.request;



import java.math.BigInteger;

import com.travelport.schema.air_v43_0.AirLegModifiers;
import com.travelport.schema.air_v43_0.FlightType;
import com.travelport.schema.air_v43_0.PreferredCabins;
import com.travelport.schema.air_v43_0.SearchAirLeg;
import com.travelport.schema.common_v43_0.CabinClass;

import com.travelport.schema.common_v43_0.CityOrAirport;
import com.travelport.schema.common_v43_0.Distance;
import com.travelport.schema.common_v43_0.TypeFlexibleTimeSpec;
import com.travelport.schema.common_v43_0.TypeFlexibleTimeSpec.SearchExtraDays;
import com.travelport.schema.common_v43_0.TypeSearchLocation;
import com.travelport.support.request.enums.SeatClass;

/**
 * 
 *
 */
public class SearchAirLegExt extends SearchAirLeg{
	
	public SearchAirLegExt() {
	}
	
	public SearchAirLegExt(String originCityorAirportCode, String destCityOrAirportCode, String departureDate) {
		this.setBounds(originCityorAirportCode, destCityOrAirportCode);
		this.addDepartureDate(departureDate,null);
	}
	/**
	 * Create a leg based on the (more general) TypeSearchLocation which can
	 * be a variety of different things (such as non-iata named rail stations).
	 * 
	 * @param originLoc starting point of leg
	 * @param destLoc endpoint of leg
	 * @return
	 */
	public void setBounds(TypeSearchLocation originLoc,TypeSearchLocation destLoc) {
		// add the origin and dest to the leg
		this.getSearchDestination().add(destLoc);
		this.getSearchOrigin().add(originLoc);			
	}
	/**
	 * Create a leg for a search based on simple origin and destination 
	 * between two cities / airports.
	 * 
	 * @param originAirportCode
	 * @param destAirportCode
	 * @return  
	 */
	public void setBounds(String originCityorAirportCode, String destCityOrAirportCode) {
		TypeSearchLocation originLoc = new TypeSearchLocation();
		TypeSearchLocation destLoc = new TypeSearchLocation();

		// airport objects are just wrappers for their codes
		CityOrAirport origin = new CityOrAirport(), dest = new CityOrAirport();
		origin.setCode(originCityorAirportCode);
		dest.setCode(destCityOrAirportCode);
		
		originLoc.setCityOrAirport(origin);
		destLoc.setCityOrAirport(dest);

		setBounds(originLoc, destLoc);
	}

	/**
	 * 
	 * @param jouneyTime
	 * @param maxStops
	 */
	public void setFlightType(int jouneyTime, int maxStops) {
		AirLegModifiers legModifiers = new AirLegModifiers();
		
		//maximum journey time
		legModifiers.setMaxJourneyTime(jouneyTime);
		//maximum stops
		FlightType flightType = new FlightType();
		flightType.setMaxStops(maxStops);
		
		legModifiers.setFlightType(flightType);
		this.setAirLegModifiers(legModifiers);
	}
	
	/**
	 * Make a search location based on a city or airport code (city is 
	 * preferred to airport in a conflict) and set the search radius to
	 * 50mi.
	 */
	public static TypeSearchLocation createLocationNear(String cityOrAirportCode) {
		TypeSearchLocation result = new TypeSearchLocation();
		
		//city
		CityOrAirport place = new CityOrAirport();
		place.setCode(cityOrAirportCode);
		place.setPreferCity(true);
		result.setCityOrAirport(place);

		//distance
		Distance dist = new Distance();
		dist.setUnits("mi");
		dist.setValue(BigInteger.valueOf(50));
		result.setDistance(dist);
		
		return result;
	}
	 
	
	/**
	 * Mmodify a search leg to use specific class of service as preferred.
	 * 
	 * @param pSeatType SeatType
	 */
	public void addPreferredSeatType(SeatClass pSeatType) {
		AirLegModifiers modifiers = new AirLegModifiers();
		PreferredCabins cabins = new PreferredCabins();
		CabinClass econ = new CabinClass();
		econ.setType(pSeatType.name());
		cabins.setCabinClass(econ);
		modifiers.setPreferredCabins(cabins);
		this.setAirLegModifiers(modifiers);
	}

	/**
	 * Modify a search leg based on a departure date
	 * 
	 * @param pExtraDays extra days to look before/ after, can be null
	 * @param pDepartureDate the departure date in YYYY-MM-dd
	 */
	public void addDepartureDate(String pDepartureDate, Integer pExtraDays) {
		// flexible time spec is flexible in that it allows you to say
		// days before or days after
		TypeFlexibleTimeSpec noFlex = new TypeFlexibleTimeSpec();
		if (pExtraDays!=null) {
			SearchExtraDays extraDays = new SearchExtraDays();
			extraDays.setDaysAfter(pExtraDays);
			extraDays.setDaysBefore(pExtraDays);
			noFlex.setSearchExtraDays(extraDays);
		}
		noFlex.setPreferredTime(pDepartureDate);
		this.getSearchDepTime().add(noFlex);
	}
	
}