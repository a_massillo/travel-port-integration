/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.travelport.support.request.enums;

/**
 * 
 * @author amassillo
 *
 */
public enum AirProvider {
	GALILEO ("1G"),
	WORLD_SPAN("1P"),
	APOLLO ("V1");
	
	private String code;
	AirProvider(String pCode) {
		this.code = pCode;
	}
	public String getCode() {
		return code;
	}
}
