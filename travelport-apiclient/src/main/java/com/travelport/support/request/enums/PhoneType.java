/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.travelport.support.request.enums;

/**
 * https://support.travelport.com/webhelp/uapi/Content/Booking/UniversalRecord/Booking_Traveler_Information.htm#phonevalues
 * @author amassillo
 *
 */
public enum PhoneType {
	Agency, // (for this value an error is returned because booking travelers cannot enter agency phone numbers: Agency Type Phone number not allowed for Traveler Phone.)
	Business,
	Mobile,
	Home,
	Fax,
	Hotel,
	Other,
	None,
	Email,
	Reservations
}
