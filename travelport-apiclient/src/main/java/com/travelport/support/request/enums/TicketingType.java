/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.travelport.support.request.enums;

/**
 * https://support.travelport.com/webhelp/uapi/Content/Air/Shared_Air_Topics/Action_Status.htm
 * @author amassillo
 *
 */
public enum TicketingType {
	ACTIVE,
	TAW,
	TTL,
	TAU
}
