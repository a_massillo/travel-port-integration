package com.travelport.support.request;

import java.math.BigInteger;
import java.util.List;

import com.travelport.schema.air_v43_0.AirSearchModifiers;
import com.travelport.schema.air_v43_0.AirSearchModifiers.PreferredProviders;
import com.travelport.schema.air_v43_0.BaseLowFareSearchReq;
import com.travelport.schema.common_v43_0.BillingPointOfSaleInfo;
import com.travelport.schema.common_v43_0.Provider;
import com.travelport.schema.common_v43_0.SearchPassenger;
import com.travelport.support.request.enums.AirProvider;
import com.travelport.support.request.enums.PassengerType;

public class AirBaseLowFareReq extends BaseLowFareSearchReq{
	
	/**
	 * As of v18 of air wsdl, the point of sale is required.  This sets
	 * the point of sale just contain the GDS and the application name.
	 * @param req the request to modify
	 * @param appName the name to send as application name
	 */
	public void addPointOfSale(String appName ) {
		BillingPointOfSaleInfo posInfo = new BillingPointOfSaleInfo();
		posInfo.setOriginApplication(appName);
		this.setBillingPointOfSaleInfo(posInfo);
	}
	
	/**
	 * summarizes the way to add search providers to requests
	 * @param pProvider
	 */
	public void addPreferredProdider(AirProvider pProvider) {
		Provider lProvider = new Provider();
		lProvider.setCode(pProvider.getCode());
		//to be able to add more than one provider
		if (null == this.getAirSearchModifiers()) {
			this.setAirSearchModifiers(new AirSearchModifiers());
		}
		if (null == this.getAirSearchModifiers().getPreferredProviders()) {
			 this.getAirSearchModifiers().setPreferredProviders(new PreferredProviders());
		}
		this.getAirSearchModifiers().getPreferredProviders()
			.getProvider().add(lProvider);
	}
	
	public List<Provider> getPreferredSearchProviders(){
		return this.getAirSearchModifiers().getPreferredProviders().getProvider();
	}
	
	public void addPassengers(int n,PassengerType pPassangerType ) {
		for (int i = 0; i < n; ++i) {
			SearchPassenger adult = new SearchPassenger();
			adult.setCode(pPassangerType.name());
			adult.setBookingTravelerRef("amasillotest123");
			this.getSearchPassenger().add(adult);
		}
	}
	/**
	 * adds n child passengers to the request
	 * @param request
	 * @param n
	 */
	public void addChildPassengers(int n) {
		// TODO Auto-generated method stub
		for (int i = 0; i < n; ++i) {
			this.addChildPassenger(10);//default age 10
		}
	}

	/**
	 * add single child passenger with age to the request
	 * @param request
	 * @param age
	 */
	public void addChildPassenger(int age) {
		SearchPassenger child = new SearchPassenger();
		child.setCode(PassengerType.CNN.name());
		child.setAge(BigInteger.valueOf(age));
		this.getSearchPassenger().add(child);
	}
}
