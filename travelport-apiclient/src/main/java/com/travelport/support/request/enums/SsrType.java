/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.travelport.support.request.enums;

/**
 * 
 * @author amassillo
 * https://support.travelport.com/webhelp/uapi/Content/Air/Shared_Air_Topics/SSRs_(Special_Service_Requests).htm
 */
public enum SsrType {
	DOCS,	//documents
	DOCA,	//address
	DOCO,
	CKIN,
	//------some disability------------- 
	BLND,
	DEAF,
	WCHC,
	WCHR,
	//---------------------------------
	TKNM,
	MEAL ("Meal SSRs"), //Meal SSRs
	INFT,
	CHLD,
	FQTU,
	GUAR,
	FOID,
	TWOV;
	
	private String ssrText;
	SsrType() {
		this.ssrText = this.name();
	}
	SsrType(String pText){
		this.ssrText = pText;
	}
	public String getSsrText() {
		return ssrText;
	}
	
}
