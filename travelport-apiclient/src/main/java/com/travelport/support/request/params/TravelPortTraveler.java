package com.travelport.support.request.params;

import com.travelport.schema.common_v43_0.BookingTraveler;
import com.travelport.schema.common_v43_0.BookingTravelerName;
import com.travelport.schema.common_v43_0.Email;
import com.travelport.schema.common_v43_0.PhoneNumber;
import com.travelport.support.request.enums.EmailType;
import com.travelport.support.request.enums.PhoneType;

/**
 * 
 * @author amassillo
 *
 */
public class TravelPortTraveler extends BookingTraveler{

	public TravelPortTraveler() {}
	/**
	 * 
	 * @param pFname
	 * @param pLname
	 */
	public TravelPortTraveler(String pFname, String pLname, String pPrefix) {
		BookingTravelerName travelerName = new BookingTravelerName();
		travelerName.setFirst(pFname);
		travelerName.setLast(pLname);
		travelerName.setPrefix(pPrefix);
		this.setBookingTravelerName(travelerName);
	}
	
	/**
	 * 
	 * @param pPhone
	 * @param pPhoneType
	 */
	public void setPhone(String pPhone, PhoneType pPhoneType) {
		PhoneNumber phone = new PhoneNumber();
		//parse phone here
		phone.setCountryCode("33");
		phone.setAreaCode("6");
		phone.setNumber(pPhone);
		phone.setType(pPhoneType.name());
		this.getPhoneNumber().add(phone);
	}
	
	/**
	 * 
	 * @param pEmail
	 * @param pEmailType
	 */
	public void setEmail(String pEmail, EmailType pEmailType) {
		Email email = new Email();
		email.setEmailID(pEmail);
		email.setType(pEmailType.name());
		this.getEmail().add(email);
	}
}
