/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.travelport.support.config;

import java.net.URL;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
/**
 * 
 * @author amassillo
 *
 */
public class PropertiesLoader<P> {
	
	final static Logger logger = Logger.getLogger(PropertiesLoader.class);
	
	private static ObjectMapper mapper = new ObjectMapper(new YAMLFactory()); 
	{
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}
	/**
	 * 
	 * @param pFileName
	 * @param pClass
	 * @return
	 */
	public P loadForInstance(String pFileName, Class<P> pClass){
		logger.info("Loading properties for: "+ pClass.getSimpleName() + " from: "+ pFileName);
		try {
	        URL lUrl = Thread.currentThread().getContextClassLoader().getResource(pFileName);
	        //read properties
	        P lConfig = mapper.readValue(lUrl, pClass);
	        return lConfig;
		}catch(Exception e) {
			logger.error("Not able to load properties: "+ e.getMessage());
		}
		return null;
    }
}
