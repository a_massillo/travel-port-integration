/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.travelport.support.config;

import lombok.Data;

/**
 * 
 * @author amassillo
 *
 */
@Data
public class TravelPortConfig {

	// make this point to the right path for you
	private String localWsdlPath;
	private String username;
	private String password;
	private String gds;
	private String targetBranch;
	private String billingPointOfSale;
	
	// these endpoint parameters vary based on which region you are in...
	// check your travelport sign up to see which url you should use...
	// use the version with copy in the name for test credentials...
	private String endPointPrefix;

}
