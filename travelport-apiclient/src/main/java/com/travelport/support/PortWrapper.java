package com.travelport.support;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.reflect.Method;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.transport.common.gzip.GZIPInInterceptor;
import org.apache.log4j.Logger;

import lombok.Data;

/**
 * This is an attempt to create a type-safe wrapper around a "Port" in the sense
 * of the uAPI. Some shenanigans are needed because in practice there is no
 * way to be certain that the generated code (from the WSDL source) actually
 * conforms to the "usual" way that ports are named and how they are retreived
 * from services.  We hide that and immediately exit if anything goes wrong
 * from the expected convention.
 * 
 * @author iansmith
 *
 * @param <P>  Type of the Port
 */
@Data
public class PortWrapper<P>{
	
	final static Logger logger = Logger.getLogger(PortWrapper.class);
	
    protected P port;
    protected Service service;
    protected boolean currentlyShown = false;
    protected String endpoint;
  
	private String username;
	private String password;

    protected LoggingInInterceptor in = new LoggingInInterceptor();
    protected LoggingOutInterceptor out = new LoggingOutInterceptor();
	
	//for spring-boot to be able to create dependencies
	public PortWrapper() {}
    /**
     * Creating a port requires giving a ServiceWrapper and the class object
     * of *this* port type.  
     * @param svc  This is where the service will be retrieved from (via get)
     * @param clazzP This port type's class object is needed so we can it's
     * name in *a reasonably type-safe way.
     */
    @SuppressWarnings("unchecked")
	public PortWrapper(Service svc, Class<P> clazzP, String endpoint, String pUserName, String pPassword) {
        this.service = svc;
        this.endpoint = endpoint;       
        this.username = pUserName;
        this.password = pPassword;
      
        String getterName = "get" + clazzP.getSimpleName();
        getterName 	= getterName.substring(0, getterName.length()-4);
        
        try {
	        Method getter = service.getClass().getMethod(getterName, (Class<Object>[]) null);
	        port = (P)getter.invoke(service);
	        addParametersToProvider((BindingProvider) port);
        }catch(Exception ex) {
        	ex.printStackTrace();
        }
        
    }
    
    public synchronized P get() {
    	return this.port;
    }
    
    /**
     * Add the necessary gunk to the BindingProvider to make it work right
     * with an authenticated SOAP service.  Also turns on the schema 
     * validation which you probably want since this is a tutorial.
     * 
     * @param provider
     *            the provider (usually this a port object also)
     * @param endpoint
     *            the string that is the internet-accessible place to access
     *            the service
     */

    protected void addParametersToProvider(BindingProvider provider) {
        //add auth values
    	provider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, this.endpoint);       
        provider.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, this.username);
        provider.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, this.password);    
        //add headers
//        Map<String, List<String>> headers = new HashMap<String, List<String>>();
//        headers.put("Accept-Encoding", Arrays.asList("gzip,deflate"));
//        headers.put("Accept"		 , Arrays.asList("gzip"));
//        headers.put("Transfer-Encoding", Arrays.asList("gzip"));
//        provider.getRequestContext().put(Message.PROTOCOL_HEADERS, headers);   
//        provider.getRequestContext().put("schema-validation-enabled", "false");       
//        logger.info("parameters used to connect: "+ this.endpoint + "/ "+this.username + ":"+ this.password);
        /*Map<String, List<String>> rspHeaders = new HashMap<String, List<String>>();
        rspHeaders.put("Content-Encoding", Arrays.asList(URLEncoder.encode("gzip")));
        rspHeaders.put("Transfer-Encoding", Arrays.asList("gzip"));
        //rspHeaders.put("Vary",Arrays.asList(URLEncoder.encode("")));
        provider.getResponseContext().put(Message.PROTOCOL_HEADERS, rspHeaders);*/
    }
    
    public void showXML(boolean show) throws FileNotFoundException {  
    	String lClassName = this.service.getClass().getSimpleName();
    	File newFile = new File("C:\\Users\\Alejandra\\Documents\\"+lClassName+"-LogginInOut.txt");		
    	PrintWriter printWriter = new PrintWriter(newFile);
    	Client cl = ClientProxy.getClient(port);
	  
    	if (show) {
	      if (!currentlyShown) {
	          //System.out.println("ADDED INTERCEPTORS:"+cl.getClass());
	    	  in = new LoggingInInterceptor(printWriter);
	    	  out = new LoggingOutInterceptor(printWriter);
	      	  cl.getInInterceptors().add(new GZIPInInterceptor());
	          cl.getInInterceptors().add(in);
	          cl.getOutInterceptors().add(out);
	          currentlyShown=true;
	      }       
    	} else {
	      if (currentlyShown) {
	      	cl.getInInterceptors().remove(new GZIPInInterceptor());
	          cl.getInInterceptors().remove(in);
	          cl.getOutInterceptors().remove(out);
	          currentlyShown=false;
	      }
    	}
	}

}