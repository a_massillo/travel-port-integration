
package com.travelport.schema.system_v32_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v32_0.BaseRsp;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v32_0}BaseRsp"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/system_v32_0}SystemInfo"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/system_v32_0}SystemTime"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "systemInfo",
    "systemTime"
})
@XmlRootElement(name = "SystemInfoRsp")
public class SystemInfoRsp
    extends BaseRsp
{

    @XmlElement(name = "SystemInfo", required = true)
    protected SystemInfo systemInfo;
    @XmlElement(name = "SystemTime", required = true)
    protected String systemTime;

    /**
     * Obtiene el valor de la propiedad systemInfo.
     * 
     * @return
     *     possible object is
     *     {@link SystemInfo }
     *     
     */
    public SystemInfo getSystemInfo() {
        return systemInfo;
    }

    /**
     * Define el valor de la propiedad systemInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemInfo }
     *     
     */
    public void setSystemInfo(SystemInfo value) {
        this.systemInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad systemTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemTime() {
        return systemTime;
    }

    /**
     * Define el valor de la propiedad systemTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemTime(String value) {
        this.systemTime = value;
    }

}
