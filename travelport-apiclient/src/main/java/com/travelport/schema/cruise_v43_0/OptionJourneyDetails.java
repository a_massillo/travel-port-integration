
package com.travelport.schema.cruise_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="PickUpLocation" type="{http://www.travelport.com/schema/common_v43_0}StringLength1to5" /&gt;
 *       &lt;attribute name="PickUpTime" type="{http://www.w3.org/2001/XMLSchema}time" /&gt;
 *       &lt;attribute name="PickUpDescription" type="{http://www.travelport.com/schema/common_v43_0}StringLength1to30" /&gt;
 *       &lt;attribute name="PickUpCarrier" type="{http://www.travelport.com/schema/common_v43_0}typeCarrier" /&gt;
 *       &lt;attribute name="PickUpFlightNumber" type="{http://www.travelport.com/schema/common_v43_0}typeFlightNumber" /&gt;
 *       &lt;attribute name="ReturnLocation" type="{http://www.travelport.com/schema/common_v43_0}StringLength1to5" /&gt;
 *       &lt;attribute name="ReturnTime" type="{http://www.w3.org/2001/XMLSchema}time" /&gt;
 *       &lt;attribute name="ReturnDescription" type="{http://www.travelport.com/schema/common_v43_0}StringLength1to30" /&gt;
 *       &lt;attribute name="ReturnCarrier" type="{http://www.travelport.com/schema/common_v43_0}typeCarrier" /&gt;
 *       &lt;attribute name="ReturnFlightNumber" type="{http://www.travelport.com/schema/common_v43_0}typeFlightNumber" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "OptionJourneyDetails")
public class OptionJourneyDetails {

    @XmlAttribute(name = "PickUpLocation")
    protected String pickUpLocation;
    @XmlAttribute(name = "PickUpTime")
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar pickUpTime;
    @XmlAttribute(name = "PickUpDescription")
    protected String pickUpDescription;
    @XmlAttribute(name = "PickUpCarrier")
    protected String pickUpCarrier;
    @XmlAttribute(name = "PickUpFlightNumber")
    protected String pickUpFlightNumber;
    @XmlAttribute(name = "ReturnLocation")
    protected String returnLocation;
    @XmlAttribute(name = "ReturnTime")
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar returnTime;
    @XmlAttribute(name = "ReturnDescription")
    protected String returnDescription;
    @XmlAttribute(name = "ReturnCarrier")
    protected String returnCarrier;
    @XmlAttribute(name = "ReturnFlightNumber")
    protected String returnFlightNumber;

    /**
     * Obtiene el valor de la propiedad pickUpLocation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPickUpLocation() {
        return pickUpLocation;
    }

    /**
     * Define el valor de la propiedad pickUpLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPickUpLocation(String value) {
        this.pickUpLocation = value;
    }

    /**
     * Obtiene el valor de la propiedad pickUpTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPickUpTime() {
        return pickUpTime;
    }

    /**
     * Define el valor de la propiedad pickUpTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPickUpTime(XMLGregorianCalendar value) {
        this.pickUpTime = value;
    }

    /**
     * Obtiene el valor de la propiedad pickUpDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPickUpDescription() {
        return pickUpDescription;
    }

    /**
     * Define el valor de la propiedad pickUpDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPickUpDescription(String value) {
        this.pickUpDescription = value;
    }

    /**
     * Obtiene el valor de la propiedad pickUpCarrier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPickUpCarrier() {
        return pickUpCarrier;
    }

    /**
     * Define el valor de la propiedad pickUpCarrier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPickUpCarrier(String value) {
        this.pickUpCarrier = value;
    }

    /**
     * Obtiene el valor de la propiedad pickUpFlightNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPickUpFlightNumber() {
        return pickUpFlightNumber;
    }

    /**
     * Define el valor de la propiedad pickUpFlightNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPickUpFlightNumber(String value) {
        this.pickUpFlightNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad returnLocation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnLocation() {
        return returnLocation;
    }

    /**
     * Define el valor de la propiedad returnLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnLocation(String value) {
        this.returnLocation = value;
    }

    /**
     * Obtiene el valor de la propiedad returnTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReturnTime() {
        return returnTime;
    }

    /**
     * Define el valor de la propiedad returnTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReturnTime(XMLGregorianCalendar value) {
        this.returnTime = value;
    }

    /**
     * Obtiene el valor de la propiedad returnDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnDescription() {
        return returnDescription;
    }

    /**
     * Define el valor de la propiedad returnDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnDescription(String value) {
        this.returnDescription = value;
    }

    /**
     * Obtiene el valor de la propiedad returnCarrier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnCarrier() {
        return returnCarrier;
    }

    /**
     * Define el valor de la propiedad returnCarrier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnCarrier(String value) {
        this.returnCarrier = value;
    }

    /**
     * Obtiene el valor de la propiedad returnFlightNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnFlightNumber() {
        return returnFlightNumber;
    }

    /**
     * Define el valor de la propiedad returnFlightNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnFlightNumber(String value) {
        this.returnFlightNumber = value;
    }

}
