
package com.travelport.schema.cruise_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="Category" type="{http://www.travelport.com/schema/common_v43_0}StringLength1to3" /&gt;
 *       &lt;attribute name="Number" type="{http://www.travelport.com/schema/common_v43_0}StringLength1to5" /&gt;
 *       &lt;attribute name="Location" type="{http://www.travelport.com/schema/common_v43_0}StringLength1" /&gt;
 *       &lt;attribute name="RelativeLocation" type="{http://www.travelport.com/schema/common_v43_0}StringLength1to12" /&gt;
 *       &lt;attribute name="DeckName" type="{http://www.travelport.com/schema/common_v43_0}StringLength1to15" /&gt;
 *       &lt;attribute name="BedConfiguration" type="{http://www.travelport.com/schema/common_v43_0}StringLength1to10" /&gt;
 *       &lt;attribute name="SmokingIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "CabinInfo")
public class CabinInfo {

    @XmlAttribute(name = "Category")
    protected String category;
    @XmlAttribute(name = "Number")
    protected String number;
    @XmlAttribute(name = "Location")
    protected String location;
    @XmlAttribute(name = "RelativeLocation")
    protected String relativeLocation;
    @XmlAttribute(name = "DeckName")
    protected String deckName;
    @XmlAttribute(name = "BedConfiguration")
    protected String bedConfiguration;
    @XmlAttribute(name = "SmokingIndicator")
    protected Boolean smokingIndicator;

    /**
     * Obtiene el valor de la propiedad category.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategory() {
        return category;
    }

    /**
     * Define el valor de la propiedad category.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategory(String value) {
        this.category = value;
    }

    /**
     * Obtiene el valor de la propiedad number.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumber() {
        return number;
    }

    /**
     * Define el valor de la propiedad number.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumber(String value) {
        this.number = value;
    }

    /**
     * Obtiene el valor de la propiedad location.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocation() {
        return location;
    }

    /**
     * Define el valor de la propiedad location.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocation(String value) {
        this.location = value;
    }

    /**
     * Obtiene el valor de la propiedad relativeLocation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelativeLocation() {
        return relativeLocation;
    }

    /**
     * Define el valor de la propiedad relativeLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelativeLocation(String value) {
        this.relativeLocation = value;
    }

    /**
     * Obtiene el valor de la propiedad deckName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeckName() {
        return deckName;
    }

    /**
     * Define el valor de la propiedad deckName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeckName(String value) {
        this.deckName = value;
    }

    /**
     * Obtiene el valor de la propiedad bedConfiguration.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBedConfiguration() {
        return bedConfiguration;
    }

    /**
     * Define el valor de la propiedad bedConfiguration.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBedConfiguration(String value) {
        this.bedConfiguration = value;
    }

    /**
     * Obtiene el valor de la propiedad smokingIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSmokingIndicator() {
        return smokingIndicator;
    }

    /**
     * Define el valor de la propiedad smokingIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSmokingIndicator(Boolean value) {
        this.smokingIndicator = value;
    }

}
