
package com.travelport.schema.cruise_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="AirCharge" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="OptionalCharge" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="WaiverCharge" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="WaiverChargeType" type="{http://www.travelport.com/schema/common_v43_0}StringLength1" /&gt;
 *       &lt;attribute name="PortCharge" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="PortChargeDescription" type="{http://www.travelport.com/schema/common_v43_0}StringLength1to13" /&gt;
 *       &lt;attribute name="PenaltyCharge" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "Charges")
public class Charges {

    @XmlAttribute(name = "AirCharge")
    protected String airCharge;
    @XmlAttribute(name = "OptionalCharge")
    protected String optionalCharge;
    @XmlAttribute(name = "WaiverCharge")
    protected String waiverCharge;
    @XmlAttribute(name = "WaiverChargeType")
    protected String waiverChargeType;
    @XmlAttribute(name = "PortCharge")
    protected String portCharge;
    @XmlAttribute(name = "PortChargeDescription")
    protected String portChargeDescription;
    @XmlAttribute(name = "PenaltyCharge")
    protected String penaltyCharge;

    /**
     * Obtiene el valor de la propiedad airCharge.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirCharge() {
        return airCharge;
    }

    /**
     * Define el valor de la propiedad airCharge.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirCharge(String value) {
        this.airCharge = value;
    }

    /**
     * Obtiene el valor de la propiedad optionalCharge.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOptionalCharge() {
        return optionalCharge;
    }

    /**
     * Define el valor de la propiedad optionalCharge.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOptionalCharge(String value) {
        this.optionalCharge = value;
    }

    /**
     * Obtiene el valor de la propiedad waiverCharge.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWaiverCharge() {
        return waiverCharge;
    }

    /**
     * Define el valor de la propiedad waiverCharge.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWaiverCharge(String value) {
        this.waiverCharge = value;
    }

    /**
     * Obtiene el valor de la propiedad waiverChargeType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWaiverChargeType() {
        return waiverChargeType;
    }

    /**
     * Define el valor de la propiedad waiverChargeType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWaiverChargeType(String value) {
        this.waiverChargeType = value;
    }

    /**
     * Obtiene el valor de la propiedad portCharge.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPortCharge() {
        return portCharge;
    }

    /**
     * Define el valor de la propiedad portCharge.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPortCharge(String value) {
        this.portCharge = value;
    }

    /**
     * Obtiene el valor de la propiedad portChargeDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPortChargeDescription() {
        return portChargeDescription;
    }

    /**
     * Define el valor de la propiedad portChargeDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPortChargeDescription(String value) {
        this.portChargeDescription = value;
    }

    /**
     * Obtiene el valor de la propiedad penaltyCharge.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPenaltyCharge() {
        return penaltyCharge;
    }

    /**
     * Define el valor de la propiedad penaltyCharge.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPenaltyCharge(String value) {
        this.penaltyCharge = value;
    }

}
