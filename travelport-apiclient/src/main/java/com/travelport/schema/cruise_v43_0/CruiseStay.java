
package com.travelport.schema.cruise_v43_0;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Package" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="Name" type="{http://www.travelport.com/schema/common_v43_0}StringLength1to30" /&gt;
 *                 &lt;attribute name="Identifier"&gt;
 *                   &lt;simpleType&gt;
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                       &lt;maxLength value="30"/&gt;
 *                     &lt;/restriction&gt;
 *                   &lt;/simpleType&gt;
 *                 &lt;/attribute&gt;
 *                 &lt;attribute name="PassengerCount" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *                 &lt;attribute name="PackageIdentifier" type="{http://www.travelport.com/schema/common_v43_0}StringLength1to14" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/cruise_v43_0}CabinInfo" minOccurs="0"/&gt;
 *         &lt;element name="DiningInfo" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="Seating" type="{http://www.travelport.com/schema/common_v43_0}StringLength1" /&gt;
 *                 &lt;attribute name="Status" type="{http://www.travelport.com/schema/common_v43_0}typeStatusCode" /&gt;
 *                 &lt;attribute name="TableSize" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ShipName" type="{http://www.travelport.com/schema/common_v43_0}StringLength1to25" /&gt;
 *       &lt;attribute name="DurationOfStay" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="UnitOfStay" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="BookingDate" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *       &lt;attribute name="BookingAgent" type="{http://www.travelport.com/schema/common_v43_0}StringLength1to12" /&gt;
 *       &lt;attribute name="BookingCredit" type="{http://www.travelport.com/schema/common_v43_0}StringLength1to10" /&gt;
 *       &lt;attribute name="OtherPartyConfNbr" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="PassengerOrigin" type="{http://www.travelport.com/schema/common_v43_0}StringLength1to5" /&gt;
 *       &lt;attribute name="ConfirmationNumber" type="{http://www.travelport.com/schema/common_v43_0}StringLength1to16" /&gt;
 *       &lt;attribute name="LinkedConfNumber" type="{http://www.travelport.com/schema/common_v43_0}StringLength1to16" /&gt;
 *       &lt;attribute name="CancellationNumber" type="{http://www.travelport.com/schema/common_v43_0}StringLength1to16" /&gt;
 *       &lt;attribute name="CancellationDate" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *       &lt;attribute name="CancellationTime" type="{http://www.w3.org/2001/XMLSchema}time" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "_package",
    "cabinInfo",
    "diningInfo"
})
@XmlRootElement(name = "CruiseStay")
public class CruiseStay {

    @XmlElement(name = "Package")
    protected CruiseStay.Package _package;
    @XmlElement(name = "CabinInfo")
    protected CabinInfo cabinInfo;
    @XmlElement(name = "DiningInfo")
    protected CruiseStay.DiningInfo diningInfo;
    @XmlAttribute(name = "ShipName")
    protected String shipName;
    @XmlAttribute(name = "DurationOfStay")
    protected BigInteger durationOfStay;
    @XmlAttribute(name = "UnitOfStay")
    protected String unitOfStay;
    @XmlAttribute(name = "BookingDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar bookingDate;
    @XmlAttribute(name = "BookingAgent")
    protected String bookingAgent;
    @XmlAttribute(name = "BookingCredit")
    protected String bookingCredit;
    @XmlAttribute(name = "OtherPartyConfNbr")
    protected BigInteger otherPartyConfNbr;
    @XmlAttribute(name = "PassengerOrigin")
    protected String passengerOrigin;
    @XmlAttribute(name = "ConfirmationNumber")
    protected String confirmationNumber;
    @XmlAttribute(name = "LinkedConfNumber")
    protected String linkedConfNumber;
    @XmlAttribute(name = "CancellationNumber")
    protected String cancellationNumber;
    @XmlAttribute(name = "CancellationDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar cancellationDate;
    @XmlAttribute(name = "CancellationTime")
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar cancellationTime;

    /**
     * Obtiene el valor de la propiedad package.
     * 
     * @return
     *     possible object is
     *     {@link CruiseStay.Package }
     *     
     */
    public CruiseStay.Package getPackage() {
        return _package;
    }

    /**
     * Define el valor de la propiedad package.
     * 
     * @param value
     *     allowed object is
     *     {@link CruiseStay.Package }
     *     
     */
    public void setPackage(CruiseStay.Package value) {
        this._package = value;
    }

    /**
     * Obtiene el valor de la propiedad cabinInfo.
     * 
     * @return
     *     possible object is
     *     {@link CabinInfo }
     *     
     */
    public CabinInfo getCabinInfo() {
        return cabinInfo;
    }

    /**
     * Define el valor de la propiedad cabinInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link CabinInfo }
     *     
     */
    public void setCabinInfo(CabinInfo value) {
        this.cabinInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad diningInfo.
     * 
     * @return
     *     possible object is
     *     {@link CruiseStay.DiningInfo }
     *     
     */
    public CruiseStay.DiningInfo getDiningInfo() {
        return diningInfo;
    }

    /**
     * Define el valor de la propiedad diningInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link CruiseStay.DiningInfo }
     *     
     */
    public void setDiningInfo(CruiseStay.DiningInfo value) {
        this.diningInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad shipName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipName() {
        return shipName;
    }

    /**
     * Define el valor de la propiedad shipName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipName(String value) {
        this.shipName = value;
    }

    /**
     * Obtiene el valor de la propiedad durationOfStay.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDurationOfStay() {
        return durationOfStay;
    }

    /**
     * Define el valor de la propiedad durationOfStay.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDurationOfStay(BigInteger value) {
        this.durationOfStay = value;
    }

    /**
     * Obtiene el valor de la propiedad unitOfStay.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitOfStay() {
        return unitOfStay;
    }

    /**
     * Define el valor de la propiedad unitOfStay.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitOfStay(String value) {
        this.unitOfStay = value;
    }

    /**
     * Obtiene el valor de la propiedad bookingDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBookingDate() {
        return bookingDate;
    }

    /**
     * Define el valor de la propiedad bookingDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBookingDate(XMLGregorianCalendar value) {
        this.bookingDate = value;
    }

    /**
     * Obtiene el valor de la propiedad bookingAgent.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookingAgent() {
        return bookingAgent;
    }

    /**
     * Define el valor de la propiedad bookingAgent.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookingAgent(String value) {
        this.bookingAgent = value;
    }

    /**
     * Obtiene el valor de la propiedad bookingCredit.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookingCredit() {
        return bookingCredit;
    }

    /**
     * Define el valor de la propiedad bookingCredit.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookingCredit(String value) {
        this.bookingCredit = value;
    }

    /**
     * Obtiene el valor de la propiedad otherPartyConfNbr.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getOtherPartyConfNbr() {
        return otherPartyConfNbr;
    }

    /**
     * Define el valor de la propiedad otherPartyConfNbr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setOtherPartyConfNbr(BigInteger value) {
        this.otherPartyConfNbr = value;
    }

    /**
     * Obtiene el valor de la propiedad passengerOrigin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassengerOrigin() {
        return passengerOrigin;
    }

    /**
     * Define el valor de la propiedad passengerOrigin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassengerOrigin(String value) {
        this.passengerOrigin = value;
    }

    /**
     * Obtiene el valor de la propiedad confirmationNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfirmationNumber() {
        return confirmationNumber;
    }

    /**
     * Define el valor de la propiedad confirmationNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfirmationNumber(String value) {
        this.confirmationNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad linkedConfNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLinkedConfNumber() {
        return linkedConfNumber;
    }

    /**
     * Define el valor de la propiedad linkedConfNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLinkedConfNumber(String value) {
        this.linkedConfNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad cancellationNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCancellationNumber() {
        return cancellationNumber;
    }

    /**
     * Define el valor de la propiedad cancellationNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCancellationNumber(String value) {
        this.cancellationNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad cancellationDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCancellationDate() {
        return cancellationDate;
    }

    /**
     * Define el valor de la propiedad cancellationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCancellationDate(XMLGregorianCalendar value) {
        this.cancellationDate = value;
    }

    /**
     * Obtiene el valor de la propiedad cancellationTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCancellationTime() {
        return cancellationTime;
    }

    /**
     * Define el valor de la propiedad cancellationTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCancellationTime(XMLGregorianCalendar value) {
        this.cancellationTime = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attribute name="Seating" type="{http://www.travelport.com/schema/common_v43_0}StringLength1" /&gt;
     *       &lt;attribute name="Status" type="{http://www.travelport.com/schema/common_v43_0}typeStatusCode" /&gt;
     *       &lt;attribute name="TableSize" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class DiningInfo {

        @XmlAttribute(name = "Seating")
        protected String seating;
        @XmlAttribute(name = "Status")
        protected String status;
        @XmlAttribute(name = "TableSize")
        protected BigInteger tableSize;

        /**
         * Obtiene el valor de la propiedad seating.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSeating() {
            return seating;
        }

        /**
         * Define el valor de la propiedad seating.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSeating(String value) {
            this.seating = value;
        }

        /**
         * Obtiene el valor de la propiedad status.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStatus() {
            return status;
        }

        /**
         * Define el valor de la propiedad status.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStatus(String value) {
            this.status = value;
        }

        /**
         * Obtiene el valor de la propiedad tableSize.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTableSize() {
            return tableSize;
        }

        /**
         * Define el valor de la propiedad tableSize.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTableSize(BigInteger value) {
            this.tableSize = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attribute name="Name" type="{http://www.travelport.com/schema/common_v43_0}StringLength1to30" /&gt;
     *       &lt;attribute name="Identifier"&gt;
     *         &lt;simpleType&gt;
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *             &lt;maxLength value="30"/&gt;
     *           &lt;/restriction&gt;
     *         &lt;/simpleType&gt;
     *       &lt;/attribute&gt;
     *       &lt;attribute name="PassengerCount" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
     *       &lt;attribute name="PackageIdentifier" type="{http://www.travelport.com/schema/common_v43_0}StringLength1to14" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Package {

        @XmlAttribute(name = "Name")
        protected String name;
        @XmlAttribute(name = "Identifier")
        protected String identifier;
        @XmlAttribute(name = "PassengerCount")
        protected BigInteger passengerCount;
        @XmlAttribute(name = "PackageIdentifier")
        protected String packageIdentifier;

        /**
         * Obtiene el valor de la propiedad name.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * Define el valor de la propiedad name.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Obtiene el valor de la propiedad identifier.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdentifier() {
            return identifier;
        }

        /**
         * Define el valor de la propiedad identifier.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdentifier(String value) {
            this.identifier = value;
        }

        /**
         * Obtiene el valor de la propiedad passengerCount.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getPassengerCount() {
            return passengerCount;
        }

        /**
         * Define el valor de la propiedad passengerCount.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setPassengerCount(BigInteger value) {
            this.passengerCount = value;
        }

        /**
         * Obtiene el valor de la propiedad packageIdentifier.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPackageIdentifier() {
            return packageIdentifier;
        }

        /**
         * Define el valor de la propiedad packageIdentifier.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPackageIdentifier(String value) {
            this.packageIdentifier = value;
        }

    }

}
