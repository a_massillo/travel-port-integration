
package com.travelport.schema.vehicle_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.TypeVoucherInformation;


/**
 * <p>Clase Java para typePaymentInformation complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="typePaymentInformation"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Voucher" type="{http://www.travelport.com/schema/common_v43_0}typeVoucherInformation" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="BillingNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="BillingReferenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="PrePayment"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="90"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "typePaymentInformation", propOrder = {
    "voucher"
})
@XmlSeeAlso({
    PaymentInformation.class
})
public class TypePaymentInformation {

    @XmlElement(name = "Voucher")
    protected TypeVoucherInformation voucher;
    @XmlAttribute(name = "BillingNumber")
    protected String billingNumber;
    @XmlAttribute(name = "BillingReferenceNumber")
    protected String billingReferenceNumber;
    @XmlAttribute(name = "PrePayment")
    protected String prePayment;

    /**
     * Obtiene el valor de la propiedad voucher.
     * 
     * @return
     *     possible object is
     *     {@link TypeVoucherInformation }
     *     
     */
    public TypeVoucherInformation getVoucher() {
        return voucher;
    }

    /**
     * Define el valor de la propiedad voucher.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeVoucherInformation }
     *     
     */
    public void setVoucher(TypeVoucherInformation value) {
        this.voucher = value;
    }

    /**
     * Obtiene el valor de la propiedad billingNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingNumber() {
        return billingNumber;
    }

    /**
     * Define el valor de la propiedad billingNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingNumber(String value) {
        this.billingNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad billingReferenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingReferenceNumber() {
        return billingReferenceNumber;
    }

    /**
     * Define el valor de la propiedad billingReferenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingReferenceNumber(String value) {
        this.billingReferenceNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad prePayment.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrePayment() {
        return prePayment;
    }

    /**
     * Define el valor de la propiedad prePayment.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrePayment(String value) {
        this.prePayment = value;
    }

}
