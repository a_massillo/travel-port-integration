
package com.travelport.schema.vehicle_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.TypeFuel;
import com.travelport.schema.common_v43_0.TypeVehicleCategory;
import com.travelport.schema.common_v43_0.TypeVehicleClass;
import com.travelport.schema.common_v43_0.TypeVehicleTransmission;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="Code" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="SupplierCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="PassengerCount" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="NumberOfDoors" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="BagCount" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="Class" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeVehicleClass" /&gt;
 *       &lt;attribute name="Category" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeVehicleCategory" /&gt;
 *       &lt;attribute name="AirConditioning" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="Transmission" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeVehicleTransmission" /&gt;
 *       &lt;attribute name="MakeModel" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="FuelType" type="{http://www.travelport.com/schema/common_v43_0}typeFuel" /&gt;
 *       &lt;attribute name="AcrissVehicleCode"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="4"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="PreferredOption" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "VehicleDetail")
public class VehicleDetail {

    @XmlAttribute(name = "Code", required = true)
    protected String code;
    @XmlAttribute(name = "SupplierCode")
    protected String supplierCode;
    @XmlAttribute(name = "PassengerCount")
    protected String passengerCount;
    @XmlAttribute(name = "NumberOfDoors")
    protected String numberOfDoors;
    @XmlAttribute(name = "BagCount")
    protected String bagCount;
    @XmlAttribute(name = "Class", required = true)
    protected TypeVehicleClass clazz;
    @XmlAttribute(name = "Category", required = true)
    protected TypeVehicleCategory category;
    @XmlAttribute(name = "AirConditioning", required = true)
    protected boolean airConditioning;
    @XmlAttribute(name = "Transmission", required = true)
    protected TypeVehicleTransmission transmission;
    @XmlAttribute(name = "MakeModel", required = true)
    protected String makeModel;
    @XmlAttribute(name = "FuelType")
    protected TypeFuel fuelType;
    @XmlAttribute(name = "AcrissVehicleCode")
    protected String acrissVehicleCode;
    @XmlAttribute(name = "PreferredOption")
    protected Boolean preferredOption;

    /**
     * Obtiene el valor de la propiedad code.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Define el valor de la propiedad code.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Obtiene el valor de la propiedad supplierCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupplierCode() {
        return supplierCode;
    }

    /**
     * Define el valor de la propiedad supplierCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupplierCode(String value) {
        this.supplierCode = value;
    }

    /**
     * Obtiene el valor de la propiedad passengerCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassengerCount() {
        return passengerCount;
    }

    /**
     * Define el valor de la propiedad passengerCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassengerCount(String value) {
        this.passengerCount = value;
    }

    /**
     * Obtiene el valor de la propiedad numberOfDoors.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumberOfDoors() {
        return numberOfDoors;
    }

    /**
     * Define el valor de la propiedad numberOfDoors.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumberOfDoors(String value) {
        this.numberOfDoors = value;
    }

    /**
     * Obtiene el valor de la propiedad bagCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBagCount() {
        return bagCount;
    }

    /**
     * Define el valor de la propiedad bagCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBagCount(String value) {
        this.bagCount = value;
    }

    /**
     * Obtiene el valor de la propiedad clazz.
     * 
     * @return
     *     possible object is
     *     {@link TypeVehicleClass }
     *     
     */
    public TypeVehicleClass getClazz() {
        return clazz;
    }

    /**
     * Define el valor de la propiedad clazz.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeVehicleClass }
     *     
     */
    public void setClazz(TypeVehicleClass value) {
        this.clazz = value;
    }

    /**
     * Obtiene el valor de la propiedad category.
     * 
     * @return
     *     possible object is
     *     {@link TypeVehicleCategory }
     *     
     */
    public TypeVehicleCategory getCategory() {
        return category;
    }

    /**
     * Define el valor de la propiedad category.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeVehicleCategory }
     *     
     */
    public void setCategory(TypeVehicleCategory value) {
        this.category = value;
    }

    /**
     * Obtiene el valor de la propiedad airConditioning.
     * 
     */
    public boolean isAirConditioning() {
        return airConditioning;
    }

    /**
     * Define el valor de la propiedad airConditioning.
     * 
     */
    public void setAirConditioning(boolean value) {
        this.airConditioning = value;
    }

    /**
     * Obtiene el valor de la propiedad transmission.
     * 
     * @return
     *     possible object is
     *     {@link TypeVehicleTransmission }
     *     
     */
    public TypeVehicleTransmission getTransmission() {
        return transmission;
    }

    /**
     * Define el valor de la propiedad transmission.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeVehicleTransmission }
     *     
     */
    public void setTransmission(TypeVehicleTransmission value) {
        this.transmission = value;
    }

    /**
     * Obtiene el valor de la propiedad makeModel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMakeModel() {
        return makeModel;
    }

    /**
     * Define el valor de la propiedad makeModel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMakeModel(String value) {
        this.makeModel = value;
    }

    /**
     * Obtiene el valor de la propiedad fuelType.
     * 
     * @return
     *     possible object is
     *     {@link TypeFuel }
     *     
     */
    public TypeFuel getFuelType() {
        return fuelType;
    }

    /**
     * Define el valor de la propiedad fuelType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeFuel }
     *     
     */
    public void setFuelType(TypeFuel value) {
        this.fuelType = value;
    }

    /**
     * Obtiene el valor de la propiedad acrissVehicleCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcrissVehicleCode() {
        return acrissVehicleCode;
    }

    /**
     * Define el valor de la propiedad acrissVehicleCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcrissVehicleCode(String value) {
        this.acrissVehicleCode = value;
    }

    /**
     * Obtiene el valor de la propiedad preferredOption.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPreferredOption() {
        return preferredOption;
    }

    /**
     * Define el valor de la propiedad preferredOption.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPreferredOption(Boolean value) {
        this.preferredOption = value;
    }

}
