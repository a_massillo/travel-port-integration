
package com.travelport.schema.vehicle_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.TypeDistance;


/**
 * Additional information for extra days or hours.
 * 
 * <p>Clase Java para typeRateInfo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="typeRateInfo"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="RateForPeriod" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="NumberOfPeriods" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="UnlimitedMileage" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="MileageAllowance" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="Units" type="{http://www.travelport.com/schema/common_v43_0}typeDistance" /&gt;
 *       &lt;attribute name="ExtraMileageCharge" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "typeRateInfo")
public class TypeRateInfo {

    @XmlAttribute(name = "RateForPeriod")
    protected String rateForPeriod;
    @XmlAttribute(name = "NumberOfPeriods")
    protected Integer numberOfPeriods;
    @XmlAttribute(name = "UnlimitedMileage")
    protected Boolean unlimitedMileage;
    @XmlAttribute(name = "MileageAllowance")
    protected Integer mileageAllowance;
    @XmlAttribute(name = "Units")
    protected TypeDistance units;
    @XmlAttribute(name = "ExtraMileageCharge")
    protected String extraMileageCharge;

    /**
     * Obtiene el valor de la propiedad rateForPeriod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateForPeriod() {
        return rateForPeriod;
    }

    /**
     * Define el valor de la propiedad rateForPeriod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateForPeriod(String value) {
        this.rateForPeriod = value;
    }

    /**
     * Obtiene el valor de la propiedad numberOfPeriods.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfPeriods() {
        return numberOfPeriods;
    }

    /**
     * Define el valor de la propiedad numberOfPeriods.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfPeriods(Integer value) {
        this.numberOfPeriods = value;
    }

    /**
     * Obtiene el valor de la propiedad unlimitedMileage.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUnlimitedMileage() {
        return unlimitedMileage;
    }

    /**
     * Define el valor de la propiedad unlimitedMileage.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUnlimitedMileage(Boolean value) {
        this.unlimitedMileage = value;
    }

    /**
     * Obtiene el valor de la propiedad mileageAllowance.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMileageAllowance() {
        return mileageAllowance;
    }

    /**
     * Define el valor de la propiedad mileageAllowance.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMileageAllowance(Integer value) {
        this.mileageAllowance = value;
    }

    /**
     * Obtiene el valor de la propiedad units.
     * 
     * @return
     *     possible object is
     *     {@link TypeDistance }
     *     
     */
    public TypeDistance getUnits() {
        return units;
    }

    /**
     * Define el valor de la propiedad units.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeDistance }
     *     
     */
    public void setUnits(TypeDistance value) {
        this.units = value;
    }

    /**
     * Obtiene el valor de la propiedad extraMileageCharge.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtraMileageCharge() {
        return extraMileageCharge;
    }

    /**
     * Define el valor de la propiedad extraMileageCharge.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtraMileageCharge(String value) {
        this.extraMileageCharge = value;
    }

}
