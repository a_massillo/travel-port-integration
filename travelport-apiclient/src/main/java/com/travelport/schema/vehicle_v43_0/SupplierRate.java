
package com.travelport.schema.vehicle_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/vehicle_v43_0}typeVehicleRates"&gt;
 *       &lt;attribute name="DiscountAmount" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="MandatoryChargeTotal" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="ApproximateTotal" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "SupplierRate")
public class SupplierRate
    extends TypeVehicleRates
{

    @XmlAttribute(name = "DiscountAmount")
    protected String discountAmount;
    @XmlAttribute(name = "MandatoryChargeTotal")
    protected String mandatoryChargeTotal;
    @XmlAttribute(name = "ApproximateTotal")
    protected String approximateTotal;

    /**
     * Obtiene el valor de la propiedad discountAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiscountAmount() {
        return discountAmount;
    }

    /**
     * Define el valor de la propiedad discountAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiscountAmount(String value) {
        this.discountAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad mandatoryChargeTotal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMandatoryChargeTotal() {
        return mandatoryChargeTotal;
    }

    /**
     * Define el valor de la propiedad mandatoryChargeTotal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMandatoryChargeTotal(String value) {
        this.mandatoryChargeTotal = value;
    }

    /**
     * Obtiene el valor de la propiedad approximateTotal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApproximateTotal() {
        return approximateTotal;
    }

    /**
     * Define el valor de la propiedad approximateTotal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApproximateTotal(String value) {
        this.approximateTotal = value;
    }

}
