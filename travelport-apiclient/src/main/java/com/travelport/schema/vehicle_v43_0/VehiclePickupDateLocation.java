
package com.travelport.schema.vehicle_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.travelport.schema.common_v43_0.TypeVehicleLocation;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="PickupDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *       &lt;attribute name="PickupLocation" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *       &lt;attribute name="PickupLocationType" type="{http://www.travelport.com/schema/common_v43_0}typeVehicleLocation" /&gt;
 *       &lt;attribute name="PickupLocationNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "VehiclePickupDateLocation")
public class VehiclePickupDateLocation {

    @XmlAttribute(name = "PickupDateTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar pickupDateTime;
    @XmlAttribute(name = "PickupLocation")
    protected String pickupLocation;
    @XmlAttribute(name = "PickupLocationType")
    protected TypeVehicleLocation pickupLocationType;
    @XmlAttribute(name = "PickupLocationNumber")
    protected String pickupLocationNumber;

    /**
     * Obtiene el valor de la propiedad pickupDateTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPickupDateTime() {
        return pickupDateTime;
    }

    /**
     * Define el valor de la propiedad pickupDateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPickupDateTime(XMLGregorianCalendar value) {
        this.pickupDateTime = value;
    }

    /**
     * Obtiene el valor de la propiedad pickupLocation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPickupLocation() {
        return pickupLocation;
    }

    /**
     * Define el valor de la propiedad pickupLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPickupLocation(String value) {
        this.pickupLocation = value;
    }

    /**
     * Obtiene el valor de la propiedad pickupLocationType.
     * 
     * @return
     *     possible object is
     *     {@link TypeVehicleLocation }
     *     
     */
    public TypeVehicleLocation getPickupLocationType() {
        return pickupLocationType;
    }

    /**
     * Define el valor de la propiedad pickupLocationType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeVehicleLocation }
     *     
     */
    public void setPickupLocationType(TypeVehicleLocation value) {
        this.pickupLocationType = value;
    }

    /**
     * Obtiene el valor de la propiedad pickupLocationNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPickupLocationNumber() {
        return pickupLocationNumber;
    }

    /**
     * Define el valor de la propiedad pickupLocationNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPickupLocationNumber(String value) {
        this.pickupLocationNumber = value;
    }

}
