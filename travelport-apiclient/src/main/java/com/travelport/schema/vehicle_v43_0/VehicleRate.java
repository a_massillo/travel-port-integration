
package com.travelport.schema.vehicle_v43_0;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.TypeDistance;
import com.travelport.schema.common_v43_0.TypeRateCategory;
import com.travelport.schema.common_v43_0.TypeRateGuarantee;
import com.travelport.schema.common_v43_0.TypeRateTimePeriod;
import com.travelport.schema.common_v43_0.TypeTrinary;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/vehicle_v43_0}SupplierRate" minOccurs="0"/&gt;
 *         &lt;element name="RateVariance" type="{http://www.travelport.com/schema/vehicle_v43_0}typeRateVariance" minOccurs="0"/&gt;
 *         &lt;element name="ApproximateRate" type="{http://www.travelport.com/schema/vehicle_v43_0}typeVehicleRates" minOccurs="0"/&gt;
 *         &lt;element name="VehicleRateDescription" type="{http://www.travelport.com/schema/vehicle_v43_0}typeVehicleRateDescription" maxOccurs="99" minOccurs="0"/&gt;
 *         &lt;element name="RateHostIndicator" type="{http://www.travelport.com/schema/vehicle_v43_0}typeRateHostIndicator" minOccurs="0"/&gt;
 *         &lt;element name="HourlyLateCharge" type="{http://www.travelport.com/schema/vehicle_v43_0}typeRateInfo" minOccurs="0"/&gt;
 *         &lt;element name="DailyLateCharge" type="{http://www.travelport.com/schema/vehicle_v43_0}typeRateInfo" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/vehicle_v43_0}PricedEquip" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/vehicle_v43_0}RateInclusions" minOccurs="0"/&gt;
 *         &lt;element name="WeeklyLateCharge" type="{http://www.travelport.com/schema/vehicle_v43_0}typeRateInfo" minOccurs="0"/&gt;
 *         &lt;element name="PrintText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="RatePeriod" type="{http://www.travelport.com/schema/common_v43_0}typeRateTimePeriod" /&gt;
 *       &lt;attribute name="NumberOfPeriods" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="UnlimitedMileage" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="MileageAllowance" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="Units" type="{http://www.travelport.com/schema/common_v43_0}typeDistance" /&gt;
 *       &lt;attribute name="RateSource" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="RateAvailability" type="{http://www.travelport.com/schema/vehicle_v43_0}typeRateAvailability" /&gt;
 *       &lt;attribute name="RequiredCharges" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="RateCode" type="{http://www.travelport.com/schema/common_v43_0}typeRateCode" /&gt;
 *       &lt;attribute name="RequestedRateCodeApplied" type="{http://www.travelport.com/schema/common_v43_0}typeTrinary" /&gt;
 *       &lt;attribute name="RateCategory" type="{http://www.travelport.com/schema/common_v43_0}typeRateCategory" /&gt;
 *       &lt;attribute name="DiscountNumber" type="{http://www.travelport.com/schema/common_v43_0}typeDiscountNumber" /&gt;
 *       &lt;attribute name="DiscountNumberApplied" type="{http://www.travelport.com/schema/common_v43_0}typeTrinary" /&gt;
 *       &lt;attribute name="VendorCode" type="{http://www.travelport.com/schema/common_v43_0}typeSupplierCode" /&gt;
 *       &lt;attribute name="RateGuaranteed" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="RateCodePeriod" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="PromotionalCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="PromotionalCodeApplied" type="{http://www.travelport.com/schema/common_v43_0}typeTrinary" /&gt;
 *       &lt;attribute name="TourCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="TourCodeApplied" type="{http://www.travelport.com/schema/common_v43_0}typeTrinary" /&gt;
 *       &lt;attribute name="RateGuaranteeType" type="{http://www.travelport.com/schema/common_v43_0}typeRateGuarantee" /&gt;
 *       &lt;attribute name="RequiredPayment"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;enumeration value="Guarantee"/&gt;
 *             &lt;enumeration value="Deposit"/&gt;
 *             &lt;enumeration value="PrePayment"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="DropOffChargesIncluded" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="CorporateRate" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="AdvancedBooking" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="RentalRestriction" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="FlightRestriction" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="CardNumber" type="{http://www.travelport.com/schema/common_v43_0}typeCardNumber" /&gt;
 *       &lt;attribute name="CardNumberApplied" type="{http://www.travelport.com/schema/common_v43_0}typeTrinary" /&gt;
 *       &lt;attribute name="RateQualifierInd" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "supplierRate",
    "rateVariance",
    "approximateRate",
    "vehicleRateDescription",
    "rateHostIndicator",
    "hourlyLateCharge",
    "dailyLateCharge",
    "pricedEquip",
    "rateInclusions",
    "weeklyLateCharge",
    "printText"
})
@XmlRootElement(name = "VehicleRate")
public class VehicleRate {

    @XmlElement(name = "SupplierRate")
    protected SupplierRate supplierRate;
    @XmlElement(name = "RateVariance")
    protected TypeRateVariance rateVariance;
    @XmlElement(name = "ApproximateRate")
    protected TypeVehicleRates approximateRate;
    @XmlElement(name = "VehicleRateDescription")
    protected List<TypeVehicleRateDescription> vehicleRateDescription;
    @XmlElement(name = "RateHostIndicator")
    protected TypeRateHostIndicator rateHostIndicator;
    @XmlElement(name = "HourlyLateCharge")
    protected TypeRateInfo hourlyLateCharge;
    @XmlElement(name = "DailyLateCharge")
    protected TypeRateInfo dailyLateCharge;
    @XmlElement(name = "PricedEquip")
    protected List<PricedEquip> pricedEquip;
    @XmlElement(name = "RateInclusions")
    protected RateInclusions rateInclusions;
    @XmlElement(name = "WeeklyLateCharge")
    protected TypeRateInfo weeklyLateCharge;
    @XmlElement(name = "PrintText")
    protected String printText;
    @XmlAttribute(name = "RatePeriod")
    protected TypeRateTimePeriod ratePeriod;
    @XmlAttribute(name = "NumberOfPeriods")
    protected Integer numberOfPeriods;
    @XmlAttribute(name = "UnlimitedMileage")
    protected Boolean unlimitedMileage;
    @XmlAttribute(name = "MileageAllowance")
    protected Integer mileageAllowance;
    @XmlAttribute(name = "Units")
    protected TypeDistance units;
    @XmlAttribute(name = "RateSource")
    protected String rateSource;
    @XmlAttribute(name = "RateAvailability")
    protected TypeRateAvailability rateAvailability;
    @XmlAttribute(name = "RequiredCharges")
    protected String requiredCharges;
    @XmlAttribute(name = "RateCode")
    protected String rateCode;
    @XmlAttribute(name = "RequestedRateCodeApplied")
    protected TypeTrinary requestedRateCodeApplied;
    @XmlAttribute(name = "RateCategory")
    protected TypeRateCategory rateCategory;
    @XmlAttribute(name = "DiscountNumber")
    protected String discountNumber;
    @XmlAttribute(name = "DiscountNumberApplied")
    protected TypeTrinary discountNumberApplied;
    @XmlAttribute(name = "VendorCode")
    protected String vendorCode;
    @XmlAttribute(name = "RateGuaranteed")
    protected Boolean rateGuaranteed;
    @XmlAttribute(name = "RateCodePeriod")
    protected String rateCodePeriod;
    @XmlAttribute(name = "PromotionalCode")
    protected String promotionalCode;
    @XmlAttribute(name = "PromotionalCodeApplied")
    protected TypeTrinary promotionalCodeApplied;
    @XmlAttribute(name = "TourCode")
    protected String tourCode;
    @XmlAttribute(name = "TourCodeApplied")
    protected TypeTrinary tourCodeApplied;
    @XmlAttribute(name = "RateGuaranteeType")
    protected TypeRateGuarantee rateGuaranteeType;
    @XmlAttribute(name = "RequiredPayment")
    protected String requiredPayment;
    @XmlAttribute(name = "DropOffChargesIncluded")
    protected Boolean dropOffChargesIncluded;
    @XmlAttribute(name = "CorporateRate")
    protected Boolean corporateRate;
    @XmlAttribute(name = "AdvancedBooking")
    protected String advancedBooking;
    @XmlAttribute(name = "RentalRestriction")
    protected Boolean rentalRestriction;
    @XmlAttribute(name = "FlightRestriction")
    protected Boolean flightRestriction;
    @XmlAttribute(name = "CardNumber")
    protected String cardNumber;
    @XmlAttribute(name = "CardNumberApplied")
    protected TypeTrinary cardNumberApplied;
    @XmlAttribute(name = "RateQualifierInd")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger rateQualifierInd;

    /**
     * Obtiene el valor de la propiedad supplierRate.
     * 
     * @return
     *     possible object is
     *     {@link SupplierRate }
     *     
     */
    public SupplierRate getSupplierRate() {
        return supplierRate;
    }

    /**
     * Define el valor de la propiedad supplierRate.
     * 
     * @param value
     *     allowed object is
     *     {@link SupplierRate }
     *     
     */
    public void setSupplierRate(SupplierRate value) {
        this.supplierRate = value;
    }

    /**
     * Obtiene el valor de la propiedad rateVariance.
     * 
     * @return
     *     possible object is
     *     {@link TypeRateVariance }
     *     
     */
    public TypeRateVariance getRateVariance() {
        return rateVariance;
    }

    /**
     * Define el valor de la propiedad rateVariance.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeRateVariance }
     *     
     */
    public void setRateVariance(TypeRateVariance value) {
        this.rateVariance = value;
    }

    /**
     * Obtiene el valor de la propiedad approximateRate.
     * 
     * @return
     *     possible object is
     *     {@link TypeVehicleRates }
     *     
     */
    public TypeVehicleRates getApproximateRate() {
        return approximateRate;
    }

    /**
     * Define el valor de la propiedad approximateRate.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeVehicleRates }
     *     
     */
    public void setApproximateRate(TypeVehicleRates value) {
        this.approximateRate = value;
    }

    /**
     * Gets the value of the vehicleRateDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vehicleRateDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVehicleRateDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TypeVehicleRateDescription }
     * 
     * 
     */
    public List<TypeVehicleRateDescription> getVehicleRateDescription() {
        if (vehicleRateDescription == null) {
            vehicleRateDescription = new ArrayList<TypeVehicleRateDescription>();
        }
        return this.vehicleRateDescription;
    }

    /**
     * Obtiene el valor de la propiedad rateHostIndicator.
     * 
     * @return
     *     possible object is
     *     {@link TypeRateHostIndicator }
     *     
     */
    public TypeRateHostIndicator getRateHostIndicator() {
        return rateHostIndicator;
    }

    /**
     * Define el valor de la propiedad rateHostIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeRateHostIndicator }
     *     
     */
    public void setRateHostIndicator(TypeRateHostIndicator value) {
        this.rateHostIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad hourlyLateCharge.
     * 
     * @return
     *     possible object is
     *     {@link TypeRateInfo }
     *     
     */
    public TypeRateInfo getHourlyLateCharge() {
        return hourlyLateCharge;
    }

    /**
     * Define el valor de la propiedad hourlyLateCharge.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeRateInfo }
     *     
     */
    public void setHourlyLateCharge(TypeRateInfo value) {
        this.hourlyLateCharge = value;
    }

    /**
     * Obtiene el valor de la propiedad dailyLateCharge.
     * 
     * @return
     *     possible object is
     *     {@link TypeRateInfo }
     *     
     */
    public TypeRateInfo getDailyLateCharge() {
        return dailyLateCharge;
    }

    /**
     * Define el valor de la propiedad dailyLateCharge.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeRateInfo }
     *     
     */
    public void setDailyLateCharge(TypeRateInfo value) {
        this.dailyLateCharge = value;
    }

    /**
     * Gets the value of the pricedEquip property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pricedEquip property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPricedEquip().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PricedEquip }
     * 
     * 
     */
    public List<PricedEquip> getPricedEquip() {
        if (pricedEquip == null) {
            pricedEquip = new ArrayList<PricedEquip>();
        }
        return this.pricedEquip;
    }

    /**
     * Obtiene el valor de la propiedad rateInclusions.
     * 
     * @return
     *     possible object is
     *     {@link RateInclusions }
     *     
     */
    public RateInclusions getRateInclusions() {
        return rateInclusions;
    }

    /**
     * Define el valor de la propiedad rateInclusions.
     * 
     * @param value
     *     allowed object is
     *     {@link RateInclusions }
     *     
     */
    public void setRateInclusions(RateInclusions value) {
        this.rateInclusions = value;
    }

    /**
     * Obtiene el valor de la propiedad weeklyLateCharge.
     * 
     * @return
     *     possible object is
     *     {@link TypeRateInfo }
     *     
     */
    public TypeRateInfo getWeeklyLateCharge() {
        return weeklyLateCharge;
    }

    /**
     * Define el valor de la propiedad weeklyLateCharge.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeRateInfo }
     *     
     */
    public void setWeeklyLateCharge(TypeRateInfo value) {
        this.weeklyLateCharge = value;
    }

    /**
     * Obtiene el valor de la propiedad printText.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrintText() {
        return printText;
    }

    /**
     * Define el valor de la propiedad printText.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrintText(String value) {
        this.printText = value;
    }

    /**
     * Obtiene el valor de la propiedad ratePeriod.
     * 
     * @return
     *     possible object is
     *     {@link TypeRateTimePeriod }
     *     
     */
    public TypeRateTimePeriod getRatePeriod() {
        return ratePeriod;
    }

    /**
     * Define el valor de la propiedad ratePeriod.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeRateTimePeriod }
     *     
     */
    public void setRatePeriod(TypeRateTimePeriod value) {
        this.ratePeriod = value;
    }

    /**
     * Obtiene el valor de la propiedad numberOfPeriods.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfPeriods() {
        return numberOfPeriods;
    }

    /**
     * Define el valor de la propiedad numberOfPeriods.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfPeriods(Integer value) {
        this.numberOfPeriods = value;
    }

    /**
     * Obtiene el valor de la propiedad unlimitedMileage.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUnlimitedMileage() {
        return unlimitedMileage;
    }

    /**
     * Define el valor de la propiedad unlimitedMileage.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUnlimitedMileage(Boolean value) {
        this.unlimitedMileage = value;
    }

    /**
     * Obtiene el valor de la propiedad mileageAllowance.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMileageAllowance() {
        return mileageAllowance;
    }

    /**
     * Define el valor de la propiedad mileageAllowance.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMileageAllowance(Integer value) {
        this.mileageAllowance = value;
    }

    /**
     * Obtiene el valor de la propiedad units.
     * 
     * @return
     *     possible object is
     *     {@link TypeDistance }
     *     
     */
    public TypeDistance getUnits() {
        return units;
    }

    /**
     * Define el valor de la propiedad units.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeDistance }
     *     
     */
    public void setUnits(TypeDistance value) {
        this.units = value;
    }

    /**
     * Obtiene el valor de la propiedad rateSource.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateSource() {
        return rateSource;
    }

    /**
     * Define el valor de la propiedad rateSource.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateSource(String value) {
        this.rateSource = value;
    }

    /**
     * Obtiene el valor de la propiedad rateAvailability.
     * 
     * @return
     *     possible object is
     *     {@link TypeRateAvailability }
     *     
     */
    public TypeRateAvailability getRateAvailability() {
        return rateAvailability;
    }

    /**
     * Define el valor de la propiedad rateAvailability.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeRateAvailability }
     *     
     */
    public void setRateAvailability(TypeRateAvailability value) {
        this.rateAvailability = value;
    }

    /**
     * Obtiene el valor de la propiedad requiredCharges.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequiredCharges() {
        return requiredCharges;
    }

    /**
     * Define el valor de la propiedad requiredCharges.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequiredCharges(String value) {
        this.requiredCharges = value;
    }

    /**
     * Obtiene el valor de la propiedad rateCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateCode() {
        return rateCode;
    }

    /**
     * Define el valor de la propiedad rateCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateCode(String value) {
        this.rateCode = value;
    }

    /**
     * Obtiene el valor de la propiedad requestedRateCodeApplied.
     * 
     * @return
     *     possible object is
     *     {@link TypeTrinary }
     *     
     */
    public TypeTrinary getRequestedRateCodeApplied() {
        return requestedRateCodeApplied;
    }

    /**
     * Define el valor de la propiedad requestedRateCodeApplied.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeTrinary }
     *     
     */
    public void setRequestedRateCodeApplied(TypeTrinary value) {
        this.requestedRateCodeApplied = value;
    }

    /**
     * Obtiene el valor de la propiedad rateCategory.
     * 
     * @return
     *     possible object is
     *     {@link TypeRateCategory }
     *     
     */
    public TypeRateCategory getRateCategory() {
        return rateCategory;
    }

    /**
     * Define el valor de la propiedad rateCategory.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeRateCategory }
     *     
     */
    public void setRateCategory(TypeRateCategory value) {
        this.rateCategory = value;
    }

    /**
     * Obtiene el valor de la propiedad discountNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiscountNumber() {
        return discountNumber;
    }

    /**
     * Define el valor de la propiedad discountNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiscountNumber(String value) {
        this.discountNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad discountNumberApplied.
     * 
     * @return
     *     possible object is
     *     {@link TypeTrinary }
     *     
     */
    public TypeTrinary getDiscountNumberApplied() {
        return discountNumberApplied;
    }

    /**
     * Define el valor de la propiedad discountNumberApplied.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeTrinary }
     *     
     */
    public void setDiscountNumberApplied(TypeTrinary value) {
        this.discountNumberApplied = value;
    }

    /**
     * Obtiene el valor de la propiedad vendorCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorCode() {
        return vendorCode;
    }

    /**
     * Define el valor de la propiedad vendorCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorCode(String value) {
        this.vendorCode = value;
    }

    /**
     * Obtiene el valor de la propiedad rateGuaranteed.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isRateGuaranteed() {
        if (rateGuaranteed == null) {
            return false;
        } else {
            return rateGuaranteed;
        }
    }

    /**
     * Define el valor de la propiedad rateGuaranteed.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRateGuaranteed(Boolean value) {
        this.rateGuaranteed = value;
    }

    /**
     * Obtiene el valor de la propiedad rateCodePeriod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateCodePeriod() {
        return rateCodePeriod;
    }

    /**
     * Define el valor de la propiedad rateCodePeriod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateCodePeriod(String value) {
        this.rateCodePeriod = value;
    }

    /**
     * Obtiene el valor de la propiedad promotionalCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPromotionalCode() {
        return promotionalCode;
    }

    /**
     * Define el valor de la propiedad promotionalCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPromotionalCode(String value) {
        this.promotionalCode = value;
    }

    /**
     * Obtiene el valor de la propiedad promotionalCodeApplied.
     * 
     * @return
     *     possible object is
     *     {@link TypeTrinary }
     *     
     */
    public TypeTrinary getPromotionalCodeApplied() {
        return promotionalCodeApplied;
    }

    /**
     * Define el valor de la propiedad promotionalCodeApplied.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeTrinary }
     *     
     */
    public void setPromotionalCodeApplied(TypeTrinary value) {
        this.promotionalCodeApplied = value;
    }

    /**
     * Obtiene el valor de la propiedad tourCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTourCode() {
        return tourCode;
    }

    /**
     * Define el valor de la propiedad tourCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTourCode(String value) {
        this.tourCode = value;
    }

    /**
     * Obtiene el valor de la propiedad tourCodeApplied.
     * 
     * @return
     *     possible object is
     *     {@link TypeTrinary }
     *     
     */
    public TypeTrinary getTourCodeApplied() {
        return tourCodeApplied;
    }

    /**
     * Define el valor de la propiedad tourCodeApplied.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeTrinary }
     *     
     */
    public void setTourCodeApplied(TypeTrinary value) {
        this.tourCodeApplied = value;
    }

    /**
     * Obtiene el valor de la propiedad rateGuaranteeType.
     * 
     * @return
     *     possible object is
     *     {@link TypeRateGuarantee }
     *     
     */
    public TypeRateGuarantee getRateGuaranteeType() {
        return rateGuaranteeType;
    }

    /**
     * Define el valor de la propiedad rateGuaranteeType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeRateGuarantee }
     *     
     */
    public void setRateGuaranteeType(TypeRateGuarantee value) {
        this.rateGuaranteeType = value;
    }

    /**
     * Obtiene el valor de la propiedad requiredPayment.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequiredPayment() {
        return requiredPayment;
    }

    /**
     * Define el valor de la propiedad requiredPayment.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequiredPayment(String value) {
        this.requiredPayment = value;
    }

    /**
     * Obtiene el valor de la propiedad dropOffChargesIncluded.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDropOffChargesIncluded() {
        return dropOffChargesIncluded;
    }

    /**
     * Define el valor de la propiedad dropOffChargesIncluded.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDropOffChargesIncluded(Boolean value) {
        this.dropOffChargesIncluded = value;
    }

    /**
     * Obtiene el valor de la propiedad corporateRate.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCorporateRate() {
        return corporateRate;
    }

    /**
     * Define el valor de la propiedad corporateRate.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCorporateRate(Boolean value) {
        this.corporateRate = value;
    }

    /**
     * Obtiene el valor de la propiedad advancedBooking.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdvancedBooking() {
        return advancedBooking;
    }

    /**
     * Define el valor de la propiedad advancedBooking.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdvancedBooking(String value) {
        this.advancedBooking = value;
    }

    /**
     * Obtiene el valor de la propiedad rentalRestriction.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRentalRestriction() {
        return rentalRestriction;
    }

    /**
     * Define el valor de la propiedad rentalRestriction.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRentalRestriction(Boolean value) {
        this.rentalRestriction = value;
    }

    /**
     * Obtiene el valor de la propiedad flightRestriction.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFlightRestriction() {
        return flightRestriction;
    }

    /**
     * Define el valor de la propiedad flightRestriction.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFlightRestriction(Boolean value) {
        this.flightRestriction = value;
    }

    /**
     * Obtiene el valor de la propiedad cardNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * Define el valor de la propiedad cardNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNumber(String value) {
        this.cardNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad cardNumberApplied.
     * 
     * @return
     *     possible object is
     *     {@link TypeTrinary }
     *     
     */
    public TypeTrinary getCardNumberApplied() {
        return cardNumberApplied;
    }

    /**
     * Define el valor de la propiedad cardNumberApplied.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeTrinary }
     *     
     */
    public void setCardNumberApplied(TypeTrinary value) {
        this.cardNumberApplied = value;
    }

    /**
     * Obtiene el valor de la propiedad rateQualifierInd.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRateQualifierInd() {
        return rateQualifierInd;
    }

    /**
     * Define el valor de la propiedad rateQualifierInd.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRateQualifierInd(BigInteger value) {
        this.rateQualifierInd = value;
    }

}
