
package com.travelport.schema.vehicle_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.CoordinateLocation;
import com.travelport.schema.common_v43_0.Distance;
import com.travelport.schema.common_v43_0.VendorLocation;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}VendorLocation"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Distance" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}CoordinateLocation" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/vehicle_v43_0}LocationInformation"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vendorLocation",
    "distance",
    "coordinateLocation",
    "locationInformation"
})
@XmlRootElement(name = "VehicleLocation")
public class VehicleLocation {

    @XmlElement(name = "VendorLocation", namespace = "http://www.travelport.com/schema/common_v43_0", required = true)
    protected VendorLocation vendorLocation;
    @XmlElement(name = "Distance", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected Distance distance;
    @XmlElement(name = "CoordinateLocation", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected CoordinateLocation coordinateLocation;
    @XmlElement(name = "LocationInformation", required = true)
    protected LocationInformation locationInformation;

    /**
     * Obtiene el valor de la propiedad vendorLocation.
     * 
     * @return
     *     possible object is
     *     {@link VendorLocation }
     *     
     */
    public VendorLocation getVendorLocation() {
        return vendorLocation;
    }

    /**
     * Define el valor de la propiedad vendorLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link VendorLocation }
     *     
     */
    public void setVendorLocation(VendorLocation value) {
        this.vendorLocation = value;
    }

    /**
     * Obtiene el valor de la propiedad distance.
     * 
     * @return
     *     possible object is
     *     {@link Distance }
     *     
     */
    public Distance getDistance() {
        return distance;
    }

    /**
     * Define el valor de la propiedad distance.
     * 
     * @param value
     *     allowed object is
     *     {@link Distance }
     *     
     */
    public void setDistance(Distance value) {
        this.distance = value;
    }

    /**
     * Obtiene el valor de la propiedad coordinateLocation.
     * 
     * @return
     *     possible object is
     *     {@link CoordinateLocation }
     *     
     */
    public CoordinateLocation getCoordinateLocation() {
        return coordinateLocation;
    }

    /**
     * Define el valor de la propiedad coordinateLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link CoordinateLocation }
     *     
     */
    public void setCoordinateLocation(CoordinateLocation value) {
        this.coordinateLocation = value;
    }

    /**
     * Obtiene el valor de la propiedad locationInformation.
     * 
     * @return
     *     possible object is
     *     {@link LocationInformation }
     *     
     */
    public LocationInformation getLocationInformation() {
        return locationInformation;
    }

    /**
     * Define el valor de la propiedad locationInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationInformation }
     *     
     */
    public void setLocationInformation(LocationInformation value) {
        this.locationInformation = value;
    }

}
