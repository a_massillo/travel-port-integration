
package com.travelport.schema.vehicle_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="Amount" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="RatePeriod" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="IncludedInEstTotalInd" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "Charge")
public class Charge {

    @XmlAttribute(name = "Amount", required = true)
    protected String amount;
    @XmlAttribute(name = "RatePeriod", required = true)
    protected String ratePeriod;
    @XmlAttribute(name = "IncludedInEstTotalInd", required = true)
    protected boolean includedInEstTotalInd;

    /**
     * Obtiene el valor de la propiedad amount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmount() {
        return amount;
    }

    /**
     * Define el valor de la propiedad amount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmount(String value) {
        this.amount = value;
    }

    /**
     * Obtiene el valor de la propiedad ratePeriod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRatePeriod() {
        return ratePeriod;
    }

    /**
     * Define el valor de la propiedad ratePeriod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRatePeriod(String value) {
        this.ratePeriod = value;
    }

    /**
     * Obtiene el valor de la propiedad includedInEstTotalInd.
     * 
     */
    public boolean isIncludedInEstTotalInd() {
        return includedInEstTotalInd;
    }

    /**
     * Define el valor de la propiedad includedInEstTotalInd.
     * 
     */
    public void setIncludedInEstTotalInd(boolean value) {
        this.includedInEstTotalInd = value;
    }

}
