
package com.travelport.schema.vehicle_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * Host-specific info for vendors
 * 
 * <p>Clase Java para typeRateHostIndicator complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="typeRateHostIndicator"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="InventoryToken" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="RateToken" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "typeRateHostIndicator")
public class TypeRateHostIndicator {

    @XmlAttribute(name = "InventoryToken")
    protected String inventoryToken;
    @XmlAttribute(name = "RateToken")
    protected String rateToken;

    /**
     * Obtiene el valor de la propiedad inventoryToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInventoryToken() {
        return inventoryToken;
    }

    /**
     * Define el valor de la propiedad inventoryToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInventoryToken(String value) {
        this.inventoryToken = value;
    }

    /**
     * Obtiene el valor de la propiedad rateToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateToken() {
        return rateToken;
    }

    /**
     * Define el valor de la propiedad rateToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateToken(String value) {
        this.rateToken = value;
    }

}
