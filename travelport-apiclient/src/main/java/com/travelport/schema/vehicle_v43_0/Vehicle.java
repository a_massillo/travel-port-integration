
package com.travelport.schema.vehicle_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.TypeDoorCount;
import com.travelport.schema.common_v43_0.TypeFuel;
import com.travelport.schema.common_v43_0.TypePolicyCodesList;
import com.travelport.schema.common_v43_0.TypeVehicleCategory;
import com.travelport.schema.common_v43_0.TypeVehicleClass;
import com.travelport.schema.common_v43_0.TypeVehicleTransmission;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PolicyCodesList" type="{http://www.travelport.com/schema/common_v43_0}typePolicyCodesList" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/vehicle_v43_0}VehicleRate" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attGroup ref="{http://www.travelport.com/schema/common_v43_0}attrPolicyMarking"/&gt;
 *       &lt;attribute name="VendorCode" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeSupplierCode" /&gt;
 *       &lt;attribute name="AirConditioning" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="TransmissionType" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeVehicleTransmission" /&gt;
 *       &lt;attribute name="VehicleClass" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeVehicleClass" /&gt;
 *       &lt;attribute name="Category" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeVehicleCategory" /&gt;
 *       &lt;attribute name="Description" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="DoorCount" type="{http://www.travelport.com/schema/common_v43_0}typeDoorCount" /&gt;
 *       &lt;attribute name="Location" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="CounterLocationCode"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="4"/&gt;
 *             &lt;maxLength value="4"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="VendorLocationKey" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="VendorName"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="20"/&gt;
 *             &lt;minLength value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="AlternateVendor" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="FuelType" type="{http://www.travelport.com/schema/common_v43_0}typeFuel" /&gt;
 *       &lt;attribute name="AcrissVehicleCode"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="4"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Key" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="ReturnAtPickup" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "policyCodesList",
    "vehicleRate"
})
@XmlRootElement(name = "Vehicle")
public class Vehicle {

    @XmlElement(name = "PolicyCodesList")
    protected TypePolicyCodesList policyCodesList;
    @XmlElement(name = "VehicleRate")
    protected VehicleRate vehicleRate;
    @XmlAttribute(name = "VendorCode", required = true)
    protected String vendorCode;
    @XmlAttribute(name = "AirConditioning", required = true)
    protected boolean airConditioning;
    @XmlAttribute(name = "TransmissionType", required = true)
    protected TypeVehicleTransmission transmissionType;
    @XmlAttribute(name = "VehicleClass", required = true)
    protected TypeVehicleClass vehicleClass;
    @XmlAttribute(name = "Category", required = true)
    protected TypeVehicleCategory category;
    @XmlAttribute(name = "Description")
    protected String description;
    @XmlAttribute(name = "DoorCount")
    protected TypeDoorCount doorCount;
    @XmlAttribute(name = "Location")
    protected String location;
    @XmlAttribute(name = "CounterLocationCode")
    protected String counterLocationCode;
    @XmlAttribute(name = "VendorLocationKey")
    protected String vendorLocationKey;
    @XmlAttribute(name = "VendorName")
    protected String vendorName;
    @XmlAttribute(name = "AlternateVendor")
    protected String alternateVendor;
    @XmlAttribute(name = "FuelType")
    protected TypeFuel fuelType;
    @XmlAttribute(name = "AcrissVehicleCode")
    protected String acrissVehicleCode;
    @XmlAttribute(name = "Key")
    protected String key;
    @XmlAttribute(name = "ReturnAtPickup")
    protected Boolean returnAtPickup;
    @XmlAttribute(name = "InPolicy")
    protected Boolean inPolicy;
    @XmlAttribute(name = "PolicyCode")
    protected Integer policyCode;
    @XmlAttribute(name = "PreferredOption")
    protected Boolean preferredOption;

    /**
     * Obtiene el valor de la propiedad policyCodesList.
     * 
     * @return
     *     possible object is
     *     {@link TypePolicyCodesList }
     *     
     */
    public TypePolicyCodesList getPolicyCodesList() {
        return policyCodesList;
    }

    /**
     * Define el valor de la propiedad policyCodesList.
     * 
     * @param value
     *     allowed object is
     *     {@link TypePolicyCodesList }
     *     
     */
    public void setPolicyCodesList(TypePolicyCodesList value) {
        this.policyCodesList = value;
    }

    /**
     * Obtiene el valor de la propiedad vehicleRate.
     * 
     * @return
     *     possible object is
     *     {@link VehicleRate }
     *     
     */
    public VehicleRate getVehicleRate() {
        return vehicleRate;
    }

    /**
     * Define el valor de la propiedad vehicleRate.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleRate }
     *     
     */
    public void setVehicleRate(VehicleRate value) {
        this.vehicleRate = value;
    }

    /**
     * Obtiene el valor de la propiedad vendorCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorCode() {
        return vendorCode;
    }

    /**
     * Define el valor de la propiedad vendorCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorCode(String value) {
        this.vendorCode = value;
    }

    /**
     * Obtiene el valor de la propiedad airConditioning.
     * 
     */
    public boolean isAirConditioning() {
        return airConditioning;
    }

    /**
     * Define el valor de la propiedad airConditioning.
     * 
     */
    public void setAirConditioning(boolean value) {
        this.airConditioning = value;
    }

    /**
     * Obtiene el valor de la propiedad transmissionType.
     * 
     * @return
     *     possible object is
     *     {@link TypeVehicleTransmission }
     *     
     */
    public TypeVehicleTransmission getTransmissionType() {
        return transmissionType;
    }

    /**
     * Define el valor de la propiedad transmissionType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeVehicleTransmission }
     *     
     */
    public void setTransmissionType(TypeVehicleTransmission value) {
        this.transmissionType = value;
    }

    /**
     * Obtiene el valor de la propiedad vehicleClass.
     * 
     * @return
     *     possible object is
     *     {@link TypeVehicleClass }
     *     
     */
    public TypeVehicleClass getVehicleClass() {
        return vehicleClass;
    }

    /**
     * Define el valor de la propiedad vehicleClass.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeVehicleClass }
     *     
     */
    public void setVehicleClass(TypeVehicleClass value) {
        this.vehicleClass = value;
    }

    /**
     * Obtiene el valor de la propiedad category.
     * 
     * @return
     *     possible object is
     *     {@link TypeVehicleCategory }
     *     
     */
    public TypeVehicleCategory getCategory() {
        return category;
    }

    /**
     * Define el valor de la propiedad category.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeVehicleCategory }
     *     
     */
    public void setCategory(TypeVehicleCategory value) {
        this.category = value;
    }

    /**
     * Obtiene el valor de la propiedad description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Define el valor de la propiedad description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Obtiene el valor de la propiedad doorCount.
     * 
     * @return
     *     possible object is
     *     {@link TypeDoorCount }
     *     
     */
    public TypeDoorCount getDoorCount() {
        return doorCount;
    }

    /**
     * Define el valor de la propiedad doorCount.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeDoorCount }
     *     
     */
    public void setDoorCount(TypeDoorCount value) {
        this.doorCount = value;
    }

    /**
     * Obtiene el valor de la propiedad location.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocation() {
        return location;
    }

    /**
     * Define el valor de la propiedad location.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocation(String value) {
        this.location = value;
    }

    /**
     * Obtiene el valor de la propiedad counterLocationCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCounterLocationCode() {
        return counterLocationCode;
    }

    /**
     * Define el valor de la propiedad counterLocationCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCounterLocationCode(String value) {
        this.counterLocationCode = value;
    }

    /**
     * Obtiene el valor de la propiedad vendorLocationKey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorLocationKey() {
        return vendorLocationKey;
    }

    /**
     * Define el valor de la propiedad vendorLocationKey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorLocationKey(String value) {
        this.vendorLocationKey = value;
    }

    /**
     * Obtiene el valor de la propiedad vendorName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorName() {
        return vendorName;
    }

    /**
     * Define el valor de la propiedad vendorName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorName(String value) {
        this.vendorName = value;
    }

    /**
     * Obtiene el valor de la propiedad alternateVendor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternateVendor() {
        return alternateVendor;
    }

    /**
     * Define el valor de la propiedad alternateVendor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternateVendor(String value) {
        this.alternateVendor = value;
    }

    /**
     * Obtiene el valor de la propiedad fuelType.
     * 
     * @return
     *     possible object is
     *     {@link TypeFuel }
     *     
     */
    public TypeFuel getFuelType() {
        return fuelType;
    }

    /**
     * Define el valor de la propiedad fuelType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeFuel }
     *     
     */
    public void setFuelType(TypeFuel value) {
        this.fuelType = value;
    }

    /**
     * Obtiene el valor de la propiedad acrissVehicleCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcrissVehicleCode() {
        return acrissVehicleCode;
    }

    /**
     * Define el valor de la propiedad acrissVehicleCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcrissVehicleCode(String value) {
        this.acrissVehicleCode = value;
    }

    /**
     * Obtiene el valor de la propiedad key.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Define el valor de la propiedad key.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Obtiene el valor de la propiedad returnAtPickup.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReturnAtPickup() {
        return returnAtPickup;
    }

    /**
     * Define el valor de la propiedad returnAtPickup.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReturnAtPickup(Boolean value) {
        this.returnAtPickup = value;
    }

    /**
     * Obtiene el valor de la propiedad inPolicy.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isInPolicy() {
        return inPolicy;
    }

    /**
     * Define el valor de la propiedad inPolicy.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInPolicy(Boolean value) {
        this.inPolicy = value;
    }

    /**
     * Obtiene el valor de la propiedad policyCode.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPolicyCode() {
        return policyCode;
    }

    /**
     * Define el valor de la propiedad policyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPolicyCode(Integer value) {
        this.policyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad preferredOption.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPreferredOption() {
        return preferredOption;
    }

    /**
     * Define el valor de la propiedad preferredOption.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPreferredOption(Boolean value) {
        this.preferredOption = value;
    }

}
