
package com.travelport.schema.vehicle_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para typeVehicleRates complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="typeVehicleRates"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="EstimatedTotalAmount" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="BaseRate" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="RateForPeriod" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="DropOffCharge" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="YoungDriverCharge" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="SeniorDriverCharge" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="FuelSurcharge" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="ExtraMileageCharge" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "typeVehicleRates")
@XmlSeeAlso({
    SupplierRate.class
})
public class TypeVehicleRates {

    @XmlAttribute(name = "EstimatedTotalAmount")
    protected String estimatedTotalAmount;
    @XmlAttribute(name = "BaseRate")
    protected String baseRate;
    @XmlAttribute(name = "RateForPeriod")
    protected String rateForPeriod;
    @XmlAttribute(name = "DropOffCharge")
    protected String dropOffCharge;
    @XmlAttribute(name = "YoungDriverCharge")
    protected String youngDriverCharge;
    @XmlAttribute(name = "SeniorDriverCharge")
    protected String seniorDriverCharge;
    @XmlAttribute(name = "FuelSurcharge")
    protected String fuelSurcharge;
    @XmlAttribute(name = "ExtraMileageCharge")
    protected String extraMileageCharge;

    /**
     * Obtiene el valor de la propiedad estimatedTotalAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstimatedTotalAmount() {
        return estimatedTotalAmount;
    }

    /**
     * Define el valor de la propiedad estimatedTotalAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstimatedTotalAmount(String value) {
        this.estimatedTotalAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad baseRate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBaseRate() {
        return baseRate;
    }

    /**
     * Define el valor de la propiedad baseRate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBaseRate(String value) {
        this.baseRate = value;
    }

    /**
     * Obtiene el valor de la propiedad rateForPeriod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateForPeriod() {
        return rateForPeriod;
    }

    /**
     * Define el valor de la propiedad rateForPeriod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateForPeriod(String value) {
        this.rateForPeriod = value;
    }

    /**
     * Obtiene el valor de la propiedad dropOffCharge.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDropOffCharge() {
        return dropOffCharge;
    }

    /**
     * Define el valor de la propiedad dropOffCharge.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDropOffCharge(String value) {
        this.dropOffCharge = value;
    }

    /**
     * Obtiene el valor de la propiedad youngDriverCharge.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getYoungDriverCharge() {
        return youngDriverCharge;
    }

    /**
     * Define el valor de la propiedad youngDriverCharge.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setYoungDriverCharge(String value) {
        this.youngDriverCharge = value;
    }

    /**
     * Obtiene el valor de la propiedad seniorDriverCharge.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeniorDriverCharge() {
        return seniorDriverCharge;
    }

    /**
     * Define el valor de la propiedad seniorDriverCharge.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeniorDriverCharge(String value) {
        this.seniorDriverCharge = value;
    }

    /**
     * Obtiene el valor de la propiedad fuelSurcharge.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFuelSurcharge() {
        return fuelSurcharge;
    }

    /**
     * Define el valor de la propiedad fuelSurcharge.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFuelSurcharge(String value) {
        this.fuelSurcharge = value;
    }

    /**
     * Obtiene el valor de la propiedad extraMileageCharge.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtraMileageCharge() {
        return extraMileageCharge;
    }

    /**
     * Define el valor de la propiedad extraMileageCharge.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtraMileageCharge(String value) {
        this.extraMileageCharge = value;
    }

}
