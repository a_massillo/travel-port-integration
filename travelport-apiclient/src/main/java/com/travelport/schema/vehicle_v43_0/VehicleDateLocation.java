
package com.travelport.schema.vehicle_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.Distance;
import com.travelport.schema.common_v43_0.TypeVehicleLocation;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VendorLocation" maxOccurs="999" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.travelport.com/schema/vehicle_v43_0}typeVehicleVendorLocation"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Distance" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="PickupDateTime" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="PickupLocation" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *       &lt;attribute name="ReturnDateTime" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="ReturnLocation" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *       &lt;attribute name="PickupLocationType" type="{http://www.travelport.com/schema/common_v43_0}typeVehicleLocation" /&gt;
 *       &lt;attribute name="ReturnLocationType" type="{http://www.travelport.com/schema/common_v43_0}typeVehicleLocation" /&gt;
 *       &lt;attribute name="PickupLocationNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="ReturnLocationNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="Key" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vendorLocation"
})
@XmlRootElement(name = "VehicleDateLocation")
public class VehicleDateLocation {

    @XmlElement(name = "VendorLocation")
    protected List<VehicleDateLocation.VendorLocation> vendorLocation;
    @XmlAttribute(name = "PickupDateTime", required = true)
    protected String pickupDateTime;
    @XmlAttribute(name = "PickupLocation")
    protected String pickupLocation;
    @XmlAttribute(name = "ReturnDateTime", required = true)
    protected String returnDateTime;
    @XmlAttribute(name = "ReturnLocation")
    protected String returnLocation;
    @XmlAttribute(name = "PickupLocationType")
    protected TypeVehicleLocation pickupLocationType;
    @XmlAttribute(name = "ReturnLocationType")
    protected TypeVehicleLocation returnLocationType;
    @XmlAttribute(name = "PickupLocationNumber")
    protected String pickupLocationNumber;
    @XmlAttribute(name = "ReturnLocationNumber")
    protected String returnLocationNumber;
    @XmlAttribute(name = "Key")
    protected String key;

    /**
     * Gets the value of the vendorLocation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vendorLocation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVendorLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VehicleDateLocation.VendorLocation }
     * 
     * 
     */
    public List<VehicleDateLocation.VendorLocation> getVendorLocation() {
        if (vendorLocation == null) {
            vendorLocation = new ArrayList<VehicleDateLocation.VendorLocation>();
        }
        return this.vendorLocation;
    }

    /**
     * Obtiene el valor de la propiedad pickupDateTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPickupDateTime() {
        return pickupDateTime;
    }

    /**
     * Define el valor de la propiedad pickupDateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPickupDateTime(String value) {
        this.pickupDateTime = value;
    }

    /**
     * Obtiene el valor de la propiedad pickupLocation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPickupLocation() {
        return pickupLocation;
    }

    /**
     * Define el valor de la propiedad pickupLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPickupLocation(String value) {
        this.pickupLocation = value;
    }

    /**
     * Obtiene el valor de la propiedad returnDateTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnDateTime() {
        return returnDateTime;
    }

    /**
     * Define el valor de la propiedad returnDateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnDateTime(String value) {
        this.returnDateTime = value;
    }

    /**
     * Obtiene el valor de la propiedad returnLocation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnLocation() {
        return returnLocation;
    }

    /**
     * Define el valor de la propiedad returnLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnLocation(String value) {
        this.returnLocation = value;
    }

    /**
     * Obtiene el valor de la propiedad pickupLocationType.
     * 
     * @return
     *     possible object is
     *     {@link TypeVehicleLocation }
     *     
     */
    public TypeVehicleLocation getPickupLocationType() {
        return pickupLocationType;
    }

    /**
     * Define el valor de la propiedad pickupLocationType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeVehicleLocation }
     *     
     */
    public void setPickupLocationType(TypeVehicleLocation value) {
        this.pickupLocationType = value;
    }

    /**
     * Obtiene el valor de la propiedad returnLocationType.
     * 
     * @return
     *     possible object is
     *     {@link TypeVehicleLocation }
     *     
     */
    public TypeVehicleLocation getReturnLocationType() {
        return returnLocationType;
    }

    /**
     * Define el valor de la propiedad returnLocationType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeVehicleLocation }
     *     
     */
    public void setReturnLocationType(TypeVehicleLocation value) {
        this.returnLocationType = value;
    }

    /**
     * Obtiene el valor de la propiedad pickupLocationNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPickupLocationNumber() {
        return pickupLocationNumber;
    }

    /**
     * Define el valor de la propiedad pickupLocationNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPickupLocationNumber(String value) {
        this.pickupLocationNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad returnLocationNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnLocationNumber() {
        return returnLocationNumber;
    }

    /**
     * Define el valor de la propiedad returnLocationNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnLocationNumber(String value) {
        this.returnLocationNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad key.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Define el valor de la propiedad key.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.travelport.com/schema/vehicle_v43_0}typeVehicleVendorLocation"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Distance" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "distance"
    })
    public static class VendorLocation
        extends TypeVehicleVendorLocation
    {

        @XmlElement(name = "Distance", namespace = "http://www.travelport.com/schema/common_v43_0")
        protected Distance distance;

        /**
         * Obtiene el valor de la propiedad distance.
         * 
         * @return
         *     possible object is
         *     {@link Distance }
         *     
         */
        public Distance getDistance() {
            return distance;
        }

        /**
         * Define el valor de la propiedad distance.
         * 
         * @param value
         *     allowed object is
         *     {@link Distance }
         *     
         */
        public void setDistance(Distance value) {
            this.distance = value;
        }

    }

}
