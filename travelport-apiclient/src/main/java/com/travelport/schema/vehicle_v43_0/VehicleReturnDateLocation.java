
package com.travelport.schema.vehicle_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.TypeVehicleLocation;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="ReturnDateTime" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="ReturnLocation" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *       &lt;attribute name="ReturnLocationType" type="{http://www.travelport.com/schema/common_v43_0}typeVehicleLocation" /&gt;
 *       &lt;attribute name="ReturnLocationNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "VehicleReturnDateLocation")
public class VehicleReturnDateLocation {

    @XmlAttribute(name = "ReturnDateTime")
    protected String returnDateTime;
    @XmlAttribute(name = "ReturnLocation")
    protected String returnLocation;
    @XmlAttribute(name = "ReturnLocationType")
    protected TypeVehicleLocation returnLocationType;
    @XmlAttribute(name = "ReturnLocationNumber")
    protected String returnLocationNumber;

    /**
     * Obtiene el valor de la propiedad returnDateTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnDateTime() {
        return returnDateTime;
    }

    /**
     * Define el valor de la propiedad returnDateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnDateTime(String value) {
        this.returnDateTime = value;
    }

    /**
     * Obtiene el valor de la propiedad returnLocation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnLocation() {
        return returnLocation;
    }

    /**
     * Define el valor de la propiedad returnLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnLocation(String value) {
        this.returnLocation = value;
    }

    /**
     * Obtiene el valor de la propiedad returnLocationType.
     * 
     * @return
     *     possible object is
     *     {@link TypeVehicleLocation }
     *     
     */
    public TypeVehicleLocation getReturnLocationType() {
        return returnLocationType;
    }

    /**
     * Define el valor de la propiedad returnLocationType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeVehicleLocation }
     *     
     */
    public void setReturnLocationType(TypeVehicleLocation value) {
        this.returnLocationType = value;
    }

    /**
     * Obtiene el valor de la propiedad returnLocationNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnLocationNumber() {
        return returnLocationNumber;
    }

    /**
     * Define el valor de la propiedad returnLocationNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnLocationNumber(String value) {
        this.returnLocationNumber = value;
    }

}
