
package com.travelport.schema.vehicle_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.TypeStructuredAddress;
import com.travelport.schema.common_v43_0.TypeVehicleLocation;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Address" type="{http://www.travelport.com/schema/common_v43_0}typeStructuredAddress" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attGroup ref="{http://www.travelport.com/schema/vehicle_v43_0}attrAreaInfo"/&gt;
 *       &lt;attribute name="LocationType" type="{http://www.travelport.com/schema/common_v43_0}typeVehicleLocation" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "address"
})
@XmlRootElement(name = "LocationInformation")
public class LocationInformation {

    @XmlElement(name = "Address")
    protected TypeStructuredAddress address;
    @XmlAttribute(name = "LocationType")
    protected TypeVehicleLocation locationType;
    @XmlAttribute(name = "AreaGroup")
    protected String areaGroup;
    @XmlAttribute(name = "Location")
    protected String location;
    @XmlAttribute(name = "AreaType")
    protected TypeAreaInfo areaType;

    /**
     * Obtiene el valor de la propiedad address.
     * 
     * @return
     *     possible object is
     *     {@link TypeStructuredAddress }
     *     
     */
    public TypeStructuredAddress getAddress() {
        return address;
    }

    /**
     * Define el valor de la propiedad address.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeStructuredAddress }
     *     
     */
    public void setAddress(TypeStructuredAddress value) {
        this.address = value;
    }

    /**
     * Obtiene el valor de la propiedad locationType.
     * 
     * @return
     *     possible object is
     *     {@link TypeVehicleLocation }
     *     
     */
    public TypeVehicleLocation getLocationType() {
        return locationType;
    }

    /**
     * Define el valor de la propiedad locationType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeVehicleLocation }
     *     
     */
    public void setLocationType(TypeVehicleLocation value) {
        this.locationType = value;
    }

    /**
     * Obtiene el valor de la propiedad areaGroup.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAreaGroup() {
        return areaGroup;
    }

    /**
     * Define el valor de la propiedad areaGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAreaGroup(String value) {
        this.areaGroup = value;
    }

    /**
     * Obtiene el valor de la propiedad location.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocation() {
        return location;
    }

    /**
     * Define el valor de la propiedad location.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocation(String value) {
        this.location = value;
    }

    /**
     * Obtiene el valor de la propiedad areaType.
     * 
     * @return
     *     possible object is
     *     {@link TypeAreaInfo }
     *     
     */
    public TypeAreaInfo getAreaType() {
        return areaType;
    }

    /**
     * Define el valor de la propiedad areaType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeAreaInfo }
     *     
     */
    public void setAreaType(TypeAreaInfo value) {
        this.areaType = value;
    }

}
