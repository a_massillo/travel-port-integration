
package com.travelport.schema.universal_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="NextOnQueue" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="ProviderLocatorCode" type="{http://www.travelport.com/schema/common_v43_0}typeLocatorCode" /&gt;
 *       &lt;attribute name="ReQueueCurrent" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="QueueSessionToken" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "QueueNextModifiers")
public class QueueNextModifiers {

    @XmlAttribute(name = "NextOnQueue")
    protected Boolean nextOnQueue;
    @XmlAttribute(name = "ProviderLocatorCode")
    protected String providerLocatorCode;
    @XmlAttribute(name = "ReQueueCurrent")
    protected Boolean reQueueCurrent;
    @XmlAttribute(name = "QueueSessionToken")
    protected String queueSessionToken;

    /**
     * Obtiene el valor de la propiedad nextOnQueue.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNextOnQueue() {
        return nextOnQueue;
    }

    /**
     * Define el valor de la propiedad nextOnQueue.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNextOnQueue(Boolean value) {
        this.nextOnQueue = value;
    }

    /**
     * Obtiene el valor de la propiedad providerLocatorCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProviderLocatorCode() {
        return providerLocatorCode;
    }

    /**
     * Define el valor de la propiedad providerLocatorCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProviderLocatorCode(String value) {
        this.providerLocatorCode = value;
    }

    /**
     * Obtiene el valor de la propiedad reQueueCurrent.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReQueueCurrent() {
        return reQueueCurrent;
    }

    /**
     * Define el valor de la propiedad reQueueCurrent.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReQueueCurrent(Boolean value) {
        this.reQueueCurrent = value;
    }

    /**
     * Obtiene el valor de la propiedad queueSessionToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueueSessionToken() {
        return queueSessionToken;
    }

    /**
     * Define el valor de la propiedad queueSessionToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueueSessionToken(String value) {
        this.queueSessionToken = value;
    }

}
