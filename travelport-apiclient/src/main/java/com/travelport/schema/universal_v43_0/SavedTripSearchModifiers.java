
package com.travelport.schema.universal_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/universal_v43_0}BaseSearchModifiers"&gt;
 *       &lt;attribute name="SavedTripName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="ExcludeURAssociated" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "SavedTripSearchModifiers")
public class SavedTripSearchModifiers
    extends BaseSearchModifiers
{

    @XmlAttribute(name = "SavedTripName")
    protected String savedTripName;
    @XmlAttribute(name = "ExcludeURAssociated")
    protected Boolean excludeURAssociated;

    /**
     * Obtiene el valor de la propiedad savedTripName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSavedTripName() {
        return savedTripName;
    }

    /**
     * Define el valor de la propiedad savedTripName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSavedTripName(String value) {
        this.savedTripName = value;
    }

    /**
     * Obtiene el valor de la propiedad excludeURAssociated.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isExcludeURAssociated() {
        if (excludeURAssociated == null) {
            return true;
        } else {
            return excludeURAssociated;
        }
    }

    /**
     * Define el valor de la propiedad excludeURAssociated.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExcludeURAssociated(Boolean value) {
        this.excludeURAssociated = value;
    }

}
