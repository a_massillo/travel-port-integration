
package com.travelport.schema.universal_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseReq;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseReq"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}UniversalRecordHistorySearchModifiers" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="UniversalRecordLocatorCode" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeLocatorCode" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "universalRecordHistorySearchModifiers"
})
@XmlRootElement(name = "UniversalRecordHistorySearchReq")
public class UniversalRecordHistorySearchReq
    extends BaseReq
{

    @XmlElement(name = "UniversalRecordHistorySearchModifiers")
    protected UniversalRecordHistorySearchModifiers universalRecordHistorySearchModifiers;
    @XmlAttribute(name = "UniversalRecordLocatorCode", required = true)
    protected String universalRecordLocatorCode;

    /**
     * Obtiene el valor de la propiedad universalRecordHistorySearchModifiers.
     * 
     * @return
     *     possible object is
     *     {@link UniversalRecordHistorySearchModifiers }
     *     
     */
    public UniversalRecordHistorySearchModifiers getUniversalRecordHistorySearchModifiers() {
        return universalRecordHistorySearchModifiers;
    }

    /**
     * Define el valor de la propiedad universalRecordHistorySearchModifiers.
     * 
     * @param value
     *     allowed object is
     *     {@link UniversalRecordHistorySearchModifiers }
     *     
     */
    public void setUniversalRecordHistorySearchModifiers(UniversalRecordHistorySearchModifiers value) {
        this.universalRecordHistorySearchModifiers = value;
    }

    /**
     * Obtiene el valor de la propiedad universalRecordLocatorCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniversalRecordLocatorCode() {
        return universalRecordLocatorCode;
    }

    /**
     * Define el valor de la propiedad universalRecordLocatorCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniversalRecordLocatorCode(String value) {
        this.universalRecordLocatorCode = value;
    }

}
