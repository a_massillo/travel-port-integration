
package com.travelport.schema.universal_v43_0;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Controls and switches for the Universal Record Search and Saved Trip search request.
 * 
 * <p>Clase Java para BaseSearchModifiers complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BaseSearchModifiers"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TravelDate" type="{http://www.travelport.com/schema/universal_v43_0}typeDateSpec" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="IncludeAllNames" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="IncludeAgentInfo" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="MaxResults" type="{http://www.travelport.com/schema/common_v43_0}typeMaxResults" default="20" /&gt;
 *       &lt;attribute name="StartFromResult" type="{http://www.travelport.com/schema/common_v43_0}typeStartFromResult" /&gt;
 *       &lt;attribute name="ExcludeAir" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="ExcludeVehicle" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="ExcludeHotel" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaseSearchModifiers", propOrder = {
    "travelDate"
})
@XmlSeeAlso({
    SavedTripSearchModifiers.class,
    UniversalRecordSearchModifiers.class
})
public class BaseSearchModifiers {

    @XmlElement(name = "TravelDate")
    protected TypeDateSpec travelDate;
    @XmlAttribute(name = "IncludeAllNames")
    protected Boolean includeAllNames;
    @XmlAttribute(name = "IncludeAgentInfo")
    protected Boolean includeAgentInfo;
    @XmlAttribute(name = "MaxResults")
    protected Integer maxResults;
    @XmlAttribute(name = "StartFromResult")
    protected BigInteger startFromResult;
    @XmlAttribute(name = "ExcludeAir")
    protected Boolean excludeAir;
    @XmlAttribute(name = "ExcludeVehicle")
    protected Boolean excludeVehicle;
    @XmlAttribute(name = "ExcludeHotel")
    protected Boolean excludeHotel;

    /**
     * Obtiene el valor de la propiedad travelDate.
     * 
     * @return
     *     possible object is
     *     {@link TypeDateSpec }
     *     
     */
    public TypeDateSpec getTravelDate() {
        return travelDate;
    }

    /**
     * Define el valor de la propiedad travelDate.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeDateSpec }
     *     
     */
    public void setTravelDate(TypeDateSpec value) {
        this.travelDate = value;
    }

    /**
     * Obtiene el valor de la propiedad includeAllNames.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIncludeAllNames() {
        if (includeAllNames == null) {
            return false;
        } else {
            return includeAllNames;
        }
    }

    /**
     * Define el valor de la propiedad includeAllNames.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeAllNames(Boolean value) {
        this.includeAllNames = value;
    }

    /**
     * Obtiene el valor de la propiedad includeAgentInfo.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIncludeAgentInfo() {
        if (includeAgentInfo == null) {
            return false;
        } else {
            return includeAgentInfo;
        }
    }

    /**
     * Define el valor de la propiedad includeAgentInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeAgentInfo(Boolean value) {
        this.includeAgentInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad maxResults.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public int getMaxResults() {
        if (maxResults == null) {
            return  20;
        } else {
            return maxResults;
        }
    }

    /**
     * Define el valor de la propiedad maxResults.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxResults(Integer value) {
        this.maxResults = value;
    }

    /**
     * Obtiene el valor de la propiedad startFromResult.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStartFromResult() {
        return startFromResult;
    }

    /**
     * Define el valor de la propiedad startFromResult.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStartFromResult(BigInteger value) {
        this.startFromResult = value;
    }

    /**
     * Obtiene el valor de la propiedad excludeAir.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isExcludeAir() {
        if (excludeAir == null) {
            return false;
        } else {
            return excludeAir;
        }
    }

    /**
     * Define el valor de la propiedad excludeAir.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExcludeAir(Boolean value) {
        this.excludeAir = value;
    }

    /**
     * Obtiene el valor de la propiedad excludeVehicle.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isExcludeVehicle() {
        if (excludeVehicle == null) {
            return false;
        } else {
            return excludeVehicle;
        }
    }

    /**
     * Define el valor de la propiedad excludeVehicle.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExcludeVehicle(Boolean value) {
        this.excludeVehicle = value;
    }

    /**
     * Obtiene el valor de la propiedad excludeHotel.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isExcludeHotel() {
        if (excludeHotel == null) {
            return false;
        } else {
            return excludeHotel;
        }
    }

    /**
     * Define el valor de la propiedad excludeHotel.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExcludeHotel(Boolean value) {
        this.excludeHotel = value;
    }

}
