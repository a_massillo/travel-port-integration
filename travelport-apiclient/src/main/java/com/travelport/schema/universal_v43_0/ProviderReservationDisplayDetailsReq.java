
package com.travelport.schema.universal_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseReq;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseReq"&gt;
 *       &lt;attGroup ref="{http://www.travelport.com/schema/universal_v43_0}providerReservation"/&gt;
 *       &lt;attGroup ref="{http://www.travelport.com/schema/universal_v43_0}attrProviderReservationDetails"/&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "ProviderReservationDisplayDetailsReq")
public class ProviderReservationDisplayDetailsReq
    extends BaseReq
{

    @XmlAttribute(name = "ProviderCode", required = true)
    protected String providerCode;
    @XmlAttribute(name = "ProviderLocatorCode", required = true)
    protected String providerLocatorCode;
    @XmlAttribute(name = "ProviderReservationDetail")
    protected Boolean providerReservationDetail;
    @XmlAttribute(name = "CustomCheck")
    protected Boolean customCheck;
    @XmlAttribute(name = "ProviderProfile")
    protected Boolean providerProfile;
    @XmlAttribute(name = "DivideDetails")
    protected Boolean divideDetails;
    @XmlAttribute(name = "EnhancedItinModifiers")
    protected Boolean enhancedItinModifiers;
    @XmlAttribute(name = "IntegratedContent")
    protected Boolean integratedContent;
    @XmlAttribute(name = "Cruise")
    protected Boolean cruise;
    @XmlAttribute(name = "RailSegment")
    protected Boolean railSegment;

    /**
     * Obtiene el valor de la propiedad providerCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProviderCode() {
        return providerCode;
    }

    /**
     * Define el valor de la propiedad providerCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProviderCode(String value) {
        this.providerCode = value;
    }

    /**
     * Obtiene el valor de la propiedad providerLocatorCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProviderLocatorCode() {
        return providerLocatorCode;
    }

    /**
     * Define el valor de la propiedad providerLocatorCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProviderLocatorCode(String value) {
        this.providerLocatorCode = value;
    }

    /**
     * Obtiene el valor de la propiedad providerReservationDetail.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isProviderReservationDetail() {
        return providerReservationDetail;
    }

    /**
     * Define el valor de la propiedad providerReservationDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProviderReservationDetail(Boolean value) {
        this.providerReservationDetail = value;
    }

    /**
     * Obtiene el valor de la propiedad customCheck.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCustomCheck() {
        return customCheck;
    }

    /**
     * Define el valor de la propiedad customCheck.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCustomCheck(Boolean value) {
        this.customCheck = value;
    }

    /**
     * Obtiene el valor de la propiedad providerProfile.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isProviderProfile() {
        return providerProfile;
    }

    /**
     * Define el valor de la propiedad providerProfile.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProviderProfile(Boolean value) {
        this.providerProfile = value;
    }

    /**
     * Obtiene el valor de la propiedad divideDetails.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDivideDetails() {
        return divideDetails;
    }

    /**
     * Define el valor de la propiedad divideDetails.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDivideDetails(Boolean value) {
        this.divideDetails = value;
    }

    /**
     * Obtiene el valor de la propiedad enhancedItinModifiers.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnhancedItinModifiers() {
        return enhancedItinModifiers;
    }

    /**
     * Define el valor de la propiedad enhancedItinModifiers.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnhancedItinModifiers(Boolean value) {
        this.enhancedItinModifiers = value;
    }

    /**
     * Obtiene el valor de la propiedad integratedContent.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIntegratedContent() {
        return integratedContent;
    }

    /**
     * Define el valor de la propiedad integratedContent.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIntegratedContent(Boolean value) {
        this.integratedContent = value;
    }

    /**
     * Obtiene el valor de la propiedad cruise.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCruise() {
        return cruise;
    }

    /**
     * Define el valor de la propiedad cruise.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCruise(Boolean value) {
        this.cruise = value;
    }

    /**
     * Obtiene el valor de la propiedad railSegment.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRailSegment() {
        return railSegment;
    }

    /**
     * Define el valor de la propiedad railSegment.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRailSegment(Boolean value) {
        this.railSegment = value;
    }

}
