
package com.travelport.schema.universal_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseReq;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseReq"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="UniversalRecordLocatorCode" type="{http://www.travelport.com/schema/common_v43_0}typeLocatorCode"/&gt;
 *         &lt;element name="ProviderReservationInfo"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attGroup ref="{http://www.travelport.com/schema/common_v43_0}ProviderReservation"/&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/choice&gt;
 *       &lt;attribute name="ViewOnlyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="TravelerLastName" type="{http://www.travelport.com/schema/common_v43_0}typeTravelerLastName" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "universalRecordLocatorCode",
    "providerReservationInfo"
})
@XmlRootElement(name = "UniversalRecordRetrieveReq")
public class UniversalRecordRetrieveReq
    extends BaseReq
{

    @XmlElement(name = "UniversalRecordLocatorCode")
    protected String universalRecordLocatorCode;
    @XmlElement(name = "ProviderReservationInfo")
    protected UniversalRecordRetrieveReq.ProviderReservationInfo providerReservationInfo;
    @XmlAttribute(name = "ViewOnlyInd")
    protected Boolean viewOnlyInd;
    @XmlAttribute(name = "TravelerLastName")
    protected String travelerLastName;

    /**
     * Obtiene el valor de la propiedad universalRecordLocatorCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniversalRecordLocatorCode() {
        return universalRecordLocatorCode;
    }

    /**
     * Define el valor de la propiedad universalRecordLocatorCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniversalRecordLocatorCode(String value) {
        this.universalRecordLocatorCode = value;
    }

    /**
     * Obtiene el valor de la propiedad providerReservationInfo.
     * 
     * @return
     *     possible object is
     *     {@link UniversalRecordRetrieveReq.ProviderReservationInfo }
     *     
     */
    public UniversalRecordRetrieveReq.ProviderReservationInfo getProviderReservationInfo() {
        return providerReservationInfo;
    }

    /**
     * Define el valor de la propiedad providerReservationInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link UniversalRecordRetrieveReq.ProviderReservationInfo }
     *     
     */
    public void setProviderReservationInfo(UniversalRecordRetrieveReq.ProviderReservationInfo value) {
        this.providerReservationInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad viewOnlyInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isViewOnlyInd() {
        if (viewOnlyInd == null) {
            return false;
        } else {
            return viewOnlyInd;
        }
    }

    /**
     * Define el valor de la propiedad viewOnlyInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setViewOnlyInd(Boolean value) {
        this.viewOnlyInd = value;
    }

    /**
     * Obtiene el valor de la propiedad travelerLastName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTravelerLastName() {
        return travelerLastName;
    }

    /**
     * Define el valor de la propiedad travelerLastName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTravelerLastName(String value) {
        this.travelerLastName = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attGroup ref="{http://www.travelport.com/schema/common_v43_0}ProviderReservation"/&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ProviderReservationInfo {

        @XmlAttribute(name = "ProviderCode", required = true)
        protected String providerCode;
        @XmlAttribute(name = "ProviderLocatorCode", required = true)
        protected String providerLocatorCode;

        /**
         * Obtiene el valor de la propiedad providerCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProviderCode() {
            return providerCode;
        }

        /**
         * Define el valor de la propiedad providerCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProviderCode(String value) {
            this.providerCode = value;
        }

        /**
         * Obtiene el valor de la propiedad providerLocatorCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProviderLocatorCode() {
            return providerLocatorCode;
        }

        /**
         * Define el valor de la propiedad providerLocatorCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProviderLocatorCode(String value) {
            this.providerLocatorCode = value;
        }

    }

}
