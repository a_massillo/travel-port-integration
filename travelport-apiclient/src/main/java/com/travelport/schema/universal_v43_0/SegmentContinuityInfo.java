
package com.travelport.schema.universal_v43_0;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}ArvlUnknSegment" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}ContinuityOverrideRemark" maxOccurs="999" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ArrivalUnknownSegmentCount" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "arvlUnknSegment",
    "continuityOverrideRemark"
})
@XmlRootElement(name = "SegmentContinuityInfo")
public class SegmentContinuityInfo {

    @XmlElement(name = "ArvlUnknSegment")
    protected List<ArvlUnknSegment> arvlUnknSegment;
    @XmlElement(name = "ContinuityOverrideRemark")
    protected List<ContinuityOverrideRemark> continuityOverrideRemark;
    @XmlAttribute(name = "ArrivalUnknownSegmentCount")
    protected BigInteger arrivalUnknownSegmentCount;

    /**
     * Gets the value of the arvlUnknSegment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the arvlUnknSegment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getArvlUnknSegment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ArvlUnknSegment }
     * 
     * 
     */
    public List<ArvlUnknSegment> getArvlUnknSegment() {
        if (arvlUnknSegment == null) {
            arvlUnknSegment = new ArrayList<ArvlUnknSegment>();
        }
        return this.arvlUnknSegment;
    }

    /**
     * Gets the value of the continuityOverrideRemark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the continuityOverrideRemark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContinuityOverrideRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContinuityOverrideRemark }
     * 
     * 
     */
    public List<ContinuityOverrideRemark> getContinuityOverrideRemark() {
        if (continuityOverrideRemark == null) {
            continuityOverrideRemark = new ArrayList<ContinuityOverrideRemark>();
        }
        return this.continuityOverrideRemark;
    }

    /**
     * Obtiene el valor de la propiedad arrivalUnknownSegmentCount.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getArrivalUnknownSegmentCount() {
        return arrivalUnknownSegmentCount;
    }

    /**
     * Define el valor de la propiedad arrivalUnknownSegmentCount.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setArrivalUnknownSegmentCount(BigInteger value) {
        this.arrivalUnknownSegmentCount = value;
    }

}
