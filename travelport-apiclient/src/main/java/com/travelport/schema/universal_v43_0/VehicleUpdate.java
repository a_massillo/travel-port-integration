
package com.travelport.schema.universal_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BookingSource;
import com.travelport.schema.common_v43_0.DriversLicense;
import com.travelport.schema.common_v43_0.Guarantee;
import com.travelport.schema.common_v43_0.LoyaltyCard;
import com.travelport.schema.common_v43_0.ThirdPartyInformation;
import com.travelport.schema.common_v43_0.TravelComplianceData;
import com.travelport.schema.vehicle_v43_0.AssociatedRemark;
import com.travelport.schema.vehicle_v43_0.CollectionAddress;
import com.travelport.schema.vehicle_v43_0.DeliveryAddress;
import com.travelport.schema.vehicle_v43_0.FlightArrivalInformation;
import com.travelport.schema.vehicle_v43_0.PaymentInformation;
import com.travelport.schema.vehicle_v43_0.VehiclePickupDateLocation;
import com.travelport.schema.vehicle_v43_0.VehicleRateInfo;
import com.travelport.schema.vehicle_v43_0.VehicleReturnDateLocation;
import com.travelport.schema.vehicle_v43_0.VehicleSpecialRequest;
import com.travelport.schema.vehicle_v43_0.VehicleTypeIdentifier;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}LoyaltyCard" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}DriversLicense" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Guarantee" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}BookingSource" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/vehicle_v43_0}VehicleRateInfo" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/vehicle_v43_0}PaymentInformation" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/vehicle_v43_0}AssociatedRemark" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/vehicle_v43_0}VehicleSpecialRequest" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/vehicle_v43_0}DeliveryAddress" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/vehicle_v43_0}CollectionAddress" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/vehicle_v43_0}VehicleReturnDateLocation" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/vehicle_v43_0}VehiclePickupDateLocation" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}ThirdPartyInformation" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}TravelComplianceData" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/vehicle_v43_0}VehicleTypeIdentifier" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/vehicle_v43_0}FlightArrivalInformation" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *       &lt;attribute name="ReservationLocatorCode" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeLocatorCode" /&gt;
 *       &lt;attribute name="BookingTravelerRef" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "loyaltyCard",
    "driversLicense",
    "guarantee",
    "bookingSource",
    "vehicleRateInfo",
    "paymentInformation",
    "associatedRemark",
    "vehicleSpecialRequest",
    "deliveryAddress",
    "collectionAddress",
    "vehicleReturnDateLocation",
    "vehiclePickupDateLocation",
    "thirdPartyInformation",
    "travelComplianceData",
    "vehicleTypeIdentifier",
    "flightArrivalInformation"
})
@XmlRootElement(name = "VehicleUpdate")
public class VehicleUpdate {

    @XmlElement(name = "LoyaltyCard", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected List<LoyaltyCard> loyaltyCard;
    @XmlElement(name = "DriversLicense", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected DriversLicense driversLicense;
    @XmlElement(name = "Guarantee", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected Guarantee guarantee;
    @XmlElement(name = "BookingSource", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected BookingSource bookingSource;
    @XmlElement(name = "VehicleRateInfo", namespace = "http://www.travelport.com/schema/vehicle_v43_0")
    protected VehicleRateInfo vehicleRateInfo;
    @XmlElement(name = "PaymentInformation", namespace = "http://www.travelport.com/schema/vehicle_v43_0")
    protected PaymentInformation paymentInformation;
    @XmlElement(name = "AssociatedRemark", namespace = "http://www.travelport.com/schema/vehicle_v43_0")
    protected List<AssociatedRemark> associatedRemark;
    @XmlElement(name = "VehicleSpecialRequest", namespace = "http://www.travelport.com/schema/vehicle_v43_0")
    protected VehicleSpecialRequest vehicleSpecialRequest;
    @XmlElement(name = "DeliveryAddress", namespace = "http://www.travelport.com/schema/vehicle_v43_0")
    protected DeliveryAddress deliveryAddress;
    @XmlElement(name = "CollectionAddress", namespace = "http://www.travelport.com/schema/vehicle_v43_0")
    protected CollectionAddress collectionAddress;
    @XmlElement(name = "VehicleReturnDateLocation", namespace = "http://www.travelport.com/schema/vehicle_v43_0")
    protected VehicleReturnDateLocation vehicleReturnDateLocation;
    @XmlElement(name = "VehiclePickupDateLocation", namespace = "http://www.travelport.com/schema/vehicle_v43_0")
    protected VehiclePickupDateLocation vehiclePickupDateLocation;
    @XmlElement(name = "ThirdPartyInformation", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected ThirdPartyInformation thirdPartyInformation;
    @XmlElement(name = "TravelComplianceData", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected List<TravelComplianceData> travelComplianceData;
    @XmlElement(name = "VehicleTypeIdentifier", namespace = "http://www.travelport.com/schema/vehicle_v43_0")
    protected VehicleTypeIdentifier vehicleTypeIdentifier;
    @XmlElement(name = "FlightArrivalInformation", namespace = "http://www.travelport.com/schema/vehicle_v43_0")
    protected FlightArrivalInformation flightArrivalInformation;
    @XmlAttribute(name = "ReservationLocatorCode", required = true)
    protected String reservationLocatorCode;
    @XmlAttribute(name = "BookingTravelerRef")
    protected String bookingTravelerRef;

    /**
     * Gets the value of the loyaltyCard property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the loyaltyCard property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLoyaltyCard().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LoyaltyCard }
     * 
     * 
     */
    public List<LoyaltyCard> getLoyaltyCard() {
        if (loyaltyCard == null) {
            loyaltyCard = new ArrayList<LoyaltyCard>();
        }
        return this.loyaltyCard;
    }

    /**
     * Obtiene el valor de la propiedad driversLicense.
     * 
     * @return
     *     possible object is
     *     {@link DriversLicense }
     *     
     */
    public DriversLicense getDriversLicense() {
        return driversLicense;
    }

    /**
     * Define el valor de la propiedad driversLicense.
     * 
     * @param value
     *     allowed object is
     *     {@link DriversLicense }
     *     
     */
    public void setDriversLicense(DriversLicense value) {
        this.driversLicense = value;
    }

    /**
     * Obtiene el valor de la propiedad guarantee.
     * 
     * @return
     *     possible object is
     *     {@link Guarantee }
     *     
     */
    public Guarantee getGuarantee() {
        return guarantee;
    }

    /**
     * Define el valor de la propiedad guarantee.
     * 
     * @param value
     *     allowed object is
     *     {@link Guarantee }
     *     
     */
    public void setGuarantee(Guarantee value) {
        this.guarantee = value;
    }

    /**
     * Obtiene el valor de la propiedad bookingSource.
     * 
     * @return
     *     possible object is
     *     {@link BookingSource }
     *     
     */
    public BookingSource getBookingSource() {
        return bookingSource;
    }

    /**
     * Define el valor de la propiedad bookingSource.
     * 
     * @param value
     *     allowed object is
     *     {@link BookingSource }
     *     
     */
    public void setBookingSource(BookingSource value) {
        this.bookingSource = value;
    }

    /**
     * Obtiene el valor de la propiedad vehicleRateInfo.
     * 
     * @return
     *     possible object is
     *     {@link VehicleRateInfo }
     *     
     */
    public VehicleRateInfo getVehicleRateInfo() {
        return vehicleRateInfo;
    }

    /**
     * Define el valor de la propiedad vehicleRateInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleRateInfo }
     *     
     */
    public void setVehicleRateInfo(VehicleRateInfo value) {
        this.vehicleRateInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad paymentInformation.
     * 
     * @return
     *     possible object is
     *     {@link PaymentInformation }
     *     
     */
    public PaymentInformation getPaymentInformation() {
        return paymentInformation;
    }

    /**
     * Define el valor de la propiedad paymentInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentInformation }
     *     
     */
    public void setPaymentInformation(PaymentInformation value) {
        this.paymentInformation = value;
    }

    /**
     * Gets the value of the associatedRemark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the associatedRemark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssociatedRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AssociatedRemark }
     * 
     * 
     */
    public List<AssociatedRemark> getAssociatedRemark() {
        if (associatedRemark == null) {
            associatedRemark = new ArrayList<AssociatedRemark>();
        }
        return this.associatedRemark;
    }

    /**
     * Obtiene el valor de la propiedad vehicleSpecialRequest.
     * 
     * @return
     *     possible object is
     *     {@link VehicleSpecialRequest }
     *     
     */
    public VehicleSpecialRequest getVehicleSpecialRequest() {
        return vehicleSpecialRequest;
    }

    /**
     * Define el valor de la propiedad vehicleSpecialRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleSpecialRequest }
     *     
     */
    public void setVehicleSpecialRequest(VehicleSpecialRequest value) {
        this.vehicleSpecialRequest = value;
    }

    /**
     * Obtiene el valor de la propiedad deliveryAddress.
     * 
     * @return
     *     possible object is
     *     {@link DeliveryAddress }
     *     
     */
    public DeliveryAddress getDeliveryAddress() {
        return deliveryAddress;
    }

    /**
     * Define el valor de la propiedad deliveryAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link DeliveryAddress }
     *     
     */
    public void setDeliveryAddress(DeliveryAddress value) {
        this.deliveryAddress = value;
    }

    /**
     * Obtiene el valor de la propiedad collectionAddress.
     * 
     * @return
     *     possible object is
     *     {@link CollectionAddress }
     *     
     */
    public CollectionAddress getCollectionAddress() {
        return collectionAddress;
    }

    /**
     * Define el valor de la propiedad collectionAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link CollectionAddress }
     *     
     */
    public void setCollectionAddress(CollectionAddress value) {
        this.collectionAddress = value;
    }

    /**
     * Obtiene el valor de la propiedad vehicleReturnDateLocation.
     * 
     * @return
     *     possible object is
     *     {@link VehicleReturnDateLocation }
     *     
     */
    public VehicleReturnDateLocation getVehicleReturnDateLocation() {
        return vehicleReturnDateLocation;
    }

    /**
     * Define el valor de la propiedad vehicleReturnDateLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleReturnDateLocation }
     *     
     */
    public void setVehicleReturnDateLocation(VehicleReturnDateLocation value) {
        this.vehicleReturnDateLocation = value;
    }

    /**
     * Obtiene el valor de la propiedad vehiclePickupDateLocation.
     * 
     * @return
     *     possible object is
     *     {@link VehiclePickupDateLocation }
     *     
     */
    public VehiclePickupDateLocation getVehiclePickupDateLocation() {
        return vehiclePickupDateLocation;
    }

    /**
     * Define el valor de la propiedad vehiclePickupDateLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link VehiclePickupDateLocation }
     *     
     */
    public void setVehiclePickupDateLocation(VehiclePickupDateLocation value) {
        this.vehiclePickupDateLocation = value;
    }

    /**
     * Third party supplier locator information. Specifically applicable for SDK booking. Previously saved values can be updated for SupplierCode, SupplierName and SupplierLocatorCode. If an attribute is not passed here, then previously saved value for that attribute will be deleted.
     * 
     * @return
     *     possible object is
     *     {@link ThirdPartyInformation }
     *     
     */
    public ThirdPartyInformation getThirdPartyInformation() {
        return thirdPartyInformation;
    }

    /**
     * Define el valor de la propiedad thirdPartyInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link ThirdPartyInformation }
     *     
     */
    public void setThirdPartyInformation(ThirdPartyInformation value) {
        this.thirdPartyInformation = value;
    }

    /**
     * Gets the value of the travelComplianceData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the travelComplianceData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTravelComplianceData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TravelComplianceData }
     * 
     * 
     */
    public List<TravelComplianceData> getTravelComplianceData() {
        if (travelComplianceData == null) {
            travelComplianceData = new ArrayList<TravelComplianceData>();
        }
        return this.travelComplianceData;
    }

    /**
     * Obtiene el valor de la propiedad vehicleTypeIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link VehicleTypeIdentifier }
     *     
     */
    public VehicleTypeIdentifier getVehicleTypeIdentifier() {
        return vehicleTypeIdentifier;
    }

    /**
     * Define el valor de la propiedad vehicleTypeIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleTypeIdentifier }
     *     
     */
    public void setVehicleTypeIdentifier(VehicleTypeIdentifier value) {
        this.vehicleTypeIdentifier = value;
    }

    /**
     * The flight arrival information(airline code and flight number) for the airport/city at which the rental car will be picked up || Addition and Update in UR Modify is currently implemented only for Galileo(1G) and Apollo(1V).
     * 
     * @return
     *     possible object is
     *     {@link FlightArrivalInformation }
     *     
     */
    public FlightArrivalInformation getFlightArrivalInformation() {
        return flightArrivalInformation;
    }

    /**
     * Define el valor de la propiedad flightArrivalInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link FlightArrivalInformation }
     *     
     */
    public void setFlightArrivalInformation(FlightArrivalInformation value) {
        this.flightArrivalInformation = value;
    }

    /**
     * Obtiene el valor de la propiedad reservationLocatorCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReservationLocatorCode() {
        return reservationLocatorCode;
    }

    /**
     * Define el valor de la propiedad reservationLocatorCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReservationLocatorCode(String value) {
        this.reservationLocatorCode = value;
    }

    /**
     * Obtiene el valor de la propiedad bookingTravelerRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookingTravelerRef() {
        return bookingTravelerRef;
    }

    /**
     * Define el valor de la propiedad bookingTravelerRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookingTravelerRef(String value) {
        this.bookingTravelerRef = value;
    }

}
