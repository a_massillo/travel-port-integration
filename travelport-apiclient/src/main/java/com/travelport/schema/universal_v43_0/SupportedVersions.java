
package com.travelport.schema.universal_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="urVersion" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="airVersion" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="hotelVersion" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="vehicleVersion" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="passiveVersion" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="railVersion" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="cruiseVersion" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "SupportedVersions")
public class SupportedVersions {

    @XmlAttribute(name = "urVersion")
    protected String urVersion;
    @XmlAttribute(name = "airVersion")
    protected String airVersion;
    @XmlAttribute(name = "hotelVersion")
    protected String hotelVersion;
    @XmlAttribute(name = "vehicleVersion")
    protected String vehicleVersion;
    @XmlAttribute(name = "passiveVersion")
    protected String passiveVersion;
    @XmlAttribute(name = "railVersion")
    protected String railVersion;
    @XmlAttribute(name = "cruiseVersion")
    protected String cruiseVersion;

    /**
     * Obtiene el valor de la propiedad urVersion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrVersion() {
        return urVersion;
    }

    /**
     * Define el valor de la propiedad urVersion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrVersion(String value) {
        this.urVersion = value;
    }

    /**
     * Obtiene el valor de la propiedad airVersion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirVersion() {
        return airVersion;
    }

    /**
     * Define el valor de la propiedad airVersion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirVersion(String value) {
        this.airVersion = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelVersion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelVersion() {
        return hotelVersion;
    }

    /**
     * Define el valor de la propiedad hotelVersion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelVersion(String value) {
        this.hotelVersion = value;
    }

    /**
     * Obtiene el valor de la propiedad vehicleVersion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleVersion() {
        return vehicleVersion;
    }

    /**
     * Define el valor de la propiedad vehicleVersion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleVersion(String value) {
        this.vehicleVersion = value;
    }

    /**
     * Obtiene el valor de la propiedad passiveVersion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassiveVersion() {
        return passiveVersion;
    }

    /**
     * Define el valor de la propiedad passiveVersion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassiveVersion(String value) {
        this.passiveVersion = value;
    }

    /**
     * Obtiene el valor de la propiedad railVersion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRailVersion() {
        return railVersion;
    }

    /**
     * Define el valor de la propiedad railVersion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRailVersion(String value) {
        this.railVersion = value;
    }

    /**
     * Obtiene el valor de la propiedad cruiseVersion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCruiseVersion() {
        return cruiseVersion;
    }

    /**
     * Define el valor de la propiedad cruiseVersion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCruiseVersion(String value) {
        this.cruiseVersion = value;
    }

}
