
package com.travelport.schema.universal_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="ClientID" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="BranchID" type="{http://www.travelport.com/schema/common_v43_0}typeBranchId" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "SearchAccount")
public class SearchAccount {

    @XmlAttribute(name = "ClientID")
    protected String clientID;
    @XmlAttribute(name = "BranchID")
    protected String branchID;

    /**
     * Obtiene el valor de la propiedad clientID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientID() {
        return clientID;
    }

    /**
     * Define el valor de la propiedad clientID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientID(String value) {
        this.clientID = value;
    }

    /**
     * Obtiene el valor de la propiedad branchID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBranchID() {
        return branchID;
    }

    /**
     * Define el valor de la propiedad branchID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBranchID(String value) {
        this.branchID = value;
    }

}
