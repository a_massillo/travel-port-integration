
package com.travelport.schema.universal_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.FormOfPayment;
import com.travelport.schema.common_v43_0.Payment;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BookingAction"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/common_v43_0}FormOfPayment" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Payment" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="Type" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ReservationLocatorCode" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeLocatorCode" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "bookingAction"
})
@XmlRootElement(name = "RailUpdate")
public class RailUpdate {

    @XmlElement(name = "BookingAction", required = true)
    protected RailUpdate.BookingAction bookingAction;
    @XmlAttribute(name = "ReservationLocatorCode", required = true)
    protected String reservationLocatorCode;

    /**
     * Obtiene el valor de la propiedad bookingAction.
     * 
     * @return
     *     possible object is
     *     {@link RailUpdate.BookingAction }
     *     
     */
    public RailUpdate.BookingAction getBookingAction() {
        return bookingAction;
    }

    /**
     * Define el valor de la propiedad bookingAction.
     * 
     * @param value
     *     allowed object is
     *     {@link RailUpdate.BookingAction }
     *     
     */
    public void setBookingAction(RailUpdate.BookingAction value) {
        this.bookingAction = value;
    }

    /**
     * Obtiene el valor de la propiedad reservationLocatorCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReservationLocatorCode() {
        return reservationLocatorCode;
    }

    /**
     * Define el valor de la propiedad reservationLocatorCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReservationLocatorCode(String value) {
        this.reservationLocatorCode = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}FormOfPayment" minOccurs="0"/&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Payment" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="Type" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "formOfPayment",
        "payment"
    })
    public static class BookingAction {

        @XmlElement(name = "FormOfPayment", namespace = "http://www.travelport.com/schema/common_v43_0")
        protected FormOfPayment formOfPayment;
        @XmlElement(name = "Payment", namespace = "http://www.travelport.com/schema/common_v43_0")
        protected Payment payment;
        @XmlAttribute(name = "Type", required = true)
        protected String type;

        /**
         * Obtiene el valor de la propiedad formOfPayment.
         * 
         * @return
         *     possible object is
         *     {@link FormOfPayment }
         *     
         */
        public FormOfPayment getFormOfPayment() {
            return formOfPayment;
        }

        /**
         * Define el valor de la propiedad formOfPayment.
         * 
         * @param value
         *     allowed object is
         *     {@link FormOfPayment }
         *     
         */
        public void setFormOfPayment(FormOfPayment value) {
            this.formOfPayment = value;
        }

        /**
         * Obtiene el valor de la propiedad payment.
         * 
         * @return
         *     possible object is
         *     {@link Payment }
         *     
         */
        public Payment getPayment() {
            return payment;
        }

        /**
         * Define el valor de la propiedad payment.
         * 
         * @param value
         *     allowed object is
         *     {@link Payment }
         *     
         */
        public void setPayment(Payment value) {
            this.payment = value;
        }

        /**
         * Obtiene el valor de la propiedad type.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getType() {
            return type;
        }

        /**
         * Define el valor de la propiedad type.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setType(String value) {
            this.type = value;
        }

    }

}
