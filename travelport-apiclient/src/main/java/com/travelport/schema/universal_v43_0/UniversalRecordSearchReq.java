
package com.travelport.schema.universal_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.travelport.schema.common_v43_0.BaseReq;
import com.travelport.schema.common_v43_0.TypeRecordStatus;
import com.travelport.schema.common_v43_0.URTicketStatus;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseReq"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}UniversalRecordSearchModifiers" minOccurs="0"/&gt;
 *         &lt;group ref="{http://www.travelport.com/schema/universal_v43_0}SearchCriteriaGroup"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}SearchAccount" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ActionDate" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *       &lt;attribute name="RecordStatus" type="{http://www.travelport.com/schema/common_v43_0}typeRecordStatus" /&gt;
 *       &lt;attribute name="ClientId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="ProviderCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="ProviderLocatorCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="ExternalSearchIndex" type="{http://www.travelport.com/schema/universal_v43_0}typeExternalSearchIndex" /&gt;
 *       &lt;attribute name="RestrictToProfileId" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="PassiveOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="TicketStatus" type="{http://www.travelport.com/schema/common_v43_0}URTicketStatus" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "universalRecordSearchModifiers",
    "travelerCriteria",
    "searchAgent",
    "airReservationCriteria",
    "hotelReservationCriteria",
    "vehicleReservationCriteria",
    "railReservationCriteria",
    "searchAccount"
})
@XmlRootElement(name = "UniversalRecordSearchReq")
public class UniversalRecordSearchReq
    extends BaseReq
{

    @XmlElement(name = "UniversalRecordSearchModifiers")
    protected UniversalRecordSearchModifiers universalRecordSearchModifiers;
    @XmlElement(name = "TravelerCriteria")
    protected TravelerCriteria travelerCriteria;
    @XmlElement(name = "SearchAgent")
    protected SearchAgent searchAgent;
    @XmlElement(name = "AirReservationCriteria")
    protected AirReservationCriteria airReservationCriteria;
    @XmlElement(name = "HotelReservationCriteria")
    protected HotelReservationCriteria hotelReservationCriteria;
    @XmlElement(name = "VehicleReservationCriteria")
    protected VehicleReservationCriteria vehicleReservationCriteria;
    @XmlElement(name = "RailReservationCriteria")
    protected RailReservationCriteria railReservationCriteria;
    @XmlElement(name = "SearchAccount")
    protected SearchAccount searchAccount;
    @XmlAttribute(name = "ActionDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar actionDate;
    @XmlAttribute(name = "RecordStatus")
    protected TypeRecordStatus recordStatus;
    @XmlAttribute(name = "ClientId")
    protected String clientId;
    @XmlAttribute(name = "ProviderCode")
    protected String providerCode;
    @XmlAttribute(name = "ProviderLocatorCode")
    protected String providerLocatorCode;
    @XmlAttribute(name = "ExternalSearchIndex")
    protected String externalSearchIndex;
    @XmlAttribute(name = "RestrictToProfileId")
    @XmlSchemaType(name = "anySimpleType")
    protected String restrictToProfileId;
    @XmlAttribute(name = "PassiveOnly")
    protected Boolean passiveOnly;
    @XmlAttribute(name = "TicketStatus")
    protected URTicketStatus ticketStatus;

    /**
     * Obtiene el valor de la propiedad universalRecordSearchModifiers.
     * 
     * @return
     *     possible object is
     *     {@link UniversalRecordSearchModifiers }
     *     
     */
    public UniversalRecordSearchModifiers getUniversalRecordSearchModifiers() {
        return universalRecordSearchModifiers;
    }

    /**
     * Define el valor de la propiedad universalRecordSearchModifiers.
     * 
     * @param value
     *     allowed object is
     *     {@link UniversalRecordSearchModifiers }
     *     
     */
    public void setUniversalRecordSearchModifiers(UniversalRecordSearchModifiers value) {
        this.universalRecordSearchModifiers = value;
    }

    /**
     * Obtiene el valor de la propiedad travelerCriteria.
     * 
     * @return
     *     possible object is
     *     {@link TravelerCriteria }
     *     
     */
    public TravelerCriteria getTravelerCriteria() {
        return travelerCriteria;
    }

    /**
     * Define el valor de la propiedad travelerCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link TravelerCriteria }
     *     
     */
    public void setTravelerCriteria(TravelerCriteria value) {
        this.travelerCriteria = value;
    }

    /**
     * Obtiene el valor de la propiedad searchAgent.
     * 
     * @return
     *     possible object is
     *     {@link SearchAgent }
     *     
     */
    public SearchAgent getSearchAgent() {
        return searchAgent;
    }

    /**
     * Define el valor de la propiedad searchAgent.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchAgent }
     *     
     */
    public void setSearchAgent(SearchAgent value) {
        this.searchAgent = value;
    }

    /**
     * Obtiene el valor de la propiedad airReservationCriteria.
     * 
     * @return
     *     possible object is
     *     {@link AirReservationCriteria }
     *     
     */
    public AirReservationCriteria getAirReservationCriteria() {
        return airReservationCriteria;
    }

    /**
     * Define el valor de la propiedad airReservationCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link AirReservationCriteria }
     *     
     */
    public void setAirReservationCriteria(AirReservationCriteria value) {
        this.airReservationCriteria = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelReservationCriteria.
     * 
     * @return
     *     possible object is
     *     {@link HotelReservationCriteria }
     *     
     */
    public HotelReservationCriteria getHotelReservationCriteria() {
        return hotelReservationCriteria;
    }

    /**
     * Define el valor de la propiedad hotelReservationCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelReservationCriteria }
     *     
     */
    public void setHotelReservationCriteria(HotelReservationCriteria value) {
        this.hotelReservationCriteria = value;
    }

    /**
     * Obtiene el valor de la propiedad vehicleReservationCriteria.
     * 
     * @return
     *     possible object is
     *     {@link VehicleReservationCriteria }
     *     
     */
    public VehicleReservationCriteria getVehicleReservationCriteria() {
        return vehicleReservationCriteria;
    }

    /**
     * Define el valor de la propiedad vehicleReservationCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleReservationCriteria }
     *     
     */
    public void setVehicleReservationCriteria(VehicleReservationCriteria value) {
        this.vehicleReservationCriteria = value;
    }

    /**
     * Obtiene el valor de la propiedad railReservationCriteria.
     * 
     * @return
     *     possible object is
     *     {@link RailReservationCriteria }
     *     
     */
    public RailReservationCriteria getRailReservationCriteria() {
        return railReservationCriteria;
    }

    /**
     * Define el valor de la propiedad railReservationCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link RailReservationCriteria }
     *     
     */
    public void setRailReservationCriteria(RailReservationCriteria value) {
        this.railReservationCriteria = value;
    }

    /**
     * Obtiene el valor de la propiedad searchAccount.
     * 
     * @return
     *     possible object is
     *     {@link SearchAccount }
     *     
     */
    public SearchAccount getSearchAccount() {
        return searchAccount;
    }

    /**
     * Define el valor de la propiedad searchAccount.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchAccount }
     *     
     */
    public void setSearchAccount(SearchAccount value) {
        this.searchAccount = value;
    }

    /**
     * Obtiene el valor de la propiedad actionDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getActionDate() {
        return actionDate;
    }

    /**
     * Define el valor de la propiedad actionDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setActionDate(XMLGregorianCalendar value) {
        this.actionDate = value;
    }

    /**
     * Obtiene el valor de la propiedad recordStatus.
     * 
     * @return
     *     possible object is
     *     {@link TypeRecordStatus }
     *     
     */
    public TypeRecordStatus getRecordStatus() {
        return recordStatus;
    }

    /**
     * Define el valor de la propiedad recordStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeRecordStatus }
     *     
     */
    public void setRecordStatus(TypeRecordStatus value) {
        this.recordStatus = value;
    }

    /**
     * Obtiene el valor de la propiedad clientId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * Define el valor de la propiedad clientId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientId(String value) {
        this.clientId = value;
    }

    /**
     * Obtiene el valor de la propiedad providerCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProviderCode() {
        return providerCode;
    }

    /**
     * Define el valor de la propiedad providerCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProviderCode(String value) {
        this.providerCode = value;
    }

    /**
     * Obtiene el valor de la propiedad providerLocatorCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProviderLocatorCode() {
        return providerLocatorCode;
    }

    /**
     * Define el valor de la propiedad providerLocatorCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProviderLocatorCode(String value) {
        this.providerLocatorCode = value;
    }

    /**
     * Obtiene el valor de la propiedad externalSearchIndex.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalSearchIndex() {
        return externalSearchIndex;
    }

    /**
     * Define el valor de la propiedad externalSearchIndex.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalSearchIndex(String value) {
        this.externalSearchIndex = value;
    }

    /**
     * Obtiene el valor de la propiedad restrictToProfileId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRestrictToProfileId() {
        return restrictToProfileId;
    }

    /**
     * Define el valor de la propiedad restrictToProfileId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRestrictToProfileId(String value) {
        this.restrictToProfileId = value;
    }

    /**
     * Obtiene el valor de la propiedad passiveOnly.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isPassiveOnly() {
        if (passiveOnly == null) {
            return false;
        } else {
            return passiveOnly;
        }
    }

    /**
     * Define el valor de la propiedad passiveOnly.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPassiveOnly(Boolean value) {
        this.passiveOnly = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketStatus.
     * 
     * @return
     *     possible object is
     *     {@link URTicketStatus }
     *     
     */
    public URTicketStatus getTicketStatus() {
        return ticketStatus;
    }

    /**
     * Define el valor de la propiedad ticketStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link URTicketStatus }
     *     
     */
    public void setTicketStatus(URTicketStatus value) {
        this.ticketStatus = value;
    }

}
