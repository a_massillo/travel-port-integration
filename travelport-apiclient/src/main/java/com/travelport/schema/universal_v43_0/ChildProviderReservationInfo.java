
package com.travelport.schema.universal_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/universal_v43_0}PNRDivideInfo"&gt;
 *       &lt;attribute name="UniversalLocatorCode" type="{http://www.travelport.com/schema/common_v43_0}typeLocatorCode" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "ChildProviderReservationInfo")
public class ChildProviderReservationInfo
    extends PNRDivideInfo
{

    @XmlAttribute(name = "UniversalLocatorCode")
    protected String universalLocatorCode;

    /**
     * Obtiene el valor de la propiedad universalLocatorCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniversalLocatorCode() {
        return universalLocatorCode;
    }

    /**
     * Define el valor de la propiedad universalLocatorCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniversalLocatorCode(String value) {
        this.universalLocatorCode = value;
    }

}
