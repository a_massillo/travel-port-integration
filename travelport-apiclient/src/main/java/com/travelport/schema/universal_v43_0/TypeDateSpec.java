
package com.travelport.schema.universal_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.travelport.schema.common_v43_0.TypeDateRange;


/**
 * Specifies dates as either specific date or a date range
 * 
 * <p>Clase Java para typeDateSpec complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="typeDateSpec"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="DateRange" type="{http://www.travelport.com/schema/common_v43_0}typeDateRange" minOccurs="0"/&gt;
 *         &lt;element name="SpecificDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "typeDateSpec", propOrder = {
    "dateRange",
    "specificDate"
})
public class TypeDateSpec {

    @XmlElement(name = "DateRange")
    protected TypeDateRange dateRange;
    @XmlElement(name = "SpecificDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar specificDate;

    /**
     * Obtiene el valor de la propiedad dateRange.
     * 
     * @return
     *     possible object is
     *     {@link TypeDateRange }
     *     
     */
    public TypeDateRange getDateRange() {
        return dateRange;
    }

    /**
     * Define el valor de la propiedad dateRange.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeDateRange }
     *     
     */
    public void setDateRange(TypeDateRange value) {
        this.dateRange = value;
    }

    /**
     * Obtiene el valor de la propiedad specificDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSpecificDate() {
        return specificDate;
    }

    /**
     * Define el valor de la propiedad specificDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSpecificDate(XMLGregorianCalendar value) {
        this.specificDate = value;
    }

}
