
package com.travelport.schema.universal_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseReq;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseReq"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}SavedTripSearchModifiers" minOccurs="0"/&gt;
 *         &lt;group ref="{http://www.travelport.com/schema/universal_v43_0}SearchCriteriaGroup"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="RecordStatus" type="{http://www.travelport.com/schema/universal_v43_0}typeSavedTripRecordStatus" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "savedTripSearchModifiers",
    "travelerCriteria",
    "searchAgent",
    "airReservationCriteria",
    "hotelReservationCriteria",
    "vehicleReservationCriteria",
    "railReservationCriteria"
})
@XmlRootElement(name = "SavedTripSearchReq")
public class SavedTripSearchReq
    extends BaseReq
{

    @XmlElement(name = "SavedTripSearchModifiers")
    protected SavedTripSearchModifiers savedTripSearchModifiers;
    @XmlElement(name = "TravelerCriteria")
    protected TravelerCriteria travelerCriteria;
    @XmlElement(name = "SearchAgent")
    protected SearchAgent searchAgent;
    @XmlElement(name = "AirReservationCriteria")
    protected AirReservationCriteria airReservationCriteria;
    @XmlElement(name = "HotelReservationCriteria")
    protected HotelReservationCriteria hotelReservationCriteria;
    @XmlElement(name = "VehicleReservationCriteria")
    protected VehicleReservationCriteria vehicleReservationCriteria;
    @XmlElement(name = "RailReservationCriteria")
    protected RailReservationCriteria railReservationCriteria;
    @XmlAttribute(name = "RecordStatus")
    protected TypeSavedTripRecordStatus recordStatus;

    /**
     * Obtiene el valor de la propiedad savedTripSearchModifiers.
     * 
     * @return
     *     possible object is
     *     {@link SavedTripSearchModifiers }
     *     
     */
    public SavedTripSearchModifiers getSavedTripSearchModifiers() {
        return savedTripSearchModifiers;
    }

    /**
     * Define el valor de la propiedad savedTripSearchModifiers.
     * 
     * @param value
     *     allowed object is
     *     {@link SavedTripSearchModifiers }
     *     
     */
    public void setSavedTripSearchModifiers(SavedTripSearchModifiers value) {
        this.savedTripSearchModifiers = value;
    }

    /**
     * Obtiene el valor de la propiedad travelerCriteria.
     * 
     * @return
     *     possible object is
     *     {@link TravelerCriteria }
     *     
     */
    public TravelerCriteria getTravelerCriteria() {
        return travelerCriteria;
    }

    /**
     * Define el valor de la propiedad travelerCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link TravelerCriteria }
     *     
     */
    public void setTravelerCriteria(TravelerCriteria value) {
        this.travelerCriteria = value;
    }

    /**
     * Obtiene el valor de la propiedad searchAgent.
     * 
     * @return
     *     possible object is
     *     {@link SearchAgent }
     *     
     */
    public SearchAgent getSearchAgent() {
        return searchAgent;
    }

    /**
     * Define el valor de la propiedad searchAgent.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchAgent }
     *     
     */
    public void setSearchAgent(SearchAgent value) {
        this.searchAgent = value;
    }

    /**
     * Obtiene el valor de la propiedad airReservationCriteria.
     * 
     * @return
     *     possible object is
     *     {@link AirReservationCriteria }
     *     
     */
    public AirReservationCriteria getAirReservationCriteria() {
        return airReservationCriteria;
    }

    /**
     * Define el valor de la propiedad airReservationCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link AirReservationCriteria }
     *     
     */
    public void setAirReservationCriteria(AirReservationCriteria value) {
        this.airReservationCriteria = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelReservationCriteria.
     * 
     * @return
     *     possible object is
     *     {@link HotelReservationCriteria }
     *     
     */
    public HotelReservationCriteria getHotelReservationCriteria() {
        return hotelReservationCriteria;
    }

    /**
     * Define el valor de la propiedad hotelReservationCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelReservationCriteria }
     *     
     */
    public void setHotelReservationCriteria(HotelReservationCriteria value) {
        this.hotelReservationCriteria = value;
    }

    /**
     * Obtiene el valor de la propiedad vehicleReservationCriteria.
     * 
     * @return
     *     possible object is
     *     {@link VehicleReservationCriteria }
     *     
     */
    public VehicleReservationCriteria getVehicleReservationCriteria() {
        return vehicleReservationCriteria;
    }

    /**
     * Define el valor de la propiedad vehicleReservationCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleReservationCriteria }
     *     
     */
    public void setVehicleReservationCriteria(VehicleReservationCriteria value) {
        this.vehicleReservationCriteria = value;
    }

    /**
     * Obtiene el valor de la propiedad railReservationCriteria.
     * 
     * @return
     *     possible object is
     *     {@link RailReservationCriteria }
     *     
     */
    public RailReservationCriteria getRailReservationCriteria() {
        return railReservationCriteria;
    }

    /**
     * Define el valor de la propiedad railReservationCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link RailReservationCriteria }
     *     
     */
    public void setRailReservationCriteria(RailReservationCriteria value) {
        this.railReservationCriteria = value;
    }

    /**
     * Obtiene el valor de la propiedad recordStatus.
     * 
     * @return
     *     possible object is
     *     {@link TypeSavedTripRecordStatus }
     *     
     */
    public TypeSavedTripRecordStatus getRecordStatus() {
        return recordStatus;
    }

    /**
     * Define el valor de la propiedad recordStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeSavedTripRecordStatus }
     *     
     */
    public void setRecordStatus(TypeSavedTripRecordStatus value) {
        this.recordStatus = value;
    }

}
