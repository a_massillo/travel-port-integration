
package com.travelport.schema.universal_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}VehicleAdd" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}VehicleDelete" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}VehicleUpdate" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}AirAdd" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}AirDelete" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}AirUpdate" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}UniversalAdd" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}UniversalDelete" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}UniversalUpdate" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}HotelAdd" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}HotelUpdate" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}HotelDelete" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}PassiveAdd" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}PassiveDelete" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}RailUpdate" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *       &lt;attribute name="Key" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vehicleAdd",
    "vehicleDelete",
    "vehicleUpdate",
    "airAdd",
    "airDelete",
    "airUpdate",
    "universalAdd",
    "universalDelete",
    "universalUpdate",
    "hotelAdd",
    "hotelUpdate",
    "hotelDelete",
    "passiveAdd",
    "passiveDelete",
    "railUpdate"
})
@XmlRootElement(name = "UniversalModifyCmd")
public class UniversalModifyCmd {

    @XmlElement(name = "VehicleAdd")
    protected VehicleAdd vehicleAdd;
    @XmlElement(name = "VehicleDelete")
    protected VehicleDelete vehicleDelete;
    @XmlElement(name = "VehicleUpdate")
    protected VehicleUpdate vehicleUpdate;
    @XmlElement(name = "AirAdd")
    protected AirAdd airAdd;
    @XmlElement(name = "AirDelete")
    protected AirDelete airDelete;
    @XmlElement(name = "AirUpdate")
    protected AirUpdate airUpdate;
    @XmlElement(name = "UniversalAdd")
    protected UniversalAdd universalAdd;
    @XmlElement(name = "UniversalDelete")
    protected UniversalDelete universalDelete;
    @XmlElement(name = "UniversalUpdate")
    protected UniversalUpdate universalUpdate;
    @XmlElement(name = "HotelAdd")
    protected HotelAdd hotelAdd;
    @XmlElement(name = "HotelUpdate")
    protected HotelUpdate hotelUpdate;
    @XmlElement(name = "HotelDelete")
    protected HotelDelete hotelDelete;
    @XmlElement(name = "PassiveAdd")
    protected PassiveAdd passiveAdd;
    @XmlElement(name = "PassiveDelete")
    protected PassiveDelete passiveDelete;
    @XmlElement(name = "RailUpdate")
    protected RailUpdate railUpdate;
    @XmlAttribute(name = "Key", required = true)
    protected String key;

    /**
     * Obtiene el valor de la propiedad vehicleAdd.
     * 
     * @return
     *     possible object is
     *     {@link VehicleAdd }
     *     
     */
    public VehicleAdd getVehicleAdd() {
        return vehicleAdd;
    }

    /**
     * Define el valor de la propiedad vehicleAdd.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleAdd }
     *     
     */
    public void setVehicleAdd(VehicleAdd value) {
        this.vehicleAdd = value;
    }

    /**
     * Obtiene el valor de la propiedad vehicleDelete.
     * 
     * @return
     *     possible object is
     *     {@link VehicleDelete }
     *     
     */
    public VehicleDelete getVehicleDelete() {
        return vehicleDelete;
    }

    /**
     * Define el valor de la propiedad vehicleDelete.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleDelete }
     *     
     */
    public void setVehicleDelete(VehicleDelete value) {
        this.vehicleDelete = value;
    }

    /**
     * Obtiene el valor de la propiedad vehicleUpdate.
     * 
     * @return
     *     possible object is
     *     {@link VehicleUpdate }
     *     
     */
    public VehicleUpdate getVehicleUpdate() {
        return vehicleUpdate;
    }

    /**
     * Define el valor de la propiedad vehicleUpdate.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleUpdate }
     *     
     */
    public void setVehicleUpdate(VehicleUpdate value) {
        this.vehicleUpdate = value;
    }

    /**
     * Obtiene el valor de la propiedad airAdd.
     * 
     * @return
     *     possible object is
     *     {@link AirAdd }
     *     
     */
    public AirAdd getAirAdd() {
        return airAdd;
    }

    /**
     * Define el valor de la propiedad airAdd.
     * 
     * @param value
     *     allowed object is
     *     {@link AirAdd }
     *     
     */
    public void setAirAdd(AirAdd value) {
        this.airAdd = value;
    }

    /**
     * Obtiene el valor de la propiedad airDelete.
     * 
     * @return
     *     possible object is
     *     {@link AirDelete }
     *     
     */
    public AirDelete getAirDelete() {
        return airDelete;
    }

    /**
     * Define el valor de la propiedad airDelete.
     * 
     * @param value
     *     allowed object is
     *     {@link AirDelete }
     *     
     */
    public void setAirDelete(AirDelete value) {
        this.airDelete = value;
    }

    /**
     * Obtiene el valor de la propiedad airUpdate.
     * 
     * @return
     *     possible object is
     *     {@link AirUpdate }
     *     
     */
    public AirUpdate getAirUpdate() {
        return airUpdate;
    }

    /**
     * Define el valor de la propiedad airUpdate.
     * 
     * @param value
     *     allowed object is
     *     {@link AirUpdate }
     *     
     */
    public void setAirUpdate(AirUpdate value) {
        this.airUpdate = value;
    }

    /**
     * Obtiene el valor de la propiedad universalAdd.
     * 
     * @return
     *     possible object is
     *     {@link UniversalAdd }
     *     
     */
    public UniversalAdd getUniversalAdd() {
        return universalAdd;
    }

    /**
     * Define el valor de la propiedad universalAdd.
     * 
     * @param value
     *     allowed object is
     *     {@link UniversalAdd }
     *     
     */
    public void setUniversalAdd(UniversalAdd value) {
        this.universalAdd = value;
    }

    /**
     * Obtiene el valor de la propiedad universalDelete.
     * 
     * @return
     *     possible object is
     *     {@link UniversalDelete }
     *     
     */
    public UniversalDelete getUniversalDelete() {
        return universalDelete;
    }

    /**
     * Define el valor de la propiedad universalDelete.
     * 
     * @param value
     *     allowed object is
     *     {@link UniversalDelete }
     *     
     */
    public void setUniversalDelete(UniversalDelete value) {
        this.universalDelete = value;
    }

    /**
     * Obtiene el valor de la propiedad universalUpdate.
     * 
     * @return
     *     possible object is
     *     {@link UniversalUpdate }
     *     
     */
    public UniversalUpdate getUniversalUpdate() {
        return universalUpdate;
    }

    /**
     * Define el valor de la propiedad universalUpdate.
     * 
     * @param value
     *     allowed object is
     *     {@link UniversalUpdate }
     *     
     */
    public void setUniversalUpdate(UniversalUpdate value) {
        this.universalUpdate = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelAdd.
     * 
     * @return
     *     possible object is
     *     {@link HotelAdd }
     *     
     */
    public HotelAdd getHotelAdd() {
        return hotelAdd;
    }

    /**
     * Define el valor de la propiedad hotelAdd.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelAdd }
     *     
     */
    public void setHotelAdd(HotelAdd value) {
        this.hotelAdd = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelUpdate.
     * 
     * @return
     *     possible object is
     *     {@link HotelUpdate }
     *     
     */
    public HotelUpdate getHotelUpdate() {
        return hotelUpdate;
    }

    /**
     * Define el valor de la propiedad hotelUpdate.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelUpdate }
     *     
     */
    public void setHotelUpdate(HotelUpdate value) {
        this.hotelUpdate = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelDelete.
     * 
     * @return
     *     possible object is
     *     {@link HotelDelete }
     *     
     */
    public HotelDelete getHotelDelete() {
        return hotelDelete;
    }

    /**
     * Define el valor de la propiedad hotelDelete.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelDelete }
     *     
     */
    public void setHotelDelete(HotelDelete value) {
        this.hotelDelete = value;
    }

    /**
     * Obtiene el valor de la propiedad passiveAdd.
     * 
     * @return
     *     possible object is
     *     {@link PassiveAdd }
     *     
     */
    public PassiveAdd getPassiveAdd() {
        return passiveAdd;
    }

    /**
     * Define el valor de la propiedad passiveAdd.
     * 
     * @param value
     *     allowed object is
     *     {@link PassiveAdd }
     *     
     */
    public void setPassiveAdd(PassiveAdd value) {
        this.passiveAdd = value;
    }

    /**
     * Obtiene el valor de la propiedad passiveDelete.
     * 
     * @return
     *     possible object is
     *     {@link PassiveDelete }
     *     
     */
    public PassiveDelete getPassiveDelete() {
        return passiveDelete;
    }

    /**
     * Define el valor de la propiedad passiveDelete.
     * 
     * @param value
     *     allowed object is
     *     {@link PassiveDelete }
     *     
     */
    public void setPassiveDelete(PassiveDelete value) {
        this.passiveDelete = value;
    }

    /**
     * Obtiene el valor de la propiedad railUpdate.
     * 
     * @return
     *     possible object is
     *     {@link RailUpdate }
     *     
     */
    public RailUpdate getRailUpdate() {
        return railUpdate;
    }

    /**
     * Define el valor de la propiedad railUpdate.
     * 
     * @param value
     *     allowed object is
     *     {@link RailUpdate }
     *     
     */
    public void setRailUpdate(RailUpdate value) {
        this.railUpdate = value;
    }

    /**
     * Obtiene el valor de la propiedad key.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Define el valor de la propiedad key.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

}
