
package com.travelport.schema.universal_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="JourneyDepartureDate" type="{http://www.travelport.com/schema/universal_v43_0}typeDateSpec" minOccurs="0"/&gt;
 *         &lt;element name="JourneyArrivalDate" type="{http://www.travelport.com/schema/universal_v43_0}typeDateSpec" minOccurs="0"/&gt;
 *         &lt;element name="SegmentDepartureDate" type="{http://www.travelport.com/schema/universal_v43_0}typeDateSpec" minOccurs="0"/&gt;
 *         &lt;element name="SegmentArrivalDate" type="{http://www.travelport.com/schema/universal_v43_0}typeDateSpec" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="JourneyOrigin" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *       &lt;attribute name="JourneyDestination" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *       &lt;attribute name="JourneyRailLocOrigin" type="{http://www.travelport.com/schema/common_v43_0}typeRailLocationCode" /&gt;
 *       &lt;attribute name="JourneyRailLocDestination" type="{http://www.travelport.com/schema/common_v43_0}typeRailLocationCode" /&gt;
 *       &lt;attribute name="SupplierCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="TrainNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="PassiveOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "journeyDepartureDate",
    "journeyArrivalDate",
    "segmentDepartureDate",
    "segmentArrivalDate"
})
@XmlRootElement(name = "RailReservationCriteria")
public class RailReservationCriteria {

    @XmlElement(name = "JourneyDepartureDate")
    protected TypeDateSpec journeyDepartureDate;
    @XmlElement(name = "JourneyArrivalDate")
    protected TypeDateSpec journeyArrivalDate;
    @XmlElement(name = "SegmentDepartureDate")
    protected TypeDateSpec segmentDepartureDate;
    @XmlElement(name = "SegmentArrivalDate")
    protected TypeDateSpec segmentArrivalDate;
    @XmlAttribute(name = "JourneyOrigin")
    protected String journeyOrigin;
    @XmlAttribute(name = "JourneyDestination")
    protected String journeyDestination;
    @XmlAttribute(name = "JourneyRailLocOrigin")
    protected String journeyRailLocOrigin;
    @XmlAttribute(name = "JourneyRailLocDestination")
    protected String journeyRailLocDestination;
    @XmlAttribute(name = "SupplierCode")
    protected String supplierCode;
    @XmlAttribute(name = "TrainNumber")
    protected String trainNumber;
    @XmlAttribute(name = "PassiveOnly")
    protected Boolean passiveOnly;

    /**
     * Obtiene el valor de la propiedad journeyDepartureDate.
     * 
     * @return
     *     possible object is
     *     {@link TypeDateSpec }
     *     
     */
    public TypeDateSpec getJourneyDepartureDate() {
        return journeyDepartureDate;
    }

    /**
     * Define el valor de la propiedad journeyDepartureDate.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeDateSpec }
     *     
     */
    public void setJourneyDepartureDate(TypeDateSpec value) {
        this.journeyDepartureDate = value;
    }

    /**
     * Obtiene el valor de la propiedad journeyArrivalDate.
     * 
     * @return
     *     possible object is
     *     {@link TypeDateSpec }
     *     
     */
    public TypeDateSpec getJourneyArrivalDate() {
        return journeyArrivalDate;
    }

    /**
     * Define el valor de la propiedad journeyArrivalDate.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeDateSpec }
     *     
     */
    public void setJourneyArrivalDate(TypeDateSpec value) {
        this.journeyArrivalDate = value;
    }

    /**
     * Obtiene el valor de la propiedad segmentDepartureDate.
     * 
     * @return
     *     possible object is
     *     {@link TypeDateSpec }
     *     
     */
    public TypeDateSpec getSegmentDepartureDate() {
        return segmentDepartureDate;
    }

    /**
     * Define el valor de la propiedad segmentDepartureDate.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeDateSpec }
     *     
     */
    public void setSegmentDepartureDate(TypeDateSpec value) {
        this.segmentDepartureDate = value;
    }

    /**
     * Obtiene el valor de la propiedad segmentArrivalDate.
     * 
     * @return
     *     possible object is
     *     {@link TypeDateSpec }
     *     
     */
    public TypeDateSpec getSegmentArrivalDate() {
        return segmentArrivalDate;
    }

    /**
     * Define el valor de la propiedad segmentArrivalDate.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeDateSpec }
     *     
     */
    public void setSegmentArrivalDate(TypeDateSpec value) {
        this.segmentArrivalDate = value;
    }

    /**
     * Obtiene el valor de la propiedad journeyOrigin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJourneyOrigin() {
        return journeyOrigin;
    }

    /**
     * Define el valor de la propiedad journeyOrigin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJourneyOrigin(String value) {
        this.journeyOrigin = value;
    }

    /**
     * Obtiene el valor de la propiedad journeyDestination.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJourneyDestination() {
        return journeyDestination;
    }

    /**
     * Define el valor de la propiedad journeyDestination.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJourneyDestination(String value) {
        this.journeyDestination = value;
    }

    /**
     * Obtiene el valor de la propiedad journeyRailLocOrigin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJourneyRailLocOrigin() {
        return journeyRailLocOrigin;
    }

    /**
     * Define el valor de la propiedad journeyRailLocOrigin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJourneyRailLocOrigin(String value) {
        this.journeyRailLocOrigin = value;
    }

    /**
     * Obtiene el valor de la propiedad journeyRailLocDestination.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJourneyRailLocDestination() {
        return journeyRailLocDestination;
    }

    /**
     * Define el valor de la propiedad journeyRailLocDestination.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJourneyRailLocDestination(String value) {
        this.journeyRailLocDestination = value;
    }

    /**
     * Obtiene el valor de la propiedad supplierCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupplierCode() {
        return supplierCode;
    }

    /**
     * Define el valor de la propiedad supplierCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupplierCode(String value) {
        this.supplierCode = value;
    }

    /**
     * Obtiene el valor de la propiedad trainNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrainNumber() {
        return trainNumber;
    }

    /**
     * Define el valor de la propiedad trainNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrainNumber(String value) {
        this.trainNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad passiveOnly.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isPassiveOnly() {
        if (passiveOnly == null) {
            return false;
        } else {
            return passiveOnly;
        }
    }

    /**
     * Define el valor de la propiedad passiveOnly.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPassiveOnly(Boolean value) {
        this.passiveOnly = value;
    }

}
