
package com.travelport.schema.universal_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseRsp;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseRsp"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}ParentProviderReservationInfo"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}ChildProviderReservationInfo"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "parentProviderReservationInfo",
    "childProviderReservationInfo"
})
@XmlRootElement(name = "ProviderReservationDivideRsp")
public class ProviderReservationDivideRsp
    extends BaseRsp
{

    @XmlElement(name = "ParentProviderReservationInfo", required = true)
    protected ParentProviderReservationInfo parentProviderReservationInfo;
    @XmlElement(name = "ChildProviderReservationInfo", required = true)
    protected ChildProviderReservationInfo childProviderReservationInfo;

    /**
     * Obtiene el valor de la propiedad parentProviderReservationInfo.
     * 
     * @return
     *     possible object is
     *     {@link ParentProviderReservationInfo }
     *     
     */
    public ParentProviderReservationInfo getParentProviderReservationInfo() {
        return parentProviderReservationInfo;
    }

    /**
     * Define el valor de la propiedad parentProviderReservationInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ParentProviderReservationInfo }
     *     
     */
    public void setParentProviderReservationInfo(ParentProviderReservationInfo value) {
        this.parentProviderReservationInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad childProviderReservationInfo.
     * 
     * @return
     *     possible object is
     *     {@link ChildProviderReservationInfo }
     *     
     */
    public ChildProviderReservationInfo getChildProviderReservationInfo() {
        return childProviderReservationInfo;
    }

    /**
     * Define el valor de la propiedad childProviderReservationInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ChildProviderReservationInfo }
     *     
     */
    public void setChildProviderReservationInfo(ChildProviderReservationInfo value) {
        this.childProviderReservationInfo = value;
    }

}
