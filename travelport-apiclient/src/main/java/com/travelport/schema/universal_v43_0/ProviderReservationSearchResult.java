
package com.travelport.schema.universal_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.AgencyInfo;
import com.travelport.schema.common_v43_0.Name;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Name" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element name="ProductInfo" type="{http://www.travelport.com/schema/universal_v43_0}typeProductInfo" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}AgencyInfo" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="UniversalRecordLocatorCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="CreatedDate" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="EarliestTravelDate" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="Ticketed" use="required" type="{http://www.travelport.com/schema/universal_v43_0}typeReservationTicketed" /&gt;
 *       &lt;attribute name="ProviderCode" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="ProviderLocatorCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="ExternalSearchIndex" type="{http://www.travelport.com/schema/universal_v43_0}typeExternalSearchIndex" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "name",
    "productInfo",
    "agencyInfo"
})
@XmlRootElement(name = "ProviderReservationSearchResult")
public class ProviderReservationSearchResult {

    @XmlElement(name = "Name", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected List<Name> name;
    @XmlElement(name = "ProductInfo")
    protected List<TypeProductInfo> productInfo;
    @XmlElement(name = "AgencyInfo", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected AgencyInfo agencyInfo;
    @XmlAttribute(name = "UniversalRecordLocatorCode")
    protected String universalRecordLocatorCode;
    @XmlAttribute(name = "CreatedDate")
    protected String createdDate;
    @XmlAttribute(name = "EarliestTravelDate")
    protected String earliestTravelDate;
    @XmlAttribute(name = "Ticketed", required = true)
    protected TypeReservationTicketed ticketed;
    @XmlAttribute(name = "ProviderCode", required = true)
    protected String providerCode;
    @XmlAttribute(name = "ProviderLocatorCode")
    protected String providerLocatorCode;
    @XmlAttribute(name = "ExternalSearchIndex")
    protected String externalSearchIndex;

    /**
     * Gets the value of the name property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the name property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Name }
     * 
     * 
     */
    public List<Name> getName() {
        if (name == null) {
            name = new ArrayList<Name>();
        }
        return this.name;
    }

    /**
     * Gets the value of the productInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TypeProductInfo }
     * 
     * 
     */
    public List<TypeProductInfo> getProductInfo() {
        if (productInfo == null) {
            productInfo = new ArrayList<TypeProductInfo>();
        }
        return this.productInfo;
    }

    /**
     * Obtiene el valor de la propiedad agencyInfo.
     * 
     * @return
     *     possible object is
     *     {@link AgencyInfo }
     *     
     */
    public AgencyInfo getAgencyInfo() {
        return agencyInfo;
    }

    /**
     * Define el valor de la propiedad agencyInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link AgencyInfo }
     *     
     */
    public void setAgencyInfo(AgencyInfo value) {
        this.agencyInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad universalRecordLocatorCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniversalRecordLocatorCode() {
        return universalRecordLocatorCode;
    }

    /**
     * Define el valor de la propiedad universalRecordLocatorCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniversalRecordLocatorCode(String value) {
        this.universalRecordLocatorCode = value;
    }

    /**
     * Obtiene el valor de la propiedad createdDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatedDate() {
        return createdDate;
    }

    /**
     * Define el valor de la propiedad createdDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatedDate(String value) {
        this.createdDate = value;
    }

    /**
     * Obtiene el valor de la propiedad earliestTravelDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEarliestTravelDate() {
        return earliestTravelDate;
    }

    /**
     * Define el valor de la propiedad earliestTravelDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEarliestTravelDate(String value) {
        this.earliestTravelDate = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketed.
     * 
     * @return
     *     possible object is
     *     {@link TypeReservationTicketed }
     *     
     */
    public TypeReservationTicketed getTicketed() {
        return ticketed;
    }

    /**
     * Define el valor de la propiedad ticketed.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeReservationTicketed }
     *     
     */
    public void setTicketed(TypeReservationTicketed value) {
        this.ticketed = value;
    }

    /**
     * Obtiene el valor de la propiedad providerCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProviderCode() {
        return providerCode;
    }

    /**
     * Define el valor de la propiedad providerCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProviderCode(String value) {
        this.providerCode = value;
    }

    /**
     * Obtiene el valor de la propiedad providerLocatorCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProviderLocatorCode() {
        return providerLocatorCode;
    }

    /**
     * Define el valor de la propiedad providerLocatorCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProviderLocatorCode(String value) {
        this.providerLocatorCode = value;
    }

    /**
     * Obtiene el valor de la propiedad externalSearchIndex.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalSearchIndex() {
        return externalSearchIndex;
    }

    /**
     * Define el valor de la propiedad externalSearchIndex.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalSearchIndex(String value) {
        this.externalSearchIndex = value;
    }

}
