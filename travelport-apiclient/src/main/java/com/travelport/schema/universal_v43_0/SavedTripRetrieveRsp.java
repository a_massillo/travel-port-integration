
package com.travelport.schema.universal_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseRsp;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseRsp"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}SavedTrip" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "savedTrip"
})
@XmlRootElement(name = "SavedTripRetrieveRsp")
public class SavedTripRetrieveRsp
    extends BaseRsp
{

    @XmlElement(name = "SavedTrip")
    protected SavedTrip savedTrip;

    /**
     * Obtiene el valor de la propiedad savedTrip.
     * 
     * @return
     *     possible object is
     *     {@link SavedTrip }
     *     
     */
    public SavedTrip getSavedTrip() {
        return savedTrip;
    }

    /**
     * Define el valor de la propiedad savedTrip.
     * 
     * @param value
     *     allowed object is
     *     {@link SavedTrip }
     *     
     */
    public void setSavedTrip(SavedTrip value) {
        this.savedTrip = value;
    }

}
