
package com.travelport.schema.universal_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;simpleContent&gt;
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *       &lt;attribute name="UniversalModifyCmdKey" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/simpleContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "value"
})
@XmlRootElement(name = "UniversalModifyCommandError")
public class UniversalModifyCommandError {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "UniversalModifyCmdKey", required = true)
    protected String universalModifyCmdKey;

    /**
     * Obtiene el valor de la propiedad value.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Obtiene el valor de la propiedad universalModifyCmdKey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniversalModifyCmdKey() {
        return universalModifyCmdKey;
    }

    /**
     * Define el valor de la propiedad universalModifyCmdKey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniversalModifyCmdKey(String value) {
        this.universalModifyCmdKey = value;
    }

}
