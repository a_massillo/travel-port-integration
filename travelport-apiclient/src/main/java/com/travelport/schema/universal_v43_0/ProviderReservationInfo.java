
package com.travelport.schema.universal_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.travelport.schema.common_v43_0.TypeElementStatus;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}ProviderReservationDetails" minOccurs="0"/&gt;
 *           &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}ProviderReservationDisplayDetailsList" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}ExternalReservationInfo" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attGroup ref="{http://www.travelport.com/schema/common_v43_0}attrAgentOverride"/&gt;
 *       &lt;attGroup ref="{http://www.travelport.com/schema/common_v43_0}attrElementKeyResults"/&gt;
 *       &lt;attribute name="Key" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="ProviderCode" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeProviderCode" /&gt;
 *       &lt;attribute name="LocatorCode" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeProviderLocatorCode" /&gt;
 *       &lt;attribute name="CreateDate" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *       &lt;attribute name="HostCreateDate" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *       &lt;attribute name="HostCreateTime" type="{http://www.w3.org/2001/XMLSchema}time" /&gt;
 *       &lt;attribute name="ModifiedDate" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *       &lt;attribute name="Imported" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="TicketingModifiersRef" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="InQueueMode" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="GroupRef" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="OwningPCC" type="{http://www.travelport.com/schema/common_v43_0}typePCC" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "providerReservationDetails",
    "providerReservationDisplayDetailsList",
    "externalReservationInfo"
})
@XmlRootElement(name = "ProviderReservationInfo")
public class ProviderReservationInfo {

    @XmlElement(name = "ProviderReservationDetails")
    protected ProviderReservationDetails providerReservationDetails;
    @XmlElement(name = "ProviderReservationDisplayDetailsList")
    protected ProviderReservationDisplayDetailsList providerReservationDisplayDetailsList;
    @XmlElement(name = "ExternalReservationInfo")
    protected ExternalReservationInfo externalReservationInfo;
    @XmlAttribute(name = "Key", required = true)
    protected String key;
    @XmlAttribute(name = "ProviderCode", required = true)
    protected String providerCode;
    @XmlAttribute(name = "LocatorCode", required = true)
    protected String locatorCode;
    @XmlAttribute(name = "CreateDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar createDate;
    @XmlAttribute(name = "HostCreateDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar hostCreateDate;
    @XmlAttribute(name = "HostCreateTime")
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar hostCreateTime;
    @XmlAttribute(name = "ModifiedDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar modifiedDate;
    @XmlAttribute(name = "Imported")
    protected Boolean imported;
    @XmlAttribute(name = "TicketingModifiersRef")
    protected String ticketingModifiersRef;
    @XmlAttribute(name = "InQueueMode")
    protected Boolean inQueueMode;
    @XmlAttribute(name = "GroupRef")
    protected String groupRef;
    @XmlAttribute(name = "OwningPCC")
    protected String owningPCC;
    @XmlAttribute(name = "AgentOverride")
    protected String agentOverride;
    @XmlAttribute(name = "ElStat")
    protected TypeElementStatus elStat;
    @XmlAttribute(name = "KeyOverride")
    protected Boolean keyOverride;

    /**
     * Obtiene el valor de la propiedad providerReservationDetails.
     * 
     * @return
     *     possible object is
     *     {@link ProviderReservationDetails }
     *     
     */
    public ProviderReservationDetails getProviderReservationDetails() {
        return providerReservationDetails;
    }

    /**
     * Define el valor de la propiedad providerReservationDetails.
     * 
     * @param value
     *     allowed object is
     *     {@link ProviderReservationDetails }
     *     
     */
    public void setProviderReservationDetails(ProviderReservationDetails value) {
        this.providerReservationDetails = value;
    }

    /**
     * Obtiene el valor de la propiedad providerReservationDisplayDetailsList.
     * 
     * @return
     *     possible object is
     *     {@link ProviderReservationDisplayDetailsList }
     *     
     */
    public ProviderReservationDisplayDetailsList getProviderReservationDisplayDetailsList() {
        return providerReservationDisplayDetailsList;
    }

    /**
     * Define el valor de la propiedad providerReservationDisplayDetailsList.
     * 
     * @param value
     *     allowed object is
     *     {@link ProviderReservationDisplayDetailsList }
     *     
     */
    public void setProviderReservationDisplayDetailsList(ProviderReservationDisplayDetailsList value) {
        this.providerReservationDisplayDetailsList = value;
    }

    /**
     * Obtiene el valor de la propiedad externalReservationInfo.
     * 
     * @return
     *     possible object is
     *     {@link ExternalReservationInfo }
     *     
     */
    public ExternalReservationInfo getExternalReservationInfo() {
        return externalReservationInfo;
    }

    /**
     * Define el valor de la propiedad externalReservationInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ExternalReservationInfo }
     *     
     */
    public void setExternalReservationInfo(ExternalReservationInfo value) {
        this.externalReservationInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad key.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Define el valor de la propiedad key.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Obtiene el valor de la propiedad providerCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProviderCode() {
        return providerCode;
    }

    /**
     * Define el valor de la propiedad providerCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProviderCode(String value) {
        this.providerCode = value;
    }

    /**
     * Obtiene el valor de la propiedad locatorCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocatorCode() {
        return locatorCode;
    }

    /**
     * Define el valor de la propiedad locatorCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocatorCode(String value) {
        this.locatorCode = value;
    }

    /**
     * Obtiene el valor de la propiedad createDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreateDate() {
        return createDate;
    }

    /**
     * Define el valor de la propiedad createDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreateDate(XMLGregorianCalendar value) {
        this.createDate = value;
    }

    /**
     * Obtiene el valor de la propiedad hostCreateDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getHostCreateDate() {
        return hostCreateDate;
    }

    /**
     * Define el valor de la propiedad hostCreateDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setHostCreateDate(XMLGregorianCalendar value) {
        this.hostCreateDate = value;
    }

    /**
     * Obtiene el valor de la propiedad hostCreateTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getHostCreateTime() {
        return hostCreateTime;
    }

    /**
     * Define el valor de la propiedad hostCreateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setHostCreateTime(XMLGregorianCalendar value) {
        this.hostCreateTime = value;
    }

    /**
     * Obtiene el valor de la propiedad modifiedDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getModifiedDate() {
        return modifiedDate;
    }

    /**
     * Define el valor de la propiedad modifiedDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setModifiedDate(XMLGregorianCalendar value) {
        this.modifiedDate = value;
    }

    /**
     * Obtiene el valor de la propiedad imported.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isImported() {
        return imported;
    }

    /**
     * Define el valor de la propiedad imported.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setImported(Boolean value) {
        this.imported = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketingModifiersRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketingModifiersRef() {
        return ticketingModifiersRef;
    }

    /**
     * Define el valor de la propiedad ticketingModifiersRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketingModifiersRef(String value) {
        this.ticketingModifiersRef = value;
    }

    /**
     * Obtiene el valor de la propiedad inQueueMode.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isInQueueMode() {
        return inQueueMode;
    }

    /**
     * Define el valor de la propiedad inQueueMode.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInQueueMode(Boolean value) {
        this.inQueueMode = value;
    }

    /**
     * Obtiene el valor de la propiedad groupRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupRef() {
        return groupRef;
    }

    /**
     * Define el valor de la propiedad groupRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupRef(String value) {
        this.groupRef = value;
    }

    /**
     * Obtiene el valor de la propiedad owningPCC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwningPCC() {
        return owningPCC;
    }

    /**
     * Define el valor de la propiedad owningPCC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwningPCC(String value) {
        this.owningPCC = value;
    }

    /**
     * Obtiene el valor de la propiedad agentOverride.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentOverride() {
        return agentOverride;
    }

    /**
     * Define el valor de la propiedad agentOverride.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentOverride(String value) {
        this.agentOverride = value;
    }

    /**
     * Obtiene el valor de la propiedad elStat.
     * 
     * @return
     *     possible object is
     *     {@link TypeElementStatus }
     *     
     */
    public TypeElementStatus getElStat() {
        return elStat;
    }

    /**
     * Define el valor de la propiedad elStat.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeElementStatus }
     *     
     */
    public void setElStat(TypeElementStatus value) {
        this.elStat = value;
    }

    /**
     * Obtiene el valor de la propiedad keyOverride.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isKeyOverride() {
        return keyOverride;
    }

    /**
     * Define el valor de la propiedad keyOverride.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setKeyOverride(Boolean value) {
        this.keyOverride = value;
    }

}
