
package com.travelport.schema.universal_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseRsp;
import com.travelport.schema.hotel_v43_0.HotelProperty;
import com.travelport.schema.hotel_v43_0.HotelRateDetail;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseRsp"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/universal_v43_0}UniversalRecord" minOccurs="0"/&gt;
 *         &lt;element name="HotelRateChangedInfo" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/hotel_v43_0}HotelProperty"/&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/hotel_v43_0}HotelRateDetail" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="Reason" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "universalRecord",
    "hotelRateChangedInfo"
})
@XmlRootElement(name = "HotelCreateReservationRsp")
public class HotelCreateReservationRsp
    extends BaseRsp
{

    @XmlElement(name = "UniversalRecord")
    protected UniversalRecord universalRecord;
    @XmlElement(name = "HotelRateChangedInfo")
    protected HotelCreateReservationRsp.HotelRateChangedInfo hotelRateChangedInfo;

    /**
     * Obtiene el valor de la propiedad universalRecord.
     * 
     * @return
     *     possible object is
     *     {@link UniversalRecord }
     *     
     */
    public UniversalRecord getUniversalRecord() {
        return universalRecord;
    }

    /**
     * Define el valor de la propiedad universalRecord.
     * 
     * @param value
     *     allowed object is
     *     {@link UniversalRecord }
     *     
     */
    public void setUniversalRecord(UniversalRecord value) {
        this.universalRecord = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelRateChangedInfo.
     * 
     * @return
     *     possible object is
     *     {@link HotelCreateReservationRsp.HotelRateChangedInfo }
     *     
     */
    public HotelCreateReservationRsp.HotelRateChangedInfo getHotelRateChangedInfo() {
        return hotelRateChangedInfo;
    }

    /**
     * Define el valor de la propiedad hotelRateChangedInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelCreateReservationRsp.HotelRateChangedInfo }
     *     
     */
    public void setHotelRateChangedInfo(HotelCreateReservationRsp.HotelRateChangedInfo value) {
        this.hotelRateChangedInfo = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/hotel_v43_0}HotelProperty"/&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/hotel_v43_0}HotelRateDetail" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="Reason" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "hotelProperty",
        "hotelRateDetail"
    })
    public static class HotelRateChangedInfo {

        @XmlElement(name = "HotelProperty", namespace = "http://www.travelport.com/schema/hotel_v43_0", required = true)
        protected HotelProperty hotelProperty;
        @XmlElement(name = "HotelRateDetail", namespace = "http://www.travelport.com/schema/hotel_v43_0")
        protected HotelRateDetail hotelRateDetail;
        @XmlAttribute(name = "Reason")
        protected String reason;

        /**
         * Obtiene el valor de la propiedad hotelProperty.
         * 
         * @return
         *     possible object is
         *     {@link HotelProperty }
         *     
         */
        public HotelProperty getHotelProperty() {
            return hotelProperty;
        }

        /**
         * Define el valor de la propiedad hotelProperty.
         * 
         * @param value
         *     allowed object is
         *     {@link HotelProperty }
         *     
         */
        public void setHotelProperty(HotelProperty value) {
            this.hotelProperty = value;
        }

        /**
         * Obtiene el valor de la propiedad hotelRateDetail.
         * 
         * @return
         *     possible object is
         *     {@link HotelRateDetail }
         *     
         */
        public HotelRateDetail getHotelRateDetail() {
            return hotelRateDetail;
        }

        /**
         * Define el valor de la propiedad hotelRateDetail.
         * 
         * @param value
         *     allowed object is
         *     {@link HotelRateDetail }
         *     
         */
        public void setHotelRateDetail(HotelRateDetail value) {
            this.hotelRateDetail = value;
        }

        /**
         * Obtiene el valor de la propiedad reason.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReason() {
            return reason;
        }

        /**
         * Define el valor de la propiedad reason.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReason(String value) {
            this.reason = value;
        }

    }

}
