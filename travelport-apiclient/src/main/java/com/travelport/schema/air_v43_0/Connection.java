
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FareNote" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ChangeOfPlane" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="ChangeOfTerminal" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="ChangeOfAirport" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="StopOver" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="MinConnectionTime" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="Duration" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="SegmentIndex" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="FlightDetailsIndex" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="IncludeStopOverToFareQuote" type="{http://www.travelport.com/schema/air_v43_0}typeIgnoreStopOver" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fareNote"
})
@XmlRootElement(name = "Connection")
public class Connection {

    @XmlElement(name = "FareNote")
    protected FareNote fareNote;
    @XmlAttribute(name = "ChangeOfPlane")
    protected Boolean changeOfPlane;
    @XmlAttribute(name = "ChangeOfTerminal")
    protected Boolean changeOfTerminal;
    @XmlAttribute(name = "ChangeOfAirport")
    protected Boolean changeOfAirport;
    @XmlAttribute(name = "StopOver")
    protected Boolean stopOver;
    @XmlAttribute(name = "MinConnectionTime")
    protected Integer minConnectionTime;
    @XmlAttribute(name = "Duration")
    protected Integer duration;
    @XmlAttribute(name = "SegmentIndex")
    protected Integer segmentIndex;
    @XmlAttribute(name = "FlightDetailsIndex")
    protected Integer flightDetailsIndex;
    @XmlAttribute(name = "IncludeStopOverToFareQuote")
    protected TypeIgnoreStopOver includeStopOverToFareQuote;

    /**
     * Obtiene el valor de la propiedad fareNote.
     * 
     * @return
     *     possible object is
     *     {@link FareNote }
     *     
     */
    public FareNote getFareNote() {
        return fareNote;
    }

    /**
     * Define el valor de la propiedad fareNote.
     * 
     * @param value
     *     allowed object is
     *     {@link FareNote }
     *     
     */
    public void setFareNote(FareNote value) {
        this.fareNote = value;
    }

    /**
     * Obtiene el valor de la propiedad changeOfPlane.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isChangeOfPlane() {
        if (changeOfPlane == null) {
            return false;
        } else {
            return changeOfPlane;
        }
    }

    /**
     * Define el valor de la propiedad changeOfPlane.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setChangeOfPlane(Boolean value) {
        this.changeOfPlane = value;
    }

    /**
     * Obtiene el valor de la propiedad changeOfTerminal.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isChangeOfTerminal() {
        if (changeOfTerminal == null) {
            return false;
        } else {
            return changeOfTerminal;
        }
    }

    /**
     * Define el valor de la propiedad changeOfTerminal.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setChangeOfTerminal(Boolean value) {
        this.changeOfTerminal = value;
    }

    /**
     * Obtiene el valor de la propiedad changeOfAirport.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isChangeOfAirport() {
        if (changeOfAirport == null) {
            return false;
        } else {
            return changeOfAirport;
        }
    }

    /**
     * Define el valor de la propiedad changeOfAirport.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setChangeOfAirport(Boolean value) {
        this.changeOfAirport = value;
    }

    /**
     * Obtiene el valor de la propiedad stopOver.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isStopOver() {
        if (stopOver == null) {
            return false;
        } else {
            return stopOver;
        }
    }

    /**
     * Define el valor de la propiedad stopOver.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStopOver(Boolean value) {
        this.stopOver = value;
    }

    /**
     * Obtiene el valor de la propiedad minConnectionTime.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinConnectionTime() {
        return minConnectionTime;
    }

    /**
     * Define el valor de la propiedad minConnectionTime.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinConnectionTime(Integer value) {
        this.minConnectionTime = value;
    }

    /**
     * Obtiene el valor de la propiedad duration.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDuration() {
        return duration;
    }

    /**
     * Define el valor de la propiedad duration.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDuration(Integer value) {
        this.duration = value;
    }

    /**
     * Obtiene el valor de la propiedad segmentIndex.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSegmentIndex() {
        return segmentIndex;
    }

    /**
     * Define el valor de la propiedad segmentIndex.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSegmentIndex(Integer value) {
        this.segmentIndex = value;
    }

    /**
     * Obtiene el valor de la propiedad flightDetailsIndex.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFlightDetailsIndex() {
        return flightDetailsIndex;
    }

    /**
     * Define el valor de la propiedad flightDetailsIndex.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFlightDetailsIndex(Integer value) {
        this.flightDetailsIndex = value;
    }

    /**
     * Obtiene el valor de la propiedad includeStopOverToFareQuote.
     * 
     * @return
     *     possible object is
     *     {@link TypeIgnoreStopOver }
     *     
     */
    public TypeIgnoreStopOver getIncludeStopOverToFareQuote() {
        return includeStopOverToFareQuote;
    }

    /**
     * Define el valor de la propiedad includeStopOverToFareQuote.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeIgnoreStopOver }
     *     
     */
    public void setIncludeStopOverToFareQuote(TypeIgnoreStopOver value) {
        this.includeStopOverToFareQuote = value;
    }

}
