
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.AccountCode;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}AirItineraryDetails"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}AccountCode" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "airItineraryDetails",
    "accountCode"
})
@XmlRootElement(name = "MerchandisingAvailabilityDetails")
public class MerchandisingAvailabilityDetails {

    @XmlElement(name = "AirItineraryDetails", required = true)
    protected AirItineraryDetails airItineraryDetails;
    @XmlElement(name = "AccountCode", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected AccountCode accountCode;

    /**
     * Obtiene el valor de la propiedad airItineraryDetails.
     * 
     * @return
     *     possible object is
     *     {@link AirItineraryDetails }
     *     
     */
    public AirItineraryDetails getAirItineraryDetails() {
        return airItineraryDetails;
    }

    /**
     * Define el valor de la propiedad airItineraryDetails.
     * 
     * @param value
     *     allowed object is
     *     {@link AirItineraryDetails }
     *     
     */
    public void setAirItineraryDetails(AirItineraryDetails value) {
        this.airItineraryDetails = value;
    }

    /**
     * Obtiene el valor de la propiedad accountCode.
     * 
     * @return
     *     possible object is
     *     {@link AccountCode }
     *     
     */
    public AccountCode getAccountCode() {
        return accountCode;
    }

    /**
     * Define el valor de la propiedad accountCode.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountCode }
     *     
     */
    public void setAccountCode(AccountCode value) {
        this.accountCode = value;
    }

}
