
package com.travelport.schema.air_v43_0;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="NumberOfPieces" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="MaxWeight" type="{http://www.travelport.com/schema/air_v43_0}typeWeight" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "numberOfPieces",
    "maxWeight"
})
@XmlRootElement(name = "BaggageAllowance")
public class BaggageAllowance {

    @XmlElement(name = "NumberOfPieces")
    protected BigInteger numberOfPieces;
    @XmlElement(name = "MaxWeight")
    protected TypeWeight maxWeight;

    /**
     * Obtiene el valor de la propiedad numberOfPieces.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfPieces() {
        return numberOfPieces;
    }

    /**
     * Define el valor de la propiedad numberOfPieces.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfPieces(BigInteger value) {
        this.numberOfPieces = value;
    }

    /**
     * Obtiene el valor de la propiedad maxWeight.
     * 
     * @return
     *     possible object is
     *     {@link TypeWeight }
     *     
     */
    public TypeWeight getMaxWeight() {
        return maxWeight;
    }

    /**
     * Define el valor de la propiedad maxWeight.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeWeight }
     *     
     */
    public void setMaxWeight(TypeWeight value) {
        this.maxWeight = value;
    }

}
