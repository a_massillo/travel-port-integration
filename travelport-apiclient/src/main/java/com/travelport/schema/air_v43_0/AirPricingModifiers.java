
package com.travelport.schema.air_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.AccountCode;
import com.travelport.schema.common_v43_0.DiscountCard;
import com.travelport.schema.common_v43_0.PointOfSale;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ProhibitedRuleCategories" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FareRuleCategory" maxOccurs="999"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="AccountCodes" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/common_v43_0}AccountCode" maxOccurs="999"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}PermittedCabins" minOccurs="0"/&gt;
 *         &lt;element name="ContractCodes" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/air_v43_0}ContractCode" maxOccurs="999"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}ExemptTaxes" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}PenaltyFareInformation" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}DiscountCard" maxOccurs="9" minOccurs="0"/&gt;
 *         &lt;element name="PromoCodes" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/air_v43_0}PromoCode" maxOccurs="999"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}ManualFareAdjustment" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}PointOfSale" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}BrandModifiers" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}MultiGDSSearchIndicator" maxOccurs="999" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ProhibitMinStayFares" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="ProhibitMaxStayFares" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="CurrencyType" type="{http://www.travelport.com/schema/common_v43_0}typeCurrency" /&gt;
 *       &lt;attribute name="ProhibitAdvancePurchaseFares" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="ProhibitNonRefundableFares" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="ProhibitRestrictedFares" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="FaresIndicator" type="{http://www.travelport.com/schema/air_v43_0}typeFaresIndicator" /&gt;
 *       &lt;attribute name="FiledCurrency" type="{http://www.travelport.com/schema/common_v43_0}typeCurrency" /&gt;
 *       &lt;attribute name="PlatingCarrier" type="{http://www.travelport.com/schema/common_v43_0}typeCarrier" /&gt;
 *       &lt;attribute name="OverrideCarrier" type="{http://www.travelport.com/schema/common_v43_0}typeCarrier" /&gt;
 *       &lt;attribute name="ETicketability" type="{http://www.travelport.com/schema/air_v43_0}typeEticketability" /&gt;
 *       &lt;attribute name="AccountCodeFaresOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="Key" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="ProhibitNonExchangeableFares" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="ForceSegmentSelect" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="InventoryRequestType" type="{http://www.travelport.com/schema/air_v43_0}typeInventoryRequest" /&gt;
 *       &lt;attribute name="OneWayShop" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="ProhibitUnbundledFareTypes" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="ReturnServices" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" /&gt;
 *       &lt;attribute name="ChannelId"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="2"/&gt;
 *             &lt;maxLength value="4"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="ReturnFareAttributes" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "prohibitedRuleCategories",
    "accountCodes",
    "permittedCabins",
    "contractCodes",
    "exemptTaxes",
    "penaltyFareInformation",
    "discountCard",
    "promoCodes",
    "manualFareAdjustment",
    "pointOfSale",
    "brandModifiers",
    "multiGDSSearchIndicator"
})
@XmlRootElement(name = "AirPricingModifiers")
public class AirPricingModifiers {

    @XmlElement(name = "ProhibitedRuleCategories")
    protected AirPricingModifiers.ProhibitedRuleCategories prohibitedRuleCategories;
    @XmlElement(name = "AccountCodes")
    protected AirPricingModifiers.AccountCodes accountCodes;
    @XmlElement(name = "PermittedCabins")
    protected PermittedCabins permittedCabins;
    @XmlElement(name = "ContractCodes")
    protected AirPricingModifiers.ContractCodes contractCodes;
    @XmlElement(name = "ExemptTaxes")
    protected ExemptTaxes exemptTaxes;
    @XmlElement(name = "PenaltyFareInformation")
    protected PenaltyFareInformation penaltyFareInformation;
    @XmlElement(name = "DiscountCard", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected List<DiscountCard> discountCard;
    @XmlElement(name = "PromoCodes")
    protected AirPricingModifiers.PromoCodes promoCodes;
    @XmlElement(name = "ManualFareAdjustment")
    protected List<ManualFareAdjustment> manualFareAdjustment;
    @XmlElement(name = "PointOfSale", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected PointOfSale pointOfSale;
    @XmlElement(name = "BrandModifiers")
    protected BrandModifiers brandModifiers;
    @XmlElement(name = "MultiGDSSearchIndicator")
    protected List<MultiGDSSearchIndicator> multiGDSSearchIndicator;
    @XmlAttribute(name = "ProhibitMinStayFares")
    protected Boolean prohibitMinStayFares;
    @XmlAttribute(name = "ProhibitMaxStayFares")
    protected Boolean prohibitMaxStayFares;
    @XmlAttribute(name = "CurrencyType")
    protected String currencyType;
    @XmlAttribute(name = "ProhibitAdvancePurchaseFares")
    protected Boolean prohibitAdvancePurchaseFares;
    @XmlAttribute(name = "ProhibitNonRefundableFares")
    protected Boolean prohibitNonRefundableFares;
    @XmlAttribute(name = "ProhibitRestrictedFares")
    protected Boolean prohibitRestrictedFares;
    @XmlAttribute(name = "FaresIndicator")
    protected TypeFaresIndicator faresIndicator;
    @XmlAttribute(name = "FiledCurrency")
    protected String filedCurrency;
    @XmlAttribute(name = "PlatingCarrier")
    protected String platingCarrier;
    @XmlAttribute(name = "OverrideCarrier")
    protected String overrideCarrier;
    @XmlAttribute(name = "ETicketability")
    protected TypeEticketability eTicketability;
    @XmlAttribute(name = "AccountCodeFaresOnly")
    protected Boolean accountCodeFaresOnly;
    @XmlAttribute(name = "Key")
    protected String key;
    @XmlAttribute(name = "ProhibitNonExchangeableFares")
    protected Boolean prohibitNonExchangeableFares;
    @XmlAttribute(name = "ForceSegmentSelect")
    protected Boolean forceSegmentSelect;
    @XmlAttribute(name = "InventoryRequestType")
    protected TypeInventoryRequest inventoryRequestType;
    @XmlAttribute(name = "OneWayShop")
    protected Boolean oneWayShop;
    @XmlAttribute(name = "ProhibitUnbundledFareTypes")
    protected Boolean prohibitUnbundledFareTypes;
    @XmlAttribute(name = "ReturnServices")
    protected Boolean returnServices;
    @XmlAttribute(name = "ChannelId")
    protected String channelId;
    @XmlAttribute(name = "ReturnFareAttributes")
    protected Boolean returnFareAttributes;

    /**
     * Obtiene el valor de la propiedad prohibitedRuleCategories.
     * 
     * @return
     *     possible object is
     *     {@link AirPricingModifiers.ProhibitedRuleCategories }
     *     
     */
    public AirPricingModifiers.ProhibitedRuleCategories getProhibitedRuleCategories() {
        return prohibitedRuleCategories;
    }

    /**
     * Define el valor de la propiedad prohibitedRuleCategories.
     * 
     * @param value
     *     allowed object is
     *     {@link AirPricingModifiers.ProhibitedRuleCategories }
     *     
     */
    public void setProhibitedRuleCategories(AirPricingModifiers.ProhibitedRuleCategories value) {
        this.prohibitedRuleCategories = value;
    }

    /**
     * Obtiene el valor de la propiedad accountCodes.
     * 
     * @return
     *     possible object is
     *     {@link AirPricingModifiers.AccountCodes }
     *     
     */
    public AirPricingModifiers.AccountCodes getAccountCodes() {
        return accountCodes;
    }

    /**
     * Define el valor de la propiedad accountCodes.
     * 
     * @param value
     *     allowed object is
     *     {@link AirPricingModifiers.AccountCodes }
     *     
     */
    public void setAccountCodes(AirPricingModifiers.AccountCodes value) {
        this.accountCodes = value;
    }

    /**
     * Obtiene el valor de la propiedad permittedCabins.
     * 
     * @return
     *     possible object is
     *     {@link PermittedCabins }
     *     
     */
    public PermittedCabins getPermittedCabins() {
        return permittedCabins;
    }

    /**
     * Define el valor de la propiedad permittedCabins.
     * 
     * @param value
     *     allowed object is
     *     {@link PermittedCabins }
     *     
     */
    public void setPermittedCabins(PermittedCabins value) {
        this.permittedCabins = value;
    }

    /**
     * Obtiene el valor de la propiedad contractCodes.
     * 
     * @return
     *     possible object is
     *     {@link AirPricingModifiers.ContractCodes }
     *     
     */
    public AirPricingModifiers.ContractCodes getContractCodes() {
        return contractCodes;
    }

    /**
     * Define el valor de la propiedad contractCodes.
     * 
     * @param value
     *     allowed object is
     *     {@link AirPricingModifiers.ContractCodes }
     *     
     */
    public void setContractCodes(AirPricingModifiers.ContractCodes value) {
        this.contractCodes = value;
    }

    /**
     * Obtiene el valor de la propiedad exemptTaxes.
     * 
     * @return
     *     possible object is
     *     {@link ExemptTaxes }
     *     
     */
    public ExemptTaxes getExemptTaxes() {
        return exemptTaxes;
    }

    /**
     * Define el valor de la propiedad exemptTaxes.
     * 
     * @param value
     *     allowed object is
     *     {@link ExemptTaxes }
     *     
     */
    public void setExemptTaxes(ExemptTaxes value) {
        this.exemptTaxes = value;
    }

    /**
     * Request Fares with specific Penalty
     *                             Information.
     * 
     * @return
     *     possible object is
     *     {@link PenaltyFareInformation }
     *     
     */
    public PenaltyFareInformation getPenaltyFareInformation() {
        return penaltyFareInformation;
    }

    /**
     * Define el valor de la propiedad penaltyFareInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link PenaltyFareInformation }
     *     
     */
    public void setPenaltyFareInformation(PenaltyFareInformation value) {
        this.penaltyFareInformation = value;
    }

    /**
     * Discount request for rail.
     *                         Gets the value of the discountCard property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the discountCard property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDiscountCard().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DiscountCard }
     * 
     * 
     */
    public List<DiscountCard> getDiscountCard() {
        if (discountCard == null) {
            discountCard = new ArrayList<DiscountCard>();
        }
        return this.discountCard;
    }

    /**
     * Obtiene el valor de la propiedad promoCodes.
     * 
     * @return
     *     possible object is
     *     {@link AirPricingModifiers.PromoCodes }
     *     
     */
    public AirPricingModifiers.PromoCodes getPromoCodes() {
        return promoCodes;
    }

    /**
     * Define el valor de la propiedad promoCodes.
     * 
     * @param value
     *     allowed object is
     *     {@link AirPricingModifiers.PromoCodes }
     *     
     */
    public void setPromoCodes(AirPricingModifiers.PromoCodes value) {
        this.promoCodes = value;
    }

    /**
     * Represents increment/discount applied manually by agent.Gets the value of the manualFareAdjustment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the manualFareAdjustment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getManualFareAdjustment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ManualFareAdjustment }
     * 
     * 
     */
    public List<ManualFareAdjustment> getManualFareAdjustment() {
        if (manualFareAdjustment == null) {
            manualFareAdjustment = new ArrayList<ManualFareAdjustment>();
        }
        return this.manualFareAdjustment;
    }

    /**
     * User can use this node to send a specific PCC to access fares allowed only for that PCC. This node gives the capability for fare redistribution at stored fare level. As multiple UAPI AirPricingInfos (all having same AirPricingInfoGroup) can converge to a single stored fare, UAPI will map PoinOfSale information from the first available one from each group
     * 
     * @return
     *     possible object is
     *     {@link PointOfSale }
     *     
     */
    public PointOfSale getPointOfSale() {
        return pointOfSale;
    }

    /**
     * Define el valor de la propiedad pointOfSale.
     * 
     * @param value
     *     allowed object is
     *     {@link PointOfSale }
     *     
     */
    public void setPointOfSale(PointOfSale value) {
        this.pointOfSale = value;
    }

    /**
     * Used to specify the level of branding requested.
     * 
     * @return
     *     possible object is
     *     {@link BrandModifiers }
     *     
     */
    public BrandModifiers getBrandModifiers() {
        return brandModifiers;
    }

    /**
     * Define el valor de la propiedad brandModifiers.
     * 
     * @param value
     *     allowed object is
     *     {@link BrandModifiers }
     *     
     */
    public void setBrandModifiers(BrandModifiers value) {
        this.brandModifiers = value;
    }

    /**
     * Gets the value of the multiGDSSearchIndicator property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the multiGDSSearchIndicator property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMultiGDSSearchIndicator().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MultiGDSSearchIndicator }
     * 
     * 
     */
    public List<MultiGDSSearchIndicator> getMultiGDSSearchIndicator() {
        if (multiGDSSearchIndicator == null) {
            multiGDSSearchIndicator = new ArrayList<MultiGDSSearchIndicator>();
        }
        return this.multiGDSSearchIndicator;
    }

    /**
     * Obtiene el valor de la propiedad prohibitMinStayFares.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isProhibitMinStayFares() {
        if (prohibitMinStayFares == null) {
            return false;
        } else {
            return prohibitMinStayFares;
        }
    }

    /**
     * Define el valor de la propiedad prohibitMinStayFares.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProhibitMinStayFares(Boolean value) {
        this.prohibitMinStayFares = value;
    }

    /**
     * Obtiene el valor de la propiedad prohibitMaxStayFares.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isProhibitMaxStayFares() {
        if (prohibitMaxStayFares == null) {
            return false;
        } else {
            return prohibitMaxStayFares;
        }
    }

    /**
     * Define el valor de la propiedad prohibitMaxStayFares.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProhibitMaxStayFares(Boolean value) {
        this.prohibitMaxStayFares = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyType() {
        return currencyType;
    }

    /**
     * Define el valor de la propiedad currencyType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyType(String value) {
        this.currencyType = value;
    }

    /**
     * Obtiene el valor de la propiedad prohibitAdvancePurchaseFares.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isProhibitAdvancePurchaseFares() {
        if (prohibitAdvancePurchaseFares == null) {
            return false;
        } else {
            return prohibitAdvancePurchaseFares;
        }
    }

    /**
     * Define el valor de la propiedad prohibitAdvancePurchaseFares.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProhibitAdvancePurchaseFares(Boolean value) {
        this.prohibitAdvancePurchaseFares = value;
    }

    /**
     * Obtiene el valor de la propiedad prohibitNonRefundableFares.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isProhibitNonRefundableFares() {
        if (prohibitNonRefundableFares == null) {
            return false;
        } else {
            return prohibitNonRefundableFares;
        }
    }

    /**
     * Define el valor de la propiedad prohibitNonRefundableFares.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProhibitNonRefundableFares(Boolean value) {
        this.prohibitNonRefundableFares = value;
    }

    /**
     * Obtiene el valor de la propiedad prohibitRestrictedFares.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isProhibitRestrictedFares() {
        if (prohibitRestrictedFares == null) {
            return false;
        } else {
            return prohibitRestrictedFares;
        }
    }

    /**
     * Define el valor de la propiedad prohibitRestrictedFares.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProhibitRestrictedFares(Boolean value) {
        this.prohibitRestrictedFares = value;
    }

    /**
     * Obtiene el valor de la propiedad faresIndicator.
     * 
     * @return
     *     possible object is
     *     {@link TypeFaresIndicator }
     *     
     */
    public TypeFaresIndicator getFaresIndicator() {
        return faresIndicator;
    }

    /**
     * Define el valor de la propiedad faresIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeFaresIndicator }
     *     
     */
    public void setFaresIndicator(TypeFaresIndicator value) {
        this.faresIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad filedCurrency.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFiledCurrency() {
        return filedCurrency;
    }

    /**
     * Define el valor de la propiedad filedCurrency.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFiledCurrency(String value) {
        this.filedCurrency = value;
    }

    /**
     * Obtiene el valor de la propiedad platingCarrier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlatingCarrier() {
        return platingCarrier;
    }

    /**
     * Define el valor de la propiedad platingCarrier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlatingCarrier(String value) {
        this.platingCarrier = value;
    }

    /**
     * Obtiene el valor de la propiedad overrideCarrier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOverrideCarrier() {
        return overrideCarrier;
    }

    /**
     * Define el valor de la propiedad overrideCarrier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOverrideCarrier(String value) {
        this.overrideCarrier = value;
    }

    /**
     * Obtiene el valor de la propiedad eTicketability.
     * 
     * @return
     *     possible object is
     *     {@link TypeEticketability }
     *     
     */
    public TypeEticketability getETicketability() {
        return eTicketability;
    }

    /**
     * Define el valor de la propiedad eTicketability.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeEticketability }
     *     
     */
    public void setETicketability(TypeEticketability value) {
        this.eTicketability = value;
    }

    /**
     * Obtiene el valor de la propiedad accountCodeFaresOnly.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAccountCodeFaresOnly() {
        return accountCodeFaresOnly;
    }

    /**
     * Define el valor de la propiedad accountCodeFaresOnly.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAccountCodeFaresOnly(Boolean value) {
        this.accountCodeFaresOnly = value;
    }

    /**
     * Obtiene el valor de la propiedad key.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Define el valor de la propiedad key.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Obtiene el valor de la propiedad prohibitNonExchangeableFares.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isProhibitNonExchangeableFares() {
        if (prohibitNonExchangeableFares == null) {
            return false;
        } else {
            return prohibitNonExchangeableFares;
        }
    }

    /**
     * Define el valor de la propiedad prohibitNonExchangeableFares.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProhibitNonExchangeableFares(Boolean value) {
        this.prohibitNonExchangeableFares = value;
    }

    /**
     * Obtiene el valor de la propiedad forceSegmentSelect.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isForceSegmentSelect() {
        if (forceSegmentSelect == null) {
            return false;
        } else {
            return forceSegmentSelect;
        }
    }

    /**
     * Define el valor de la propiedad forceSegmentSelect.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForceSegmentSelect(Boolean value) {
        this.forceSegmentSelect = value;
    }

    /**
     * Obtiene el valor de la propiedad inventoryRequestType.
     * 
     * @return
     *     possible object is
     *     {@link TypeInventoryRequest }
     *     
     */
    public TypeInventoryRequest getInventoryRequestType() {
        return inventoryRequestType;
    }

    /**
     * Define el valor de la propiedad inventoryRequestType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeInventoryRequest }
     *     
     */
    public void setInventoryRequestType(TypeInventoryRequest value) {
        this.inventoryRequestType = value;
    }

    /**
     * Obtiene el valor de la propiedad oneWayShop.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isOneWayShop() {
        if (oneWayShop == null) {
            return false;
        } else {
            return oneWayShop;
        }
    }

    /**
     * Define el valor de la propiedad oneWayShop.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOneWayShop(Boolean value) {
        this.oneWayShop = value;
    }

    /**
     * Obtiene el valor de la propiedad prohibitUnbundledFareTypes.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isProhibitUnbundledFareTypes() {
        return prohibitUnbundledFareTypes;
    }

    /**
     * Define el valor de la propiedad prohibitUnbundledFareTypes.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProhibitUnbundledFareTypes(Boolean value) {
        this.prohibitUnbundledFareTypes = value;
    }

    /**
     * Obtiene el valor de la propiedad returnServices.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isReturnServices() {
        if (returnServices == null) {
            return true;
        } else {
            return returnServices;
        }
    }

    /**
     * Define el valor de la propiedad returnServices.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReturnServices(Boolean value) {
        this.returnServices = value;
    }

    /**
     * Obtiene el valor de la propiedad channelId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelId() {
        return channelId;
    }

    /**
     * Define el valor de la propiedad channelId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelId(String value) {
        this.channelId = value;
    }

    /**
     * Obtiene el valor de la propiedad returnFareAttributes.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isReturnFareAttributes() {
        if (returnFareAttributes == null) {
            return false;
        } else {
            return returnFareAttributes;
        }
    }

    /**
     * Define el valor de la propiedad returnFareAttributes.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReturnFareAttributes(Boolean value) {
        this.returnFareAttributes = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}AccountCode" maxOccurs="999"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "accountCode"
    })
    public static class AccountCodes {

        @XmlElement(name = "AccountCode", namespace = "http://www.travelport.com/schema/common_v43_0", required = true)
        protected List<AccountCode> accountCode;

        /**
         * Used to get negotiated pricing. Provider:ACH.Gets the value of the accountCode property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the accountCode property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAccountCode().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AccountCode }
         * 
         * 
         */
        public List<AccountCode> getAccountCode() {
            if (accountCode == null) {
                accountCode = new ArrayList<AccountCode>();
            }
            return this.accountCode;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}ContractCode" maxOccurs="999"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "contractCode"
    })
    public static class ContractCodes {

        @XmlElement(name = "ContractCode", required = true)
        protected List<ContractCode> contractCode;

        /**
         * Gets the value of the contractCode property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the contractCode property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getContractCode().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ContractCode }
         * 
         * 
         */
        public List<ContractCode> getContractCode() {
            if (contractCode == null) {
                contractCode = new ArrayList<ContractCode>();
            }
            return this.contractCode;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FareRuleCategory" maxOccurs="999"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fareRuleCategory"
    })
    public static class ProhibitedRuleCategories {

        @XmlElement(name = "FareRuleCategory", required = true)
        protected List<FareRuleCategory> fareRuleCategory;

        /**
         * Gets the value of the fareRuleCategory property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the fareRuleCategory property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFareRuleCategory().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link FareRuleCategory }
         * 
         * 
         */
        public List<FareRuleCategory> getFareRuleCategory() {
            if (fareRuleCategory == null) {
                fareRuleCategory = new ArrayList<FareRuleCategory>();
            }
            return this.fareRuleCategory;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}PromoCode" maxOccurs="999"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "promoCode"
    })
    public static class PromoCodes {

        @XmlElement(name = "PromoCode", required = true)
        protected List<PromoCode> promoCode;

        /**
         * Gets the value of the promoCode property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the promoCode property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPromoCode().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link PromoCode }
         * 
         * 
         */
        public List<PromoCode> getPromoCode() {
            if (promoCode == null) {
                promoCode = new ArrayList<PromoCode>();
            }
            return this.promoCode;
        }

    }

}
