
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/air_v43_0}BaseLowFareSearchReq"&gt;
 *       &lt;attribute name="PolicyReference" type="{http://www.travelport.com/schema/common_v43_0}typePolicyReference" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "LowFareSearchReq")
public class LowFareSearchReq
    extends BaseLowFareSearchReq
{

    @XmlAttribute(name = "PolicyReference")
    protected String policyReference;

    /**
     * Obtiene el valor de la propiedad policyReference.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyReference() {
        return policyReference;
    }

    /**
     * Define el valor de la propiedad policyReference.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyReference(String value) {
        this.policyReference = value;
    }

}
