
package com.travelport.schema.air_v43_0;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.travelport.schema.common_v43_0.Segment;


/**
 * <p>Clase Java para typeBaseAirSegment complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="typeBaseAirSegment"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}Segment"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}SponsoredFltInfo" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}CodeshareInfo" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}AirAvailInfo" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FlightDetails" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FlightDetailsRef" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}AlternateLocationDistanceRef" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}Connection" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}SellMessage" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}RailCoachDetails" maxOccurs="999" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attGroup ref="{http://www.travelport.com/schema/common_v43_0}attrProviderSupplier"/&gt;
 *       &lt;attGroup ref="{http://www.travelport.com/schema/air_v43_0}attrLinkInfo"/&gt;
 *       &lt;attGroup ref="{http://www.travelport.com/schema/common_v43_0}attrOrigDestDepatureInfo"/&gt;
 *       &lt;attGroup ref="{http://www.travelport.com/schema/common_v43_0}attrFlightTimes"/&gt;
 *       &lt;attribute name="OpenSegment" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="Group" use="required" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="Carrier" type="{http://www.travelport.com/schema/common_v43_0}typeCarrier" /&gt;
 *       &lt;attribute name="CabinClass" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="FlightNumber" type="{http://www.travelport.com/schema/common_v43_0}typeFlightNumber" /&gt;
 *       &lt;attribute name="ClassOfService" type="{http://www.travelport.com/schema/common_v43_0}typeClassOfService" /&gt;
 *       &lt;attribute name="ETicketability" type="{http://www.travelport.com/schema/air_v43_0}typeEticketability" /&gt;
 *       &lt;attribute name="Equipment" type="{http://www.travelport.com/schema/air_v43_0}typeEquipment" /&gt;
 *       &lt;attribute name="MarriageGroup" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="NumberOfStops" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="Seamless" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="ChangeOfPlane" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="GuaranteedPaymentCarrier" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="HostTokenRef" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="ProviderReservationInfoRef" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="PassiveProviderReservationInfoRef" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="OptionalServicesIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="AvailabilitySource" type="{http://www.travelport.com/schema/air_v43_0}typeAvailabilitySource" /&gt;
 *       &lt;attribute name="APISRequirementsRef" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="BlackListed" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="OperationalStatus" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="NumberInParty"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}positiveInteger"&gt;
 *             &lt;minInclusive value="1"/&gt;
 *             &lt;maxInclusive value="99"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="RailCoachNumber"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="4"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="BookingDate" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *       &lt;attribute name="FlownSegment" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="ScheduleChange" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="BrandIndicator" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "typeBaseAirSegment", propOrder = {
    "sponsoredFltInfo",
    "codeshareInfo",
    "airAvailInfo",
    "flightDetails",
    "flightDetailsRef",
    "alternateLocationDistanceRef",
    "connection",
    "sellMessage",
    "railCoachDetails"
})
public class TypeBaseAirSegment
    extends Segment
{

    @XmlElement(name = "SponsoredFltInfo")
    protected SponsoredFltInfo sponsoredFltInfo;
    @XmlElement(name = "CodeshareInfo")
    protected CodeshareInfo codeshareInfo;
    @XmlElement(name = "AirAvailInfo")
    protected List<AirAvailInfo> airAvailInfo;
    @XmlElement(name = "FlightDetails")
    protected List<FlightDetails> flightDetails;
    @XmlElement(name = "FlightDetailsRef")
    protected List<FlightDetailsRef> flightDetailsRef;
    @XmlElement(name = "AlternateLocationDistanceRef")
    protected List<AlternateLocationDistanceRef> alternateLocationDistanceRef;
    @XmlElement(name = "Connection")
    protected Connection connection;
    @XmlElement(name = "SellMessage", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected List<String> sellMessage;
    @XmlElement(name = "RailCoachDetails")
    protected List<RailCoachDetails> railCoachDetails;
    @XmlAttribute(name = "OpenSegment")
    protected Boolean openSegment;
    @XmlAttribute(name = "Group", required = true)
    protected int group;
    @XmlAttribute(name = "Carrier")
    protected String carrier;
    @XmlAttribute(name = "CabinClass")
    protected String cabinClass;
    @XmlAttribute(name = "FlightNumber")
    protected String flightNumber;
    @XmlAttribute(name = "ClassOfService")
    protected String classOfService;
    @XmlAttribute(name = "ETicketability")
    protected TypeEticketability eTicketability;
    @XmlAttribute(name = "Equipment")
    protected String equipment;
    @XmlAttribute(name = "MarriageGroup")
    protected Integer marriageGroup;
    @XmlAttribute(name = "NumberOfStops")
    protected Integer numberOfStops;
    @XmlAttribute(name = "Seamless")
    protected Boolean seamless;
    @XmlAttribute(name = "ChangeOfPlane")
    protected Boolean changeOfPlane;
    @XmlAttribute(name = "GuaranteedPaymentCarrier")
    protected String guaranteedPaymentCarrier;
    @XmlAttribute(name = "HostTokenRef")
    protected String hostTokenRef;
    @XmlAttribute(name = "ProviderReservationInfoRef")
    protected String providerReservationInfoRef;
    @XmlAttribute(name = "PassiveProviderReservationInfoRef")
    protected String passiveProviderReservationInfoRef;
    @XmlAttribute(name = "OptionalServicesIndicator")
    protected Boolean optionalServicesIndicator;
    @XmlAttribute(name = "AvailabilitySource")
    protected String availabilitySource;
    @XmlAttribute(name = "APISRequirementsRef")
    protected String apisRequirementsRef;
    @XmlAttribute(name = "BlackListed")
    protected Boolean blackListed;
    @XmlAttribute(name = "OperationalStatus")
    protected String operationalStatus;
    @XmlAttribute(name = "NumberInParty")
    protected Integer numberInParty;
    @XmlAttribute(name = "RailCoachNumber")
    protected String railCoachNumber;
    @XmlAttribute(name = "BookingDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar bookingDate;
    @XmlAttribute(name = "FlownSegment")
    protected Boolean flownSegment;
    @XmlAttribute(name = "ScheduleChange")
    protected Boolean scheduleChange;
    @XmlAttribute(name = "BrandIndicator")
    protected String brandIndicator;
    @XmlAttribute(name = "ProviderCode")
    protected String providerCode;
    @XmlAttribute(name = "SupplierCode")
    protected String supplierCode;
    @XmlAttribute(name = "ParticipantLevel")
    protected String participantLevel;
    @XmlAttribute(name = "LinkAvailability")
    protected Boolean linkAvailability;
    @XmlAttribute(name = "PolledAvailabilityOption")
    protected String polledAvailabilityOption;
    @XmlAttribute(name = "AvailabilityDisplayType")
    protected String availabilityDisplayType;
    @XmlAttribute(name = "Origin", required = true)
    protected String origin;
    @XmlAttribute(name = "Destination", required = true)
    protected String destination;
    @XmlAttribute(name = "DepartureTime")
    protected String departureTime;
    @XmlAttribute(name = "ArrivalTime")
    protected String arrivalTime;
    @XmlAttribute(name = "FlightTime")
    protected BigInteger flightTime;
    @XmlAttribute(name = "TravelTime")
    protected BigInteger travelTime;
    @XmlAttribute(name = "Distance")
    protected BigInteger distance;

    /**
     * Obtiene el valor de la propiedad sponsoredFltInfo.
     * 
     * @return
     *     possible object is
     *     {@link SponsoredFltInfo }
     *     
     */
    public SponsoredFltInfo getSponsoredFltInfo() {
        return sponsoredFltInfo;
    }

    /**
     * Define el valor de la propiedad sponsoredFltInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link SponsoredFltInfo }
     *     
     */
    public void setSponsoredFltInfo(SponsoredFltInfo value) {
        this.sponsoredFltInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad codeshareInfo.
     * 
     * @return
     *     possible object is
     *     {@link CodeshareInfo }
     *     
     */
    public CodeshareInfo getCodeshareInfo() {
        return codeshareInfo;
    }

    /**
     * Define el valor de la propiedad codeshareInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeshareInfo }
     *     
     */
    public void setCodeshareInfo(CodeshareInfo value) {
        this.codeshareInfo = value;
    }

    /**
     * Gets the value of the airAvailInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the airAvailInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAirAvailInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AirAvailInfo }
     * 
     * 
     */
    public List<AirAvailInfo> getAirAvailInfo() {
        if (airAvailInfo == null) {
            airAvailInfo = new ArrayList<AirAvailInfo>();
        }
        return this.airAvailInfo;
    }

    /**
     * Gets the value of the flightDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the flightDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFlightDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FlightDetails }
     * 
     * 
     */
    public List<FlightDetails> getFlightDetails() {
        if (flightDetails == null) {
            flightDetails = new ArrayList<FlightDetails>();
        }
        return this.flightDetails;
    }

    /**
     * Gets the value of the flightDetailsRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the flightDetailsRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFlightDetailsRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FlightDetailsRef }
     * 
     * 
     */
    public List<FlightDetailsRef> getFlightDetailsRef() {
        if (flightDetailsRef == null) {
            flightDetailsRef = new ArrayList<FlightDetailsRef>();
        }
        return this.flightDetailsRef;
    }

    /**
     * Gets the value of the alternateLocationDistanceRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alternateLocationDistanceRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternateLocationDistanceRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlternateLocationDistanceRef }
     * 
     * 
     */
    public List<AlternateLocationDistanceRef> getAlternateLocationDistanceRef() {
        if (alternateLocationDistanceRef == null) {
            alternateLocationDistanceRef = new ArrayList<AlternateLocationDistanceRef>();
        }
        return this.alternateLocationDistanceRef;
    }

    /**
     * Obtiene el valor de la propiedad connection.
     * 
     * @return
     *     possible object is
     *     {@link Connection }
     *     
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * Define el valor de la propiedad connection.
     * 
     * @param value
     *     allowed object is
     *     {@link Connection }
     *     
     */
    public void setConnection(Connection value) {
        this.connection = value;
    }

    /**
     * Gets the value of the sellMessage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sellMessage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSellMessage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getSellMessage() {
        if (sellMessage == null) {
            sellMessage = new ArrayList<String>();
        }
        return this.sellMessage;
    }

    /**
     * Gets the value of the railCoachDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the railCoachDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRailCoachDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RailCoachDetails }
     * 
     * 
     */
    public List<RailCoachDetails> getRailCoachDetails() {
        if (railCoachDetails == null) {
            railCoachDetails = new ArrayList<RailCoachDetails>();
        }
        return this.railCoachDetails;
    }

    /**
     * Obtiene el valor de la propiedad openSegment.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOpenSegment() {
        return openSegment;
    }

    /**
     * Define el valor de la propiedad openSegment.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOpenSegment(Boolean value) {
        this.openSegment = value;
    }

    /**
     * Obtiene el valor de la propiedad group.
     * 
     */
    public int getGroup() {
        return group;
    }

    /**
     * Define el valor de la propiedad group.
     * 
     */
    public void setGroup(int value) {
        this.group = value;
    }

    /**
     * Obtiene el valor de la propiedad carrier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCarrier() {
        return carrier;
    }

    /**
     * Define el valor de la propiedad carrier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarrier(String value) {
        this.carrier = value;
    }

    /**
     * Obtiene el valor de la propiedad cabinClass.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCabinClass() {
        return cabinClass;
    }

    /**
     * Define el valor de la propiedad cabinClass.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCabinClass(String value) {
        this.cabinClass = value;
    }

    /**
     * Obtiene el valor de la propiedad flightNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Define el valor de la propiedad flightNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightNumber(String value) {
        this.flightNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad classOfService.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassOfService() {
        return classOfService;
    }

    /**
     * Define el valor de la propiedad classOfService.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassOfService(String value) {
        this.classOfService = value;
    }

    /**
     * Obtiene el valor de la propiedad eTicketability.
     * 
     * @return
     *     possible object is
     *     {@link TypeEticketability }
     *     
     */
    public TypeEticketability getETicketability() {
        return eTicketability;
    }

    /**
     * Define el valor de la propiedad eTicketability.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeEticketability }
     *     
     */
    public void setETicketability(TypeEticketability value) {
        this.eTicketability = value;
    }

    /**
     * Obtiene el valor de la propiedad equipment.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipment() {
        return equipment;
    }

    /**
     * Define el valor de la propiedad equipment.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipment(String value) {
        this.equipment = value;
    }

    /**
     * Obtiene el valor de la propiedad marriageGroup.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMarriageGroup() {
        return marriageGroup;
    }

    /**
     * Define el valor de la propiedad marriageGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMarriageGroup(Integer value) {
        this.marriageGroup = value;
    }

    /**
     * Obtiene el valor de la propiedad numberOfStops.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfStops() {
        return numberOfStops;
    }

    /**
     * Define el valor de la propiedad numberOfStops.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfStops(Integer value) {
        this.numberOfStops = value;
    }

    /**
     * Obtiene el valor de la propiedad seamless.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSeamless() {
        return seamless;
    }

    /**
     * Define el valor de la propiedad seamless.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSeamless(Boolean value) {
        this.seamless = value;
    }

    /**
     * Obtiene el valor de la propiedad changeOfPlane.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isChangeOfPlane() {
        if (changeOfPlane == null) {
            return false;
        } else {
            return changeOfPlane;
        }
    }

    /**
     * Define el valor de la propiedad changeOfPlane.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setChangeOfPlane(Boolean value) {
        this.changeOfPlane = value;
    }

    /**
     * Obtiene el valor de la propiedad guaranteedPaymentCarrier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuaranteedPaymentCarrier() {
        return guaranteedPaymentCarrier;
    }

    /**
     * Define el valor de la propiedad guaranteedPaymentCarrier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuaranteedPaymentCarrier(String value) {
        this.guaranteedPaymentCarrier = value;
    }

    /**
     * Obtiene el valor de la propiedad hostTokenRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHostTokenRef() {
        return hostTokenRef;
    }

    /**
     * Define el valor de la propiedad hostTokenRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHostTokenRef(String value) {
        this.hostTokenRef = value;
    }

    /**
     * Obtiene el valor de la propiedad providerReservationInfoRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProviderReservationInfoRef() {
        return providerReservationInfoRef;
    }

    /**
     * Define el valor de la propiedad providerReservationInfoRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProviderReservationInfoRef(String value) {
        this.providerReservationInfoRef = value;
    }

    /**
     * Obtiene el valor de la propiedad passiveProviderReservationInfoRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassiveProviderReservationInfoRef() {
        return passiveProviderReservationInfoRef;
    }

    /**
     * Define el valor de la propiedad passiveProviderReservationInfoRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassiveProviderReservationInfoRef(String value) {
        this.passiveProviderReservationInfoRef = value;
    }

    /**
     * Obtiene el valor de la propiedad optionalServicesIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOptionalServicesIndicator() {
        return optionalServicesIndicator;
    }

    /**
     * Define el valor de la propiedad optionalServicesIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOptionalServicesIndicator(Boolean value) {
        this.optionalServicesIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad availabilitySource.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAvailabilitySource() {
        return availabilitySource;
    }

    /**
     * Define el valor de la propiedad availabilitySource.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAvailabilitySource(String value) {
        this.availabilitySource = value;
    }

    /**
     * Obtiene el valor de la propiedad apisRequirementsRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAPISRequirementsRef() {
        return apisRequirementsRef;
    }

    /**
     * Define el valor de la propiedad apisRequirementsRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPISRequirementsRef(String value) {
        this.apisRequirementsRef = value;
    }

    /**
     * Obtiene el valor de la propiedad blackListed.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBlackListed() {
        return blackListed;
    }

    /**
     * Define el valor de la propiedad blackListed.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBlackListed(Boolean value) {
        this.blackListed = value;
    }

    /**
     * Obtiene el valor de la propiedad operationalStatus.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperationalStatus() {
        return operationalStatus;
    }

    /**
     * Define el valor de la propiedad operationalStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperationalStatus(String value) {
        this.operationalStatus = value;
    }

    /**
     * Obtiene el valor de la propiedad numberInParty.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberInParty() {
        return numberInParty;
    }

    /**
     * Define el valor de la propiedad numberInParty.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberInParty(Integer value) {
        this.numberInParty = value;
    }

    /**
     * Obtiene el valor de la propiedad railCoachNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRailCoachNumber() {
        return railCoachNumber;
    }

    /**
     * Define el valor de la propiedad railCoachNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRailCoachNumber(String value) {
        this.railCoachNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad bookingDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBookingDate() {
        return bookingDate;
    }

    /**
     * Define el valor de la propiedad bookingDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBookingDate(XMLGregorianCalendar value) {
        this.bookingDate = value;
    }

    /**
     * Obtiene el valor de la propiedad flownSegment.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isFlownSegment() {
        if (flownSegment == null) {
            return false;
        } else {
            return flownSegment;
        }
    }

    /**
     * Define el valor de la propiedad flownSegment.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFlownSegment(Boolean value) {
        this.flownSegment = value;
    }

    /**
     * Obtiene el valor de la propiedad scheduleChange.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isScheduleChange() {
        if (scheduleChange == null) {
            return false;
        } else {
            return scheduleChange;
        }
    }

    /**
     * Define el valor de la propiedad scheduleChange.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setScheduleChange(Boolean value) {
        this.scheduleChange = value;
    }

    /**
     * Obtiene el valor de la propiedad brandIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandIndicator() {
        return brandIndicator;
    }

    /**
     * Define el valor de la propiedad brandIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandIndicator(String value) {
        this.brandIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad providerCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProviderCode() {
        return providerCode;
    }

    /**
     * Define el valor de la propiedad providerCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProviderCode(String value) {
        this.providerCode = value;
    }

    /**
     * Obtiene el valor de la propiedad supplierCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupplierCode() {
        return supplierCode;
    }

    /**
     * Define el valor de la propiedad supplierCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupplierCode(String value) {
        this.supplierCode = value;
    }

    /**
     * Obtiene el valor de la propiedad participantLevel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParticipantLevel() {
        return participantLevel;
    }

    /**
     * Define el valor de la propiedad participantLevel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParticipantLevel(String value) {
        this.participantLevel = value;
    }

    /**
     * Obtiene el valor de la propiedad linkAvailability.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLinkAvailability() {
        return linkAvailability;
    }

    /**
     * Define el valor de la propiedad linkAvailability.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLinkAvailability(Boolean value) {
        this.linkAvailability = value;
    }

    /**
     * Obtiene el valor de la propiedad polledAvailabilityOption.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolledAvailabilityOption() {
        return polledAvailabilityOption;
    }

    /**
     * Define el valor de la propiedad polledAvailabilityOption.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolledAvailabilityOption(String value) {
        this.polledAvailabilityOption = value;
    }

    /**
     * Obtiene el valor de la propiedad availabilityDisplayType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAvailabilityDisplayType() {
        return availabilityDisplayType;
    }

    /**
     * Define el valor de la propiedad availabilityDisplayType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAvailabilityDisplayType(String value) {
        this.availabilityDisplayType = value;
    }

    /**
     * Obtiene el valor de la propiedad origin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * Define el valor de la propiedad origin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigin(String value) {
        this.origin = value;
    }

    /**
     * Obtiene el valor de la propiedad destination.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestination() {
        return destination;
    }

    /**
     * Define el valor de la propiedad destination.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestination(String value) {
        this.destination = value;
    }

    /**
     * Obtiene el valor de la propiedad departureTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureTime() {
        return departureTime;
    }

    /**
     * Define el valor de la propiedad departureTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureTime(String value) {
        this.departureTime = value;
    }

    /**
     * Obtiene el valor de la propiedad arrivalTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArrivalTime() {
        return arrivalTime;
    }

    /**
     * Define el valor de la propiedad arrivalTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArrivalTime(String value) {
        this.arrivalTime = value;
    }

    /**
     * Obtiene el valor de la propiedad flightTime.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getFlightTime() {
        return flightTime;
    }

    /**
     * Define el valor de la propiedad flightTime.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setFlightTime(BigInteger value) {
        this.flightTime = value;
    }

    /**
     * Obtiene el valor de la propiedad travelTime.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTravelTime() {
        return travelTime;
    }

    /**
     * Define el valor de la propiedad travelTime.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTravelTime(BigInteger value) {
        this.travelTime = value;
    }

    /**
     * Obtiene el valor de la propiedad distance.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDistance() {
        return distance;
    }

    /**
     * Define el valor de la propiedad distance.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDistance(BigInteger value) {
        this.distance = value;
    }

}
