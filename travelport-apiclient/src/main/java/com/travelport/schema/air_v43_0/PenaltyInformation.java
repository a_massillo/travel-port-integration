
package com.travelport.schema.air_v43_0;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;simpleContent&gt;
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *       &lt;attribute name="Carrier" type="{http://www.travelport.com/schema/common_v43_0}typeCarrier" /&gt;
 *       &lt;attribute name="FareBasis" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="FareComponent" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="PriceableUnit" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="BoardPoint" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *       &lt;attribute name="OffPoint" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *       &lt;attribute name="MinimumChangeFee" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="MaximumChangeFee" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="FiledCurrency" type="{http://www.travelport.com/schema/common_v43_0}typeCurrency" /&gt;
 *       &lt;attribute name="ConversionRate" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
 *       &lt;attribute name="Refundable" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="ChangeFeeApplicationCode"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;length value="2"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/extension&gt;
 *   &lt;/simpleContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "value"
})
@XmlRootElement(name = "PenaltyInformation")
public class PenaltyInformation {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "Carrier")
    protected String carrier;
    @XmlAttribute(name = "FareBasis")
    protected String fareBasis;
    @XmlAttribute(name = "FareComponent")
    protected BigInteger fareComponent;
    @XmlAttribute(name = "PriceableUnit")
    protected BigInteger priceableUnit;
    @XmlAttribute(name = "BoardPoint")
    protected String boardPoint;
    @XmlAttribute(name = "OffPoint")
    protected String offPoint;
    @XmlAttribute(name = "MinimumChangeFee")
    protected String minimumChangeFee;
    @XmlAttribute(name = "MaximumChangeFee")
    protected String maximumChangeFee;
    @XmlAttribute(name = "FiledCurrency")
    protected String filedCurrency;
    @XmlAttribute(name = "ConversionRate")
    protected BigDecimal conversionRate;
    @XmlAttribute(name = "Refundable")
    protected Boolean refundable;
    @XmlAttribute(name = "ChangeFeeApplicationCode")
    protected String changeFeeApplicationCode;

    /**
     * Obtiene el valor de la propiedad value.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Obtiene el valor de la propiedad carrier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCarrier() {
        return carrier;
    }

    /**
     * Define el valor de la propiedad carrier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarrier(String value) {
        this.carrier = value;
    }

    /**
     * Obtiene el valor de la propiedad fareBasis.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFareBasis() {
        return fareBasis;
    }

    /**
     * Define el valor de la propiedad fareBasis.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFareBasis(String value) {
        this.fareBasis = value;
    }

    /**
     * Obtiene el valor de la propiedad fareComponent.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getFareComponent() {
        return fareComponent;
    }

    /**
     * Define el valor de la propiedad fareComponent.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setFareComponent(BigInteger value) {
        this.fareComponent = value;
    }

    /**
     * Obtiene el valor de la propiedad priceableUnit.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPriceableUnit() {
        return priceableUnit;
    }

    /**
     * Define el valor de la propiedad priceableUnit.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPriceableUnit(BigInteger value) {
        this.priceableUnit = value;
    }

    /**
     * Obtiene el valor de la propiedad boardPoint.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoardPoint() {
        return boardPoint;
    }

    /**
     * Define el valor de la propiedad boardPoint.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoardPoint(String value) {
        this.boardPoint = value;
    }

    /**
     * Obtiene el valor de la propiedad offPoint.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOffPoint() {
        return offPoint;
    }

    /**
     * Define el valor de la propiedad offPoint.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOffPoint(String value) {
        this.offPoint = value;
    }

    /**
     * Obtiene el valor de la propiedad minimumChangeFee.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMinimumChangeFee() {
        return minimumChangeFee;
    }

    /**
     * Define el valor de la propiedad minimumChangeFee.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMinimumChangeFee(String value) {
        this.minimumChangeFee = value;
    }

    /**
     * Obtiene el valor de la propiedad maximumChangeFee.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaximumChangeFee() {
        return maximumChangeFee;
    }

    /**
     * Define el valor de la propiedad maximumChangeFee.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaximumChangeFee(String value) {
        this.maximumChangeFee = value;
    }

    /**
     * Obtiene el valor de la propiedad filedCurrency.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFiledCurrency() {
        return filedCurrency;
    }

    /**
     * Define el valor de la propiedad filedCurrency.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFiledCurrency(String value) {
        this.filedCurrency = value;
    }

    /**
     * Obtiene el valor de la propiedad conversionRate.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getConversionRate() {
        return conversionRate;
    }

    /**
     * Define el valor de la propiedad conversionRate.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setConversionRate(BigDecimal value) {
        this.conversionRate = value;
    }

    /**
     * Obtiene el valor de la propiedad refundable.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRefundable() {
        return refundable;
    }

    /**
     * Define el valor de la propiedad refundable.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRefundable(Boolean value) {
        this.refundable = value;
    }

    /**
     * Obtiene el valor de la propiedad changeFeeApplicationCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChangeFeeApplicationCode() {
        return changeFeeApplicationCode;
    }

    /**
     * Define el valor de la propiedad changeFeeApplicationCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChangeFeeApplicationCode(String value) {
        this.changeFeeApplicationCode = value;
    }

}
