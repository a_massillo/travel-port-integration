
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="Name" use="required" type="{http://www.travelport.com/schema/air_v43_0}typeBillingDetailsName" /&gt;
 *       &lt;attribute name="DataType" use="required" type="{http://www.travelport.com/schema/air_v43_0}typeBillingDetailsDataType" /&gt;
 *       &lt;attribute name="MinLength" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="MaxLength" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="Value" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "BillingDetailItem")
public class BillingDetailItem {

    @XmlAttribute(name = "Name", required = true)
    protected TypeBillingDetailsName name;
    @XmlAttribute(name = "DataType", required = true)
    protected TypeBillingDetailsDataType dataType;
    @XmlAttribute(name = "MinLength", required = true)
    protected String minLength;
    @XmlAttribute(name = "MaxLength", required = true)
    protected String maxLength;
    @XmlAttribute(name = "Value")
    protected String value;

    /**
     * Obtiene el valor de la propiedad name.
     * 
     * @return
     *     possible object is
     *     {@link TypeBillingDetailsName }
     *     
     */
    public TypeBillingDetailsName getName() {
        return name;
    }

    /**
     * Define el valor de la propiedad name.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeBillingDetailsName }
     *     
     */
    public void setName(TypeBillingDetailsName value) {
        this.name = value;
    }

    /**
     * Obtiene el valor de la propiedad dataType.
     * 
     * @return
     *     possible object is
     *     {@link TypeBillingDetailsDataType }
     *     
     */
    public TypeBillingDetailsDataType getDataType() {
        return dataType;
    }

    /**
     * Define el valor de la propiedad dataType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeBillingDetailsDataType }
     *     
     */
    public void setDataType(TypeBillingDetailsDataType value) {
        this.dataType = value;
    }

    /**
     * Obtiene el valor de la propiedad minLength.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMinLength() {
        return minLength;
    }

    /**
     * Define el valor de la propiedad minLength.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMinLength(String value) {
        this.minLength = value;
    }

    /**
     * Obtiene el valor de la propiedad maxLength.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxLength() {
        return maxLength;
    }

    /**
     * Define el valor de la propiedad maxLength.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxLength(String value) {
        this.maxLength = value;
    }

    /**
     * Obtiene el valor de la propiedad value.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

}
