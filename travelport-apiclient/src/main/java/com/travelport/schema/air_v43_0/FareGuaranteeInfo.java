
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="GuaranteeDate" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *       &lt;attribute name="GuaranteeType" use="required" type="{http://www.travelport.com/schema/air_v43_0}typeFareGuarantee" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "FareGuaranteeInfo")
public class FareGuaranteeInfo {

    @XmlAttribute(name = "GuaranteeDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar guaranteeDate;
    @XmlAttribute(name = "GuaranteeType", required = true)
    protected TypeFareGuarantee guaranteeType;

    /**
     * Obtiene el valor de la propiedad guaranteeDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGuaranteeDate() {
        return guaranteeDate;
    }

    /**
     * Define el valor de la propiedad guaranteeDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGuaranteeDate(XMLGregorianCalendar value) {
        this.guaranteeDate = value;
    }

    /**
     * Obtiene el valor de la propiedad guaranteeType.
     * 
     * @return
     *     possible object is
     *     {@link TypeFareGuarantee }
     *     
     */
    public TypeFareGuarantee getGuaranteeType() {
        return guaranteeType;
    }

    /**
     * Define el valor de la propiedad guaranteeType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeFareGuarantee }
     *     
     */
    public void setGuaranteeType(TypeFareGuarantee value) {
        this.guaranteeType = value;
    }

}
