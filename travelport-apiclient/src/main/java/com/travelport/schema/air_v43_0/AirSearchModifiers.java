
package com.travelport.schema.air_v43_0;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.Carrier;
import com.travelport.schema.common_v43_0.Provider;
import com.travelport.schema.common_v43_0.TypeDistance;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DisfavoredProviders" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Provider" maxOccurs="999"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="PreferredProviders" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Provider" maxOccurs="999"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="DisfavoredCarriers" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Carrier" maxOccurs="999"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}PermittedCarriers" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}ProhibitedCarriers" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}PreferredCarriers" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}PermittedCabins" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}PreferredCabins" minOccurs="0"/&gt;
 *         &lt;element name="PreferredAlliances" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/air_v43_0}Alliance" maxOccurs="999"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="DisfavoredAlliances" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/air_v43_0}Alliance" maxOccurs="999"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="PermittedBookingCodes" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/air_v43_0}BookingCode" maxOccurs="999"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}PreferredBookingCodes" minOccurs="0"/&gt;
 *         &lt;element name="ProhibitedBookingCodes" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/air_v43_0}BookingCode" maxOccurs="999"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FlightType" minOccurs="0"/&gt;
 *         &lt;element name="MaxLayoverDuration" type="{http://www.travelport.com/schema/air_v43_0}MaxLayoverDurationType" minOccurs="0"/&gt;
 *         &lt;element name="NativeSearchModifier" type="{http://www.travelport.com/schema/air_v43_0}typeNativeSearchModifier" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="DistanceType" type="{http://www.travelport.com/schema/common_v43_0}typeDistance" default="MI" /&gt;
 *       &lt;attribute name="IncludeFlightDetails" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" /&gt;
 *       &lt;attribute name="AllowChangeOfAirport" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" /&gt;
 *       &lt;attribute name="ProhibitOvernightLayovers" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="MaxSolutions" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="MaxConnectionTime" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="SearchWeekends" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="IncludeExtraSolutions" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="ProhibitMultiAirportConnection" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="PreferNonStop" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="OrderBy"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;enumeration value="JourneyTime"/&gt;
 *             &lt;enumeration value="DepartureTime"/&gt;
 *             &lt;enumeration value="ArrivalTime"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="ExcludeOpenJawAirport" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="ExcludeGroundTransportation" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="MaxJourneyTime" type="{http://www.travelport.com/schema/air_v43_0}typeMaxJourneyTime" /&gt;
 *       &lt;attribute name="JetServiceOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "disfavoredProviders",
    "preferredProviders",
    "disfavoredCarriers",
    "permittedCarriers",
    "prohibitedCarriers",
    "preferredCarriers",
    "permittedCabins",
    "preferredCabins",
    "preferredAlliances",
    "disfavoredAlliances",
    "permittedBookingCodes",
    "preferredBookingCodes",
    "prohibitedBookingCodes",
    "flightType",
    "maxLayoverDuration",
    "nativeSearchModifier"
})
@XmlRootElement(name = "AirSearchModifiers")
public class AirSearchModifiers {

    @XmlElement(name = "DisfavoredProviders")
    protected AirSearchModifiers.DisfavoredProviders disfavoredProviders;
    @XmlElement(name = "PreferredProviders")
    protected AirSearchModifiers.PreferredProviders preferredProviders;
    @XmlElement(name = "DisfavoredCarriers")
    protected AirSearchModifiers.DisfavoredCarriers disfavoredCarriers;
    @XmlElement(name = "PermittedCarriers")
    protected PermittedCarriers permittedCarriers;
    @XmlElement(name = "ProhibitedCarriers")
    protected ProhibitedCarriers prohibitedCarriers;
    @XmlElement(name = "PreferredCarriers")
    protected PreferredCarriers preferredCarriers;
    @XmlElement(name = "PermittedCabins")
    protected PermittedCabins permittedCabins;
    @XmlElement(name = "PreferredCabins")
    protected PreferredCabins preferredCabins;
    @XmlElement(name = "PreferredAlliances")
    protected AirSearchModifiers.PreferredAlliances preferredAlliances;
    @XmlElement(name = "DisfavoredAlliances")
    protected AirSearchModifiers.DisfavoredAlliances disfavoredAlliances;
    @XmlElement(name = "PermittedBookingCodes")
    protected AirSearchModifiers.PermittedBookingCodes permittedBookingCodes;
    @XmlElement(name = "PreferredBookingCodes")
    protected PreferredBookingCodes preferredBookingCodes;
    @XmlElement(name = "ProhibitedBookingCodes")
    protected AirSearchModifiers.ProhibitedBookingCodes prohibitedBookingCodes;
    @XmlElement(name = "FlightType")
    protected FlightType flightType;
    @XmlElement(name = "MaxLayoverDuration")
    protected MaxLayoverDurationType maxLayoverDuration;
    @XmlElement(name = "NativeSearchModifier")
    protected TypeNativeSearchModifier nativeSearchModifier;
    @XmlAttribute(name = "DistanceType")
    protected TypeDistance distanceType;
    @XmlAttribute(name = "IncludeFlightDetails")
    protected Boolean includeFlightDetails;
    @XmlAttribute(name = "AllowChangeOfAirport")
    protected Boolean allowChangeOfAirport;
    @XmlAttribute(name = "ProhibitOvernightLayovers")
    protected Boolean prohibitOvernightLayovers;
    @XmlAttribute(name = "MaxSolutions")
    protected BigInteger maxSolutions;
    @XmlAttribute(name = "MaxConnectionTime")
    protected BigInteger maxConnectionTime;
    @XmlAttribute(name = "SearchWeekends")
    protected Boolean searchWeekends;
    @XmlAttribute(name = "IncludeExtraSolutions")
    protected Boolean includeExtraSolutions;
    @XmlAttribute(name = "ProhibitMultiAirportConnection")
    protected Boolean prohibitMultiAirportConnection;
    @XmlAttribute(name = "PreferNonStop")
    protected Boolean preferNonStop;
    @XmlAttribute(name = "OrderBy")
    protected String orderBy;
    @XmlAttribute(name = "ExcludeOpenJawAirport")
    protected Boolean excludeOpenJawAirport;
    @XmlAttribute(name = "ExcludeGroundTransportation")
    protected Boolean excludeGroundTransportation;
    @XmlAttribute(name = "MaxJourneyTime")
    protected Integer maxJourneyTime;
    @XmlAttribute(name = "JetServiceOnly")
    protected Boolean jetServiceOnly;

    /**
     * Obtiene el valor de la propiedad disfavoredProviders.
     * 
     * @return
     *     possible object is
     *     {@link AirSearchModifiers.DisfavoredProviders }
     *     
     */
    public AirSearchModifiers.DisfavoredProviders getDisfavoredProviders() {
        return disfavoredProviders;
    }

    /**
     * Define el valor de la propiedad disfavoredProviders.
     * 
     * @param value
     *     allowed object is
     *     {@link AirSearchModifiers.DisfavoredProviders }
     *     
     */
    public void setDisfavoredProviders(AirSearchModifiers.DisfavoredProviders value) {
        this.disfavoredProviders = value;
    }

    /**
     * Obtiene el valor de la propiedad preferredProviders.
     * 
     * @return
     *     possible object is
     *     {@link AirSearchModifiers.PreferredProviders }
     *     
     */
    public AirSearchModifiers.PreferredProviders getPreferredProviders() {
        return preferredProviders;
    }

    /**
     * Define el valor de la propiedad preferredProviders.
     * 
     * @param value
     *     allowed object is
     *     {@link AirSearchModifiers.PreferredProviders }
     *     
     */
    public void setPreferredProviders(AirSearchModifiers.PreferredProviders value) {
        this.preferredProviders = value;
    }

    /**
     * Obtiene el valor de la propiedad disfavoredCarriers.
     * 
     * @return
     *     possible object is
     *     {@link AirSearchModifiers.DisfavoredCarriers }
     *     
     */
    public AirSearchModifiers.DisfavoredCarriers getDisfavoredCarriers() {
        return disfavoredCarriers;
    }

    /**
     * Define el valor de la propiedad disfavoredCarriers.
     * 
     * @param value
     *     allowed object is
     *     {@link AirSearchModifiers.DisfavoredCarriers }
     *     
     */
    public void setDisfavoredCarriers(AirSearchModifiers.DisfavoredCarriers value) {
        this.disfavoredCarriers = value;
    }

    /**
     * Obtiene el valor de la propiedad permittedCarriers.
     * 
     * @return
     *     possible object is
     *     {@link PermittedCarriers }
     *     
     */
    public PermittedCarriers getPermittedCarriers() {
        return permittedCarriers;
    }

    /**
     * Define el valor de la propiedad permittedCarriers.
     * 
     * @param value
     *     allowed object is
     *     {@link PermittedCarriers }
     *     
     */
    public void setPermittedCarriers(PermittedCarriers value) {
        this.permittedCarriers = value;
    }

    /**
     * Obtiene el valor de la propiedad prohibitedCarriers.
     * 
     * @return
     *     possible object is
     *     {@link ProhibitedCarriers }
     *     
     */
    public ProhibitedCarriers getProhibitedCarriers() {
        return prohibitedCarriers;
    }

    /**
     * Define el valor de la propiedad prohibitedCarriers.
     * 
     * @param value
     *     allowed object is
     *     {@link ProhibitedCarriers }
     *     
     */
    public void setProhibitedCarriers(ProhibitedCarriers value) {
        this.prohibitedCarriers = value;
    }

    /**
     * Obtiene el valor de la propiedad preferredCarriers.
     * 
     * @return
     *     possible object is
     *     {@link PreferredCarriers }
     *     
     */
    public PreferredCarriers getPreferredCarriers() {
        return preferredCarriers;
    }

    /**
     * Define el valor de la propiedad preferredCarriers.
     * 
     * @param value
     *     allowed object is
     *     {@link PreferredCarriers }
     *     
     */
    public void setPreferredCarriers(PreferredCarriers value) {
        this.preferredCarriers = value;
    }

    /**
     * Obtiene el valor de la propiedad permittedCabins.
     * 
     * @return
     *     possible object is
     *     {@link PermittedCabins }
     *     
     */
    public PermittedCabins getPermittedCabins() {
        return permittedCabins;
    }

    /**
     * Define el valor de la propiedad permittedCabins.
     * 
     * @param value
     *     allowed object is
     *     {@link PermittedCabins }
     *     
     */
    public void setPermittedCabins(PermittedCabins value) {
        this.permittedCabins = value;
    }

    /**
     * Obtiene el valor de la propiedad preferredCabins.
     * 
     * @return
     *     possible object is
     *     {@link PreferredCabins }
     *     
     */
    public PreferredCabins getPreferredCabins() {
        return preferredCabins;
    }

    /**
     * Define el valor de la propiedad preferredCabins.
     * 
     * @param value
     *     allowed object is
     *     {@link PreferredCabins }
     *     
     */
    public void setPreferredCabins(PreferredCabins value) {
        this.preferredCabins = value;
    }

    /**
     * Obtiene el valor de la propiedad preferredAlliances.
     * 
     * @return
     *     possible object is
     *     {@link AirSearchModifiers.PreferredAlliances }
     *     
     */
    public AirSearchModifiers.PreferredAlliances getPreferredAlliances() {
        return preferredAlliances;
    }

    /**
     * Define el valor de la propiedad preferredAlliances.
     * 
     * @param value
     *     allowed object is
     *     {@link AirSearchModifiers.PreferredAlliances }
     *     
     */
    public void setPreferredAlliances(AirSearchModifiers.PreferredAlliances value) {
        this.preferredAlliances = value;
    }

    /**
     * Obtiene el valor de la propiedad disfavoredAlliances.
     * 
     * @return
     *     possible object is
     *     {@link AirSearchModifiers.DisfavoredAlliances }
     *     
     */
    public AirSearchModifiers.DisfavoredAlliances getDisfavoredAlliances() {
        return disfavoredAlliances;
    }

    /**
     * Define el valor de la propiedad disfavoredAlliances.
     * 
     * @param value
     *     allowed object is
     *     {@link AirSearchModifiers.DisfavoredAlliances }
     *     
     */
    public void setDisfavoredAlliances(AirSearchModifiers.DisfavoredAlliances value) {
        this.disfavoredAlliances = value;
    }

    /**
     * Obtiene el valor de la propiedad permittedBookingCodes.
     * 
     * @return
     *     possible object is
     *     {@link AirSearchModifiers.PermittedBookingCodes }
     *     
     */
    public AirSearchModifiers.PermittedBookingCodes getPermittedBookingCodes() {
        return permittedBookingCodes;
    }

    /**
     * Define el valor de la propiedad permittedBookingCodes.
     * 
     * @param value
     *     allowed object is
     *     {@link AirSearchModifiers.PermittedBookingCodes }
     *     
     */
    public void setPermittedBookingCodes(AirSearchModifiers.PermittedBookingCodes value) {
        this.permittedBookingCodes = value;
    }

    /**
     * Obtiene el valor de la propiedad preferredBookingCodes.
     * 
     * @return
     *     possible object is
     *     {@link PreferredBookingCodes }
     *     
     */
    public PreferredBookingCodes getPreferredBookingCodes() {
        return preferredBookingCodes;
    }

    /**
     * Define el valor de la propiedad preferredBookingCodes.
     * 
     * @param value
     *     allowed object is
     *     {@link PreferredBookingCodes }
     *     
     */
    public void setPreferredBookingCodes(PreferredBookingCodes value) {
        this.preferredBookingCodes = value;
    }

    /**
     * Obtiene el valor de la propiedad prohibitedBookingCodes.
     * 
     * @return
     *     possible object is
     *     {@link AirSearchModifiers.ProhibitedBookingCodes }
     *     
     */
    public AirSearchModifiers.ProhibitedBookingCodes getProhibitedBookingCodes() {
        return prohibitedBookingCodes;
    }

    /**
     * Define el valor de la propiedad prohibitedBookingCodes.
     * 
     * @param value
     *     allowed object is
     *     {@link AirSearchModifiers.ProhibitedBookingCodes }
     *     
     */
    public void setProhibitedBookingCodes(AirSearchModifiers.ProhibitedBookingCodes value) {
        this.prohibitedBookingCodes = value;
    }

    /**
     * Obtiene el valor de la propiedad flightType.
     * 
     * @return
     *     possible object is
     *     {@link FlightType }
     *     
     */
    public FlightType getFlightType() {
        return flightType;
    }

    /**
     * Define el valor de la propiedad flightType.
     * 
     * @param value
     *     allowed object is
     *     {@link FlightType }
     *     
     */
    public void setFlightType(FlightType value) {
        this.flightType = value;
    }

    /**
     * Obtiene el valor de la propiedad maxLayoverDuration.
     * 
     * @return
     *     possible object is
     *     {@link MaxLayoverDurationType }
     *     
     */
    public MaxLayoverDurationType getMaxLayoverDuration() {
        return maxLayoverDuration;
    }

    /**
     * Define el valor de la propiedad maxLayoverDuration.
     * 
     * @param value
     *     allowed object is
     *     {@link MaxLayoverDurationType }
     *     
     */
    public void setMaxLayoverDuration(MaxLayoverDurationType value) {
        this.maxLayoverDuration = value;
    }

    /**
     * Obtiene el valor de la propiedad nativeSearchModifier.
     * 
     * @return
     *     possible object is
     *     {@link TypeNativeSearchModifier }
     *     
     */
    public TypeNativeSearchModifier getNativeSearchModifier() {
        return nativeSearchModifier;
    }

    /**
     * Define el valor de la propiedad nativeSearchModifier.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeNativeSearchModifier }
     *     
     */
    public void setNativeSearchModifier(TypeNativeSearchModifier value) {
        this.nativeSearchModifier = value;
    }

    /**
     * Obtiene el valor de la propiedad distanceType.
     * 
     * @return
     *     possible object is
     *     {@link TypeDistance }
     *     
     */
    public TypeDistance getDistanceType() {
        if (distanceType == null) {
            return TypeDistance.MI;
        } else {
            return distanceType;
        }
    }

    /**
     * Define el valor de la propiedad distanceType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeDistance }
     *     
     */
    public void setDistanceType(TypeDistance value) {
        this.distanceType = value;
    }

    /**
     * Obtiene el valor de la propiedad includeFlightDetails.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIncludeFlightDetails() {
        if (includeFlightDetails == null) {
            return true;
        } else {
            return includeFlightDetails;
        }
    }

    /**
     * Define el valor de la propiedad includeFlightDetails.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeFlightDetails(Boolean value) {
        this.includeFlightDetails = value;
    }

    /**
     * Obtiene el valor de la propiedad allowChangeOfAirport.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isAllowChangeOfAirport() {
        if (allowChangeOfAirport == null) {
            return true;
        } else {
            return allowChangeOfAirport;
        }
    }

    /**
     * Define el valor de la propiedad allowChangeOfAirport.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowChangeOfAirport(Boolean value) {
        this.allowChangeOfAirport = value;
    }

    /**
     * Obtiene el valor de la propiedad prohibitOvernightLayovers.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isProhibitOvernightLayovers() {
        if (prohibitOvernightLayovers == null) {
            return false;
        } else {
            return prohibitOvernightLayovers;
        }
    }

    /**
     * Define el valor de la propiedad prohibitOvernightLayovers.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProhibitOvernightLayovers(Boolean value) {
        this.prohibitOvernightLayovers = value;
    }

    /**
     * Obtiene el valor de la propiedad maxSolutions.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxSolutions() {
        return maxSolutions;
    }

    /**
     * Define el valor de la propiedad maxSolutions.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxSolutions(BigInteger value) {
        this.maxSolutions = value;
    }

    /**
     * Obtiene el valor de la propiedad maxConnectionTime.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxConnectionTime() {
        return maxConnectionTime;
    }

    /**
     * Define el valor de la propiedad maxConnectionTime.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxConnectionTime(BigInteger value) {
        this.maxConnectionTime = value;
    }

    /**
     * Obtiene el valor de la propiedad searchWeekends.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSearchWeekends() {
        return searchWeekends;
    }

    /**
     * Define el valor de la propiedad searchWeekends.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSearchWeekends(Boolean value) {
        this.searchWeekends = value;
    }

    /**
     * Obtiene el valor de la propiedad includeExtraSolutions.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeExtraSolutions() {
        return includeExtraSolutions;
    }

    /**
     * Define el valor de la propiedad includeExtraSolutions.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeExtraSolutions(Boolean value) {
        this.includeExtraSolutions = value;
    }

    /**
     * Obtiene el valor de la propiedad prohibitMultiAirportConnection.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isProhibitMultiAirportConnection() {
        return prohibitMultiAirportConnection;
    }

    /**
     * Define el valor de la propiedad prohibitMultiAirportConnection.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProhibitMultiAirportConnection(Boolean value) {
        this.prohibitMultiAirportConnection = value;
    }

    /**
     * Obtiene el valor de la propiedad preferNonStop.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isPreferNonStop() {
        if (preferNonStop == null) {
            return false;
        } else {
            return preferNonStop;
        }
    }

    /**
     * Define el valor de la propiedad preferNonStop.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPreferNonStop(Boolean value) {
        this.preferNonStop = value;
    }

    /**
     * Obtiene el valor de la propiedad orderBy.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderBy() {
        return orderBy;
    }

    /**
     * Define el valor de la propiedad orderBy.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderBy(String value) {
        this.orderBy = value;
    }

    /**
     * Obtiene el valor de la propiedad excludeOpenJawAirport.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isExcludeOpenJawAirport() {
        if (excludeOpenJawAirport == null) {
            return false;
        } else {
            return excludeOpenJawAirport;
        }
    }

    /**
     * Define el valor de la propiedad excludeOpenJawAirport.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExcludeOpenJawAirport(Boolean value) {
        this.excludeOpenJawAirport = value;
    }

    /**
     * Obtiene el valor de la propiedad excludeGroundTransportation.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isExcludeGroundTransportation() {
        if (excludeGroundTransportation == null) {
            return false;
        } else {
            return excludeGroundTransportation;
        }
    }

    /**
     * Define el valor de la propiedad excludeGroundTransportation.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExcludeGroundTransportation(Boolean value) {
        this.excludeGroundTransportation = value;
    }

    /**
     * Obtiene el valor de la propiedad maxJourneyTime.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxJourneyTime() {
        return maxJourneyTime;
    }

    /**
     * Define el valor de la propiedad maxJourneyTime.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxJourneyTime(Integer value) {
        this.maxJourneyTime = value;
    }

    /**
     * Obtiene el valor de la propiedad jetServiceOnly.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isJetServiceOnly() {
        return jetServiceOnly;
    }

    /**
     * Define el valor de la propiedad jetServiceOnly.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setJetServiceOnly(Boolean value) {
        this.jetServiceOnly = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}Alliance" maxOccurs="999"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "alliance"
    })
    public static class DisfavoredAlliances {

        @XmlElement(name = "Alliance", required = true)
        protected List<Alliance> alliance;

        /**
         * Gets the value of the alliance property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the alliance property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAlliance().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Alliance }
         * 
         * 
         */
        public List<Alliance> getAlliance() {
            if (alliance == null) {
                alliance = new ArrayList<Alliance>();
            }
            return this.alliance;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Carrier" maxOccurs="999"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "carrier"
    })
    public static class DisfavoredCarriers {

        @XmlElement(name = "Carrier", namespace = "http://www.travelport.com/schema/common_v43_0", required = true)
        protected List<Carrier> carrier;

        /**
         * Gets the value of the carrier property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the carrier property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCarrier().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Carrier }
         * 
         * 
         */
        public List<Carrier> getCarrier() {
            if (carrier == null) {
                carrier = new ArrayList<Carrier>();
            }
            return this.carrier;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Provider" maxOccurs="999"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "provider"
    })
    public static class DisfavoredProviders {

        @XmlElement(name = "Provider", namespace = "http://www.travelport.com/schema/common_v43_0", required = true)
        protected List<Provider> provider;

        /**
         * Gets the value of the provider property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the provider property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getProvider().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Provider }
         * 
         * 
         */
        public List<Provider> getProvider() {
            if (provider == null) {
                provider = new ArrayList<Provider>();
            }
            return this.provider;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}BookingCode" maxOccurs="999"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "bookingCode"
    })
    public static class PermittedBookingCodes {

        @XmlElement(name = "BookingCode", required = true)
        protected List<BookingCode> bookingCode;

        /**
         * Gets the value of the bookingCode property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the bookingCode property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getBookingCode().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BookingCode }
         * 
         * 
         */
        public List<BookingCode> getBookingCode() {
            if (bookingCode == null) {
                bookingCode = new ArrayList<BookingCode>();
            }
            return this.bookingCode;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}Alliance" maxOccurs="999"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "alliance"
    })
    public static class PreferredAlliances {

        @XmlElement(name = "Alliance", required = true)
        protected List<Alliance> alliance;

        /**
         * Gets the value of the alliance property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the alliance property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAlliance().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Alliance }
         * 
         * 
         */
        public List<Alliance> getAlliance() {
            if (alliance == null) {
                alliance = new ArrayList<Alliance>();
            }
            return this.alliance;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Provider" maxOccurs="999"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "provider"
    })
    public static class PreferredProviders {

        @XmlElement(name = "Provider", namespace = "http://www.travelport.com/schema/common_v43_0", required = true)
        protected List<Provider> provider;

        /**
         * Gets the value of the provider property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the provider property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getProvider().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Provider }
         * 
         * 
         */
        public List<Provider> getProvider() {
            if (provider == null) {
                provider = new ArrayList<Provider>();
            }
            return this.provider;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}BookingCode" maxOccurs="999"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "bookingCode"
    })
    public static class ProhibitedBookingCodes {

        @XmlElement(name = "BookingCode", required = true)
        protected List<BookingCode> bookingCode;

        /**
         * Gets the value of the bookingCode property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the bookingCode property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getBookingCode().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BookingCode }
         * 
         * 
         */
        public List<BookingCode> getBookingCode() {
            if (bookingCode == null) {
                bookingCode = new ArrayList<BookingCode>();
            }
            return this.bookingCode;
        }

    }

}
