
package com.travelport.schema.air_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.travelport.schema.common_v43_0.TypePriceClassOfService;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PrivateFareOptions" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="50"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FareType" maxOccurs="100" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FareTicketDesignator" minOccurs="0"/&gt;
 *         &lt;element name="OverrideCurrency" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="CurrencyCode" type="{http://www.travelport.com/schema/common_v43_0}typeCurrency" /&gt;
 *                 &lt;attribute name="CountryCode" type="{http://www.travelport.com/schema/common_v43_0}typeCountry" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}AirSegmentPricingModifiers" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element name="WithholdTaxCode" maxOccurs="4" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;length value="2"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="PriceClassOfService" type="{http://www.travelport.com/schema/common_v43_0}typePriceClassOfService" /&gt;
 *       &lt;attribute name="CreateDate" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *       &lt;attribute name="ReissueLocCityCode" type="{http://www.travelport.com/schema/common_v43_0}typeCity" /&gt;
 *       &lt;attribute name="ReissueLocCountryCode" type="{http://www.travelport.com/schema/common_v43_0}typeCountry" /&gt;
 *       &lt;attribute name="BulkTicket" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="AccountCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="PenaltyAsTaxCode"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;length value="2"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="AirPricingSolutionRef" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="PenaltyToFare" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="PricePTCOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "privateFareOptions",
    "fareType",
    "fareTicketDesignator",
    "overrideCurrency",
    "airSegmentPricingModifiers",
    "withholdTaxCode"
})
@XmlRootElement(name = "RepricingModifiers")
public class RepricingModifiers {

    @XmlElement(name = "PrivateFareOptions")
    protected String privateFareOptions;
    @XmlElement(name = "FareType")
    protected List<FareType> fareType;
    @XmlElement(name = "FareTicketDesignator")
    protected FareTicketDesignator fareTicketDesignator;
    @XmlElement(name = "OverrideCurrency")
    protected RepricingModifiers.OverrideCurrency overrideCurrency;
    @XmlElement(name = "AirSegmentPricingModifiers")
    protected List<AirSegmentPricingModifiers> airSegmentPricingModifiers;
    @XmlElement(name = "WithholdTaxCode")
    protected List<String> withholdTaxCode;
    @XmlAttribute(name = "PriceClassOfService")
    protected TypePriceClassOfService priceClassOfService;
    @XmlAttribute(name = "CreateDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar createDate;
    @XmlAttribute(name = "ReissueLocCityCode")
    protected String reissueLocCityCode;
    @XmlAttribute(name = "ReissueLocCountryCode")
    protected String reissueLocCountryCode;
    @XmlAttribute(name = "BulkTicket")
    protected Boolean bulkTicket;
    @XmlAttribute(name = "AccountCode")
    protected String accountCode;
    @XmlAttribute(name = "PenaltyAsTaxCode")
    protected String penaltyAsTaxCode;
    @XmlAttribute(name = "AirPricingSolutionRef")
    protected String airPricingSolutionRef;
    @XmlAttribute(name = "PenaltyToFare")
    protected Boolean penaltyToFare;
    @XmlAttribute(name = "PricePTCOnly")
    protected Boolean pricePTCOnly;

    /**
     * Obtiene el valor de la propiedad privateFareOptions.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrivateFareOptions() {
        return privateFareOptions;
    }

    /**
     * Define el valor de la propiedad privateFareOptions.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrivateFareOptions(String value) {
        this.privateFareOptions = value;
    }

    /**
     * Gets the value of the fareType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fareType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFareType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FareType }
     * 
     * 
     */
    public List<FareType> getFareType() {
        if (fareType == null) {
            fareType = new ArrayList<FareType>();
        }
        return this.fareType;
    }

    /**
     * Obtiene el valor de la propiedad fareTicketDesignator.
     * 
     * @return
     *     possible object is
     *     {@link FareTicketDesignator }
     *     
     */
    public FareTicketDesignator getFareTicketDesignator() {
        return fareTicketDesignator;
    }

    /**
     * Define el valor de la propiedad fareTicketDesignator.
     * 
     * @param value
     *     allowed object is
     *     {@link FareTicketDesignator }
     *     
     */
    public void setFareTicketDesignator(FareTicketDesignator value) {
        this.fareTicketDesignator = value;
    }

    /**
     * Obtiene el valor de la propiedad overrideCurrency.
     * 
     * @return
     *     possible object is
     *     {@link RepricingModifiers.OverrideCurrency }
     *     
     */
    public RepricingModifiers.OverrideCurrency getOverrideCurrency() {
        return overrideCurrency;
    }

    /**
     * Define el valor de la propiedad overrideCurrency.
     * 
     * @param value
     *     allowed object is
     *     {@link RepricingModifiers.OverrideCurrency }
     *     
     */
    public void setOverrideCurrency(RepricingModifiers.OverrideCurrency value) {
        this.overrideCurrency = value;
    }

    /**
     * Gets the value of the airSegmentPricingModifiers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the airSegmentPricingModifiers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAirSegmentPricingModifiers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AirSegmentPricingModifiers }
     * 
     * 
     */
    public List<AirSegmentPricingModifiers> getAirSegmentPricingModifiers() {
        if (airSegmentPricingModifiers == null) {
            airSegmentPricingModifiers = new ArrayList<AirSegmentPricingModifiers>();
        }
        return this.airSegmentPricingModifiers;
    }

    /**
     * Gets the value of the withholdTaxCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the withholdTaxCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWithholdTaxCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getWithholdTaxCode() {
        if (withholdTaxCode == null) {
            withholdTaxCode = new ArrayList<String>();
        }
        return this.withholdTaxCode;
    }

    /**
     * Obtiene el valor de la propiedad priceClassOfService.
     * 
     * @return
     *     possible object is
     *     {@link TypePriceClassOfService }
     *     
     */
    public TypePriceClassOfService getPriceClassOfService() {
        return priceClassOfService;
    }

    /**
     * Define el valor de la propiedad priceClassOfService.
     * 
     * @param value
     *     allowed object is
     *     {@link TypePriceClassOfService }
     *     
     */
    public void setPriceClassOfService(TypePriceClassOfService value) {
        this.priceClassOfService = value;
    }

    /**
     * Obtiene el valor de la propiedad createDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreateDate() {
        return createDate;
    }

    /**
     * Define el valor de la propiedad createDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreateDate(XMLGregorianCalendar value) {
        this.createDate = value;
    }

    /**
     * Obtiene el valor de la propiedad reissueLocCityCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReissueLocCityCode() {
        return reissueLocCityCode;
    }

    /**
     * Define el valor de la propiedad reissueLocCityCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReissueLocCityCode(String value) {
        this.reissueLocCityCode = value;
    }

    /**
     * Obtiene el valor de la propiedad reissueLocCountryCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReissueLocCountryCode() {
        return reissueLocCountryCode;
    }

    /**
     * Define el valor de la propiedad reissueLocCountryCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReissueLocCountryCode(String value) {
        this.reissueLocCountryCode = value;
    }

    /**
     * Obtiene el valor de la propiedad bulkTicket.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isBulkTicket() {
        if (bulkTicket == null) {
            return false;
        } else {
            return bulkTicket;
        }
    }

    /**
     * Define el valor de la propiedad bulkTicket.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBulkTicket(Boolean value) {
        this.bulkTicket = value;
    }

    /**
     * Obtiene el valor de la propiedad accountCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountCode() {
        return accountCode;
    }

    /**
     * Define el valor de la propiedad accountCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountCode(String value) {
        this.accountCode = value;
    }

    /**
     * Obtiene el valor de la propiedad penaltyAsTaxCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPenaltyAsTaxCode() {
        return penaltyAsTaxCode;
    }

    /**
     * Define el valor de la propiedad penaltyAsTaxCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPenaltyAsTaxCode(String value) {
        this.penaltyAsTaxCode = value;
    }

    /**
     * Obtiene el valor de la propiedad airPricingSolutionRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirPricingSolutionRef() {
        return airPricingSolutionRef;
    }

    /**
     * Define el valor de la propiedad airPricingSolutionRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirPricingSolutionRef(String value) {
        this.airPricingSolutionRef = value;
    }

    /**
     * Obtiene el valor de la propiedad penaltyToFare.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPenaltyToFare() {
        return penaltyToFare;
    }

    /**
     * Define el valor de la propiedad penaltyToFare.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPenaltyToFare(Boolean value) {
        this.penaltyToFare = value;
    }

    /**
     * Obtiene el valor de la propiedad pricePTCOnly.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isPricePTCOnly() {
        if (pricePTCOnly == null) {
            return false;
        } else {
            return pricePTCOnly;
        }
    }

    /**
     * Define el valor de la propiedad pricePTCOnly.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPricePTCOnly(Boolean value) {
        this.pricePTCOnly = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attribute name="CurrencyCode" type="{http://www.travelport.com/schema/common_v43_0}typeCurrency" /&gt;
     *       &lt;attribute name="CountryCode" type="{http://www.travelport.com/schema/common_v43_0}typeCountry" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class OverrideCurrency {

        @XmlAttribute(name = "CurrencyCode")
        protected String currencyCode;
        @XmlAttribute(name = "CountryCode")
        protected String countryCode;

        /**
         * Obtiene el valor de la propiedad currencyCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCurrencyCode() {
            return currencyCode;
        }

        /**
         * Define el valor de la propiedad currencyCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCurrencyCode(String value) {
            this.currencyCode = value;
        }

        /**
         * Obtiene el valor de la propiedad countryCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCountryCode() {
            return countryCode;
        }

        /**
         * Define el valor de la propiedad countryCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCountryCode(String value) {
            this.countryCode = value;
        }

    }

}
