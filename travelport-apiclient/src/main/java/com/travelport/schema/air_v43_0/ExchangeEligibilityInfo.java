
package com.travelport.schema.air_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}ExchangePenaltyInfo" maxOccurs="999" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="EligibleFares" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="RefundableFares" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="PassedAutomationChecks" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "exchangePenaltyInfo"
})
@XmlRootElement(name = "ExchangeEligibilityInfo")
public class ExchangeEligibilityInfo {

    @XmlElement(name = "ExchangePenaltyInfo")
    protected List<ExchangePenaltyInfo> exchangePenaltyInfo;
    @XmlAttribute(name = "EligibleFares")
    protected String eligibleFares;
    @XmlAttribute(name = "RefundableFares")
    protected String refundableFares;
    @XmlAttribute(name = "PassedAutomationChecks")
    protected Boolean passedAutomationChecks;

    /**
     * Gets the value of the exchangePenaltyInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the exchangePenaltyInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExchangePenaltyInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ExchangePenaltyInfo }
     * 
     * 
     */
    public List<ExchangePenaltyInfo> getExchangePenaltyInfo() {
        if (exchangePenaltyInfo == null) {
            exchangePenaltyInfo = new ArrayList<ExchangePenaltyInfo>();
        }
        return this.exchangePenaltyInfo;
    }

    /**
     * Obtiene el valor de la propiedad eligibleFares.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEligibleFares() {
        return eligibleFares;
    }

    /**
     * Define el valor de la propiedad eligibleFares.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEligibleFares(String value) {
        this.eligibleFares = value;
    }

    /**
     * Obtiene el valor de la propiedad refundableFares.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefundableFares() {
        return refundableFares;
    }

    /**
     * Define el valor de la propiedad refundableFares.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefundableFares(String value) {
        this.refundableFares = value;
    }

    /**
     * Obtiene el valor de la propiedad passedAutomationChecks.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPassedAutomationChecks() {
        return passedAutomationChecks;
    }

    /**
     * Define el valor de la propiedad passedAutomationChecks.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPassedAutomationChecks(Boolean value) {
        this.passedAutomationChecks = value;
    }

}
