
package com.travelport.schema.air_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.AccountCode;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FareDisplayRule"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FarePricing" maxOccurs="999"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FareRestriction" maxOccurs="99" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FareRoutingInformation" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FareMileageInformation" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}AirFareDisplayRuleKey" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}BookingCode" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}AccountCode" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}AddlBookingCodeInformation" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FareRuleFailureInfo" minOccurs="0"/&gt;
 *         &lt;element name="PriceChange" type="{http://www.travelport.com/schema/air_v43_0}PriceChangeType" maxOccurs="99" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Carrier" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeCarrier" /&gt;
 *       &lt;attribute name="FareBasis" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="Amount" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="TripType" type="{http://www.travelport.com/schema/air_v43_0}typeFareTripType" /&gt;
 *       &lt;attribute name="FareTypeCode" type="{http://www.travelport.com/schema/air_v43_0}typeFareTypeCode" /&gt;
 *       &lt;attribute name="SpecialFare" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="InstantPurchase" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="EligibilityRestricted" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="FlightRestricted" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="StopoversRestricted" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="TransfersRestricted" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="BlackoutsExist" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="AccompaniedTravel" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="MileOrRouteBasedFare" type="{http://www.travelport.com/schema/air_v43_0}typeMileOrRouteBasedFare" /&gt;
 *       &lt;attribute name="GlobalIndicator" type="{http://www.travelport.com/schema/air_v43_0}typeATPCOGlobalIndicator" /&gt;
 *       &lt;attribute name="Origin" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *       &lt;attribute name="Destination" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *       &lt;attribute name="FareTicketingCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="FareTicketingDesignator" type="{http://www.travelport.com/schema/air_v43_0}typeTicketDesignator" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fareDisplayRule",
    "farePricing",
    "fareRestriction",
    "fareRoutingInformation",
    "fareMileageInformation",
    "airFareDisplayRuleKey",
    "bookingCode",
    "accountCode",
    "addlBookingCodeInformation",
    "fareRuleFailureInfo",
    "priceChange"
})
@XmlRootElement(name = "FareDisplay")
public class FareDisplay {

    @XmlElement(name = "FareDisplayRule", required = true)
    protected FareDisplayRule fareDisplayRule;
    @XmlElement(name = "FarePricing", required = true)
    protected List<FarePricing> farePricing;
    @XmlElement(name = "FareRestriction")
    protected List<FareRestriction> fareRestriction;
    @XmlElement(name = "FareRoutingInformation")
    protected String fareRoutingInformation;
    @XmlElement(name = "FareMileageInformation")
    protected String fareMileageInformation;
    @XmlElement(name = "AirFareDisplayRuleKey")
    protected AirFareDisplayRuleKey airFareDisplayRuleKey;
    @XmlElement(name = "BookingCode")
    protected List<BookingCode> bookingCode;
    @XmlElement(name = "AccountCode", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected List<AccountCode> accountCode;
    @XmlElement(name = "AddlBookingCodeInformation")
    protected AddlBookingCodeInformation addlBookingCodeInformation;
    @XmlElement(name = "FareRuleFailureInfo")
    protected FareRuleFailureInfo fareRuleFailureInfo;
    @XmlElement(name = "PriceChange")
    protected List<PriceChangeType> priceChange;
    @XmlAttribute(name = "Carrier", required = true)
    protected String carrier;
    @XmlAttribute(name = "FareBasis", required = true)
    protected String fareBasis;
    @XmlAttribute(name = "Amount", required = true)
    protected String amount;
    @XmlAttribute(name = "TripType")
    protected TypeFareTripType tripType;
    @XmlAttribute(name = "FareTypeCode")
    protected String fareTypeCode;
    @XmlAttribute(name = "SpecialFare")
    protected Boolean specialFare;
    @XmlAttribute(name = "InstantPurchase")
    protected Boolean instantPurchase;
    @XmlAttribute(name = "EligibilityRestricted")
    protected Boolean eligibilityRestricted;
    @XmlAttribute(name = "FlightRestricted")
    protected Boolean flightRestricted;
    @XmlAttribute(name = "StopoversRestricted")
    protected Boolean stopoversRestricted;
    @XmlAttribute(name = "TransfersRestricted")
    protected Boolean transfersRestricted;
    @XmlAttribute(name = "BlackoutsExist")
    protected Boolean blackoutsExist;
    @XmlAttribute(name = "AccompaniedTravel")
    protected Boolean accompaniedTravel;
    @XmlAttribute(name = "MileOrRouteBasedFare")
    protected TypeMileOrRouteBasedFare mileOrRouteBasedFare;
    @XmlAttribute(name = "GlobalIndicator")
    protected TypeATPCOGlobalIndicator globalIndicator;
    @XmlAttribute(name = "Origin")
    protected String origin;
    @XmlAttribute(name = "Destination")
    protected String destination;
    @XmlAttribute(name = "FareTicketingCode")
    protected String fareTicketingCode;
    @XmlAttribute(name = "FareTicketingDesignator")
    protected String fareTicketingDesignator;

    /**
     * Obtiene el valor de la propiedad fareDisplayRule.
     * 
     * @return
     *     possible object is
     *     {@link FareDisplayRule }
     *     
     */
    public FareDisplayRule getFareDisplayRule() {
        return fareDisplayRule;
    }

    /**
     * Define el valor de la propiedad fareDisplayRule.
     * 
     * @param value
     *     allowed object is
     *     {@link FareDisplayRule }
     *     
     */
    public void setFareDisplayRule(FareDisplayRule value) {
        this.fareDisplayRule = value;
    }

    /**
     * Gets the value of the farePricing property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the farePricing property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFarePricing().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FarePricing }
     * 
     * 
     */
    public List<FarePricing> getFarePricing() {
        if (farePricing == null) {
            farePricing = new ArrayList<FarePricing>();
        }
        return this.farePricing;
    }

    /**
     * Gets the value of the fareRestriction property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fareRestriction property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFareRestriction().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FareRestriction }
     * 
     * 
     */
    public List<FareRestriction> getFareRestriction() {
        if (fareRestriction == null) {
            fareRestriction = new ArrayList<FareRestriction>();
        }
        return this.fareRestriction;
    }

    /**
     * Obtiene el valor de la propiedad fareRoutingInformation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFareRoutingInformation() {
        return fareRoutingInformation;
    }

    /**
     * Define el valor de la propiedad fareRoutingInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFareRoutingInformation(String value) {
        this.fareRoutingInformation = value;
    }

    /**
     * Obtiene el valor de la propiedad fareMileageInformation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFareMileageInformation() {
        return fareMileageInformation;
    }

    /**
     * Define el valor de la propiedad fareMileageInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFareMileageInformation(String value) {
        this.fareMileageInformation = value;
    }

    /**
     * Obtiene el valor de la propiedad airFareDisplayRuleKey.
     * 
     * @return
     *     possible object is
     *     {@link AirFareDisplayRuleKey }
     *     
     */
    public AirFareDisplayRuleKey getAirFareDisplayRuleKey() {
        return airFareDisplayRuleKey;
    }

    /**
     * Define el valor de la propiedad airFareDisplayRuleKey.
     * 
     * @param value
     *     allowed object is
     *     {@link AirFareDisplayRuleKey }
     *     
     */
    public void setAirFareDisplayRuleKey(AirFareDisplayRuleKey value) {
        this.airFareDisplayRuleKey = value;
    }

    /**
     * Gets the value of the bookingCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bookingCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBookingCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BookingCode }
     * 
     * 
     */
    public List<BookingCode> getBookingCode() {
        if (bookingCode == null) {
            bookingCode = new ArrayList<BookingCode>();
        }
        return this.bookingCode;
    }

    /**
     * Gets the value of the accountCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accountCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccountCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccountCode }
     * 
     * 
     */
    public List<AccountCode> getAccountCode() {
        if (accountCode == null) {
            accountCode = new ArrayList<AccountCode>();
        }
        return this.accountCode;
    }

    /**
     * Obtiene el valor de la propiedad addlBookingCodeInformation.
     * 
     * @return
     *     possible object is
     *     {@link AddlBookingCodeInformation }
     *     
     */
    public AddlBookingCodeInformation getAddlBookingCodeInformation() {
        return addlBookingCodeInformation;
    }

    /**
     * Define el valor de la propiedad addlBookingCodeInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link AddlBookingCodeInformation }
     *     
     */
    public void setAddlBookingCodeInformation(AddlBookingCodeInformation value) {
        this.addlBookingCodeInformation = value;
    }

    /**
     * Returns fare rule failure info for Non
     *                             Valid fares.
     * 
     * @return
     *     possible object is
     *     {@link FareRuleFailureInfo }
     *     
     */
    public FareRuleFailureInfo getFareRuleFailureInfo() {
        return fareRuleFailureInfo;
    }

    /**
     * Define el valor de la propiedad fareRuleFailureInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link FareRuleFailureInfo }
     *     
     */
    public void setFareRuleFailureInfo(FareRuleFailureInfo value) {
        this.fareRuleFailureInfo = value;
    }

    /**
     * Gets the value of the priceChange property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the priceChange property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPriceChange().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PriceChangeType }
     * 
     * 
     */
    public List<PriceChangeType> getPriceChange() {
        if (priceChange == null) {
            priceChange = new ArrayList<PriceChangeType>();
        }
        return this.priceChange;
    }

    /**
     * Obtiene el valor de la propiedad carrier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCarrier() {
        return carrier;
    }

    /**
     * Define el valor de la propiedad carrier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarrier(String value) {
        this.carrier = value;
    }

    /**
     * Obtiene el valor de la propiedad fareBasis.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFareBasis() {
        return fareBasis;
    }

    /**
     * Define el valor de la propiedad fareBasis.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFareBasis(String value) {
        this.fareBasis = value;
    }

    /**
     * Obtiene el valor de la propiedad amount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmount() {
        return amount;
    }

    /**
     * Define el valor de la propiedad amount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmount(String value) {
        this.amount = value;
    }

    /**
     * Obtiene el valor de la propiedad tripType.
     * 
     * @return
     *     possible object is
     *     {@link TypeFareTripType }
     *     
     */
    public TypeFareTripType getTripType() {
        return tripType;
    }

    /**
     * Define el valor de la propiedad tripType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeFareTripType }
     *     
     */
    public void setTripType(TypeFareTripType value) {
        this.tripType = value;
    }

    /**
     * Obtiene el valor de la propiedad fareTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFareTypeCode() {
        return fareTypeCode;
    }

    /**
     * Define el valor de la propiedad fareTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFareTypeCode(String value) {
        this.fareTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad specialFare.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSpecialFare() {
        return specialFare;
    }

    /**
     * Define el valor de la propiedad specialFare.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSpecialFare(Boolean value) {
        this.specialFare = value;
    }

    /**
     * Obtiene el valor de la propiedad instantPurchase.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isInstantPurchase() {
        return instantPurchase;
    }

    /**
     * Define el valor de la propiedad instantPurchase.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInstantPurchase(Boolean value) {
        this.instantPurchase = value;
    }

    /**
     * Obtiene el valor de la propiedad eligibilityRestricted.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEligibilityRestricted() {
        return eligibilityRestricted;
    }

    /**
     * Define el valor de la propiedad eligibilityRestricted.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEligibilityRestricted(Boolean value) {
        this.eligibilityRestricted = value;
    }

    /**
     * Obtiene el valor de la propiedad flightRestricted.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFlightRestricted() {
        return flightRestricted;
    }

    /**
     * Define el valor de la propiedad flightRestricted.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFlightRestricted(Boolean value) {
        this.flightRestricted = value;
    }

    /**
     * Obtiene el valor de la propiedad stopoversRestricted.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isStopoversRestricted() {
        return stopoversRestricted;
    }

    /**
     * Define el valor de la propiedad stopoversRestricted.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStopoversRestricted(Boolean value) {
        this.stopoversRestricted = value;
    }

    /**
     * Obtiene el valor de la propiedad transfersRestricted.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTransfersRestricted() {
        return transfersRestricted;
    }

    /**
     * Define el valor de la propiedad transfersRestricted.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTransfersRestricted(Boolean value) {
        this.transfersRestricted = value;
    }

    /**
     * Obtiene el valor de la propiedad blackoutsExist.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBlackoutsExist() {
        return blackoutsExist;
    }

    /**
     * Define el valor de la propiedad blackoutsExist.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBlackoutsExist(Boolean value) {
        this.blackoutsExist = value;
    }

    /**
     * Obtiene el valor de la propiedad accompaniedTravel.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAccompaniedTravel() {
        return accompaniedTravel;
    }

    /**
     * Define el valor de la propiedad accompaniedTravel.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAccompaniedTravel(Boolean value) {
        this.accompaniedTravel = value;
    }

    /**
     * Obtiene el valor de la propiedad mileOrRouteBasedFare.
     * 
     * @return
     *     possible object is
     *     {@link TypeMileOrRouteBasedFare }
     *     
     */
    public TypeMileOrRouteBasedFare getMileOrRouteBasedFare() {
        return mileOrRouteBasedFare;
    }

    /**
     * Define el valor de la propiedad mileOrRouteBasedFare.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeMileOrRouteBasedFare }
     *     
     */
    public void setMileOrRouteBasedFare(TypeMileOrRouteBasedFare value) {
        this.mileOrRouteBasedFare = value;
    }

    /**
     * Obtiene el valor de la propiedad globalIndicator.
     * 
     * @return
     *     possible object is
     *     {@link TypeATPCOGlobalIndicator }
     *     
     */
    public TypeATPCOGlobalIndicator getGlobalIndicator() {
        return globalIndicator;
    }

    /**
     * Define el valor de la propiedad globalIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeATPCOGlobalIndicator }
     *     
     */
    public void setGlobalIndicator(TypeATPCOGlobalIndicator value) {
        this.globalIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad origin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * Define el valor de la propiedad origin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigin(String value) {
        this.origin = value;
    }

    /**
     * Obtiene el valor de la propiedad destination.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestination() {
        return destination;
    }

    /**
     * Define el valor de la propiedad destination.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestination(String value) {
        this.destination = value;
    }

    /**
     * Obtiene el valor de la propiedad fareTicketingCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFareTicketingCode() {
        return fareTicketingCode;
    }

    /**
     * Define el valor de la propiedad fareTicketingCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFareTicketingCode(String value) {
        this.fareTicketingCode = value;
    }

    /**
     * Obtiene el valor de la propiedad fareTicketingDesignator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFareTicketingDesignator() {
        return fareTicketingDesignator;
    }

    /**
     * Define el valor de la propiedad fareTicketingDesignator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFareTicketingDesignator(String value) {
        this.fareTicketingDesignator = value;
    }

}
