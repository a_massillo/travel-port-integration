
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MinimumStay" type="{http://www.travelport.com/schema/air_v43_0}typeRestrictionLengthOfStay" minOccurs="0"/&gt;
 *         &lt;element name="MaximumStay" type="{http://www.travelport.com/schema/air_v43_0}typeRestrictionLengthOfStay" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "minimumStay",
    "maximumStay"
})
@XmlRootElement(name = "RuleLengthOfStay")
public class RuleLengthOfStay {

    @XmlElement(name = "MinimumStay")
    protected TypeRestrictionLengthOfStay minimumStay;
    @XmlElement(name = "MaximumStay")
    protected TypeRestrictionLengthOfStay maximumStay;

    /**
     * Obtiene el valor de la propiedad minimumStay.
     * 
     * @return
     *     possible object is
     *     {@link TypeRestrictionLengthOfStay }
     *     
     */
    public TypeRestrictionLengthOfStay getMinimumStay() {
        return minimumStay;
    }

    /**
     * Define el valor de la propiedad minimumStay.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeRestrictionLengthOfStay }
     *     
     */
    public void setMinimumStay(TypeRestrictionLengthOfStay value) {
        this.minimumStay = value;
    }

    /**
     * Obtiene el valor de la propiedad maximumStay.
     * 
     * @return
     *     possible object is
     *     {@link TypeRestrictionLengthOfStay }
     *     
     */
    public TypeRestrictionLengthOfStay getMaximumStay() {
        return maximumStay;
    }

    /**
     * Define el valor de la propiedad maximumStay.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeRestrictionLengthOfStay }
     *     
     */
    public void setMaximumStay(TypeRestrictionLengthOfStay value) {
        this.maximumStay = value;
    }

}
