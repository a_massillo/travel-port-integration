
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="Direction" type="{http://www.travelport.com/schema/air_v43_0}typeFareDirectionality" /&gt;
 *       &lt;attribute name="TripType" type="{http://www.travelport.com/schema/air_v43_0}typeFareTripType" /&gt;
 *       &lt;attribute name="Monday" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="Tuesday" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="Wednesday" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="Thursday" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="Friday" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="Saturday" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="Sunday" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "FareRestrictionDaysOfWeek")
public class FareRestrictionDaysOfWeek {

    @XmlAttribute(name = "Direction")
    protected TypeFareDirectionality direction;
    @XmlAttribute(name = "TripType")
    protected TypeFareTripType tripType;
    @XmlAttribute(name = "Monday")
    protected Boolean monday;
    @XmlAttribute(name = "Tuesday")
    protected Boolean tuesday;
    @XmlAttribute(name = "Wednesday")
    protected Boolean wednesday;
    @XmlAttribute(name = "Thursday")
    protected Boolean thursday;
    @XmlAttribute(name = "Friday")
    protected Boolean friday;
    @XmlAttribute(name = "Saturday")
    protected Boolean saturday;
    @XmlAttribute(name = "Sunday")
    protected Boolean sunday;

    /**
     * Obtiene el valor de la propiedad direction.
     * 
     * @return
     *     possible object is
     *     {@link TypeFareDirectionality }
     *     
     */
    public TypeFareDirectionality getDirection() {
        return direction;
    }

    /**
     * Define el valor de la propiedad direction.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeFareDirectionality }
     *     
     */
    public void setDirection(TypeFareDirectionality value) {
        this.direction = value;
    }

    /**
     * Obtiene el valor de la propiedad tripType.
     * 
     * @return
     *     possible object is
     *     {@link TypeFareTripType }
     *     
     */
    public TypeFareTripType getTripType() {
        return tripType;
    }

    /**
     * Define el valor de la propiedad tripType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeFareTripType }
     *     
     */
    public void setTripType(TypeFareTripType value) {
        this.tripType = value;
    }

    /**
     * Obtiene el valor de la propiedad monday.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMonday() {
        return monday;
    }

    /**
     * Define el valor de la propiedad monday.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMonday(Boolean value) {
        this.monday = value;
    }

    /**
     * Obtiene el valor de la propiedad tuesday.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTuesday() {
        return tuesday;
    }

    /**
     * Define el valor de la propiedad tuesday.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTuesday(Boolean value) {
        this.tuesday = value;
    }

    /**
     * Obtiene el valor de la propiedad wednesday.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWednesday() {
        return wednesday;
    }

    /**
     * Define el valor de la propiedad wednesday.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWednesday(Boolean value) {
        this.wednesday = value;
    }

    /**
     * Obtiene el valor de la propiedad thursday.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isThursday() {
        return thursday;
    }

    /**
     * Define el valor de la propiedad thursday.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setThursday(Boolean value) {
        this.thursday = value;
    }

    /**
     * Obtiene el valor de la propiedad friday.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFriday() {
        return friday;
    }

    /**
     * Define el valor de la propiedad friday.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFriday(Boolean value) {
        this.friday = value;
    }

    /**
     * Obtiene el valor de la propiedad saturday.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSaturday() {
        return saturday;
    }

    /**
     * Define el valor de la propiedad saturday.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSaturday(Boolean value) {
        this.saturday = value;
    }

    /**
     * Obtiene el valor de la propiedad sunday.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSunday() {
        return sunday;
    }

    /**
     * Define el valor de la propiedad sunday.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSunday(Boolean value) {
        this.sunday = value;
    }

}
