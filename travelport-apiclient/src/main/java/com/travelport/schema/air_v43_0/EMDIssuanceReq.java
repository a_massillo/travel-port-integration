
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseReq;
import com.travelport.schema.common_v43_0.TypeProviderReservationDetail;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseReq"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}ProviderReservationDetail"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}TicketNumber" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}IssuanceModifiers" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}SelectionModifiers" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="UniversalRecordLocatorCode" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeLocatorCode" /&gt;
 *       &lt;attribute name="ShowDetails" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "providerReservationDetail",
    "ticketNumber",
    "issuanceModifiers",
    "selectionModifiers"
})
@XmlRootElement(name = "EMDIssuanceReq")
public class EMDIssuanceReq
    extends BaseReq
{

    @XmlElement(name = "ProviderReservationDetail", namespace = "http://www.travelport.com/schema/common_v43_0", required = true)
    protected TypeProviderReservationDetail providerReservationDetail;
    @XmlElement(name = "TicketNumber", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected String ticketNumber;
    @XmlElement(name = "IssuanceModifiers")
    protected IssuanceModifiers issuanceModifiers;
    @XmlElement(name = "SelectionModifiers")
    protected SelectionModifiers selectionModifiers;
    @XmlAttribute(name = "UniversalRecordLocatorCode", required = true)
    protected String universalRecordLocatorCode;
    @XmlAttribute(name = "ShowDetails")
    protected Boolean showDetails;

    /**
     * PNR information for which EMD is going to be issued.
     * 
     * @return
     *     possible object is
     *     {@link TypeProviderReservationDetail }
     *     
     */
    public TypeProviderReservationDetail getProviderReservationDetail() {
        return providerReservationDetail;
    }

    /**
     * Define el valor de la propiedad providerReservationDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeProviderReservationDetail }
     *     
     */
    public void setProviderReservationDetail(TypeProviderReservationDetail value) {
        this.providerReservationDetail = value;
    }

    /**
     * Ticket number for which EMD is going to be issued.Required for EMD-A issuance.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketNumber() {
        return ticketNumber;
    }

    /**
     * Define el valor de la propiedad ticketNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketNumber(String value) {
        this.ticketNumber = value;
    }

    /**
     * General modifiers related to EMD issuance.
     * 
     * @return
     *     possible object is
     *     {@link IssuanceModifiers }
     *     
     */
    public IssuanceModifiers getIssuanceModifiers() {
        return issuanceModifiers;
    }

    /**
     * Define el valor de la propiedad issuanceModifiers.
     * 
     * @param value
     *     allowed object is
     *     {@link IssuanceModifiers }
     *     
     */
    public void setIssuanceModifiers(IssuanceModifiers value) {
        this.issuanceModifiers = value;
    }

    /**
     * Modifiers related to selection of services during EMD issuance.
     * 
     * @return
     *     possible object is
     *     {@link SelectionModifiers }
     *     
     */
    public SelectionModifiers getSelectionModifiers() {
        return selectionModifiers;
    }

    /**
     * Define el valor de la propiedad selectionModifiers.
     * 
     * @param value
     *     allowed object is
     *     {@link SelectionModifiers }
     *     
     */
    public void setSelectionModifiers(SelectionModifiers value) {
        this.selectionModifiers = value;
    }

    /**
     * Obtiene el valor de la propiedad universalRecordLocatorCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniversalRecordLocatorCode() {
        return universalRecordLocatorCode;
    }

    /**
     * Define el valor de la propiedad universalRecordLocatorCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniversalRecordLocatorCode(String value) {
        this.universalRecordLocatorCode = value;
    }

    /**
     * Obtiene el valor de la propiedad showDetails.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isShowDetails() {
        if (showDetails == null) {
            return false;
        } else {
            return showDetails;
        }
    }

    /**
     * Define el valor de la propiedad showDetails.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShowDetails(Boolean value) {
        this.showDetails = value;
    }

}
