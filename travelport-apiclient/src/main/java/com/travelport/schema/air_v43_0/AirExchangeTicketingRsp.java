
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseRsp;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseRsp"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}AirSolutionChangedInfo" minOccurs="0"/&gt;
 *         &lt;sequence&gt;
 *           &lt;element ref="{http://www.travelport.com/schema/air_v43_0}ETR" minOccurs="0"/&gt;
 *           &lt;element ref="{http://www.travelport.com/schema/air_v43_0}TicketFailureInfo" minOccurs="0"/&gt;
 *           &lt;element ref="{http://www.travelport.com/schema/air_v43_0}DetailedBillingInformation" minOccurs="0"/&gt;
 *         &lt;/sequence&gt;
 *       &lt;/choice&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "airSolutionChangedInfo",
    "etr",
    "ticketFailureInfo",
    "detailedBillingInformation"
})
@XmlRootElement(name = "AirExchangeTicketingRsp")
public class AirExchangeTicketingRsp
    extends BaseRsp
{

    @XmlElement(name = "AirSolutionChangedInfo")
    protected AirSolutionChangedInfo airSolutionChangedInfo;
    @XmlElement(name = "ETR")
    protected ETR etr;
    @XmlElement(name = "TicketFailureInfo")
    protected TicketFailureInfo ticketFailureInfo;
    @XmlElement(name = "DetailedBillingInformation")
    protected DetailedBillingInformation detailedBillingInformation;

    /**
     * Obtiene el valor de la propiedad airSolutionChangedInfo.
     * 
     * @return
     *     possible object is
     *     {@link AirSolutionChangedInfo }
     *     
     */
    public AirSolutionChangedInfo getAirSolutionChangedInfo() {
        return airSolutionChangedInfo;
    }

    /**
     * Define el valor de la propiedad airSolutionChangedInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link AirSolutionChangedInfo }
     *     
     */
    public void setAirSolutionChangedInfo(AirSolutionChangedInfo value) {
        this.airSolutionChangedInfo = value;
    }

    /**
     * Provider 1G, 1V, 1P.
     * 
     * @return
     *     possible object is
     *     {@link ETR }
     *     
     */
    public ETR getETR() {
        return etr;
    }

    /**
     * Define el valor de la propiedad etr.
     * 
     * @param value
     *     allowed object is
     *     {@link ETR }
     *     
     */
    public void setETR(ETR value) {
        this.etr = value;
    }

    /**
     * Provider 1G, 1V, 1P.
     * 
     * @return
     *     possible object is
     *     {@link TicketFailureInfo }
     *     
     */
    public TicketFailureInfo getTicketFailureInfo() {
        return ticketFailureInfo;
    }

    /**
     * Define el valor de la propiedad ticketFailureInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link TicketFailureInfo }
     *     
     */
    public void setTicketFailureInfo(TicketFailureInfo value) {
        this.ticketFailureInfo = value;
    }

    /**
     * Providers 1G, 1V, 1P.
     * 
     * @return
     *     possible object is
     *     {@link DetailedBillingInformation }
     *     
     */
    public DetailedBillingInformation getDetailedBillingInformation() {
        return detailedBillingInformation;
    }

    /**
     * Define el valor de la propiedad detailedBillingInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link DetailedBillingInformation }
     *     
     */
    public void setDetailedBillingInformation(DetailedBillingInformation value) {
        this.detailedBillingInformation = value;
    }

}
