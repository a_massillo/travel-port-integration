
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * A container for EMD elements related to an
 *                     OptionalService
 * 
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="FulfillmentType"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer"&gt;
 *             &lt;minInclusive value="1"/&gt;
 *             &lt;maxInclusive value="5"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="FulfillmentTypeDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="AssociatedItem" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="AvailabilityChargeIndicator"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;enumeration value="X"/&gt;
 *             &lt;enumeration value="E"/&gt;
 *             &lt;enumeration value="F"/&gt;
 *             &lt;enumeration value="G"/&gt;
 *             &lt;enumeration value="H"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="RefundReissueIndicator"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;enumeration value="Refundable"/&gt;
 *             &lt;enumeration value="NonRefundable"/&gt;
 *             &lt;enumeration value="Reuse"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Commissionable"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}boolean"&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="MileageIndicator"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}boolean"&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Location"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.travelport.com/schema/common_v43_0}typeIATACode"&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Date"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}dateTime"&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Booking"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.travelport.com/schema/air_v43_0}typeBooking"&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="DisplayCategory"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.travelport.com/schema/air_v43_0}typeDisplayCategory"&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Reusable" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "EMD")
public class EMD {

    @XmlAttribute(name = "FulfillmentType")
    protected Integer fulfillmentType;
    @XmlAttribute(name = "FulfillmentTypeDescription")
    protected String fulfillmentTypeDescription;
    @XmlAttribute(name = "AssociatedItem")
    protected String associatedItem;
    @XmlAttribute(name = "AvailabilityChargeIndicator")
    protected String availabilityChargeIndicator;
    @XmlAttribute(name = "RefundReissueIndicator")
    protected String refundReissueIndicator;
    @XmlAttribute(name = "Commissionable")
    protected Boolean commissionable;
    @XmlAttribute(name = "MileageIndicator")
    protected Boolean mileageIndicator;
    @XmlAttribute(name = "Location")
    protected String location;
    @XmlAttribute(name = "Date")
    protected XMLGregorianCalendar date;
    @XmlAttribute(name = "Booking")
    protected TypeBooking booking;
    @XmlAttribute(name = "DisplayCategory")
    protected TypeDisplayCategory displayCategory;
    @XmlAttribute(name = "Reusable")
    protected Boolean reusable;

    /**
     * Obtiene el valor de la propiedad fulfillmentType.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFulfillmentType() {
        return fulfillmentType;
    }

    /**
     * Define el valor de la propiedad fulfillmentType.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFulfillmentType(Integer value) {
        this.fulfillmentType = value;
    }

    /**
     * Obtiene el valor de la propiedad fulfillmentTypeDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFulfillmentTypeDescription() {
        return fulfillmentTypeDescription;
    }

    /**
     * Define el valor de la propiedad fulfillmentTypeDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFulfillmentTypeDescription(String value) {
        this.fulfillmentTypeDescription = value;
    }

    /**
     * Obtiene el valor de la propiedad associatedItem.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssociatedItem() {
        return associatedItem;
    }

    /**
     * Define el valor de la propiedad associatedItem.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssociatedItem(String value) {
        this.associatedItem = value;
    }

    /**
     * Obtiene el valor de la propiedad availabilityChargeIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAvailabilityChargeIndicator() {
        return availabilityChargeIndicator;
    }

    /**
     * Define el valor de la propiedad availabilityChargeIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAvailabilityChargeIndicator(String value) {
        this.availabilityChargeIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad refundReissueIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefundReissueIndicator() {
        return refundReissueIndicator;
    }

    /**
     * Define el valor de la propiedad refundReissueIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefundReissueIndicator(String value) {
        this.refundReissueIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad commissionable.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCommissionable() {
        return commissionable;
    }

    /**
     * Define el valor de la propiedad commissionable.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCommissionable(Boolean value) {
        this.commissionable = value;
    }

    /**
     * Obtiene el valor de la propiedad mileageIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMileageIndicator() {
        return mileageIndicator;
    }

    /**
     * Define el valor de la propiedad mileageIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMileageIndicator(Boolean value) {
        this.mileageIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad location.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocation() {
        return location;
    }

    /**
     * Define el valor de la propiedad location.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocation(String value) {
        this.location = value;
    }

    /**
     * Obtiene el valor de la propiedad date.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDate() {
        return date;
    }

    /**
     * Define el valor de la propiedad date.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDate(XMLGregorianCalendar value) {
        this.date = value;
    }

    /**
     * Obtiene el valor de la propiedad booking.
     * 
     * @return
     *     possible object is
     *     {@link TypeBooking }
     *     
     */
    public TypeBooking getBooking() {
        return booking;
    }

    /**
     * Define el valor de la propiedad booking.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeBooking }
     *     
     */
    public void setBooking(TypeBooking value) {
        this.booking = value;
    }

    /**
     * Obtiene el valor de la propiedad displayCategory.
     * 
     * @return
     *     possible object is
     *     {@link TypeDisplayCategory }
     *     
     */
    public TypeDisplayCategory getDisplayCategory() {
        return displayCategory;
    }

    /**
     * Define el valor de la propiedad displayCategory.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeDisplayCategory }
     *     
     */
    public void setDisplayCategory(TypeDisplayCategory value) {
        this.displayCategory = value;
    }

    /**
     * Obtiene el valor de la propiedad reusable.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReusable() {
        return reusable;
    }

    /**
     * Define el valor de la propiedad reusable.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReusable(Boolean value) {
        this.reusable = value;
    }

}
