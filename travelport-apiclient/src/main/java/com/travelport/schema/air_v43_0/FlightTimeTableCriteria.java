
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}GeneralTimeTable"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}SpecificTimeTable"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "generalTimeTable",
    "specificTimeTable"
})
@XmlRootElement(name = "FlightTimeTableCriteria")
public class FlightTimeTableCriteria {

    @XmlElement(name = "GeneralTimeTable")
    protected GeneralTimeTable generalTimeTable;
    @XmlElement(name = "SpecificTimeTable")
    protected SpecificTimeTable specificTimeTable;

    /**
     * Obtiene el valor de la propiedad generalTimeTable.
     * 
     * @return
     *     possible object is
     *     {@link GeneralTimeTable }
     *     
     */
    public GeneralTimeTable getGeneralTimeTable() {
        return generalTimeTable;
    }

    /**
     * Define el valor de la propiedad generalTimeTable.
     * 
     * @param value
     *     allowed object is
     *     {@link GeneralTimeTable }
     *     
     */
    public void setGeneralTimeTable(GeneralTimeTable value) {
        this.generalTimeTable = value;
    }

    /**
     * Obtiene el valor de la propiedad specificTimeTable.
     * 
     * @return
     *     possible object is
     *     {@link SpecificTimeTable }
     *     
     */
    public SpecificTimeTable getSpecificTimeTable() {
        return specificTimeTable;
    }

    /**
     * Define el valor de la propiedad specificTimeTable.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecificTimeTable }
     *     
     */
    public void setSpecificTimeTable(SpecificTimeTable value) {
        this.specificTimeTable = value;
    }

}
