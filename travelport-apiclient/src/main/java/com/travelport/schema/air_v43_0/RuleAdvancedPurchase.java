
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="ReservationLatestPeriod" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="ReservationLatestUnit" type="{http://www.travelport.com/schema/air_v43_0}typeStayUnit" /&gt;
 *       &lt;attribute name="TicketingEarliestDate" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="TicketingLatestDate" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="MoreRulesPresent" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "RuleAdvancedPurchase")
public class RuleAdvancedPurchase {

    @XmlAttribute(name = "ReservationLatestPeriod")
    protected String reservationLatestPeriod;
    @XmlAttribute(name = "ReservationLatestUnit")
    protected TypeStayUnit reservationLatestUnit;
    @XmlAttribute(name = "TicketingEarliestDate")
    protected String ticketingEarliestDate;
    @XmlAttribute(name = "TicketingLatestDate")
    protected String ticketingLatestDate;
    @XmlAttribute(name = "MoreRulesPresent")
    protected Boolean moreRulesPresent;

    /**
     * Obtiene el valor de la propiedad reservationLatestPeriod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReservationLatestPeriod() {
        return reservationLatestPeriod;
    }

    /**
     * Define el valor de la propiedad reservationLatestPeriod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReservationLatestPeriod(String value) {
        this.reservationLatestPeriod = value;
    }

    /**
     * Obtiene el valor de la propiedad reservationLatestUnit.
     * 
     * @return
     *     possible object is
     *     {@link TypeStayUnit }
     *     
     */
    public TypeStayUnit getReservationLatestUnit() {
        return reservationLatestUnit;
    }

    /**
     * Define el valor de la propiedad reservationLatestUnit.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeStayUnit }
     *     
     */
    public void setReservationLatestUnit(TypeStayUnit value) {
        this.reservationLatestUnit = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketingEarliestDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketingEarliestDate() {
        return ticketingEarliestDate;
    }

    /**
     * Define el valor de la propiedad ticketingEarliestDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketingEarliestDate(String value) {
        this.ticketingEarliestDate = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketingLatestDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketingLatestDate() {
        return ticketingLatestDate;
    }

    /**
     * Define el valor de la propiedad ticketingLatestDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketingLatestDate(String value) {
        this.ticketingLatestDate = value;
    }

    /**
     * Obtiene el valor de la propiedad moreRulesPresent.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMoreRulesPresent() {
        return moreRulesPresent;
    }

    /**
     * Define el valor de la propiedad moreRulesPresent.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMoreRulesPresent(Boolean value) {
        this.moreRulesPresent = value;
    }

}
