
package com.travelport.schema.air_v43_0;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.FormOfPayment;
import com.travelport.schema.common_v43_0.SearchPassenger;


/**
 * 
 *             Base Low Fare Search Request
 *         
 * 
 * <p>Clase Java para BaseLowFareSearchReq complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BaseLowFareSearchReq"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/air_v43_0}BaseAirSearchReq"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}SearchPassenger" maxOccurs="18"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}AirPricingModifiers" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}Enumeration" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}AirExchangeModifiers" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FlexExploreModifiers" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}PCC" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FareRulesFilterCategory" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}FormOfPayment" maxOccurs="99" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="EnablePointToPointSearch" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="EnablePointToPointAlternates" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="MaxNumberOfExpertSolutions" type="{http://www.w3.org/2001/XMLSchema}integer" default="0" /&gt;
 *       &lt;attribute name="SolutionResult" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="PreferCompleteItinerary" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" /&gt;
 *       &lt;attribute name="MetaOptionIdentifier"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="1"/&gt;
 *             &lt;maxLength value="2"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="ReturnUpsellFare" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="IncludeFareInfoMessages" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="ReturnBrandedFares" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" /&gt;
 *       &lt;attribute name="MultiGDSSearch" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="ReturnMM" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="CheckOBFees" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="NSCC"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="1"/&gt;
 *             &lt;maxLength value="3"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaseLowFareSearchReq", propOrder = {
    "searchPassenger",
    "airPricingModifiers",
    "enumeration",
    "airExchangeModifiers",
    "flexExploreModifiers",
    "pcc",
    "fareRulesFilterCategory",
    "formOfPayment"
})
@XmlSeeAlso({
    LowFareSearchAsynchReq.class,
    LowFareSearchReq.class
})
public class BaseLowFareSearchReq
    extends BaseAirSearchReq
{

    @XmlElement(name = "SearchPassenger", namespace = "http://www.travelport.com/schema/common_v43_0", required = true)
    protected List<SearchPassenger> searchPassenger;
    @XmlElement(name = "AirPricingModifiers")
    protected AirPricingModifiers airPricingModifiers;
    @XmlElement(name = "Enumeration")
    protected Enumeration enumeration;
    @XmlElement(name = "AirExchangeModifiers")
    protected AirExchangeModifiers airExchangeModifiers;
    @XmlElement(name = "FlexExploreModifiers")
    protected FlexExploreModifiers flexExploreModifiers;
    @XmlElement(name = "PCC")
    protected PCC pcc;
    @XmlElement(name = "FareRulesFilterCategory")
    protected FareRulesFilterCategory fareRulesFilterCategory;
    @XmlElement(name = "FormOfPayment", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected List<FormOfPayment> formOfPayment;
    @XmlAttribute(name = "EnablePointToPointSearch")
    protected Boolean enablePointToPointSearch;
    @XmlAttribute(name = "EnablePointToPointAlternates")
    protected Boolean enablePointToPointAlternates;
    @XmlAttribute(name = "MaxNumberOfExpertSolutions")
    protected BigInteger maxNumberOfExpertSolutions;
    @XmlAttribute(name = "SolutionResult")
    protected Boolean solutionResult;
    @XmlAttribute(name = "PreferCompleteItinerary")
    protected Boolean preferCompleteItinerary;
    @XmlAttribute(name = "MetaOptionIdentifier")
    protected String metaOptionIdentifier;
    @XmlAttribute(name = "ReturnUpsellFare")
    protected Boolean returnUpsellFare;
    @XmlAttribute(name = "IncludeFareInfoMessages")
    protected Boolean includeFareInfoMessages;
    @XmlAttribute(name = "ReturnBrandedFares")
    protected Boolean returnBrandedFares;
    @XmlAttribute(name = "MultiGDSSearch")
    protected Boolean multiGDSSearch;
    @XmlAttribute(name = "ReturnMM")
    protected Boolean returnMM;
    @XmlAttribute(name = "CheckOBFees")
    protected String checkOBFees;
    @XmlAttribute(name = "NSCC")
    protected String nscc;

    /**
     * Provider: 1G,1V,1P,1J,ACH-Maxinumber of passenger increased in to 18 to support 9 INF passenger along with 9 ADT,CHD,INS 					passenger Gets the value of the searchPassenger property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the searchPassenger property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchPassenger().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchPassenger }
     * 
     * 
     */
    public List<SearchPassenger> getSearchPassenger() {
        if (searchPassenger == null) {
            searchPassenger = new ArrayList<SearchPassenger>();
        }
        return this.searchPassenger;
    }

    /**
     * Provider: 1G,1V,1P,1J,ACH.
     * 
     * @return
     *     possible object is
     *     {@link AirPricingModifiers }
     *     
     */
    public AirPricingModifiers getAirPricingModifiers() {
        return airPricingModifiers;
    }

    /**
     * Define el valor de la propiedad airPricingModifiers.
     * 
     * @param value
     *     allowed object is
     *     {@link AirPricingModifiers }
     *     
     */
    public void setAirPricingModifiers(AirPricingModifiers value) {
        this.airPricingModifiers = value;
    }

    /**
     * Provider: 1G,1V,1P,1J,ACH.
     * 
     * @return
     *     possible object is
     *     {@link Enumeration }
     *     
     */
    public Enumeration getEnumeration() {
        return enumeration;
    }

    /**
     * Define el valor de la propiedad enumeration.
     * 
     * @param value
     *     allowed object is
     *     {@link Enumeration }
     *     
     */
    public void setEnumeration(Enumeration value) {
        this.enumeration = value;
    }

    /**
     * Provider: ACH.
     * 
     * @return
     *     possible object is
     *     {@link AirExchangeModifiers }
     *     
     */
    public AirExchangeModifiers getAirExchangeModifiers() {
        return airExchangeModifiers;
    }

    /**
     * Define el valor de la propiedad airExchangeModifiers.
     * 
     * @param value
     *     allowed object is
     *     {@link AirExchangeModifiers }
     *     
     */
    public void setAirExchangeModifiers(AirExchangeModifiers value) {
        this.airExchangeModifiers = value;
    }

    /**
     * This is the container for a set of modifiers which allow the user to perform a special kind of low fare search, depicted as flex explore, based on different parameters like Area, Zone, Country, State, Specific locations, Distance around the actual destination of the itinerary. Applicable for providers 1G,1V,1P.
     * 
     * @return
     *     possible object is
     *     {@link FlexExploreModifiers }
     *     
     */
    public FlexExploreModifiers getFlexExploreModifiers() {
        return flexExploreModifiers;
    }

    /**
     * Define el valor de la propiedad flexExploreModifiers.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexExploreModifiers }
     *     
     */
    public void setFlexExploreModifiers(FlexExploreModifiers value) {
        this.flexExploreModifiers = value;
    }

    /**
     * Obtiene el valor de la propiedad pcc.
     * 
     * @return
     *     possible object is
     *     {@link PCC }
     *     
     */
    public PCC getPCC() {
        return pcc;
    }

    /**
     * Define el valor de la propiedad pcc.
     * 
     * @param value
     *     allowed object is
     *     {@link PCC }
     *     
     */
    public void setPCC(PCC value) {
        this.pcc = value;
    }

    /**
     * Obtiene el valor de la propiedad fareRulesFilterCategory.
     * 
     * @return
     *     possible object is
     *     {@link FareRulesFilterCategory }
     *     
     */
    public FareRulesFilterCategory getFareRulesFilterCategory() {
        return fareRulesFilterCategory;
    }

    /**
     * Define el valor de la propiedad fareRulesFilterCategory.
     * 
     * @param value
     *     allowed object is
     *     {@link FareRulesFilterCategory }
     *     
     */
    public void setFareRulesFilterCategory(FareRulesFilterCategory value) {
        this.fareRulesFilterCategory = value;
    }

    /**
     * Provider: 1P,1J Gets the value of the formOfPayment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the formOfPayment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFormOfPayment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FormOfPayment }
     * 
     * 
     */
    public List<FormOfPayment> getFormOfPayment() {
        if (formOfPayment == null) {
            formOfPayment = new ArrayList<FormOfPayment>();
        }
        return this.formOfPayment;
    }

    /**
     * Obtiene el valor de la propiedad enablePointToPointSearch.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isEnablePointToPointSearch() {
        if (enablePointToPointSearch == null) {
            return false;
        } else {
            return enablePointToPointSearch;
        }
    }

    /**
     * Define el valor de la propiedad enablePointToPointSearch.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnablePointToPointSearch(Boolean value) {
        this.enablePointToPointSearch = value;
    }

    /**
     * Obtiene el valor de la propiedad enablePointToPointAlternates.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isEnablePointToPointAlternates() {
        if (enablePointToPointAlternates == null) {
            return false;
        } else {
            return enablePointToPointAlternates;
        }
    }

    /**
     * Define el valor de la propiedad enablePointToPointAlternates.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnablePointToPointAlternates(Boolean value) {
        this.enablePointToPointAlternates = value;
    }

    /**
     * Obtiene el valor de la propiedad maxNumberOfExpertSolutions.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxNumberOfExpertSolutions() {
        if (maxNumberOfExpertSolutions == null) {
            return new BigInteger("0");
        } else {
            return maxNumberOfExpertSolutions;
        }
    }

    /**
     * Define el valor de la propiedad maxNumberOfExpertSolutions.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxNumberOfExpertSolutions(BigInteger value) {
        this.maxNumberOfExpertSolutions = value;
    }

    /**
     * Obtiene el valor de la propiedad solutionResult.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isSolutionResult() {
        if (solutionResult == null) {
            return false;
        } else {
            return solutionResult;
        }
    }

    /**
     * Define el valor de la propiedad solutionResult.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSolutionResult(Boolean value) {
        this.solutionResult = value;
    }

    /**
     * Obtiene el valor de la propiedad preferCompleteItinerary.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isPreferCompleteItinerary() {
        if (preferCompleteItinerary == null) {
            return true;
        } else {
            return preferCompleteItinerary;
        }
    }

    /**
     * Define el valor de la propiedad preferCompleteItinerary.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPreferCompleteItinerary(Boolean value) {
        this.preferCompleteItinerary = value;
    }

    /**
     * Obtiene el valor de la propiedad metaOptionIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMetaOptionIdentifier() {
        return metaOptionIdentifier;
    }

    /**
     * Define el valor de la propiedad metaOptionIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMetaOptionIdentifier(String value) {
        this.metaOptionIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad returnUpsellFare.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isReturnUpsellFare() {
        if (returnUpsellFare == null) {
            return false;
        } else {
            return returnUpsellFare;
        }
    }

    /**
     * Define el valor de la propiedad returnUpsellFare.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReturnUpsellFare(Boolean value) {
        this.returnUpsellFare = value;
    }

    /**
     * Obtiene el valor de la propiedad includeFareInfoMessages.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIncludeFareInfoMessages() {
        if (includeFareInfoMessages == null) {
            return false;
        } else {
            return includeFareInfoMessages;
        }
    }

    /**
     * Define el valor de la propiedad includeFareInfoMessages.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeFareInfoMessages(Boolean value) {
        this.includeFareInfoMessages = value;
    }

    /**
     * Obtiene el valor de la propiedad returnBrandedFares.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isReturnBrandedFares() {
        if (returnBrandedFares == null) {
            return true;
        } else {
            return returnBrandedFares;
        }
    }

    /**
     * Define el valor de la propiedad returnBrandedFares.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReturnBrandedFares(Boolean value) {
        this.returnBrandedFares = value;
    }

    /**
     * Obtiene el valor de la propiedad multiGDSSearch.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isMultiGDSSearch() {
        if (multiGDSSearch == null) {
            return false;
        } else {
            return multiGDSSearch;
        }
    }

    /**
     * Define el valor de la propiedad multiGDSSearch.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMultiGDSSearch(Boolean value) {
        this.multiGDSSearch = value;
    }

    /**
     * Obtiene el valor de la propiedad returnMM.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isReturnMM() {
        if (returnMM == null) {
            return false;
        } else {
            return returnMM;
        }
    }

    /**
     * Define el valor de la propiedad returnMM.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReturnMM(Boolean value) {
        this.returnMM = value;
    }

    /**
     * Obtiene el valor de la propiedad checkOBFees.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckOBFees() {
        return checkOBFees;
    }

    /**
     * Define el valor de la propiedad checkOBFees.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckOBFees(String value) {
        this.checkOBFees = value;
    }

    /**
     * Obtiene el valor de la propiedad nscc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNSCC() {
        return nscc;
    }

    /**
     * Define el valor de la propiedad nscc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNSCC(String value) {
        this.nscc = value;
    }

}
