
package com.travelport.schema.air_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.OverridePCC;
import com.travelport.schema.common_v43_0.PointOfSale;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}OverridePCC" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}PointOfSale" maxOccurs="5" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}TicketAgency" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "overridePCC",
    "pointOfSale",
    "ticketAgency"
})
@XmlRootElement(name = "PCC")
public class PCC {

    @XmlElement(name = "OverridePCC", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected OverridePCC overridePCC;
    @XmlElement(name = "PointOfSale", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected List<PointOfSale> pointOfSale;
    @XmlElement(name = "TicketAgency")
    protected TicketAgency ticketAgency;

    /**
     * Obtiene el valor de la propiedad overridePCC.
     * 
     * @return
     *     possible object is
     *     {@link OverridePCC }
     *     
     */
    public OverridePCC getOverridePCC() {
        return overridePCC;
    }

    /**
     * Define el valor de la propiedad overridePCC.
     * 
     * @param value
     *     allowed object is
     *     {@link OverridePCC }
     *     
     */
    public void setOverridePCC(OverridePCC value) {
        this.overridePCC = value;
    }

    /**
     * Gets the value of the pointOfSale property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pointOfSale property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPointOfSale().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PointOfSale }
     * 
     * 
     */
    public List<PointOfSale> getPointOfSale() {
        if (pointOfSale == null) {
            pointOfSale = new ArrayList<PointOfSale>();
        }
        return this.pointOfSale;
    }

    /**
     * Obtiene el valor de la propiedad ticketAgency.
     * 
     * @return
     *     possible object is
     *     {@link TicketAgency }
     *     
     */
    public TicketAgency getTicketAgency() {
        return ticketAgency;
    }

    /**
     * Define el valor de la propiedad ticketAgency.
     * 
     * @param value
     *     allowed object is
     *     {@link TicketAgency }
     *     
     */
    public void setTicketAgency(TicketAgency value) {
        this.ticketAgency = value;
    }

}
