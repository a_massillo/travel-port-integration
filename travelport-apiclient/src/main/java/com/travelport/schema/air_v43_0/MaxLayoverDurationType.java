
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * User can specify its attribute's value in Minutes. Maximum size of each attribute is 4.
 * 
 * <p>Clase Java para MaxLayoverDurationType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="MaxLayoverDurationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="Domestic" type="{http://www.travelport.com/schema/air_v43_0}MaxLayoverDurationRangeType" /&gt;
 *       &lt;attribute name="Gateway" type="{http://www.travelport.com/schema/air_v43_0}MaxLayoverDurationRangeType" /&gt;
 *       &lt;attribute name="International" type="{http://www.travelport.com/schema/air_v43_0}MaxLayoverDurationRangeType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MaxLayoverDurationType")
public class MaxLayoverDurationType {

    @XmlAttribute(name = "Domestic")
    protected Integer domestic;
    @XmlAttribute(name = "Gateway")
    protected Integer gateway;
    @XmlAttribute(name = "International")
    protected Integer international;

    /**
     * Obtiene el valor de la propiedad domestic.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDomestic() {
        return domestic;
    }

    /**
     * Define el valor de la propiedad domestic.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDomestic(Integer value) {
        this.domestic = value;
    }

    /**
     * Obtiene el valor de la propiedad gateway.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGateway() {
        return gateway;
    }

    /**
     * Define el valor de la propiedad gateway.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGateway(Integer value) {
        this.gateway = value;
    }

    /**
     * Obtiene el valor de la propiedad international.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInternational() {
        return international;
    }

    /**
     * Define el valor de la propiedad international.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInternational(Integer value) {
        this.international = value;
    }

}
