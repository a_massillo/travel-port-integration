
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseSearchReq;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseSearchReq"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FlightTimeTableCriteria"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "flightTimeTableCriteria"
})
@XmlRootElement(name = "FlightTimeTableReq")
public class FlightTimeTableReq
    extends BaseSearchReq
{

    @XmlElement(name = "FlightTimeTableCriteria", required = true)
    protected FlightTimeTableCriteria flightTimeTableCriteria;

    /**
     * Provider: 1G,1V.
     * 
     * @return
     *     possible object is
     *     {@link FlightTimeTableCriteria }
     *     
     */
    public FlightTimeTableCriteria getFlightTimeTableCriteria() {
        return flightTimeTableCriteria;
    }

    /**
     * Define el valor de la propiedad flightTimeTableCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link FlightTimeTableCriteria }
     *     
     */
    public void setFlightTimeTableCriteria(FlightTimeTableCriteria value) {
        this.flightTimeTableCriteria = value;
    }

}
