
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/air_v43_0}AirSearchRsp"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}BrandList" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="CurrencyType" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeCurrency" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "brandList"
})
@XmlRootElement(name = "LowFareSearchRsp")
public class LowFareSearchRsp
    extends AirSearchRsp
{

    @XmlElement(name = "BrandList")
    protected BrandList brandList;
    @XmlAttribute(name = "CurrencyType", required = true)
    protected String currencyType;

    /**
     * Obtiene el valor de la propiedad brandList.
     * 
     * @return
     *     possible object is
     *     {@link BrandList }
     *     
     */
    public BrandList getBrandList() {
        return brandList;
    }

    /**
     * Define el valor de la propiedad brandList.
     * 
     * @param value
     *     allowed object is
     *     {@link BrandList }
     *     
     */
    public void setBrandList(BrandList value) {
        this.brandList = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyType() {
        return currencyType;
    }

    /**
     * Define el valor de la propiedad currencyType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyType(String value) {
        this.currencyType = value;
    }

}
