
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="Type" use="required" type="{http://www.travelport.com/schema/air_v43_0}typeCarrierCode" /&gt;
 *       &lt;attribute name="SecondaryCarrier" type="{http://www.travelport.com/schema/common_v43_0}typeCarrier" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "IncludeAddlBookingCodeInfo")
public class IncludeAddlBookingCodeInfo {

    @XmlAttribute(name = "Type", required = true)
    protected TypeCarrierCode type;
    @XmlAttribute(name = "SecondaryCarrier")
    protected String secondaryCarrier;

    /**
     * Obtiene el valor de la propiedad type.
     * 
     * @return
     *     possible object is
     *     {@link TypeCarrierCode }
     *     
     */
    public TypeCarrierCode getType() {
        return type;
    }

    /**
     * Define el valor de la propiedad type.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeCarrierCode }
     *     
     */
    public void setType(TypeCarrierCode value) {
        this.type = value;
    }

    /**
     * Obtiene el valor de la propiedad secondaryCarrier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondaryCarrier() {
        return secondaryCarrier;
    }

    /**
     * Define el valor de la propiedad secondaryCarrier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondaryCarrier(String value) {
        this.secondaryCarrier = value;
    }

}
