
package com.travelport.schema.air_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}PassengerReceiptOverride" minOccurs="0"/&gt;
 *         &lt;element name="OverrideOption" type="{http://www.travelport.com/schema/air_v43_0}typeOverrideOption" maxOccurs="999" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="SuppressItineraryRemarks" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="GenerateItinNumbers" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "passengerReceiptOverride",
    "overrideOption"
})
@XmlRootElement(name = "DocumentOptions")
public class DocumentOptions {

    @XmlElement(name = "PassengerReceiptOverride")
    protected PassengerReceiptOverride passengerReceiptOverride;
    @XmlElement(name = "OverrideOption")
    protected List<String> overrideOption;
    @XmlAttribute(name = "SuppressItineraryRemarks")
    protected Boolean suppressItineraryRemarks;
    @XmlAttribute(name = "GenerateItinNumbers")
    protected Boolean generateItinNumbers;

    /**
     * Obtiene el valor de la propiedad passengerReceiptOverride.
     * 
     * @return
     *     possible object is
     *     {@link PassengerReceiptOverride }
     *     
     */
    public PassengerReceiptOverride getPassengerReceiptOverride() {
        return passengerReceiptOverride;
    }

    /**
     * Define el valor de la propiedad passengerReceiptOverride.
     * 
     * @param value
     *     allowed object is
     *     {@link PassengerReceiptOverride }
     *     
     */
    public void setPassengerReceiptOverride(PassengerReceiptOverride value) {
        this.passengerReceiptOverride = value;
    }

    /**
     * Gets the value of the overrideOption property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the overrideOption property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOverrideOption().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getOverrideOption() {
        if (overrideOption == null) {
            overrideOption = new ArrayList<String>();
        }
        return this.overrideOption;
    }

    /**
     * Obtiene el valor de la propiedad suppressItineraryRemarks.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSuppressItineraryRemarks() {
        return suppressItineraryRemarks;
    }

    /**
     * Define el valor de la propiedad suppressItineraryRemarks.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSuppressItineraryRemarks(Boolean value) {
        this.suppressItineraryRemarks = value;
    }

    /**
     * Obtiene el valor de la propiedad generateItinNumbers.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isGenerateItinNumbers() {
        return generateItinNumbers;
    }

    /**
     * Define el valor de la propiedad generateItinNumbers.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGenerateItinNumbers(Boolean value) {
        this.generateItinNumbers = value;
    }

}
