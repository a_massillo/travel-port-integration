
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para typeApplicableSegment complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="typeApplicableSegment"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="Key" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="AirItineraryDetailsRef" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="BookingCounts" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "typeApplicableSegment")
public class TypeApplicableSegment {

    @XmlAttribute(name = "Key", required = true)
    protected String key;
    @XmlAttribute(name = "AirItineraryDetailsRef")
    protected String airItineraryDetailsRef;
    @XmlAttribute(name = "BookingCounts")
    protected String bookingCounts;

    /**
     * Obtiene el valor de la propiedad key.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Define el valor de la propiedad key.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Obtiene el valor de la propiedad airItineraryDetailsRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirItineraryDetailsRef() {
        return airItineraryDetailsRef;
    }

    /**
     * Define el valor de la propiedad airItineraryDetailsRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirItineraryDetailsRef(String value) {
        this.airItineraryDetailsRef = value;
    }

    /**
     * Obtiene el valor de la propiedad bookingCounts.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookingCounts() {
        return bookingCounts;
    }

    /**
     * Define el valor de la propiedad bookingCounts.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookingCounts(String value) {
        this.bookingCounts = value;
    }

}
