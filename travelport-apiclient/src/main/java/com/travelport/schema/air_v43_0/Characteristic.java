
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="Value" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="Position" type="{http://www.travelport.com/schema/air_v43_0}typePosition" /&gt;
 *       &lt;attribute name="RowLocation" type="{http://www.travelport.com/schema/air_v43_0}typeRowLocation" /&gt;
 *       &lt;attribute name="PADISCode" type="{http://www.travelport.com/schema/common_v43_0}StringLength1to99" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "Characteristic")
public class Characteristic {

    @XmlAttribute(name = "Value", required = true)
    protected String value;
    @XmlAttribute(name = "Position")
    protected TypePosition position;
    @XmlAttribute(name = "RowLocation")
    protected TypeRowLocation rowLocation;
    @XmlAttribute(name = "PADISCode")
    protected String padisCode;

    /**
     * Obtiene el valor de la propiedad value.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Obtiene el valor de la propiedad position.
     * 
     * @return
     *     possible object is
     *     {@link TypePosition }
     *     
     */
    public TypePosition getPosition() {
        return position;
    }

    /**
     * Define el valor de la propiedad position.
     * 
     * @param value
     *     allowed object is
     *     {@link TypePosition }
     *     
     */
    public void setPosition(TypePosition value) {
        this.position = value;
    }

    /**
     * Obtiene el valor de la propiedad rowLocation.
     * 
     * @return
     *     possible object is
     *     {@link TypeRowLocation }
     *     
     */
    public TypeRowLocation getRowLocation() {
        return rowLocation;
    }

    /**
     * Define el valor de la propiedad rowLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeRowLocation }
     *     
     */
    public void setRowLocation(TypeRowLocation value) {
        this.rowLocation = value;
    }

    /**
     * Obtiene el valor de la propiedad padisCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPADISCode() {
        return padisCode;
    }

    /**
     * Define el valor de la propiedad padisCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPADISCode(String value) {
        this.padisCode = value;
    }

}
