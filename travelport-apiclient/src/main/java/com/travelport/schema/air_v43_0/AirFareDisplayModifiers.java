
package com.travelport.schema.air_v43_0;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.travelport.schema.common_v43_0.CabinClass;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TripType" type="{http://www.travelport.com/schema/air_v43_0}typeFareTripType" maxOccurs="3" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}CabinClass" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}PenaltyFareInformation" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="MaxResponses" type="{http://www.w3.org/2001/XMLSchema}integer" default="200" /&gt;
 *       &lt;attribute name="DepartureDate" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *       &lt;attribute name="TicketingDate" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *       &lt;attribute name="ReturnDate" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *       &lt;attribute name="BaseFareOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="UnrestrictedFaresOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="FaresIndicator" type="{http://www.travelport.com/schema/air_v43_0}typeFaresIndicator" /&gt;
 *       &lt;attribute name="CurrencyType" type="{http://www.travelport.com/schema/common_v43_0}typeCurrency" /&gt;
 *       &lt;attribute name="IncludeTaxes" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="IncludeEstimatedTaxes" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="IncludeSurcharges" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="GlobalIndicator" type="{http://www.travelport.com/schema/air_v43_0}typeATPCOGlobalIndicator" /&gt;
 *       &lt;attribute name="ProhibitMinStayFares" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="ProhibitMaxStayFares" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="ProhibitAdvancePurchaseFares" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="ProhibitNonRefundableFares" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="ValidatedFaresOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="ProhibitTravelRestrictedFares" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" /&gt;
 *       &lt;attribute name="FiledCurrency" type="{http://www.travelport.com/schema/common_v43_0}typeCurrency" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "tripType",
    "cabinClass",
    "penaltyFareInformation"
})
@XmlRootElement(name = "AirFareDisplayModifiers")
public class AirFareDisplayModifiers {

    @XmlElement(name = "TripType")
    @XmlSchemaType(name = "string")
    protected List<TypeFareTripType> tripType;
    @XmlElement(name = "CabinClass", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected CabinClass cabinClass;
    @XmlElement(name = "PenaltyFareInformation")
    protected PenaltyFareInformation penaltyFareInformation;
    @XmlAttribute(name = "MaxResponses")
    protected BigInteger maxResponses;
    @XmlAttribute(name = "DepartureDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar departureDate;
    @XmlAttribute(name = "TicketingDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar ticketingDate;
    @XmlAttribute(name = "ReturnDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar returnDate;
    @XmlAttribute(name = "BaseFareOnly")
    protected Boolean baseFareOnly;
    @XmlAttribute(name = "UnrestrictedFaresOnly")
    protected Boolean unrestrictedFaresOnly;
    @XmlAttribute(name = "FaresIndicator")
    protected TypeFaresIndicator faresIndicator;
    @XmlAttribute(name = "CurrencyType")
    protected String currencyType;
    @XmlAttribute(name = "IncludeTaxes")
    protected Boolean includeTaxes;
    @XmlAttribute(name = "IncludeEstimatedTaxes")
    protected Boolean includeEstimatedTaxes;
    @XmlAttribute(name = "IncludeSurcharges")
    protected Boolean includeSurcharges;
    @XmlAttribute(name = "GlobalIndicator")
    protected TypeATPCOGlobalIndicator globalIndicator;
    @XmlAttribute(name = "ProhibitMinStayFares")
    protected Boolean prohibitMinStayFares;
    @XmlAttribute(name = "ProhibitMaxStayFares")
    protected Boolean prohibitMaxStayFares;
    @XmlAttribute(name = "ProhibitAdvancePurchaseFares")
    protected Boolean prohibitAdvancePurchaseFares;
    @XmlAttribute(name = "ProhibitNonRefundableFares")
    protected Boolean prohibitNonRefundableFares;
    @XmlAttribute(name = "ValidatedFaresOnly")
    protected Boolean validatedFaresOnly;
    @XmlAttribute(name = "ProhibitTravelRestrictedFares")
    protected Boolean prohibitTravelRestrictedFares;
    @XmlAttribute(name = "FiledCurrency")
    protected String filedCurrency;

    /**
     * Gets the value of the tripType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tripType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTripType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TypeFareTripType }
     * 
     * 
     */
    public List<TypeFareTripType> getTripType() {
        if (tripType == null) {
            tripType = new ArrayList<TypeFareTripType>();
        }
        return this.tripType;
    }

    /**
     * Obtiene el valor de la propiedad cabinClass.
     * 
     * @return
     *     possible object is
     *     {@link CabinClass }
     *     
     */
    public CabinClass getCabinClass() {
        return cabinClass;
    }

    /**
     * Define el valor de la propiedad cabinClass.
     * 
     * @param value
     *     allowed object is
     *     {@link CabinClass }
     *     
     */
    public void setCabinClass(CabinClass value) {
        this.cabinClass = value;
    }

    /**
     * Request Fares with specific Penalty
     *                             Information.
     * 
     * @return
     *     possible object is
     *     {@link PenaltyFareInformation }
     *     
     */
    public PenaltyFareInformation getPenaltyFareInformation() {
        return penaltyFareInformation;
    }

    /**
     * Define el valor de la propiedad penaltyFareInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link PenaltyFareInformation }
     *     
     */
    public void setPenaltyFareInformation(PenaltyFareInformation value) {
        this.penaltyFareInformation = value;
    }

    /**
     * Obtiene el valor de la propiedad maxResponses.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxResponses() {
        if (maxResponses == null) {
            return new BigInteger("200");
        } else {
            return maxResponses;
        }
    }

    /**
     * Define el valor de la propiedad maxResponses.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxResponses(BigInteger value) {
        this.maxResponses = value;
    }

    /**
     * Obtiene el valor de la propiedad departureDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDepartureDate() {
        return departureDate;
    }

    /**
     * Define el valor de la propiedad departureDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDepartureDate(XMLGregorianCalendar value) {
        this.departureDate = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketingDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTicketingDate() {
        return ticketingDate;
    }

    /**
     * Define el valor de la propiedad ticketingDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTicketingDate(XMLGregorianCalendar value) {
        this.ticketingDate = value;
    }

    /**
     * Obtiene el valor de la propiedad returnDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReturnDate() {
        return returnDate;
    }

    /**
     * Define el valor de la propiedad returnDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReturnDate(XMLGregorianCalendar value) {
        this.returnDate = value;
    }

    /**
     * Obtiene el valor de la propiedad baseFareOnly.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isBaseFareOnly() {
        if (baseFareOnly == null) {
            return false;
        } else {
            return baseFareOnly;
        }
    }

    /**
     * Define el valor de la propiedad baseFareOnly.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBaseFareOnly(Boolean value) {
        this.baseFareOnly = value;
    }

    /**
     * Obtiene el valor de la propiedad unrestrictedFaresOnly.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isUnrestrictedFaresOnly() {
        if (unrestrictedFaresOnly == null) {
            return false;
        } else {
            return unrestrictedFaresOnly;
        }
    }

    /**
     * Define el valor de la propiedad unrestrictedFaresOnly.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUnrestrictedFaresOnly(Boolean value) {
        this.unrestrictedFaresOnly = value;
    }

    /**
     * Obtiene el valor de la propiedad faresIndicator.
     * 
     * @return
     *     possible object is
     *     {@link TypeFaresIndicator }
     *     
     */
    public TypeFaresIndicator getFaresIndicator() {
        return faresIndicator;
    }

    /**
     * Define el valor de la propiedad faresIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeFaresIndicator }
     *     
     */
    public void setFaresIndicator(TypeFaresIndicator value) {
        this.faresIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyType() {
        return currencyType;
    }

    /**
     * Define el valor de la propiedad currencyType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyType(String value) {
        this.currencyType = value;
    }

    /**
     * Obtiene el valor de la propiedad includeTaxes.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeTaxes() {
        return includeTaxes;
    }

    /**
     * Define el valor de la propiedad includeTaxes.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeTaxes(Boolean value) {
        this.includeTaxes = value;
    }

    /**
     * Obtiene el valor de la propiedad includeEstimatedTaxes.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeEstimatedTaxes() {
        return includeEstimatedTaxes;
    }

    /**
     * Define el valor de la propiedad includeEstimatedTaxes.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeEstimatedTaxes(Boolean value) {
        this.includeEstimatedTaxes = value;
    }

    /**
     * Obtiene el valor de la propiedad includeSurcharges.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeSurcharges() {
        return includeSurcharges;
    }

    /**
     * Define el valor de la propiedad includeSurcharges.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeSurcharges(Boolean value) {
        this.includeSurcharges = value;
    }

    /**
     * Obtiene el valor de la propiedad globalIndicator.
     * 
     * @return
     *     possible object is
     *     {@link TypeATPCOGlobalIndicator }
     *     
     */
    public TypeATPCOGlobalIndicator getGlobalIndicator() {
        return globalIndicator;
    }

    /**
     * Define el valor de la propiedad globalIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeATPCOGlobalIndicator }
     *     
     */
    public void setGlobalIndicator(TypeATPCOGlobalIndicator value) {
        this.globalIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad prohibitMinStayFares.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isProhibitMinStayFares() {
        if (prohibitMinStayFares == null) {
            return false;
        } else {
            return prohibitMinStayFares;
        }
    }

    /**
     * Define el valor de la propiedad prohibitMinStayFares.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProhibitMinStayFares(Boolean value) {
        this.prohibitMinStayFares = value;
    }

    /**
     * Obtiene el valor de la propiedad prohibitMaxStayFares.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isProhibitMaxStayFares() {
        if (prohibitMaxStayFares == null) {
            return false;
        } else {
            return prohibitMaxStayFares;
        }
    }

    /**
     * Define el valor de la propiedad prohibitMaxStayFares.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProhibitMaxStayFares(Boolean value) {
        this.prohibitMaxStayFares = value;
    }

    /**
     * Obtiene el valor de la propiedad prohibitAdvancePurchaseFares.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isProhibitAdvancePurchaseFares() {
        if (prohibitAdvancePurchaseFares == null) {
            return false;
        } else {
            return prohibitAdvancePurchaseFares;
        }
    }

    /**
     * Define el valor de la propiedad prohibitAdvancePurchaseFares.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProhibitAdvancePurchaseFares(Boolean value) {
        this.prohibitAdvancePurchaseFares = value;
    }

    /**
     * Obtiene el valor de la propiedad prohibitNonRefundableFares.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isProhibitNonRefundableFares() {
        if (prohibitNonRefundableFares == null) {
            return false;
        } else {
            return prohibitNonRefundableFares;
        }
    }

    /**
     * Define el valor de la propiedad prohibitNonRefundableFares.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProhibitNonRefundableFares(Boolean value) {
        this.prohibitNonRefundableFares = value;
    }

    /**
     * Obtiene el valor de la propiedad validatedFaresOnly.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isValidatedFaresOnly() {
        return validatedFaresOnly;
    }

    /**
     * Define el valor de la propiedad validatedFaresOnly.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setValidatedFaresOnly(Boolean value) {
        this.validatedFaresOnly = value;
    }

    /**
     * Obtiene el valor de la propiedad prohibitTravelRestrictedFares.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isProhibitTravelRestrictedFares() {
        if (prohibitTravelRestrictedFares == null) {
            return true;
        } else {
            return prohibitTravelRestrictedFares;
        }
    }

    /**
     * Define el valor de la propiedad prohibitTravelRestrictedFares.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProhibitTravelRestrictedFares(Boolean value) {
        this.prohibitTravelRestrictedFares = value;
    }

    /**
     * Obtiene el valor de la propiedad filedCurrency.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFiledCurrency() {
        return filedCurrency;
    }

    /**
     * Define el valor de la propiedad filedCurrency.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFiledCurrency(String value) {
        this.filedCurrency = value;
    }

}
