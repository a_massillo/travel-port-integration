
package com.travelport.schema.air_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}CO2Emission" maxOccurs="99" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="TotalValue" type="{http://www.w3.org/2001/XMLSchema}float" /&gt;
 *       &lt;attribute name="Unit" type="{http://www.travelport.com/schema/common_v43_0}StringLength1to64" /&gt;
 *       &lt;attribute name="Category" type="{http://www.travelport.com/schema/common_v43_0}StringLength1to64" /&gt;
 *       &lt;attribute name="Source" type="{http://www.travelport.com/schema/common_v43_0}StringLength1to64" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "co2Emission"
})
@XmlRootElement(name = "CO2Emissions")
public class CO2Emissions {

    @XmlElement(name = "CO2Emission")
    protected List<CO2Emission> co2Emission;
    @XmlAttribute(name = "TotalValue")
    protected Float totalValue;
    @XmlAttribute(name = "Unit")
    protected String unit;
    @XmlAttribute(name = "Category")
    protected String category;
    @XmlAttribute(name = "Source")
    protected String source;

    /**
     * Gets the value of the co2Emission property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the co2Emission property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCO2Emission().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CO2Emission }
     * 
     * 
     */
    public List<CO2Emission> getCO2Emission() {
        if (co2Emission == null) {
            co2Emission = new ArrayList<CO2Emission>();
        }
        return this.co2Emission;
    }

    /**
     * Obtiene el valor de la propiedad totalValue.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getTotalValue() {
        return totalValue;
    }

    /**
     * Define el valor de la propiedad totalValue.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setTotalValue(Float value) {
        this.totalValue = value;
    }

    /**
     * Obtiene el valor de la propiedad unit.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Define el valor de la propiedad unit.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnit(String value) {
        this.unit = value;
    }

    /**
     * Obtiene el valor de la propiedad category.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategory() {
        return category;
    }

    /**
     * Define el valor de la propiedad category.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategory(String value) {
        this.category = value;
    }

    /**
     * Obtiene el valor de la propiedad source.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSource() {
        return source;
    }

    /**
     * Define el valor de la propiedad source.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSource(String value) {
        this.source = value;
    }

}
