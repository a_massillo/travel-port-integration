
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="Type" type="{http://www.travelport.com/schema/air_v43_0}typeItinerary" /&gt;
 *       &lt;attribute name="Option" type="{http://www.travelport.com/schema/air_v43_0}typeItineraryOption" /&gt;
 *       &lt;attribute name="SeparateIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "Itinerary")
public class Itinerary {

    @XmlAttribute(name = "Type")
    protected TypeItinerary type;
    @XmlAttribute(name = "Option")
    protected TypeItineraryOption option;
    @XmlAttribute(name = "SeparateIndicator")
    protected Boolean separateIndicator;

    /**
     * Obtiene el valor de la propiedad type.
     * 
     * @return
     *     possible object is
     *     {@link TypeItinerary }
     *     
     */
    public TypeItinerary getType() {
        return type;
    }

    /**
     * Define el valor de la propiedad type.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeItinerary }
     *     
     */
    public void setType(TypeItinerary value) {
        this.type = value;
    }

    /**
     * Obtiene el valor de la propiedad option.
     * 
     * @return
     *     possible object is
     *     {@link TypeItineraryOption }
     *     
     */
    public TypeItineraryOption getOption() {
        return option;
    }

    /**
     * Define el valor de la propiedad option.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeItineraryOption }
     *     
     */
    public void setOption(TypeItineraryOption value) {
        this.option = value;
    }

    /**
     * Obtiene el valor de la propiedad separateIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSeparateIndicator() {
        return separateIndicator;
    }

    /**
     * Define el valor de la propiedad separateIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSeparateIndicator(Boolean value) {
        this.separateIndicator = value;
    }

}
