
package com.travelport.schema.air_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DaysOfWeekRestriction" maxOccurs="3" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attGroup ref="{http://www.travelport.com/schema/common_v43_0}attrDOW"/&gt;
 *                 &lt;attribute name="RestrictionExistsInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                 &lt;attribute name="Application" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="IncludeExcludeUseInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="RestrictionPassengerTypes" maxOccurs="999" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="MaxNbrTravelers" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="TotalNbrPTC" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "daysOfWeekRestriction",
    "restrictionPassengerTypes"
})
@XmlRootElement(name = "Restriction")
public class Restriction {

    @XmlElement(name = "DaysOfWeekRestriction")
    protected List<Restriction.DaysOfWeekRestriction> daysOfWeekRestriction;
    @XmlElement(name = "RestrictionPassengerTypes")
    protected List<Restriction.RestrictionPassengerTypes> restrictionPassengerTypes;

    /**
     * Gets the value of the daysOfWeekRestriction property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the daysOfWeekRestriction property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDaysOfWeekRestriction().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Restriction.DaysOfWeekRestriction }
     * 
     * 
     */
    public List<Restriction.DaysOfWeekRestriction> getDaysOfWeekRestriction() {
        if (daysOfWeekRestriction == null) {
            daysOfWeekRestriction = new ArrayList<Restriction.DaysOfWeekRestriction>();
        }
        return this.daysOfWeekRestriction;
    }

    /**
     * Gets the value of the restrictionPassengerTypes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the restrictionPassengerTypes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRestrictionPassengerTypes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Restriction.RestrictionPassengerTypes }
     * 
     * 
     */
    public List<Restriction.RestrictionPassengerTypes> getRestrictionPassengerTypes() {
        if (restrictionPassengerTypes == null) {
            restrictionPassengerTypes = new ArrayList<Restriction.RestrictionPassengerTypes>();
        }
        return this.restrictionPassengerTypes;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attGroup ref="{http://www.travelport.com/schema/common_v43_0}attrDOW"/&gt;
     *       &lt;attribute name="RestrictionExistsInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *       &lt;attribute name="Application" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="IncludeExcludeUseInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class DaysOfWeekRestriction {

        @XmlAttribute(name = "RestrictionExistsInd")
        protected Boolean restrictionExistsInd;
        @XmlAttribute(name = "Application")
        protected String application;
        @XmlAttribute(name = "IncludeExcludeUseInd")
        protected Boolean includeExcludeUseInd;
        @XmlAttribute(name = "Mon")
        protected Boolean mon;
        @XmlAttribute(name = "Tue")
        protected Boolean tue;
        @XmlAttribute(name = "Wed")
        protected Boolean wed;
        @XmlAttribute(name = "Thu")
        protected Boolean thu;
        @XmlAttribute(name = "Fri")
        protected Boolean fri;
        @XmlAttribute(name = "Sat")
        protected Boolean sat;
        @XmlAttribute(name = "Sun")
        protected Boolean sun;

        /**
         * Obtiene el valor de la propiedad restrictionExistsInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isRestrictionExistsInd() {
            return restrictionExistsInd;
        }

        /**
         * Define el valor de la propiedad restrictionExistsInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setRestrictionExistsInd(Boolean value) {
            this.restrictionExistsInd = value;
        }

        /**
         * Obtiene el valor de la propiedad application.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getApplication() {
            return application;
        }

        /**
         * Define el valor de la propiedad application.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setApplication(String value) {
            this.application = value;
        }

        /**
         * Obtiene el valor de la propiedad includeExcludeUseInd.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isIncludeExcludeUseInd() {
            return includeExcludeUseInd;
        }

        /**
         * Define el valor de la propiedad includeExcludeUseInd.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setIncludeExcludeUseInd(Boolean value) {
            this.includeExcludeUseInd = value;
        }

        /**
         * Obtiene el valor de la propiedad mon.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isMon() {
            return mon;
        }

        /**
         * Define el valor de la propiedad mon.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setMon(Boolean value) {
            this.mon = value;
        }

        /**
         * Obtiene el valor de la propiedad tue.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isTue() {
            return tue;
        }

        /**
         * Define el valor de la propiedad tue.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setTue(Boolean value) {
            this.tue = value;
        }

        /**
         * Obtiene el valor de la propiedad wed.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isWed() {
            return wed;
        }

        /**
         * Define el valor de la propiedad wed.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setWed(Boolean value) {
            this.wed = value;
        }

        /**
         * Obtiene el valor de la propiedad thu.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isThu() {
            return thu;
        }

        /**
         * Define el valor de la propiedad thu.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setThu(Boolean value) {
            this.thu = value;
        }

        /**
         * Obtiene el valor de la propiedad fri.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isFri() {
            return fri;
        }

        /**
         * Define el valor de la propiedad fri.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setFri(Boolean value) {
            this.fri = value;
        }

        /**
         * Obtiene el valor de la propiedad sat.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSat() {
            return sat;
        }

        /**
         * Define el valor de la propiedad sat.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSat(Boolean value) {
            this.sat = value;
        }

        /**
         * Obtiene el valor de la propiedad sun.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSun() {
            return sun;
        }

        /**
         * Define el valor de la propiedad sun.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSun(Boolean value) {
            this.sun = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attribute name="MaxNbrTravelers" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="TotalNbrPTC" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class RestrictionPassengerTypes {

        @XmlAttribute(name = "MaxNbrTravelers")
        protected String maxNbrTravelers;
        @XmlAttribute(name = "TotalNbrPTC")
        protected String totalNbrPTC;

        /**
         * Obtiene el valor de la propiedad maxNbrTravelers.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMaxNbrTravelers() {
            return maxNbrTravelers;
        }

        /**
         * Define el valor de la propiedad maxNbrTravelers.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMaxNbrTravelers(String value) {
            this.maxNbrTravelers = value;
        }

        /**
         * Obtiene el valor de la propiedad totalNbrPTC.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTotalNbrPTC() {
            return totalNbrPTC;
        }

        /**
         * Define el valor de la propiedad totalNbrPTC.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTotalNbrPTC(String value) {
            this.totalNbrPTC = value;
        }

    }

}
