
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="RequireSingleCarrier" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="MaxConnections" default="-1"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer"&gt;
 *             &lt;minInclusive value="-1"/&gt;
 *             &lt;maxInclusive value="3"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="MaxStops" default="-1"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer"&gt;
 *             &lt;minInclusive value="-1"/&gt;
 *             &lt;maxInclusive value="3"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="NonStopDirects" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="StopDirects" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="SingleOnlineCon" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="DoubleOnlineCon" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="TripleOnlineCon" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="SingleInterlineCon" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="DoubleInterlineCon" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="TripleInterlineCon" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "FlightType")
public class FlightType {

    @XmlAttribute(name = "RequireSingleCarrier")
    protected Boolean requireSingleCarrier;
    @XmlAttribute(name = "MaxConnections")
    protected Integer maxConnections;
    @XmlAttribute(name = "MaxStops")
    protected Integer maxStops;
    @XmlAttribute(name = "NonStopDirects")
    protected Boolean nonStopDirects;
    @XmlAttribute(name = "StopDirects")
    protected Boolean stopDirects;
    @XmlAttribute(name = "SingleOnlineCon")
    protected Boolean singleOnlineCon;
    @XmlAttribute(name = "DoubleOnlineCon")
    protected Boolean doubleOnlineCon;
    @XmlAttribute(name = "TripleOnlineCon")
    protected Boolean tripleOnlineCon;
    @XmlAttribute(name = "SingleInterlineCon")
    protected Boolean singleInterlineCon;
    @XmlAttribute(name = "DoubleInterlineCon")
    protected Boolean doubleInterlineCon;
    @XmlAttribute(name = "TripleInterlineCon")
    protected Boolean tripleInterlineCon;

    /**
     * Obtiene el valor de la propiedad requireSingleCarrier.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isRequireSingleCarrier() {
        if (requireSingleCarrier == null) {
            return false;
        } else {
            return requireSingleCarrier;
        }
    }

    /**
     * Define el valor de la propiedad requireSingleCarrier.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRequireSingleCarrier(Boolean value) {
        this.requireSingleCarrier = value;
    }

    /**
     * Obtiene el valor de la propiedad maxConnections.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public int getMaxConnections() {
        if (maxConnections == null) {
            return -1;
        } else {
            return maxConnections;
        }
    }

    /**
     * Define el valor de la propiedad maxConnections.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxConnections(Integer value) {
        this.maxConnections = value;
    }

    /**
     * Obtiene el valor de la propiedad maxStops.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public int getMaxStops() {
        if (maxStops == null) {
            return -1;
        } else {
            return maxStops;
        }
    }

    /**
     * Define el valor de la propiedad maxStops.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxStops(Integer value) {
        this.maxStops = value;
    }

    /**
     * Obtiene el valor de la propiedad nonStopDirects.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNonStopDirects() {
        return nonStopDirects;
    }

    /**
     * Define el valor de la propiedad nonStopDirects.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNonStopDirects(Boolean value) {
        this.nonStopDirects = value;
    }

    /**
     * Obtiene el valor de la propiedad stopDirects.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isStopDirects() {
        return stopDirects;
    }

    /**
     * Define el valor de la propiedad stopDirects.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStopDirects(Boolean value) {
        this.stopDirects = value;
    }

    /**
     * Obtiene el valor de la propiedad singleOnlineCon.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSingleOnlineCon() {
        return singleOnlineCon;
    }

    /**
     * Define el valor de la propiedad singleOnlineCon.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSingleOnlineCon(Boolean value) {
        this.singleOnlineCon = value;
    }

    /**
     * Obtiene el valor de la propiedad doubleOnlineCon.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDoubleOnlineCon() {
        return doubleOnlineCon;
    }

    /**
     * Define el valor de la propiedad doubleOnlineCon.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDoubleOnlineCon(Boolean value) {
        this.doubleOnlineCon = value;
    }

    /**
     * Obtiene el valor de la propiedad tripleOnlineCon.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTripleOnlineCon() {
        return tripleOnlineCon;
    }

    /**
     * Define el valor de la propiedad tripleOnlineCon.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTripleOnlineCon(Boolean value) {
        this.tripleOnlineCon = value;
    }

    /**
     * Obtiene el valor de la propiedad singleInterlineCon.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSingleInterlineCon() {
        return singleInterlineCon;
    }

    /**
     * Define el valor de la propiedad singleInterlineCon.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSingleInterlineCon(Boolean value) {
        this.singleInterlineCon = value;
    }

    /**
     * Obtiene el valor de la propiedad doubleInterlineCon.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDoubleInterlineCon() {
        return doubleInterlineCon;
    }

    /**
     * Define el valor de la propiedad doubleInterlineCon.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDoubleInterlineCon(Boolean value) {
        this.doubleInterlineCon = value;
    }

    /**
     * Obtiene el valor de la propiedad tripleInterlineCon.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTripleInterlineCon() {
        return tripleInterlineCon;
    }

    /**
     * Define el valor de la propiedad tripleInterlineCon.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTripleInterlineCon(Boolean value) {
        this.tripleInterlineCon = value;
    }

}
