
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="ModifierType" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;enumeration value="FareFamilyDisplay"/&gt;
 *             &lt;enumeration value="BasicDetailOnly"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "BrandModifiers")
public class BrandModifiers {

    @XmlAttribute(name = "ModifierType", required = true)
    protected String modifierType;

    /**
     * Obtiene el valor de la propiedad modifierType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModifierType() {
        return modifierType;
    }

    /**
     * Define el valor de la propiedad modifierType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModifierType(String value) {
        this.modifierType = value;
    }

}
