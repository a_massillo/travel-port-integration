
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="RailCoachNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="AvailableRailSeats" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="RailSeatMapAvailability" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "RailCoachDetails")
public class RailCoachDetails {

    @XmlAttribute(name = "RailCoachNumber")
    protected String railCoachNumber;
    @XmlAttribute(name = "AvailableRailSeats")
    protected String availableRailSeats;
    @XmlAttribute(name = "RailSeatMapAvailability")
    protected Boolean railSeatMapAvailability;

    /**
     * Obtiene el valor de la propiedad railCoachNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRailCoachNumber() {
        return railCoachNumber;
    }

    /**
     * Define el valor de la propiedad railCoachNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRailCoachNumber(String value) {
        this.railCoachNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad availableRailSeats.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAvailableRailSeats() {
        return availableRailSeats;
    }

    /**
     * Define el valor de la propiedad availableRailSeats.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAvailableRailSeats(String value) {
        this.availableRailSeats = value;
    }

    /**
     * Obtiene el valor de la propiedad railSeatMapAvailability.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRailSeatMapAvailability() {
        return railSeatMapAvailability;
    }

    /**
     * Define el valor de la propiedad railSeatMapAvailability.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRailSeatMapAvailability(Boolean value) {
        this.railSeatMapAvailability = value;
    }

}
