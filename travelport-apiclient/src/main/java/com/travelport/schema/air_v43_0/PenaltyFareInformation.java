
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PenaltyInfo" type="{http://www.travelport.com/schema/air_v43_0}typeFarePenalty" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ProhibitPenaltyFares" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "penaltyInfo"
})
@XmlRootElement(name = "PenaltyFareInformation")
public class PenaltyFareInformation {

    @XmlElement(name = "PenaltyInfo")
    protected TypeFarePenalty penaltyInfo;
    @XmlAttribute(name = "ProhibitPenaltyFares", required = true)
    protected boolean prohibitPenaltyFares;

    /**
     * Obtiene el valor de la propiedad penaltyInfo.
     * 
     * @return
     *     possible object is
     *     {@link TypeFarePenalty }
     *     
     */
    public TypeFarePenalty getPenaltyInfo() {
        return penaltyInfo;
    }

    /**
     * Define el valor de la propiedad penaltyInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeFarePenalty }
     *     
     */
    public void setPenaltyInfo(TypeFarePenalty value) {
        this.penaltyInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad prohibitPenaltyFares.
     * 
     */
    public boolean isProhibitPenaltyFares() {
        return prohibitPenaltyFares;
    }

    /**
     * Define el valor de la propiedad prohibitPenaltyFares.
     * 
     */
    public void setProhibitPenaltyFares(boolean value) {
        this.prohibitPenaltyFares = value;
    }

}
