
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseRsp;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseRsp"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}ExchangeEligibilityInfo"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "exchangeEligibilityInfo"
})
@XmlRootElement(name = "AirExchangeEligibilityRsp")
public class AirExchangeEligibilityRsp
    extends BaseRsp
{

    @XmlElement(name = "ExchangeEligibilityInfo", required = true)
    protected ExchangeEligibilityInfo exchangeEligibilityInfo;

    /**
     * Obtiene el valor de la propiedad exchangeEligibilityInfo.
     * 
     * @return
     *     possible object is
     *     {@link ExchangeEligibilityInfo }
     *     
     */
    public ExchangeEligibilityInfo getExchangeEligibilityInfo() {
        return exchangeEligibilityInfo;
    }

    /**
     * Define el valor de la propiedad exchangeEligibilityInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ExchangeEligibilityInfo }
     *     
     */
    public void setExchangeEligibilityInfo(ExchangeEligibilityInfo value) {
        this.exchangeEligibilityInfo = value;
    }

}
