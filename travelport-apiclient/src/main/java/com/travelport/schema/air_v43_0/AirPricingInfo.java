
package com.travelport.schema.air_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BookingTravelerRef;
import com.travelport.schema.common_v43_0.TypeElementStatus;
import com.travelport.schema.common_v43_0.TypeFeeInfo;
import com.travelport.schema.common_v43_0.TypeTaxInfo;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FareInfo" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FareStatus" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FareInfoRef" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}BookingInfo" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}TaxInfo" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FareCalc" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}PassengerType" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}BookingTravelerRef" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}WaiverCode" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}PaymentRef" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element name="ChangePenalty" type="{http://www.travelport.com/schema/air_v43_0}typeFarePenalty" minOccurs="0"/&gt;
 *         &lt;element name="CancelPenalty" type="{http://www.travelport.com/schema/air_v43_0}typeFarePenalty" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FeeInfo" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}Adjustment" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}Yield" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}AirPricingModifiers" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}TicketingModifiersRef" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}AirSegmentPricingModifiers" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FlightOptionsList" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}BaggageAllowances" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FareRulesFilter" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}PolicyCodesList" minOccurs="0"/&gt;
 *         &lt;element name="PriceChange" type="{http://www.travelport.com/schema/air_v43_0}PriceChangeType" maxOccurs="99" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}ActionDetails" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attGroup ref="{http://www.travelport.com/schema/common_v43_0}attrPrices"/&gt;
 *       &lt;attGroup ref="{http://www.travelport.com/schema/common_v43_0}attrElementKeyResults"/&gt;
 *       &lt;attGroup ref="{http://www.travelport.com/schema/common_v43_0}attrProviderSupplier"/&gt;
 *       &lt;attGroup ref="{http://www.travelport.com/schema/air_v43_0}attrPolicyMarking"/&gt;
 *       &lt;attribute name="Key" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="CommandKey"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="10"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="AmountType" type="{http://www.travelport.com/schema/common_v43_0}StringLength1to32" /&gt;
 *       &lt;attribute name="IncludesVAT" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="ExchangeAmount" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="ForfeitAmount" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="Refundable" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="Exchangeable" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="LatestTicketingTime" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="PricingMethod" use="required" type="{http://www.travelport.com/schema/air_v43_0}typePricingMethod" /&gt;
 *       &lt;attribute name="Checksum" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="ETicketability" type="{http://www.travelport.com/schema/air_v43_0}typeEticketability" /&gt;
 *       &lt;attribute name="PlatingCarrier" type="{http://www.travelport.com/schema/common_v43_0}typeCarrier" /&gt;
 *       &lt;attribute name="ProviderReservationInfoRef" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="AirPricingInfoGroup" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="TotalNetPrice" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="Ticketed" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="PricingType"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="25"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="TrueLastDateToTicket" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="FareCalculationInd"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;length value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Cat35Indicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fareInfo",
    "fareStatus",
    "fareInfoRef",
    "bookingInfo",
    "taxInfo",
    "fareCalc",
    "passengerType",
    "bookingTravelerRef",
    "waiverCode",
    "paymentRef",
    "changePenalty",
    "cancelPenalty",
    "feeInfo",
    "adjustment",
    "yield",
    "airPricingModifiers",
    "ticketingModifiersRef",
    "airSegmentPricingModifiers",
    "flightOptionsList",
    "baggageAllowances",
    "fareRulesFilter",
    "policyCodesList",
    "priceChange",
    "actionDetails"
})
@XmlRootElement(name = "AirPricingInfo")
public class AirPricingInfo {

    @XmlElement(name = "FareInfo")
    protected List<FareInfo> fareInfo;
    @XmlElement(name = "FareStatus")
    protected FareStatus fareStatus;
    @XmlElement(name = "FareInfoRef")
    protected List<FareInfoRef> fareInfoRef;
    @XmlElement(name = "BookingInfo")
    protected List<BookingInfo> bookingInfo;
    @XmlElement(name = "TaxInfo")
    protected List<TypeTaxInfo> taxInfo;
    @XmlElement(name = "FareCalc")
    protected String fareCalc;
    @XmlElement(name = "PassengerType")
    protected List<PassengerType> passengerType;
    @XmlElement(name = "BookingTravelerRef", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected List<BookingTravelerRef> bookingTravelerRef;
    @XmlElement(name = "WaiverCode")
    protected WaiverCode waiverCode;
    @XmlElement(name = "PaymentRef")
    protected List<PaymentRef> paymentRef;
    @XmlElement(name = "ChangePenalty")
    protected TypeFarePenalty changePenalty;
    @XmlElement(name = "CancelPenalty")
    protected TypeFarePenalty cancelPenalty;
    @XmlElement(name = "FeeInfo")
    protected List<TypeFeeInfo> feeInfo;
    @XmlElement(name = "Adjustment")
    protected List<Adjustment> adjustment;
    @XmlElement(name = "Yield")
    protected List<Yield> yield;
    @XmlElement(name = "AirPricingModifiers")
    protected AirPricingModifiers airPricingModifiers;
    @XmlElement(name = "TicketingModifiersRef")
    protected List<TicketingModifiersRef> ticketingModifiersRef;
    @XmlElement(name = "AirSegmentPricingModifiers")
    protected List<AirSegmentPricingModifiers> airSegmentPricingModifiers;
    @XmlElement(name = "FlightOptionsList")
    protected FlightOptionsList flightOptionsList;
    @XmlElement(name = "BaggageAllowances")
    protected BaggageAllowances baggageAllowances;
    @XmlElement(name = "FareRulesFilter")
    protected FareRulesFilter fareRulesFilter;
    @XmlElement(name = "PolicyCodesList")
    protected PolicyCodesList policyCodesList;
    @XmlElement(name = "PriceChange")
    protected List<PriceChangeType> priceChange;
    @XmlElement(name = "ActionDetails")
    protected ActionDetails actionDetails;
    @XmlAttribute(name = "Key", required = true)
    protected String key;
    @XmlAttribute(name = "CommandKey")
    protected String commandKey;
    @XmlAttribute(name = "AmountType")
    protected String amountType;
    @XmlAttribute(name = "IncludesVAT")
    protected Boolean includesVAT;
    @XmlAttribute(name = "ExchangeAmount")
    protected String exchangeAmount;
    @XmlAttribute(name = "ForfeitAmount")
    protected String forfeitAmount;
    @XmlAttribute(name = "Refundable")
    protected Boolean refundable;
    @XmlAttribute(name = "Exchangeable")
    protected Boolean exchangeable;
    @XmlAttribute(name = "LatestTicketingTime")
    protected String latestTicketingTime;
    @XmlAttribute(name = "PricingMethod", required = true)
    protected TypePricingMethod pricingMethod;
    @XmlAttribute(name = "Checksum")
    protected String checksum;
    @XmlAttribute(name = "ETicketability")
    protected TypeEticketability eTicketability;
    @XmlAttribute(name = "PlatingCarrier")
    protected String platingCarrier;
    @XmlAttribute(name = "ProviderReservationInfoRef")
    protected String providerReservationInfoRef;
    @XmlAttribute(name = "AirPricingInfoGroup")
    protected Integer airPricingInfoGroup;
    @XmlAttribute(name = "TotalNetPrice")
    protected String totalNetPrice;
    @XmlAttribute(name = "Ticketed")
    protected Boolean ticketed;
    @XmlAttribute(name = "PricingType")
    protected String pricingType;
    @XmlAttribute(name = "TrueLastDateToTicket")
    protected String trueLastDateToTicket;
    @XmlAttribute(name = "FareCalculationInd")
    protected String fareCalculationInd;
    @XmlAttribute(name = "Cat35Indicator")
    protected Boolean cat35Indicator;
    @XmlAttribute(name = "TotalPrice")
    protected String totalPrice;
    @XmlAttribute(name = "BasePrice")
    protected String basePrice;
    @XmlAttribute(name = "ApproximateTotalPrice")
    protected String approximateTotalPrice;
    @XmlAttribute(name = "ApproximateBasePrice")
    protected String approximateBasePrice;
    @XmlAttribute(name = "EquivalentBasePrice")
    protected String equivalentBasePrice;
    @XmlAttribute(name = "Taxes")
    protected String taxes;
    @XmlAttribute(name = "Fees")
    protected String fees;
    @XmlAttribute(name = "Services")
    protected String services;
    @XmlAttribute(name = "ApproximateTaxes")
    protected String approximateTaxes;
    @XmlAttribute(name = "ApproximateFees")
    protected String approximateFees;
    @XmlAttribute(name = "ElStat")
    protected TypeElementStatus elStat;
    @XmlAttribute(name = "KeyOverride")
    protected Boolean keyOverride;
    @XmlAttribute(name = "ProviderCode")
    protected String providerCode;
    @XmlAttribute(name = "SupplierCode")
    protected String supplierCode;
    @XmlAttribute(name = "InPolicy")
    protected Boolean inPolicy;
    @XmlAttribute(name = "PreferredOption")
    protected Boolean preferredOption;

    /**
     * Gets the value of the fareInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fareInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFareInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FareInfo }
     * 
     * 
     */
    public List<FareInfo> getFareInfo() {
        if (fareInfo == null) {
            fareInfo = new ArrayList<FareInfo>();
        }
        return this.fareInfo;
    }

    /**
     * Obtiene el valor de la propiedad fareStatus.
     * 
     * @return
     *     possible object is
     *     {@link FareStatus }
     *     
     */
    public FareStatus getFareStatus() {
        return fareStatus;
    }

    /**
     * Define el valor de la propiedad fareStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link FareStatus }
     *     
     */
    public void setFareStatus(FareStatus value) {
        this.fareStatus = value;
    }

    /**
     * Gets the value of the fareInfoRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fareInfoRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFareInfoRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FareInfoRef }
     * 
     * 
     */
    public List<FareInfoRef> getFareInfoRef() {
        if (fareInfoRef == null) {
            fareInfoRef = new ArrayList<FareInfoRef>();
        }
        return this.fareInfoRef;
    }

    /**
     * Gets the value of the bookingInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bookingInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBookingInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BookingInfo }
     * 
     * 
     */
    public List<BookingInfo> getBookingInfo() {
        if (bookingInfo == null) {
            bookingInfo = new ArrayList<BookingInfo>();
        }
        return this.bookingInfo;
    }

    /**
     * Gets the value of the taxInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the taxInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTaxInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TypeTaxInfo }
     * 
     * 
     */
    public List<TypeTaxInfo> getTaxInfo() {
        if (taxInfo == null) {
            taxInfo = new ArrayList<TypeTaxInfo>();
        }
        return this.taxInfo;
    }

    /**
     * Obtiene el valor de la propiedad fareCalc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFareCalc() {
        return fareCalc;
    }

    /**
     * Define el valor de la propiedad fareCalc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFareCalc(String value) {
        this.fareCalc = value;
    }

    /**
     * Gets the value of the passengerType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the passengerType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPassengerType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PassengerType }
     * 
     * 
     */
    public List<PassengerType> getPassengerType() {
        if (passengerType == null) {
            passengerType = new ArrayList<PassengerType>();
        }
        return this.passengerType;
    }

    /**
     * Gets the value of the bookingTravelerRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bookingTravelerRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBookingTravelerRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BookingTravelerRef }
     * 
     * 
     */
    public List<BookingTravelerRef> getBookingTravelerRef() {
        if (bookingTravelerRef == null) {
            bookingTravelerRef = new ArrayList<BookingTravelerRef>();
        }
        return this.bookingTravelerRef;
    }

    /**
     * Obtiene el valor de la propiedad waiverCode.
     * 
     * @return
     *     possible object is
     *     {@link WaiverCode }
     *     
     */
    public WaiverCode getWaiverCode() {
        return waiverCode;
    }

    /**
     * Define el valor de la propiedad waiverCode.
     * 
     * @param value
     *     allowed object is
     *     {@link WaiverCode }
     *     
     */
    public void setWaiverCode(WaiverCode value) {
        this.waiverCode = value;
    }

    /**
     * The reference to the Payment if Air Pricing
     *                             is charged Gets the value of the paymentRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paymentRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaymentRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaymentRef }
     * 
     * 
     */
    public List<PaymentRef> getPaymentRef() {
        if (paymentRef == null) {
            paymentRef = new ArrayList<PaymentRef>();
        }
        return this.paymentRef;
    }

    /**
     * Obtiene el valor de la propiedad changePenalty.
     * 
     * @return
     *     possible object is
     *     {@link TypeFarePenalty }
     *     
     */
    public TypeFarePenalty getChangePenalty() {
        return changePenalty;
    }

    /**
     * Define el valor de la propiedad changePenalty.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeFarePenalty }
     *     
     */
    public void setChangePenalty(TypeFarePenalty value) {
        this.changePenalty = value;
    }

    /**
     * Obtiene el valor de la propiedad cancelPenalty.
     * 
     * @return
     *     possible object is
     *     {@link TypeFarePenalty }
     *     
     */
    public TypeFarePenalty getCancelPenalty() {
        return cancelPenalty;
    }

    /**
     * Define el valor de la propiedad cancelPenalty.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeFarePenalty }
     *     
     */
    public void setCancelPenalty(TypeFarePenalty value) {
        this.cancelPenalty = value;
    }

    /**
     * Gets the value of the feeInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the feeInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFeeInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TypeFeeInfo }
     * 
     * 
     */
    public List<TypeFeeInfo> getFeeInfo() {
        if (feeInfo == null) {
            feeInfo = new ArrayList<TypeFeeInfo>();
        }
        return this.feeInfo;
    }

    /**
     * Gets the value of the adjustment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the adjustment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdjustment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Adjustment }
     * 
     * 
     */
    public List<Adjustment> getAdjustment() {
        if (adjustment == null) {
            adjustment = new ArrayList<Adjustment>();
        }
        return this.adjustment;
    }

    /**
     * Gets the value of the yield property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the yield property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getYield().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Yield }
     * 
     * 
     */
    public List<Yield> getYield() {
        if (yield == null) {
            yield = new ArrayList<Yield>();
        }
        return this.yield;
    }

    /**
     * Obtiene el valor de la propiedad airPricingModifiers.
     * 
     * @return
     *     possible object is
     *     {@link AirPricingModifiers }
     *     
     */
    public AirPricingModifiers getAirPricingModifiers() {
        return airPricingModifiers;
    }

    /**
     * Define el valor de la propiedad airPricingModifiers.
     * 
     * @param value
     *     allowed object is
     *     {@link AirPricingModifiers }
     *     
     */
    public void setAirPricingModifiers(AirPricingModifiers value) {
        this.airPricingModifiers = value;
    }

    /**
     * Gets the value of the ticketingModifiersRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ticketingModifiersRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTicketingModifiersRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TicketingModifiersRef }
     * 
     * 
     */
    public List<TicketingModifiersRef> getTicketingModifiersRef() {
        if (ticketingModifiersRef == null) {
            ticketingModifiersRef = new ArrayList<TicketingModifiersRef>();
        }
        return this.ticketingModifiersRef;
    }

    /**
     * Gets the value of the airSegmentPricingModifiers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the airSegmentPricingModifiers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAirSegmentPricingModifiers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AirSegmentPricingModifiers }
     * 
     * 
     */
    public List<AirSegmentPricingModifiers> getAirSegmentPricingModifiers() {
        if (airSegmentPricingModifiers == null) {
            airSegmentPricingModifiers = new ArrayList<AirSegmentPricingModifiers>();
        }
        return this.airSegmentPricingModifiers;
    }

    /**
     * Obtiene el valor de la propiedad flightOptionsList.
     * 
     * @return
     *     possible object is
     *     {@link FlightOptionsList }
     *     
     */
    public FlightOptionsList getFlightOptionsList() {
        return flightOptionsList;
    }

    /**
     * Define el valor de la propiedad flightOptionsList.
     * 
     * @param value
     *     allowed object is
     *     {@link FlightOptionsList }
     *     
     */
    public void setFlightOptionsList(FlightOptionsList value) {
        this.flightOptionsList = value;
    }

    /**
     * Obtiene el valor de la propiedad baggageAllowances.
     * 
     * @return
     *     possible object is
     *     {@link BaggageAllowances }
     *     
     */
    public BaggageAllowances getBaggageAllowances() {
        return baggageAllowances;
    }

    /**
     * Define el valor de la propiedad baggageAllowances.
     * 
     * @param value
     *     allowed object is
     *     {@link BaggageAllowances }
     *     
     */
    public void setBaggageAllowances(BaggageAllowances value) {
        this.baggageAllowances = value;
    }

    /**
     * Obtiene el valor de la propiedad fareRulesFilter.
     * 
     * @return
     *     possible object is
     *     {@link FareRulesFilter }
     *     
     */
    public FareRulesFilter getFareRulesFilter() {
        return fareRulesFilter;
    }

    /**
     * Define el valor de la propiedad fareRulesFilter.
     * 
     * @param value
     *     allowed object is
     *     {@link FareRulesFilter }
     *     
     */
    public void setFareRulesFilter(FareRulesFilter value) {
        this.fareRulesFilter = value;
    }

    /**
     * A list of codes that indicate why an item was determined to be �out of policy�
     * 
     * @return
     *     possible object is
     *     {@link PolicyCodesList }
     *     
     */
    public PolicyCodesList getPolicyCodesList() {
        return policyCodesList;
    }

    /**
     * Define el valor de la propiedad policyCodesList.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyCodesList }
     *     
     */
    public void setPolicyCodesList(PolicyCodesList value) {
        this.policyCodesList = value;
    }

    /**
     * Gets the value of the priceChange property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the priceChange property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPriceChange().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PriceChangeType }
     * 
     * 
     */
    public List<PriceChangeType> getPriceChange() {
        if (priceChange == null) {
            priceChange = new ArrayList<PriceChangeType>();
        }
        return this.priceChange;
    }

    /**
     * Obtiene el valor de la propiedad actionDetails.
     * 
     * @return
     *     possible object is
     *     {@link ActionDetails }
     *     
     */
    public ActionDetails getActionDetails() {
        return actionDetails;
    }

    /**
     * Define el valor de la propiedad actionDetails.
     * 
     * @param value
     *     allowed object is
     *     {@link ActionDetails }
     *     
     */
    public void setActionDetails(ActionDetails value) {
        this.actionDetails = value;
    }

    /**
     * Obtiene el valor de la propiedad key.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Define el valor de la propiedad key.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Obtiene el valor de la propiedad commandKey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommandKey() {
        return commandKey;
    }

    /**
     * Define el valor de la propiedad commandKey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommandKey(String value) {
        this.commandKey = value;
    }

    /**
     * Obtiene el valor de la propiedad amountType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmountType() {
        return amountType;
    }

    /**
     * Define el valor de la propiedad amountType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmountType(String value) {
        this.amountType = value;
    }

    /**
     * Obtiene el valor de la propiedad includesVAT.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludesVAT() {
        return includesVAT;
    }

    /**
     * Define el valor de la propiedad includesVAT.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludesVAT(Boolean value) {
        this.includesVAT = value;
    }

    /**
     * Obtiene el valor de la propiedad exchangeAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExchangeAmount() {
        return exchangeAmount;
    }

    /**
     * Define el valor de la propiedad exchangeAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExchangeAmount(String value) {
        this.exchangeAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad forfeitAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForfeitAmount() {
        return forfeitAmount;
    }

    /**
     * Define el valor de la propiedad forfeitAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForfeitAmount(String value) {
        this.forfeitAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad refundable.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRefundable() {
        return refundable;
    }

    /**
     * Define el valor de la propiedad refundable.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRefundable(Boolean value) {
        this.refundable = value;
    }

    /**
     * Obtiene el valor de la propiedad exchangeable.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExchangeable() {
        return exchangeable;
    }

    /**
     * Define el valor de la propiedad exchangeable.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExchangeable(Boolean value) {
        this.exchangeable = value;
    }

    /**
     * Obtiene el valor de la propiedad latestTicketingTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLatestTicketingTime() {
        return latestTicketingTime;
    }

    /**
     * Define el valor de la propiedad latestTicketingTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLatestTicketingTime(String value) {
        this.latestTicketingTime = value;
    }

    /**
     * Obtiene el valor de la propiedad pricingMethod.
     * 
     * @return
     *     possible object is
     *     {@link TypePricingMethod }
     *     
     */
    public TypePricingMethod getPricingMethod() {
        return pricingMethod;
    }

    /**
     * Define el valor de la propiedad pricingMethod.
     * 
     * @param value
     *     allowed object is
     *     {@link TypePricingMethod }
     *     
     */
    public void setPricingMethod(TypePricingMethod value) {
        this.pricingMethod = value;
    }

    /**
     * Obtiene el valor de la propiedad checksum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChecksum() {
        return checksum;
    }

    /**
     * Define el valor de la propiedad checksum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChecksum(String value) {
        this.checksum = value;
    }

    /**
     * Obtiene el valor de la propiedad eTicketability.
     * 
     * @return
     *     possible object is
     *     {@link TypeEticketability }
     *     
     */
    public TypeEticketability getETicketability() {
        return eTicketability;
    }

    /**
     * Define el valor de la propiedad eTicketability.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeEticketability }
     *     
     */
    public void setETicketability(TypeEticketability value) {
        this.eTicketability = value;
    }

    /**
     * Obtiene el valor de la propiedad platingCarrier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlatingCarrier() {
        return platingCarrier;
    }

    /**
     * Define el valor de la propiedad platingCarrier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlatingCarrier(String value) {
        this.platingCarrier = value;
    }

    /**
     * Obtiene el valor de la propiedad providerReservationInfoRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProviderReservationInfoRef() {
        return providerReservationInfoRef;
    }

    /**
     * Define el valor de la propiedad providerReservationInfoRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProviderReservationInfoRef(String value) {
        this.providerReservationInfoRef = value;
    }

    /**
     * Obtiene el valor de la propiedad airPricingInfoGroup.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAirPricingInfoGroup() {
        return airPricingInfoGroup;
    }

    /**
     * Define el valor de la propiedad airPricingInfoGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAirPricingInfoGroup(Integer value) {
        this.airPricingInfoGroup = value;
    }

    /**
     * Obtiene el valor de la propiedad totalNetPrice.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalNetPrice() {
        return totalNetPrice;
    }

    /**
     * Define el valor de la propiedad totalNetPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalNetPrice(String value) {
        this.totalNetPrice = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketed.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTicketed() {
        return ticketed;
    }

    /**
     * Define el valor de la propiedad ticketed.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTicketed(Boolean value) {
        this.ticketed = value;
    }

    /**
     * Obtiene el valor de la propiedad pricingType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPricingType() {
        return pricingType;
    }

    /**
     * Define el valor de la propiedad pricingType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPricingType(String value) {
        this.pricingType = value;
    }

    /**
     * Obtiene el valor de la propiedad trueLastDateToTicket.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrueLastDateToTicket() {
        return trueLastDateToTicket;
    }

    /**
     * Define el valor de la propiedad trueLastDateToTicket.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrueLastDateToTicket(String value) {
        this.trueLastDateToTicket = value;
    }

    /**
     * Obtiene el valor de la propiedad fareCalculationInd.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFareCalculationInd() {
        return fareCalculationInd;
    }

    /**
     * Define el valor de la propiedad fareCalculationInd.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFareCalculationInd(String value) {
        this.fareCalculationInd = value;
    }

    /**
     * Obtiene el valor de la propiedad cat35Indicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCat35Indicator() {
        return cat35Indicator;
    }

    /**
     * Define el valor de la propiedad cat35Indicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCat35Indicator(Boolean value) {
        this.cat35Indicator = value;
    }

    /**
     * Obtiene el valor de la propiedad totalPrice.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalPrice() {
        return totalPrice;
    }

    /**
     * Define el valor de la propiedad totalPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalPrice(String value) {
        this.totalPrice = value;
    }

    /**
     * Obtiene el valor de la propiedad basePrice.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBasePrice() {
        return basePrice;
    }

    /**
     * Define el valor de la propiedad basePrice.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBasePrice(String value) {
        this.basePrice = value;
    }

    /**
     * Obtiene el valor de la propiedad approximateTotalPrice.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApproximateTotalPrice() {
        return approximateTotalPrice;
    }

    /**
     * Define el valor de la propiedad approximateTotalPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApproximateTotalPrice(String value) {
        this.approximateTotalPrice = value;
    }

    /**
     * Obtiene el valor de la propiedad approximateBasePrice.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApproximateBasePrice() {
        return approximateBasePrice;
    }

    /**
     * Define el valor de la propiedad approximateBasePrice.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApproximateBasePrice(String value) {
        this.approximateBasePrice = value;
    }

    /**
     * Obtiene el valor de la propiedad equivalentBasePrice.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquivalentBasePrice() {
        return equivalentBasePrice;
    }

    /**
     * Define el valor de la propiedad equivalentBasePrice.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquivalentBasePrice(String value) {
        this.equivalentBasePrice = value;
    }

    /**
     * Obtiene el valor de la propiedad taxes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxes() {
        return taxes;
    }

    /**
     * Define el valor de la propiedad taxes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxes(String value) {
        this.taxes = value;
    }

    /**
     * Obtiene el valor de la propiedad fees.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFees() {
        return fees;
    }

    /**
     * Define el valor de la propiedad fees.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFees(String value) {
        this.fees = value;
    }

    /**
     * Obtiene el valor de la propiedad services.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServices() {
        return services;
    }

    /**
     * Define el valor de la propiedad services.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServices(String value) {
        this.services = value;
    }

    /**
     * Obtiene el valor de la propiedad approximateTaxes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApproximateTaxes() {
        return approximateTaxes;
    }

    /**
     * Define el valor de la propiedad approximateTaxes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApproximateTaxes(String value) {
        this.approximateTaxes = value;
    }

    /**
     * Obtiene el valor de la propiedad approximateFees.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApproximateFees() {
        return approximateFees;
    }

    /**
     * Define el valor de la propiedad approximateFees.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApproximateFees(String value) {
        this.approximateFees = value;
    }

    /**
     * Obtiene el valor de la propiedad elStat.
     * 
     * @return
     *     possible object is
     *     {@link TypeElementStatus }
     *     
     */
    public TypeElementStatus getElStat() {
        return elStat;
    }

    /**
     * Define el valor de la propiedad elStat.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeElementStatus }
     *     
     */
    public void setElStat(TypeElementStatus value) {
        this.elStat = value;
    }

    /**
     * Obtiene el valor de la propiedad keyOverride.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isKeyOverride() {
        return keyOverride;
    }

    /**
     * Define el valor de la propiedad keyOverride.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setKeyOverride(Boolean value) {
        this.keyOverride = value;
    }

    /**
     * Obtiene el valor de la propiedad providerCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProviderCode() {
        return providerCode;
    }

    /**
     * Define el valor de la propiedad providerCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProviderCode(String value) {
        this.providerCode = value;
    }

    /**
     * Obtiene el valor de la propiedad supplierCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupplierCode() {
        return supplierCode;
    }

    /**
     * Define el valor de la propiedad supplierCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupplierCode(String value) {
        this.supplierCode = value;
    }

    /**
     * Obtiene el valor de la propiedad inPolicy.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isInPolicy() {
        return inPolicy;
    }

    /**
     * Define el valor de la propiedad inPolicy.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInPolicy(Boolean value) {
        this.inPolicy = value;
    }

    /**
     * Obtiene el valor de la propiedad preferredOption.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPreferredOption() {
        return preferredOption;
    }

    /**
     * Define el valor de la propiedad preferredOption.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPreferredOption(Boolean value) {
        this.preferredOption = value;
    }

}
