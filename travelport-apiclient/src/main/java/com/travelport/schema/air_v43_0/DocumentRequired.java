
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="DocType" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="IncludeExcludeUseInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="DocId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="AllowedIds" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "DocumentRequired")
public class DocumentRequired {

    @XmlAttribute(name = "DocType")
    protected String docType;
    @XmlAttribute(name = "IncludeExcludeUseInd")
    protected Boolean includeExcludeUseInd;
    @XmlAttribute(name = "DocId")
    protected String docId;
    @XmlAttribute(name = "AllowedIds")
    protected String allowedIds;

    /**
     * Obtiene el valor de la propiedad docType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocType() {
        return docType;
    }

    /**
     * Define el valor de la propiedad docType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocType(String value) {
        this.docType = value;
    }

    /**
     * Obtiene el valor de la propiedad includeExcludeUseInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeExcludeUseInd() {
        return includeExcludeUseInd;
    }

    /**
     * Define el valor de la propiedad includeExcludeUseInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeExcludeUseInd(Boolean value) {
        this.includeExcludeUseInd = value;
    }

    /**
     * Obtiene el valor de la propiedad docId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocId() {
        return docId;
    }

    /**
     * Define el valor de la propiedad docId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocId(String value) {
        this.docId = value;
    }

    /**
     * Obtiene el valor de la propiedad allowedIds.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllowedIds() {
        return allowedIds;
    }

    /**
     * Define el valor de la propiedad allowedIds.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllowedIds(String value) {
        this.allowedIds = value;
    }

}
