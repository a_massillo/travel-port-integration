
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseRsp;
import com.travelport.schema.common_v43_0.Remark;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseRsp"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}AirSolution"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Remark" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}OptionalServices" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}EmbargoList" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "airSolution",
    "remark",
    "optionalServices",
    "embargoList"
})
@XmlRootElement(name = "AirMerchandisingOfferAvailabilityRsp")
public class AirMerchandisingOfferAvailabilityRsp
    extends BaseRsp
{

    @XmlElement(name = "AirSolution", required = true)
    protected AirSolution airSolution;
    @XmlElement(name = "Remark", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected Remark remark;
    @XmlElement(name = "OptionalServices")
    protected OptionalServices optionalServices;
    @XmlElement(name = "EmbargoList")
    protected EmbargoList embargoList;

    /**
     * Provider: 1G,1V,1P,1J,ACH.
     * 
     * @return
     *     possible object is
     *     {@link AirSolution }
     *     
     */
    public AirSolution getAirSolution() {
        return airSolution;
    }

    /**
     * Define el valor de la propiedad airSolution.
     * 
     * @param value
     *     allowed object is
     *     {@link AirSolution }
     *     
     */
    public void setAirSolution(AirSolution value) {
        this.airSolution = value;
    }

    /**
     * Provider: 1G,1V,1P,1J,ACH.
     * 
     * @return
     *     possible object is
     *     {@link Remark }
     *     
     */
    public Remark getRemark() {
        return remark;
    }

    /**
     * Define el valor de la propiedad remark.
     * 
     * @param value
     *     allowed object is
     *     {@link Remark }
     *     
     */
    public void setRemark(Remark value) {
        this.remark = value;
    }

    /**
     * Obtiene el valor de la propiedad optionalServices.
     * 
     * @return
     *     possible object is
     *     {@link OptionalServices }
     *     
     */
    public OptionalServices getOptionalServices() {
        return optionalServices;
    }

    /**
     * Define el valor de la propiedad optionalServices.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionalServices }
     *     
     */
    public void setOptionalServices(OptionalServices value) {
        this.optionalServices = value;
    }

    /**
     * Obtiene el valor de la propiedad embargoList.
     * 
     * @return
     *     possible object is
     *     {@link EmbargoList }
     *     
     */
    public EmbargoList getEmbargoList() {
        return embargoList;
    }

    /**
     * Define el valor de la propiedad embargoList.
     * 
     * @param value
     *     allowed object is
     *     {@link EmbargoList }
     *     
     */
    public void setEmbargoList(EmbargoList value) {
        this.embargoList = value;
    }

}
