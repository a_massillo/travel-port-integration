
package com.travelport.schema.air_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.ServiceRuleType;
import com.travelport.schema.common_v43_0.TypeFeeInfo;
import com.travelport.schema.common_v43_0.TypeTaxInfo;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OptionalServicesTotal" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/air_v43_0}TaxInfo" maxOccurs="999" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FeeInfo" maxOccurs="999" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attGroup ref="{http://www.travelport.com/schema/common_v43_0}attrPrices"/&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}OptionalService" maxOccurs="999"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}GroupedOptionInfo" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element name="OptionalServiceRules" type="{http://www.travelport.com/schema/common_v43_0}ServiceRuleType" maxOccurs="999" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "optionalServicesTotal",
    "optionalService",
    "groupedOptionInfo",
    "optionalServiceRules"
})
@XmlRootElement(name = "OptionalServices")
public class OptionalServices {

    @XmlElement(name = "OptionalServicesTotal")
    protected OptionalServices.OptionalServicesTotal optionalServicesTotal;
    @XmlElement(name = "OptionalService", required = true)
    protected List<OptionalService> optionalService;
    @XmlElement(name = "GroupedOptionInfo")
    protected List<GroupedOptionInfo> groupedOptionInfo;
    @XmlElement(name = "OptionalServiceRules")
    protected List<ServiceRuleType> optionalServiceRules;

    /**
     * Obtiene el valor de la propiedad optionalServicesTotal.
     * 
     * @return
     *     possible object is
     *     {@link OptionalServices.OptionalServicesTotal }
     *     
     */
    public OptionalServices.OptionalServicesTotal getOptionalServicesTotal() {
        return optionalServicesTotal;
    }

    /**
     * Define el valor de la propiedad optionalServicesTotal.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionalServices.OptionalServicesTotal }
     *     
     */
    public void setOptionalServicesTotal(OptionalServices.OptionalServicesTotal value) {
        this.optionalServicesTotal = value;
    }

    /**
     * Gets the value of the optionalService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the optionalService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOptionalService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OptionalService }
     * 
     * 
     */
    public List<OptionalService> getOptionalService() {
        if (optionalService == null) {
            optionalService = new ArrayList<OptionalService>();
        }
        return this.optionalService;
    }

    /**
     * Details about an unselected or "other" option when optional services are grouped together.Gets the value of the groupedOptionInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the groupedOptionInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGroupedOptionInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GroupedOptionInfo }
     * 
     * 
     */
    public List<GroupedOptionInfo> getGroupedOptionInfo() {
        if (groupedOptionInfo == null) {
            groupedOptionInfo = new ArrayList<GroupedOptionInfo>();
        }
        return this.groupedOptionInfo;
    }

    /**
     * Gets the value of the optionalServiceRules property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the optionalServiceRules property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOptionalServiceRules().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceRuleType }
     * 
     * 
     */
    public List<ServiceRuleType> getOptionalServiceRules() {
        if (optionalServiceRules == null) {
            optionalServiceRules = new ArrayList<ServiceRuleType>();
        }
        return this.optionalServiceRules;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}TaxInfo" maxOccurs="999" minOccurs="0"/&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FeeInfo" maxOccurs="999" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attGroup ref="{http://www.travelport.com/schema/common_v43_0}attrPrices"/&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "taxInfo",
        "feeInfo"
    })
    public static class OptionalServicesTotal {

        @XmlElement(name = "TaxInfo")
        protected List<TypeTaxInfo> taxInfo;
        @XmlElement(name = "FeeInfo")
        protected List<TypeFeeInfo> feeInfo;
        @XmlAttribute(name = "TotalPrice")
        protected String totalPrice;
        @XmlAttribute(name = "BasePrice")
        protected String basePrice;
        @XmlAttribute(name = "ApproximateTotalPrice")
        protected String approximateTotalPrice;
        @XmlAttribute(name = "ApproximateBasePrice")
        protected String approximateBasePrice;
        @XmlAttribute(name = "EquivalentBasePrice")
        protected String equivalentBasePrice;
        @XmlAttribute(name = "Taxes")
        protected String taxes;
        @XmlAttribute(name = "Fees")
        protected String fees;
        @XmlAttribute(name = "Services")
        protected String services;
        @XmlAttribute(name = "ApproximateTaxes")
        protected String approximateTaxes;
        @XmlAttribute(name = "ApproximateFees")
        protected String approximateFees;

        /**
         * Gets the value of the taxInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the taxInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTaxInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TypeTaxInfo }
         * 
         * 
         */
        public List<TypeTaxInfo> getTaxInfo() {
            if (taxInfo == null) {
                taxInfo = new ArrayList<TypeTaxInfo>();
            }
            return this.taxInfo;
        }

        /**
         * Gets the value of the feeInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the feeInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFeeInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TypeFeeInfo }
         * 
         * 
         */
        public List<TypeFeeInfo> getFeeInfo() {
            if (feeInfo == null) {
                feeInfo = new ArrayList<TypeFeeInfo>();
            }
            return this.feeInfo;
        }

        /**
         * Obtiene el valor de la propiedad totalPrice.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTotalPrice() {
            return totalPrice;
        }

        /**
         * Define el valor de la propiedad totalPrice.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTotalPrice(String value) {
            this.totalPrice = value;
        }

        /**
         * Obtiene el valor de la propiedad basePrice.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBasePrice() {
            return basePrice;
        }

        /**
         * Define el valor de la propiedad basePrice.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBasePrice(String value) {
            this.basePrice = value;
        }

        /**
         * Obtiene el valor de la propiedad approximateTotalPrice.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getApproximateTotalPrice() {
            return approximateTotalPrice;
        }

        /**
         * Define el valor de la propiedad approximateTotalPrice.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setApproximateTotalPrice(String value) {
            this.approximateTotalPrice = value;
        }

        /**
         * Obtiene el valor de la propiedad approximateBasePrice.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getApproximateBasePrice() {
            return approximateBasePrice;
        }

        /**
         * Define el valor de la propiedad approximateBasePrice.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setApproximateBasePrice(String value) {
            this.approximateBasePrice = value;
        }

        /**
         * Obtiene el valor de la propiedad equivalentBasePrice.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEquivalentBasePrice() {
            return equivalentBasePrice;
        }

        /**
         * Define el valor de la propiedad equivalentBasePrice.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEquivalentBasePrice(String value) {
            this.equivalentBasePrice = value;
        }

        /**
         * Obtiene el valor de la propiedad taxes.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTaxes() {
            return taxes;
        }

        /**
         * Define el valor de la propiedad taxes.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTaxes(String value) {
            this.taxes = value;
        }

        /**
         * Obtiene el valor de la propiedad fees.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFees() {
            return fees;
        }

        /**
         * Define el valor de la propiedad fees.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFees(String value) {
            this.fees = value;
        }

        /**
         * Obtiene el valor de la propiedad services.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getServices() {
            return services;
        }

        /**
         * Define el valor de la propiedad services.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setServices(String value) {
            this.services = value;
        }

        /**
         * Obtiene el valor de la propiedad approximateTaxes.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getApproximateTaxes() {
            return approximateTaxes;
        }

        /**
         * Define el valor de la propiedad approximateTaxes.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setApproximateTaxes(String value) {
            this.approximateTaxes = value;
        }

        /**
         * Obtiene el valor de la propiedad approximateFees.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getApproximateFees() {
            return approximateFees;
        }

        /**
         * Define el valor de la propiedad approximateFees.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setApproximateFees(String value) {
            this.approximateFees = value;
        }

    }

}
