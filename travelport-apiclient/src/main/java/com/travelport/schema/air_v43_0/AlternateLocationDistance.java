
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.Distance;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Distance"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Key" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="SearchLocation" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *       &lt;attribute name="AlternateLocation" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "distance"
})
@XmlRootElement(name = "AlternateLocationDistance")
public class AlternateLocationDistance {

    @XmlElement(name = "Distance", namespace = "http://www.travelport.com/schema/common_v43_0", required = true)
    protected Distance distance;
    @XmlAttribute(name = "Key", required = true)
    protected String key;
    @XmlAttribute(name = "SearchLocation", required = true)
    protected String searchLocation;
    @XmlAttribute(name = "AlternateLocation", required = true)
    protected String alternateLocation;

    /**
     * Obtiene el valor de la propiedad distance.
     * 
     * @return
     *     possible object is
     *     {@link Distance }
     *     
     */
    public Distance getDistance() {
        return distance;
    }

    /**
     * Define el valor de la propiedad distance.
     * 
     * @param value
     *     allowed object is
     *     {@link Distance }
     *     
     */
    public void setDistance(Distance value) {
        this.distance = value;
    }

    /**
     * Obtiene el valor de la propiedad key.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Define el valor de la propiedad key.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Obtiene el valor de la propiedad searchLocation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchLocation() {
        return searchLocation;
    }

    /**
     * Define el valor de la propiedad searchLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchLocation(String value) {
        this.searchLocation = value;
    }

    /**
     * Obtiene el valor de la propiedad alternateLocation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternateLocation() {
        return alternateLocation;
    }

    /**
     * Define el valor de la propiedad alternateLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternateLocation(String value) {
        this.alternateLocation = value;
    }

}
