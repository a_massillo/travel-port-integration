
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Bulk ticketing modifier type.
 *             
 * 
 * <p>Clase Java para typeBulkTicketModifierType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="typeBulkTicketModifierType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="SuppressOnFareCalc" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "typeBulkTicketModifierType")
@XmlSeeAlso({
    com.travelport.schema.air_v43_0.TicketingModifiers.BulkTicket.class
})
public class TypeBulkTicketModifierType {

    @XmlAttribute(name = "SuppressOnFareCalc")
    protected Boolean suppressOnFareCalc;

    /**
     * Obtiene el valor de la propiedad suppressOnFareCalc.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSuppressOnFareCalc() {
        return suppressOnFareCalc;
    }

    /**
     * Define el valor de la propiedad suppressOnFareCalc.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSuppressOnFareCalc(Boolean value) {
        this.suppressOnFareCalc = value;
    }

}
