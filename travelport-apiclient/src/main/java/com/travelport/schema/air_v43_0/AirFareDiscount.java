
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="Percentage" type="{http://www.w3.org/2001/XMLSchema}double" /&gt;
 *       &lt;attribute name="Amount" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="DiscountMethod" type="{http://www.travelport.com/schema/air_v43_0}typeFareDiscount" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "AirFareDiscount")
public class AirFareDiscount {

    @XmlAttribute(name = "Percentage")
    protected Double percentage;
    @XmlAttribute(name = "Amount")
    protected String amount;
    @XmlAttribute(name = "DiscountMethod")
    protected TypeFareDiscount discountMethod;

    /**
     * Obtiene el valor de la propiedad percentage.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getPercentage() {
        return percentage;
    }

    /**
     * Define el valor de la propiedad percentage.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setPercentage(Double value) {
        this.percentage = value;
    }

    /**
     * Obtiene el valor de la propiedad amount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmount() {
        return amount;
    }

    /**
     * Define el valor de la propiedad amount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmount(String value) {
        this.amount = value;
    }

    /**
     * Obtiene el valor de la propiedad discountMethod.
     * 
     * @return
     *     possible object is
     *     {@link TypeFareDiscount }
     *     
     */
    public TypeFareDiscount getDiscountMethod() {
        return discountMethod;
    }

    /**
     * Define el valor de la propiedad discountMethod.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeFareDiscount }
     *     
     */
    public void setDiscountMethod(TypeFareDiscount value) {
        this.discountMethod = value;
    }

}
