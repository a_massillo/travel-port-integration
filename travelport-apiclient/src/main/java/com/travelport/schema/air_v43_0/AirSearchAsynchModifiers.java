
package com.travelport.schema.air_v43_0;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="InitialAsynchResult" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="MaxWait" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "initialAsynchResult"
})
@XmlRootElement(name = "AirSearchAsynchModifiers")
public class AirSearchAsynchModifiers {

    @XmlElement(name = "InitialAsynchResult")
    protected AirSearchAsynchModifiers.InitialAsynchResult initialAsynchResult;

    /**
     * Obtiene el valor de la propiedad initialAsynchResult.
     * 
     * @return
     *     possible object is
     *     {@link AirSearchAsynchModifiers.InitialAsynchResult }
     *     
     */
    public AirSearchAsynchModifiers.InitialAsynchResult getInitialAsynchResult() {
        return initialAsynchResult;
    }

    /**
     * Define el valor de la propiedad initialAsynchResult.
     * 
     * @param value
     *     allowed object is
     *     {@link AirSearchAsynchModifiers.InitialAsynchResult }
     *     
     */
    public void setInitialAsynchResult(AirSearchAsynchModifiers.InitialAsynchResult value) {
        this.initialAsynchResult = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attribute name="MaxWait" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class InitialAsynchResult {

        @XmlAttribute(name = "MaxWait")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger maxWait;

        /**
         * Obtiene el valor de la propiedad maxWait.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMaxWait() {
            return maxWait;
        }

        /**
         * Define el valor de la propiedad maxWait.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMaxWait(BigInteger value) {
            this.maxWait = value;
        }

    }

}
