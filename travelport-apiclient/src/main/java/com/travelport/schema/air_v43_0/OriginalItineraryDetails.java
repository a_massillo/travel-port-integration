
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.travelport.schema.common_v43_0.TypeItineraryCode;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="ItineraryType" type="{http://www.travelport.com/schema/common_v43_0}typeItineraryCode" /&gt;
 *       &lt;attribute name="BulkTicket" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="TicketingPCC" type="{http://www.travelport.com/schema/common_v43_0}typePCC" /&gt;
 *       &lt;attribute name="TicketingIATA" type="{http://www.travelport.com/schema/common_v43_0}typeIATA" /&gt;
 *       &lt;attribute name="TicketingCountry" type="{http://www.travelport.com/schema/common_v43_0}typeCountry" /&gt;
 *       &lt;attribute name="TourCode" type="{http://www.travelport.com/schema/air_v43_0}typeTourCode" /&gt;
 *       &lt;attribute name="TicketingDate" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "OriginalItineraryDetails")
public class OriginalItineraryDetails {

    @XmlAttribute(name = "ItineraryType")
    protected TypeItineraryCode itineraryType;
    @XmlAttribute(name = "BulkTicket")
    protected Boolean bulkTicket;
    @XmlAttribute(name = "TicketingPCC")
    protected String ticketingPCC;
    @XmlAttribute(name = "TicketingIATA")
    protected String ticketingIATA;
    @XmlAttribute(name = "TicketingCountry")
    protected String ticketingCountry;
    @XmlAttribute(name = "TourCode")
    protected String tourCode;
    @XmlAttribute(name = "TicketingDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar ticketingDate;

    /**
     * Obtiene el valor de la propiedad itineraryType.
     * 
     * @return
     *     possible object is
     *     {@link TypeItineraryCode }
     *     
     */
    public TypeItineraryCode getItineraryType() {
        return itineraryType;
    }

    /**
     * Define el valor de la propiedad itineraryType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeItineraryCode }
     *     
     */
    public void setItineraryType(TypeItineraryCode value) {
        this.itineraryType = value;
    }

    /**
     * Obtiene el valor de la propiedad bulkTicket.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isBulkTicket() {
        if (bulkTicket == null) {
            return false;
        } else {
            return bulkTicket;
        }
    }

    /**
     * Define el valor de la propiedad bulkTicket.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBulkTicket(Boolean value) {
        this.bulkTicket = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketingPCC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketingPCC() {
        return ticketingPCC;
    }

    /**
     * Define el valor de la propiedad ticketingPCC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketingPCC(String value) {
        this.ticketingPCC = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketingIATA.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketingIATA() {
        return ticketingIATA;
    }

    /**
     * Define el valor de la propiedad ticketingIATA.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketingIATA(String value) {
        this.ticketingIATA = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketingCountry.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketingCountry() {
        return ticketingCountry;
    }

    /**
     * Define el valor de la propiedad ticketingCountry.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketingCountry(String value) {
        this.ticketingCountry = value;
    }

    /**
     * Obtiene el valor de la propiedad tourCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTourCode() {
        return tourCode;
    }

    /**
     * Define el valor de la propiedad tourCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTourCode(String value) {
        this.tourCode = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketingDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTicketingDate() {
        return ticketingDate;
    }

    /**
     * Define el valor de la propiedad ticketingDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTicketingDate(XMLGregorianCalendar value) {
        this.ticketingDate = value;
    }

}
