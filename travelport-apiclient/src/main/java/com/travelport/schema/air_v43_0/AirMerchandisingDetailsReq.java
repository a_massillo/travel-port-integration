
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseReq;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseReq"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}MerchandisingDetails"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}OptionalServiceModifiers"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}MerchandisingAvailabilityDetails"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "merchandisingDetails",
    "optionalServiceModifiers",
    "merchandisingAvailabilityDetails"
})
@XmlRootElement(name = "AirMerchandisingDetailsReq")
public class AirMerchandisingDetailsReq
    extends BaseReq
{

    @XmlElement(name = "MerchandisingDetails")
    protected MerchandisingDetails merchandisingDetails;
    @XmlElement(name = "OptionalServiceModifiers")
    protected OptionalServiceModifiers optionalServiceModifiers;
    @XmlElement(name = "MerchandisingAvailabilityDetails")
    protected MerchandisingAvailabilityDetails merchandisingAvailabilityDetails;

    /**
     * Obtiene el valor de la propiedad merchandisingDetails.
     * 
     * @return
     *     possible object is
     *     {@link MerchandisingDetails }
     *     
     */
    public MerchandisingDetails getMerchandisingDetails() {
        return merchandisingDetails;
    }

    /**
     * Define el valor de la propiedad merchandisingDetails.
     * 
     * @param value
     *     allowed object is
     *     {@link MerchandisingDetails }
     *     
     */
    public void setMerchandisingDetails(MerchandisingDetails value) {
        this.merchandisingDetails = value;
    }

    /**
     * Obtiene el valor de la propiedad optionalServiceModifiers.
     * 
     * @return
     *     possible object is
     *     {@link OptionalServiceModifiers }
     *     
     */
    public OptionalServiceModifiers getOptionalServiceModifiers() {
        return optionalServiceModifiers;
    }

    /**
     * Define el valor de la propiedad optionalServiceModifiers.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionalServiceModifiers }
     *     
     */
    public void setOptionalServiceModifiers(OptionalServiceModifiers value) {
        this.optionalServiceModifiers = value;
    }

    /**
     * Obtiene el valor de la propiedad merchandisingAvailabilityDetails.
     * 
     * @return
     *     possible object is
     *     {@link MerchandisingAvailabilityDetails }
     *     
     */
    public MerchandisingAvailabilityDetails getMerchandisingAvailabilityDetails() {
        return merchandisingAvailabilityDetails;
    }

    /**
     * Define el valor de la propiedad merchandisingAvailabilityDetails.
     * 
     * @param value
     *     allowed object is
     *     {@link MerchandisingAvailabilityDetails }
     *     
     */
    public void setMerchandisingAvailabilityDetails(MerchandisingAvailabilityDetails value) {
        this.merchandisingAvailabilityDetails = value;
    }

}
