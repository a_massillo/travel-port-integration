
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}BackOfficeHandOff" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}Itinerary" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="IssueTicketOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="IssueElectronicTicket" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="FaxIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "backOfficeHandOff",
    "itinerary"
})
@XmlRootElement(name = "DocumentSelect")
public class DocumentSelect {

    @XmlElement(name = "BackOfficeHandOff")
    protected BackOfficeHandOff backOfficeHandOff;
    @XmlElement(name = "Itinerary")
    protected Itinerary itinerary;
    @XmlAttribute(name = "IssueTicketOnly")
    protected Boolean issueTicketOnly;
    @XmlAttribute(name = "IssueElectronicTicket")
    protected Boolean issueElectronicTicket;
    @XmlAttribute(name = "FaxIndicator")
    protected Boolean faxIndicator;

    /**
     * Obtiene el valor de la propiedad backOfficeHandOff.
     * 
     * @return
     *     possible object is
     *     {@link BackOfficeHandOff }
     *     
     */
    public BackOfficeHandOff getBackOfficeHandOff() {
        return backOfficeHandOff;
    }

    /**
     * Define el valor de la propiedad backOfficeHandOff.
     * 
     * @param value
     *     allowed object is
     *     {@link BackOfficeHandOff }
     *     
     */
    public void setBackOfficeHandOff(BackOfficeHandOff value) {
        this.backOfficeHandOff = value;
    }

    /**
     * Obtiene el valor de la propiedad itinerary.
     * 
     * @return
     *     possible object is
     *     {@link Itinerary }
     *     
     */
    public Itinerary getItinerary() {
        return itinerary;
    }

    /**
     * Define el valor de la propiedad itinerary.
     * 
     * @param value
     *     allowed object is
     *     {@link Itinerary }
     *     
     */
    public void setItinerary(Itinerary value) {
        this.itinerary = value;
    }

    /**
     * Obtiene el valor de la propiedad issueTicketOnly.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIssueTicketOnly() {
        return issueTicketOnly;
    }

    /**
     * Define el valor de la propiedad issueTicketOnly.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIssueTicketOnly(Boolean value) {
        this.issueTicketOnly = value;
    }

    /**
     * Obtiene el valor de la propiedad issueElectronicTicket.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIssueElectronicTicket() {
        return issueElectronicTicket;
    }

    /**
     * Define el valor de la propiedad issueElectronicTicket.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIssueElectronicTicket(Boolean value) {
        this.issueElectronicTicket = value;
    }

    /**
     * Obtiene el valor de la propiedad faxIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFaxIndicator() {
        return faxIndicator;
    }

    /**
     * Define el valor de la propiedad faxIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFaxIndicator(Boolean value) {
        this.faxIndicator = value;
    }

}
