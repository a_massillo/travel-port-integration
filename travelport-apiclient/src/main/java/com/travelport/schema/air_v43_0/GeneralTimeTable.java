
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.travelport.schema.common_v43_0.TypeLocation;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DaysOfOperation" type="{http://www.travelport.com/schema/air_v43_0}typeDaysOfOperation" minOccurs="0"/&gt;
 *         &lt;element name="FlightOrigin" type="{http://www.travelport.com/schema/common_v43_0}typeLocation"/&gt;
 *         &lt;element name="FlightDestination" type="{http://www.travelport.com/schema/common_v43_0}typeLocation"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}CarrierList" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="StartDate" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="EndDate" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="StartTime" type="{http://www.w3.org/2001/XMLSchema}time" /&gt;
 *       &lt;attribute name="EndTime" type="{http://www.w3.org/2001/XMLSchema}time" /&gt;
 *       &lt;attribute name="IncludeConnection" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "daysOfOperation",
    "flightOrigin",
    "flightDestination",
    "carrierList"
})
@XmlRootElement(name = "GeneralTimeTable")
public class GeneralTimeTable {

    @XmlElement(name = "DaysOfOperation")
    protected TypeDaysOfOperation daysOfOperation;
    @XmlElement(name = "FlightOrigin", required = true)
    protected TypeLocation flightOrigin;
    @XmlElement(name = "FlightDestination", required = true)
    protected TypeLocation flightDestination;
    @XmlElement(name = "CarrierList")
    protected CarrierList carrierList;
    @XmlAttribute(name = "StartDate", required = true)
    protected String startDate;
    @XmlAttribute(name = "EndDate")
    protected String endDate;
    @XmlAttribute(name = "StartTime")
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar startTime;
    @XmlAttribute(name = "EndTime")
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar endTime;
    @XmlAttribute(name = "IncludeConnection")
    protected Boolean includeConnection;

    /**
     * Obtiene el valor de la propiedad daysOfOperation.
     * 
     * @return
     *     possible object is
     *     {@link TypeDaysOfOperation }
     *     
     */
    public TypeDaysOfOperation getDaysOfOperation() {
        return daysOfOperation;
    }

    /**
     * Define el valor de la propiedad daysOfOperation.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeDaysOfOperation }
     *     
     */
    public void setDaysOfOperation(TypeDaysOfOperation value) {
        this.daysOfOperation = value;
    }

    /**
     * Obtiene el valor de la propiedad flightOrigin.
     * 
     * @return
     *     possible object is
     *     {@link TypeLocation }
     *     
     */
    public TypeLocation getFlightOrigin() {
        return flightOrigin;
    }

    /**
     * Define el valor de la propiedad flightOrigin.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeLocation }
     *     
     */
    public void setFlightOrigin(TypeLocation value) {
        this.flightOrigin = value;
    }

    /**
     * Obtiene el valor de la propiedad flightDestination.
     * 
     * @return
     *     possible object is
     *     {@link TypeLocation }
     *     
     */
    public TypeLocation getFlightDestination() {
        return flightDestination;
    }

    /**
     * Define el valor de la propiedad flightDestination.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeLocation }
     *     
     */
    public void setFlightDestination(TypeLocation value) {
        this.flightDestination = value;
    }

    /**
     * Obtiene el valor de la propiedad carrierList.
     * 
     * @return
     *     possible object is
     *     {@link CarrierList }
     *     
     */
    public CarrierList getCarrierList() {
        return carrierList;
    }

    /**
     * Define el valor de la propiedad carrierList.
     * 
     * @param value
     *     allowed object is
     *     {@link CarrierList }
     *     
     */
    public void setCarrierList(CarrierList value) {
        this.carrierList = value;
    }

    /**
     * Obtiene el valor de la propiedad startDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * Define el valor de la propiedad startDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartDate(String value) {
        this.startDate = value;
    }

    /**
     * Obtiene el valor de la propiedad endDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * Define el valor de la propiedad endDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndDate(String value) {
        this.endDate = value;
    }

    /**
     * Obtiene el valor de la propiedad startTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartTime() {
        return startTime;
    }

    /**
     * Define el valor de la propiedad startTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartTime(XMLGregorianCalendar value) {
        this.startTime = value;
    }

    /**
     * Obtiene el valor de la propiedad endTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndTime() {
        return endTime;
    }

    /**
     * Define el valor de la propiedad endTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndTime(XMLGregorianCalendar value) {
        this.endTime = value;
    }

    /**
     * Obtiene el valor de la propiedad includeConnection.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeConnection() {
        return includeConnection;
    }

    /**
     * Define el valor de la propiedad includeConnection.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeConnection(Boolean value) {
        this.includeConnection = value;
    }

}
