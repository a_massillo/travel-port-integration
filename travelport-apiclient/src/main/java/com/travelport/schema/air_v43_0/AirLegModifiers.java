
package com.travelport.schema.air_v43_0;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.TypeLocation;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}PermittedCabins" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}PreferredCabins" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}PermittedCarriers" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}ProhibitedCarriers" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}PreferredCarriers" minOccurs="0"/&gt;
 *         &lt;element name="PermittedConnectionPoints" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/common_v43_0}ConnectionPoint" maxOccurs="999"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ProhibitedConnectionPoints" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/common_v43_0}ConnectionPoint" maxOccurs="999"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="PreferredConnectionPoints" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/common_v43_0}ConnectionPoint" maxOccurs="99"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="PermittedBookingCodes" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/air_v43_0}BookingCode" maxOccurs="999"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}PreferredBookingCodes" minOccurs="0"/&gt;
 *         &lt;element name="PreferredAlliances" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/air_v43_0}Alliance" maxOccurs="999"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ProhibitedBookingCodes" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/air_v43_0}BookingCode" maxOccurs="999"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="DisfavoredAlliances" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/air_v43_0}Alliance" maxOccurs="999"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FlightType" minOccurs="0"/&gt;
 *         &lt;element name="AnchorFlightData" type="{http://www.travelport.com/schema/air_v43_0}typeAnchorFlightData" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ProhibitOvernightLayovers" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="MaxConnectionTime" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="ReturnFirstAvailableOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="AllowDirectAccess" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="ProhibitMultiAirportConnection" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="PreferNonStop" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="OrderBy"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;enumeration value="JourneyTime"/&gt;
 *             &lt;enumeration value="DepartureTime"/&gt;
 *             &lt;enumeration value="ArrivalTime"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="MaxJourneyTime" type="{http://www.travelport.com/schema/air_v43_0}typeMaxJourneyTime" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "permittedCabins",
    "preferredCabins",
    "permittedCarriers",
    "prohibitedCarriers",
    "preferredCarriers",
    "permittedConnectionPoints",
    "prohibitedConnectionPoints",
    "preferredConnectionPoints",
    "permittedBookingCodes",
    "preferredBookingCodes",
    "preferredAlliances",
    "prohibitedBookingCodes",
    "disfavoredAlliances",
    "flightType",
    "anchorFlightData"
})
@XmlRootElement(name = "AirLegModifiers")
public class AirLegModifiers {

    @XmlElement(name = "PermittedCabins")
    protected PermittedCabins permittedCabins;
    @XmlElement(name = "PreferredCabins")
    protected PreferredCabins preferredCabins;
    @XmlElement(name = "PermittedCarriers")
    protected PermittedCarriers permittedCarriers;
    @XmlElement(name = "ProhibitedCarriers")
    protected ProhibitedCarriers prohibitedCarriers;
    @XmlElement(name = "PreferredCarriers")
    protected PreferredCarriers preferredCarriers;
    @XmlElement(name = "PermittedConnectionPoints")
    protected AirLegModifiers.PermittedConnectionPoints permittedConnectionPoints;
    @XmlElement(name = "ProhibitedConnectionPoints")
    protected AirLegModifiers.ProhibitedConnectionPoints prohibitedConnectionPoints;
    @XmlElement(name = "PreferredConnectionPoints")
    protected AirLegModifiers.PreferredConnectionPoints preferredConnectionPoints;
    @XmlElement(name = "PermittedBookingCodes")
    protected AirLegModifiers.PermittedBookingCodes permittedBookingCodes;
    @XmlElement(name = "PreferredBookingCodes")
    protected PreferredBookingCodes preferredBookingCodes;
    @XmlElement(name = "PreferredAlliances")
    protected AirLegModifiers.PreferredAlliances preferredAlliances;
    @XmlElement(name = "ProhibitedBookingCodes")
    protected AirLegModifiers.ProhibitedBookingCodes prohibitedBookingCodes;
    @XmlElement(name = "DisfavoredAlliances")
    protected AirLegModifiers.DisfavoredAlliances disfavoredAlliances;
    @XmlElement(name = "FlightType")
    protected FlightType flightType;
    @XmlElement(name = "AnchorFlightData")
    protected TypeAnchorFlightData anchorFlightData;
    @XmlAttribute(name = "ProhibitOvernightLayovers")
    protected Boolean prohibitOvernightLayovers;
    @XmlAttribute(name = "MaxConnectionTime")
    protected BigInteger maxConnectionTime;
    @XmlAttribute(name = "ReturnFirstAvailableOnly")
    protected Boolean returnFirstAvailableOnly;
    @XmlAttribute(name = "AllowDirectAccess")
    protected Boolean allowDirectAccess;
    @XmlAttribute(name = "ProhibitMultiAirportConnection")
    protected Boolean prohibitMultiAirportConnection;
    @XmlAttribute(name = "PreferNonStop")
    protected Boolean preferNonStop;
    @XmlAttribute(name = "OrderBy")
    protected String orderBy;
    @XmlAttribute(name = "MaxJourneyTime")
    protected Integer maxJourneyTime;

    /**
     * Obtiene el valor de la propiedad permittedCabins.
     * 
     * @return
     *     possible object is
     *     {@link PermittedCabins }
     *     
     */
    public PermittedCabins getPermittedCabins() {
        return permittedCabins;
    }

    /**
     * Define el valor de la propiedad permittedCabins.
     * 
     * @param value
     *     allowed object is
     *     {@link PermittedCabins }
     *     
     */
    public void setPermittedCabins(PermittedCabins value) {
        this.permittedCabins = value;
    }

    /**
     * Obtiene el valor de la propiedad preferredCabins.
     * 
     * @return
     *     possible object is
     *     {@link PreferredCabins }
     *     
     */
    public PreferredCabins getPreferredCabins() {
        return preferredCabins;
    }

    /**
     * Define el valor de la propiedad preferredCabins.
     * 
     * @param value
     *     allowed object is
     *     {@link PreferredCabins }
     *     
     */
    public void setPreferredCabins(PreferredCabins value) {
        this.preferredCabins = value;
    }

    /**
     * Obtiene el valor de la propiedad permittedCarriers.
     * 
     * @return
     *     possible object is
     *     {@link PermittedCarriers }
     *     
     */
    public PermittedCarriers getPermittedCarriers() {
        return permittedCarriers;
    }

    /**
     * Define el valor de la propiedad permittedCarriers.
     * 
     * @param value
     *     allowed object is
     *     {@link PermittedCarriers }
     *     
     */
    public void setPermittedCarriers(PermittedCarriers value) {
        this.permittedCarriers = value;
    }

    /**
     * Obtiene el valor de la propiedad prohibitedCarriers.
     * 
     * @return
     *     possible object is
     *     {@link ProhibitedCarriers }
     *     
     */
    public ProhibitedCarriers getProhibitedCarriers() {
        return prohibitedCarriers;
    }

    /**
     * Define el valor de la propiedad prohibitedCarriers.
     * 
     * @param value
     *     allowed object is
     *     {@link ProhibitedCarriers }
     *     
     */
    public void setProhibitedCarriers(ProhibitedCarriers value) {
        this.prohibitedCarriers = value;
    }

    /**
     * Obtiene el valor de la propiedad preferredCarriers.
     * 
     * @return
     *     possible object is
     *     {@link PreferredCarriers }
     *     
     */
    public PreferredCarriers getPreferredCarriers() {
        return preferredCarriers;
    }

    /**
     * Define el valor de la propiedad preferredCarriers.
     * 
     * @param value
     *     allowed object is
     *     {@link PreferredCarriers }
     *     
     */
    public void setPreferredCarriers(PreferredCarriers value) {
        this.preferredCarriers = value;
    }

    /**
     * Obtiene el valor de la propiedad permittedConnectionPoints.
     * 
     * @return
     *     possible object is
     *     {@link AirLegModifiers.PermittedConnectionPoints }
     *     
     */
    public AirLegModifiers.PermittedConnectionPoints getPermittedConnectionPoints() {
        return permittedConnectionPoints;
    }

    /**
     * Define el valor de la propiedad permittedConnectionPoints.
     * 
     * @param value
     *     allowed object is
     *     {@link AirLegModifiers.PermittedConnectionPoints }
     *     
     */
    public void setPermittedConnectionPoints(AirLegModifiers.PermittedConnectionPoints value) {
        this.permittedConnectionPoints = value;
    }

    /**
     * Obtiene el valor de la propiedad prohibitedConnectionPoints.
     * 
     * @return
     *     possible object is
     *     {@link AirLegModifiers.ProhibitedConnectionPoints }
     *     
     */
    public AirLegModifiers.ProhibitedConnectionPoints getProhibitedConnectionPoints() {
        return prohibitedConnectionPoints;
    }

    /**
     * Define el valor de la propiedad prohibitedConnectionPoints.
     * 
     * @param value
     *     allowed object is
     *     {@link AirLegModifiers.ProhibitedConnectionPoints }
     *     
     */
    public void setProhibitedConnectionPoints(AirLegModifiers.ProhibitedConnectionPoints value) {
        this.prohibitedConnectionPoints = value;
    }

    /**
     * Obtiene el valor de la propiedad preferredConnectionPoints.
     * 
     * @return
     *     possible object is
     *     {@link AirLegModifiers.PreferredConnectionPoints }
     *     
     */
    public AirLegModifiers.PreferredConnectionPoints getPreferredConnectionPoints() {
        return preferredConnectionPoints;
    }

    /**
     * Define el valor de la propiedad preferredConnectionPoints.
     * 
     * @param value
     *     allowed object is
     *     {@link AirLegModifiers.PreferredConnectionPoints }
     *     
     */
    public void setPreferredConnectionPoints(AirLegModifiers.PreferredConnectionPoints value) {
        this.preferredConnectionPoints = value;
    }

    /**
     * Obtiene el valor de la propiedad permittedBookingCodes.
     * 
     * @return
     *     possible object is
     *     {@link AirLegModifiers.PermittedBookingCodes }
     *     
     */
    public AirLegModifiers.PermittedBookingCodes getPermittedBookingCodes() {
        return permittedBookingCodes;
    }

    /**
     * Define el valor de la propiedad permittedBookingCodes.
     * 
     * @param value
     *     allowed object is
     *     {@link AirLegModifiers.PermittedBookingCodes }
     *     
     */
    public void setPermittedBookingCodes(AirLegModifiers.PermittedBookingCodes value) {
        this.permittedBookingCodes = value;
    }

    /**
     * Obtiene el valor de la propiedad preferredBookingCodes.
     * 
     * @return
     *     possible object is
     *     {@link PreferredBookingCodes }
     *     
     */
    public PreferredBookingCodes getPreferredBookingCodes() {
        return preferredBookingCodes;
    }

    /**
     * Define el valor de la propiedad preferredBookingCodes.
     * 
     * @param value
     *     allowed object is
     *     {@link PreferredBookingCodes }
     *     
     */
    public void setPreferredBookingCodes(PreferredBookingCodes value) {
        this.preferredBookingCodes = value;
    }

    /**
     * Obtiene el valor de la propiedad preferredAlliances.
     * 
     * @return
     *     possible object is
     *     {@link AirLegModifiers.PreferredAlliances }
     *     
     */
    public AirLegModifiers.PreferredAlliances getPreferredAlliances() {
        return preferredAlliances;
    }

    /**
     * Define el valor de la propiedad preferredAlliances.
     * 
     * @param value
     *     allowed object is
     *     {@link AirLegModifiers.PreferredAlliances }
     *     
     */
    public void setPreferredAlliances(AirLegModifiers.PreferredAlliances value) {
        this.preferredAlliances = value;
    }

    /**
     * Obtiene el valor de la propiedad prohibitedBookingCodes.
     * 
     * @return
     *     possible object is
     *     {@link AirLegModifiers.ProhibitedBookingCodes }
     *     
     */
    public AirLegModifiers.ProhibitedBookingCodes getProhibitedBookingCodes() {
        return prohibitedBookingCodes;
    }

    /**
     * Define el valor de la propiedad prohibitedBookingCodes.
     * 
     * @param value
     *     allowed object is
     *     {@link AirLegModifiers.ProhibitedBookingCodes }
     *     
     */
    public void setProhibitedBookingCodes(AirLegModifiers.ProhibitedBookingCodes value) {
        this.prohibitedBookingCodes = value;
    }

    /**
     * Obtiene el valor de la propiedad disfavoredAlliances.
     * 
     * @return
     *     possible object is
     *     {@link AirLegModifiers.DisfavoredAlliances }
     *     
     */
    public AirLegModifiers.DisfavoredAlliances getDisfavoredAlliances() {
        return disfavoredAlliances;
    }

    /**
     * Define el valor de la propiedad disfavoredAlliances.
     * 
     * @param value
     *     allowed object is
     *     {@link AirLegModifiers.DisfavoredAlliances }
     *     
     */
    public void setDisfavoredAlliances(AirLegModifiers.DisfavoredAlliances value) {
        this.disfavoredAlliances = value;
    }

    /**
     * Obtiene el valor de la propiedad flightType.
     * 
     * @return
     *     possible object is
     *     {@link FlightType }
     *     
     */
    public FlightType getFlightType() {
        return flightType;
    }

    /**
     * Define el valor de la propiedad flightType.
     * 
     * @param value
     *     allowed object is
     *     {@link FlightType }
     *     
     */
    public void setFlightType(FlightType value) {
        this.flightType = value;
    }

    /**
     * Obtiene el valor de la propiedad anchorFlightData.
     * 
     * @return
     *     possible object is
     *     {@link TypeAnchorFlightData }
     *     
     */
    public TypeAnchorFlightData getAnchorFlightData() {
        return anchorFlightData;
    }

    /**
     * Define el valor de la propiedad anchorFlightData.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeAnchorFlightData }
     *     
     */
    public void setAnchorFlightData(TypeAnchorFlightData value) {
        this.anchorFlightData = value;
    }

    /**
     * Obtiene el valor de la propiedad prohibitOvernightLayovers.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isProhibitOvernightLayovers() {
        if (prohibitOvernightLayovers == null) {
            return false;
        } else {
            return prohibitOvernightLayovers;
        }
    }

    /**
     * Define el valor de la propiedad prohibitOvernightLayovers.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProhibitOvernightLayovers(Boolean value) {
        this.prohibitOvernightLayovers = value;
    }

    /**
     * Obtiene el valor de la propiedad maxConnectionTime.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxConnectionTime() {
        return maxConnectionTime;
    }

    /**
     * Define el valor de la propiedad maxConnectionTime.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxConnectionTime(BigInteger value) {
        this.maxConnectionTime = value;
    }

    /**
     * Obtiene el valor de la propiedad returnFirstAvailableOnly.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReturnFirstAvailableOnly() {
        return returnFirstAvailableOnly;
    }

    /**
     * Define el valor de la propiedad returnFirstAvailableOnly.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReturnFirstAvailableOnly(Boolean value) {
        this.returnFirstAvailableOnly = value;
    }

    /**
     * Obtiene el valor de la propiedad allowDirectAccess.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isAllowDirectAccess() {
        if (allowDirectAccess == null) {
            return false;
        } else {
            return allowDirectAccess;
        }
    }

    /**
     * Define el valor de la propiedad allowDirectAccess.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowDirectAccess(Boolean value) {
        this.allowDirectAccess = value;
    }

    /**
     * Obtiene el valor de la propiedad prohibitMultiAirportConnection.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isProhibitMultiAirportConnection() {
        return prohibitMultiAirportConnection;
    }

    /**
     * Define el valor de la propiedad prohibitMultiAirportConnection.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProhibitMultiAirportConnection(Boolean value) {
        this.prohibitMultiAirportConnection = value;
    }

    /**
     * Obtiene el valor de la propiedad preferNonStop.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isPreferNonStop() {
        if (preferNonStop == null) {
            return false;
        } else {
            return preferNonStop;
        }
    }

    /**
     * Define el valor de la propiedad preferNonStop.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPreferNonStop(Boolean value) {
        this.preferNonStop = value;
    }

    /**
     * Obtiene el valor de la propiedad orderBy.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderBy() {
        return orderBy;
    }

    /**
     * Define el valor de la propiedad orderBy.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderBy(String value) {
        this.orderBy = value;
    }

    /**
     * Obtiene el valor de la propiedad maxJourneyTime.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxJourneyTime() {
        return maxJourneyTime;
    }

    /**
     * Define el valor de la propiedad maxJourneyTime.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxJourneyTime(Integer value) {
        this.maxJourneyTime = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}Alliance" maxOccurs="999"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "alliance"
    })
    public static class DisfavoredAlliances {

        @XmlElement(name = "Alliance", required = true)
        protected List<Alliance> alliance;

        /**
         * Gets the value of the alliance property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the alliance property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAlliance().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Alliance }
         * 
         * 
         */
        public List<Alliance> getAlliance() {
            if (alliance == null) {
                alliance = new ArrayList<Alliance>();
            }
            return this.alliance;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}BookingCode" maxOccurs="999"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "bookingCode"
    })
    public static class PermittedBookingCodes {

        @XmlElement(name = "BookingCode", required = true)
        protected List<BookingCode> bookingCode;

        /**
         * Gets the value of the bookingCode property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the bookingCode property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getBookingCode().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BookingCode }
         * 
         * 
         */
        public List<BookingCode> getBookingCode() {
            if (bookingCode == null) {
                bookingCode = new ArrayList<BookingCode>();
            }
            return this.bookingCode;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}ConnectionPoint" maxOccurs="999"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "connectionPoint"
    })
    public static class PermittedConnectionPoints {

        @XmlElement(name = "ConnectionPoint", namespace = "http://www.travelport.com/schema/common_v43_0", required = true)
        protected List<TypeLocation> connectionPoint;

        /**
         * Gets the value of the connectionPoint property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the connectionPoint property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getConnectionPoint().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TypeLocation }
         * 
         * 
         */
        public List<TypeLocation> getConnectionPoint() {
            if (connectionPoint == null) {
                connectionPoint = new ArrayList<TypeLocation>();
            }
            return this.connectionPoint;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}Alliance" maxOccurs="999"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "alliance"
    })
    public static class PreferredAlliances {

        @XmlElement(name = "Alliance", required = true)
        protected List<Alliance> alliance;

        /**
         * Gets the value of the alliance property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the alliance property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAlliance().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Alliance }
         * 
         * 
         */
        public List<Alliance> getAlliance() {
            if (alliance == null) {
                alliance = new ArrayList<Alliance>();
            }
            return this.alliance;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}ConnectionPoint" maxOccurs="99"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "connectionPoint"
    })
    public static class PreferredConnectionPoints {

        @XmlElement(name = "ConnectionPoint", namespace = "http://www.travelport.com/schema/common_v43_0", required = true)
        protected List<TypeLocation> connectionPoint;

        /**
         * Gets the value of the connectionPoint property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the connectionPoint property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getConnectionPoint().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TypeLocation }
         * 
         * 
         */
        public List<TypeLocation> getConnectionPoint() {
            if (connectionPoint == null) {
                connectionPoint = new ArrayList<TypeLocation>();
            }
            return this.connectionPoint;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}BookingCode" maxOccurs="999"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "bookingCode"
    })
    public static class ProhibitedBookingCodes {

        @XmlElement(name = "BookingCode", required = true)
        protected List<BookingCode> bookingCode;

        /**
         * Gets the value of the bookingCode property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the bookingCode property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getBookingCode().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BookingCode }
         * 
         * 
         */
        public List<BookingCode> getBookingCode() {
            if (bookingCode == null) {
                bookingCode = new ArrayList<BookingCode>();
            }
            return this.bookingCode;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}ConnectionPoint" maxOccurs="999"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "connectionPoint"
    })
    public static class ProhibitedConnectionPoints {

        @XmlElement(name = "ConnectionPoint", namespace = "http://www.travelport.com/schema/common_v43_0", required = true)
        protected List<TypeLocation> connectionPoint;

        /**
         * Gets the value of the connectionPoint property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the connectionPoint property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getConnectionPoint().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TypeLocation }
         * 
         * 
         */
        public List<TypeLocation> getConnectionPoint() {
            if (connectionPoint == null) {
                connectionPoint = new ArrayList<TypeLocation>();
            }
            return this.connectionPoint;
        }

    }

}
