
package com.travelport.schema.air_v43_0;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DaysOfOperation" type="{http://www.travelport.com/schema/air_v43_0}typeDaysOfOperation" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}Connection" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Key" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="VendorCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="FlightNumber" type="{http://www.travelport.com/schema/common_v43_0}typeFlightNumber" /&gt;
 *       &lt;attribute name="Origin" type="{http://www.travelport.com/schema/common_v43_0}typeAirport" /&gt;
 *       &lt;attribute name="Destination" type="{http://www.travelport.com/schema/common_v43_0}typeAirport" /&gt;
 *       &lt;attribute name="DepartureTime" type="{http://www.w3.org/2001/XMLSchema}time" /&gt;
 *       &lt;attribute name="ArrivalTime" type="{http://www.w3.org/2001/XMLSchema}time" /&gt;
 *       &lt;attribute name="StopCount" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="Equipment" type="{http://www.travelport.com/schema/air_v43_0}typeEquipment" /&gt;
 *       &lt;attribute name="ScheduleStartDate" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *       &lt;attribute name="ScheduleEndDate" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *       &lt;attribute name="DisplayOption" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="OnTimePerformance" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="DayChange" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="JourneyTime" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="FlightTime" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="StartTerminal" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="EndTerminal" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="FirstIntermediateStop" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *       &lt;attribute name="LastIntermediateStop" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *       &lt;attribute name="InsideAvailability"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="1"/&gt;
 *             &lt;maxLength value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="SecureSell"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="0"/&gt;
 *             &lt;maxLength value="2"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="AvailabilitySource" type="{http://www.travelport.com/schema/air_v43_0}typeAvailabilitySource" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "daysOfOperation",
    "connection"
})
@XmlRootElement(name = "FlightTimeDetail")
public class FlightTimeDetail {

    @XmlElement(name = "DaysOfOperation")
    protected TypeDaysOfOperation daysOfOperation;
    @XmlElement(name = "Connection")
    protected Connection connection;
    @XmlAttribute(name = "Key", required = true)
    protected String key;
    @XmlAttribute(name = "VendorCode")
    protected String vendorCode;
    @XmlAttribute(name = "FlightNumber")
    protected String flightNumber;
    @XmlAttribute(name = "Origin")
    protected String origin;
    @XmlAttribute(name = "Destination")
    protected String destination;
    @XmlAttribute(name = "DepartureTime")
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar departureTime;
    @XmlAttribute(name = "ArrivalTime")
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar arrivalTime;
    @XmlAttribute(name = "StopCount")
    protected BigInteger stopCount;
    @XmlAttribute(name = "Equipment")
    protected String equipment;
    @XmlAttribute(name = "ScheduleStartDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar scheduleStartDate;
    @XmlAttribute(name = "ScheduleEndDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar scheduleEndDate;
    @XmlAttribute(name = "DisplayOption")
    protected Boolean displayOption;
    @XmlAttribute(name = "OnTimePerformance")
    protected BigInteger onTimePerformance;
    @XmlAttribute(name = "DayChange")
    protected BigInteger dayChange;
    @XmlAttribute(name = "JourneyTime")
    protected BigInteger journeyTime;
    @XmlAttribute(name = "FlightTime")
    protected BigInteger flightTime;
    @XmlAttribute(name = "StartTerminal")
    protected String startTerminal;
    @XmlAttribute(name = "EndTerminal")
    protected String endTerminal;
    @XmlAttribute(name = "FirstIntermediateStop")
    protected String firstIntermediateStop;
    @XmlAttribute(name = "LastIntermediateStop")
    protected String lastIntermediateStop;
    @XmlAttribute(name = "InsideAvailability")
    protected String insideAvailability;
    @XmlAttribute(name = "SecureSell")
    protected String secureSell;
    @XmlAttribute(name = "AvailabilitySource")
    protected String availabilitySource;

    /**
     * Obtiene el valor de la propiedad daysOfOperation.
     * 
     * @return
     *     possible object is
     *     {@link TypeDaysOfOperation }
     *     
     */
    public TypeDaysOfOperation getDaysOfOperation() {
        return daysOfOperation;
    }

    /**
     * Define el valor de la propiedad daysOfOperation.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeDaysOfOperation }
     *     
     */
    public void setDaysOfOperation(TypeDaysOfOperation value) {
        this.daysOfOperation = value;
    }

    /**
     * Obtiene el valor de la propiedad connection.
     * 
     * @return
     *     possible object is
     *     {@link Connection }
     *     
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * Define el valor de la propiedad connection.
     * 
     * @param value
     *     allowed object is
     *     {@link Connection }
     *     
     */
    public void setConnection(Connection value) {
        this.connection = value;
    }

    /**
     * Obtiene el valor de la propiedad key.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Define el valor de la propiedad key.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Obtiene el valor de la propiedad vendorCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorCode() {
        return vendorCode;
    }

    /**
     * Define el valor de la propiedad vendorCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorCode(String value) {
        this.vendorCode = value;
    }

    /**
     * Obtiene el valor de la propiedad flightNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Define el valor de la propiedad flightNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightNumber(String value) {
        this.flightNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad origin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * Define el valor de la propiedad origin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigin(String value) {
        this.origin = value;
    }

    /**
     * Obtiene el valor de la propiedad destination.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestination() {
        return destination;
    }

    /**
     * Define el valor de la propiedad destination.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestination(String value) {
        this.destination = value;
    }

    /**
     * Obtiene el valor de la propiedad departureTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDepartureTime() {
        return departureTime;
    }

    /**
     * Define el valor de la propiedad departureTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDepartureTime(XMLGregorianCalendar value) {
        this.departureTime = value;
    }

    /**
     * Obtiene el valor de la propiedad arrivalTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getArrivalTime() {
        return arrivalTime;
    }

    /**
     * Define el valor de la propiedad arrivalTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setArrivalTime(XMLGregorianCalendar value) {
        this.arrivalTime = value;
    }

    /**
     * Obtiene el valor de la propiedad stopCount.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStopCount() {
        return stopCount;
    }

    /**
     * Define el valor de la propiedad stopCount.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStopCount(BigInteger value) {
        this.stopCount = value;
    }

    /**
     * Obtiene el valor de la propiedad equipment.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipment() {
        return equipment;
    }

    /**
     * Define el valor de la propiedad equipment.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipment(String value) {
        this.equipment = value;
    }

    /**
     * Obtiene el valor de la propiedad scheduleStartDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getScheduleStartDate() {
        return scheduleStartDate;
    }

    /**
     * Define el valor de la propiedad scheduleStartDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setScheduleStartDate(XMLGregorianCalendar value) {
        this.scheduleStartDate = value;
    }

    /**
     * Obtiene el valor de la propiedad scheduleEndDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getScheduleEndDate() {
        return scheduleEndDate;
    }

    /**
     * Define el valor de la propiedad scheduleEndDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setScheduleEndDate(XMLGregorianCalendar value) {
        this.scheduleEndDate = value;
    }

    /**
     * Obtiene el valor de la propiedad displayOption.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisplayOption() {
        return displayOption;
    }

    /**
     * Define el valor de la propiedad displayOption.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisplayOption(Boolean value) {
        this.displayOption = value;
    }

    /**
     * Obtiene el valor de la propiedad onTimePerformance.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getOnTimePerformance() {
        return onTimePerformance;
    }

    /**
     * Define el valor de la propiedad onTimePerformance.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setOnTimePerformance(BigInteger value) {
        this.onTimePerformance = value;
    }

    /**
     * Obtiene el valor de la propiedad dayChange.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDayChange() {
        return dayChange;
    }

    /**
     * Define el valor de la propiedad dayChange.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDayChange(BigInteger value) {
        this.dayChange = value;
    }

    /**
     * Obtiene el valor de la propiedad journeyTime.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getJourneyTime() {
        return journeyTime;
    }

    /**
     * Define el valor de la propiedad journeyTime.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setJourneyTime(BigInteger value) {
        this.journeyTime = value;
    }

    /**
     * Obtiene el valor de la propiedad flightTime.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getFlightTime() {
        return flightTime;
    }

    /**
     * Define el valor de la propiedad flightTime.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setFlightTime(BigInteger value) {
        this.flightTime = value;
    }

    /**
     * Obtiene el valor de la propiedad startTerminal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartTerminal() {
        return startTerminal;
    }

    /**
     * Define el valor de la propiedad startTerminal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartTerminal(String value) {
        this.startTerminal = value;
    }

    /**
     * Obtiene el valor de la propiedad endTerminal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndTerminal() {
        return endTerminal;
    }

    /**
     * Define el valor de la propiedad endTerminal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndTerminal(String value) {
        this.endTerminal = value;
    }

    /**
     * Obtiene el valor de la propiedad firstIntermediateStop.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstIntermediateStop() {
        return firstIntermediateStop;
    }

    /**
     * Define el valor de la propiedad firstIntermediateStop.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstIntermediateStop(String value) {
        this.firstIntermediateStop = value;
    }

    /**
     * Obtiene el valor de la propiedad lastIntermediateStop.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastIntermediateStop() {
        return lastIntermediateStop;
    }

    /**
     * Define el valor de la propiedad lastIntermediateStop.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastIntermediateStop(String value) {
        this.lastIntermediateStop = value;
    }

    /**
     * Obtiene el valor de la propiedad insideAvailability.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsideAvailability() {
        return insideAvailability;
    }

    /**
     * Define el valor de la propiedad insideAvailability.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsideAvailability(String value) {
        this.insideAvailability = value;
    }

    /**
     * Obtiene el valor de la propiedad secureSell.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecureSell() {
        return secureSell;
    }

    /**
     * Define el valor de la propiedad secureSell.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecureSell(String value) {
        this.secureSell = value;
    }

    /**
     * Obtiene el valor de la propiedad availabilitySource.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAvailabilitySource() {
        return availabilitySource;
    }

    /**
     * Define el valor de la propiedad availabilitySource.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAvailabilitySource(String value) {
        this.availabilitySource = value;
    }

}
