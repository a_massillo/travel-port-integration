
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.TypeReqSeat;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="SegmentRef" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="Smoking" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="SeatType" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeReqSeat" /&gt;
 *       &lt;attribute name="Group" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="BookingTravelerRef" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "AutoSeatAssignment")
public class AutoSeatAssignment {

    @XmlAttribute(name = "SegmentRef")
    protected String segmentRef;
    @XmlAttribute(name = "Smoking")
    protected Boolean smoking;
    @XmlAttribute(name = "SeatType", required = true)
    protected TypeReqSeat seatType;
    @XmlAttribute(name = "Group")
    protected Boolean group;
    @XmlAttribute(name = "BookingTravelerRef")
    protected String bookingTravelerRef;

    /**
     * Obtiene el valor de la propiedad segmentRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSegmentRef() {
        return segmentRef;
    }

    /**
     * Define el valor de la propiedad segmentRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSegmentRef(String value) {
        this.segmentRef = value;
    }

    /**
     * Obtiene el valor de la propiedad smoking.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isSmoking() {
        if (smoking == null) {
            return false;
        } else {
            return smoking;
        }
    }

    /**
     * Define el valor de la propiedad smoking.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSmoking(Boolean value) {
        this.smoking = value;
    }

    /**
     * Obtiene el valor de la propiedad seatType.
     * 
     * @return
     *     possible object is
     *     {@link TypeReqSeat }
     *     
     */
    public TypeReqSeat getSeatType() {
        return seatType;
    }

    /**
     * Define el valor de la propiedad seatType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeReqSeat }
     *     
     */
    public void setSeatType(TypeReqSeat value) {
        this.seatType = value;
    }

    /**
     * Obtiene el valor de la propiedad group.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isGroup() {
        if (group == null) {
            return false;
        } else {
            return group;
        }
    }

    /**
     * Define el valor de la propiedad group.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGroup(Boolean value) {
        this.group = value;
    }

    /**
     * Obtiene el valor de la propiedad bookingTravelerRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookingTravelerRef() {
        return bookingTravelerRef;
    }

    /**
     * Define el valor de la propiedad bookingTravelerRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookingTravelerRef(String value) {
        this.bookingTravelerRef = value;
    }

}
