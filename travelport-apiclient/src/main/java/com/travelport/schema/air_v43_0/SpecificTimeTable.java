
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.Airport;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FlightOrigin" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Airport"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="FlightDestination" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Airport"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="StartDate" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="Carrier" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeCarrier" /&gt;
 *       &lt;attribute name="FlightNumber" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeFlightNumber" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "flightOrigin",
    "flightDestination"
})
@XmlRootElement(name = "SpecificTimeTable")
public class SpecificTimeTable {

    @XmlElement(name = "FlightOrigin")
    protected SpecificTimeTable.FlightOrigin flightOrigin;
    @XmlElement(name = "FlightDestination")
    protected SpecificTimeTable.FlightDestination flightDestination;
    @XmlAttribute(name = "StartDate", required = true)
    protected String startDate;
    @XmlAttribute(name = "Carrier", required = true)
    protected String carrier;
    @XmlAttribute(name = "FlightNumber", required = true)
    protected String flightNumber;

    /**
     * Obtiene el valor de la propiedad flightOrigin.
     * 
     * @return
     *     possible object is
     *     {@link SpecificTimeTable.FlightOrigin }
     *     
     */
    public SpecificTimeTable.FlightOrigin getFlightOrigin() {
        return flightOrigin;
    }

    /**
     * Define el valor de la propiedad flightOrigin.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecificTimeTable.FlightOrigin }
     *     
     */
    public void setFlightOrigin(SpecificTimeTable.FlightOrigin value) {
        this.flightOrigin = value;
    }

    /**
     * Obtiene el valor de la propiedad flightDestination.
     * 
     * @return
     *     possible object is
     *     {@link SpecificTimeTable.FlightDestination }
     *     
     */
    public SpecificTimeTable.FlightDestination getFlightDestination() {
        return flightDestination;
    }

    /**
     * Define el valor de la propiedad flightDestination.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecificTimeTable.FlightDestination }
     *     
     */
    public void setFlightDestination(SpecificTimeTable.FlightDestination value) {
        this.flightDestination = value;
    }

    /**
     * Obtiene el valor de la propiedad startDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * Define el valor de la propiedad startDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartDate(String value) {
        this.startDate = value;
    }

    /**
     * Obtiene el valor de la propiedad carrier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCarrier() {
        return carrier;
    }

    /**
     * Define el valor de la propiedad carrier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarrier(String value) {
        this.carrier = value;
    }

    /**
     * Obtiene el valor de la propiedad flightNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Define el valor de la propiedad flightNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightNumber(String value) {
        this.flightNumber = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Airport"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "airport"
    })
    public static class FlightDestination {

        @XmlElement(name = "Airport", namespace = "http://www.travelport.com/schema/common_v43_0", required = true)
        protected Airport airport;

        /**
         * Obtiene el valor de la propiedad airport.
         * 
         * @return
         *     possible object is
         *     {@link Airport }
         *     
         */
        public Airport getAirport() {
            return airport;
        }

        /**
         * Define el valor de la propiedad airport.
         * 
         * @param value
         *     allowed object is
         *     {@link Airport }
         *     
         */
        public void setAirport(Airport value) {
            this.airport = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Airport"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "airport"
    })
    public static class FlightOrigin {

        @XmlElement(name = "Airport", namespace = "http://www.travelport.com/schema/common_v43_0", required = true)
        protected Airport airport;

        /**
         * Obtiene el valor de la propiedad airport.
         * 
         * @return
         *     possible object is
         *     {@link Airport }
         *     
         */
        public Airport getAirport() {
            return airport;
        }

        /**
         * Define el valor de la propiedad airport.
         * 
         * @param value
         *     allowed object is
         *     {@link Airport }
         *     
         */
        public void setAirport(Airport value) {
            this.airport = value;
        }

    }

}
