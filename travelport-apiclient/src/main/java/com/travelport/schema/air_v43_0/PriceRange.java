
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="DefaultCurrency" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="StartPrice" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="EndPrice" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "PriceRange")
public class PriceRange {

    @XmlAttribute(name = "DefaultCurrency")
    protected Boolean defaultCurrency;
    @XmlAttribute(name = "StartPrice")
    protected String startPrice;
    @XmlAttribute(name = "EndPrice")
    protected String endPrice;

    /**
     * Obtiene el valor de la propiedad defaultCurrency.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDefaultCurrency() {
        return defaultCurrency;
    }

    /**
     * Define el valor de la propiedad defaultCurrency.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDefaultCurrency(Boolean value) {
        this.defaultCurrency = value;
    }

    /**
     * Obtiene el valor de la propiedad startPrice.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartPrice() {
        return startPrice;
    }

    /**
     * Define el valor de la propiedad startPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartPrice(String value) {
        this.startPrice = value;
    }

    /**
     * Obtiene el valor de la propiedad endPrice.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndPrice() {
        return endPrice;
    }

    /**
     * Define el valor de la propiedad endPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndPrice(String value) {
        this.endPrice = value;
    }

}
