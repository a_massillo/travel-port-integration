
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Refundability" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="Value" use="required" type="{http://www.travelport.com/schema/air_v43_0}typeRefundabilityValue" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="LatestTicketingTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "refundability",
    "latestTicketingTime"
})
@XmlRootElement(name = "FareRulesFilter")
public class FareRulesFilter {

    @XmlElement(name = "Refundability")
    protected FareRulesFilter.Refundability refundability;
    @XmlElement(name = "LatestTicketingTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar latestTicketingTime;

    /**
     * Obtiene el valor de la propiedad refundability.
     * 
     * @return
     *     possible object is
     *     {@link FareRulesFilter.Refundability }
     *     
     */
    public FareRulesFilter.Refundability getRefundability() {
        return refundability;
    }

    /**
     * Define el valor de la propiedad refundability.
     * 
     * @param value
     *     allowed object is
     *     {@link FareRulesFilter.Refundability }
     *     
     */
    public void setRefundability(FareRulesFilter.Refundability value) {
        this.refundability = value;
    }

    /**
     * Obtiene el valor de la propiedad latestTicketingTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLatestTicketingTime() {
        return latestTicketingTime;
    }

    /**
     * Define el valor de la propiedad latestTicketingTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLatestTicketingTime(XMLGregorianCalendar value) {
        this.latestTicketingTime = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attribute name="Value" use="required" type="{http://www.travelport.com/schema/air_v43_0}typeRefundabilityValue" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Refundability {

        @XmlAttribute(name = "Value", required = true)
        protected String value;

        /**
         * Obtiene el valor de la propiedad value.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * Define el valor de la propiedad value.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

    }

}
