
package com.travelport.schema.air_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.Commission;
import com.travelport.schema.common_v43_0.SupplierLocator;
import com.travelport.schema.common_v43_0.TypeElementStatus;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BookingTravelerRef" type="{http://www.travelport.com/schema/common_v43_0}typeRef" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element name="NetRemit" type="{http://www.travelport.com/schema/air_v43_0}typeTicketModifierAmountType" minOccurs="0"/&gt;
 *         &lt;element name="NetFare" type="{http://www.travelport.com/schema/air_v43_0}typeTicketModifierAmountType" minOccurs="0"/&gt;
 *         &lt;element name="ActualSellingFare" type="{http://www.travelport.com/schema/air_v43_0}typeTicketModifierAmountType" minOccurs="0"/&gt;
 *         &lt;element name="InvoiceFare" type="{http://www.travelport.com/schema/air_v43_0}typeTicketModifierAccountingType" minOccurs="0"/&gt;
 *         &lt;element name="CorporateDiscount" type="{http://www.travelport.com/schema/air_v43_0}typeTicketModifierAccountingType" minOccurs="0"/&gt;
 *         &lt;element name="AccountingInfo" type="{http://www.travelport.com/schema/air_v43_0}typeTicketModifierAccountingType" minOccurs="0"/&gt;
 *         &lt;element name="BulkTicket" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.travelport.com/schema/air_v43_0}typeBulkTicketModifierType"&gt;
 *                 &lt;attribute name="NonRefundable" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="GroupTour" type="{http://www.travelport.com/schema/air_v43_0}typeBulkTicketModifierType" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Commission" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}TourCode" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}TicketEndorsement" maxOccurs="3" minOccurs="0"/&gt;
 *         &lt;element name="ValueModifier" type="{http://www.travelport.com/schema/air_v43_0}typeTicketModifierValueType" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}DocumentSelect" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}DocumentOptions" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}SegmentSelect" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}SegmentModifiers" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}SupplierLocator" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}DestinationPurposeCode" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}LanguageOption" maxOccurs="2" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}LandCharges" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}PrintBlankFormItinerary" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}ExcludeTicketing" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}ExemptOBFee" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attGroup ref="{http://www.travelport.com/schema/common_v43_0}attrElementKeyResults"/&gt;
 *       &lt;attribute name="PlatingCarrier" type="{http://www.travelport.com/schema/common_v43_0}typeCarrier" /&gt;
 *       &lt;attribute name="ExemptVAT" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="NetRemitApplied" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="FreeTicket" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="CurrencyOverrideCode"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;length value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Key" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "bookingTravelerRef",
    "netRemit",
    "netFare",
    "actualSellingFare",
    "invoiceFare",
    "corporateDiscount",
    "accountingInfo",
    "bulkTicket",
    "groupTour",
    "commission",
    "tourCode",
    "ticketEndorsement",
    "valueModifier",
    "documentSelect",
    "documentOptions",
    "segmentSelect",
    "segmentModifiers",
    "supplierLocator",
    "destinationPurposeCode",
    "languageOption",
    "landCharges",
    "printBlankFormItinerary",
    "excludeTicketing",
    "exemptOBFee"
})
@XmlRootElement(name = "TicketingModifiers")
public class TicketingModifiers {

    @XmlElement(name = "BookingTravelerRef")
    protected List<String> bookingTravelerRef;
    @XmlElement(name = "NetRemit")
    protected TypeTicketModifierAmountType netRemit;
    @XmlElement(name = "NetFare")
    protected TypeTicketModifierAmountType netFare;
    @XmlElement(name = "ActualSellingFare")
    protected TypeTicketModifierAmountType actualSellingFare;
    @XmlElement(name = "InvoiceFare")
    protected TypeTicketModifierAccountingType invoiceFare;
    @XmlElement(name = "CorporateDiscount")
    protected TypeTicketModifierAccountingType corporateDiscount;
    @XmlElement(name = "AccountingInfo")
    protected TypeTicketModifierAccountingType accountingInfo;
    @XmlElement(name = "BulkTicket")
    protected TicketingModifiers.BulkTicket bulkTicket;
    @XmlElement(name = "GroupTour")
    protected TypeBulkTicketModifierType groupTour;
    @XmlElement(name = "Commission", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected Commission commission;
    @XmlElement(name = "TourCode")
    protected TourCode tourCode;
    @XmlElement(name = "TicketEndorsement")
    protected List<TicketEndorsement> ticketEndorsement;
    @XmlElement(name = "ValueModifier")
    protected TypeTicketModifierValueType valueModifier;
    @XmlElement(name = "DocumentSelect")
    protected DocumentSelect documentSelect;
    @XmlElement(name = "DocumentOptions")
    protected DocumentOptions documentOptions;
    @XmlElement(name = "SegmentSelect")
    protected SegmentSelect segmentSelect;
    @XmlElement(name = "SegmentModifiers")
    protected List<SegmentModifiers> segmentModifiers;
    @XmlElement(name = "SupplierLocator", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected SupplierLocator supplierLocator;
    @XmlElement(name = "DestinationPurposeCode")
    protected DestinationPurposeCode destinationPurposeCode;
    @XmlElement(name = "LanguageOption")
    protected List<LanguageOption> languageOption;
    @XmlElement(name = "LandCharges")
    protected LandCharges landCharges;
    @XmlElement(name = "PrintBlankFormItinerary")
    protected PrintBlankFormItinerary printBlankFormItinerary;
    @XmlElement(name = "ExcludeTicketing")
    protected ExcludeTicketing excludeTicketing;
    @XmlElement(name = "ExemptOBFee")
    protected ExemptOBFee exemptOBFee;
    @XmlAttribute(name = "PlatingCarrier")
    protected String platingCarrier;
    @XmlAttribute(name = "ExemptVAT")
    protected Boolean exemptVAT;
    @XmlAttribute(name = "NetRemitApplied")
    protected Boolean netRemitApplied;
    @XmlAttribute(name = "FreeTicket")
    protected Boolean freeTicket;
    @XmlAttribute(name = "CurrencyOverrideCode")
    protected String currencyOverrideCode;
    @XmlAttribute(name = "Key")
    protected String key;
    @XmlAttribute(name = "ElStat")
    protected TypeElementStatus elStat;
    @XmlAttribute(name = "KeyOverride")
    protected Boolean keyOverride;

    /**
     * Gets the value of the bookingTravelerRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bookingTravelerRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBookingTravelerRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getBookingTravelerRef() {
        if (bookingTravelerRef == null) {
            bookingTravelerRef = new ArrayList<String>();
        }
        return this.bookingTravelerRef;
    }

    /**
     * Obtiene el valor de la propiedad netRemit.
     * 
     * @return
     *     possible object is
     *     {@link TypeTicketModifierAmountType }
     *     
     */
    public TypeTicketModifierAmountType getNetRemit() {
        return netRemit;
    }

    /**
     * Define el valor de la propiedad netRemit.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeTicketModifierAmountType }
     *     
     */
    public void setNetRemit(TypeTicketModifierAmountType value) {
        this.netRemit = value;
    }

    /**
     * Obtiene el valor de la propiedad netFare.
     * 
     * @return
     *     possible object is
     *     {@link TypeTicketModifierAmountType }
     *     
     */
    public TypeTicketModifierAmountType getNetFare() {
        return netFare;
    }

    /**
     * Define el valor de la propiedad netFare.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeTicketModifierAmountType }
     *     
     */
    public void setNetFare(TypeTicketModifierAmountType value) {
        this.netFare = value;
    }

    /**
     * Obtiene el valor de la propiedad actualSellingFare.
     * 
     * @return
     *     possible object is
     *     {@link TypeTicketModifierAmountType }
     *     
     */
    public TypeTicketModifierAmountType getActualSellingFare() {
        return actualSellingFare;
    }

    /**
     * Define el valor de la propiedad actualSellingFare.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeTicketModifierAmountType }
     *     
     */
    public void setActualSellingFare(TypeTicketModifierAmountType value) {
        this.actualSellingFare = value;
    }

    /**
     * Obtiene el valor de la propiedad invoiceFare.
     * 
     * @return
     *     possible object is
     *     {@link TypeTicketModifierAccountingType }
     *     
     */
    public TypeTicketModifierAccountingType getInvoiceFare() {
        return invoiceFare;
    }

    /**
     * Define el valor de la propiedad invoiceFare.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeTicketModifierAccountingType }
     *     
     */
    public void setInvoiceFare(TypeTicketModifierAccountingType value) {
        this.invoiceFare = value;
    }

    /**
     * Obtiene el valor de la propiedad corporateDiscount.
     * 
     * @return
     *     possible object is
     *     {@link TypeTicketModifierAccountingType }
     *     
     */
    public TypeTicketModifierAccountingType getCorporateDiscount() {
        return corporateDiscount;
    }

    /**
     * Define el valor de la propiedad corporateDiscount.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeTicketModifierAccountingType }
     *     
     */
    public void setCorporateDiscount(TypeTicketModifierAccountingType value) {
        this.corporateDiscount = value;
    }

    /**
     * Obtiene el valor de la propiedad accountingInfo.
     * 
     * @return
     *     possible object is
     *     {@link TypeTicketModifierAccountingType }
     *     
     */
    public TypeTicketModifierAccountingType getAccountingInfo() {
        return accountingInfo;
    }

    /**
     * Define el valor de la propiedad accountingInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeTicketModifierAccountingType }
     *     
     */
    public void setAccountingInfo(TypeTicketModifierAccountingType value) {
        this.accountingInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad bulkTicket.
     * 
     * @return
     *     possible object is
     *     {@link TicketingModifiers.BulkTicket }
     *     
     */
    public TicketingModifiers.BulkTicket getBulkTicket() {
        return bulkTicket;
    }

    /**
     * Define el valor de la propiedad bulkTicket.
     * 
     * @param value
     *     allowed object is
     *     {@link TicketingModifiers.BulkTicket }
     *     
     */
    public void setBulkTicket(TicketingModifiers.BulkTicket value) {
        this.bulkTicket = value;
    }

    /**
     * Obtiene el valor de la propiedad groupTour.
     * 
     * @return
     *     possible object is
     *     {@link TypeBulkTicketModifierType }
     *     
     */
    public TypeBulkTicketModifierType getGroupTour() {
        return groupTour;
    }

    /**
     * Define el valor de la propiedad groupTour.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeBulkTicketModifierType }
     *     
     */
    public void setGroupTour(TypeBulkTicketModifierType value) {
        this.groupTour = value;
    }

    /**
     * Allows an agency to update the commission
     *                      to a new or different commission rate which will be applied at
     *                      time of ticketing. The commission Modifier allows the user
     *                      specify how the commission change is to applied
     *                   
     * 
     * @return
     *     possible object is
     *     {@link Commission }
     *     
     */
    public Commission getCommission() {
        return commission;
    }

    /**
     * Define el valor de la propiedad commission.
     * 
     * @param value
     *     allowed object is
     *     {@link Commission }
     *     
     */
    public void setCommission(Commission value) {
        this.commission = value;
    }

    /**
     * Allows an agency to modify the tour code
     *                      information on the ticket
     * 
     * @return
     *     possible object is
     *     {@link TourCode }
     *     
     */
    public TourCode getTourCode() {
        return tourCode;
    }

    /**
     * Define el valor de la propiedad tourCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TourCode }
     *     
     */
    public void setTourCode(TourCode value) {
        this.tourCode = value;
    }

    /**
     * Allows an agency to add user defined
     *                      ticketing endorsements the ticket Gets the value of the ticketEndorsement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ticketEndorsement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTicketEndorsement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TicketEndorsement }
     * 
     * 
     */
    public List<TicketEndorsement> getTicketEndorsement() {
        if (ticketEndorsement == null) {
            ticketEndorsement = new ArrayList<TicketEndorsement>();
        }
        return this.ticketEndorsement;
    }

    /**
     * Obtiene el valor de la propiedad valueModifier.
     * 
     * @return
     *     possible object is
     *     {@link TypeTicketModifierValueType }
     *     
     */
    public TypeTicketModifierValueType getValueModifier() {
        return valueModifier;
    }

    /**
     * Define el valor de la propiedad valueModifier.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeTicketModifierValueType }
     *     
     */
    public void setValueModifier(TypeTicketModifierValueType value) {
        this.valueModifier = value;
    }

    /**
     * Obtiene el valor de la propiedad documentSelect.
     * 
     * @return
     *     possible object is
     *     {@link DocumentSelect }
     *     
     */
    public DocumentSelect getDocumentSelect() {
        return documentSelect;
    }

    /**
     * Define el valor de la propiedad documentSelect.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentSelect }
     *     
     */
    public void setDocumentSelect(DocumentSelect value) {
        this.documentSelect = value;
    }

    /**
     * Obtiene el valor de la propiedad documentOptions.
     * 
     * @return
     *     possible object is
     *     {@link DocumentOptions }
     *     
     */
    public DocumentOptions getDocumentOptions() {
        return documentOptions;
    }

    /**
     * Define el valor de la propiedad documentOptions.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentOptions }
     *     
     */
    public void setDocumentOptions(DocumentOptions value) {
        this.documentOptions = value;
    }

    /**
     * Obtiene el valor de la propiedad segmentSelect.
     * 
     * @return
     *     possible object is
     *     {@link SegmentSelect }
     *     
     */
    public SegmentSelect getSegmentSelect() {
        return segmentSelect;
    }

    /**
     * Define el valor de la propiedad segmentSelect.
     * 
     * @param value
     *     allowed object is
     *     {@link SegmentSelect }
     *     
     */
    public void setSegmentSelect(SegmentSelect value) {
        this.segmentSelect = value;
    }

    /**
     * Gets the value of the segmentModifiers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the segmentModifiers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSegmentModifiers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SegmentModifiers }
     * 
     * 
     */
    public List<SegmentModifiers> getSegmentModifiers() {
        if (segmentModifiers == null) {
            segmentModifiers = new ArrayList<SegmentModifiers>();
        }
        return this.segmentModifiers;
    }

    /**
     * Obtiene el valor de la propiedad supplierLocator.
     * 
     * @return
     *     possible object is
     *     {@link SupplierLocator }
     *     
     */
    public SupplierLocator getSupplierLocator() {
        return supplierLocator;
    }

    /**
     * Define el valor de la propiedad supplierLocator.
     * 
     * @param value
     *     allowed object is
     *     {@link SupplierLocator }
     *     
     */
    public void setSupplierLocator(SupplierLocator value) {
        this.supplierLocator = value;
    }

    /**
     * Obtiene el valor de la propiedad destinationPurposeCode.
     * 
     * @return
     *     possible object is
     *     {@link DestinationPurposeCode }
     *     
     */
    public DestinationPurposeCode getDestinationPurposeCode() {
        return destinationPurposeCode;
    }

    /**
     * Define el valor de la propiedad destinationPurposeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link DestinationPurposeCode }
     *     
     */
    public void setDestinationPurposeCode(DestinationPurposeCode value) {
        this.destinationPurposeCode = value;
    }

    /**
     * Gets the value of the languageOption property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the languageOption property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLanguageOption().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LanguageOption }
     * 
     * 
     */
    public List<LanguageOption> getLanguageOption() {
        if (languageOption == null) {
            languageOption = new ArrayList<LanguageOption>();
        }
        return this.languageOption;
    }

    /**
     * Obtiene el valor de la propiedad landCharges.
     * 
     * @return
     *     possible object is
     *     {@link LandCharges }
     *     
     */
    public LandCharges getLandCharges() {
        return landCharges;
    }

    /**
     * Define el valor de la propiedad landCharges.
     * 
     * @param value
     *     allowed object is
     *     {@link LandCharges }
     *     
     */
    public void setLandCharges(LandCharges value) {
        this.landCharges = value;
    }

    /**
     * Obtiene el valor de la propiedad printBlankFormItinerary.
     * 
     * @return
     *     possible object is
     *     {@link PrintBlankFormItinerary }
     *     
     */
    public PrintBlankFormItinerary getPrintBlankFormItinerary() {
        return printBlankFormItinerary;
    }

    /**
     * Define el valor de la propiedad printBlankFormItinerary.
     * 
     * @param value
     *     allowed object is
     *     {@link PrintBlankFormItinerary }
     *     
     */
    public void setPrintBlankFormItinerary(PrintBlankFormItinerary value) {
        this.printBlankFormItinerary = value;
    }

    /**
     * Obtiene el valor de la propiedad excludeTicketing.
     * 
     * @return
     *     possible object is
     *     {@link ExcludeTicketing }
     *     
     */
    public ExcludeTicketing getExcludeTicketing() {
        return excludeTicketing;
    }

    /**
     * Define el valor de la propiedad excludeTicketing.
     * 
     * @param value
     *     allowed object is
     *     {@link ExcludeTicketing }
     *     
     */
    public void setExcludeTicketing(ExcludeTicketing value) {
        this.excludeTicketing = value;
    }

    /**
     * Obtiene el valor de la propiedad exemptOBFee.
     * 
     * @return
     *     possible object is
     *     {@link ExemptOBFee }
     *     
     */
    public ExemptOBFee getExemptOBFee() {
        return exemptOBFee;
    }

    /**
     * Define el valor de la propiedad exemptOBFee.
     * 
     * @param value
     *     allowed object is
     *     {@link ExemptOBFee }
     *     
     */
    public void setExemptOBFee(ExemptOBFee value) {
        this.exemptOBFee = value;
    }

    /**
     * Obtiene el valor de la propiedad platingCarrier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlatingCarrier() {
        return platingCarrier;
    }

    /**
     * Define el valor de la propiedad platingCarrier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlatingCarrier(String value) {
        this.platingCarrier = value;
    }

    /**
     * Obtiene el valor de la propiedad exemptVAT.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExemptVAT() {
        return exemptVAT;
    }

    /**
     * Define el valor de la propiedad exemptVAT.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExemptVAT(Boolean value) {
        this.exemptVAT = value;
    }

    /**
     * Obtiene el valor de la propiedad netRemitApplied.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNetRemitApplied() {
        return netRemitApplied;
    }

    /**
     * Define el valor de la propiedad netRemitApplied.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNetRemitApplied(Boolean value) {
        this.netRemitApplied = value;
    }

    /**
     * Obtiene el valor de la propiedad freeTicket.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFreeTicket() {
        return freeTicket;
    }

    /**
     * Define el valor de la propiedad freeTicket.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFreeTicket(Boolean value) {
        this.freeTicket = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyOverrideCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyOverrideCode() {
        return currencyOverrideCode;
    }

    /**
     * Define el valor de la propiedad currencyOverrideCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyOverrideCode(String value) {
        this.currencyOverrideCode = value;
    }

    /**
     * Obtiene el valor de la propiedad key.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Define el valor de la propiedad key.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Obtiene el valor de la propiedad elStat.
     * 
     * @return
     *     possible object is
     *     {@link TypeElementStatus }
     *     
     */
    public TypeElementStatus getElStat() {
        return elStat;
    }

    /**
     * Define el valor de la propiedad elStat.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeElementStatus }
     *     
     */
    public void setElStat(TypeElementStatus value) {
        this.elStat = value;
    }

    /**
     * Obtiene el valor de la propiedad keyOverride.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isKeyOverride() {
        return keyOverride;
    }

    /**
     * Define el valor de la propiedad keyOverride.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setKeyOverride(Boolean value) {
        this.keyOverride = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.travelport.com/schema/air_v43_0}typeBulkTicketModifierType"&gt;
     *       &lt;attribute name="NonRefundable" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class BulkTicket
        extends TypeBulkTicketModifierType
    {

        @XmlAttribute(name = "NonRefundable")
        protected Boolean nonRefundable;

        /**
         * Obtiene el valor de la propiedad nonRefundable.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isNonRefundable() {
            return nonRefundable;
        }

        /**
         * Define el valor de la propiedad nonRefundable.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setNonRefundable(Boolean value) {
            this.nonRefundable = value;
        }

    }

}
