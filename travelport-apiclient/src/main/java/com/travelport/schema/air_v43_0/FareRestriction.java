
package com.travelport.schema.air_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FareRestrictionDaysOfWeek" maxOccurs="3" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FareRestrictionDate" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FareRestrictionSaleDate" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="FareRestrictiontype" type="{http://www.travelport.com/schema/air_v43_0}typeFareRestrictionType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fareRestrictionDaysOfWeek",
    "fareRestrictionDate",
    "fareRestrictionSaleDate"
})
@XmlRootElement(name = "FareRestriction")
public class FareRestriction {

    @XmlElement(name = "FareRestrictionDaysOfWeek")
    protected List<FareRestrictionDaysOfWeek> fareRestrictionDaysOfWeek;
    @XmlElement(name = "FareRestrictionDate")
    protected List<FareRestrictionDate> fareRestrictionDate;
    @XmlElement(name = "FareRestrictionSaleDate")
    protected FareRestrictionSaleDate fareRestrictionSaleDate;
    @XmlAttribute(name = "FareRestrictiontype")
    protected TypeFareRestrictionType fareRestrictiontype;

    /**
     * Gets the value of the fareRestrictionDaysOfWeek property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fareRestrictionDaysOfWeek property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFareRestrictionDaysOfWeek().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FareRestrictionDaysOfWeek }
     * 
     * 
     */
    public List<FareRestrictionDaysOfWeek> getFareRestrictionDaysOfWeek() {
        if (fareRestrictionDaysOfWeek == null) {
            fareRestrictionDaysOfWeek = new ArrayList<FareRestrictionDaysOfWeek>();
        }
        return this.fareRestrictionDaysOfWeek;
    }

    /**
     * Gets the value of the fareRestrictionDate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fareRestrictionDate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFareRestrictionDate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FareRestrictionDate }
     * 
     * 
     */
    public List<FareRestrictionDate> getFareRestrictionDate() {
        if (fareRestrictionDate == null) {
            fareRestrictionDate = new ArrayList<FareRestrictionDate>();
        }
        return this.fareRestrictionDate;
    }

    /**
     * Obtiene el valor de la propiedad fareRestrictionSaleDate.
     * 
     * @return
     *     possible object is
     *     {@link FareRestrictionSaleDate }
     *     
     */
    public FareRestrictionSaleDate getFareRestrictionSaleDate() {
        return fareRestrictionSaleDate;
    }

    /**
     * Define el valor de la propiedad fareRestrictionSaleDate.
     * 
     * @param value
     *     allowed object is
     *     {@link FareRestrictionSaleDate }
     *     
     */
    public void setFareRestrictionSaleDate(FareRestrictionSaleDate value) {
        this.fareRestrictionSaleDate = value;
    }

    /**
     * Obtiene el valor de la propiedad fareRestrictiontype.
     * 
     * @return
     *     possible object is
     *     {@link TypeFareRestrictionType }
     *     
     */
    public TypeFareRestrictionType getFareRestrictiontype() {
        return fareRestrictiontype;
    }

    /**
     * Define el valor de la propiedad fareRestrictiontype.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeFareRestrictionType }
     *     
     */
    public void setFareRestrictiontype(TypeFareRestrictionType value) {
        this.fareRestrictiontype = value;
    }

}
