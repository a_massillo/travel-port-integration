
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="IncludeDescription" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="IncludeHeader" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "PrintBlankFormItinerary")
public class PrintBlankFormItinerary {

    @XmlAttribute(name = "IncludeDescription", required = true)
    protected boolean includeDescription;
    @XmlAttribute(name = "IncludeHeader", required = true)
    protected boolean includeHeader;

    /**
     * Obtiene el valor de la propiedad includeDescription.
     * 
     */
    public boolean isIncludeDescription() {
        return includeDescription;
    }

    /**
     * Define el valor de la propiedad includeDescription.
     * 
     */
    public void setIncludeDescription(boolean value) {
        this.includeDescription = value;
    }

    /**
     * Obtiene el valor de la propiedad includeHeader.
     * 
     */
    public boolean isIncludeHeader() {
        return includeHeader;
    }

    /**
     * Define el valor de la propiedad includeHeader.
     * 
     */
    public void setIncludeHeader(boolean value) {
        this.includeHeader = value;
    }

}
