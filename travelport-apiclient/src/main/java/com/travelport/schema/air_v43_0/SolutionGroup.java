
package com.travelport.schema.air_v43_0;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.AccountCode;
import com.travelport.schema.common_v43_0.PointOfSale;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PermittedAccountCodes" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/common_v43_0}AccountCode" maxOccurs="999"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="PreferredAccountCodes" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/common_v43_0}AccountCode" maxOccurs="999"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ProhibitedAccountCodes" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/common_v43_0}AccountCode" maxOccurs="999"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="PermittedPointOfSales" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/common_v43_0}PointOfSale" maxOccurs="999"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ProhibitedPointOfSales" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/common_v43_0}PointOfSale" maxOccurs="999"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Count" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="TripType" use="required" type="{http://www.travelport.com/schema/air_v43_0}typeTripType" /&gt;
 *       &lt;attribute name="Diversification" type="{http://www.travelport.com/schema/air_v43_0}typeDiversity" /&gt;
 *       &lt;attribute name="Tag"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="20"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Primary" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "permittedAccountCodes",
    "preferredAccountCodes",
    "prohibitedAccountCodes",
    "permittedPointOfSales",
    "prohibitedPointOfSales"
})
@XmlRootElement(name = "SolutionGroup")
public class SolutionGroup {

    @XmlElement(name = "PermittedAccountCodes")
    protected SolutionGroup.PermittedAccountCodes permittedAccountCodes;
    @XmlElement(name = "PreferredAccountCodes")
    protected SolutionGroup.PreferredAccountCodes preferredAccountCodes;
    @XmlElement(name = "ProhibitedAccountCodes")
    protected SolutionGroup.ProhibitedAccountCodes prohibitedAccountCodes;
    @XmlElement(name = "PermittedPointOfSales")
    protected SolutionGroup.PermittedPointOfSales permittedPointOfSales;
    @XmlElement(name = "ProhibitedPointOfSales")
    protected SolutionGroup.ProhibitedPointOfSales prohibitedPointOfSales;
    @XmlAttribute(name = "Count")
    protected BigInteger count;
    @XmlAttribute(name = "TripType", required = true)
    protected TypeTripType tripType;
    @XmlAttribute(name = "Diversification")
    protected TypeDiversity diversification;
    @XmlAttribute(name = "Tag")
    protected String tag;
    @XmlAttribute(name = "Primary")
    protected Boolean primary;

    /**
     * Obtiene el valor de la propiedad permittedAccountCodes.
     * 
     * @return
     *     possible object is
     *     {@link SolutionGroup.PermittedAccountCodes }
     *     
     */
    public SolutionGroup.PermittedAccountCodes getPermittedAccountCodes() {
        return permittedAccountCodes;
    }

    /**
     * Define el valor de la propiedad permittedAccountCodes.
     * 
     * @param value
     *     allowed object is
     *     {@link SolutionGroup.PermittedAccountCodes }
     *     
     */
    public void setPermittedAccountCodes(SolutionGroup.PermittedAccountCodes value) {
        this.permittedAccountCodes = value;
    }

    /**
     * Obtiene el valor de la propiedad preferredAccountCodes.
     * 
     * @return
     *     possible object is
     *     {@link SolutionGroup.PreferredAccountCodes }
     *     
     */
    public SolutionGroup.PreferredAccountCodes getPreferredAccountCodes() {
        return preferredAccountCodes;
    }

    /**
     * Define el valor de la propiedad preferredAccountCodes.
     * 
     * @param value
     *     allowed object is
     *     {@link SolutionGroup.PreferredAccountCodes }
     *     
     */
    public void setPreferredAccountCodes(SolutionGroup.PreferredAccountCodes value) {
        this.preferredAccountCodes = value;
    }

    /**
     * Obtiene el valor de la propiedad prohibitedAccountCodes.
     * 
     * @return
     *     possible object is
     *     {@link SolutionGroup.ProhibitedAccountCodes }
     *     
     */
    public SolutionGroup.ProhibitedAccountCodes getProhibitedAccountCodes() {
        return prohibitedAccountCodes;
    }

    /**
     * Define el valor de la propiedad prohibitedAccountCodes.
     * 
     * @param value
     *     allowed object is
     *     {@link SolutionGroup.ProhibitedAccountCodes }
     *     
     */
    public void setProhibitedAccountCodes(SolutionGroup.ProhibitedAccountCodes value) {
        this.prohibitedAccountCodes = value;
    }

    /**
     * Obtiene el valor de la propiedad permittedPointOfSales.
     * 
     * @return
     *     possible object is
     *     {@link SolutionGroup.PermittedPointOfSales }
     *     
     */
    public SolutionGroup.PermittedPointOfSales getPermittedPointOfSales() {
        return permittedPointOfSales;
    }

    /**
     * Define el valor de la propiedad permittedPointOfSales.
     * 
     * @param value
     *     allowed object is
     *     {@link SolutionGroup.PermittedPointOfSales }
     *     
     */
    public void setPermittedPointOfSales(SolutionGroup.PermittedPointOfSales value) {
        this.permittedPointOfSales = value;
    }

    /**
     * Obtiene el valor de la propiedad prohibitedPointOfSales.
     * 
     * @return
     *     possible object is
     *     {@link SolutionGroup.ProhibitedPointOfSales }
     *     
     */
    public SolutionGroup.ProhibitedPointOfSales getProhibitedPointOfSales() {
        return prohibitedPointOfSales;
    }

    /**
     * Define el valor de la propiedad prohibitedPointOfSales.
     * 
     * @param value
     *     allowed object is
     *     {@link SolutionGroup.ProhibitedPointOfSales }
     *     
     */
    public void setProhibitedPointOfSales(SolutionGroup.ProhibitedPointOfSales value) {
        this.prohibitedPointOfSales = value;
    }

    /**
     * Obtiene el valor de la propiedad count.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCount() {
        return count;
    }

    /**
     * Define el valor de la propiedad count.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCount(BigInteger value) {
        this.count = value;
    }

    /**
     * Obtiene el valor de la propiedad tripType.
     * 
     * @return
     *     possible object is
     *     {@link TypeTripType }
     *     
     */
    public TypeTripType getTripType() {
        return tripType;
    }

    /**
     * Define el valor de la propiedad tripType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeTripType }
     *     
     */
    public void setTripType(TypeTripType value) {
        this.tripType = value;
    }

    /**
     * Obtiene el valor de la propiedad diversification.
     * 
     * @return
     *     possible object is
     *     {@link TypeDiversity }
     *     
     */
    public TypeDiversity getDiversification() {
        return diversification;
    }

    /**
     * Define el valor de la propiedad diversification.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeDiversity }
     *     
     */
    public void setDiversification(TypeDiversity value) {
        this.diversification = value;
    }

    /**
     * Obtiene el valor de la propiedad tag.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTag() {
        return tag;
    }

    /**
     * Define el valor de la propiedad tag.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTag(String value) {
        this.tag = value;
    }

    /**
     * Obtiene el valor de la propiedad primary.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isPrimary() {
        if (primary == null) {
            return false;
        } else {
            return primary;
        }
    }

    /**
     * Define el valor de la propiedad primary.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPrimary(Boolean value) {
        this.primary = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}AccountCode" maxOccurs="999"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "accountCode"
    })
    public static class PermittedAccountCodes {

        @XmlElement(name = "AccountCode", namespace = "http://www.travelport.com/schema/common_v43_0", required = true)
        protected List<AccountCode> accountCode;

        /**
         * Gets the value of the accountCode property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the accountCode property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAccountCode().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AccountCode }
         * 
         * 
         */
        public List<AccountCode> getAccountCode() {
            if (accountCode == null) {
                accountCode = new ArrayList<AccountCode>();
            }
            return this.accountCode;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}PointOfSale" maxOccurs="999"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "pointOfSale"
    })
    public static class PermittedPointOfSales {

        @XmlElement(name = "PointOfSale", namespace = "http://www.travelport.com/schema/common_v43_0", required = true)
        protected List<PointOfSale> pointOfSale;

        /**
         * Gets the value of the pointOfSale property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the pointOfSale property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPointOfSale().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link PointOfSale }
         * 
         * 
         */
        public List<PointOfSale> getPointOfSale() {
            if (pointOfSale == null) {
                pointOfSale = new ArrayList<PointOfSale>();
            }
            return this.pointOfSale;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}AccountCode" maxOccurs="999"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "accountCode"
    })
    public static class PreferredAccountCodes {

        @XmlElement(name = "AccountCode", namespace = "http://www.travelport.com/schema/common_v43_0", required = true)
        protected List<AccountCode> accountCode;

        /**
         * Gets the value of the accountCode property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the accountCode property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAccountCode().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AccountCode }
         * 
         * 
         */
        public List<AccountCode> getAccountCode() {
            if (accountCode == null) {
                accountCode = new ArrayList<AccountCode>();
            }
            return this.accountCode;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}AccountCode" maxOccurs="999"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "accountCode"
    })
    public static class ProhibitedAccountCodes {

        @XmlElement(name = "AccountCode", namespace = "http://www.travelport.com/schema/common_v43_0", required = true)
        protected List<AccountCode> accountCode;

        /**
         * Gets the value of the accountCode property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the accountCode property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAccountCode().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AccountCode }
         * 
         * 
         */
        public List<AccountCode> getAccountCode() {
            if (accountCode == null) {
                accountCode = new ArrayList<AccountCode>();
            }
            return this.accountCode;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}PointOfSale" maxOccurs="999"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "pointOfSale"
    })
    public static class ProhibitedPointOfSales {

        @XmlElement(name = "PointOfSale", namespace = "http://www.travelport.com/schema/common_v43_0", required = true)
        protected List<PointOfSale> pointOfSale;

        /**
         * Gets the value of the pointOfSale property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the pointOfSale property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPointOfSale().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link PointOfSale }
         * 
         * 
         */
        public List<PointOfSale> getPointOfSale() {
            if (pointOfSale == null) {
                pointOfSale = new ArrayList<PointOfSale>();
            }
            return this.pointOfSale;
        }

    }

}
