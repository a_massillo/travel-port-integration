
package com.travelport.schema.air_v43_0;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.travelport.schema.common_v43_0.TypeFormOfRefund;
import com.travelport.schema.common_v43_0.TypeItineraryCode;
import com.travelport.schema.common_v43_0.TypePricingType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AdvisoryMessage" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element name="EndorsementText" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element name="WaiverText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="LowFarePricing" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="LowFareFound" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="PenaltyApplies" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="DiscountApplies" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="ItineraryType" type="{http://www.travelport.com/schema/common_v43_0}typeItineraryCode" /&gt;
 *       &lt;attribute name="ValidatingVendorCode" type="{http://www.travelport.com/schema/common_v43_0}typeCarrier" /&gt;
 *       &lt;attribute name="ForTicketingOnDate" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *       &lt;attribute name="LastDateToTicket" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *       &lt;attribute name="FormOfRefund" type="{http://www.travelport.com/schema/common_v43_0}typeFormOfRefund" /&gt;
 *       &lt;attribute name="AccountCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="BankersSellingRate" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
 *       &lt;attribute name="PricingType" type="{http://www.travelport.com/schema/common_v43_0}typePricingType" /&gt;
 *       &lt;attribute name="ConversionRate" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
 *       &lt;attribute name="RateOfExchange" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
 *       &lt;attribute name="OriginalTicketCurrency" type="{http://www.travelport.com/schema/common_v43_0}typeCurrency" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "advisoryMessage",
    "endorsementText",
    "waiverText"
})
@XmlRootElement(name = "PricingDetails")
public class PricingDetails {

    @XmlElement(name = "AdvisoryMessage")
    protected List<String> advisoryMessage;
    @XmlElement(name = "EndorsementText")
    protected List<String> endorsementText;
    @XmlElement(name = "WaiverText")
    protected String waiverText;
    @XmlAttribute(name = "LowFarePricing")
    protected Boolean lowFarePricing;
    @XmlAttribute(name = "LowFareFound")
    protected Boolean lowFareFound;
    @XmlAttribute(name = "PenaltyApplies")
    protected Boolean penaltyApplies;
    @XmlAttribute(name = "DiscountApplies")
    protected Boolean discountApplies;
    @XmlAttribute(name = "ItineraryType")
    protected TypeItineraryCode itineraryType;
    @XmlAttribute(name = "ValidatingVendorCode")
    protected String validatingVendorCode;
    @XmlAttribute(name = "ForTicketingOnDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar forTicketingOnDate;
    @XmlAttribute(name = "LastDateToTicket")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar lastDateToTicket;
    @XmlAttribute(name = "FormOfRefund")
    protected TypeFormOfRefund formOfRefund;
    @XmlAttribute(name = "AccountCode")
    protected String accountCode;
    @XmlAttribute(name = "BankersSellingRate")
    protected BigDecimal bankersSellingRate;
    @XmlAttribute(name = "PricingType")
    protected TypePricingType pricingType;
    @XmlAttribute(name = "ConversionRate")
    protected BigDecimal conversionRate;
    @XmlAttribute(name = "RateOfExchange")
    protected BigDecimal rateOfExchange;
    @XmlAttribute(name = "OriginalTicketCurrency")
    protected String originalTicketCurrency;

    /**
     * Gets the value of the advisoryMessage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the advisoryMessage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdvisoryMessage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getAdvisoryMessage() {
        if (advisoryMessage == null) {
            advisoryMessage = new ArrayList<String>();
        }
        return this.advisoryMessage;
    }

    /**
     * Gets the value of the endorsementText property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the endorsementText property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEndorsementText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getEndorsementText() {
        if (endorsementText == null) {
            endorsementText = new ArrayList<String>();
        }
        return this.endorsementText;
    }

    /**
     * Obtiene el valor de la propiedad waiverText.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWaiverText() {
        return waiverText;
    }

    /**
     * Define el valor de la propiedad waiverText.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWaiverText(String value) {
        this.waiverText = value;
    }

    /**
     * Obtiene el valor de la propiedad lowFarePricing.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isLowFarePricing() {
        if (lowFarePricing == null) {
            return false;
        } else {
            return lowFarePricing;
        }
    }

    /**
     * Define el valor de la propiedad lowFarePricing.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLowFarePricing(Boolean value) {
        this.lowFarePricing = value;
    }

    /**
     * Obtiene el valor de la propiedad lowFareFound.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isLowFareFound() {
        if (lowFareFound == null) {
            return false;
        } else {
            return lowFareFound;
        }
    }

    /**
     * Define el valor de la propiedad lowFareFound.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLowFareFound(Boolean value) {
        this.lowFareFound = value;
    }

    /**
     * Obtiene el valor de la propiedad penaltyApplies.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isPenaltyApplies() {
        if (penaltyApplies == null) {
            return false;
        } else {
            return penaltyApplies;
        }
    }

    /**
     * Define el valor de la propiedad penaltyApplies.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPenaltyApplies(Boolean value) {
        this.penaltyApplies = value;
    }

    /**
     * Obtiene el valor de la propiedad discountApplies.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isDiscountApplies() {
        if (discountApplies == null) {
            return false;
        } else {
            return discountApplies;
        }
    }

    /**
     * Define el valor de la propiedad discountApplies.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDiscountApplies(Boolean value) {
        this.discountApplies = value;
    }

    /**
     * Obtiene el valor de la propiedad itineraryType.
     * 
     * @return
     *     possible object is
     *     {@link TypeItineraryCode }
     *     
     */
    public TypeItineraryCode getItineraryType() {
        return itineraryType;
    }

    /**
     * Define el valor de la propiedad itineraryType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeItineraryCode }
     *     
     */
    public void setItineraryType(TypeItineraryCode value) {
        this.itineraryType = value;
    }

    /**
     * Obtiene el valor de la propiedad validatingVendorCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValidatingVendorCode() {
        return validatingVendorCode;
    }

    /**
     * Define el valor de la propiedad validatingVendorCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValidatingVendorCode(String value) {
        this.validatingVendorCode = value;
    }

    /**
     * Obtiene el valor de la propiedad forTicketingOnDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getForTicketingOnDate() {
        return forTicketingOnDate;
    }

    /**
     * Define el valor de la propiedad forTicketingOnDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setForTicketingOnDate(XMLGregorianCalendar value) {
        this.forTicketingOnDate = value;
    }

    /**
     * Obtiene el valor de la propiedad lastDateToTicket.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastDateToTicket() {
        return lastDateToTicket;
    }

    /**
     * Define el valor de la propiedad lastDateToTicket.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastDateToTicket(XMLGregorianCalendar value) {
        this.lastDateToTicket = value;
    }

    /**
     * Obtiene el valor de la propiedad formOfRefund.
     * 
     * @return
     *     possible object is
     *     {@link TypeFormOfRefund }
     *     
     */
    public TypeFormOfRefund getFormOfRefund() {
        return formOfRefund;
    }

    /**
     * Define el valor de la propiedad formOfRefund.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeFormOfRefund }
     *     
     */
    public void setFormOfRefund(TypeFormOfRefund value) {
        this.formOfRefund = value;
    }

    /**
     * Obtiene el valor de la propiedad accountCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountCode() {
        return accountCode;
    }

    /**
     * Define el valor de la propiedad accountCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountCode(String value) {
        this.accountCode = value;
    }

    /**
     * Obtiene el valor de la propiedad bankersSellingRate.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBankersSellingRate() {
        return bankersSellingRate;
    }

    /**
     * Define el valor de la propiedad bankersSellingRate.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBankersSellingRate(BigDecimal value) {
        this.bankersSellingRate = value;
    }

    /**
     * Obtiene el valor de la propiedad pricingType.
     * 
     * @return
     *     possible object is
     *     {@link TypePricingType }
     *     
     */
    public TypePricingType getPricingType() {
        return pricingType;
    }

    /**
     * Define el valor de la propiedad pricingType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypePricingType }
     *     
     */
    public void setPricingType(TypePricingType value) {
        this.pricingType = value;
    }

    /**
     * Obtiene el valor de la propiedad conversionRate.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getConversionRate() {
        return conversionRate;
    }

    /**
     * Define el valor de la propiedad conversionRate.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setConversionRate(BigDecimal value) {
        this.conversionRate = value;
    }

    /**
     * Obtiene el valor de la propiedad rateOfExchange.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRateOfExchange() {
        return rateOfExchange;
    }

    /**
     * Define el valor de la propiedad rateOfExchange.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRateOfExchange(BigDecimal value) {
        this.rateOfExchange = value;
    }

    /**
     * Obtiene el valor de la propiedad originalTicketCurrency.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginalTicketCurrency() {
        return originalTicketCurrency;
    }

    /**
     * Define el valor de la propiedad originalTicketCurrency.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalTicketCurrency(String value) {
        this.originalTicketCurrency = value;
    }

}
