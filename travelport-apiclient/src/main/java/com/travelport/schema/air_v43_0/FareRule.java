
package com.travelport.schema.air_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.TypeResultMessage;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FareRuleLong" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FareRuleShort" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}RuleAdvancedPurchase" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}RuleLengthOfStay" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}RuleCharges" minOccurs="0"/&gt;
 *         &lt;element name="FareRuleResultMessage" type="{http://www.travelport.com/schema/common_v43_0}typeResultMessage" maxOccurs="999" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attGroup ref="{http://www.travelport.com/schema/common_v43_0}attrProviderSupplier"/&gt;
 *       &lt;attribute name="FareInfoRef" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="RuleNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="Source" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="TariffNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fareRuleLong",
    "fareRuleShort",
    "ruleAdvancedPurchase",
    "ruleLengthOfStay",
    "ruleCharges",
    "fareRuleResultMessage"
})
@XmlRootElement(name = "FareRule")
public class FareRule {

    @XmlElement(name = "FareRuleLong")
    protected List<FareRuleLong> fareRuleLong;
    @XmlElement(name = "FareRuleShort")
    protected List<FareRuleShort> fareRuleShort;
    @XmlElement(name = "RuleAdvancedPurchase")
    protected RuleAdvancedPurchase ruleAdvancedPurchase;
    @XmlElement(name = "RuleLengthOfStay")
    protected RuleLengthOfStay ruleLengthOfStay;
    @XmlElement(name = "RuleCharges")
    protected RuleCharges ruleCharges;
    @XmlElement(name = "FareRuleResultMessage")
    protected List<TypeResultMessage> fareRuleResultMessage;
    @XmlAttribute(name = "FareInfoRef")
    protected String fareInfoRef;
    @XmlAttribute(name = "RuleNumber")
    protected String ruleNumber;
    @XmlAttribute(name = "Source")
    protected String source;
    @XmlAttribute(name = "TariffNumber")
    protected String tariffNumber;
    @XmlAttribute(name = "ProviderCode")
    protected String providerCode;
    @XmlAttribute(name = "SupplierCode")
    protected String supplierCode;

    /**
     * Gets the value of the fareRuleLong property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fareRuleLong property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFareRuleLong().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FareRuleLong }
     * 
     * 
     */
    public List<FareRuleLong> getFareRuleLong() {
        if (fareRuleLong == null) {
            fareRuleLong = new ArrayList<FareRuleLong>();
        }
        return this.fareRuleLong;
    }

    /**
     * Gets the value of the fareRuleShort property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fareRuleShort property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFareRuleShort().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FareRuleShort }
     * 
     * 
     */
    public List<FareRuleShort> getFareRuleShort() {
        if (fareRuleShort == null) {
            fareRuleShort = new ArrayList<FareRuleShort>();
        }
        return this.fareRuleShort;
    }

    /**
     * Obtiene el valor de la propiedad ruleAdvancedPurchase.
     * 
     * @return
     *     possible object is
     *     {@link RuleAdvancedPurchase }
     *     
     */
    public RuleAdvancedPurchase getRuleAdvancedPurchase() {
        return ruleAdvancedPurchase;
    }

    /**
     * Define el valor de la propiedad ruleAdvancedPurchase.
     * 
     * @param value
     *     allowed object is
     *     {@link RuleAdvancedPurchase }
     *     
     */
    public void setRuleAdvancedPurchase(RuleAdvancedPurchase value) {
        this.ruleAdvancedPurchase = value;
    }

    /**
     * Obtiene el valor de la propiedad ruleLengthOfStay.
     * 
     * @return
     *     possible object is
     *     {@link RuleLengthOfStay }
     *     
     */
    public RuleLengthOfStay getRuleLengthOfStay() {
        return ruleLengthOfStay;
    }

    /**
     * Define el valor de la propiedad ruleLengthOfStay.
     * 
     * @param value
     *     allowed object is
     *     {@link RuleLengthOfStay }
     *     
     */
    public void setRuleLengthOfStay(RuleLengthOfStay value) {
        this.ruleLengthOfStay = value;
    }

    /**
     * Obtiene el valor de la propiedad ruleCharges.
     * 
     * @return
     *     possible object is
     *     {@link RuleCharges }
     *     
     */
    public RuleCharges getRuleCharges() {
        return ruleCharges;
    }

    /**
     * Define el valor de la propiedad ruleCharges.
     * 
     * @param value
     *     allowed object is
     *     {@link RuleCharges }
     *     
     */
    public void setRuleCharges(RuleCharges value) {
        this.ruleCharges = value;
    }

    /**
     * Gets the value of the fareRuleResultMessage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fareRuleResultMessage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFareRuleResultMessage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TypeResultMessage }
     * 
     * 
     */
    public List<TypeResultMessage> getFareRuleResultMessage() {
        if (fareRuleResultMessage == null) {
            fareRuleResultMessage = new ArrayList<TypeResultMessage>();
        }
        return this.fareRuleResultMessage;
    }

    /**
     * Obtiene el valor de la propiedad fareInfoRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFareInfoRef() {
        return fareInfoRef;
    }

    /**
     * Define el valor de la propiedad fareInfoRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFareInfoRef(String value) {
        this.fareInfoRef = value;
    }

    /**
     * Obtiene el valor de la propiedad ruleNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRuleNumber() {
        return ruleNumber;
    }

    /**
     * Define el valor de la propiedad ruleNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRuleNumber(String value) {
        this.ruleNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad source.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSource() {
        return source;
    }

    /**
     * Define el valor de la propiedad source.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSource(String value) {
        this.source = value;
    }

    /**
     * Obtiene el valor de la propiedad tariffNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffNumber() {
        return tariffNumber;
    }

    /**
     * Define el valor de la propiedad tariffNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffNumber(String value) {
        this.tariffNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad providerCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProviderCode() {
        return providerCode;
    }

    /**
     * Define el valor de la propiedad providerCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProviderCode(String value) {
        this.providerCode = value;
    }

    /**
     * Obtiene el valor de la propiedad supplierCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupplierCode() {
        return supplierCode;
    }

    /**
     * Define el valor de la propiedad supplierCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupplierCode(String value) {
        this.supplierCode = value;
    }

}
