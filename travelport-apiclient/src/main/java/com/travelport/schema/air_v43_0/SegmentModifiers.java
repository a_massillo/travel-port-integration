
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}AirSegmentRef"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}TicketValidity" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}BaggageAllowance" minOccurs="0"/&gt;
 *         &lt;element name="TicketDesignator" type="{http://www.travelport.com/schema/air_v43_0}typeTicketDesignator" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "airSegmentRef",
    "ticketValidity",
    "baggageAllowance",
    "ticketDesignator"
})
@XmlRootElement(name = "SegmentModifiers")
public class SegmentModifiers {

    @XmlElement(name = "AirSegmentRef", required = true)
    protected AirSegmentRef airSegmentRef;
    @XmlElement(name = "TicketValidity")
    protected TicketValidity ticketValidity;
    @XmlElement(name = "BaggageAllowance")
    protected BaggageAllowance baggageAllowance;
    @XmlElement(name = "TicketDesignator")
    protected String ticketDesignator;

    /**
     * Obtiene el valor de la propiedad airSegmentRef.
     * 
     * @return
     *     possible object is
     *     {@link AirSegmentRef }
     *     
     */
    public AirSegmentRef getAirSegmentRef() {
        return airSegmentRef;
    }

    /**
     * Define el valor de la propiedad airSegmentRef.
     * 
     * @param value
     *     allowed object is
     *     {@link AirSegmentRef }
     *     
     */
    public void setAirSegmentRef(AirSegmentRef value) {
        this.airSegmentRef = value;
    }

    /**
     * To be used to pass the ticket validity dates
     *                         
     * 
     * @return
     *     possible object is
     *     {@link TicketValidity }
     *     
     */
    public TicketValidity getTicketValidity() {
        return ticketValidity;
    }

    /**
     * Define el valor de la propiedad ticketValidity.
     * 
     * @param value
     *     allowed object is
     *     {@link TicketValidity }
     *     
     */
    public void setTicketValidity(TicketValidity value) {
        this.ticketValidity = value;
    }

    /**
     * Obtiene el valor de la propiedad baggageAllowance.
     * 
     * @return
     *     possible object is
     *     {@link BaggageAllowance }
     *     
     */
    public BaggageAllowance getBaggageAllowance() {
        return baggageAllowance;
    }

    /**
     * Define el valor de la propiedad baggageAllowance.
     * 
     * @param value
     *     allowed object is
     *     {@link BaggageAllowance }
     *     
     */
    public void setBaggageAllowance(BaggageAllowance value) {
        this.baggageAllowance = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketDesignator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketDesignator() {
        return ticketDesignator;
    }

    /**
     * Define el valor de la propiedad ticketDesignator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketDesignator(String value) {
        this.ticketDesignator = value;
    }

}
