
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}CreditSummary" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}PrePayPriceInfo" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ProgramTitle" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="CertificateNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="ProgramName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="EffectiveDate" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *       &lt;attribute name="ExpireDate" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "creditSummary",
    "prePayPriceInfo"
})
@XmlRootElement(name = "PrePayAccount")
public class PrePayAccount {

    @XmlElement(name = "CreditSummary")
    protected CreditSummary creditSummary;
    @XmlElement(name = "PrePayPriceInfo")
    protected PrePayPriceInfo prePayPriceInfo;
    @XmlAttribute(name = "ProgramTitle")
    protected String programTitle;
    @XmlAttribute(name = "CertificateNumber")
    protected String certificateNumber;
    @XmlAttribute(name = "ProgramName")
    protected String programName;
    @XmlAttribute(name = "EffectiveDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar effectiveDate;
    @XmlAttribute(name = "ExpireDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar expireDate;

    /**
     * Obtiene el valor de la propiedad creditSummary.
     * 
     * @return
     *     possible object is
     *     {@link CreditSummary }
     *     
     */
    public CreditSummary getCreditSummary() {
        return creditSummary;
    }

    /**
     * Define el valor de la propiedad creditSummary.
     * 
     * @param value
     *     allowed object is
     *     {@link CreditSummary }
     *     
     */
    public void setCreditSummary(CreditSummary value) {
        this.creditSummary = value;
    }

    /**
     * Obtiene el valor de la propiedad prePayPriceInfo.
     * 
     * @return
     *     possible object is
     *     {@link PrePayPriceInfo }
     *     
     */
    public PrePayPriceInfo getPrePayPriceInfo() {
        return prePayPriceInfo;
    }

    /**
     * Define el valor de la propiedad prePayPriceInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link PrePayPriceInfo }
     *     
     */
    public void setPrePayPriceInfo(PrePayPriceInfo value) {
        this.prePayPriceInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad programTitle.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgramTitle() {
        return programTitle;
    }

    /**
     * Define el valor de la propiedad programTitle.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgramTitle(String value) {
        this.programTitle = value;
    }

    /**
     * Obtiene el valor de la propiedad certificateNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertificateNumber() {
        return certificateNumber;
    }

    /**
     * Define el valor de la propiedad certificateNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertificateNumber(String value) {
        this.certificateNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad programName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgramName() {
        return programName;
    }

    /**
     * Define el valor de la propiedad programName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgramName(String value) {
        this.programName = value;
    }

    /**
     * Obtiene el valor de la propiedad effectiveDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Define el valor de la propiedad effectiveDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDate(XMLGregorianCalendar value) {
        this.effectiveDate = value;
    }

    /**
     * Obtiene el valor de la propiedad expireDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpireDate() {
        return expireDate;
    }

    /**
     * Define el valor de la propiedad expireDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpireDate(XMLGregorianCalendar value) {
        this.expireDate = value;
    }

}
