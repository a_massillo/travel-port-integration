
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="Destination" use="required" type="{http://www.travelport.com/schema/air_v43_0}typeDestinationCode" /&gt;
 *       &lt;attribute name="Purpose" use="required" type="{http://www.travelport.com/schema/air_v43_0}typePurposeCode" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "DestinationPurposeCode")
public class DestinationPurposeCode {

    @XmlAttribute(name = "Destination", required = true)
    protected TypeDestinationCode destination;
    @XmlAttribute(name = "Purpose", required = true)
    protected TypePurposeCode purpose;

    /**
     * Obtiene el valor de la propiedad destination.
     * 
     * @return
     *     possible object is
     *     {@link TypeDestinationCode }
     *     
     */
    public TypeDestinationCode getDestination() {
        return destination;
    }

    /**
     * Define el valor de la propiedad destination.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeDestinationCode }
     *     
     */
    public void setDestination(TypeDestinationCode value) {
        this.destination = value;
    }

    /**
     * Obtiene el valor de la propiedad purpose.
     * 
     * @return
     *     possible object is
     *     {@link TypePurposeCode }
     *     
     */
    public TypePurposeCode getPurpose() {
        return purpose;
    }

    /**
     * Define el valor de la propiedad purpose.
     * 
     * @param value
     *     allowed object is
     *     {@link TypePurposeCode }
     *     
     */
    public void setPurpose(TypePurposeCode value) {
        this.purpose = value;
    }

}
