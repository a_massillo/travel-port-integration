
package com.travelport.schema.air_v43_0;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.TypeAdjustmentTarget;
import com.travelport.schema.common_v43_0.TypeAdjustmentType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="AppliedOn" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeAdjustmentTarget" /&gt;
 *       &lt;attribute name="AdjustmentType" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeAdjustmentType" /&gt;
 *       &lt;attribute name="Value" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
 *       &lt;attribute name="PassengerRef" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="TicketDesignator" type="{http://www.travelport.com/schema/air_v43_0}typeTicketDesignator" /&gt;
 *       &lt;attribute name="FareType" type="{http://www.travelport.com/schema/air_v43_0}typeFareTypeCode" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "ManualFareAdjustment")
public class ManualFareAdjustment {

    @XmlAttribute(name = "AppliedOn", required = true)
    protected TypeAdjustmentTarget appliedOn;
    @XmlAttribute(name = "AdjustmentType", required = true)
    protected TypeAdjustmentType adjustmentType;
    @XmlAttribute(name = "Value", required = true)
    protected BigDecimal value;
    @XmlAttribute(name = "PassengerRef")
    protected String passengerRef;
    @XmlAttribute(name = "TicketDesignator")
    protected String ticketDesignator;
    @XmlAttribute(name = "FareType")
    protected String fareType;

    /**
     * Obtiene el valor de la propiedad appliedOn.
     * 
     * @return
     *     possible object is
     *     {@link TypeAdjustmentTarget }
     *     
     */
    public TypeAdjustmentTarget getAppliedOn() {
        return appliedOn;
    }

    /**
     * Define el valor de la propiedad appliedOn.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeAdjustmentTarget }
     *     
     */
    public void setAppliedOn(TypeAdjustmentTarget value) {
        this.appliedOn = value;
    }

    /**
     * Obtiene el valor de la propiedad adjustmentType.
     * 
     * @return
     *     possible object is
     *     {@link TypeAdjustmentType }
     *     
     */
    public TypeAdjustmentType getAdjustmentType() {
        return adjustmentType;
    }

    /**
     * Define el valor de la propiedad adjustmentType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeAdjustmentType }
     *     
     */
    public void setAdjustmentType(TypeAdjustmentType value) {
        this.adjustmentType = value;
    }

    /**
     * Obtiene el valor de la propiedad value.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValue() {
        return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValue(BigDecimal value) {
        this.value = value;
    }

    /**
     * Obtiene el valor de la propiedad passengerRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassengerRef() {
        return passengerRef;
    }

    /**
     * Define el valor de la propiedad passengerRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassengerRef(String value) {
        this.passengerRef = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketDesignator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketDesignator() {
        return ticketDesignator;
    }

    /**
     * Define el valor de la propiedad ticketDesignator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketDesignator(String value) {
        this.ticketDesignator = value;
    }

    /**
     * Obtiene el valor de la propiedad fareType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFareType() {
        return fareType;
    }

    /**
     * Define el valor de la propiedad fareType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFareType(String value) {
        this.fareType = value;
    }

}
