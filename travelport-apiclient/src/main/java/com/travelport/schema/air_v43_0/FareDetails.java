
package com.travelport.schema.air_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/air_v43_0}FareTicketDesignator" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Key" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="PassengerDetailRef" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="FareBasis" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeFareBasisCode" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fareTicketDesignator"
})
@XmlRootElement(name = "FareDetails")
public class FareDetails {

    @XmlElement(name = "FareTicketDesignator")
    protected FareTicketDesignator fareTicketDesignator;
    @XmlAttribute(name = "Key", required = true)
    protected String key;
    @XmlAttribute(name = "PassengerDetailRef", required = true)
    protected String passengerDetailRef;
    @XmlAttribute(name = "FareBasis", required = true)
    protected String fareBasis;

    /**
     * Obtiene el valor de la propiedad fareTicketDesignator.
     * 
     * @return
     *     possible object is
     *     {@link FareTicketDesignator }
     *     
     */
    public FareTicketDesignator getFareTicketDesignator() {
        return fareTicketDesignator;
    }

    /**
     * Define el valor de la propiedad fareTicketDesignator.
     * 
     * @param value
     *     allowed object is
     *     {@link FareTicketDesignator }
     *     
     */
    public void setFareTicketDesignator(FareTicketDesignator value) {
        this.fareTicketDesignator = value;
    }

    /**
     * Obtiene el valor de la propiedad key.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Define el valor de la propiedad key.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Obtiene el valor de la propiedad passengerDetailRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassengerDetailRef() {
        return passengerDetailRef;
    }

    /**
     * Define el valor de la propiedad passengerDetailRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassengerDetailRef(String value) {
        this.passengerDetailRef = value;
    }

    /**
     * Obtiene el valor de la propiedad fareBasis.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFareBasis() {
        return fareBasis;
    }

    /**
     * Define el valor de la propiedad fareBasis.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFareBasis(String value) {
        this.fareBasis = value;
    }

}
