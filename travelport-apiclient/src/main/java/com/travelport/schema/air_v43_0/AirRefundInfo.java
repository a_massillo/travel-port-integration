
package com.travelport.schema.air_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.RefundRemark;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}RefundRemark" maxOccurs="999" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="RefundAmount" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="RetainAmount" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="RefundFee" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="ForfeitAmount" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="Retain" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="Refund" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "refundRemark"
})
@XmlRootElement(name = "AirRefundInfo")
public class AirRefundInfo {

    @XmlElement(name = "RefundRemark", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected List<RefundRemark> refundRemark;
    @XmlAttribute(name = "RefundAmount")
    protected String refundAmount;
    @XmlAttribute(name = "RetainAmount")
    protected String retainAmount;
    @XmlAttribute(name = "RefundFee")
    protected String refundFee;
    @XmlAttribute(name = "ForfeitAmount")
    protected String forfeitAmount;
    @XmlAttribute(name = "Retain")
    protected Boolean retain;
    @XmlAttribute(name = "Refund")
    protected Boolean refund;

    /**
     * Gets the value of the refundRemark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refundRemark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefundRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefundRemark }
     * 
     * 
     */
    public List<RefundRemark> getRefundRemark() {
        if (refundRemark == null) {
            refundRemark = new ArrayList<RefundRemark>();
        }
        return this.refundRemark;
    }

    /**
     * Obtiene el valor de la propiedad refundAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefundAmount() {
        return refundAmount;
    }

    /**
     * Define el valor de la propiedad refundAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefundAmount(String value) {
        this.refundAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad retainAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetainAmount() {
        return retainAmount;
    }

    /**
     * Define el valor de la propiedad retainAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetainAmount(String value) {
        this.retainAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad refundFee.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefundFee() {
        return refundFee;
    }

    /**
     * Define el valor de la propiedad refundFee.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefundFee(String value) {
        this.refundFee = value;
    }

    /**
     * Obtiene el valor de la propiedad forfeitAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForfeitAmount() {
        return forfeitAmount;
    }

    /**
     * Define el valor de la propiedad forfeitAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForfeitAmount(String value) {
        this.forfeitAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad retain.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isRetain() {
        if (retain == null) {
            return false;
        } else {
            return retain;
        }
    }

    /**
     * Define el valor de la propiedad retain.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetain(Boolean value) {
        this.retain = value;
    }

    /**
     * Obtiene el valor de la propiedad refund.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isRefund() {
        if (refund == null) {
            return false;
        } else {
            return refund;
        }
    }

    /**
     * Define el valor de la propiedad refund.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRefund(Boolean value) {
        this.refund = value;
    }

}
