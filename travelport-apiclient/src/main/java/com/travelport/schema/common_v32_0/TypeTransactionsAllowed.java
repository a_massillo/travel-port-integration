
package com.travelport.schema.common_v32_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para typeTransactionsAllowed complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="typeTransactionsAllowed"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v32_0}typeBookingTransactionsAllowed"&gt;
 *       &lt;attribute name="ShoppingEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="PricingEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "typeTransactionsAllowed")
@XmlSeeAlso({
    com.travelport.schema.common_v32_0.TransactionType.Air.class
})
public class TypeTransactionsAllowed
    extends TypeBookingTransactionsAllowed
{

    @XmlAttribute(name = "ShoppingEnabled")
    protected Boolean shoppingEnabled;
    @XmlAttribute(name = "PricingEnabled")
    protected Boolean pricingEnabled;

    /**
     * Obtiene el valor de la propiedad shoppingEnabled.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isShoppingEnabled() {
        return shoppingEnabled;
    }

    /**
     * Define el valor de la propiedad shoppingEnabled.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShoppingEnabled(Boolean value) {
        this.shoppingEnabled = value;
    }

    /**
     * Obtiene el valor de la propiedad pricingEnabled.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPricingEnabled() {
        return pricingEnabled;
    }

    /**
     * Define el valor de la propiedad pricingEnabled.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPricingEnabled(Boolean value) {
        this.pricingEnabled = value;
    }

}
