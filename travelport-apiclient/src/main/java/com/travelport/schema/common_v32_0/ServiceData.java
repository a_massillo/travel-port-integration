
package com.travelport.schema.common_v32_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v32_0}SeatAttributes" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v32_0}CabinClass" minOccurs="0"/&gt;
 *         &lt;element name="SSRRef" type="{http://www.travelport.com/schema/common_v32_0}typeKeyBasedReference" maxOccurs="999" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Data" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="AirSegmentRef" type="{http://www.travelport.com/schema/common_v32_0}typeRef" /&gt;
 *       &lt;attribute name="BookingTravelerRef" type="{http://www.travelport.com/schema/common_v32_0}typeRef" /&gt;
 *       &lt;attribute name="StopOver" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="TravelerType" type="{http://www.travelport.com/schema/common_v32_0}typePTC" /&gt;
 *       &lt;attribute name="EMDSummaryRef" type="{http://www.travelport.com/schema/common_v32_0}typeRef" /&gt;
 *       &lt;attribute name="EMDCouponRef" type="{http://www.travelport.com/schema/common_v32_0}typeRef" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "seatAttributes",
    "cabinClass",
    "ssrRef"
})
@XmlRootElement(name = "ServiceData")
public class ServiceData {

    @XmlElement(name = "SeatAttributes")
    protected SeatAttributes seatAttributes;
    @XmlElement(name = "CabinClass")
    protected CabinClass cabinClass;
    @XmlElement(name = "SSRRef")
    protected List<TypeKeyBasedReference> ssrRef;
    @XmlAttribute(name = "Data")
    protected String data;
    @XmlAttribute(name = "AirSegmentRef")
    protected String airSegmentRef;
    @XmlAttribute(name = "BookingTravelerRef")
    protected String bookingTravelerRef;
    @XmlAttribute(name = "StopOver")
    protected Boolean stopOver;
    @XmlAttribute(name = "TravelerType")
    protected String travelerType;
    @XmlAttribute(name = "EMDSummaryRef")
    protected String emdSummaryRef;
    @XmlAttribute(name = "EMDCouponRef")
    protected String emdCouponRef;

    /**
     * Obtiene el valor de la propiedad seatAttributes.
     * 
     * @return
     *     possible object is
     *     {@link SeatAttributes }
     *     
     */
    public SeatAttributes getSeatAttributes() {
        return seatAttributes;
    }

    /**
     * Define el valor de la propiedad seatAttributes.
     * 
     * @param value
     *     allowed object is
     *     {@link SeatAttributes }
     *     
     */
    public void setSeatAttributes(SeatAttributes value) {
        this.seatAttributes = value;
    }

    /**
     * Obtiene el valor de la propiedad cabinClass.
     * 
     * @return
     *     possible object is
     *     {@link CabinClass }
     *     
     */
    public CabinClass getCabinClass() {
        return cabinClass;
    }

    /**
     * Define el valor de la propiedad cabinClass.
     * 
     * @param value
     *     allowed object is
     *     {@link CabinClass }
     *     
     */
    public void setCabinClass(CabinClass value) {
        this.cabinClass = value;
    }

    /**
     * Gets the value of the ssrRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ssrRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSSRRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TypeKeyBasedReference }
     * 
     * 
     */
    public List<TypeKeyBasedReference> getSSRRef() {
        if (ssrRef == null) {
            ssrRef = new ArrayList<TypeKeyBasedReference>();
        }
        return this.ssrRef;
    }

    /**
     * Obtiene el valor de la propiedad data.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getData() {
        return data;
    }

    /**
     * Define el valor de la propiedad data.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setData(String value) {
        this.data = value;
    }

    /**
     * Obtiene el valor de la propiedad airSegmentRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirSegmentRef() {
        return airSegmentRef;
    }

    /**
     * Define el valor de la propiedad airSegmentRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirSegmentRef(String value) {
        this.airSegmentRef = value;
    }

    /**
     * Obtiene el valor de la propiedad bookingTravelerRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookingTravelerRef() {
        return bookingTravelerRef;
    }

    /**
     * Define el valor de la propiedad bookingTravelerRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookingTravelerRef(String value) {
        this.bookingTravelerRef = value;
    }

    /**
     * Obtiene el valor de la propiedad stopOver.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isStopOver() {
        if (stopOver == null) {
            return false;
        } else {
            return stopOver;
        }
    }

    /**
     * Define el valor de la propiedad stopOver.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStopOver(Boolean value) {
        this.stopOver = value;
    }

    /**
     * Obtiene el valor de la propiedad travelerType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTravelerType() {
        return travelerType;
    }

    /**
     * Define el valor de la propiedad travelerType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTravelerType(String value) {
        this.travelerType = value;
    }

    /**
     * Obtiene el valor de la propiedad emdSummaryRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMDSummaryRef() {
        return emdSummaryRef;
    }

    /**
     * Define el valor de la propiedad emdSummaryRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMDSummaryRef(String value) {
        this.emdSummaryRef = value;
    }

    /**
     * Obtiene el valor de la propiedad emdCouponRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMDCouponRef() {
        return emdCouponRef;
    }

    /**
     * Define el valor de la propiedad emdCouponRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMDCouponRef(String value) {
        this.emdCouponRef = value;
    }

}
