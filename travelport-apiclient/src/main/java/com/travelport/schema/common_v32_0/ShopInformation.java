
package com.travelport.schema.common_v32_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SearchRequest" maxOccurs="999" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="Origin" type="{http://www.travelport.com/schema/common_v32_0}typeIATACode" /&gt;
 *                 &lt;attribute name="Destination" type="{http://www.travelport.com/schema/common_v32_0}typeIATACode" /&gt;
 *                 &lt;attribute name="DepartureTime" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="ClassOfService" type="{http://www.travelport.com/schema/common_v32_0}typeClassOfService" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="FlightsOffered" maxOccurs="999" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="Origin" type="{http://www.travelport.com/schema/common_v32_0}typeIATACode" /&gt;
 *                 &lt;attribute name="Destination" type="{http://www.travelport.com/schema/common_v32_0}typeIATACode" /&gt;
 *                 &lt;attribute name="DepartureTime" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="TravelOrder" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *                 &lt;attribute name="Carrier" type="{http://www.travelport.com/schema/common_v32_0}typeCarrier" /&gt;
 *                 &lt;attribute name="FlightNumber" type="{http://www.travelport.com/schema/common_v32_0}typeFlightNumber" /&gt;
 *                 &lt;attribute name="ClassOfService" type="{http://www.travelport.com/schema/common_v32_0}typeClassOfService" /&gt;
 *                 &lt;attribute name="StopOver" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *                 &lt;attribute name="Connection" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="CabinShopped" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="CabinSelected" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="LowestFareOffered" type="{http://www.travelport.com/schema/common_v32_0}typeMoney" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchRequest",
    "flightsOffered"
})
@XmlRootElement(name = "ShopInformation")
public class ShopInformation {

    @XmlElement(name = "SearchRequest")
    protected List<ShopInformation.SearchRequest> searchRequest;
    @XmlElement(name = "FlightsOffered")
    protected List<ShopInformation.FlightsOffered> flightsOffered;
    @XmlAttribute(name = "CabinShopped")
    protected String cabinShopped;
    @XmlAttribute(name = "CabinSelected")
    protected String cabinSelected;
    @XmlAttribute(name = "LowestFareOffered")
    protected String lowestFareOffered;

    /**
     * Gets the value of the searchRequest property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the searchRequest property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchRequest().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShopInformation.SearchRequest }
     * 
     * 
     */
    public List<ShopInformation.SearchRequest> getSearchRequest() {
        if (searchRequest == null) {
            searchRequest = new ArrayList<ShopInformation.SearchRequest>();
        }
        return this.searchRequest;
    }

    /**
     * Gets the value of the flightsOffered property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the flightsOffered property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFlightsOffered().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShopInformation.FlightsOffered }
     * 
     * 
     */
    public List<ShopInformation.FlightsOffered> getFlightsOffered() {
        if (flightsOffered == null) {
            flightsOffered = new ArrayList<ShopInformation.FlightsOffered>();
        }
        return this.flightsOffered;
    }

    /**
     * Obtiene el valor de la propiedad cabinShopped.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCabinShopped() {
        return cabinShopped;
    }

    /**
     * Define el valor de la propiedad cabinShopped.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCabinShopped(String value) {
        this.cabinShopped = value;
    }

    /**
     * Obtiene el valor de la propiedad cabinSelected.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCabinSelected() {
        return cabinSelected;
    }

    /**
     * Define el valor de la propiedad cabinSelected.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCabinSelected(String value) {
        this.cabinSelected = value;
    }

    /**
     * Obtiene el valor de la propiedad lowestFareOffered.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLowestFareOffered() {
        return lowestFareOffered;
    }

    /**
     * Define el valor de la propiedad lowestFareOffered.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLowestFareOffered(String value) {
        this.lowestFareOffered = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attribute name="Origin" type="{http://www.travelport.com/schema/common_v32_0}typeIATACode" /&gt;
     *       &lt;attribute name="Destination" type="{http://www.travelport.com/schema/common_v32_0}typeIATACode" /&gt;
     *       &lt;attribute name="DepartureTime" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="TravelOrder" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
     *       &lt;attribute name="Carrier" type="{http://www.travelport.com/schema/common_v32_0}typeCarrier" /&gt;
     *       &lt;attribute name="FlightNumber" type="{http://www.travelport.com/schema/common_v32_0}typeFlightNumber" /&gt;
     *       &lt;attribute name="ClassOfService" type="{http://www.travelport.com/schema/common_v32_0}typeClassOfService" /&gt;
     *       &lt;attribute name="StopOver" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
     *       &lt;attribute name="Connection" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class FlightsOffered {

        @XmlAttribute(name = "Origin")
        protected String origin;
        @XmlAttribute(name = "Destination")
        protected String destination;
        @XmlAttribute(name = "DepartureTime")
        protected String departureTime;
        @XmlAttribute(name = "TravelOrder")
        protected Integer travelOrder;
        @XmlAttribute(name = "Carrier")
        protected String carrier;
        @XmlAttribute(name = "FlightNumber")
        protected String flightNumber;
        @XmlAttribute(name = "ClassOfService")
        protected String classOfService;
        @XmlAttribute(name = "StopOver")
        protected Boolean stopOver;
        @XmlAttribute(name = "Connection")
        protected Boolean connection;

        /**
         * Obtiene el valor de la propiedad origin.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOrigin() {
            return origin;
        }

        /**
         * Define el valor de la propiedad origin.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOrigin(String value) {
            this.origin = value;
        }

        /**
         * Obtiene el valor de la propiedad destination.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDestination() {
            return destination;
        }

        /**
         * Define el valor de la propiedad destination.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDestination(String value) {
            this.destination = value;
        }

        /**
         * Obtiene el valor de la propiedad departureTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDepartureTime() {
            return departureTime;
        }

        /**
         * Define el valor de la propiedad departureTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDepartureTime(String value) {
            this.departureTime = value;
        }

        /**
         * Obtiene el valor de la propiedad travelOrder.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getTravelOrder() {
            return travelOrder;
        }

        /**
         * Define el valor de la propiedad travelOrder.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setTravelOrder(Integer value) {
            this.travelOrder = value;
        }

        /**
         * Obtiene el valor de la propiedad carrier.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCarrier() {
            return carrier;
        }

        /**
         * Define el valor de la propiedad carrier.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCarrier(String value) {
            this.carrier = value;
        }

        /**
         * Obtiene el valor de la propiedad flightNumber.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFlightNumber() {
            return flightNumber;
        }

        /**
         * Define el valor de la propiedad flightNumber.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFlightNumber(String value) {
            this.flightNumber = value;
        }

        /**
         * Obtiene el valor de la propiedad classOfService.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getClassOfService() {
            return classOfService;
        }

        /**
         * Define el valor de la propiedad classOfService.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setClassOfService(String value) {
            this.classOfService = value;
        }

        /**
         * Obtiene el valor de la propiedad stopOver.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public boolean isStopOver() {
            if (stopOver == null) {
                return false;
            } else {
                return stopOver;
            }
        }

        /**
         * Define el valor de la propiedad stopOver.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setStopOver(Boolean value) {
            this.stopOver = value;
        }

        /**
         * Obtiene el valor de la propiedad connection.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public boolean isConnection() {
            if (connection == null) {
                return false;
            } else {
                return connection;
            }
        }

        /**
         * Define el valor de la propiedad connection.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setConnection(Boolean value) {
            this.connection = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attribute name="Origin" type="{http://www.travelport.com/schema/common_v32_0}typeIATACode" /&gt;
     *       &lt;attribute name="Destination" type="{http://www.travelport.com/schema/common_v32_0}typeIATACode" /&gt;
     *       &lt;attribute name="DepartureTime" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="ClassOfService" type="{http://www.travelport.com/schema/common_v32_0}typeClassOfService" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class SearchRequest {

        @XmlAttribute(name = "Origin")
        protected String origin;
        @XmlAttribute(name = "Destination")
        protected String destination;
        @XmlAttribute(name = "DepartureTime")
        protected String departureTime;
        @XmlAttribute(name = "ClassOfService")
        protected String classOfService;

        /**
         * Obtiene el valor de la propiedad origin.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOrigin() {
            return origin;
        }

        /**
         * Define el valor de la propiedad origin.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOrigin(String value) {
            this.origin = value;
        }

        /**
         * Obtiene el valor de la propiedad destination.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDestination() {
            return destination;
        }

        /**
         * Define el valor de la propiedad destination.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDestination(String value) {
            this.destination = value;
        }

        /**
         * Obtiene el valor de la propiedad departureTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDepartureTime() {
            return departureTime;
        }

        /**
         * Define el valor de la propiedad departureTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDepartureTime(String value) {
            this.departureTime = value;
        }

        /**
         * Obtiene el valor de la propiedad classOfService.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getClassOfService() {
            return classOfService;
        }

        /**
         * Define el valor de la propiedad classOfService.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setClassOfService(String value) {
            this.classOfService = value;
        }

    }

}
