
package com.travelport.schema.common_v32_0;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="OriginApplication" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="CIDBNumber"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer"&gt;
 *             &lt;pattern value="\d{10}"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "BillingPointOfSaleInfo")
public class BillingPointOfSaleInfo {

    @XmlAttribute(name = "OriginApplication", required = true)
    protected String originApplication;
    @XmlAttribute(name = "CIDBNumber")
    protected BigInteger cidbNumber;

    /**
     * Obtiene el valor de la propiedad originApplication.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginApplication() {
        return originApplication;
    }

    /**
     * Define el valor de la propiedad originApplication.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginApplication(String value) {
        this.originApplication = value;
    }

    /**
     * Obtiene el valor de la propiedad cidbNumber.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCIDBNumber() {
        return cidbNumber;
    }

    /**
     * Define el valor de la propiedad cidbNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCIDBNumber(BigInteger value) {
        this.cidbNumber = value;
    }

}
