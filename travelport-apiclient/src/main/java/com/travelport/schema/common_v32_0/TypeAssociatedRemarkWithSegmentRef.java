
package com.travelport.schema.common_v32_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * A textual remark container to hold Associated itinerary remarks with segment association
 * 
 * <p>Clase Java para typeAssociatedRemarkWithSegmentRef complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="typeAssociatedRemarkWithSegmentRef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v32_0}typeAssociatedRemark"&gt;
 *       &lt;attribute name="SegmentRef" type="{http://www.travelport.com/schema/common_v32_0}typeRef" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "typeAssociatedRemarkWithSegmentRef")
public class TypeAssociatedRemarkWithSegmentRef
    extends TypeAssociatedRemark
{

    @XmlAttribute(name = "SegmentRef")
    protected String segmentRef;

    /**
     * Obtiene el valor de la propiedad segmentRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSegmentRef() {
        return segmentRef;
    }

    /**
     * Define el valor de la propiedad segmentRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSegmentRef(String value) {
        this.segmentRef = value;
    }

}
