
package com.travelport.schema.common_v32_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * ProfileEntityID and ProfileLevel together identity a profile entity.
 * 
 * <p>Clase Java para typeProfileRef complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="typeProfileRef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="ProfileEntityID" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="ProfileLevel" use="required" type="{http://www.travelport.com/schema/common_v32_0}typeProfileLevel" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "typeProfileRef")
public class TypeProfileRef {

    @XmlAttribute(name = "ProfileEntityID", required = true)
    protected String profileEntityID;
    @XmlAttribute(name = "ProfileLevel", required = true)
    protected TypeProfileLevel profileLevel;

    /**
     * Obtiene el valor de la propiedad profileEntityID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfileEntityID() {
        return profileEntityID;
    }

    /**
     * Define el valor de la propiedad profileEntityID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfileEntityID(String value) {
        this.profileEntityID = value;
    }

    /**
     * Obtiene el valor de la propiedad profileLevel.
     * 
     * @return
     *     possible object is
     *     {@link TypeProfileLevel }
     *     
     */
    public TypeProfileLevel getProfileLevel() {
        return profileLevel;
    }

    /**
     * Define el valor de la propiedad profileLevel.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeProfileLevel }
     *     
     */
    public void setProfileLevel(TypeProfileLevel value) {
        this.profileLevel = value;
    }

}
