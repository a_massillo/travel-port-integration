
package com.travelport.schema.common_v32_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v32_0}typeAssociatedRemark"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="AirSegmentRef" type="{http://www.travelport.com/schema/common_v32_0}typeSegmentRef" minOccurs="0"/&gt;
 *         &lt;element name="HotelReservationRef" type="{http://www.travelport.com/schema/common_v32_0}typeNonAirReservationRef" minOccurs="0"/&gt;
 *         &lt;element name="VehicleReservationRef" type="{http://www.travelport.com/schema/common_v32_0}typeNonAirReservationRef" minOccurs="0"/&gt;
 *         &lt;element name="PassiveSegmentRef" type="{http://www.travelport.com/schema/common_v32_0}typeSegmentRef" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "airSegmentRef",
    "hotelReservationRef",
    "vehicleReservationRef",
    "passiveSegmentRef"
})
@XmlRootElement(name = "InvoiceRemark")
public class InvoiceRemark
    extends TypeAssociatedRemark
{

    @XmlElement(name = "AirSegmentRef")
    protected TypeSegmentRef airSegmentRef;
    @XmlElement(name = "HotelReservationRef")
    protected TypeNonAirReservationRef hotelReservationRef;
    @XmlElement(name = "VehicleReservationRef")
    protected TypeNonAirReservationRef vehicleReservationRef;
    @XmlElement(name = "PassiveSegmentRef")
    protected TypeSegmentRef passiveSegmentRef;

    /**
     * Obtiene el valor de la propiedad airSegmentRef.
     * 
     * @return
     *     possible object is
     *     {@link TypeSegmentRef }
     *     
     */
    public TypeSegmentRef getAirSegmentRef() {
        return airSegmentRef;
    }

    /**
     * Define el valor de la propiedad airSegmentRef.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeSegmentRef }
     *     
     */
    public void setAirSegmentRef(TypeSegmentRef value) {
        this.airSegmentRef = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelReservationRef.
     * 
     * @return
     *     possible object is
     *     {@link TypeNonAirReservationRef }
     *     
     */
    public TypeNonAirReservationRef getHotelReservationRef() {
        return hotelReservationRef;
    }

    /**
     * Define el valor de la propiedad hotelReservationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeNonAirReservationRef }
     *     
     */
    public void setHotelReservationRef(TypeNonAirReservationRef value) {
        this.hotelReservationRef = value;
    }

    /**
     * Obtiene el valor de la propiedad vehicleReservationRef.
     * 
     * @return
     *     possible object is
     *     {@link TypeNonAirReservationRef }
     *     
     */
    public TypeNonAirReservationRef getVehicleReservationRef() {
        return vehicleReservationRef;
    }

    /**
     * Define el valor de la propiedad vehicleReservationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeNonAirReservationRef }
     *     
     */
    public void setVehicleReservationRef(TypeNonAirReservationRef value) {
        this.vehicleReservationRef = value;
    }

    /**
     * Obtiene el valor de la propiedad passiveSegmentRef.
     * 
     * @return
     *     possible object is
     *     {@link TypeSegmentRef }
     *     
     */
    public TypeSegmentRef getPassiveSegmentRef() {
        return passiveSegmentRef;
    }

    /**
     * Define el valor de la propiedad passiveSegmentRef.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeSegmentRef }
     *     
     */
    public void setPassiveSegmentRef(TypeSegmentRef value) {
        this.passiveSegmentRef = value;
    }

}
