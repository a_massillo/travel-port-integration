
package com.travelport.schema.common_v32_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PseudoCityCode" type="{http://www.travelport.com/schema/common_v32_0}typePCC" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v32_0}QueueSelector" maxOccurs="999" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pseudoCityCode",
    "queueSelector"
})
@XmlRootElement(name = "QueuePlace")
public class QueuePlace {

    @XmlElement(name = "PseudoCityCode")
    protected String pseudoCityCode;
    @XmlElement(name = "QueueSelector")
    protected List<QueueSelector> queueSelector;

    /**
     * Obtiene el valor de la propiedad pseudoCityCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPseudoCityCode() {
        return pseudoCityCode;
    }

    /**
     * Define el valor de la propiedad pseudoCityCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPseudoCityCode(String value) {
        this.pseudoCityCode = value;
    }

    /**
     * Identifies the Queue Information to be selected for placing the UR Gets the value of the queueSelector property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the queueSelector property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQueueSelector().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueueSelector }
     * 
     * 
     */
    public List<QueueSelector> getQueueSelector() {
        if (queueSelector == null) {
            queueSelector = new ArrayList<QueueSelector>();
        }
        return this.queueSelector;
    }

}
