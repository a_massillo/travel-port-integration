
package com.travelport.schema.common_v32_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * Information pertaining to the payment of a Vehicle Rental.
 * 
 * <p>Clase Java para typeVoucherInformation complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="typeVoucherInformation"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="VoucherType" use="required" type="{http://www.travelport.com/schema/common_v32_0}typeVoucherType" /&gt;
 *       &lt;attribute name="Amount" type="{http://www.travelport.com/schema/common_v32_0}typeMoney" /&gt;
 *       &lt;attribute name="ConfirmationNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="AccountName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="Number" type="{http://www.travelport.com/schema/common_v32_0}StringLength1to16" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "typeVoucherInformation")
public class TypeVoucherInformation {

    @XmlAttribute(name = "VoucherType", required = true)
    protected TypeVoucherType voucherType;
    @XmlAttribute(name = "Amount")
    protected String amount;
    @XmlAttribute(name = "ConfirmationNumber")
    protected String confirmationNumber;
    @XmlAttribute(name = "AccountName")
    protected String accountName;
    @XmlAttribute(name = "Number")
    protected String number;

    /**
     * Obtiene el valor de la propiedad voucherType.
     * 
     * @return
     *     possible object is
     *     {@link TypeVoucherType }
     *     
     */
    public TypeVoucherType getVoucherType() {
        return voucherType;
    }

    /**
     * Define el valor de la propiedad voucherType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeVoucherType }
     *     
     */
    public void setVoucherType(TypeVoucherType value) {
        this.voucherType = value;
    }

    /**
     * Obtiene el valor de la propiedad amount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmount() {
        return amount;
    }

    /**
     * Define el valor de la propiedad amount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmount(String value) {
        this.amount = value;
    }

    /**
     * Obtiene el valor de la propiedad confirmationNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfirmationNumber() {
        return confirmationNumber;
    }

    /**
     * Define el valor de la propiedad confirmationNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfirmationNumber(String value) {
        this.confirmationNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad accountName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     * Define el valor de la propiedad accountName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountName(String value) {
        this.accountName = value;
    }

    /**
     * Obtiene el valor de la propiedad number.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumber() {
        return number;
    }

    /**
     * Define el valor de la propiedad number.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumber(String value) {
        this.number = value;
    }

}
