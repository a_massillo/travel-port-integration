
package com.travelport.schema.common_v32_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element ref="{http://www.travelport.com/schema/common_v32_0}CreditCard" minOccurs="0"/&gt;
 *           &lt;element ref="{http://www.travelport.com/schema/common_v32_0}DebitCard" minOccurs="0"/&gt;
 *           &lt;element ref="{http://www.travelport.com/schema/common_v32_0}EnettVan" minOccurs="0"/&gt;
 *           &lt;group ref="{http://www.travelport.com/schema/common_v32_0}FormOfPaymentSequenceGroup"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="ProviderReservationInfoRef" type="{http://www.travelport.com/schema/common_v32_0}typeFormOfPaymentPNRReference" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element name="SegmentRef" type="{http://www.travelport.com/schema/common_v32_0}typeGeneralReference" maxOccurs="999" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attGroup ref="{http://www.travelport.com/schema/common_v32_0}attrElementKeyResults"/&gt;
 *       &lt;attribute name="Key" type="{http://www.travelport.com/schema/common_v32_0}typeRef" /&gt;
 *       &lt;attribute name="Type" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="25"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="FulfillmentType" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="FulfillmentLocation" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="FulfillmentIDType" type="{http://www.travelport.com/schema/common_v32_0}typeFulfillmentIDType" /&gt;
 *       &lt;attribute name="FulfillmentIDNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="IsAgentType" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="AgentText" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="ReuseFOP" type="{http://www.travelport.com/schema/common_v32_0}typeRef" /&gt;
 *       &lt;attribute name="ExternalReference" type="{http://www.travelport.com/schema/common_v32_0}typeExternalReference" /&gt;
 *       &lt;attribute name="Reusable" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="ProfileID" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="ProfileKey" type="{http://www.travelport.com/schema/common_v32_0}typeRef" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "creditCard",
    "debitCard",
    "enettVan",
    "certificate",
    "ticketNumber",
    "check",
    "requisition",
    "miscFormOfPayment",
    "agencyPayment",
    "unitedNations",
    "directPayment",
    "agentVoucher",
    "paymentAdvice",
    "providerReservationInfoRef",
    "segmentRef"
})
@XmlRootElement(name = "FormOfPayment")
public class FormOfPayment {

    @XmlElement(name = "CreditCard")
    protected CreditCard creditCard;
    @XmlElement(name = "DebitCard")
    protected DebitCard debitCard;
    @XmlElement(name = "EnettVan")
    protected EnettVan enettVan;
    @XmlElement(name = "Certificate")
    protected Certificate certificate;
    @XmlElement(name = "TicketNumber")
    protected String ticketNumber;
    @XmlElement(name = "Check")
    protected Check check;
    @XmlElement(name = "Requisition")
    protected Requisition requisition;
    @XmlElement(name = "MiscFormOfPayment")
    protected MiscFormOfPayment miscFormOfPayment;
    @XmlElement(name = "AgencyPayment")
    protected TypeAgencyPayment agencyPayment;
    @XmlElement(name = "UnitedNations")
    protected UnitedNations unitedNations;
    @XmlElement(name = "DirectPayment")
    protected DirectPayment directPayment;
    @XmlElement(name = "AgentVoucher")
    protected AgentVoucher agentVoucher;
    @XmlElement(name = "PaymentAdvice")
    protected PaymentAdvice paymentAdvice;
    @XmlElement(name = "ProviderReservationInfoRef")
    protected List<TypeFormOfPaymentPNRReference> providerReservationInfoRef;
    @XmlElement(name = "SegmentRef")
    protected List<TypeGeneralReference> segmentRef;
    @XmlAttribute(name = "Key")
    protected String key;
    @XmlAttribute(name = "Type", required = true)
    protected String type;
    @XmlAttribute(name = "FulfillmentType")
    protected String fulfillmentType;
    @XmlAttribute(name = "FulfillmentLocation")
    protected String fulfillmentLocation;
    @XmlAttribute(name = "FulfillmentIDType")
    protected TypeFulfillmentIDType fulfillmentIDType;
    @XmlAttribute(name = "FulfillmentIDNumber")
    protected String fulfillmentIDNumber;
    @XmlAttribute(name = "IsAgentType")
    protected Boolean isAgentType;
    @XmlAttribute(name = "AgentText")
    protected String agentText;
    @XmlAttribute(name = "ReuseFOP")
    protected String reuseFOP;
    @XmlAttribute(name = "ExternalReference")
    protected String externalReference;
    @XmlAttribute(name = "Reusable")
    protected Boolean reusable;
    @XmlAttribute(name = "ProfileID")
    protected String profileID;
    @XmlAttribute(name = "ProfileKey")
    protected String profileKey;
    @XmlAttribute(name = "ElStat")
    protected TypeElementStatus elStat;
    @XmlAttribute(name = "KeyOverride")
    protected Boolean keyOverride;

    /**
     * Obtiene el valor de la propiedad creditCard.
     * 
     * @return
     *     possible object is
     *     {@link CreditCard }
     *     
     */
    public CreditCard getCreditCard() {
        return creditCard;
    }

    /**
     * Define el valor de la propiedad creditCard.
     * 
     * @param value
     *     allowed object is
     *     {@link CreditCard }
     *     
     */
    public void setCreditCard(CreditCard value) {
        this.creditCard = value;
    }

    /**
     * Obtiene el valor de la propiedad debitCard.
     * 
     * @return
     *     possible object is
     *     {@link DebitCard }
     *     
     */
    public DebitCard getDebitCard() {
        return debitCard;
    }

    /**
     * Define el valor de la propiedad debitCard.
     * 
     * @param value
     *     allowed object is
     *     {@link DebitCard }
     *     
     */
    public void setDebitCard(DebitCard value) {
        this.debitCard = value;
    }

    /**
     * Obtiene el valor de la propiedad enettVan.
     * 
     * @return
     *     possible object is
     *     {@link EnettVan }
     *     
     */
    public EnettVan getEnettVan() {
        return enettVan;
    }

    /**
     * Define el valor de la propiedad enettVan.
     * 
     * @param value
     *     allowed object is
     *     {@link EnettVan }
     *     
     */
    public void setEnettVan(EnettVan value) {
        this.enettVan = value;
    }

    /**
     * Obtiene el valor de la propiedad certificate.
     * 
     * @return
     *     possible object is
     *     {@link Certificate }
     *     
     */
    public Certificate getCertificate() {
        return certificate;
    }

    /**
     * Define el valor de la propiedad certificate.
     * 
     * @param value
     *     allowed object is
     *     {@link Certificate }
     *     
     */
    public void setCertificate(Certificate value) {
        this.certificate = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketNumber() {
        return ticketNumber;
    }

    /**
     * Define el valor de la propiedad ticketNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketNumber(String value) {
        this.ticketNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad check.
     * 
     * @return
     *     possible object is
     *     {@link Check }
     *     
     */
    public Check getCheck() {
        return check;
    }

    /**
     * Define el valor de la propiedad check.
     * 
     * @param value
     *     allowed object is
     *     {@link Check }
     *     
     */
    public void setCheck(Check value) {
        this.check = value;
    }

    /**
     * Obtiene el valor de la propiedad requisition.
     * 
     * @return
     *     possible object is
     *     {@link Requisition }
     *     
     */
    public Requisition getRequisition() {
        return requisition;
    }

    /**
     * Define el valor de la propiedad requisition.
     * 
     * @param value
     *     allowed object is
     *     {@link Requisition }
     *     
     */
    public void setRequisition(Requisition value) {
        this.requisition = value;
    }

    /**
     * Obtiene el valor de la propiedad miscFormOfPayment.
     * 
     * @return
     *     possible object is
     *     {@link MiscFormOfPayment }
     *     
     */
    public MiscFormOfPayment getMiscFormOfPayment() {
        return miscFormOfPayment;
    }

    /**
     * Define el valor de la propiedad miscFormOfPayment.
     * 
     * @param value
     *     allowed object is
     *     {@link MiscFormOfPayment }
     *     
     */
    public void setMiscFormOfPayment(MiscFormOfPayment value) {
        this.miscFormOfPayment = value;
    }

    /**
     * Obtiene el valor de la propiedad agencyPayment.
     * 
     * @return
     *     possible object is
     *     {@link TypeAgencyPayment }
     *     
     */
    public TypeAgencyPayment getAgencyPayment() {
        return agencyPayment;
    }

    /**
     * Define el valor de la propiedad agencyPayment.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeAgencyPayment }
     *     
     */
    public void setAgencyPayment(TypeAgencyPayment value) {
        this.agencyPayment = value;
    }

    /**
     * Obtiene el valor de la propiedad unitedNations.
     * 
     * @return
     *     possible object is
     *     {@link UnitedNations }
     *     
     */
    public UnitedNations getUnitedNations() {
        return unitedNations;
    }

    /**
     * Define el valor de la propiedad unitedNations.
     * 
     * @param value
     *     allowed object is
     *     {@link UnitedNations }
     *     
     */
    public void setUnitedNations(UnitedNations value) {
        this.unitedNations = value;
    }

    /**
     * Obtiene el valor de la propiedad directPayment.
     * 
     * @return
     *     possible object is
     *     {@link DirectPayment }
     *     
     */
    public DirectPayment getDirectPayment() {
        return directPayment;
    }

    /**
     * Define el valor de la propiedad directPayment.
     * 
     * @param value
     *     allowed object is
     *     {@link DirectPayment }
     *     
     */
    public void setDirectPayment(DirectPayment value) {
        this.directPayment = value;
    }

    /**
     * Obtiene el valor de la propiedad agentVoucher.
     * 
     * @return
     *     possible object is
     *     {@link AgentVoucher }
     *     
     */
    public AgentVoucher getAgentVoucher() {
        return agentVoucher;
    }

    /**
     * Define el valor de la propiedad agentVoucher.
     * 
     * @param value
     *     allowed object is
     *     {@link AgentVoucher }
     *     
     */
    public void setAgentVoucher(AgentVoucher value) {
        this.agentVoucher = value;
    }

    /**
     * Obtiene el valor de la propiedad paymentAdvice.
     * 
     * @return
     *     possible object is
     *     {@link PaymentAdvice }
     *     
     */
    public PaymentAdvice getPaymentAdvice() {
        return paymentAdvice;
    }

    /**
     * Define el valor de la propiedad paymentAdvice.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentAdvice }
     *     
     */
    public void setPaymentAdvice(PaymentAdvice value) {
        this.paymentAdvice = value;
    }

    /**
     * Gets the value of the providerReservationInfoRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the providerReservationInfoRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProviderReservationInfoRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TypeFormOfPaymentPNRReference }
     * 
     * 
     */
    public List<TypeFormOfPaymentPNRReference> getProviderReservationInfoRef() {
        if (providerReservationInfoRef == null) {
            providerReservationInfoRef = new ArrayList<TypeFormOfPaymentPNRReference>();
        }
        return this.providerReservationInfoRef;
    }

    /**
     * Gets the value of the segmentRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the segmentRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSegmentRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TypeGeneralReference }
     * 
     * 
     */
    public List<TypeGeneralReference> getSegmentRef() {
        if (segmentRef == null) {
            segmentRef = new ArrayList<TypeGeneralReference>();
        }
        return this.segmentRef;
    }

    /**
     * Obtiene el valor de la propiedad key.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Define el valor de la propiedad key.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Obtiene el valor de la propiedad type.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Define el valor de la propiedad type.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Obtiene el valor de la propiedad fulfillmentType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFulfillmentType() {
        return fulfillmentType;
    }

    /**
     * Define el valor de la propiedad fulfillmentType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFulfillmentType(String value) {
        this.fulfillmentType = value;
    }

    /**
     * Obtiene el valor de la propiedad fulfillmentLocation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFulfillmentLocation() {
        return fulfillmentLocation;
    }

    /**
     * Define el valor de la propiedad fulfillmentLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFulfillmentLocation(String value) {
        this.fulfillmentLocation = value;
    }

    /**
     * Obtiene el valor de la propiedad fulfillmentIDType.
     * 
     * @return
     *     possible object is
     *     {@link TypeFulfillmentIDType }
     *     
     */
    public TypeFulfillmentIDType getFulfillmentIDType() {
        return fulfillmentIDType;
    }

    /**
     * Define el valor de la propiedad fulfillmentIDType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeFulfillmentIDType }
     *     
     */
    public void setFulfillmentIDType(TypeFulfillmentIDType value) {
        this.fulfillmentIDType = value;
    }

    /**
     * Obtiene el valor de la propiedad fulfillmentIDNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFulfillmentIDNumber() {
        return fulfillmentIDNumber;
    }

    /**
     * Define el valor de la propiedad fulfillmentIDNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFulfillmentIDNumber(String value) {
        this.fulfillmentIDNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad isAgentType.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIsAgentType() {
        if (isAgentType == null) {
            return false;
        } else {
            return isAgentType;
        }
    }

    /**
     * Define el valor de la propiedad isAgentType.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsAgentType(Boolean value) {
        this.isAgentType = value;
    }

    /**
     * Obtiene el valor de la propiedad agentText.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentText() {
        return agentText;
    }

    /**
     * Define el valor de la propiedad agentText.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentText(String value) {
        this.agentText = value;
    }

    /**
     * Obtiene el valor de la propiedad reuseFOP.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReuseFOP() {
        return reuseFOP;
    }

    /**
     * Define el valor de la propiedad reuseFOP.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReuseFOP(String value) {
        this.reuseFOP = value;
    }

    /**
     * Obtiene el valor de la propiedad externalReference.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalReference() {
        return externalReference;
    }

    /**
     * Define el valor de la propiedad externalReference.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalReference(String value) {
        this.externalReference = value;
    }

    /**
     * Obtiene el valor de la propiedad reusable.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isReusable() {
        if (reusable == null) {
            return false;
        } else {
            return reusable;
        }
    }

    /**
     * Define el valor de la propiedad reusable.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReusable(Boolean value) {
        this.reusable = value;
    }

    /**
     * Obtiene el valor de la propiedad profileID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfileID() {
        return profileID;
    }

    /**
     * Define el valor de la propiedad profileID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfileID(String value) {
        this.profileID = value;
    }

    /**
     * Obtiene el valor de la propiedad profileKey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfileKey() {
        return profileKey;
    }

    /**
     * Define el valor de la propiedad profileKey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfileKey(String value) {
        this.profileKey = value;
    }

    /**
     * Obtiene el valor de la propiedad elStat.
     * 
     * @return
     *     possible object is
     *     {@link TypeElementStatus }
     *     
     */
    public TypeElementStatus getElStat() {
        return elStat;
    }

    /**
     * Define el valor de la propiedad elStat.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeElementStatus }
     *     
     */
    public void setElStat(TypeElementStatus value) {
        this.elStat = value;
    }

    /**
     * Obtiene el valor de la propiedad keyOverride.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isKeyOverride() {
        return keyOverride;
    }

    /**
     * Define el valor de la propiedad keyOverride.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setKeyOverride(Boolean value) {
        this.keyOverride = value;
    }

}
