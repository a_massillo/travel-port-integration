
package com.travelport.schema.common_v32_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Air" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.travelport.com/schema/common_v32_0}typeTransactionsAllowed"&gt;
 *                 &lt;attGroup ref="{http://www.travelport.com/schema/common_v32_0}attrFlexShopping"/&gt;
 *                 &lt;attribute name="OneWayShop" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                 &lt;attribute name="FlexExplore" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                 &lt;attribute name="RapidRepriceEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Hotel" type="{http://www.travelport.com/schema/common_v32_0}typeTransactionsAllowed" minOccurs="0"/&gt;
 *         &lt;element name="Rail" type="{http://www.travelport.com/schema/common_v32_0}typeTransactionsAllowed" minOccurs="0"/&gt;
 *         &lt;element name="Vehicle" type="{http://www.travelport.com/schema/common_v32_0}typeTransactionsAllowed" minOccurs="0"/&gt;
 *         &lt;element name="Passive" type="{http://www.travelport.com/schema/common_v32_0}typeBookingTransactionsAllowed" minOccurs="0"/&gt;
 *         &lt;element name="BackgroundPassive" type="{http://www.travelport.com/schema/common_v32_0}typeBookingTransactionsAllowed" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "air",
    "hotel",
    "rail",
    "vehicle",
    "passive",
    "backgroundPassive"
})
@XmlRootElement(name = "TransactionType")
public class TransactionType {

    @XmlElement(name = "Air")
    protected TransactionType.Air air;
    @XmlElement(name = "Hotel")
    protected TypeTransactionsAllowed hotel;
    @XmlElement(name = "Rail")
    protected TypeTransactionsAllowed rail;
    @XmlElement(name = "Vehicle")
    protected TypeTransactionsAllowed vehicle;
    @XmlElement(name = "Passive")
    protected TypeBookingTransactionsAllowed passive;
    @XmlElement(name = "BackgroundPassive")
    protected TypeBookingTransactionsAllowed backgroundPassive;

    /**
     * Obtiene el valor de la propiedad air.
     * 
     * @return
     *     possible object is
     *     {@link TransactionType.Air }
     *     
     */
    public TransactionType.Air getAir() {
        return air;
    }

    /**
     * Define el valor de la propiedad air.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionType.Air }
     *     
     */
    public void setAir(TransactionType.Air value) {
        this.air = value;
    }

    /**
     * Obtiene el valor de la propiedad hotel.
     * 
     * @return
     *     possible object is
     *     {@link TypeTransactionsAllowed }
     *     
     */
    public TypeTransactionsAllowed getHotel() {
        return hotel;
    }

    /**
     * Define el valor de la propiedad hotel.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeTransactionsAllowed }
     *     
     */
    public void setHotel(TypeTransactionsAllowed value) {
        this.hotel = value;
    }

    /**
     * Obtiene el valor de la propiedad rail.
     * 
     * @return
     *     possible object is
     *     {@link TypeTransactionsAllowed }
     *     
     */
    public TypeTransactionsAllowed getRail() {
        return rail;
    }

    /**
     * Define el valor de la propiedad rail.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeTransactionsAllowed }
     *     
     */
    public void setRail(TypeTransactionsAllowed value) {
        this.rail = value;
    }

    /**
     * Obtiene el valor de la propiedad vehicle.
     * 
     * @return
     *     possible object is
     *     {@link TypeTransactionsAllowed }
     *     
     */
    public TypeTransactionsAllowed getVehicle() {
        return vehicle;
    }

    /**
     * Define el valor de la propiedad vehicle.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeTransactionsAllowed }
     *     
     */
    public void setVehicle(TypeTransactionsAllowed value) {
        this.vehicle = value;
    }

    /**
     * Obtiene el valor de la propiedad passive.
     * 
     * @return
     *     possible object is
     *     {@link TypeBookingTransactionsAllowed }
     *     
     */
    public TypeBookingTransactionsAllowed getPassive() {
        return passive;
    }

    /**
     * Define el valor de la propiedad passive.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeBookingTransactionsAllowed }
     *     
     */
    public void setPassive(TypeBookingTransactionsAllowed value) {
        this.passive = value;
    }

    /**
     * Obtiene el valor de la propiedad backgroundPassive.
     * 
     * @return
     *     possible object is
     *     {@link TypeBookingTransactionsAllowed }
     *     
     */
    public TypeBookingTransactionsAllowed getBackgroundPassive() {
        return backgroundPassive;
    }

    /**
     * Define el valor de la propiedad backgroundPassive.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeBookingTransactionsAllowed }
     *     
     */
    public void setBackgroundPassive(TypeBookingTransactionsAllowed value) {
        this.backgroundPassive = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.travelport.com/schema/common_v32_0}typeTransactionsAllowed"&gt;
     *       &lt;attGroup ref="{http://www.travelport.com/schema/common_v32_0}attrFlexShopping"/&gt;
     *       &lt;attribute name="OneWayShop" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *       &lt;attribute name="FlexExplore" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *       &lt;attribute name="RapidRepriceEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Air
        extends TypeTransactionsAllowed
    {

        @XmlAttribute(name = "OneWayShop")
        protected Boolean oneWayShop;
        @XmlAttribute(name = "FlexExplore")
        protected Boolean flexExplore;
        @XmlAttribute(name = "RapidRepriceEnabled")
        protected Boolean rapidRepriceEnabled;
        @XmlAttribute(name = "Tier")
        protected Integer tier;
        @XmlAttribute(name = "DaysEnabled")
        protected Boolean daysEnabled;
        @XmlAttribute(name = "WeekendsEnabled")
        protected Boolean weekendsEnabled;
        @XmlAttribute(name = "AirportsEnabled")
        protected Boolean airportsEnabled;
        @XmlAttribute(name = "ODEnabled")
        protected Boolean odEnabled;

        /**
         * Obtiene el valor de la propiedad oneWayShop.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isOneWayShop() {
            return oneWayShop;
        }

        /**
         * Define el valor de la propiedad oneWayShop.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setOneWayShop(Boolean value) {
            this.oneWayShop = value;
        }

        /**
         * Obtiene el valor de la propiedad flexExplore.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isFlexExplore() {
            return flexExplore;
        }

        /**
         * Define el valor de la propiedad flexExplore.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setFlexExplore(Boolean value) {
            this.flexExplore = value;
        }

        /**
         * Obtiene el valor de la propiedad rapidRepriceEnabled.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isRapidRepriceEnabled() {
            return rapidRepriceEnabled;
        }

        /**
         * Define el valor de la propiedad rapidRepriceEnabled.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setRapidRepriceEnabled(Boolean value) {
            this.rapidRepriceEnabled = value;
        }

        /**
         * Obtiene el valor de la propiedad tier.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getTier() {
            return tier;
        }

        /**
         * Define el valor de la propiedad tier.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setTier(Integer value) {
            this.tier = value;
        }

        /**
         * Obtiene el valor de la propiedad daysEnabled.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isDaysEnabled() {
            return daysEnabled;
        }

        /**
         * Define el valor de la propiedad daysEnabled.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setDaysEnabled(Boolean value) {
            this.daysEnabled = value;
        }

        /**
         * Obtiene el valor de la propiedad weekendsEnabled.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isWeekendsEnabled() {
            return weekendsEnabled;
        }

        /**
         * Define el valor de la propiedad weekendsEnabled.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setWeekendsEnabled(Boolean value) {
            this.weekendsEnabled = value;
        }

        /**
         * Obtiene el valor de la propiedad airportsEnabled.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isAirportsEnabled() {
            return airportsEnabled;
        }

        /**
         * Define el valor de la propiedad airportsEnabled.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setAirportsEnabled(Boolean value) {
            this.airportsEnabled = value;
        }

        /**
         * Obtiene el valor de la propiedad odEnabled.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isODEnabled() {
            return odEnabled;
        }

        /**
         * Define el valor de la propiedad odEnabled.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setODEnabled(Boolean value) {
            this.odEnabled = value;
        }

    }

}
