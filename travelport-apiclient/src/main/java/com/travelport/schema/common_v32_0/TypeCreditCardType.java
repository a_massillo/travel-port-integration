
package com.travelport.schema.common_v32_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para typeCreditCardType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="typeCreditCardType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v32_0}typePaymentCard"&gt;
 *       &lt;attribute name="ExtendedPayment" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="CustomerReference" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="AcceptanceOverride" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="ThirdPartyPayment" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="BankName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="BankCountryCode" type="{http://www.travelport.com/schema/common_v32_0}typeCountry" /&gt;
 *       &lt;attribute name="BankStateCode" type="{http://www.travelport.com/schema/common_v32_0}typeState" /&gt;
 *       &lt;attribute name="Enett" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "typeCreditCardType")
@XmlSeeAlso({
    CreditCard.class
})
public class TypeCreditCardType
    extends TypePaymentCard
{

    @XmlAttribute(name = "ExtendedPayment")
    protected String extendedPayment;
    @XmlAttribute(name = "CustomerReference")
    protected String customerReference;
    @XmlAttribute(name = "AcceptanceOverride")
    protected Boolean acceptanceOverride;
    @XmlAttribute(name = "ThirdPartyPayment")
    protected Boolean thirdPartyPayment;
    @XmlAttribute(name = "BankName")
    protected String bankName;
    @XmlAttribute(name = "BankCountryCode")
    protected String bankCountryCode;
    @XmlAttribute(name = "BankStateCode")
    protected String bankStateCode;
    @XmlAttribute(name = "Enett")
    protected Boolean enett;

    /**
     * Obtiene el valor de la propiedad extendedPayment.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtendedPayment() {
        return extendedPayment;
    }

    /**
     * Define el valor de la propiedad extendedPayment.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtendedPayment(String value) {
        this.extendedPayment = value;
    }

    /**
     * Obtiene el valor de la propiedad customerReference.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerReference() {
        return customerReference;
    }

    /**
     * Define el valor de la propiedad customerReference.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerReference(String value) {
        this.customerReference = value;
    }

    /**
     * Obtiene el valor de la propiedad acceptanceOverride.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAcceptanceOverride() {
        return acceptanceOverride;
    }

    /**
     * Define el valor de la propiedad acceptanceOverride.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAcceptanceOverride(Boolean value) {
        this.acceptanceOverride = value;
    }

    /**
     * Obtiene el valor de la propiedad thirdPartyPayment.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isThirdPartyPayment() {
        if (thirdPartyPayment == null) {
            return false;
        } else {
            return thirdPartyPayment;
        }
    }

    /**
     * Define el valor de la propiedad thirdPartyPayment.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setThirdPartyPayment(Boolean value) {
        this.thirdPartyPayment = value;
    }

    /**
     * Obtiene el valor de la propiedad bankName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * Define el valor de la propiedad bankName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankName(String value) {
        this.bankName = value;
    }

    /**
     * Obtiene el valor de la propiedad bankCountryCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankCountryCode() {
        return bankCountryCode;
    }

    /**
     * Define el valor de la propiedad bankCountryCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankCountryCode(String value) {
        this.bankCountryCode = value;
    }

    /**
     * Obtiene el valor de la propiedad bankStateCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankStateCode() {
        return bankStateCode;
    }

    /**
     * Define el valor de la propiedad bankStateCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankStateCode(String value) {
        this.bankStateCode = value;
    }

    /**
     * Obtiene el valor de la propiedad enett.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isEnett() {
        if (enett == null) {
            return false;
        } else {
            return enett;
        }
    }

    /**
     * Define el valor de la propiedad enett.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnett(Boolean value) {
        this.enett = value;
    }

}
