
package com.travelport.schema.common_v32_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="MinPercentage" type="{http://www.travelport.com/schema/common_v32_0}typeIntegerPercentage" /&gt;
 *       &lt;attribute name="MaxPercentage" type="{http://www.travelport.com/schema/common_v32_0}typeIntegerPercentage" /&gt;
 *       &lt;attribute name="ExpiryDays" type="{http://www.travelport.com/schema/common_v32_0}typeDurationYearInDays" /&gt;
 *       &lt;attribute name="MultiUse" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "EnettVan")
public class EnettVan {

    @XmlAttribute(name = "MinPercentage")
    protected Integer minPercentage;
    @XmlAttribute(name = "MaxPercentage")
    protected Integer maxPercentage;
    @XmlAttribute(name = "ExpiryDays")
    protected Duration expiryDays;
    @XmlAttribute(name = "MultiUse")
    protected Boolean multiUse;

    /**
     * Obtiene el valor de la propiedad minPercentage.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinPercentage() {
        return minPercentage;
    }

    /**
     * Define el valor de la propiedad minPercentage.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinPercentage(Integer value) {
        this.minPercentage = value;
    }

    /**
     * Obtiene el valor de la propiedad maxPercentage.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxPercentage() {
        return maxPercentage;
    }

    /**
     * Define el valor de la propiedad maxPercentage.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxPercentage(Integer value) {
        this.maxPercentage = value;
    }

    /**
     * Obtiene el valor de la propiedad expiryDays.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getExpiryDays() {
        return expiryDays;
    }

    /**
     * Define el valor de la propiedad expiryDays.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setExpiryDays(Duration value) {
        this.expiryDays = value;
    }

    /**
     * Obtiene el valor de la propiedad multiUse.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isMultiUse() {
        if (multiUse == null) {
            return true;
        } else {
            return multiUse;
        }
    }

    /**
     * Define el valor de la propiedad multiUse.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMultiUse(Boolean value) {
        this.multiUse = value;
    }

}
