
package com.travelport.schema.rail_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="RefundAmount" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="CancellationFee" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="ExchangeAmount" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="ApproximateRefundAmount" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="ApproximateCancellationFee" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="ApproximateExchangeAmount" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="RetainAmount" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "RailExchangeInfo")
public class RailExchangeInfo {

    @XmlAttribute(name = "RefundAmount")
    protected String refundAmount;
    @XmlAttribute(name = "CancellationFee")
    protected String cancellationFee;
    @XmlAttribute(name = "ExchangeAmount")
    protected String exchangeAmount;
    @XmlAttribute(name = "ApproximateRefundAmount")
    protected String approximateRefundAmount;
    @XmlAttribute(name = "ApproximateCancellationFee")
    protected String approximateCancellationFee;
    @XmlAttribute(name = "ApproximateExchangeAmount")
    protected String approximateExchangeAmount;
    @XmlAttribute(name = "RetainAmount")
    protected String retainAmount;

    /**
     * Obtiene el valor de la propiedad refundAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefundAmount() {
        return refundAmount;
    }

    /**
     * Define el valor de la propiedad refundAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefundAmount(String value) {
        this.refundAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad cancellationFee.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCancellationFee() {
        return cancellationFee;
    }

    /**
     * Define el valor de la propiedad cancellationFee.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCancellationFee(String value) {
        this.cancellationFee = value;
    }

    /**
     * Obtiene el valor de la propiedad exchangeAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExchangeAmount() {
        return exchangeAmount;
    }

    /**
     * Define el valor de la propiedad exchangeAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExchangeAmount(String value) {
        this.exchangeAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad approximateRefundAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApproximateRefundAmount() {
        return approximateRefundAmount;
    }

    /**
     * Define el valor de la propiedad approximateRefundAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApproximateRefundAmount(String value) {
        this.approximateRefundAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad approximateCancellationFee.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApproximateCancellationFee() {
        return approximateCancellationFee;
    }

    /**
     * Define el valor de la propiedad approximateCancellationFee.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApproximateCancellationFee(String value) {
        this.approximateCancellationFee = value;
    }

    /**
     * Obtiene el valor de la propiedad approximateExchangeAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApproximateExchangeAmount() {
        return approximateExchangeAmount;
    }

    /**
     * Define el valor de la propiedad approximateExchangeAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApproximateExchangeAmount(String value) {
        this.approximateExchangeAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad retainAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetainAmount() {
        return retainAmount;
    }

    /**
     * Define el valor de la propiedad retainAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetainAmount(String value) {
        this.retainAmount = value;
    }

}
