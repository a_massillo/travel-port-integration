
package com.travelport.schema.rail_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="CoachLabel" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="PlaceLabel" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="Assignment" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="RailSegmentRef" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="BookingTravelerRef" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "RailSpecificSeatAssignment")
public class RailSpecificSeatAssignment {

    @XmlAttribute(name = "CoachLabel", required = true)
    protected String coachLabel;
    @XmlAttribute(name = "PlaceLabel", required = true)
    protected String placeLabel;
    @XmlAttribute(name = "Assignment", required = true)
    protected String assignment;
    @XmlAttribute(name = "RailSegmentRef", required = true)
    protected String railSegmentRef;
    @XmlAttribute(name = "BookingTravelerRef", required = true)
    protected String bookingTravelerRef;

    /**
     * Obtiene el valor de la propiedad coachLabel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoachLabel() {
        return coachLabel;
    }

    /**
     * Define el valor de la propiedad coachLabel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoachLabel(String value) {
        this.coachLabel = value;
    }

    /**
     * Obtiene el valor de la propiedad placeLabel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaceLabel() {
        return placeLabel;
    }

    /**
     * Define el valor de la propiedad placeLabel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaceLabel(String value) {
        this.placeLabel = value;
    }

    /**
     * Obtiene el valor de la propiedad assignment.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssignment() {
        return assignment;
    }

    /**
     * Define el valor de la propiedad assignment.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssignment(String value) {
        this.assignment = value;
    }

    /**
     * Obtiene el valor de la propiedad railSegmentRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRailSegmentRef() {
        return railSegmentRef;
    }

    /**
     * Define el valor de la propiedad railSegmentRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRailSegmentRef(String value) {
        this.railSegmentRef = value;
    }

    /**
     * Obtiene el valor de la propiedad bookingTravelerRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookingTravelerRef() {
        return bookingTravelerRef;
    }

    /**
     * Define el valor de la propiedad bookingTravelerRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookingTravelerRef(String value) {
        this.bookingTravelerRef = value;
    }

}
