
package com.travelport.schema.rail_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/rail_v43_0}RailPricingSolution"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ReasonCode" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;enumeration value="Price"/&gt;
 *             &lt;enumeration value="Schedule"/&gt;
 *             &lt;enumeration value="Both"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "railPricingSolution"
})
@XmlRootElement(name = "RailSolutionChangedInfo")
public class RailSolutionChangedInfo {

    @XmlElement(name = "RailPricingSolution", required = true)
    protected RailPricingSolution railPricingSolution;
    @XmlAttribute(name = "ReasonCode", required = true)
    protected String reasonCode;

    /**
     * Obtiene el valor de la propiedad railPricingSolution.
     * 
     * @return
     *     possible object is
     *     {@link RailPricingSolution }
     *     
     */
    public RailPricingSolution getRailPricingSolution() {
        return railPricingSolution;
    }

    /**
     * Define el valor de la propiedad railPricingSolution.
     * 
     * @param value
     *     allowed object is
     *     {@link RailPricingSolution }
     *     
     */
    public void setRailPricingSolution(RailPricingSolution value) {
        this.railPricingSolution = value;
    }

    /**
     * Obtiene el valor de la propiedad reasonCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonCode() {
        return reasonCode;
    }

    /**
     * Define el valor de la propiedad reasonCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonCode(String value) {
        this.reasonCode = value;
    }

}
