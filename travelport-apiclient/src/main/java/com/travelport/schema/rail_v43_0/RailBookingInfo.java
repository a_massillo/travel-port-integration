
package com.travelport.schema.rail_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="RailFareRef" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="RailJourneyRef" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="OptionalService" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "RailBookingInfo")
public class RailBookingInfo {

    @XmlAttribute(name = "RailFareRef", required = true)
    protected String railFareRef;
    @XmlAttribute(name = "RailJourneyRef", required = true)
    protected String railJourneyRef;
    @XmlAttribute(name = "OptionalService")
    protected Boolean optionalService;

    /**
     * Obtiene el valor de la propiedad railFareRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRailFareRef() {
        return railFareRef;
    }

    /**
     * Define el valor de la propiedad railFareRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRailFareRef(String value) {
        this.railFareRef = value;
    }

    /**
     * Obtiene el valor de la propiedad railJourneyRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRailJourneyRef() {
        return railJourneyRef;
    }

    /**
     * Define el valor de la propiedad railJourneyRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRailJourneyRef(String value) {
        this.railJourneyRef = value;
    }

    /**
     * Obtiene el valor de la propiedad optionalService.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isOptionalService() {
        if (optionalService == null) {
            return false;
        } else {
            return optionalService;
        }
    }

    /**
     * Define el valor de la propiedad optionalService.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOptionalService(Boolean value) {
        this.optionalService = value;
    }

}
