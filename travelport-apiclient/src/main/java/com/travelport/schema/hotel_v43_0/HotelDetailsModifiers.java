
package com.travelport.schema.hotel_v43_0;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.CorporateDiscountID;
import com.travelport.schema.common_v43_0.LoyaltyCard;
import com.travelport.schema.common_v43_0.PermittedProviders;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}PermittedProviders" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}LoyaltyCard" maxOccurs="4" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}CorporateDiscountID" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/hotel_v43_0}HotelStay"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/hotel_v43_0}NumberOfChildren" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/hotel_v43_0}HotelBedding" maxOccurs="4" minOccurs="0"/&gt;
 *         &lt;element name="RateCategory" type="{http://www.travelport.com/schema/common_v43_0}typeOTACode" maxOccurs="8" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/hotel_v43_0}PermittedAggregators" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/hotel_v43_0}BookingGuestInformation" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="NumberOfAdults" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="RateRuleDetail" type="{http://www.travelport.com/schema/hotel_v43_0}typeRateRuleDetail" default="None" /&gt;
 *       &lt;attribute name="NumberOfRooms" type="{http://www.w3.org/2001/XMLSchema}int" default="1" /&gt;
 *       &lt;attribute name="Key" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="PreferredCurrency" type="{http://www.travelport.com/schema/common_v43_0}typeCurrency" /&gt;
 *       &lt;attribute name="TotalOccupants" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="ProcessAllNegoRatesInd" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="MaxWait" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "permittedProviders",
    "loyaltyCard",
    "corporateDiscountID",
    "hotelStay",
    "numberOfChildren",
    "hotelBedding",
    "rateCategory",
    "permittedAggregators",
    "bookingGuestInformation"
})
@XmlRootElement(name = "HotelDetailsModifiers")
public class HotelDetailsModifiers {

    @XmlElement(name = "PermittedProviders", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected PermittedProviders permittedProviders;
    @XmlElement(name = "LoyaltyCard", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected List<LoyaltyCard> loyaltyCard;
    @XmlElement(name = "CorporateDiscountID", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected List<CorporateDiscountID> corporateDiscountID;
    @XmlElement(name = "HotelStay", required = true)
    protected HotelStay hotelStay;
    @XmlElement(name = "NumberOfChildren")
    protected NumberOfChildren numberOfChildren;
    @XmlElement(name = "HotelBedding")
    protected List<HotelBedding> hotelBedding;
    @XmlElement(name = "RateCategory")
    @XmlSchemaType(name = "positiveInteger")
    protected List<BigInteger> rateCategory;
    @XmlElement(name = "PermittedAggregators")
    protected PermittedAggregators permittedAggregators;
    @XmlElement(name = "BookingGuestInformation")
    protected BookingGuestInformation bookingGuestInformation;
    @XmlAttribute(name = "NumberOfAdults")
    protected Integer numberOfAdults;
    @XmlAttribute(name = "RateRuleDetail")
    protected TypeRateRuleDetail rateRuleDetail;
    @XmlAttribute(name = "NumberOfRooms")
    protected Integer numberOfRooms;
    @XmlAttribute(name = "Key")
    protected String key;
    @XmlAttribute(name = "PreferredCurrency")
    protected String preferredCurrency;
    @XmlAttribute(name = "TotalOccupants")
    protected Integer totalOccupants;
    @XmlAttribute(name = "ProcessAllNegoRatesInd")
    protected Boolean processAllNegoRatesInd;
    @XmlAttribute(name = "MaxWait")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger maxWait;

    /**
     * Obtiene el valor de la propiedad permittedProviders.
     * 
     * @return
     *     possible object is
     *     {@link PermittedProviders }
     *     
     */
    public PermittedProviders getPermittedProviders() {
        return permittedProviders;
    }

    /**
     * Define el valor de la propiedad permittedProviders.
     * 
     * @param value
     *     allowed object is
     *     {@link PermittedProviders }
     *     
     */
    public void setPermittedProviders(PermittedProviders value) {
        this.permittedProviders = value;
    }

    /**
     * Gets the value of the loyaltyCard property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the loyaltyCard property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLoyaltyCard().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LoyaltyCard }
     * 
     * 
     */
    public List<LoyaltyCard> getLoyaltyCard() {
        if (loyaltyCard == null) {
            loyaltyCard = new ArrayList<LoyaltyCard>();
        }
        return this.loyaltyCard;
    }

    /**
     * Search with corporate discount IDs or negotiated rate codes. 1G/1V allows a max of 4.  1P/1J allows a max of 1 corporate discount ID and up to 30 negotiated rate codes.  Support for this function is hotel supplier dependent.Gets the value of the corporateDiscountID property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the corporateDiscountID property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCorporateDiscountID().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CorporateDiscountID }
     * 
     * 
     */
    public List<CorporateDiscountID> getCorporateDiscountID() {
        if (corporateDiscountID == null) {
            corporateDiscountID = new ArrayList<CorporateDiscountID>();
        }
        return this.corporateDiscountID;
    }

    /**
     * Obtiene el valor de la propiedad hotelStay.
     * 
     * @return
     *     possible object is
     *     {@link HotelStay }
     *     
     */
    public HotelStay getHotelStay() {
        return hotelStay;
    }

    /**
     * Define el valor de la propiedad hotelStay.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelStay }
     *     
     */
    public void setHotelStay(HotelStay value) {
        this.hotelStay = value;
    }

    /**
     * Obtiene el valor de la propiedad numberOfChildren.
     * 
     * @return
     *     possible object is
     *     {@link NumberOfChildren }
     *     
     */
    public NumberOfChildren getNumberOfChildren() {
        return numberOfChildren;
    }

    /**
     * Define el valor de la propiedad numberOfChildren.
     * 
     * @param value
     *     allowed object is
     *     {@link NumberOfChildren }
     *     
     */
    public void setNumberOfChildren(NumberOfChildren value) {
        this.numberOfChildren = value;
    }

    /**
     * Gets the value of the hotelBedding property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hotelBedding property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHotelBedding().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HotelBedding }
     * 
     * 
     */
    public List<HotelBedding> getHotelBedding() {
        if (hotelBedding == null) {
            hotelBedding = new ArrayList<HotelBedding>();
        }
        return this.hotelBedding;
    }

    /**
     * Gets the value of the rateCategory property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rateCategory property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRateCategory().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BigInteger }
     * 
     * 
     */
    public List<BigInteger> getRateCategory() {
        if (rateCategory == null) {
            rateCategory = new ArrayList<BigInteger>();
        }
        return this.rateCategory;
    }

    /**
     * TRM Only
     * 
     * @return
     *     possible object is
     *     {@link PermittedAggregators }
     *     
     */
    public PermittedAggregators getPermittedAggregators() {
        return permittedAggregators;
    }

    /**
     * Define el valor de la propiedad permittedAggregators.
     * 
     * @param value
     *     allowed object is
     *     {@link PermittedAggregators }
     *     
     */
    public void setPermittedAggregators(PermittedAggregators value) {
        this.permittedAggregators = value;
    }

    /**
     * Information about requested rooms and allocation of guests per room. Provider: TRM.
     * 
     * @return
     *     possible object is
     *     {@link BookingGuestInformation }
     *     
     */
    public BookingGuestInformation getBookingGuestInformation() {
        return bookingGuestInformation;
    }

    /**
     * Define el valor de la propiedad bookingGuestInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link BookingGuestInformation }
     *     
     */
    public void setBookingGuestInformation(BookingGuestInformation value) {
        this.bookingGuestInformation = value;
    }

    /**
     * Obtiene el valor de la propiedad numberOfAdults.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfAdults() {
        return numberOfAdults;
    }

    /**
     * Define el valor de la propiedad numberOfAdults.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfAdults(Integer value) {
        this.numberOfAdults = value;
    }

    /**
     * Obtiene el valor de la propiedad rateRuleDetail.
     * 
     * @return
     *     possible object is
     *     {@link TypeRateRuleDetail }
     *     
     */
    public TypeRateRuleDetail getRateRuleDetail() {
        if (rateRuleDetail == null) {
            return TypeRateRuleDetail.NONE;
        } else {
            return rateRuleDetail;
        }
    }

    /**
     * Define el valor de la propiedad rateRuleDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeRateRuleDetail }
     *     
     */
    public void setRateRuleDetail(TypeRateRuleDetail value) {
        this.rateRuleDetail = value;
    }

    /**
     * Obtiene el valor de la propiedad numberOfRooms.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public int getNumberOfRooms() {
        if (numberOfRooms == null) {
            return  1;
        } else {
            return numberOfRooms;
        }
    }

    /**
     * Define el valor de la propiedad numberOfRooms.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfRooms(Integer value) {
        this.numberOfRooms = value;
    }

    /**
     * Obtiene el valor de la propiedad key.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Define el valor de la propiedad key.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Obtiene el valor de la propiedad preferredCurrency.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreferredCurrency() {
        return preferredCurrency;
    }

    /**
     * Define el valor de la propiedad preferredCurrency.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreferredCurrency(String value) {
        this.preferredCurrency = value;
    }

    /**
     * Obtiene el valor de la propiedad totalOccupants.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalOccupants() {
        return totalOccupants;
    }

    /**
     * Define el valor de la propiedad totalOccupants.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalOccupants(Integer value) {
        this.totalOccupants = value;
    }

    /**
     * Obtiene el valor de la propiedad processAllNegoRatesInd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isProcessAllNegoRatesInd() {
        if (processAllNegoRatesInd == null) {
            return false;
        } else {
            return processAllNegoRatesInd;
        }
    }

    /**
     * Define el valor de la propiedad processAllNegoRatesInd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProcessAllNegoRatesInd(Boolean value) {
        this.processAllNegoRatesInd = value;
    }

    /**
     * Obtiene el valor de la propiedad maxWait.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxWait() {
        return maxWait;
    }

    /**
     * Define el valor de la propiedad maxWait.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxWait(BigInteger value) {
        this.maxWait = value;
    }

}
