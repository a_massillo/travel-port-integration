
package com.travelport.schema.hotel_v43_0;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para typeAllImageSize.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;simpleType name="typeAllImageSize"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="A"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "typeAllImageSize")
@XmlEnum
public enum TypeAllImageSize {

    A;

    public String value() {
        return name();
    }

    public static TypeAllImageSize fromValue(String v) {
        return valueOf(v);
    }

}
