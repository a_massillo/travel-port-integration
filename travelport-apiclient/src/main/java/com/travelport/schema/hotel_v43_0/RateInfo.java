
package com.travelport.schema.hotel_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.TypePolicyCodesList;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RoomDispatch" type="{http://www.travelport.com/schema/hotel_v43_0}typeRoomDispatch" minOccurs="0"/&gt;
 *         &lt;element name="PolicyCodesList" type="{http://www.travelport.com/schema/common_v43_0}typePolicyCodesList" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attGroup ref="{http://www.travelport.com/schema/hotel_v43_0}attrPolicyMarkingMaxMinPolicyCodes"/&gt;
 *       &lt;attribute name="MinimumAmount" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="ApproximateMinimumAmount" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="MinAmountRateChanged" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="MaximumAmount" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="ApproximateMaximumAmount" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="MaxAmountRateChanged" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="MinimumStayAmount" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="ApproximateMinimumStayAmount" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="Commission" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="RateSupplier" type="{http://www.travelport.com/schema/common_v43_0}typeThirdPartySupplier" /&gt;
 *       &lt;attribute name="RateSupplierLogo" type="{http://www.w3.org/2001/XMLSchema}anyURI" /&gt;
 *       &lt;attribute name="PaymentType" type="{http://www.travelport.com/schema/hotel_v43_0}typeHotelPaymentType" /&gt;
 *       &lt;attribute name="ApproxAvgNightlyAmt" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="TaxesIncluded" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="AmountConverted" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="MultipleRoom" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="PackageOffer" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "roomDispatch",
    "policyCodesList"
})
@XmlRootElement(name = "RateInfo")
public class RateInfo {

    @XmlElement(name = "RoomDispatch")
    protected TypeRoomDispatch roomDispatch;
    @XmlElement(name = "PolicyCodesList")
    protected TypePolicyCodesList policyCodesList;
    @XmlAttribute(name = "MinimumAmount")
    protected String minimumAmount;
    @XmlAttribute(name = "ApproximateMinimumAmount")
    protected String approximateMinimumAmount;
    @XmlAttribute(name = "MinAmountRateChanged")
    protected Boolean minAmountRateChanged;
    @XmlAttribute(name = "MaximumAmount")
    protected String maximumAmount;
    @XmlAttribute(name = "ApproximateMaximumAmount")
    protected String approximateMaximumAmount;
    @XmlAttribute(name = "MaxAmountRateChanged")
    protected Boolean maxAmountRateChanged;
    @XmlAttribute(name = "MinimumStayAmount")
    protected String minimumStayAmount;
    @XmlAttribute(name = "ApproximateMinimumStayAmount")
    protected String approximateMinimumStayAmount;
    @XmlAttribute(name = "Commission")
    protected String commission;
    @XmlAttribute(name = "RateSupplier")
    protected String rateSupplier;
    @XmlAttribute(name = "RateSupplierLogo")
    @XmlSchemaType(name = "anyURI")
    protected String rateSupplierLogo;
    @XmlAttribute(name = "PaymentType")
    protected TypeHotelPaymentType paymentType;
    @XmlAttribute(name = "ApproxAvgNightlyAmt")
    protected String approxAvgNightlyAmt;
    @XmlAttribute(name = "TaxesIncluded")
    protected Boolean taxesIncluded;
    @XmlAttribute(name = "AmountConverted")
    protected Boolean amountConverted;
    @XmlAttribute(name = "MultipleRoom")
    protected String multipleRoom;
    @XmlAttribute(name = "PackageOffer")
    protected String packageOffer;
    @XmlAttribute(name = "MinInPolicy")
    protected Boolean minInPolicy;
    @XmlAttribute(name = "MaxInPolicy")
    protected Boolean maxInPolicy;

    /**
     * Obtiene el valor de la propiedad roomDispatch.
     * 
     * @return
     *     possible object is
     *     {@link TypeRoomDispatch }
     *     
     */
    public TypeRoomDispatch getRoomDispatch() {
        return roomDispatch;
    }

    /**
     * Define el valor de la propiedad roomDispatch.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeRoomDispatch }
     *     
     */
    public void setRoomDispatch(TypeRoomDispatch value) {
        this.roomDispatch = value;
    }

    /**
     * Obtiene el valor de la propiedad policyCodesList.
     * 
     * @return
     *     possible object is
     *     {@link TypePolicyCodesList }
     *     
     */
    public TypePolicyCodesList getPolicyCodesList() {
        return policyCodesList;
    }

    /**
     * Define el valor de la propiedad policyCodesList.
     * 
     * @param value
     *     allowed object is
     *     {@link TypePolicyCodesList }
     *     
     */
    public void setPolicyCodesList(TypePolicyCodesList value) {
        this.policyCodesList = value;
    }

    /**
     * Obtiene el valor de la propiedad minimumAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMinimumAmount() {
        return minimumAmount;
    }

    /**
     * Define el valor de la propiedad minimumAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMinimumAmount(String value) {
        this.minimumAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad approximateMinimumAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApproximateMinimumAmount() {
        return approximateMinimumAmount;
    }

    /**
     * Define el valor de la propiedad approximateMinimumAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApproximateMinimumAmount(String value) {
        this.approximateMinimumAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad minAmountRateChanged.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMinAmountRateChanged() {
        return minAmountRateChanged;
    }

    /**
     * Define el valor de la propiedad minAmountRateChanged.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMinAmountRateChanged(Boolean value) {
        this.minAmountRateChanged = value;
    }

    /**
     * Obtiene el valor de la propiedad maximumAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaximumAmount() {
        return maximumAmount;
    }

    /**
     * Define el valor de la propiedad maximumAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaximumAmount(String value) {
        this.maximumAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad approximateMaximumAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApproximateMaximumAmount() {
        return approximateMaximumAmount;
    }

    /**
     * Define el valor de la propiedad approximateMaximumAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApproximateMaximumAmount(String value) {
        this.approximateMaximumAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad maxAmountRateChanged.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMaxAmountRateChanged() {
        return maxAmountRateChanged;
    }

    /**
     * Define el valor de la propiedad maxAmountRateChanged.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMaxAmountRateChanged(Boolean value) {
        this.maxAmountRateChanged = value;
    }

    /**
     * Obtiene el valor de la propiedad minimumStayAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMinimumStayAmount() {
        return minimumStayAmount;
    }

    /**
     * Define el valor de la propiedad minimumStayAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMinimumStayAmount(String value) {
        this.minimumStayAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad approximateMinimumStayAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApproximateMinimumStayAmount() {
        return approximateMinimumStayAmount;
    }

    /**
     * Define el valor de la propiedad approximateMinimumStayAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApproximateMinimumStayAmount(String value) {
        this.approximateMinimumStayAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad commission.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommission() {
        return commission;
    }

    /**
     * Define el valor de la propiedad commission.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommission(String value) {
        this.commission = value;
    }

    /**
     * Obtiene el valor de la propiedad rateSupplier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateSupplier() {
        return rateSupplier;
    }

    /**
     * Define el valor de la propiedad rateSupplier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateSupplier(String value) {
        this.rateSupplier = value;
    }

    /**
     * Obtiene el valor de la propiedad rateSupplierLogo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateSupplierLogo() {
        return rateSupplierLogo;
    }

    /**
     * Define el valor de la propiedad rateSupplierLogo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateSupplierLogo(String value) {
        this.rateSupplierLogo = value;
    }

    /**
     * Obtiene el valor de la propiedad paymentType.
     * 
     * @return
     *     possible object is
     *     {@link TypeHotelPaymentType }
     *     
     */
    public TypeHotelPaymentType getPaymentType() {
        return paymentType;
    }

    /**
     * Define el valor de la propiedad paymentType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeHotelPaymentType }
     *     
     */
    public void setPaymentType(TypeHotelPaymentType value) {
        this.paymentType = value;
    }

    /**
     * Obtiene el valor de la propiedad approxAvgNightlyAmt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApproxAvgNightlyAmt() {
        return approxAvgNightlyAmt;
    }

    /**
     * Define el valor de la propiedad approxAvgNightlyAmt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApproxAvgNightlyAmt(String value) {
        this.approxAvgNightlyAmt = value;
    }

    /**
     * Obtiene el valor de la propiedad taxesIncluded.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTaxesIncluded() {
        return taxesIncluded;
    }

    /**
     * Define el valor de la propiedad taxesIncluded.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTaxesIncluded(Boolean value) {
        this.taxesIncluded = value;
    }

    /**
     * Obtiene el valor de la propiedad amountConverted.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAmountConverted() {
        return amountConverted;
    }

    /**
     * Define el valor de la propiedad amountConverted.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAmountConverted(Boolean value) {
        this.amountConverted = value;
    }

    /**
     * Obtiene el valor de la propiedad multipleRoom.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMultipleRoom() {
        return multipleRoom;
    }

    /**
     * Define el valor de la propiedad multipleRoom.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMultipleRoom(String value) {
        this.multipleRoom = value;
    }

    /**
     * Obtiene el valor de la propiedad packageOffer.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackageOffer() {
        return packageOffer;
    }

    /**
     * Define el valor de la propiedad packageOffer.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackageOffer(String value) {
        this.packageOffer = value;
    }

    /**
     * Obtiene el valor de la propiedad minInPolicy.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMinInPolicy() {
        return minInPolicy;
    }

    /**
     * Define el valor de la propiedad minInPolicy.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMinInPolicy(Boolean value) {
        this.minInPolicy = value;
    }

    /**
     * Obtiene el valor de la propiedad maxInPolicy.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMaxInPolicy() {
        return maxInPolicy;
    }

    /**
     * Define el valor de la propiedad maxInPolicy.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMaxInPolicy(Boolean value) {
        this.maxInPolicy = value;
    }

}
