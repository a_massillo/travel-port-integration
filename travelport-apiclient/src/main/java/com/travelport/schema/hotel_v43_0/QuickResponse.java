
package com.travelport.schema.hotel_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="FastResult" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="MoreToken" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "QuickResponse")
public class QuickResponse {

    @XmlAttribute(name = "FastResult")
    protected Boolean fastResult;
    @XmlAttribute(name = "MoreToken")
    protected String moreToken;

    /**
     * Obtiene el valor de la propiedad fastResult.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFastResult() {
        return fastResult;
    }

    /**
     * Define el valor de la propiedad fastResult.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFastResult(Boolean value) {
        this.fastResult = value;
    }

    /**
     * Obtiene el valor de la propiedad moreToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMoreToken() {
        return moreToken;
    }

    /**
     * Define el valor de la propiedad moreToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMoreToken(String value) {
        this.moreToken = value;
    }

}
