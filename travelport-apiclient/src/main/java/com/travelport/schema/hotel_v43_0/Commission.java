
package com.travelport.schema.hotel_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.TypeTrinary;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="Indicator" type="{http://www.travelport.com/schema/common_v43_0}typeTrinary" /&gt;
 *       &lt;attribute name="Percent" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="CommissionAmount" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="ApproxCommissionAmount" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="CommissionOnSurcharges" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="ApproxCommissionOnSurcharges" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "Commission")
public class Commission {

    @XmlAttribute(name = "Indicator")
    protected TypeTrinary indicator;
    @XmlAttribute(name = "Percent")
    protected String percent;
    @XmlAttribute(name = "CommissionAmount")
    protected String commissionAmount;
    @XmlAttribute(name = "ApproxCommissionAmount")
    protected String approxCommissionAmount;
    @XmlAttribute(name = "CommissionOnSurcharges")
    protected String commissionOnSurcharges;
    @XmlAttribute(name = "ApproxCommissionOnSurcharges")
    protected String approxCommissionOnSurcharges;

    /**
     * Obtiene el valor de la propiedad indicator.
     * 
     * @return
     *     possible object is
     *     {@link TypeTrinary }
     *     
     */
    public TypeTrinary getIndicator() {
        return indicator;
    }

    /**
     * Define el valor de la propiedad indicator.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeTrinary }
     *     
     */
    public void setIndicator(TypeTrinary value) {
        this.indicator = value;
    }

    /**
     * Obtiene el valor de la propiedad percent.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPercent() {
        return percent;
    }

    /**
     * Define el valor de la propiedad percent.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPercent(String value) {
        this.percent = value;
    }

    /**
     * Obtiene el valor de la propiedad commissionAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommissionAmount() {
        return commissionAmount;
    }

    /**
     * Define el valor de la propiedad commissionAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommissionAmount(String value) {
        this.commissionAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad approxCommissionAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApproxCommissionAmount() {
        return approxCommissionAmount;
    }

    /**
     * Define el valor de la propiedad approxCommissionAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApproxCommissionAmount(String value) {
        this.approxCommissionAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad commissionOnSurcharges.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommissionOnSurcharges() {
        return commissionOnSurcharges;
    }

    /**
     * Define el valor de la propiedad commissionOnSurcharges.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommissionOnSurcharges(String value) {
        this.commissionOnSurcharges = value;
    }

    /**
     * Obtiene el valor de la propiedad approxCommissionOnSurcharges.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApproxCommissionOnSurcharges() {
        return approxCommissionOnSurcharges;
    }

    /**
     * Define el valor de la propiedad approxCommissionOnSurcharges.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApproxCommissionOnSurcharges(String value) {
        this.approxCommissionOnSurcharges = value;
    }

}
