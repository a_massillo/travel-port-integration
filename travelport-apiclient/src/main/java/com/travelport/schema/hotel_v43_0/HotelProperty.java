
package com.travelport.schema.hotel_v43_0;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.CoordinateLocation;
import com.travelport.schema.common_v43_0.Distance;
import com.travelport.schema.common_v43_0.PhoneNumber;
import com.travelport.schema.common_v43_0.TypeReserveRequirement;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PropertyAddress" type="{http://www.travelport.com/schema/hotel_v43_0}typeUnstructuredAddress" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}PhoneNumber" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}CoordinateLocation" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Distance" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/hotel_v43_0}HotelRating" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/hotel_v43_0}Amenities" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/hotel_v43_0}MarketingMessage" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="HotelChain" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeHotelChainCode" /&gt;
 *       &lt;attribute name="HotelCode" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeHotelCode" /&gt;
 *       &lt;attribute name="HotelLocation" type="{http://www.travelport.com/schema/hotel_v43_0}typeHotelLocationCode" /&gt;
 *       &lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="VendorLocationKey" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="HotelTransportation" type="{http://www.travelport.com/schema/common_v43_0}typeOTACode" /&gt;
 *       &lt;attribute name="ReserveRequirement" type="{http://www.travelport.com/schema/common_v43_0}typeReserveRequirement" /&gt;
 *       &lt;attribute name="ParticipationLevel" type="{http://www.travelport.com/schema/common_v43_0}StringLength1" /&gt;
 *       &lt;attribute name="Availability" type="{http://www.travelport.com/schema/hotel_v43_0}typeHotelAvailability" /&gt;
 *       &lt;attribute name="Key" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="PreferredOption" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="MoreRates" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="MoreRatesToken"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="1"/&gt;
 *             &lt;maxLength value="30"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="NetTransCommissionInd" type="{http://www.travelport.com/schema/hotel_v43_0}typeNetTransCommission" /&gt;
 *       &lt;attribute name="NumOfRatePlans" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "propertyAddress",
    "phoneNumber",
    "coordinateLocation",
    "distance",
    "hotelRating",
    "amenities",
    "marketingMessage"
})
@XmlRootElement(name = "HotelProperty")
public class HotelProperty {

    @XmlElement(name = "PropertyAddress")
    protected TypeUnstructuredAddress propertyAddress;
    @XmlElement(name = "PhoneNumber", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected List<PhoneNumber> phoneNumber;
    @XmlElement(name = "CoordinateLocation", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected CoordinateLocation coordinateLocation;
    @XmlElement(name = "Distance", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected Distance distance;
    @XmlElement(name = "HotelRating")
    protected List<HotelRating> hotelRating;
    @XmlElement(name = "Amenities")
    protected Amenities amenities;
    @XmlElement(name = "MarketingMessage")
    protected MarketingMessage marketingMessage;
    @XmlAttribute(name = "HotelChain", required = true)
    protected String hotelChain;
    @XmlAttribute(name = "HotelCode", required = true)
    protected String hotelCode;
    @XmlAttribute(name = "HotelLocation")
    protected String hotelLocation;
    @XmlAttribute(name = "Name")
    protected String name;
    @XmlAttribute(name = "VendorLocationKey")
    protected String vendorLocationKey;
    @XmlAttribute(name = "HotelTransportation")
    protected BigInteger hotelTransportation;
    @XmlAttribute(name = "ReserveRequirement")
    protected TypeReserveRequirement reserveRequirement;
    @XmlAttribute(name = "ParticipationLevel")
    protected String participationLevel;
    @XmlAttribute(name = "Availability")
    protected TypeHotelAvailability availability;
    @XmlAttribute(name = "Key")
    protected String key;
    @XmlAttribute(name = "PreferredOption")
    protected Boolean preferredOption;
    @XmlAttribute(name = "MoreRates")
    protected Boolean moreRates;
    @XmlAttribute(name = "MoreRatesToken")
    protected String moreRatesToken;
    @XmlAttribute(name = "NetTransCommissionInd")
    protected TypeNetTransCommission netTransCommissionInd;
    @XmlAttribute(name = "NumOfRatePlans")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger numOfRatePlans;

    /**
     * Obtiene el valor de la propiedad propertyAddress.
     * 
     * @return
     *     possible object is
     *     {@link TypeUnstructuredAddress }
     *     
     */
    public TypeUnstructuredAddress getPropertyAddress() {
        return propertyAddress;
    }

    /**
     * Define el valor de la propiedad propertyAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeUnstructuredAddress }
     *     
     */
    public void setPropertyAddress(TypeUnstructuredAddress value) {
        this.propertyAddress = value;
    }

    /**
     * Gets the value of the phoneNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the phoneNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPhoneNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PhoneNumber }
     * 
     * 
     */
    public List<PhoneNumber> getPhoneNumber() {
        if (phoneNumber == null) {
            phoneNumber = new ArrayList<PhoneNumber>();
        }
        return this.phoneNumber;
    }

    /**
     * Obtiene el valor de la propiedad coordinateLocation.
     * 
     * @return
     *     possible object is
     *     {@link CoordinateLocation }
     *     
     */
    public CoordinateLocation getCoordinateLocation() {
        return coordinateLocation;
    }

    /**
     * Define el valor de la propiedad coordinateLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link CoordinateLocation }
     *     
     */
    public void setCoordinateLocation(CoordinateLocation value) {
        this.coordinateLocation = value;
    }

    /**
     * Obtiene el valor de la propiedad distance.
     * 
     * @return
     *     possible object is
     *     {@link Distance }
     *     
     */
    public Distance getDistance() {
        return distance;
    }

    /**
     * Define el valor de la propiedad distance.
     * 
     * @param value
     *     allowed object is
     *     {@link Distance }
     *     
     */
    public void setDistance(Distance value) {
        this.distance = value;
    }

    /**
     * Gets the value of the hotelRating property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hotelRating property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHotelRating().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HotelRating }
     * 
     * 
     */
    public List<HotelRating> getHotelRating() {
        if (hotelRating == null) {
            hotelRating = new ArrayList<HotelRating>();
        }
        return this.hotelRating;
    }

    /**
     * Obtiene el valor de la propiedad amenities.
     * 
     * @return
     *     possible object is
     *     {@link Amenities }
     *     
     */
    public Amenities getAmenities() {
        return amenities;
    }

    /**
     * Define el valor de la propiedad amenities.
     * 
     * @param value
     *     allowed object is
     *     {@link Amenities }
     *     
     */
    public void setAmenities(Amenities value) {
        this.amenities = value;
    }

    /**
     * Obtiene el valor de la propiedad marketingMessage.
     * 
     * @return
     *     possible object is
     *     {@link MarketingMessage }
     *     
     */
    public MarketingMessage getMarketingMessage() {
        return marketingMessage;
    }

    /**
     * Define el valor de la propiedad marketingMessage.
     * 
     * @param value
     *     allowed object is
     *     {@link MarketingMessage }
     *     
     */
    public void setMarketingMessage(MarketingMessage value) {
        this.marketingMessage = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelChain.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelChain() {
        return hotelChain;
    }

    /**
     * Define el valor de la propiedad hotelChain.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelChain(String value) {
        this.hotelChain = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelCode() {
        return hotelCode;
    }

    /**
     * Define el valor de la propiedad hotelCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelCode(String value) {
        this.hotelCode = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelLocation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelLocation() {
        return hotelLocation;
    }

    /**
     * Define el valor de la propiedad hotelLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelLocation(String value) {
        this.hotelLocation = value;
    }

    /**
     * Obtiene el valor de la propiedad name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Define el valor de la propiedad name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Obtiene el valor de la propiedad vendorLocationKey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorLocationKey() {
        return vendorLocationKey;
    }

    /**
     * Define el valor de la propiedad vendorLocationKey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorLocationKey(String value) {
        this.vendorLocationKey = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelTransportation.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getHotelTransportation() {
        return hotelTransportation;
    }

    /**
     * Define el valor de la propiedad hotelTransportation.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setHotelTransportation(BigInteger value) {
        this.hotelTransportation = value;
    }

    /**
     * Obtiene el valor de la propiedad reserveRequirement.
     * 
     * @return
     *     possible object is
     *     {@link TypeReserveRequirement }
     *     
     */
    public TypeReserveRequirement getReserveRequirement() {
        return reserveRequirement;
    }

    /**
     * Define el valor de la propiedad reserveRequirement.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeReserveRequirement }
     *     
     */
    public void setReserveRequirement(TypeReserveRequirement value) {
        this.reserveRequirement = value;
    }

    /**
     * Obtiene el valor de la propiedad participationLevel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParticipationLevel() {
        return participationLevel;
    }

    /**
     * Define el valor de la propiedad participationLevel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParticipationLevel(String value) {
        this.participationLevel = value;
    }

    /**
     * Obtiene el valor de la propiedad availability.
     * 
     * @return
     *     possible object is
     *     {@link TypeHotelAvailability }
     *     
     */
    public TypeHotelAvailability getAvailability() {
        return availability;
    }

    /**
     * Define el valor de la propiedad availability.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeHotelAvailability }
     *     
     */
    public void setAvailability(TypeHotelAvailability value) {
        this.availability = value;
    }

    /**
     * Obtiene el valor de la propiedad key.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Define el valor de la propiedad key.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Obtiene el valor de la propiedad preferredOption.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPreferredOption() {
        return preferredOption;
    }

    /**
     * Define el valor de la propiedad preferredOption.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPreferredOption(Boolean value) {
        this.preferredOption = value;
    }

    /**
     * Obtiene el valor de la propiedad moreRates.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMoreRates() {
        return moreRates;
    }

    /**
     * Define el valor de la propiedad moreRates.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMoreRates(Boolean value) {
        this.moreRates = value;
    }

    /**
     * Obtiene el valor de la propiedad moreRatesToken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMoreRatesToken() {
        return moreRatesToken;
    }

    /**
     * Define el valor de la propiedad moreRatesToken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMoreRatesToken(String value) {
        this.moreRatesToken = value;
    }

    /**
     * Obtiene el valor de la propiedad netTransCommissionInd.
     * 
     * @return
     *     possible object is
     *     {@link TypeNetTransCommission }
     *     
     */
    public TypeNetTransCommission getNetTransCommissionInd() {
        return netTransCommissionInd;
    }

    /**
     * Define el valor de la propiedad netTransCommissionInd.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeNetTransCommission }
     *     
     */
    public void setNetTransCommissionInd(TypeNetTransCommission value) {
        this.netTransCommissionInd = value;
    }

    /**
     * Obtiene el valor de la propiedad numOfRatePlans.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumOfRatePlans() {
        return numOfRatePlans;
    }

    /**
     * Define el valor de la propiedad numOfRatePlans.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumOfRatePlans(BigInteger value) {
        this.numOfRatePlans = value;
    }

}
