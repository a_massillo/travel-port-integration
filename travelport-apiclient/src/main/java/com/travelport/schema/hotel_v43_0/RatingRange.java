
package com.travelport.schema.hotel_v43_0;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Search for a range of rating
 * 
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="MinimumRating" type="{http://www.travelport.com/schema/hotel_v43_0}typeSimpleHotelRating" /&gt;
 *       &lt;attribute name="MaximumRating" type="{http://www.travelport.com/schema/hotel_v43_0}typeSimpleHotelRating" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "RatingRange")
public class RatingRange {

    @XmlAttribute(name = "MinimumRating")
    protected BigInteger minimumRating;
    @XmlAttribute(name = "MaximumRating")
    protected BigInteger maximumRating;

    /**
     * Obtiene el valor de la propiedad minimumRating.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMinimumRating() {
        return minimumRating;
    }

    /**
     * Define el valor de la propiedad minimumRating.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMinimumRating(BigInteger value) {
        this.minimumRating = value;
    }

    /**
     * Obtiene el valor de la propiedad maximumRating.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaximumRating() {
        return maximumRating;
    }

    /**
     * Define el valor de la propiedad maximumRating.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaximumRating(BigInteger value) {
        this.maximumRating = value;
    }

}
