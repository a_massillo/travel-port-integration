
package com.travelport.schema.hotel_v43_0;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="RateConversion" type="{http://www.w3.org/2001/XMLSchema}float" /&gt;
 *       &lt;attribute name="SourceCurrencyCode" type="{http://www.travelport.com/schema/common_v43_0}typeCurrency" /&gt;
 *       &lt;attribute name="RequestedCurrencyCode" type="{http://www.travelport.com/schema/common_v43_0}typeCurrency" /&gt;
 *       &lt;attribute name="DecimalPlaces" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "CurrencyRateConversion")
public class CurrencyRateConversion {

    @XmlAttribute(name = "RateConversion")
    protected Float rateConversion;
    @XmlAttribute(name = "SourceCurrencyCode")
    protected String sourceCurrencyCode;
    @XmlAttribute(name = "RequestedCurrencyCode")
    protected String requestedCurrencyCode;
    @XmlAttribute(name = "DecimalPlaces")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger decimalPlaces;

    /**
     * Obtiene el valor de la propiedad rateConversion.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getRateConversion() {
        return rateConversion;
    }

    /**
     * Define el valor de la propiedad rateConversion.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setRateConversion(Float value) {
        this.rateConversion = value;
    }

    /**
     * Obtiene el valor de la propiedad sourceCurrencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceCurrencyCode() {
        return sourceCurrencyCode;
    }

    /**
     * Define el valor de la propiedad sourceCurrencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceCurrencyCode(String value) {
        this.sourceCurrencyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad requestedCurrencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestedCurrencyCode() {
        return requestedCurrencyCode;
    }

    /**
     * Define el valor de la propiedad requestedCurrencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestedCurrencyCode(String value) {
        this.requestedCurrencyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad decimalPlaces.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDecimalPlaces() {
        return decimalPlaces;
    }

    /**
     * Define el valor de la propiedad decimalPlaces.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDecimalPlaces(BigInteger value) {
        this.decimalPlaces = value;
    }

}
