
package com.travelport.schema.common_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Specify a range of times.
 * 
 * <p>Clase Java para typeTimeRange complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="typeTimeRange"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="EarliestTime" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="LatestTime" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "typeTimeRange")
@XmlSeeAlso({
    SearchEvent.class
})
public class TypeTimeRange {

    @XmlAttribute(name = "EarliestTime", required = true)
    protected String earliestTime;
    @XmlAttribute(name = "LatestTime", required = true)
    protected String latestTime;

    /**
     * Obtiene el valor de la propiedad earliestTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEarliestTime() {
        return earliestTime;
    }

    /**
     * Define el valor de la propiedad earliestTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEarliestTime(String value) {
        this.earliestTime = value;
    }

    /**
     * Obtiene el valor de la propiedad latestTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLatestTime() {
        return latestTime;
    }

    /**
     * Define el valor de la propiedad latestTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLatestTime(String value) {
        this.latestTime = value;
    }

}
