
package com.travelport.schema.common_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TotalPenaltyTaxInfo" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="PenaltyTaxInfo" type="{http://www.travelport.com/schema/common_v43_0}typeTax" maxOccurs="999" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="TotalPenaltyTax" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="PaidTax" type="{http://www.travelport.com/schema/common_v43_0}typeTax" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element name="TicketFeeInfo" maxOccurs="999" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="Base" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *                 &lt;attribute name="Tax" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *                 &lt;attribute name="Total" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Reason" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element name="FeeInfo" type="{http://www.travelport.com/schema/common_v43_0}typeFeeInfo" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element name="TaxInfo" type="{http://www.travelport.com/schema/common_v43_0}typeTaxInfo" maxOccurs="999" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ExchangeAmount" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="BaseFare" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="EquivalentBaseFare" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="Taxes" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="ChangeFee" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="ForfeitAmount" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="Refundable" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="Exchangeable" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="FirstClassUpgrade" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="TicketByDate" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="PricingTag" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="EquivalentChangeFee" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="EquivalentExchangeAmount" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="AddCollection" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="ResidualValue" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="TotalResidualValue" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="OriginalFlightValue" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="FlownSegmentValue" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="BulkTicketAdvisory" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="FarePull" type="{http://www.travelport.com/schema/common_v43_0}typeFarePull" /&gt;
 *       &lt;attribute name="Refund" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "totalPenaltyTaxInfo",
    "paidTax",
    "ticketFeeInfo",
    "reason",
    "feeInfo",
    "taxInfo"
})
@XmlRootElement(name = "AirExchangeInfo")
public class AirExchangeInfo {

    @XmlElement(name = "TotalPenaltyTaxInfo")
    protected AirExchangeInfo.TotalPenaltyTaxInfo totalPenaltyTaxInfo;
    @XmlElement(name = "PaidTax")
    protected List<TypeTax> paidTax;
    @XmlElement(name = "TicketFeeInfo")
    protected List<AirExchangeInfo.TicketFeeInfo> ticketFeeInfo;
    @XmlElement(name = "Reason")
    protected List<String> reason;
    @XmlElement(name = "FeeInfo")
    protected List<TypeFeeInfo> feeInfo;
    @XmlElement(name = "TaxInfo")
    protected List<TypeTaxInfo> taxInfo;
    @XmlAttribute(name = "ExchangeAmount", required = true)
    protected String exchangeAmount;
    @XmlAttribute(name = "BaseFare")
    protected String baseFare;
    @XmlAttribute(name = "EquivalentBaseFare")
    protected String equivalentBaseFare;
    @XmlAttribute(name = "Taxes")
    protected String taxes;
    @XmlAttribute(name = "ChangeFee")
    protected String changeFee;
    @XmlAttribute(name = "ForfeitAmount")
    protected String forfeitAmount;
    @XmlAttribute(name = "Refundable")
    protected Boolean refundable;
    @XmlAttribute(name = "Exchangeable")
    protected Boolean exchangeable;
    @XmlAttribute(name = "FirstClassUpgrade")
    protected Boolean firstClassUpgrade;
    @XmlAttribute(name = "TicketByDate")
    protected String ticketByDate;
    @XmlAttribute(name = "PricingTag")
    protected String pricingTag;
    @XmlAttribute(name = "EquivalentChangeFee")
    protected String equivalentChangeFee;
    @XmlAttribute(name = "EquivalentExchangeAmount")
    protected String equivalentExchangeAmount;
    @XmlAttribute(name = "AddCollection")
    protected String addCollection;
    @XmlAttribute(name = "ResidualValue")
    protected String residualValue;
    @XmlAttribute(name = "TotalResidualValue")
    protected String totalResidualValue;
    @XmlAttribute(name = "OriginalFlightValue")
    protected String originalFlightValue;
    @XmlAttribute(name = "FlownSegmentValue")
    protected String flownSegmentValue;
    @XmlAttribute(name = "BulkTicketAdvisory")
    protected Boolean bulkTicketAdvisory;
    @XmlAttribute(name = "FarePull")
    protected TypeFarePull farePull;
    @XmlAttribute(name = "Refund")
    protected String refund;

    /**
     * Obtiene el valor de la propiedad totalPenaltyTaxInfo.
     * 
     * @return
     *     possible object is
     *     {@link AirExchangeInfo.TotalPenaltyTaxInfo }
     *     
     */
    public AirExchangeInfo.TotalPenaltyTaxInfo getTotalPenaltyTaxInfo() {
        return totalPenaltyTaxInfo;
    }

    /**
     * Define el valor de la propiedad totalPenaltyTaxInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link AirExchangeInfo.TotalPenaltyTaxInfo }
     *     
     */
    public void setTotalPenaltyTaxInfo(AirExchangeInfo.TotalPenaltyTaxInfo value) {
        this.totalPenaltyTaxInfo = value;
    }

    /**
     * Gets the value of the paidTax property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paidTax property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaidTax().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TypeTax }
     * 
     * 
     */
    public List<TypeTax> getPaidTax() {
        if (paidTax == null) {
            paidTax = new ArrayList<TypeTax>();
        }
        return this.paidTax;
    }

    /**
     * Gets the value of the ticketFeeInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ticketFeeInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTicketFeeInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AirExchangeInfo.TicketFeeInfo }
     * 
     * 
     */
    public List<AirExchangeInfo.TicketFeeInfo> getTicketFeeInfo() {
        if (ticketFeeInfo == null) {
            ticketFeeInfo = new ArrayList<AirExchangeInfo.TicketFeeInfo>();
        }
        return this.ticketFeeInfo;
    }

    /**
     * Gets the value of the reason property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reason property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReason().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getReason() {
        if (reason == null) {
            reason = new ArrayList<String>();
        }
        return this.reason;
    }

    /**
     * Gets the value of the feeInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the feeInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFeeInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TypeFeeInfo }
     * 
     * 
     */
    public List<TypeFeeInfo> getFeeInfo() {
        if (feeInfo == null) {
            feeInfo = new ArrayList<TypeFeeInfo>();
        }
        return this.feeInfo;
    }

    /**
     * Gets the value of the taxInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the taxInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTaxInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TypeTaxInfo }
     * 
     * 
     */
    public List<TypeTaxInfo> getTaxInfo() {
        if (taxInfo == null) {
            taxInfo = new ArrayList<TypeTaxInfo>();
        }
        return this.taxInfo;
    }

    /**
     * Obtiene el valor de la propiedad exchangeAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExchangeAmount() {
        return exchangeAmount;
    }

    /**
     * Define el valor de la propiedad exchangeAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExchangeAmount(String value) {
        this.exchangeAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad baseFare.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBaseFare() {
        return baseFare;
    }

    /**
     * Define el valor de la propiedad baseFare.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBaseFare(String value) {
        this.baseFare = value;
    }

    /**
     * Obtiene el valor de la propiedad equivalentBaseFare.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquivalentBaseFare() {
        return equivalentBaseFare;
    }

    /**
     * Define el valor de la propiedad equivalentBaseFare.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquivalentBaseFare(String value) {
        this.equivalentBaseFare = value;
    }

    /**
     * Obtiene el valor de la propiedad taxes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxes() {
        return taxes;
    }

    /**
     * Define el valor de la propiedad taxes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxes(String value) {
        this.taxes = value;
    }

    /**
     * Obtiene el valor de la propiedad changeFee.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChangeFee() {
        return changeFee;
    }

    /**
     * Define el valor de la propiedad changeFee.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChangeFee(String value) {
        this.changeFee = value;
    }

    /**
     * Obtiene el valor de la propiedad forfeitAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForfeitAmount() {
        return forfeitAmount;
    }

    /**
     * Define el valor de la propiedad forfeitAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForfeitAmount(String value) {
        this.forfeitAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad refundable.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRefundable() {
        return refundable;
    }

    /**
     * Define el valor de la propiedad refundable.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRefundable(Boolean value) {
        this.refundable = value;
    }

    /**
     * Obtiene el valor de la propiedad exchangeable.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExchangeable() {
        return exchangeable;
    }

    /**
     * Define el valor de la propiedad exchangeable.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExchangeable(Boolean value) {
        this.exchangeable = value;
    }

    /**
     * Obtiene el valor de la propiedad firstClassUpgrade.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFirstClassUpgrade() {
        return firstClassUpgrade;
    }

    /**
     * Define el valor de la propiedad firstClassUpgrade.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFirstClassUpgrade(Boolean value) {
        this.firstClassUpgrade = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketByDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketByDate() {
        return ticketByDate;
    }

    /**
     * Define el valor de la propiedad ticketByDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketByDate(String value) {
        this.ticketByDate = value;
    }

    /**
     * Obtiene el valor de la propiedad pricingTag.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPricingTag() {
        return pricingTag;
    }

    /**
     * Define el valor de la propiedad pricingTag.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPricingTag(String value) {
        this.pricingTag = value;
    }

    /**
     * Obtiene el valor de la propiedad equivalentChangeFee.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquivalentChangeFee() {
        return equivalentChangeFee;
    }

    /**
     * Define el valor de la propiedad equivalentChangeFee.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquivalentChangeFee(String value) {
        this.equivalentChangeFee = value;
    }

    /**
     * Obtiene el valor de la propiedad equivalentExchangeAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquivalentExchangeAmount() {
        return equivalentExchangeAmount;
    }

    /**
     * Define el valor de la propiedad equivalentExchangeAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquivalentExchangeAmount(String value) {
        this.equivalentExchangeAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad addCollection.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddCollection() {
        return addCollection;
    }

    /**
     * Define el valor de la propiedad addCollection.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddCollection(String value) {
        this.addCollection = value;
    }

    /**
     * Obtiene el valor de la propiedad residualValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResidualValue() {
        return residualValue;
    }

    /**
     * Define el valor de la propiedad residualValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResidualValue(String value) {
        this.residualValue = value;
    }

    /**
     * Obtiene el valor de la propiedad totalResidualValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalResidualValue() {
        return totalResidualValue;
    }

    /**
     * Define el valor de la propiedad totalResidualValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalResidualValue(String value) {
        this.totalResidualValue = value;
    }

    /**
     * Obtiene el valor de la propiedad originalFlightValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginalFlightValue() {
        return originalFlightValue;
    }

    /**
     * Define el valor de la propiedad originalFlightValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalFlightValue(String value) {
        this.originalFlightValue = value;
    }

    /**
     * Obtiene el valor de la propiedad flownSegmentValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlownSegmentValue() {
        return flownSegmentValue;
    }

    /**
     * Define el valor de la propiedad flownSegmentValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlownSegmentValue(String value) {
        this.flownSegmentValue = value;
    }

    /**
     * Obtiene el valor de la propiedad bulkTicketAdvisory.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBulkTicketAdvisory() {
        return bulkTicketAdvisory;
    }

    /**
     * Define el valor de la propiedad bulkTicketAdvisory.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBulkTicketAdvisory(Boolean value) {
        this.bulkTicketAdvisory = value;
    }

    /**
     * Obtiene el valor de la propiedad farePull.
     * 
     * @return
     *     possible object is
     *     {@link TypeFarePull }
     *     
     */
    public TypeFarePull getFarePull() {
        return farePull;
    }

    /**
     * Define el valor de la propiedad farePull.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeFarePull }
     *     
     */
    public void setFarePull(TypeFarePull value) {
        this.farePull = value;
    }

    /**
     * Obtiene el valor de la propiedad refund.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefund() {
        return refund;
    }

    /**
     * Define el valor de la propiedad refund.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefund(String value) {
        this.refund = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attribute name="Base" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
     *       &lt;attribute name="Tax" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
     *       &lt;attribute name="Total" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class TicketFeeInfo {

        @XmlAttribute(name = "Base")
        protected String base;
        @XmlAttribute(name = "Tax")
        protected String tax;
        @XmlAttribute(name = "Total")
        protected String total;

        /**
         * Obtiene el valor de la propiedad base.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBase() {
            return base;
        }

        /**
         * Define el valor de la propiedad base.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBase(String value) {
            this.base = value;
        }

        /**
         * Obtiene el valor de la propiedad tax.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTax() {
            return tax;
        }

        /**
         * Define el valor de la propiedad tax.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTax(String value) {
            this.tax = value;
        }

        /**
         * Obtiene el valor de la propiedad total.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTotal() {
            return total;
        }

        /**
         * Define el valor de la propiedad total.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTotal(String value) {
            this.total = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="PenaltyTaxInfo" type="{http://www.travelport.com/schema/common_v43_0}typeTax" maxOccurs="999" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="TotalPenaltyTax" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "penaltyTaxInfo"
    })
    public static class TotalPenaltyTaxInfo {

        @XmlElement(name = "PenaltyTaxInfo")
        protected List<TypeTax> penaltyTaxInfo;
        @XmlAttribute(name = "TotalPenaltyTax")
        protected String totalPenaltyTax;

        /**
         * Gets the value of the penaltyTaxInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the penaltyTaxInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPenaltyTaxInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TypeTax }
         * 
         * 
         */
        public List<TypeTax> getPenaltyTaxInfo() {
            if (penaltyTaxInfo == null) {
                penaltyTaxInfo = new ArrayList<TypeTax>();
            }
            return this.penaltyTaxInfo;
        }

        /**
         * Obtiene el valor de la propiedad totalPenaltyTax.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTotalPenaltyTax() {
            return totalPenaltyTax;
        }

        /**
         * Define el valor de la propiedad totalPenaltyTax.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTotalPenaltyTax(String value) {
            this.totalPenaltyTax = value;
        }

    }

}
