
package com.travelport.schema.common_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para typeLocation complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="typeLocation"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Airport" minOccurs="0"/&gt;
 *           &lt;element ref="{http://www.travelport.com/schema/common_v43_0}City" minOccurs="0"/&gt;
 *           &lt;element ref="{http://www.travelport.com/schema/common_v43_0}CityOrAirport" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "typeLocation", propOrder = {
    "airport",
    "city",
    "cityOrAirport"
})
public class TypeLocation {

    @XmlElement(name = "Airport")
    protected Airport airport;
    @XmlElement(name = "City")
    protected City city;
    @XmlElement(name = "CityOrAirport")
    protected CityOrAirport cityOrAirport;

    /**
     * Obtiene el valor de la propiedad airport.
     * 
     * @return
     *     possible object is
     *     {@link Airport }
     *     
     */
    public Airport getAirport() {
        return airport;
    }

    /**
     * Define el valor de la propiedad airport.
     * 
     * @param value
     *     allowed object is
     *     {@link Airport }
     *     
     */
    public void setAirport(Airport value) {
        this.airport = value;
    }

    /**
     * Obtiene el valor de la propiedad city.
     * 
     * @return
     *     possible object is
     *     {@link City }
     *     
     */
    public City getCity() {
        return city;
    }

    /**
     * Define el valor de la propiedad city.
     * 
     * @param value
     *     allowed object is
     *     {@link City }
     *     
     */
    public void setCity(City value) {
        this.city = value;
    }

    /**
     * Obtiene el valor de la propiedad cityOrAirport.
     * 
     * @return
     *     possible object is
     *     {@link CityOrAirport }
     *     
     */
    public CityOrAirport getCityOrAirport() {
        return cityOrAirport;
    }

    /**
     * Define el valor de la propiedad cityOrAirport.
     * 
     * @param value
     *     allowed object is
     *     {@link CityOrAirport }
     *     
     */
    public void setCityOrAirport(CityOrAirport value) {
        this.cityOrAirport = value;
    }

}
