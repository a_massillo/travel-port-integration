
package com.travelport.schema.common_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReasonCode" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="OutOfPolicy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="PurposeOfTrip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Remark" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Type" use="required" type="{http://www.travelport.com/schema/common_v43_0}typePolicy" /&gt;
 *       &lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="OutOfPolicy" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="SegmentRef" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "reasonCode"
})
@XmlRootElement(name = "PolicyInformation")
public class PolicyInformation {

    @XmlElement(name = "ReasonCode")
    protected PolicyInformation.ReasonCode reasonCode;
    @XmlAttribute(name = "Type", required = true)
    protected TypePolicy type;
    @XmlAttribute(name = "Name")
    @XmlSchemaType(name = "anySimpleType")
    protected String name;
    @XmlAttribute(name = "OutOfPolicy")
    protected Boolean outOfPolicy;
    @XmlAttribute(name = "SegmentRef")
    protected String segmentRef;

    /**
     * Obtiene el valor de la propiedad reasonCode.
     * 
     * @return
     *     possible object is
     *     {@link PolicyInformation.ReasonCode }
     *     
     */
    public PolicyInformation.ReasonCode getReasonCode() {
        return reasonCode;
    }

    /**
     * Define el valor de la propiedad reasonCode.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyInformation.ReasonCode }
     *     
     */
    public void setReasonCode(PolicyInformation.ReasonCode value) {
        this.reasonCode = value;
    }

    /**
     * Obtiene el valor de la propiedad type.
     * 
     * @return
     *     possible object is
     *     {@link TypePolicy }
     *     
     */
    public TypePolicy getType() {
        return type;
    }

    /**
     * Define el valor de la propiedad type.
     * 
     * @param value
     *     allowed object is
     *     {@link TypePolicy }
     *     
     */
    public void setType(TypePolicy value) {
        this.type = value;
    }

    /**
     * Obtiene el valor de la propiedad name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Define el valor de la propiedad name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Obtiene el valor de la propiedad outOfPolicy.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOutOfPolicy() {
        return outOfPolicy;
    }

    /**
     * Define el valor de la propiedad outOfPolicy.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOutOfPolicy(Boolean value) {
        this.outOfPolicy = value;
    }

    /**
     * Obtiene el valor de la propiedad segmentRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSegmentRef() {
        return segmentRef;
    }

    /**
     * Define el valor de la propiedad segmentRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSegmentRef(String value) {
        this.segmentRef = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="OutOfPolicy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="PurposeOfTrip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Remark" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "outOfPolicy",
        "purposeOfTrip",
        "remark"
    })
    public static class ReasonCode {

        @XmlElement(name = "OutOfPolicy")
        protected String outOfPolicy;
        @XmlElement(name = "PurposeOfTrip")
        protected String purposeOfTrip;
        @XmlElement(name = "Remark")
        protected Remark remark;

        /**
         * Obtiene el valor de la propiedad outOfPolicy.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOutOfPolicy() {
            return outOfPolicy;
        }

        /**
         * Define el valor de la propiedad outOfPolicy.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOutOfPolicy(String value) {
            this.outOfPolicy = value;
        }

        /**
         * Obtiene el valor de la propiedad purposeOfTrip.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPurposeOfTrip() {
            return purposeOfTrip;
        }

        /**
         * Define el valor de la propiedad purposeOfTrip.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPurposeOfTrip(String value) {
            this.purposeOfTrip = value;
        }

        /**
         * Obtiene el valor de la propiedad remark.
         * 
         * @return
         *     possible object is
         *     {@link Remark }
         *     
         */
        public Remark getRemark() {
            return remark;
        }

        /**
         * Define el valor de la propiedad remark.
         * 
         * @param value
         *     allowed object is
         *     {@link Remark }
         *     
         */
        public void setRemark(Remark value) {
            this.remark = value;
        }

    }

}
