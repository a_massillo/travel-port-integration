
package com.travelport.schema.common_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}MCOInformation"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}FormOfPayment" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Endorsement" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}MCOExchangeInfo" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}MCOFeeInfo" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}MCORemark" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}MCOPriceData" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}StockControl" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}MCOText" maxOccurs="999" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="TicketType" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="TicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="MCOIssued" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="MCOIssueDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *       &lt;attribute name="MCODocNum" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="IssueReasonCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="PlatingCarrier" type="{http://www.travelport.com/schema/common_v43_0}typeCarrier" /&gt;
 *       &lt;attribute name="TourOperator" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="Location" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="TourCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="ProviderCode" type="{http://www.travelport.com/schema/common_v43_0}typeProviderCode" /&gt;
 *       &lt;attribute name="ProviderLocatorCode" type="{http://www.travelport.com/schema/common_v43_0}typeProviderLocatorCode" /&gt;
 *       &lt;attribute name="PseudoCityCode" type="{http://www.travelport.com/schema/common_v43_0}typePCC" /&gt;
 *       &lt;attribute name="ExpiryDate" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "formOfPayment",
    "endorsement",
    "mcoExchangeInfo",
    "mcoFeeInfo",
    "mcoRemark",
    "mcoPriceData",
    "stockControl",
    "mcoText"
})
@XmlRootElement(name = "MCO")
public class MCO
    extends MCOInformation
{

    @XmlElement(name = "FormOfPayment")
    protected List<FormOfPayment> formOfPayment;
    @XmlElement(name = "Endorsement")
    protected Endorsement endorsement;
    @XmlElement(name = "MCOExchangeInfo")
    protected MCOExchangeInfo mcoExchangeInfo;
    @XmlElement(name = "MCOFeeInfo")
    protected MCOFeeInfo mcoFeeInfo;
    @XmlElement(name = "MCORemark")
    protected List<MCORemark> mcoRemark;
    @XmlElement(name = "MCOPriceData")
    protected MCOPriceData mcoPriceData;
    @XmlElement(name = "StockControl")
    protected List<StockControl> stockControl;
    @XmlElement(name = "MCOText")
    protected List<MCOText> mcoText;
    @XmlAttribute(name = "TicketType")
    protected String ticketType;
    @XmlAttribute(name = "TicketNumber")
    protected String ticketNumber;
    @XmlAttribute(name = "MCOIssued", required = true)
    protected boolean mcoIssued;
    @XmlAttribute(name = "MCOIssueDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar mcoIssueDate;
    @XmlAttribute(name = "MCODocNum")
    protected String mcoDocNum;
    @XmlAttribute(name = "IssueReasonCode")
    protected String issueReasonCode;
    @XmlAttribute(name = "PlatingCarrier")
    protected String platingCarrier;
    @XmlAttribute(name = "TourOperator")
    protected String tourOperator;
    @XmlAttribute(name = "Location")
    protected String location;
    @XmlAttribute(name = "TourCode")
    protected String tourCode;
    @XmlAttribute(name = "ProviderCode")
    protected String providerCode;
    @XmlAttribute(name = "ProviderLocatorCode")
    protected String providerLocatorCode;
    @XmlAttribute(name = "PseudoCityCode")
    protected String pseudoCityCode;
    @XmlAttribute(name = "ExpiryDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar expiryDate;

    /**
     * Gets the value of the formOfPayment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the formOfPayment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFormOfPayment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FormOfPayment }
     * 
     * 
     */
    public List<FormOfPayment> getFormOfPayment() {
        if (formOfPayment == null) {
            formOfPayment = new ArrayList<FormOfPayment>();
        }
        return this.formOfPayment;
    }

    /**
     * Obtiene el valor de la propiedad endorsement.
     * 
     * @return
     *     possible object is
     *     {@link Endorsement }
     *     
     */
    public Endorsement getEndorsement() {
        return endorsement;
    }

    /**
     * Define el valor de la propiedad endorsement.
     * 
     * @param value
     *     allowed object is
     *     {@link Endorsement }
     *     
     */
    public void setEndorsement(Endorsement value) {
        this.endorsement = value;
    }

    /**
     * Obtiene el valor de la propiedad mcoExchangeInfo.
     * 
     * @return
     *     possible object is
     *     {@link MCOExchangeInfo }
     *     
     */
    public MCOExchangeInfo getMCOExchangeInfo() {
        return mcoExchangeInfo;
    }

    /**
     * Define el valor de la propiedad mcoExchangeInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link MCOExchangeInfo }
     *     
     */
    public void setMCOExchangeInfo(MCOExchangeInfo value) {
        this.mcoExchangeInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad mcoFeeInfo.
     * 
     * @return
     *     possible object is
     *     {@link MCOFeeInfo }
     *     
     */
    public MCOFeeInfo getMCOFeeInfo() {
        return mcoFeeInfo;
    }

    /**
     * Define el valor de la propiedad mcoFeeInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link MCOFeeInfo }
     *     
     */
    public void setMCOFeeInfo(MCOFeeInfo value) {
        this.mcoFeeInfo = value;
    }

    /**
     * Gets the value of the mcoRemark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mcoRemark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMCORemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MCORemark }
     * 
     * 
     */
    public List<MCORemark> getMCORemark() {
        if (mcoRemark == null) {
            mcoRemark = new ArrayList<MCORemark>();
        }
        return this.mcoRemark;
    }

    /**
     * Obtiene el valor de la propiedad mcoPriceData.
     * 
     * @return
     *     possible object is
     *     {@link MCOPriceData }
     *     
     */
    public MCOPriceData getMCOPriceData() {
        return mcoPriceData;
    }

    /**
     * Define el valor de la propiedad mcoPriceData.
     * 
     * @param value
     *     allowed object is
     *     {@link MCOPriceData }
     *     
     */
    public void setMCOPriceData(MCOPriceData value) {
        this.mcoPriceData = value;
    }

    /**
     * Gets the value of the stockControl property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the stockControl property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStockControl().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StockControl }
     * 
     * 
     */
    public List<StockControl> getStockControl() {
        if (stockControl == null) {
            stockControl = new ArrayList<StockControl>();
        }
        return this.stockControl;
    }

    /**
     * Gets the value of the mcoText property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mcoText property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMCOText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MCOText }
     * 
     * 
     */
    public List<MCOText> getMCOText() {
        if (mcoText == null) {
            mcoText = new ArrayList<MCOText>();
        }
        return this.mcoText;
    }

    /**
     * Obtiene el valor de la propiedad ticketType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketType() {
        return ticketType;
    }

    /**
     * Define el valor de la propiedad ticketType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketType(String value) {
        this.ticketType = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketNumber() {
        return ticketNumber;
    }

    /**
     * Define el valor de la propiedad ticketNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketNumber(String value) {
        this.ticketNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad mcoIssued.
     * 
     */
    public boolean isMCOIssued() {
        return mcoIssued;
    }

    /**
     * Define el valor de la propiedad mcoIssued.
     * 
     */
    public void setMCOIssued(boolean value) {
        this.mcoIssued = value;
    }

    /**
     * Obtiene el valor de la propiedad mcoIssueDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMCOIssueDate() {
        return mcoIssueDate;
    }

    /**
     * Define el valor de la propiedad mcoIssueDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMCOIssueDate(XMLGregorianCalendar value) {
        this.mcoIssueDate = value;
    }

    /**
     * Obtiene el valor de la propiedad mcoDocNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMCODocNum() {
        return mcoDocNum;
    }

    /**
     * Define el valor de la propiedad mcoDocNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMCODocNum(String value) {
        this.mcoDocNum = value;
    }

    /**
     * Obtiene el valor de la propiedad issueReasonCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssueReasonCode() {
        return issueReasonCode;
    }

    /**
     * Define el valor de la propiedad issueReasonCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssueReasonCode(String value) {
        this.issueReasonCode = value;
    }

    /**
     * Obtiene el valor de la propiedad platingCarrier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlatingCarrier() {
        return platingCarrier;
    }

    /**
     * Define el valor de la propiedad platingCarrier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlatingCarrier(String value) {
        this.platingCarrier = value;
    }

    /**
     * Obtiene el valor de la propiedad tourOperator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTourOperator() {
        return tourOperator;
    }

    /**
     * Define el valor de la propiedad tourOperator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTourOperator(String value) {
        this.tourOperator = value;
    }

    /**
     * Obtiene el valor de la propiedad location.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocation() {
        return location;
    }

    /**
     * Define el valor de la propiedad location.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocation(String value) {
        this.location = value;
    }

    /**
     * Obtiene el valor de la propiedad tourCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTourCode() {
        return tourCode;
    }

    /**
     * Define el valor de la propiedad tourCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTourCode(String value) {
        this.tourCode = value;
    }

    /**
     * Obtiene el valor de la propiedad providerCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProviderCode() {
        return providerCode;
    }

    /**
     * Define el valor de la propiedad providerCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProviderCode(String value) {
        this.providerCode = value;
    }

    /**
     * Obtiene el valor de la propiedad providerLocatorCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProviderLocatorCode() {
        return providerLocatorCode;
    }

    /**
     * Define el valor de la propiedad providerLocatorCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProviderLocatorCode(String value) {
        this.providerLocatorCode = value;
    }

    /**
     * Obtiene el valor de la propiedad pseudoCityCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPseudoCityCode() {
        return pseudoCityCode;
    }

    /**
     * Define el valor de la propiedad pseudoCityCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPseudoCityCode(String value) {
        this.pseudoCityCode = value;
    }

    /**
     * Obtiene el valor de la propiedad expiryDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpiryDate() {
        return expiryDate;
    }

    /**
     * Define el valor de la propiedad expiryDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpiryDate(XMLGregorianCalendar value) {
        this.expiryDate = value;
    }

}
