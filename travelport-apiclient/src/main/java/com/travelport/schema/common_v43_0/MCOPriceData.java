
package com.travelport.schema.common_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TaxInfo" type="{http://www.travelport.com/schema/common_v43_0}typeTaxInfo" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element name="Commission" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attGroup ref="{http://www.travelport.com/schema/common_v43_0}attrAmountPercent"/&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="MCOAmount" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="MCOEquivalentFare" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="MCOTotalAmount" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "taxInfo",
    "commission"
})
@XmlRootElement(name = "MCOPriceData")
public class MCOPriceData {

    @XmlElement(name = "TaxInfo")
    protected List<TypeTaxInfo> taxInfo;
    @XmlElement(name = "Commission")
    protected MCOPriceData.Commission commission;
    @XmlAttribute(name = "MCOAmount", required = true)
    protected String mcoAmount;
    @XmlAttribute(name = "MCOEquivalentFare")
    protected String mcoEquivalentFare;
    @XmlAttribute(name = "MCOTotalAmount")
    protected String mcoTotalAmount;

    /**
     * Gets the value of the taxInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the taxInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTaxInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TypeTaxInfo }
     * 
     * 
     */
    public List<TypeTaxInfo> getTaxInfo() {
        if (taxInfo == null) {
            taxInfo = new ArrayList<TypeTaxInfo>();
        }
        return this.taxInfo;
    }

    /**
     * Obtiene el valor de la propiedad commission.
     * 
     * @return
     *     possible object is
     *     {@link MCOPriceData.Commission }
     *     
     */
    public MCOPriceData.Commission getCommission() {
        return commission;
    }

    /**
     * Define el valor de la propiedad commission.
     * 
     * @param value
     *     allowed object is
     *     {@link MCOPriceData.Commission }
     *     
     */
    public void setCommission(MCOPriceData.Commission value) {
        this.commission = value;
    }

    /**
     * Obtiene el valor de la propiedad mcoAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMCOAmount() {
        return mcoAmount;
    }

    /**
     * Define el valor de la propiedad mcoAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMCOAmount(String value) {
        this.mcoAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad mcoEquivalentFare.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMCOEquivalentFare() {
        return mcoEquivalentFare;
    }

    /**
     * Define el valor de la propiedad mcoEquivalentFare.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMCOEquivalentFare(String value) {
        this.mcoEquivalentFare = value;
    }

    /**
     * Obtiene el valor de la propiedad mcoTotalAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMCOTotalAmount() {
        return mcoTotalAmount;
    }

    /**
     * Define el valor de la propiedad mcoTotalAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMCOTotalAmount(String value) {
        this.mcoTotalAmount = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attGroup ref="{http://www.travelport.com/schema/common_v43_0}attrAmountPercent"/&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Commission {

        @XmlAttribute(name = "Amount")
        protected String amount;
        @XmlAttribute(name = "Percentage")
        protected String percentage;

        /**
         * Obtiene el valor de la propiedad amount.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAmount() {
            return amount;
        }

        /**
         * Define el valor de la propiedad amount.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAmount(String value) {
            this.amount = value;
        }

        /**
         * Obtiene el valor de la propiedad percentage.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPercentage() {
            return percentage;
        }

        /**
         * Define el valor de la propiedad percentage.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPercentage(String value) {
            this.percentage = value;
        }

    }

}
