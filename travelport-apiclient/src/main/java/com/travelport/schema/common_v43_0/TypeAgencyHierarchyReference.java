
package com.travelport.schema.common_v43_0;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para typeAgencyHierarchyReference complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="typeAgencyHierarchyReference"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="ProfileID" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeProfileID" /&gt;
 *       &lt;attribute name="ProfileType" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeAgencyProfileLevel" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "typeAgencyHierarchyReference")
@XmlSeeAlso({
    TypeAgencyHierarchyLongReference.class
})
public class TypeAgencyHierarchyReference {

    @XmlAttribute(name = "ProfileID", required = true)
    protected BigInteger profileID;
    @XmlAttribute(name = "ProfileType", required = true)
    protected TypeAgencyProfileLevel profileType;

    /**
     * Obtiene el valor de la propiedad profileID.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getProfileID() {
        return profileID;
    }

    /**
     * Define el valor de la propiedad profileID.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setProfileID(BigInteger value) {
        this.profileID = value;
    }

    /**
     * Obtiene el valor de la propiedad profileType.
     * 
     * @return
     *     possible object is
     *     {@link TypeAgencyProfileLevel }
     *     
     */
    public TypeAgencyProfileLevel getProfileType() {
        return profileType;
    }

    /**
     * Define el valor de la propiedad profileType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeAgencyProfileLevel }
     *     
     */
    public void setProfileType(TypeAgencyProfileLevel value) {
        this.profileType = value;
    }

}
