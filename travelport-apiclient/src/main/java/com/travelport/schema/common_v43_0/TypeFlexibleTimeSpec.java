
package com.travelport.schema.common_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A type which can be used for flexible date/time specification -extends the generic type typeTimeSpec to provide extra options for search.
 * 
 * <p>Clase Java para typeFlexibleTimeSpec complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="typeFlexibleTimeSpec"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}typeTimeSpec"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SearchExtraDays" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="DaysBefore" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *                 &lt;attribute name="DaysAfter" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "typeFlexibleTimeSpec", propOrder = {
    "searchExtraDays"
})
public class TypeFlexibleTimeSpec
    extends TypeTimeSpec
{

    @XmlElement(name = "SearchExtraDays")
    protected TypeFlexibleTimeSpec.SearchExtraDays searchExtraDays;

    /**
     * Obtiene el valor de la propiedad searchExtraDays.
     * 
     * @return
     *     possible object is
     *     {@link TypeFlexibleTimeSpec.SearchExtraDays }
     *     
     */
    public TypeFlexibleTimeSpec.SearchExtraDays getSearchExtraDays() {
        return searchExtraDays;
    }

    /**
     * Define el valor de la propiedad searchExtraDays.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeFlexibleTimeSpec.SearchExtraDays }
     *     
     */
    public void setSearchExtraDays(TypeFlexibleTimeSpec.SearchExtraDays value) {
        this.searchExtraDays = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attribute name="DaysBefore" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
     *       &lt;attribute name="DaysAfter" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class SearchExtraDays {

        @XmlAttribute(name = "DaysBefore")
        protected Integer daysBefore;
        @XmlAttribute(name = "DaysAfter")
        protected Integer daysAfter;

        /**
         * Obtiene el valor de la propiedad daysBefore.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getDaysBefore() {
            return daysBefore;
        }

        /**
         * Define el valor de la propiedad daysBefore.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setDaysBefore(Integer value) {
            this.daysBefore = value;
        }

        /**
         * Obtiene el valor de la propiedad daysAfter.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getDaysAfter() {
            return daysAfter;
        }

        /**
         * Define el valor de la propiedad daysAfter.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setDaysAfter(Integer value) {
            this.daysAfter = value;
        }

    }

}
