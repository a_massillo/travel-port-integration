
package com.travelport.schema.common_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * Type for Agency Payment.
 * 
 * <p>Clase Java para typeAgencyPayment complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="typeAgencyPayment"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="AgencyBillingIdentifier" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="128"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="AgencyBillingNumber"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="128"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="AgencyBillingPassword"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="128"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "typeAgencyPayment")
public class TypeAgencyPayment {

    @XmlAttribute(name = "AgencyBillingIdentifier", required = true)
    protected String agencyBillingIdentifier;
    @XmlAttribute(name = "AgencyBillingNumber")
    protected String agencyBillingNumber;
    @XmlAttribute(name = "AgencyBillingPassword")
    protected String agencyBillingPassword;

    /**
     * Obtiene el valor de la propiedad agencyBillingIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyBillingIdentifier() {
        return agencyBillingIdentifier;
    }

    /**
     * Define el valor de la propiedad agencyBillingIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyBillingIdentifier(String value) {
        this.agencyBillingIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad agencyBillingNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyBillingNumber() {
        return agencyBillingNumber;
    }

    /**
     * Define el valor de la propiedad agencyBillingNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyBillingNumber(String value) {
        this.agencyBillingNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad agencyBillingPassword.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyBillingPassword() {
        return agencyBillingPassword;
    }

    /**
     * Define el valor de la propiedad agencyBillingPassword.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyBillingPassword(String value) {
        this.agencyBillingPassword = value;
    }

}
