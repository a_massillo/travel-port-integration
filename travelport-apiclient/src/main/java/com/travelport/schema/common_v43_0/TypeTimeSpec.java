
package com.travelport.schema.common_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies times as either specific times, or a time range
 * 
 * <p>Clase Java para typeTimeSpec complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="typeTimeSpec"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="TimeRange" type="{http://www.travelport.com/schema/common_v43_0}typeTimeRange" minOccurs="0"/&gt;
 *         &lt;element name="SpecificTime" type="{http://www.travelport.com/schema/common_v43_0}typeSpecificTime" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *       &lt;attribute name="PreferredTime" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "typeTimeSpec", propOrder = {
    "timeRange",
    "specificTime"
})
@XmlSeeAlso({
    TypeFlexibleTimeSpec.class
})
public class TypeTimeSpec {

    @XmlElement(name = "TimeRange")
    protected TypeTimeRange timeRange;
    @XmlElement(name = "SpecificTime")
    protected TypeSpecificTime specificTime;
    @XmlAttribute(name = "PreferredTime")
    protected String preferredTime;

    /**
     * Obtiene el valor de la propiedad timeRange.
     * 
     * @return
     *     possible object is
     *     {@link TypeTimeRange }
     *     
     */
    public TypeTimeRange getTimeRange() {
        return timeRange;
    }

    /**
     * Define el valor de la propiedad timeRange.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeTimeRange }
     *     
     */
    public void setTimeRange(TypeTimeRange value) {
        this.timeRange = value;
    }

    /**
     * Obtiene el valor de la propiedad specificTime.
     * 
     * @return
     *     possible object is
     *     {@link TypeSpecificTime }
     *     
     */
    public TypeSpecificTime getSpecificTime() {
        return specificTime;
    }

    /**
     * Define el valor de la propiedad specificTime.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeSpecificTime }
     *     
     */
    public void setSpecificTime(TypeSpecificTime value) {
        this.specificTime = value;
    }

    /**
     * Obtiene el valor de la propiedad preferredTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreferredTime() {
        return preferredTime;
    }

    /**
     * Define el valor de la propiedad preferredTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreferredTime(String value) {
        this.preferredTime = value;
    }

}
