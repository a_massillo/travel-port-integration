
package com.travelport.schema.common_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.util_v43_0.AgencyServiceFeeCreateReq;
import com.travelport.schema.util_v43_0.BrandedFareAdminReq;
import com.travelport.schema.util_v43_0.BrandedFareSearchReq;
import com.travelport.schema.util_v43_0.CalculateTaxReq;
import com.travelport.schema.util_v43_0.ContentProviderRetrieveReq;
import com.travelport.schema.util_v43_0.CreateAgencyFeeMcoReq;
import com.travelport.schema.util_v43_0.CreateAirlineFeeMcoReq;
import com.travelport.schema.util_v43_0.CreditCardAuthReq;
import com.travelport.schema.util_v43_0.CurrencyConversionReq;
import com.travelport.schema.util_v43_0.FindEmployeesOnFlightReq;
import com.travelport.schema.util_v43_0.McoSearchReq;
import com.travelport.schema.util_v43_0.McoVoidReq;
import com.travelport.schema.util_v43_0.MctCountReq;
import com.travelport.schema.util_v43_0.MctLookupReq;
import com.travelport.schema.util_v43_0.MirReportRetrieveReq;
import com.travelport.schema.util_v43_0.ReferenceDataRetrieveReq;
import com.travelport.schema.util_v43_0.ReferenceDataSearchReq;
import com.travelport.schema.util_v43_0.ReferenceDataUpdateReq;
import com.travelport.schema.util_v43_0.UpsellAdminReq;
import com.travelport.schema.util_v43_0.UpsellSearchReq;


/**
 * <p>Clase Java para BaseReq complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BaseReq"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseCoreReq"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}OverridePCC" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="RetrieveProviderReservationDetails" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaseReq", propOrder = {
    "overridePCC"
})
@XmlSeeAlso({
    BaseSearchReq.class,
    BaseCreateReservationReq.class,
    ReferenceDataUpdateReq.class,
    FindEmployeesOnFlightReq.class,
    AgencyServiceFeeCreateReq.class,
    BrandedFareSearchReq.class,
    BrandedFareAdminReq.class,
    ContentProviderRetrieveReq.class,
    CalculateTaxReq.class,
    ReferenceDataSearchReq.class,
    ReferenceDataRetrieveReq.class,
    MctCountReq.class,
    MctLookupReq.class,
    McoVoidReq.class,
    McoSearchReq.class,
    MirReportRetrieveReq.class,
    CreateAirlineFeeMcoReq.class,
    CreateAgencyFeeMcoReq.class,
    CurrencyConversionReq.class,
    CreditCardAuthReq.class,
    UpsellSearchReq.class,
    UpsellAdminReq.class
})
public class BaseReq
    extends BaseCoreReq
{

    @XmlElement(name = "OverridePCC")
    protected OverridePCC overridePCC;
    @XmlAttribute(name = "RetrieveProviderReservationDetails")
    protected Boolean retrieveProviderReservationDetails;

    /**
     * Obtiene el valor de la propiedad overridePCC.
     * 
     * @return
     *     possible object is
     *     {@link OverridePCC }
     *     
     */
    public OverridePCC getOverridePCC() {
        return overridePCC;
    }

    /**
     * Define el valor de la propiedad overridePCC.
     * 
     * @param value
     *     allowed object is
     *     {@link OverridePCC }
     *     
     */
    public void setOverridePCC(OverridePCC value) {
        this.overridePCC = value;
    }

    /**
     * Obtiene el valor de la propiedad retrieveProviderReservationDetails.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isRetrieveProviderReservationDetails() {
        if (retrieveProviderReservationDetails == null) {
            return false;
        } else {
            return retrieveProviderReservationDetails;
        }
    }

    /**
     * Define el valor de la propiedad retrieveProviderReservationDetails.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetrieveProviderReservationDetails(Boolean value) {
        this.retrieveProviderReservationDetails = value;
    }

}
