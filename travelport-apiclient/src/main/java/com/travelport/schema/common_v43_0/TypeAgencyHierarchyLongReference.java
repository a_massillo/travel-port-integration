
package com.travelport.schema.common_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para typeAgencyHierarchyLongReference complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="typeAgencyHierarchyLongReference"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}typeAgencyHierarchyReference"&gt;
 *       &lt;attribute name="ProfileVersion" use="required" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="ProfileName" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;maxLength value="102"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "typeAgencyHierarchyLongReference")
public class TypeAgencyHierarchyLongReference
    extends TypeAgencyHierarchyReference
{

    @XmlAttribute(name = "ProfileVersion", required = true)
    protected int profileVersion;
    @XmlAttribute(name = "ProfileName", required = true)
    protected String profileName;

    /**
     * Obtiene el valor de la propiedad profileVersion.
     * 
     */
    public int getProfileVersion() {
        return profileVersion;
    }

    /**
     * Define el valor de la propiedad profileVersion.
     * 
     */
    public void setProfileVersion(int value) {
        this.profileVersion = value;
    }

    /**
     * Obtiene el valor de la propiedad profileName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfileName() {
        return profileName;
    }

    /**
     * Define el valor de la propiedad profileName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfileName(String value) {
        this.profileName = value;
    }

}
