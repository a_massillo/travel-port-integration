
package com.travelport.schema.common_v43_0;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para typeOtherImageSize.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;simpleType name="typeOtherImageSize"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="X"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "typeOtherImageSize")
@XmlEnum
public enum TypeOtherImageSize {

    X;

    public String value() {
        return name();
    }

    public static TypeOtherImageSize fromValue(String v) {
        return valueOf(v);
    }

}
