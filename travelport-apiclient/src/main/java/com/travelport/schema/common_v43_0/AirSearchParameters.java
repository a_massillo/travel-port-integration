
package com.travelport.schema.common_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="NoAdvancePurchase" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="RefundableFares" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="NonPenaltyFares" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="UnRestrictedFares" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "AirSearchParameters")
public class AirSearchParameters {

    @XmlAttribute(name = "NoAdvancePurchase")
    protected Boolean noAdvancePurchase;
    @XmlAttribute(name = "RefundableFares")
    protected Boolean refundableFares;
    @XmlAttribute(name = "NonPenaltyFares")
    protected Boolean nonPenaltyFares;
    @XmlAttribute(name = "UnRestrictedFares")
    protected Boolean unRestrictedFares;

    /**
     * Obtiene el valor de la propiedad noAdvancePurchase.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNoAdvancePurchase() {
        return noAdvancePurchase;
    }

    /**
     * Define el valor de la propiedad noAdvancePurchase.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNoAdvancePurchase(Boolean value) {
        this.noAdvancePurchase = value;
    }

    /**
     * Obtiene el valor de la propiedad refundableFares.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRefundableFares() {
        return refundableFares;
    }

    /**
     * Define el valor de la propiedad refundableFares.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRefundableFares(Boolean value) {
        this.refundableFares = value;
    }

    /**
     * Obtiene el valor de la propiedad nonPenaltyFares.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNonPenaltyFares() {
        return nonPenaltyFares;
    }

    /**
     * Define el valor de la propiedad nonPenaltyFares.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNonPenaltyFares(Boolean value) {
        this.nonPenaltyFares = value;
    }

    /**
     * Obtiene el valor de la propiedad unRestrictedFares.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUnRestrictedFares() {
        return unRestrictedFares;
    }

    /**
     * Define el valor de la propiedad unRestrictedFares.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUnRestrictedFares(Boolean value) {
        this.unRestrictedFares = value;
    }

}
