
package com.travelport.schema.common_v43_0;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}BookingTravelerName" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}NameRemark" minOccurs="0"/&gt;
 *         &lt;element name="DOB" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element ref="{http://www.travelport.com/schema/common_v43_0}TravelInfo" minOccurs="0"/&gt;
 *           &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Email" minOccurs="0"/&gt;
 *           &lt;element ref="{http://www.travelport.com/schema/common_v43_0}PhoneNumber" minOccurs="0"/&gt;
 *           &lt;element name="Address" type="{http://www.travelport.com/schema/common_v43_0}typeStructuredAddress" minOccurs="0"/&gt;
 *           &lt;element name="EmergencyInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *           &lt;element ref="{http://www.travelport.com/schema/common_v43_0}DeliveryInfo" minOccurs="0"/&gt;
 *           &lt;element name="Age" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *           &lt;element ref="{http://www.travelport.com/schema/common_v43_0}CustomizedNameData" minOccurs="0"/&gt;
 *           &lt;element ref="{http://www.travelport.com/schema/common_v43_0}AppliedProfile" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Key" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="TravelerType" type="{http://www.travelport.com/schema/common_v43_0}typePTC" /&gt;
 *       &lt;attribute name="Gender" type="{http://www.travelport.com/schema/common_v43_0}typeGender" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "bookingTravelerName",
    "nameRemark",
    "dob",
    "travelInfo",
    "email",
    "phoneNumber",
    "address",
    "emergencyInfo",
    "deliveryInfo",
    "age",
    "customizedNameData",
    "appliedProfile"
})
@XmlRootElement(name = "BookingTravelerInfo")
public class BookingTravelerInfo {

    @XmlElement(name = "BookingTravelerName")
    protected BookingTravelerName bookingTravelerName;
    @XmlElement(name = "NameRemark")
    protected NameRemark nameRemark;
    @XmlElement(name = "DOB")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dob;
    @XmlElement(name = "TravelInfo")
    protected TravelInfo travelInfo;
    @XmlElement(name = "Email")
    protected Email email;
    @XmlElement(name = "PhoneNumber")
    protected PhoneNumber phoneNumber;
    @XmlElement(name = "Address")
    protected TypeStructuredAddress address;
    @XmlElement(name = "EmergencyInfo")
    protected String emergencyInfo;
    @XmlElement(name = "DeliveryInfo")
    protected DeliveryInfo deliveryInfo;
    @XmlElement(name = "Age")
    protected BigInteger age;
    @XmlElement(name = "CustomizedNameData")
    protected CustomizedNameData customizedNameData;
    @XmlElement(name = "AppliedProfile")
    protected AppliedProfile appliedProfile;
    @XmlAttribute(name = "Key")
    protected String key;
    @XmlAttribute(name = "TravelerType")
    protected String travelerType;
    @XmlAttribute(name = "Gender")
    protected String gender;

    /**
     * Obtiene el valor de la propiedad bookingTravelerName.
     * 
     * @return
     *     possible object is
     *     {@link BookingTravelerName }
     *     
     */
    public BookingTravelerName getBookingTravelerName() {
        return bookingTravelerName;
    }

    /**
     * Define el valor de la propiedad bookingTravelerName.
     * 
     * @param value
     *     allowed object is
     *     {@link BookingTravelerName }
     *     
     */
    public void setBookingTravelerName(BookingTravelerName value) {
        this.bookingTravelerName = value;
    }

    /**
     * Obtiene el valor de la propiedad nameRemark.
     * 
     * @return
     *     possible object is
     *     {@link NameRemark }
     *     
     */
    public NameRemark getNameRemark() {
        return nameRemark;
    }

    /**
     * Define el valor de la propiedad nameRemark.
     * 
     * @param value
     *     allowed object is
     *     {@link NameRemark }
     *     
     */
    public void setNameRemark(NameRemark value) {
        this.nameRemark = value;
    }

    /**
     * Obtiene el valor de la propiedad dob.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDOB() {
        return dob;
    }

    /**
     * Define el valor de la propiedad dob.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDOB(XMLGregorianCalendar value) {
        this.dob = value;
    }

    /**
     * Obtiene el valor de la propiedad travelInfo.
     * 
     * @return
     *     possible object is
     *     {@link TravelInfo }
     *     
     */
    public TravelInfo getTravelInfo() {
        return travelInfo;
    }

    /**
     * Define el valor de la propiedad travelInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link TravelInfo }
     *     
     */
    public void setTravelInfo(TravelInfo value) {
        this.travelInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad email.
     * 
     * @return
     *     possible object is
     *     {@link Email }
     *     
     */
    public Email getEmail() {
        return email;
    }

    /**
     * Define el valor de la propiedad email.
     * 
     * @param value
     *     allowed object is
     *     {@link Email }
     *     
     */
    public void setEmail(Email value) {
        this.email = value;
    }

    /**
     * Obtiene el valor de la propiedad phoneNumber.
     * 
     * @return
     *     possible object is
     *     {@link PhoneNumber }
     *     
     */
    public PhoneNumber getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Define el valor de la propiedad phoneNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link PhoneNumber }
     *     
     */
    public void setPhoneNumber(PhoneNumber value) {
        this.phoneNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad address.
     * 
     * @return
     *     possible object is
     *     {@link TypeStructuredAddress }
     *     
     */
    public TypeStructuredAddress getAddress() {
        return address;
    }

    /**
     * Define el valor de la propiedad address.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeStructuredAddress }
     *     
     */
    public void setAddress(TypeStructuredAddress value) {
        this.address = value;
    }

    /**
     * Obtiene el valor de la propiedad emergencyInfo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmergencyInfo() {
        return emergencyInfo;
    }

    /**
     * Define el valor de la propiedad emergencyInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmergencyInfo(String value) {
        this.emergencyInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad deliveryInfo.
     * 
     * @return
     *     possible object is
     *     {@link DeliveryInfo }
     *     
     */
    public DeliveryInfo getDeliveryInfo() {
        return deliveryInfo;
    }

    /**
     * Define el valor de la propiedad deliveryInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link DeliveryInfo }
     *     
     */
    public void setDeliveryInfo(DeliveryInfo value) {
        this.deliveryInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad age.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAge() {
        return age;
    }

    /**
     * Define el valor de la propiedad age.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAge(BigInteger value) {
        this.age = value;
    }

    /**
     * Obtiene el valor de la propiedad customizedNameData.
     * 
     * @return
     *     possible object is
     *     {@link CustomizedNameData }
     *     
     */
    public CustomizedNameData getCustomizedNameData() {
        return customizedNameData;
    }

    /**
     * Define el valor de la propiedad customizedNameData.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomizedNameData }
     *     
     */
    public void setCustomizedNameData(CustomizedNameData value) {
        this.customizedNameData = value;
    }

    /**
     * Obtiene el valor de la propiedad appliedProfile.
     * 
     * @return
     *     possible object is
     *     {@link AppliedProfile }
     *     
     */
    public AppliedProfile getAppliedProfile() {
        return appliedProfile;
    }

    /**
     * Define el valor de la propiedad appliedProfile.
     * 
     * @param value
     *     allowed object is
     *     {@link AppliedProfile }
     *     
     */
    public void setAppliedProfile(AppliedProfile value) {
        this.appliedProfile = value;
    }

    /**
     * Obtiene el valor de la propiedad key.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Define el valor de la propiedad key.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Obtiene el valor de la propiedad travelerType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTravelerType() {
        return travelerType;
    }

    /**
     * Define el valor de la propiedad travelerType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTravelerType(String value) {
        this.travelerType = value;
    }

    /**
     * Obtiene el valor de la propiedad gender.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGender() {
        return gender;
    }

    /**
     * Define el valor de la propiedad gender.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGender(String value) {
        this.gender = value;
    }

}
