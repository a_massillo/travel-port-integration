
package com.travelport.schema.common_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="SeatType"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="0"/&gt;
 *             &lt;maxLength value="255"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="SeatDescription"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="0"/&gt;
 *             &lt;maxLength value="255"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="SeatValue"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="0"/&gt;
 *             &lt;maxLength value="255"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="SeatValueDescription"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="0"/&gt;
 *             &lt;maxLength value="255"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "Characteristic")
public class Characteristic {

    @XmlAttribute(name = "SeatType")
    protected String seatType;
    @XmlAttribute(name = "SeatDescription")
    protected String seatDescription;
    @XmlAttribute(name = "SeatValue")
    protected String seatValue;
    @XmlAttribute(name = "SeatValueDescription")
    protected String seatValueDescription;

    /**
     * Obtiene el valor de la propiedad seatType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeatType() {
        return seatType;
    }

    /**
     * Define el valor de la propiedad seatType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeatType(String value) {
        this.seatType = value;
    }

    /**
     * Obtiene el valor de la propiedad seatDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeatDescription() {
        return seatDescription;
    }

    /**
     * Define el valor de la propiedad seatDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeatDescription(String value) {
        this.seatDescription = value;
    }

    /**
     * Obtiene el valor de la propiedad seatValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeatValue() {
        return seatValue;
    }

    /**
     * Define el valor de la propiedad seatValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeatValue(String value) {
        this.seatValue = value;
    }

    /**
     * Obtiene el valor de la propiedad seatValueDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeatValueDescription() {
        return seatValueDescription;
    }

    /**
     * Define el valor de la propiedad seatValueDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeatValueDescription(String value) {
        this.seatValueDescription = value;
    }

}
