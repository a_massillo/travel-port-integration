
package com.travelport.schema.common_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="CancelRefund" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="NonRefundable" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="NonExchangeable" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="CancelationPenalty" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="ReissuePenalty" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="NonReissuePenalty" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="TicketRefundPenalty" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="ChargeApplicable" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="ChargePortion" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="PenaltyAmount" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "Penalty")
public class Penalty {

    @XmlAttribute(name = "CancelRefund")
    protected Boolean cancelRefund;
    @XmlAttribute(name = "NonRefundable")
    protected Boolean nonRefundable;
    @XmlAttribute(name = "NonExchangeable")
    protected Boolean nonExchangeable;
    @XmlAttribute(name = "CancelationPenalty")
    protected Boolean cancelationPenalty;
    @XmlAttribute(name = "ReissuePenalty")
    protected Boolean reissuePenalty;
    @XmlAttribute(name = "NonReissuePenalty")
    protected Boolean nonReissuePenalty;
    @XmlAttribute(name = "TicketRefundPenalty")
    protected Boolean ticketRefundPenalty;
    @XmlAttribute(name = "ChargeApplicable")
    protected Boolean chargeApplicable;
    @XmlAttribute(name = "ChargePortion")
    protected Boolean chargePortion;
    @XmlAttribute(name = "PenaltyAmount")
    protected String penaltyAmount;

    /**
     * Obtiene el valor de la propiedad cancelRefund.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCancelRefund() {
        return cancelRefund;
    }

    /**
     * Define el valor de la propiedad cancelRefund.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCancelRefund(Boolean value) {
        this.cancelRefund = value;
    }

    /**
     * Obtiene el valor de la propiedad nonRefundable.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNonRefundable() {
        return nonRefundable;
    }

    /**
     * Define el valor de la propiedad nonRefundable.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNonRefundable(Boolean value) {
        this.nonRefundable = value;
    }

    /**
     * Obtiene el valor de la propiedad nonExchangeable.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNonExchangeable() {
        return nonExchangeable;
    }

    /**
     * Define el valor de la propiedad nonExchangeable.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNonExchangeable(Boolean value) {
        this.nonExchangeable = value;
    }

    /**
     * Obtiene el valor de la propiedad cancelationPenalty.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCancelationPenalty() {
        return cancelationPenalty;
    }

    /**
     * Define el valor de la propiedad cancelationPenalty.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCancelationPenalty(Boolean value) {
        this.cancelationPenalty = value;
    }

    /**
     * Obtiene el valor de la propiedad reissuePenalty.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReissuePenalty() {
        return reissuePenalty;
    }

    /**
     * Define el valor de la propiedad reissuePenalty.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReissuePenalty(Boolean value) {
        this.reissuePenalty = value;
    }

    /**
     * Obtiene el valor de la propiedad nonReissuePenalty.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNonReissuePenalty() {
        return nonReissuePenalty;
    }

    /**
     * Define el valor de la propiedad nonReissuePenalty.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNonReissuePenalty(Boolean value) {
        this.nonReissuePenalty = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketRefundPenalty.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTicketRefundPenalty() {
        return ticketRefundPenalty;
    }

    /**
     * Define el valor de la propiedad ticketRefundPenalty.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTicketRefundPenalty(Boolean value) {
        this.ticketRefundPenalty = value;
    }

    /**
     * Obtiene el valor de la propiedad chargeApplicable.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isChargeApplicable() {
        return chargeApplicable;
    }

    /**
     * Define el valor de la propiedad chargeApplicable.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setChargeApplicable(Boolean value) {
        this.chargeApplicable = value;
    }

    /**
     * Obtiene el valor de la propiedad chargePortion.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isChargePortion() {
        return chargePortion;
    }

    /**
     * Define el valor de la propiedad chargePortion.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setChargePortion(Boolean value) {
        this.chargePortion = value;
    }

    /**
     * Obtiene el valor de la propiedad penaltyAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPenaltyAmount() {
        return penaltyAmount;
    }

    /**
     * Define el valor de la propiedad penaltyAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPenaltyAmount(String value) {
        this.penaltyAmount = value;
    }

}
