
package com.travelport.schema.common_v43_0;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * The optional service application limit
 * 
 * <p>Clase Java para OptionalServiceApplicationLimitType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OptionalServiceApplicationLimitType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attGroup ref="{http://www.travelport.com/schema/common_v43_0}OptionalServiceApplicabilityLimitGroup"/&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OptionalServiceApplicationLimitType")
public class OptionalServiceApplicationLimitType {

    @XmlAttribute(name = "ApplicableLevel", required = true)
    protected OptionalServiceApplicabilityType applicableLevel;
    @XmlAttribute(name = "ProviderDefinedApplicableLevels")
    protected String providerDefinedApplicableLevels;
    @XmlAttribute(name = "MaximumQuantity", required = true)
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger maximumQuantity;
    @XmlAttribute(name = "MinimumQuantity")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger minimumQuantity;

    /**
     * Obtiene el valor de la propiedad applicableLevel.
     * 
     * @return
     *     possible object is
     *     {@link OptionalServiceApplicabilityType }
     *     
     */
    public OptionalServiceApplicabilityType getApplicableLevel() {
        return applicableLevel;
    }

    /**
     * Define el valor de la propiedad applicableLevel.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionalServiceApplicabilityType }
     *     
     */
    public void setApplicableLevel(OptionalServiceApplicabilityType value) {
        this.applicableLevel = value;
    }

    /**
     * Obtiene el valor de la propiedad providerDefinedApplicableLevels.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProviderDefinedApplicableLevels() {
        return providerDefinedApplicableLevels;
    }

    /**
     * Define el valor de la propiedad providerDefinedApplicableLevels.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProviderDefinedApplicableLevels(String value) {
        this.providerDefinedApplicableLevels = value;
    }

    /**
     * Obtiene el valor de la propiedad maximumQuantity.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaximumQuantity() {
        return maximumQuantity;
    }

    /**
     * Define el valor de la propiedad maximumQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaximumQuantity(BigInteger value) {
        this.maximumQuantity = value;
    }

    /**
     * Obtiene el valor de la propiedad minimumQuantity.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMinimumQuantity() {
        return minimumQuantity;
    }

    /**
     * Define el valor de la propiedad minimumQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMinimumQuantity(BigInteger value) {
        this.minimumQuantity = value;
    }

}
