
package com.travelport.schema.util_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}AirUpsellQualify" maxOccurs="999"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}AirUpsellOffer"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "airUpsellQualify",
    "airUpsellOffer"
})
@XmlRootElement(name = "AirUpsellSearchResult")
public class AirUpsellSearchResult {

    @XmlElement(name = "AirUpsellQualify", required = true)
    protected List<AirUpsellQualify> airUpsellQualify;
    @XmlElement(name = "AirUpsellOffer", required = true)
    protected AirUpsellOffer airUpsellOffer;

    /**
     * Gets the value of the airUpsellQualify property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the airUpsellQualify property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAirUpsellQualify().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AirUpsellQualify }
     * 
     * 
     */
    public List<AirUpsellQualify> getAirUpsellQualify() {
        if (airUpsellQualify == null) {
            airUpsellQualify = new ArrayList<AirUpsellQualify>();
        }
        return this.airUpsellQualify;
    }

    /**
     * Obtiene el valor de la propiedad airUpsellOffer.
     * 
     * @return
     *     possible object is
     *     {@link AirUpsellOffer }
     *     
     */
    public AirUpsellOffer getAirUpsellOffer() {
        return airUpsellOffer;
    }

    /**
     * Define el valor de la propiedad airUpsellOffer.
     * 
     * @param value
     *     allowed object is
     *     {@link AirUpsellOffer }
     *     
     */
    public void setAirUpsellOffer(AirUpsellOffer value) {
        this.airUpsellOffer = value;
    }

}
