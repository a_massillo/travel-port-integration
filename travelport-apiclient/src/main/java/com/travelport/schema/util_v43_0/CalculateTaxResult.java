
package com.travelport.schema.util_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}TaxCalcInfo" maxOccurs="999" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="TotalBaseFare" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="TotalTax" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *       &lt;attribute name="TotalFare" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeMoney" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "taxCalcInfo"
})
@XmlRootElement(name = "CalculateTaxResult")
public class CalculateTaxResult {

    @XmlElement(name = "TaxCalcInfo")
    protected List<TaxCalcInfo> taxCalcInfo;
    @XmlAttribute(name = "TotalBaseFare", required = true)
    protected String totalBaseFare;
    @XmlAttribute(name = "TotalTax", required = true)
    protected String totalTax;
    @XmlAttribute(name = "TotalFare", required = true)
    protected String totalFare;

    /**
     * Gets the value of the taxCalcInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the taxCalcInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTaxCalcInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TaxCalcInfo }
     * 
     * 
     */
    public List<TaxCalcInfo> getTaxCalcInfo() {
        if (taxCalcInfo == null) {
            taxCalcInfo = new ArrayList<TaxCalcInfo>();
        }
        return this.taxCalcInfo;
    }

    /**
     * Obtiene el valor de la propiedad totalBaseFare.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalBaseFare() {
        return totalBaseFare;
    }

    /**
     * Define el valor de la propiedad totalBaseFare.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalBaseFare(String value) {
        this.totalBaseFare = value;
    }

    /**
     * Obtiene el valor de la propiedad totalTax.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalTax() {
        return totalTax;
    }

    /**
     * Define el valor de la propiedad totalTax.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalTax(String value) {
        this.totalTax = value;
    }

    /**
     * Obtiene el valor de la propiedad totalFare.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalFare() {
        return totalFare;
    }

    /**
     * Define el valor de la propiedad totalFare.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalFare(String value) {
        this.totalFare = value;
    }

}
