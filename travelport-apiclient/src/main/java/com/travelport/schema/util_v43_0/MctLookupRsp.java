
package com.travelport.schema.util_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseRsp;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseRsp"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}MctStandard" maxOccurs="4" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}MctException" maxOccurs="999" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "mctStandard",
    "mctException"
})
@XmlRootElement(name = "MctLookupRsp")
public class MctLookupRsp
    extends BaseRsp
{

    @XmlElement(name = "MctStandard")
    protected List<MctStandard> mctStandard;
    @XmlElement(name = "MctException")
    protected List<MctException> mctException;

    /**
     * Gets the value of the mctStandard property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mctStandard property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMctStandard().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MctStandard }
     * 
     * 
     */
    public List<MctStandard> getMctStandard() {
        if (mctStandard == null) {
            mctStandard = new ArrayList<MctStandard>();
        }
        return this.mctStandard;
    }

    /**
     * Gets the value of the mctException property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mctException property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMctException().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MctException }
     * 
     * 
     */
    public List<MctException> getMctException() {
        if (mctException == null) {
            mctException = new ArrayList<MctException>();
        }
        return this.mctException;
    }

}
