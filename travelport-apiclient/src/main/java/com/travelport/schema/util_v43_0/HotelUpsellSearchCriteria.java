
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}HotelUpsellOfferSearchCriteria" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}HotelUpsellQualifySearchCriteria"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "hotelUpsellOfferSearchCriteria",
    "hotelUpsellQualifySearchCriteria"
})
@XmlRootElement(name = "HotelUpsellSearchCriteria")
public class HotelUpsellSearchCriteria {

    @XmlElement(name = "HotelUpsellOfferSearchCriteria")
    protected HotelUpsellOfferSearchCriteria hotelUpsellOfferSearchCriteria;
    @XmlElement(name = "HotelUpsellQualifySearchCriteria", required = true)
    protected HotelUpsellQualifySearchCriteria hotelUpsellQualifySearchCriteria;

    /**
     * Obtiene el valor de la propiedad hotelUpsellOfferSearchCriteria.
     * 
     * @return
     *     possible object is
     *     {@link HotelUpsellOfferSearchCriteria }
     *     
     */
    public HotelUpsellOfferSearchCriteria getHotelUpsellOfferSearchCriteria() {
        return hotelUpsellOfferSearchCriteria;
    }

    /**
     * Define el valor de la propiedad hotelUpsellOfferSearchCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelUpsellOfferSearchCriteria }
     *     
     */
    public void setHotelUpsellOfferSearchCriteria(HotelUpsellOfferSearchCriteria value) {
        this.hotelUpsellOfferSearchCriteria = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelUpsellQualifySearchCriteria.
     * 
     * @return
     *     possible object is
     *     {@link HotelUpsellQualifySearchCriteria }
     *     
     */
    public HotelUpsellQualifySearchCriteria getHotelUpsellQualifySearchCriteria() {
        return hotelUpsellQualifySearchCriteria;
    }

    /**
     * Define el valor de la propiedad hotelUpsellQualifySearchCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelUpsellQualifySearchCriteria }
     *     
     */
    public void setHotelUpsellQualifySearchCriteria(HotelUpsellQualifySearchCriteria value) {
        this.hotelUpsellQualifySearchCriteria = value;
    }

}
