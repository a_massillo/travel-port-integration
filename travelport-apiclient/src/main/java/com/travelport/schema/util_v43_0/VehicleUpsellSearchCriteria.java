
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}VehicleUpsellOfferSearchCriteria" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}VehicleUpsellQualifySearchCriteria"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vehicleUpsellOfferSearchCriteria",
    "vehicleUpsellQualifySearchCriteria"
})
@XmlRootElement(name = "VehicleUpsellSearchCriteria")
public class VehicleUpsellSearchCriteria {

    @XmlElement(name = "VehicleUpsellOfferSearchCriteria")
    protected VehicleUpsellOfferSearchCriteria vehicleUpsellOfferSearchCriteria;
    @XmlElement(name = "VehicleUpsellQualifySearchCriteria", required = true)
    protected VehicleUpsellQualifySearchCriteria vehicleUpsellQualifySearchCriteria;

    /**
     * Obtiene el valor de la propiedad vehicleUpsellOfferSearchCriteria.
     * 
     * @return
     *     possible object is
     *     {@link VehicleUpsellOfferSearchCriteria }
     *     
     */
    public VehicleUpsellOfferSearchCriteria getVehicleUpsellOfferSearchCriteria() {
        return vehicleUpsellOfferSearchCriteria;
    }

    /**
     * Define el valor de la propiedad vehicleUpsellOfferSearchCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleUpsellOfferSearchCriteria }
     *     
     */
    public void setVehicleUpsellOfferSearchCriteria(VehicleUpsellOfferSearchCriteria value) {
        this.vehicleUpsellOfferSearchCriteria = value;
    }

    /**
     * Obtiene el valor de la propiedad vehicleUpsellQualifySearchCriteria.
     * 
     * @return
     *     possible object is
     *     {@link VehicleUpsellQualifySearchCriteria }
     *     
     */
    public VehicleUpsellQualifySearchCriteria getVehicleUpsellQualifySearchCriteria() {
        return vehicleUpsellQualifySearchCriteria;
    }

    /**
     * Define el valor de la propiedad vehicleUpsellQualifySearchCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleUpsellQualifySearchCriteria }
     *     
     */
    public void setVehicleUpsellQualifySearchCriteria(VehicleUpsellQualifySearchCriteria value) {
        this.vehicleUpsellQualifySearchCriteria = value;
    }

}
