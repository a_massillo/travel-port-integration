
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.TypeDoorCount;
import com.travelport.schema.common_v43_0.TypeElementStatus;
import com.travelport.schema.common_v43_0.TypeFuel;
import com.travelport.schema.common_v43_0.TypeRateCategory;
import com.travelport.schema.common_v43_0.TypeVehicleCategory;
import com.travelport.schema.common_v43_0.TypeVehicleClass;
import com.travelport.schema.common_v43_0.TypeVehicleTransmission;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attGroup ref="{http://www.travelport.com/schema/common_v43_0}attrElementKeyResults"/&gt;
 *       &lt;attribute name="AirConditioning" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="TransmissionType" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeVehicleTransmission" /&gt;
 *       &lt;attribute name="VehicleClass" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeVehicleClass" /&gt;
 *       &lt;attribute name="Category" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeVehicleCategory" /&gt;
 *       &lt;attribute name="DoorCount" type="{http://www.travelport.com/schema/common_v43_0}typeDoorCount" /&gt;
 *       &lt;attribute name="RateCode" type="{http://www.travelport.com/schema/common_v43_0}typeRateCode" /&gt;
 *       &lt;attribute name="RateCategory" type="{http://www.travelport.com/schema/common_v43_0}typeRateCategory" /&gt;
 *       &lt;attribute name="DiscountNumber" type="{http://www.travelport.com/schema/common_v43_0}typeDiscountNumber" /&gt;
 *       &lt;attribute name="FuelType" type="{http://www.travelport.com/schema/common_v43_0}typeFuel" /&gt;
 *       &lt;attribute name="Key" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "VehicleUpsellOffer")
public class VehicleUpsellOffer {

    @XmlAttribute(name = "AirConditioning", required = true)
    protected boolean airConditioning;
    @XmlAttribute(name = "TransmissionType", required = true)
    protected TypeVehicleTransmission transmissionType;
    @XmlAttribute(name = "VehicleClass", required = true)
    protected TypeVehicleClass vehicleClass;
    @XmlAttribute(name = "Category", required = true)
    protected TypeVehicleCategory category;
    @XmlAttribute(name = "DoorCount")
    protected TypeDoorCount doorCount;
    @XmlAttribute(name = "RateCode")
    protected String rateCode;
    @XmlAttribute(name = "RateCategory")
    protected TypeRateCategory rateCategory;
    @XmlAttribute(name = "DiscountNumber")
    protected String discountNumber;
    @XmlAttribute(name = "FuelType")
    protected TypeFuel fuelType;
    @XmlAttribute(name = "Key")
    protected String key;
    @XmlAttribute(name = "ElStat")
    protected TypeElementStatus elStat;
    @XmlAttribute(name = "KeyOverride")
    protected Boolean keyOverride;

    /**
     * Obtiene el valor de la propiedad airConditioning.
     * 
     */
    public boolean isAirConditioning() {
        return airConditioning;
    }

    /**
     * Define el valor de la propiedad airConditioning.
     * 
     */
    public void setAirConditioning(boolean value) {
        this.airConditioning = value;
    }

    /**
     * Obtiene el valor de la propiedad transmissionType.
     * 
     * @return
     *     possible object is
     *     {@link TypeVehicleTransmission }
     *     
     */
    public TypeVehicleTransmission getTransmissionType() {
        return transmissionType;
    }

    /**
     * Define el valor de la propiedad transmissionType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeVehicleTransmission }
     *     
     */
    public void setTransmissionType(TypeVehicleTransmission value) {
        this.transmissionType = value;
    }

    /**
     * Obtiene el valor de la propiedad vehicleClass.
     * 
     * @return
     *     possible object is
     *     {@link TypeVehicleClass }
     *     
     */
    public TypeVehicleClass getVehicleClass() {
        return vehicleClass;
    }

    /**
     * Define el valor de la propiedad vehicleClass.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeVehicleClass }
     *     
     */
    public void setVehicleClass(TypeVehicleClass value) {
        this.vehicleClass = value;
    }

    /**
     * Obtiene el valor de la propiedad category.
     * 
     * @return
     *     possible object is
     *     {@link TypeVehicleCategory }
     *     
     */
    public TypeVehicleCategory getCategory() {
        return category;
    }

    /**
     * Define el valor de la propiedad category.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeVehicleCategory }
     *     
     */
    public void setCategory(TypeVehicleCategory value) {
        this.category = value;
    }

    /**
     * Obtiene el valor de la propiedad doorCount.
     * 
     * @return
     *     possible object is
     *     {@link TypeDoorCount }
     *     
     */
    public TypeDoorCount getDoorCount() {
        return doorCount;
    }

    /**
     * Define el valor de la propiedad doorCount.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeDoorCount }
     *     
     */
    public void setDoorCount(TypeDoorCount value) {
        this.doorCount = value;
    }

    /**
     * Obtiene el valor de la propiedad rateCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateCode() {
        return rateCode;
    }

    /**
     * Define el valor de la propiedad rateCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateCode(String value) {
        this.rateCode = value;
    }

    /**
     * Obtiene el valor de la propiedad rateCategory.
     * 
     * @return
     *     possible object is
     *     {@link TypeRateCategory }
     *     
     */
    public TypeRateCategory getRateCategory() {
        return rateCategory;
    }

    /**
     * Define el valor de la propiedad rateCategory.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeRateCategory }
     *     
     */
    public void setRateCategory(TypeRateCategory value) {
        this.rateCategory = value;
    }

    /**
     * Obtiene el valor de la propiedad discountNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiscountNumber() {
        return discountNumber;
    }

    /**
     * Define el valor de la propiedad discountNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiscountNumber(String value) {
        this.discountNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad fuelType.
     * 
     * @return
     *     possible object is
     *     {@link TypeFuel }
     *     
     */
    public TypeFuel getFuelType() {
        return fuelType;
    }

    /**
     * Define el valor de la propiedad fuelType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeFuel }
     *     
     */
    public void setFuelType(TypeFuel value) {
        this.fuelType = value;
    }

    /**
     * Obtiene el valor de la propiedad key.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Define el valor de la propiedad key.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Obtiene el valor de la propiedad elStat.
     * 
     * @return
     *     possible object is
     *     {@link TypeElementStatus }
     *     
     */
    public TypeElementStatus getElStat() {
        return elStat;
    }

    /**
     * Define el valor de la propiedad elStat.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeElementStatus }
     *     
     */
    public void setElStat(TypeElementStatus value) {
        this.elStat = value;
    }

    /**
     * Obtiene el valor de la propiedad keyOverride.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isKeyOverride() {
        return keyOverride;
    }

    /**
     * Define el valor de la propiedad keyOverride.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setKeyOverride(Boolean value) {
        this.keyOverride = value;
    }

}
