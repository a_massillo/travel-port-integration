
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             Specifies flight number as either specific flight number
 *             or a flight number range
 *          
 * 
 * <p>Clase Java para typeFlightSpec complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="typeFlightSpec"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="FlightNumberRange" type="{http://www.travelport.com/schema/util_v43_0}typeFlightNumberRange" minOccurs="0"/&gt;
 *         &lt;element name="SpecificFlightNumber" type="{http://www.travelport.com/schema/util_v43_0}typeSpecificFlightNumber" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "typeFlightSpec", propOrder = {
    "flightNumberRange",
    "specificFlightNumber"
})
public class TypeFlightSpec {

    @XmlElement(name = "FlightNumberRange")
    protected TypeFlightNumberRange flightNumberRange;
    @XmlElement(name = "SpecificFlightNumber")
    protected TypeSpecificFlightNumber specificFlightNumber;

    /**
     * Obtiene el valor de la propiedad flightNumberRange.
     * 
     * @return
     *     possible object is
     *     {@link TypeFlightNumberRange }
     *     
     */
    public TypeFlightNumberRange getFlightNumberRange() {
        return flightNumberRange;
    }

    /**
     * Define el valor de la propiedad flightNumberRange.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeFlightNumberRange }
     *     
     */
    public void setFlightNumberRange(TypeFlightNumberRange value) {
        this.flightNumberRange = value;
    }

    /**
     * Obtiene el valor de la propiedad specificFlightNumber.
     * 
     * @return
     *     possible object is
     *     {@link TypeSpecificFlightNumber }
     *     
     */
    public TypeSpecificFlightNumber getSpecificFlightNumber() {
        return specificFlightNumber;
    }

    /**
     * Define el valor de la propiedad specificFlightNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeSpecificFlightNumber }
     *     
     */
    public void setSpecificFlightNumber(TypeSpecificFlightNumber value) {
        this.specificFlightNumber = value;
    }

}
