
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseReq;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseReq"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}FareFamilyCriteria"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}BrandedFareSearchModifier"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fareFamilyCriteria",
    "brandedFareSearchModifier"
})
@XmlRootElement(name = "BrandedFareSearchReq")
public class BrandedFareSearchReq
    extends BaseReq
{

    @XmlElement(name = "FareFamilyCriteria", required = true)
    protected FareFamilyCriteria fareFamilyCriteria;
    @XmlElement(name = "BrandedFareSearchModifier", required = true)
    protected BrandedFareSearchModifier brandedFareSearchModifier;

    /**
     * Obtiene el valor de la propiedad fareFamilyCriteria.
     * 
     * @return
     *     possible object is
     *     {@link FareFamilyCriteria }
     *     
     */
    public FareFamilyCriteria getFareFamilyCriteria() {
        return fareFamilyCriteria;
    }

    /**
     * Define el valor de la propiedad fareFamilyCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link FareFamilyCriteria }
     *     
     */
    public void setFareFamilyCriteria(FareFamilyCriteria value) {
        this.fareFamilyCriteria = value;
    }

    /**
     * Obtiene el valor de la propiedad brandedFareSearchModifier.
     * 
     * @return
     *     possible object is
     *     {@link BrandedFareSearchModifier }
     *     
     */
    public BrandedFareSearchModifier getBrandedFareSearchModifier() {
        return brandedFareSearchModifier;
    }

    /**
     * Define el valor de la propiedad brandedFareSearchModifier.
     * 
     * @param value
     *     allowed object is
     *     {@link BrandedFareSearchModifier }
     *     
     */
    public void setBrandedFareSearchModifier(BrandedFareSearchModifier value) {
        this.brandedFareSearchModifier = value;
    }

}
