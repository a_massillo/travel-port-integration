
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseRsp;
import com.travelport.schema.common_v43_0.MCO;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseRsp"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}MCO"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "mco"
})
@XmlRootElement(name = "CreateAgencyFeeMcoRsp")
public class CreateAgencyFeeMcoRsp
    extends BaseRsp
{

    @XmlElement(name = "MCO", namespace = "http://www.travelport.com/schema/common_v43_0", required = true)
    protected MCO mco;

    /**
     * Obtiene el valor de la propiedad mco.
     * 
     * @return
     *     possible object is
     *     {@link MCO }
     *     
     */
    public MCO getMCO() {
        return mco;
    }

    /**
     * Define el valor de la propiedad mco.
     * 
     * @param value
     *     allowed object is
     *     {@link MCO }
     *     
     */
    public void setMCO(MCO value) {
        this.mco = value;
    }

}
