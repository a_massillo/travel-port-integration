
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="ArriveStation" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *       &lt;attribute name="DepartStation" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *       &lt;attribute name="Connection" type="{http://www.travelport.com/schema/util_v43_0}typeMctConnection" /&gt;
 *       &lt;attribute name="ArriveCarrier" type="{http://www.travelport.com/schema/common_v43_0}typeCarrier" /&gt;
 *       &lt;attribute name="DepartCarrier" type="{http://www.travelport.com/schema/common_v43_0}typeCarrier" /&gt;
 *       &lt;attribute name="ArriveFlight" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="DepartFlight" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="PreviousStation" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *       &lt;attribute name="NextStation" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *       &lt;attribute name="PreviousCountry" type="{http://www.travelport.com/schema/common_v43_0}typeCountry" /&gt;
 *       &lt;attribute name="NextCountry" type="{http://www.travelport.com/schema/common_v43_0}typeCountry" /&gt;
 *       &lt;attribute name="PreviousState" type="{http://www.travelport.com/schema/common_v43_0}typeState" /&gt;
 *       &lt;attribute name="NextState" type="{http://www.travelport.com/schema/common_v43_0}typeState" /&gt;
 *       &lt;attribute name="TravelDate" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "MctSearch")
public class MctSearch {

    @XmlAttribute(name = "ArriveStation", required = true)
    protected String arriveStation;
    @XmlAttribute(name = "DepartStation")
    protected String departStation;
    @XmlAttribute(name = "Connection")
    protected TypeMctConnection connection;
    @XmlAttribute(name = "ArriveCarrier")
    protected String arriveCarrier;
    @XmlAttribute(name = "DepartCarrier")
    protected String departCarrier;
    @XmlAttribute(name = "ArriveFlight")
    protected String arriveFlight;
    @XmlAttribute(name = "DepartFlight")
    protected String departFlight;
    @XmlAttribute(name = "PreviousStation")
    protected String previousStation;
    @XmlAttribute(name = "NextStation")
    protected String nextStation;
    @XmlAttribute(name = "PreviousCountry")
    protected String previousCountry;
    @XmlAttribute(name = "NextCountry")
    protected String nextCountry;
    @XmlAttribute(name = "PreviousState")
    protected String previousState;
    @XmlAttribute(name = "NextState")
    protected String nextState;
    @XmlAttribute(name = "TravelDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar travelDate;

    /**
     * Obtiene el valor de la propiedad arriveStation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArriveStation() {
        return arriveStation;
    }

    /**
     * Define el valor de la propiedad arriveStation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArriveStation(String value) {
        this.arriveStation = value;
    }

    /**
     * Obtiene el valor de la propiedad departStation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartStation() {
        return departStation;
    }

    /**
     * Define el valor de la propiedad departStation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartStation(String value) {
        this.departStation = value;
    }

    /**
     * Obtiene el valor de la propiedad connection.
     * 
     * @return
     *     possible object is
     *     {@link TypeMctConnection }
     *     
     */
    public TypeMctConnection getConnection() {
        return connection;
    }

    /**
     * Define el valor de la propiedad connection.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeMctConnection }
     *     
     */
    public void setConnection(TypeMctConnection value) {
        this.connection = value;
    }

    /**
     * Obtiene el valor de la propiedad arriveCarrier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArriveCarrier() {
        return arriveCarrier;
    }

    /**
     * Define el valor de la propiedad arriveCarrier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArriveCarrier(String value) {
        this.arriveCarrier = value;
    }

    /**
     * Obtiene el valor de la propiedad departCarrier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartCarrier() {
        return departCarrier;
    }

    /**
     * Define el valor de la propiedad departCarrier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartCarrier(String value) {
        this.departCarrier = value;
    }

    /**
     * Obtiene el valor de la propiedad arriveFlight.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArriveFlight() {
        return arriveFlight;
    }

    /**
     * Define el valor de la propiedad arriveFlight.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArriveFlight(String value) {
        this.arriveFlight = value;
    }

    /**
     * Obtiene el valor de la propiedad departFlight.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartFlight() {
        return departFlight;
    }

    /**
     * Define el valor de la propiedad departFlight.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartFlight(String value) {
        this.departFlight = value;
    }

    /**
     * Obtiene el valor de la propiedad previousStation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreviousStation() {
        return previousStation;
    }

    /**
     * Define el valor de la propiedad previousStation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreviousStation(String value) {
        this.previousStation = value;
    }

    /**
     * Obtiene el valor de la propiedad nextStation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextStation() {
        return nextStation;
    }

    /**
     * Define el valor de la propiedad nextStation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextStation(String value) {
        this.nextStation = value;
    }

    /**
     * Obtiene el valor de la propiedad previousCountry.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreviousCountry() {
        return previousCountry;
    }

    /**
     * Define el valor de la propiedad previousCountry.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreviousCountry(String value) {
        this.previousCountry = value;
    }

    /**
     * Obtiene el valor de la propiedad nextCountry.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextCountry() {
        return nextCountry;
    }

    /**
     * Define el valor de la propiedad nextCountry.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextCountry(String value) {
        this.nextCountry = value;
    }

    /**
     * Obtiene el valor de la propiedad previousState.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreviousState() {
        return previousState;
    }

    /**
     * Define el valor de la propiedad previousState.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreviousState(String value) {
        this.previousState = value;
    }

    /**
     * Obtiene el valor de la propiedad nextState.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextState() {
        return nextState;
    }

    /**
     * Define el valor de la propiedad nextState.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextState(String value) {
        this.nextState = value;
    }

    /**
     * Obtiene el valor de la propiedad travelDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTravelDate() {
        return travelDate;
    }

    /**
     * Define el valor de la propiedad travelDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTravelDate(XMLGregorianCalendar value) {
        this.travelDate = value;
    }

}
