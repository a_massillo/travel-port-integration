
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseRsp;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseRsp"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}CalculateTaxResult"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "calculateTaxResult"
})
@XmlRootElement(name = "CalculateTaxRsp")
public class CalculateTaxRsp
    extends BaseRsp
{

    @XmlElement(name = "CalculateTaxResult", required = true)
    protected CalculateTaxResult calculateTaxResult;

    /**
     * Obtiene el valor de la propiedad calculateTaxResult.
     * 
     * @return
     *     possible object is
     *     {@link CalculateTaxResult }
     *     
     */
    public CalculateTaxResult getCalculateTaxResult() {
        return calculateTaxResult;
    }

    /**
     * Define el valor de la propiedad calculateTaxResult.
     * 
     * @param value
     *     allowed object is
     *     {@link CalculateTaxResult }
     *     
     */
    public void setCalculateTaxResult(CalculateTaxResult value) {
        this.calculateTaxResult = value;
    }

}
