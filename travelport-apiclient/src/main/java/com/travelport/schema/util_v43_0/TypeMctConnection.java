
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para typeMctConnection.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;simpleType name="typeMctConnection"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="DD"/&gt;
 *     &lt;enumeration value="DI"/&gt;
 *     &lt;enumeration value="ID"/&gt;
 *     &lt;enumeration value="II"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "typeMctConnection")
@XmlEnum
public enum TypeMctConnection {

    DD,
    DI,
    ID,
    II;

    public String value() {
        return name();
    }

    public static TypeMctConnection fromValue(String v) {
        return valueOf(v);
    }

}
