
package com.travelport.schema.util_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseReq;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseReq"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}FareFamilyAdd" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}FareFamilyUpdate" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}FareFamilyDelete" maxOccurs="999" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fareFamilyAdd",
    "fareFamilyUpdate",
    "fareFamilyDelete"
})
@XmlRootElement(name = "BrandedFareAdminReq")
public class BrandedFareAdminReq
    extends BaseReq
{

    @XmlElement(name = "FareFamilyAdd")
    protected List<FareFamilyAdd> fareFamilyAdd;
    @XmlElement(name = "FareFamilyUpdate")
    protected List<FareFamilyUpdate> fareFamilyUpdate;
    @XmlElement(name = "FareFamilyDelete")
    protected List<FareFamilyDelete> fareFamilyDelete;

    /**
     * Gets the value of the fareFamilyAdd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fareFamilyAdd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFareFamilyAdd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FareFamilyAdd }
     * 
     * 
     */
    public List<FareFamilyAdd> getFareFamilyAdd() {
        if (fareFamilyAdd == null) {
            fareFamilyAdd = new ArrayList<FareFamilyAdd>();
        }
        return this.fareFamilyAdd;
    }

    /**
     * Gets the value of the fareFamilyUpdate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fareFamilyUpdate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFareFamilyUpdate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FareFamilyUpdate }
     * 
     * 
     */
    public List<FareFamilyUpdate> getFareFamilyUpdate() {
        if (fareFamilyUpdate == null) {
            fareFamilyUpdate = new ArrayList<FareFamilyUpdate>();
        }
        return this.fareFamilyUpdate;
    }

    /**
     * Gets the value of the fareFamilyDelete property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fareFamilyDelete property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFareFamilyDelete().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FareFamilyDelete }
     * 
     * 
     */
    public List<FareFamilyDelete> getFareFamilyDelete() {
        if (fareFamilyDelete == null) {
            fareFamilyDelete = new ArrayList<FareFamilyDelete>();
        }
        return this.fareFamilyDelete;
    }

}
