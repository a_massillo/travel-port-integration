
package com.travelport.schema.util_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseReq;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseReq"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}ReferenceDataSearchModifiers" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}ReferenceDataSearchItem" maxOccurs="999"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "referenceDataSearchModifiers",
    "referenceDataSearchItem"
})
@XmlRootElement(name = "ReferenceDataSearchReq")
public class ReferenceDataSearchReq
    extends BaseReq
{

    @XmlElement(name = "ReferenceDataSearchModifiers")
    protected ReferenceDataSearchModifiers referenceDataSearchModifiers;
    @XmlElement(name = "ReferenceDataSearchItem", required = true)
    protected List<ReferenceDataSearchItem> referenceDataSearchItem;

    /**
     * Obtiene el valor de la propiedad referenceDataSearchModifiers.
     * 
     * @return
     *     possible object is
     *     {@link ReferenceDataSearchModifiers }
     *     
     */
    public ReferenceDataSearchModifiers getReferenceDataSearchModifiers() {
        return referenceDataSearchModifiers;
    }

    /**
     * Define el valor de la propiedad referenceDataSearchModifiers.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferenceDataSearchModifiers }
     *     
     */
    public void setReferenceDataSearchModifiers(ReferenceDataSearchModifiers value) {
        this.referenceDataSearchModifiers = value;
    }

    /**
     * Gets the value of the referenceDataSearchItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the referenceDataSearchItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReferenceDataSearchItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReferenceDataSearchItem }
     * 
     * 
     */
    public List<ReferenceDataSearchItem> getReferenceDataSearchItem() {
        if (referenceDataSearchItem == null) {
            referenceDataSearchItem = new ArrayList<ReferenceDataSearchItem>();
        }
        return this.referenceDataSearchItem;
    }

}
