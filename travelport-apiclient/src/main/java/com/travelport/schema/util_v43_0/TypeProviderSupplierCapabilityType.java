
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para typeProviderSupplierCapabilityType.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;simpleType name="typeProviderSupplierCapabilityType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Yes"/&gt;
 *     &lt;enumeration value="No"/&gt;
 *     &lt;enumeration value="Partial"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "typeProviderSupplierCapabilityType")
@XmlEnum
public enum TypeProviderSupplierCapabilityType {

    @XmlEnumValue("Yes")
    YES("Yes"),
    @XmlEnumValue("No")
    NO("No"),
    @XmlEnumValue("Partial")
    PARTIAL("Partial");
    private final String value;

    TypeProviderSupplierCapabilityType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TypeProviderSupplierCapabilityType fromValue(String v) {
        for (TypeProviderSupplierCapabilityType c: TypeProviderSupplierCapabilityType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
