
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseReq;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseReq"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}MctSearch"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "mctSearch"
})
@XmlRootElement(name = "MctCountReq")
public class MctCountReq
    extends BaseReq
{

    @XmlElement(name = "MctSearch", required = true)
    protected MctSearch mctSearch;

    /**
     * Obtiene el valor de la propiedad mctSearch.
     * 
     * @return
     *     possible object is
     *     {@link MctSearch }
     *     
     */
    public MctSearch getMctSearch() {
        return mctSearch;
    }

    /**
     * Define el valor de la propiedad mctSearch.
     * 
     * @param value
     *     allowed object is
     *     {@link MctSearch }
     *     
     */
    public void setMctSearch(MctSearch value) {
        this.mctSearch = value;
    }

}
