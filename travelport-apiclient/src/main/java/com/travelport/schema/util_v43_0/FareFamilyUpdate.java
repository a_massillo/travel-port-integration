
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}FareFamily"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fareFamily"
})
@XmlRootElement(name = "FareFamilyUpdate")
public class FareFamilyUpdate {

    @XmlElement(name = "FareFamily", required = true)
    protected FareFamily fareFamily;

    /**
     * Obtiene el valor de la propiedad fareFamily.
     * 
     * @return
     *     possible object is
     *     {@link FareFamily }
     *     
     */
    public FareFamily getFareFamily() {
        return fareFamily;
    }

    /**
     * Define el valor de la propiedad fareFamily.
     * 
     * @param value
     *     allowed object is
     *     {@link FareFamily }
     *     
     */
    public void setFareFamily(FareFamily value) {
        this.fareFamily = value;
    }

}
