
package com.travelport.schema.util_v43_0;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}RailStationLocationModifiers" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="MaxResults" default="20"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer"&gt;
 *             &lt;minInclusive value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="StartFromResult" default="0"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer"&gt;
 *             &lt;minInclusive value="0"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="ProviderCode" type="{http://www.travelport.com/schema/common_v43_0}typeProviderCode" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "railStationLocationModifiers"
})
@XmlRootElement(name = "ReferenceDataSearchModifiers")
public class ReferenceDataSearchModifiers {

    @XmlElement(name = "RailStationLocationModifiers")
    protected RailStationLocationModifiers railStationLocationModifiers;
    @XmlAttribute(name = "MaxResults")
    protected BigInteger maxResults;
    @XmlAttribute(name = "StartFromResult")
    protected BigInteger startFromResult;
    @XmlAttribute(name = "ProviderCode")
    protected String providerCode;

    /**
     * Obtiene el valor de la propiedad railStationLocationModifiers.
     * 
     * @return
     *     possible object is
     *     {@link RailStationLocationModifiers }
     *     
     */
    public RailStationLocationModifiers getRailStationLocationModifiers() {
        return railStationLocationModifiers;
    }

    /**
     * Define el valor de la propiedad railStationLocationModifiers.
     * 
     * @param value
     *     allowed object is
     *     {@link RailStationLocationModifiers }
     *     
     */
    public void setRailStationLocationModifiers(RailStationLocationModifiers value) {
        this.railStationLocationModifiers = value;
    }

    /**
     * Obtiene el valor de la propiedad maxResults.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxResults() {
        if (maxResults == null) {
            return new BigInteger("20");
        } else {
            return maxResults;
        }
    }

    /**
     * Define el valor de la propiedad maxResults.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxResults(BigInteger value) {
        this.maxResults = value;
    }

    /**
     * Obtiene el valor de la propiedad startFromResult.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStartFromResult() {
        if (startFromResult == null) {
            return new BigInteger("0");
        } else {
            return startFromResult;
        }
    }

    /**
     * Define el valor de la propiedad startFromResult.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStartFromResult(BigInteger value) {
        this.startFromResult = value;
    }

    /**
     * Obtiene el valor de la propiedad providerCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProviderCode() {
        return providerCode;
    }

    /**
     * Define el valor de la propiedad providerCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProviderCode(String value) {
        this.providerCode = value;
    }

}
