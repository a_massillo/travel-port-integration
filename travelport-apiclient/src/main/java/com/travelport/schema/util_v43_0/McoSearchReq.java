
package com.travelport.schema.util_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.Airport;
import com.travelport.schema.common_v43_0.BaseReq;
import com.travelport.schema.common_v43_0.Carrier;
import com.travelport.schema.common_v43_0.Name;
import com.travelport.schema.common_v43_0.TypeTimeRange;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseReq"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Name" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Carrier" maxOccurs="10" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Airport" maxOccurs="10" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}TicketNumber" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}McoCreateDate" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}McoSearchModifiers" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "name",
    "carrier",
    "airport",
    "ticketNumber",
    "mcoCreateDate",
    "mcoSearchModifiers"
})
@XmlRootElement(name = "McoSearchReq")
public class McoSearchReq
    extends BaseReq
{

    @XmlElement(name = "Name", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected Name name;
    @XmlElement(name = "Carrier", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected List<Carrier> carrier;
    @XmlElement(name = "Airport", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected List<Airport> airport;
    @XmlElement(name = "TicketNumber", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected String ticketNumber;
    @XmlElement(name = "McoCreateDate")
    protected TypeTimeRange mcoCreateDate;
    @XmlElement(name = "McoSearchModifiers")
    protected McoSearchModifiers mcoSearchModifiers;

    /**
     * Obtiene el valor de la propiedad name.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getName() {
        return name;
    }

    /**
     * Define el valor de la propiedad name.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setName(Name value) {
        this.name = value;
    }

    /**
     * Gets the value of the carrier property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the carrier property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCarrier().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Carrier }
     * 
     * 
     */
    public List<Carrier> getCarrier() {
        if (carrier == null) {
            carrier = new ArrayList<Carrier>();
        }
        return this.carrier;
    }

    /**
     * Gets the value of the airport property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the airport property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAirport().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Airport }
     * 
     * 
     */
    public List<Airport> getAirport() {
        if (airport == null) {
            airport = new ArrayList<Airport>();
        }
        return this.airport;
    }

    /**
     * 
     *                                     The ticket that this MCO was issued
     *                                     in connection with. Could be the
     *                                     ticket that caused the fee, a
     *                                     residual from an exchange, or an
     *                                     airline service fee.
     *                                 
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketNumber() {
        return ticketNumber;
    }

    /**
     * Define el valor de la propiedad ticketNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketNumber(String value) {
        this.ticketNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad mcoCreateDate.
     * 
     * @return
     *     possible object is
     *     {@link TypeTimeRange }
     *     
     */
    public TypeTimeRange getMcoCreateDate() {
        return mcoCreateDate;
    }

    /**
     * Define el valor de la propiedad mcoCreateDate.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeTimeRange }
     *     
     */
    public void setMcoCreateDate(TypeTimeRange value) {
        this.mcoCreateDate = value;
    }

    /**
     * Obtiene el valor de la propiedad mcoSearchModifiers.
     * 
     * @return
     *     possible object is
     *     {@link McoSearchModifiers }
     *     
     */
    public McoSearchModifiers getMcoSearchModifiers() {
        return mcoSearchModifiers;
    }

    /**
     * Define el valor de la propiedad mcoSearchModifiers.
     * 
     * @param value
     *     allowed object is
     *     {@link McoSearchModifiers }
     *     
     */
    public void setMcoSearchModifiers(McoSearchModifiers value) {
        this.mcoSearchModifiers = value;
    }

}
