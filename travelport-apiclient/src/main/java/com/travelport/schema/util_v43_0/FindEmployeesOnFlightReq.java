
package com.travelport.schema.util_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseReq;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseReq"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}FlightCriteria" maxOccurs="999"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="AccountID" use="required" type="{http://www.travelport.com/schema/util_v43_0}typeAccountID" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "flightCriteria"
})
@XmlRootElement(name = "FindEmployeesOnFlightReq")
public class FindEmployeesOnFlightReq
    extends BaseReq
{

    @XmlElement(name = "FlightCriteria", required = true)
    protected List<FlightCriteria> flightCriteria;
    @XmlAttribute(name = "AccountID", required = true)
    protected long accountID;

    /**
     * Gets the value of the flightCriteria property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the flightCriteria property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFlightCriteria().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FlightCriteria }
     * 
     * 
     */
    public List<FlightCriteria> getFlightCriteria() {
        if (flightCriteria == null) {
            flightCriteria = new ArrayList<FlightCriteria>();
        }
        return this.flightCriteria;
    }

    /**
     * Obtiene el valor de la propiedad accountID.
     * 
     */
    public long getAccountID() {
        return accountID;
    }

    /**
     * Define el valor de la propiedad accountID.
     * 
     */
    public void setAccountID(long value) {
        this.accountID = value;
    }

}
