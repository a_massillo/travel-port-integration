
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para typeShowProvidersType.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;simpleType name="typeShowProvidersType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="All"/&gt;
 *     &lt;enumeration value="Provisioned"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "typeShowProvidersType")
@XmlEnum
public enum TypeShowProvidersType {

    @XmlEnumValue("All")
    ALL("All"),
    @XmlEnumValue("Provisioned")
    PROVISIONED("Provisioned");
    private final String value;

    TypeShowProvidersType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TypeShowProvidersType fromValue(String v) {
        for (TypeShowProvidersType c: TypeShowProvidersType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
