
package com.travelport.schema.util_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}VehicleUpsellQualify" maxOccurs="999"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}VehicleUpsellOffer"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vehicleUpsellQualify",
    "vehicleUpsellOffer"
})
@XmlRootElement(name = "VehicleUpsellSearchResult")
public class VehicleUpsellSearchResult {

    @XmlElement(name = "VehicleUpsellQualify", required = true)
    protected List<VehicleUpsellQualify> vehicleUpsellQualify;
    @XmlElement(name = "VehicleUpsellOffer", required = true)
    protected VehicleUpsellOffer vehicleUpsellOffer;

    /**
     * Gets the value of the vehicleUpsellQualify property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vehicleUpsellQualify property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVehicleUpsellQualify().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VehicleUpsellQualify }
     * 
     * 
     */
    public List<VehicleUpsellQualify> getVehicleUpsellQualify() {
        if (vehicleUpsellQualify == null) {
            vehicleUpsellQualify = new ArrayList<VehicleUpsellQualify>();
        }
        return this.vehicleUpsellQualify;
    }

    /**
     * Obtiene el valor de la propiedad vehicleUpsellOffer.
     * 
     * @return
     *     possible object is
     *     {@link VehicleUpsellOffer }
     *     
     */
    public VehicleUpsellOffer getVehicleUpsellOffer() {
        return vehicleUpsellOffer;
    }

    /**
     * Define el valor de la propiedad vehicleUpsellOffer.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleUpsellOffer }
     *     
     */
    public void setVehicleUpsellOffer(VehicleUpsellOffer value) {
        this.vehicleUpsellOffer = value;
    }

}
