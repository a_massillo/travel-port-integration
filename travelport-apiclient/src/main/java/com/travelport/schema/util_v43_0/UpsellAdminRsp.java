
package com.travelport.schema.util_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseRsp;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseRsp"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}AirUpsellRule" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}VehicleUpsellRule" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}HotelUpsellRule" maxOccurs="999" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "airUpsellRule",
    "vehicleUpsellRule",
    "hotelUpsellRule"
})
@XmlRootElement(name = "UpsellAdminRsp")
public class UpsellAdminRsp
    extends BaseRsp
{

    @XmlElement(name = "AirUpsellRule")
    protected List<AirUpsellRule> airUpsellRule;
    @XmlElement(name = "VehicleUpsellRule")
    protected List<VehicleUpsellRule> vehicleUpsellRule;
    @XmlElement(name = "HotelUpsellRule")
    protected List<HotelUpsellRule> hotelUpsellRule;

    /**
     * Gets the value of the airUpsellRule property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the airUpsellRule property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAirUpsellRule().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AirUpsellRule }
     * 
     * 
     */
    public List<AirUpsellRule> getAirUpsellRule() {
        if (airUpsellRule == null) {
            airUpsellRule = new ArrayList<AirUpsellRule>();
        }
        return this.airUpsellRule;
    }

    /**
     * Gets the value of the vehicleUpsellRule property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vehicleUpsellRule property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVehicleUpsellRule().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VehicleUpsellRule }
     * 
     * 
     */
    public List<VehicleUpsellRule> getVehicleUpsellRule() {
        if (vehicleUpsellRule == null) {
            vehicleUpsellRule = new ArrayList<VehicleUpsellRule>();
        }
        return this.vehicleUpsellRule;
    }

    /**
     * Gets the value of the hotelUpsellRule property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hotelUpsellRule property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHotelUpsellRule().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HotelUpsellRule }
     * 
     * 
     */
    public List<HotelUpsellRule> getHotelUpsellRule() {
        if (hotelUpsellRule == null) {
            hotelUpsellRule = new ArrayList<HotelUpsellRule>();
        }
        return this.hotelUpsellRule;
    }

}
