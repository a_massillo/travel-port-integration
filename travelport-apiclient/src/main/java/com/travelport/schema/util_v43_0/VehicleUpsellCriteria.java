
package com.travelport.schema.util_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}VehicleUpsellAdd" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}VehicleUpsellUpdate" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}VehicleUpsellDelete" maxOccurs="999" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vehicleUpsellAdd",
    "vehicleUpsellUpdate",
    "vehicleUpsellDelete"
})
@XmlRootElement(name = "VehicleUpsellCriteria")
public class VehicleUpsellCriteria {

    @XmlElement(name = "VehicleUpsellAdd")
    protected List<VehicleUpsellAdd> vehicleUpsellAdd;
    @XmlElement(name = "VehicleUpsellUpdate")
    protected List<VehicleUpsellUpdate> vehicleUpsellUpdate;
    @XmlElement(name = "VehicleUpsellDelete")
    protected List<VehicleUpsellDelete> vehicleUpsellDelete;

    /**
     * Gets the value of the vehicleUpsellAdd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vehicleUpsellAdd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVehicleUpsellAdd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VehicleUpsellAdd }
     * 
     * 
     */
    public List<VehicleUpsellAdd> getVehicleUpsellAdd() {
        if (vehicleUpsellAdd == null) {
            vehicleUpsellAdd = new ArrayList<VehicleUpsellAdd>();
        }
        return this.vehicleUpsellAdd;
    }

    /**
     * Gets the value of the vehicleUpsellUpdate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vehicleUpsellUpdate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVehicleUpsellUpdate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VehicleUpsellUpdate }
     * 
     * 
     */
    public List<VehicleUpsellUpdate> getVehicleUpsellUpdate() {
        if (vehicleUpsellUpdate == null) {
            vehicleUpsellUpdate = new ArrayList<VehicleUpsellUpdate>();
        }
        return this.vehicleUpsellUpdate;
    }

    /**
     * Gets the value of the vehicleUpsellDelete property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vehicleUpsellDelete property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVehicleUpsellDelete().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VehicleUpsellDelete }
     * 
     * 
     */
    public List<VehicleUpsellDelete> getVehicleUpsellDelete() {
        if (vehicleUpsellDelete == null) {
            vehicleUpsellDelete = new ArrayList<VehicleUpsellDelete>();
        }
        return this.vehicleUpsellDelete;
    }

}
