
package com.travelport.schema.util_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}AirUpsellAdd" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}AirUpsellUpdate" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}AirUpsellDelete" maxOccurs="999" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "airUpsellAdd",
    "airUpsellUpdate",
    "airUpsellDelete"
})
@XmlRootElement(name = "AirUpsellCriteria")
public class AirUpsellCriteria {

    @XmlElement(name = "AirUpsellAdd")
    protected List<AirUpsellAdd> airUpsellAdd;
    @XmlElement(name = "AirUpsellUpdate")
    protected List<AirUpsellUpdate> airUpsellUpdate;
    @XmlElement(name = "AirUpsellDelete")
    protected List<AirUpsellDelete> airUpsellDelete;

    /**
     * Gets the value of the airUpsellAdd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the airUpsellAdd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAirUpsellAdd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AirUpsellAdd }
     * 
     * 
     */
    public List<AirUpsellAdd> getAirUpsellAdd() {
        if (airUpsellAdd == null) {
            airUpsellAdd = new ArrayList<AirUpsellAdd>();
        }
        return this.airUpsellAdd;
    }

    /**
     * Gets the value of the airUpsellUpdate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the airUpsellUpdate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAirUpsellUpdate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AirUpsellUpdate }
     * 
     * 
     */
    public List<AirUpsellUpdate> getAirUpsellUpdate() {
        if (airUpsellUpdate == null) {
            airUpsellUpdate = new ArrayList<AirUpsellUpdate>();
        }
        return this.airUpsellUpdate;
    }

    /**
     * Gets the value of the airUpsellDelete property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the airUpsellDelete property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAirUpsellDelete().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AirUpsellDelete }
     * 
     * 
     */
    public List<AirUpsellDelete> getAirUpsellDelete() {
        if (airUpsellDelete == null) {
            airUpsellDelete = new ArrayList<AirUpsellDelete>();
        }
        return this.airUpsellDelete;
    }

}
