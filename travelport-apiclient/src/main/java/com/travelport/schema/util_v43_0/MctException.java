
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="Time" use="required" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="ArriveStation" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *       &lt;attribute name="DepartStation" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *       &lt;attribute name="Connection" use="required" type="{http://www.travelport.com/schema/util_v43_0}typeMctConnection" /&gt;
 *       &lt;attribute name="ArriveCarrier" type="{http://www.travelport.com/schema/common_v43_0}typeCarrier" /&gt;
 *       &lt;attribute name="DepartCarrier" type="{http://www.travelport.com/schema/common_v43_0}typeCarrier" /&gt;
 *       &lt;attribute name="ArriveFlightRangeBegin" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="ArriveFlightRangeEnd" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="DepartFlightRangeBegin" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="DepartFlightRangeEnd" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="ArriveEquipment" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="DepartEquipment" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="PreviousStation" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *       &lt;attribute name="NextStation" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *       &lt;attribute name="PreviousCountry" type="{http://www.travelport.com/schema/common_v43_0}typeCountry" /&gt;
 *       &lt;attribute name="NextCountry" type="{http://www.travelport.com/schema/common_v43_0}typeCountry" /&gt;
 *       &lt;attribute name="PreviousState" type="{http://www.travelport.com/schema/common_v43_0}typeState" /&gt;
 *       &lt;attribute name="NextState" type="{http://www.travelport.com/schema/common_v43_0}typeState" /&gt;
 *       &lt;attribute name="ArriveTerminal" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="DepartTerminal" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="EffectiveDate" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *       &lt;attribute name="DiscontinueDate" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "MctException")
public class MctException {

    @XmlAttribute(name = "Time", required = true)
    protected int time;
    @XmlAttribute(name = "ArriveStation", required = true)
    protected String arriveStation;
    @XmlAttribute(name = "DepartStation")
    protected String departStation;
    @XmlAttribute(name = "Connection", required = true)
    protected TypeMctConnection connection;
    @XmlAttribute(name = "ArriveCarrier")
    protected String arriveCarrier;
    @XmlAttribute(name = "DepartCarrier")
    protected String departCarrier;
    @XmlAttribute(name = "ArriveFlightRangeBegin")
    protected String arriveFlightRangeBegin;
    @XmlAttribute(name = "ArriveFlightRangeEnd")
    protected String arriveFlightRangeEnd;
    @XmlAttribute(name = "DepartFlightRangeBegin")
    protected String departFlightRangeBegin;
    @XmlAttribute(name = "DepartFlightRangeEnd")
    protected String departFlightRangeEnd;
    @XmlAttribute(name = "ArriveEquipment")
    protected String arriveEquipment;
    @XmlAttribute(name = "DepartEquipment")
    protected String departEquipment;
    @XmlAttribute(name = "PreviousStation")
    protected String previousStation;
    @XmlAttribute(name = "NextStation")
    protected String nextStation;
    @XmlAttribute(name = "PreviousCountry")
    protected String previousCountry;
    @XmlAttribute(name = "NextCountry")
    protected String nextCountry;
    @XmlAttribute(name = "PreviousState")
    protected String previousState;
    @XmlAttribute(name = "NextState")
    protected String nextState;
    @XmlAttribute(name = "ArriveTerminal")
    protected String arriveTerminal;
    @XmlAttribute(name = "DepartTerminal")
    protected String departTerminal;
    @XmlAttribute(name = "EffectiveDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar effectiveDate;
    @XmlAttribute(name = "DiscontinueDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar discontinueDate;

    /**
     * Obtiene el valor de la propiedad time.
     * 
     */
    public int getTime() {
        return time;
    }

    /**
     * Define el valor de la propiedad time.
     * 
     */
    public void setTime(int value) {
        this.time = value;
    }

    /**
     * Obtiene el valor de la propiedad arriveStation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArriveStation() {
        return arriveStation;
    }

    /**
     * Define el valor de la propiedad arriveStation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArriveStation(String value) {
        this.arriveStation = value;
    }

    /**
     * Obtiene el valor de la propiedad departStation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartStation() {
        return departStation;
    }

    /**
     * Define el valor de la propiedad departStation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartStation(String value) {
        this.departStation = value;
    }

    /**
     * Obtiene el valor de la propiedad connection.
     * 
     * @return
     *     possible object is
     *     {@link TypeMctConnection }
     *     
     */
    public TypeMctConnection getConnection() {
        return connection;
    }

    /**
     * Define el valor de la propiedad connection.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeMctConnection }
     *     
     */
    public void setConnection(TypeMctConnection value) {
        this.connection = value;
    }

    /**
     * Obtiene el valor de la propiedad arriveCarrier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArriveCarrier() {
        return arriveCarrier;
    }

    /**
     * Define el valor de la propiedad arriveCarrier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArriveCarrier(String value) {
        this.arriveCarrier = value;
    }

    /**
     * Obtiene el valor de la propiedad departCarrier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartCarrier() {
        return departCarrier;
    }

    /**
     * Define el valor de la propiedad departCarrier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartCarrier(String value) {
        this.departCarrier = value;
    }

    /**
     * Obtiene el valor de la propiedad arriveFlightRangeBegin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArriveFlightRangeBegin() {
        return arriveFlightRangeBegin;
    }

    /**
     * Define el valor de la propiedad arriveFlightRangeBegin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArriveFlightRangeBegin(String value) {
        this.arriveFlightRangeBegin = value;
    }

    /**
     * Obtiene el valor de la propiedad arriveFlightRangeEnd.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArriveFlightRangeEnd() {
        return arriveFlightRangeEnd;
    }

    /**
     * Define el valor de la propiedad arriveFlightRangeEnd.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArriveFlightRangeEnd(String value) {
        this.arriveFlightRangeEnd = value;
    }

    /**
     * Obtiene el valor de la propiedad departFlightRangeBegin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartFlightRangeBegin() {
        return departFlightRangeBegin;
    }

    /**
     * Define el valor de la propiedad departFlightRangeBegin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartFlightRangeBegin(String value) {
        this.departFlightRangeBegin = value;
    }

    /**
     * Obtiene el valor de la propiedad departFlightRangeEnd.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartFlightRangeEnd() {
        return departFlightRangeEnd;
    }

    /**
     * Define el valor de la propiedad departFlightRangeEnd.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartFlightRangeEnd(String value) {
        this.departFlightRangeEnd = value;
    }

    /**
     * Obtiene el valor de la propiedad arriveEquipment.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArriveEquipment() {
        return arriveEquipment;
    }

    /**
     * Define el valor de la propiedad arriveEquipment.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArriveEquipment(String value) {
        this.arriveEquipment = value;
    }

    /**
     * Obtiene el valor de la propiedad departEquipment.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartEquipment() {
        return departEquipment;
    }

    /**
     * Define el valor de la propiedad departEquipment.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartEquipment(String value) {
        this.departEquipment = value;
    }

    /**
     * Obtiene el valor de la propiedad previousStation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreviousStation() {
        return previousStation;
    }

    /**
     * Define el valor de la propiedad previousStation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreviousStation(String value) {
        this.previousStation = value;
    }

    /**
     * Obtiene el valor de la propiedad nextStation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextStation() {
        return nextStation;
    }

    /**
     * Define el valor de la propiedad nextStation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextStation(String value) {
        this.nextStation = value;
    }

    /**
     * Obtiene el valor de la propiedad previousCountry.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreviousCountry() {
        return previousCountry;
    }

    /**
     * Define el valor de la propiedad previousCountry.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreviousCountry(String value) {
        this.previousCountry = value;
    }

    /**
     * Obtiene el valor de la propiedad nextCountry.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextCountry() {
        return nextCountry;
    }

    /**
     * Define el valor de la propiedad nextCountry.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextCountry(String value) {
        this.nextCountry = value;
    }

    /**
     * Obtiene el valor de la propiedad previousState.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreviousState() {
        return previousState;
    }

    /**
     * Define el valor de la propiedad previousState.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreviousState(String value) {
        this.previousState = value;
    }

    /**
     * Obtiene el valor de la propiedad nextState.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextState() {
        return nextState;
    }

    /**
     * Define el valor de la propiedad nextState.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextState(String value) {
        this.nextState = value;
    }

    /**
     * Obtiene el valor de la propiedad arriveTerminal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArriveTerminal() {
        return arriveTerminal;
    }

    /**
     * Define el valor de la propiedad arriveTerminal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArriveTerminal(String value) {
        this.arriveTerminal = value;
    }

    /**
     * Obtiene el valor de la propiedad departTerminal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartTerminal() {
        return departTerminal;
    }

    /**
     * Define el valor de la propiedad departTerminal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartTerminal(String value) {
        this.departTerminal = value;
    }

    /**
     * Obtiene el valor de la propiedad effectiveDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Define el valor de la propiedad effectiveDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDate(XMLGregorianCalendar value) {
        this.effectiveDate = value;
    }

    /**
     * Obtiene el valor de la propiedad discontinueDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDiscontinueDate() {
        return discontinueDate;
    }

    /**
     * Define el valor de la propiedad discontinueDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDiscontinueDate(XMLGregorianCalendar value) {
        this.discontinueDate = value;
    }

}
