
package com.travelport.schema.util_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.travelport.schema.common_v43_0.BaseRsp;
import com.travelport.schema.common_v43_0.Name;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseRsp"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="EmployeesOnFlight" maxOccurs="999" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Name" maxOccurs="999"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="UniversalRecordLocator" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeLocatorCode" /&gt;
 *                 &lt;attribute name="Destination" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *                 &lt;attribute name="Origin" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *                 &lt;attribute name="DepartureDate" use="required" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *                 &lt;attribute name="FlightNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="Carrier" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeCarrier" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "employeesOnFlight"
})
@XmlRootElement(name = "FindEmployeesOnFlightRsp")
public class FindEmployeesOnFlightRsp
    extends BaseRsp
{

    @XmlElement(name = "EmployeesOnFlight")
    protected List<FindEmployeesOnFlightRsp.EmployeesOnFlight> employeesOnFlight;

    /**
     * Gets the value of the employeesOnFlight property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the employeesOnFlight property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEmployeesOnFlight().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FindEmployeesOnFlightRsp.EmployeesOnFlight }
     * 
     * 
     */
    public List<FindEmployeesOnFlightRsp.EmployeesOnFlight> getEmployeesOnFlight() {
        if (employeesOnFlight == null) {
            employeesOnFlight = new ArrayList<FindEmployeesOnFlightRsp.EmployeesOnFlight>();
        }
        return this.employeesOnFlight;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}Name" maxOccurs="999"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="UniversalRecordLocator" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeLocatorCode" /&gt;
     *       &lt;attribute name="Destination" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
     *       &lt;attribute name="Origin" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
     *       &lt;attribute name="DepartureDate" use="required" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
     *       &lt;attribute name="FlightNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="Carrier" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeCarrier" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "name"
    })
    public static class EmployeesOnFlight {

        @XmlElement(name = "Name", namespace = "http://www.travelport.com/schema/common_v43_0", required = true)
        protected List<Name> name;
        @XmlAttribute(name = "UniversalRecordLocator", required = true)
        protected String universalRecordLocator;
        @XmlAttribute(name = "Destination", required = true)
        protected String destination;
        @XmlAttribute(name = "Origin", required = true)
        protected String origin;
        @XmlAttribute(name = "DepartureDate", required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar departureDate;
        @XmlAttribute(name = "FlightNumber", required = true)
        protected String flightNumber;
        @XmlAttribute(name = "Carrier", required = true)
        protected String carrier;

        /**
         * Gets the value of the name property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the name property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getName().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Name }
         * 
         * 
         */
        public List<Name> getName() {
            if (name == null) {
                name = new ArrayList<Name>();
            }
            return this.name;
        }

        /**
         * Obtiene el valor de la propiedad universalRecordLocator.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUniversalRecordLocator() {
            return universalRecordLocator;
        }

        /**
         * Define el valor de la propiedad universalRecordLocator.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUniversalRecordLocator(String value) {
            this.universalRecordLocator = value;
        }

        /**
         * Obtiene el valor de la propiedad destination.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDestination() {
            return destination;
        }

        /**
         * Define el valor de la propiedad destination.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDestination(String value) {
            this.destination = value;
        }

        /**
         * Obtiene el valor de la propiedad origin.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOrigin() {
            return origin;
        }

        /**
         * Define el valor de la propiedad origin.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOrigin(String value) {
            this.origin = value;
        }

        /**
         * Obtiene el valor de la propiedad departureDate.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDepartureDate() {
            return departureDate;
        }

        /**
         * Define el valor de la propiedad departureDate.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDepartureDate(XMLGregorianCalendar value) {
            this.departureDate = value;
        }

        /**
         * Obtiene el valor de la propiedad flightNumber.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFlightNumber() {
            return flightNumber;
        }

        /**
         * Define el valor de la propiedad flightNumber.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFlightNumber(String value) {
            this.flightNumber = value;
        }

        /**
         * Obtiene el valor de la propiedad carrier.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCarrier() {
            return carrier;
        }

        /**
         * Define el valor de la propiedad carrier.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCarrier(String value) {
            this.carrier = value;
        }

    }

}
