
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseReq;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseReq"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element ref="{http://www.travelport.com/schema/util_v43_0}AirUpsellSearchCriteria" minOccurs="0"/&gt;
 *           &lt;element ref="{http://www.travelport.com/schema/util_v43_0}HotelUpsellSearchCriteria" minOccurs="0"/&gt;
 *           &lt;element ref="{http://www.travelport.com/schema/util_v43_0}VehicleUpsellSearchCriteria" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}UpsellSearchModifier"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "airUpsellSearchCriteria",
    "hotelUpsellSearchCriteria",
    "vehicleUpsellSearchCriteria",
    "upsellSearchModifier"
})
@XmlRootElement(name = "UpsellSearchReq")
public class UpsellSearchReq
    extends BaseReq
{

    @XmlElement(name = "AirUpsellSearchCriteria")
    protected AirUpsellSearchCriteria airUpsellSearchCriteria;
    @XmlElement(name = "HotelUpsellSearchCriteria")
    protected HotelUpsellSearchCriteria hotelUpsellSearchCriteria;
    @XmlElement(name = "VehicleUpsellSearchCriteria")
    protected VehicleUpsellSearchCriteria vehicleUpsellSearchCriteria;
    @XmlElement(name = "UpsellSearchModifier", required = true)
    protected UpsellSearchModifier upsellSearchModifier;

    /**
     * Obtiene el valor de la propiedad airUpsellSearchCriteria.
     * 
     * @return
     *     possible object is
     *     {@link AirUpsellSearchCriteria }
     *     
     */
    public AirUpsellSearchCriteria getAirUpsellSearchCriteria() {
        return airUpsellSearchCriteria;
    }

    /**
     * Define el valor de la propiedad airUpsellSearchCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link AirUpsellSearchCriteria }
     *     
     */
    public void setAirUpsellSearchCriteria(AirUpsellSearchCriteria value) {
        this.airUpsellSearchCriteria = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelUpsellSearchCriteria.
     * 
     * @return
     *     possible object is
     *     {@link HotelUpsellSearchCriteria }
     *     
     */
    public HotelUpsellSearchCriteria getHotelUpsellSearchCriteria() {
        return hotelUpsellSearchCriteria;
    }

    /**
     * Define el valor de la propiedad hotelUpsellSearchCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelUpsellSearchCriteria }
     *     
     */
    public void setHotelUpsellSearchCriteria(HotelUpsellSearchCriteria value) {
        this.hotelUpsellSearchCriteria = value;
    }

    /**
     * Obtiene el valor de la propiedad vehicleUpsellSearchCriteria.
     * 
     * @return
     *     possible object is
     *     {@link VehicleUpsellSearchCriteria }
     *     
     */
    public VehicleUpsellSearchCriteria getVehicleUpsellSearchCriteria() {
        return vehicleUpsellSearchCriteria;
    }

    /**
     * Define el valor de la propiedad vehicleUpsellSearchCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleUpsellSearchCriteria }
     *     
     */
    public void setVehicleUpsellSearchCriteria(VehicleUpsellSearchCriteria value) {
        this.vehicleUpsellSearchCriteria = value;
    }

    /**
     * Obtiene el valor de la propiedad upsellSearchModifier.
     * 
     * @return
     *     possible object is
     *     {@link UpsellSearchModifier }
     *     
     */
    public UpsellSearchModifier getUpsellSearchModifier() {
        return upsellSearchModifier;
    }

    /**
     * Define el valor de la propiedad upsellSearchModifier.
     * 
     * @param value
     *     allowed object is
     *     {@link UpsellSearchModifier }
     *     
     */
    public void setUpsellSearchModifier(UpsellSearchModifier value) {
        this.upsellSearchModifier = value;
    }

}
