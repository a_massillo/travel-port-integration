
package com.travelport.schema.util_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}HotelUpsellAdd" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}HotelUpsellUpdate" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}HotelUpsellDelete" maxOccurs="999" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "hotelUpsellAdd",
    "hotelUpsellUpdate",
    "hotelUpsellDelete"
})
@XmlRootElement(name = "HotelUpsellCriteria")
public class HotelUpsellCriteria {

    @XmlElement(name = "HotelUpsellAdd")
    protected List<HotelUpsellAdd> hotelUpsellAdd;
    @XmlElement(name = "HotelUpsellUpdate")
    protected List<HotelUpsellUpdate> hotelUpsellUpdate;
    @XmlElement(name = "HotelUpsellDelete")
    protected List<HotelUpsellDelete> hotelUpsellDelete;

    /**
     * Gets the value of the hotelUpsellAdd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hotelUpsellAdd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHotelUpsellAdd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HotelUpsellAdd }
     * 
     * 
     */
    public List<HotelUpsellAdd> getHotelUpsellAdd() {
        if (hotelUpsellAdd == null) {
            hotelUpsellAdd = new ArrayList<HotelUpsellAdd>();
        }
        return this.hotelUpsellAdd;
    }

    /**
     * Gets the value of the hotelUpsellUpdate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hotelUpsellUpdate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHotelUpsellUpdate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HotelUpsellUpdate }
     * 
     * 
     */
    public List<HotelUpsellUpdate> getHotelUpsellUpdate() {
        if (hotelUpsellUpdate == null) {
            hotelUpsellUpdate = new ArrayList<HotelUpsellUpdate>();
        }
        return this.hotelUpsellUpdate;
    }

    /**
     * Gets the value of the hotelUpsellDelete property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hotelUpsellDelete property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHotelUpsellDelete().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HotelUpsellDelete }
     * 
     * 
     */
    public List<HotelUpsellDelete> getHotelUpsellDelete() {
        if (hotelUpsellDelete == null) {
            hotelUpsellDelete = new ArrayList<HotelUpsellDelete>();
        }
        return this.hotelUpsellDelete;
    }

}
