
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}VehicleUpsellQualify" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}VehicleUpsellOffer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vehicleUpsellQualify",
    "vehicleUpsellOffer"
})
@XmlRootElement(name = "VehicleUpsellUpdate")
public class VehicleUpsellUpdate {

    @XmlElement(name = "VehicleUpsellQualify")
    protected VehicleUpsellQualify vehicleUpsellQualify;
    @XmlElement(name = "VehicleUpsellOffer")
    protected VehicleUpsellOffer vehicleUpsellOffer;

    /**
     * Obtiene el valor de la propiedad vehicleUpsellQualify.
     * 
     * @return
     *     possible object is
     *     {@link VehicleUpsellQualify }
     *     
     */
    public VehicleUpsellQualify getVehicleUpsellQualify() {
        return vehicleUpsellQualify;
    }

    /**
     * Define el valor de la propiedad vehicleUpsellQualify.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleUpsellQualify }
     *     
     */
    public void setVehicleUpsellQualify(VehicleUpsellQualify value) {
        this.vehicleUpsellQualify = value;
    }

    /**
     * Obtiene el valor de la propiedad vehicleUpsellOffer.
     * 
     * @return
     *     possible object is
     *     {@link VehicleUpsellOffer }
     *     
     */
    public VehicleUpsellOffer getVehicleUpsellOffer() {
        return vehicleUpsellOffer;
    }

    /**
     * Define el valor de la propiedad vehicleUpsellOffer.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleUpsellOffer }
     *     
     */
    public void setVehicleUpsellOffer(VehicleUpsellOffer value) {
        this.vehicleUpsellOffer = value;
    }

}
