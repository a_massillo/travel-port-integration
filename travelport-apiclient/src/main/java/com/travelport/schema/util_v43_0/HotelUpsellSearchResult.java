
package com.travelport.schema.util_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}HotelUpsellQualify" maxOccurs="999"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}HotelUpsellOffer"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "hotelUpsellQualify",
    "hotelUpsellOffer"
})
@XmlRootElement(name = "HotelUpsellSearchResult")
public class HotelUpsellSearchResult {

    @XmlElement(name = "HotelUpsellQualify", required = true)
    protected List<HotelUpsellQualify> hotelUpsellQualify;
    @XmlElement(name = "HotelUpsellOffer", required = true)
    protected HotelUpsellOffer hotelUpsellOffer;

    /**
     * Gets the value of the hotelUpsellQualify property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hotelUpsellQualify property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHotelUpsellQualify().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HotelUpsellQualify }
     * 
     * 
     */
    public List<HotelUpsellQualify> getHotelUpsellQualify() {
        if (hotelUpsellQualify == null) {
            hotelUpsellQualify = new ArrayList<HotelUpsellQualify>();
        }
        return this.hotelUpsellQualify;
    }

    /**
     * Obtiene el valor de la propiedad hotelUpsellOffer.
     * 
     * @return
     *     possible object is
     *     {@link HotelUpsellOffer }
     *     
     */
    public HotelUpsellOffer getHotelUpsellOffer() {
        return hotelUpsellOffer;
    }

    /**
     * Define el valor de la propiedad hotelUpsellOffer.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelUpsellOffer }
     *     
     */
    public void setHotelUpsellOffer(HotelUpsellOffer value) {
        this.hotelUpsellOffer = value;
    }

}
