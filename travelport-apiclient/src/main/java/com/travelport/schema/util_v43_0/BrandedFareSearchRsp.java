
package com.travelport.schema.util_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseRsp;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseRsp"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}FareFamily" maxOccurs="999"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="MoreResults" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeMoreResults" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fareFamily"
})
@XmlRootElement(name = "BrandedFareSearchRsp")
public class BrandedFareSearchRsp
    extends BaseRsp
{

    @XmlElement(name = "FareFamily", required = true)
    protected List<FareFamily> fareFamily;
    @XmlAttribute(name = "MoreResults", required = true)
    protected boolean moreResults;

    /**
     * Gets the value of the fareFamily property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fareFamily property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFareFamily().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FareFamily }
     * 
     * 
     */
    public List<FareFamily> getFareFamily() {
        if (fareFamily == null) {
            fareFamily = new ArrayList<FareFamily>();
        }
        return this.fareFamily;
    }

    /**
     * Obtiene el valor de la propiedad moreResults.
     * 
     */
    public boolean isMoreResults() {
        return moreResults;
    }

    /**
     * Define el valor de la propiedad moreResults.
     * 
     */
    public void setMoreResults(boolean value) {
        this.moreResults = value;
    }

}
