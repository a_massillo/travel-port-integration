
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}AirUpsellQualify"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}AirUpsellOffer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "airUpsellQualify",
    "airUpsellOffer"
})
@XmlRootElement(name = "AirUpsellAdd")
public class AirUpsellAdd {

    @XmlElement(name = "AirUpsellQualify", required = true)
    protected AirUpsellQualify airUpsellQualify;
    @XmlElement(name = "AirUpsellOffer")
    protected AirUpsellOffer airUpsellOffer;

    /**
     * Obtiene el valor de la propiedad airUpsellQualify.
     * 
     * @return
     *     possible object is
     *     {@link AirUpsellQualify }
     *     
     */
    public AirUpsellQualify getAirUpsellQualify() {
        return airUpsellQualify;
    }

    /**
     * Define el valor de la propiedad airUpsellQualify.
     * 
     * @param value
     *     allowed object is
     *     {@link AirUpsellQualify }
     *     
     */
    public void setAirUpsellQualify(AirUpsellQualify value) {
        this.airUpsellQualify = value;
    }

    /**
     * Obtiene el valor de la propiedad airUpsellOffer.
     * 
     * @return
     *     possible object is
     *     {@link AirUpsellOffer }
     *     
     */
    public AirUpsellOffer getAirUpsellOffer() {
        return airUpsellOffer;
    }

    /**
     * Define el valor de la propiedad airUpsellOffer.
     * 
     * @param value
     *     allowed object is
     *     {@link AirUpsellOffer }
     *     
     */
    public void setAirUpsellOffer(AirUpsellOffer value) {
        this.airUpsellOffer = value;
    }

}
