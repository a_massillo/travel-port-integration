
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}AirUpsellOfferSearchCriteria" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}AirUpsellQualifySearchCriteria"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "airUpsellOfferSearchCriteria",
    "airUpsellQualifySearchCriteria"
})
@XmlRootElement(name = "AirUpsellSearchCriteria")
public class AirUpsellSearchCriteria {

    @XmlElement(name = "AirUpsellOfferSearchCriteria")
    protected AirUpsellOfferSearchCriteria airUpsellOfferSearchCriteria;
    @XmlElement(name = "AirUpsellQualifySearchCriteria", required = true)
    protected AirUpsellQualifySearchCriteria airUpsellQualifySearchCriteria;

    /**
     * Obtiene el valor de la propiedad airUpsellOfferSearchCriteria.
     * 
     * @return
     *     possible object is
     *     {@link AirUpsellOfferSearchCriteria }
     *     
     */
    public AirUpsellOfferSearchCriteria getAirUpsellOfferSearchCriteria() {
        return airUpsellOfferSearchCriteria;
    }

    /**
     * Define el valor de la propiedad airUpsellOfferSearchCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link AirUpsellOfferSearchCriteria }
     *     
     */
    public void setAirUpsellOfferSearchCriteria(AirUpsellOfferSearchCriteria value) {
        this.airUpsellOfferSearchCriteria = value;
    }

    /**
     * Obtiene el valor de la propiedad airUpsellQualifySearchCriteria.
     * 
     * @return
     *     possible object is
     *     {@link AirUpsellQualifySearchCriteria }
     *     
     */
    public AirUpsellQualifySearchCriteria getAirUpsellQualifySearchCriteria() {
        return airUpsellQualifySearchCriteria;
    }

    /**
     * Define el valor de la propiedad airUpsellQualifySearchCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link AirUpsellQualifySearchCriteria }
     *     
     */
    public void setAirUpsellQualifySearchCriteria(AirUpsellQualifySearchCriteria value) {
        this.airUpsellQualifySearchCriteria = value;
    }

}
