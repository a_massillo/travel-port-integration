
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}HotelUpsellQualify" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}HotelUpsellOffer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "hotelUpsellQualify",
    "hotelUpsellOffer"
})
@XmlRootElement(name = "HotelUpsellRule")
public class HotelUpsellRule {

    @XmlElement(name = "HotelUpsellQualify")
    protected HotelUpsellQualify hotelUpsellQualify;
    @XmlElement(name = "HotelUpsellOffer")
    protected HotelUpsellOffer hotelUpsellOffer;

    /**
     * Obtiene el valor de la propiedad hotelUpsellQualify.
     * 
     * @return
     *     possible object is
     *     {@link HotelUpsellQualify }
     *     
     */
    public HotelUpsellQualify getHotelUpsellQualify() {
        return hotelUpsellQualify;
    }

    /**
     * Define el valor de la propiedad hotelUpsellQualify.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelUpsellQualify }
     *     
     */
    public void setHotelUpsellQualify(HotelUpsellQualify value) {
        this.hotelUpsellQualify = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelUpsellOffer.
     * 
     * @return
     *     possible object is
     *     {@link HotelUpsellOffer }
     *     
     */
    public HotelUpsellOffer getHotelUpsellOffer() {
        return hotelUpsellOffer;
    }

    /**
     * Define el valor de la propiedad hotelUpsellOffer.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelUpsellOffer }
     *     
     */
    public void setHotelUpsellOffer(HotelUpsellOffer value) {
        this.hotelUpsellOffer = value;
    }

}
