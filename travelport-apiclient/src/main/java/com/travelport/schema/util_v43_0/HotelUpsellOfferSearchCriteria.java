
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.CorporateDiscountID;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}CorporateDiscountID" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="RatePlanType" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeRatePlanType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "corporateDiscountID"
})
@XmlRootElement(name = "HotelUpsellOfferSearchCriteria")
public class HotelUpsellOfferSearchCriteria {

    @XmlElement(name = "CorporateDiscountID", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected CorporateDiscountID corporateDiscountID;
    @XmlAttribute(name = "RatePlanType", required = true)
    protected String ratePlanType;

    /**
     * Obtiene el valor de la propiedad corporateDiscountID.
     * 
     * @return
     *     possible object is
     *     {@link CorporateDiscountID }
     *     
     */
    public CorporateDiscountID getCorporateDiscountID() {
        return corporateDiscountID;
    }

    /**
     * Define el valor de la propiedad corporateDiscountID.
     * 
     * @param value
     *     allowed object is
     *     {@link CorporateDiscountID }
     *     
     */
    public void setCorporateDiscountID(CorporateDiscountID value) {
        this.corporateDiscountID = value;
    }

    /**
     * Obtiene el valor de la propiedad ratePlanType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRatePlanType() {
        return ratePlanType;
    }

    /**
     * Define el valor de la propiedad ratePlanType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRatePlanType(String value) {
        this.ratePlanType = value;
    }

}
