
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseReq;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseReq"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}AirUpsellCriteria" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}VehicleUpsellCriteria" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}HotelUpsellCriteria" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "airUpsellCriteria",
    "vehicleUpsellCriteria",
    "hotelUpsellCriteria"
})
@XmlRootElement(name = "UpsellAdminReq")
public class UpsellAdminReq
    extends BaseReq
{

    @XmlElement(name = "AirUpsellCriteria")
    protected AirUpsellCriteria airUpsellCriteria;
    @XmlElement(name = "VehicleUpsellCriteria")
    protected VehicleUpsellCriteria vehicleUpsellCriteria;
    @XmlElement(name = "HotelUpsellCriteria")
    protected HotelUpsellCriteria hotelUpsellCriteria;

    /**
     * Obtiene el valor de la propiedad airUpsellCriteria.
     * 
     * @return
     *     possible object is
     *     {@link AirUpsellCriteria }
     *     
     */
    public AirUpsellCriteria getAirUpsellCriteria() {
        return airUpsellCriteria;
    }

    /**
     * Define el valor de la propiedad airUpsellCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link AirUpsellCriteria }
     *     
     */
    public void setAirUpsellCriteria(AirUpsellCriteria value) {
        this.airUpsellCriteria = value;
    }

    /**
     * Obtiene el valor de la propiedad vehicleUpsellCriteria.
     * 
     * @return
     *     possible object is
     *     {@link VehicleUpsellCriteria }
     *     
     */
    public VehicleUpsellCriteria getVehicleUpsellCriteria() {
        return vehicleUpsellCriteria;
    }

    /**
     * Define el valor de la propiedad vehicleUpsellCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleUpsellCriteria }
     *     
     */
    public void setVehicleUpsellCriteria(VehicleUpsellCriteria value) {
        this.vehicleUpsellCriteria = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelUpsellCriteria.
     * 
     * @return
     *     possible object is
     *     {@link HotelUpsellCriteria }
     *     
     */
    public HotelUpsellCriteria getHotelUpsellCriteria() {
        return hotelUpsellCriteria;
    }

    /**
     * Define el valor de la propiedad hotelUpsellCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelUpsellCriteria }
     *     
     */
    public void setHotelUpsellCriteria(HotelUpsellCriteria value) {
        this.hotelUpsellCriteria = value;
    }

}
