
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.CorporateDiscountID;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/util_v43_0}UpsellSearchCriteria"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}CorporateDiscountID" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="HotelChainCode" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeHotelChainCode" /&gt;
 *       &lt;attribute name="HotelCode" type="{http://www.travelport.com/schema/common_v43_0}typeHotelCode" /&gt;
 *       &lt;attribute name="HotelLocation" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *       &lt;attribute name="RatePlanType" type="{http://www.travelport.com/schema/common_v43_0}typeRatePlanType" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "corporateDiscountID"
})
@XmlRootElement(name = "HotelUpsellQualifySearchCriteria")
public class HotelUpsellQualifySearchCriteria
    extends UpsellSearchCriteria
{

    @XmlElement(name = "CorporateDiscountID", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected CorporateDiscountID corporateDiscountID;
    @XmlAttribute(name = "HotelChainCode", required = true)
    protected String hotelChainCode;
    @XmlAttribute(name = "HotelCode")
    protected String hotelCode;
    @XmlAttribute(name = "HotelLocation")
    protected String hotelLocation;
    @XmlAttribute(name = "RatePlanType")
    protected String ratePlanType;

    /**
     * Obtiene el valor de la propiedad corporateDiscountID.
     * 
     * @return
     *     possible object is
     *     {@link CorporateDiscountID }
     *     
     */
    public CorporateDiscountID getCorporateDiscountID() {
        return corporateDiscountID;
    }

    /**
     * Define el valor de la propiedad corporateDiscountID.
     * 
     * @param value
     *     allowed object is
     *     {@link CorporateDiscountID }
     *     
     */
    public void setCorporateDiscountID(CorporateDiscountID value) {
        this.corporateDiscountID = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelChainCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelChainCode() {
        return hotelChainCode;
    }

    /**
     * Define el valor de la propiedad hotelChainCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelChainCode(String value) {
        this.hotelChainCode = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelCode() {
        return hotelCode;
    }

    /**
     * Define el valor de la propiedad hotelCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelCode(String value) {
        this.hotelCode = value;
    }

    /**
     * Obtiene el valor de la propiedad hotelLocation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelLocation() {
        return hotelLocation;
    }

    /**
     * Define el valor de la propiedad hotelLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelLocation(String value) {
        this.hotelLocation = value;
    }

    /**
     * Obtiene el valor de la propiedad ratePlanType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRatePlanType() {
        return ratePlanType;
    }

    /**
     * Define el valor de la propiedad ratePlanType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRatePlanType(String value) {
        this.ratePlanType = value;
    }

}
