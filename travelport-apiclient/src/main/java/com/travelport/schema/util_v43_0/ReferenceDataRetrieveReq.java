
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseReq;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseReq"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}ReferenceDataSearchModifiers" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}RequestReferenceDataItem" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="TypeCode" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeTypeCode" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "referenceDataSearchModifiers",
    "requestReferenceDataItem"
})
@XmlRootElement(name = "ReferenceDataRetrieveReq")
public class ReferenceDataRetrieveReq
    extends BaseReq
{

    @XmlElement(name = "ReferenceDataSearchModifiers")
    protected ReferenceDataSearchModifiers referenceDataSearchModifiers;
    @XmlElement(name = "RequestReferenceDataItem")
    protected RequestReferenceDataItem requestReferenceDataItem;
    @XmlAttribute(name = "TypeCode", required = true)
    protected String typeCode;

    /**
     * Obtiene el valor de la propiedad referenceDataSearchModifiers.
     * 
     * @return
     *     possible object is
     *     {@link ReferenceDataSearchModifiers }
     *     
     */
    public ReferenceDataSearchModifiers getReferenceDataSearchModifiers() {
        return referenceDataSearchModifiers;
    }

    /**
     * Define el valor de la propiedad referenceDataSearchModifiers.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferenceDataSearchModifiers }
     *     
     */
    public void setReferenceDataSearchModifiers(ReferenceDataSearchModifiers value) {
        this.referenceDataSearchModifiers = value;
    }

    /**
     * Obtiene el valor de la propiedad requestReferenceDataItem.
     * 
     * @return
     *     possible object is
     *     {@link RequestReferenceDataItem }
     *     
     */
    public RequestReferenceDataItem getRequestReferenceDataItem() {
        return requestReferenceDataItem;
    }

    /**
     * Define el valor de la propiedad requestReferenceDataItem.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestReferenceDataItem }
     *     
     */
    public void setRequestReferenceDataItem(RequestReferenceDataItem value) {
        this.requestReferenceDataItem = value;
    }

    /**
     * Obtiene el valor de la propiedad typeCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeCode() {
        return typeCode;
    }

    /**
     * Define el valor de la propiedad typeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeCode(String value) {
        this.typeCode = value;
    }

}
