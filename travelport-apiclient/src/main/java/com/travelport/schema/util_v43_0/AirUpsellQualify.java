
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.travelport.schema.common_v43_0.AccountCode;
import com.travelport.schema.common_v43_0.TypeElementStatus;
import com.travelport.schema.common_v43_0.TypeTimeSpec;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DepartureTime" type="{http://www.travelport.com/schema/common_v43_0}typeTimeSpec" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}FlightSpec" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}OperatingFlightSpec" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/common_v43_0}AccountCode" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attGroup ref="{http://www.travelport.com/schema/common_v43_0}attrElementKeyResults"/&gt;
 *       &lt;attribute name="Carrier" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeCarrier" /&gt;
 *       &lt;attribute name="EffectiveDate" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeDate" /&gt;
 *       &lt;attribute name="ExpirationDate" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeDate" /&gt;
 *       &lt;attribute name="ProviderCode" type="{http://www.travelport.com/schema/common_v43_0}typeProviderCode" /&gt;
 *       &lt;attribute name="Origin" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *       &lt;attribute name="Destination" type="{http://www.travelport.com/schema/common_v43_0}typeIATACode" /&gt;
 *       &lt;attribute name="ClassOfService" type="{http://www.travelport.com/schema/util_v43_0}typeClassOfService" /&gt;
 *       &lt;attribute name="OperatingCarrier" type="{http://www.travelport.com/schema/common_v43_0}typeCarrier" /&gt;
 *       &lt;attribute name="OfferRef" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="Key" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="FareBasis" type="{http://www.travelport.com/schema/common_v43_0}typeFareBasisCode" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "departureTime",
    "flightSpec",
    "operatingFlightSpec",
    "accountCode"
})
@XmlRootElement(name = "AirUpsellQualify")
public class AirUpsellQualify {

    @XmlElement(name = "DepartureTime")
    protected TypeTimeSpec departureTime;
    @XmlElement(name = "FlightSpec")
    protected TypeFlightSpec flightSpec;
    @XmlElement(name = "OperatingFlightSpec")
    protected TypeFlightSpec operatingFlightSpec;
    @XmlElement(name = "AccountCode", namespace = "http://www.travelport.com/schema/common_v43_0")
    protected AccountCode accountCode;
    @XmlAttribute(name = "Carrier", required = true)
    protected String carrier;
    @XmlAttribute(name = "EffectiveDate", required = true)
    protected XMLGregorianCalendar effectiveDate;
    @XmlAttribute(name = "ExpirationDate", required = true)
    protected XMLGregorianCalendar expirationDate;
    @XmlAttribute(name = "ProviderCode")
    protected String providerCode;
    @XmlAttribute(name = "Origin")
    protected String origin;
    @XmlAttribute(name = "Destination")
    protected String destination;
    @XmlAttribute(name = "ClassOfService")
    protected String classOfService;
    @XmlAttribute(name = "OperatingCarrier")
    protected String operatingCarrier;
    @XmlAttribute(name = "OfferRef")
    protected String offerRef;
    @XmlAttribute(name = "Key")
    protected String key;
    @XmlAttribute(name = "FareBasis")
    protected String fareBasis;
    @XmlAttribute(name = "ElStat")
    protected TypeElementStatus elStat;
    @XmlAttribute(name = "KeyOverride")
    protected Boolean keyOverride;

    /**
     * Obtiene el valor de la propiedad departureTime.
     * 
     * @return
     *     possible object is
     *     {@link TypeTimeSpec }
     *     
     */
    public TypeTimeSpec getDepartureTime() {
        return departureTime;
    }

    /**
     * Define el valor de la propiedad departureTime.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeTimeSpec }
     *     
     */
    public void setDepartureTime(TypeTimeSpec value) {
        this.departureTime = value;
    }

    /**
     * Obtiene el valor de la propiedad flightSpec.
     * 
     * @return
     *     possible object is
     *     {@link TypeFlightSpec }
     *     
     */
    public TypeFlightSpec getFlightSpec() {
        return flightSpec;
    }

    /**
     * Define el valor de la propiedad flightSpec.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeFlightSpec }
     *     
     */
    public void setFlightSpec(TypeFlightSpec value) {
        this.flightSpec = value;
    }

    /**
     * Obtiene el valor de la propiedad operatingFlightSpec.
     * 
     * @return
     *     possible object is
     *     {@link TypeFlightSpec }
     *     
     */
    public TypeFlightSpec getOperatingFlightSpec() {
        return operatingFlightSpec;
    }

    /**
     * Define el valor de la propiedad operatingFlightSpec.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeFlightSpec }
     *     
     */
    public void setOperatingFlightSpec(TypeFlightSpec value) {
        this.operatingFlightSpec = value;
    }

    /**
     * Obtiene el valor de la propiedad accountCode.
     * 
     * @return
     *     possible object is
     *     {@link AccountCode }
     *     
     */
    public AccountCode getAccountCode() {
        return accountCode;
    }

    /**
     * Define el valor de la propiedad accountCode.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountCode }
     *     
     */
    public void setAccountCode(AccountCode value) {
        this.accountCode = value;
    }

    /**
     * Obtiene el valor de la propiedad carrier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCarrier() {
        return carrier;
    }

    /**
     * Define el valor de la propiedad carrier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarrier(String value) {
        this.carrier = value;
    }

    /**
     * Obtiene el valor de la propiedad effectiveDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Define el valor de la propiedad effectiveDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDate(XMLGregorianCalendar value) {
        this.effectiveDate = value;
    }

    /**
     * Obtiene el valor de la propiedad expirationDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Define el valor de la propiedad expirationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Obtiene el valor de la propiedad providerCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProviderCode() {
        return providerCode;
    }

    /**
     * Define el valor de la propiedad providerCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProviderCode(String value) {
        this.providerCode = value;
    }

    /**
     * Obtiene el valor de la propiedad origin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * Define el valor de la propiedad origin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigin(String value) {
        this.origin = value;
    }

    /**
     * Obtiene el valor de la propiedad destination.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestination() {
        return destination;
    }

    /**
     * Define el valor de la propiedad destination.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestination(String value) {
        this.destination = value;
    }

    /**
     * Obtiene el valor de la propiedad classOfService.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassOfService() {
        return classOfService;
    }

    /**
     * Define el valor de la propiedad classOfService.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassOfService(String value) {
        this.classOfService = value;
    }

    /**
     * Obtiene el valor de la propiedad operatingCarrier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperatingCarrier() {
        return operatingCarrier;
    }

    /**
     * Define el valor de la propiedad operatingCarrier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperatingCarrier(String value) {
        this.operatingCarrier = value;
    }

    /**
     * Obtiene el valor de la propiedad offerRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferRef() {
        return offerRef;
    }

    /**
     * Define el valor de la propiedad offerRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferRef(String value) {
        this.offerRef = value;
    }

    /**
     * Obtiene el valor de la propiedad key.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Define el valor de la propiedad key.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Obtiene el valor de la propiedad fareBasis.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFareBasis() {
        return fareBasis;
    }

    /**
     * Define el valor de la propiedad fareBasis.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFareBasis(String value) {
        this.fareBasis = value;
    }

    /**
     * Obtiene el valor de la propiedad elStat.
     * 
     * @return
     *     possible object is
     *     {@link TypeElementStatus }
     *     
     */
    public TypeElementStatus getElStat() {
        return elStat;
    }

    /**
     * Define el valor de la propiedad elStat.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeElementStatus }
     *     
     */
    public void setElStat(TypeElementStatus value) {
        this.elStat = value;
    }

    /**
     * Obtiene el valor de la propiedad keyOverride.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isKeyOverride() {
        return keyOverride;
    }

    /**
     * Define el valor de la propiedad keyOverride.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setKeyOverride(Boolean value) {
        this.keyOverride = value;
    }

}
