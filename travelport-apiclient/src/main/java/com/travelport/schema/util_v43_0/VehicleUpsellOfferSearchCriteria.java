
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.TypeDoorCount;
import com.travelport.schema.common_v43_0.TypeVehicleCategory;
import com.travelport.schema.common_v43_0.TypeVehicleClass;
import com.travelport.schema.common_v43_0.TypeVehicleTransmission;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="VehicleClass" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeVehicleClass" /&gt;
 *       &lt;attribute name="Category" type="{http://www.travelport.com/schema/common_v43_0}typeVehicleCategory" /&gt;
 *       &lt;attribute name="AirConditioning" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="TransmissionType" type="{http://www.travelport.com/schema/common_v43_0}typeVehicleTransmission" /&gt;
 *       &lt;attribute name="DoorCount" type="{http://www.travelport.com/schema/common_v43_0}typeDoorCount" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "VehicleUpsellOfferSearchCriteria")
public class VehicleUpsellOfferSearchCriteria {

    @XmlAttribute(name = "VehicleClass", required = true)
    protected TypeVehicleClass vehicleClass;
    @XmlAttribute(name = "Category")
    protected TypeVehicleCategory category;
    @XmlAttribute(name = "AirConditioning")
    protected Boolean airConditioning;
    @XmlAttribute(name = "TransmissionType")
    protected TypeVehicleTransmission transmissionType;
    @XmlAttribute(name = "DoorCount")
    protected TypeDoorCount doorCount;

    /**
     * Obtiene el valor de la propiedad vehicleClass.
     * 
     * @return
     *     possible object is
     *     {@link TypeVehicleClass }
     *     
     */
    public TypeVehicleClass getVehicleClass() {
        return vehicleClass;
    }

    /**
     * Define el valor de la propiedad vehicleClass.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeVehicleClass }
     *     
     */
    public void setVehicleClass(TypeVehicleClass value) {
        this.vehicleClass = value;
    }

    /**
     * Obtiene el valor de la propiedad category.
     * 
     * @return
     *     possible object is
     *     {@link TypeVehicleCategory }
     *     
     */
    public TypeVehicleCategory getCategory() {
        return category;
    }

    /**
     * Define el valor de la propiedad category.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeVehicleCategory }
     *     
     */
    public void setCategory(TypeVehicleCategory value) {
        this.category = value;
    }

    /**
     * Obtiene el valor de la propiedad airConditioning.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAirConditioning() {
        return airConditioning;
    }

    /**
     * Define el valor de la propiedad airConditioning.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAirConditioning(Boolean value) {
        this.airConditioning = value;
    }

    /**
     * Obtiene el valor de la propiedad transmissionType.
     * 
     * @return
     *     possible object is
     *     {@link TypeVehicleTransmission }
     *     
     */
    public TypeVehicleTransmission getTransmissionType() {
        return transmissionType;
    }

    /**
     * Define el valor de la propiedad transmissionType.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeVehicleTransmission }
     *     
     */
    public void setTransmissionType(TypeVehicleTransmission value) {
        this.transmissionType = value;
    }

    /**
     * Obtiene el valor de la propiedad doorCount.
     * 
     * @return
     *     possible object is
     *     {@link TypeDoorCount }
     *     
     */
    public TypeDoorCount getDoorCount() {
        return doorCount;
    }

    /**
     * Define el valor de la propiedad doorCount.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeDoorCount }
     *     
     */
    public void setDoorCount(TypeDoorCount value) {
        this.doorCount = value;
    }

}
