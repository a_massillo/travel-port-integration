
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *                 Specify a range of flight numbers.
 *             
 * 
 * <p>Clase Java para typeFlightNumberRange complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="typeFlightNumberRange"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="FlightNumberRangeStart" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeFlightNumber" /&gt;
 *       &lt;attribute name="FlightNumberRangeEnd" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeFlightNumber" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "typeFlightNumberRange")
public class TypeFlightNumberRange {

    @XmlAttribute(name = "FlightNumberRangeStart", required = true)
    protected String flightNumberRangeStart;
    @XmlAttribute(name = "FlightNumberRangeEnd", required = true)
    protected String flightNumberRangeEnd;

    /**
     * Obtiene el valor de la propiedad flightNumberRangeStart.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightNumberRangeStart() {
        return flightNumberRangeStart;
    }

    /**
     * Define el valor de la propiedad flightNumberRangeStart.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightNumberRangeStart(String value) {
        this.flightNumberRangeStart = value;
    }

    /**
     * Obtiene el valor de la propiedad flightNumberRangeEnd.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightNumberRangeEnd() {
        return flightNumberRangeEnd;
    }

    /**
     * Define el valor de la propiedad flightNumberRangeEnd.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightNumberRangeEnd(String value) {
        this.flightNumberRangeEnd = value;
    }

}
