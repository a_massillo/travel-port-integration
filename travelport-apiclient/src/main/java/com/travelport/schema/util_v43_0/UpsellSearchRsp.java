
package com.travelport.schema.util_v43_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.travelport.schema.common_v43_0.BaseRsp;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.travelport.com/schema/common_v43_0}BaseRsp"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}AirUpsellSearchResult" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}HotelUpsellSearchResult" maxOccurs="999" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.travelport.com/schema/util_v43_0}VehicleUpsellSearchResult" maxOccurs="999" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *       &lt;attribute name="MoreResults" use="required" type="{http://www.travelport.com/schema/common_v43_0}typeMoreResults" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "airUpsellSearchResult",
    "hotelUpsellSearchResult",
    "vehicleUpsellSearchResult"
})
@XmlRootElement(name = "UpsellSearchRsp")
public class UpsellSearchRsp
    extends BaseRsp
{

    @XmlElement(name = "AirUpsellSearchResult")
    protected List<AirUpsellSearchResult> airUpsellSearchResult;
    @XmlElement(name = "HotelUpsellSearchResult")
    protected List<HotelUpsellSearchResult> hotelUpsellSearchResult;
    @XmlElement(name = "VehicleUpsellSearchResult")
    protected List<VehicleUpsellSearchResult> vehicleUpsellSearchResult;
    @XmlAttribute(name = "MoreResults", required = true)
    protected boolean moreResults;

    /**
     * Gets the value of the airUpsellSearchResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the airUpsellSearchResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAirUpsellSearchResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AirUpsellSearchResult }
     * 
     * 
     */
    public List<AirUpsellSearchResult> getAirUpsellSearchResult() {
        if (airUpsellSearchResult == null) {
            airUpsellSearchResult = new ArrayList<AirUpsellSearchResult>();
        }
        return this.airUpsellSearchResult;
    }

    /**
     * Gets the value of the hotelUpsellSearchResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hotelUpsellSearchResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHotelUpsellSearchResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HotelUpsellSearchResult }
     * 
     * 
     */
    public List<HotelUpsellSearchResult> getHotelUpsellSearchResult() {
        if (hotelUpsellSearchResult == null) {
            hotelUpsellSearchResult = new ArrayList<HotelUpsellSearchResult>();
        }
        return this.hotelUpsellSearchResult;
    }

    /**
     * Gets the value of the vehicleUpsellSearchResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vehicleUpsellSearchResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVehicleUpsellSearchResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VehicleUpsellSearchResult }
     * 
     * 
     */
    public List<VehicleUpsellSearchResult> getVehicleUpsellSearchResult() {
        if (vehicleUpsellSearchResult == null) {
            vehicleUpsellSearchResult = new ArrayList<VehicleUpsellSearchResult>();
        }
        return this.vehicleUpsellSearchResult;
    }

    /**
     * Obtiene el valor de la propiedad moreResults.
     * 
     */
    public boolean isMoreResults() {
        return moreResults;
    }

    /**
     * Define el valor de la propiedad moreResults.
     * 
     */
    public void setMoreResults(boolean value) {
        this.moreResults = value;
    }

}
