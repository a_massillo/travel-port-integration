
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="QualifyRef" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *       &lt;attribute name="OfferRef" type="{http://www.travelport.com/schema/common_v43_0}typeRef" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "AirUpsellDelete")
public class AirUpsellDelete {

    @XmlAttribute(name = "QualifyRef")
    protected String qualifyRef;
    @XmlAttribute(name = "OfferRef")
    protected String offerRef;

    /**
     * Obtiene el valor de la propiedad qualifyRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQualifyRef() {
        return qualifyRef;
    }

    /**
     * Define el valor de la propiedad qualifyRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQualifyRef(String value) {
        this.qualifyRef = value;
    }

    /**
     * Obtiene el valor de la propiedad offerRef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferRef() {
        return offerRef;
    }

    /**
     * Define el valor de la propiedad offerRef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferRef(String value) {
        this.offerRef = value;
    }

}
