
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attGroup ref="{http://www.travelport.com/schema/util_v43_0}attrProviderSupplierCapabilities"/&gt;
 *       &lt;attribute name="ProviderCode" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="SupplierCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="AgencyCredentials" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;enumeration value="Required"/&gt;
 *             &lt;enumeration value="Optional"/&gt;
 *             &lt;enumeration value="Prohibited"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="Active" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="Provisionable" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="MerchandisingACHAdapter" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="StaticDataCarrier" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="MerchandisingACHCarrier" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="MerchandisingHubCarrier" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "ContentProvider")
public class ContentProvider {

    @XmlAttribute(name = "ProviderCode", required = true)
    protected String providerCode;
    @XmlAttribute(name = "SupplierCode")
    protected String supplierCode;
    @XmlAttribute(name = "AgencyCredentials", required = true)
    protected String agencyCredentials;
    @XmlAttribute(name = "Active", required = true)
    protected boolean active;
    @XmlAttribute(name = "Provisionable", required = true)
    protected boolean provisionable;
    @XmlAttribute(name = "MerchandisingACHAdapter")
    protected String merchandisingACHAdapter;
    @XmlAttribute(name = "StaticDataCarrier")
    protected Boolean staticDataCarrier;
    @XmlAttribute(name = "MerchandisingACHCarrier")
    protected Boolean merchandisingACHCarrier;
    @XmlAttribute(name = "MerchandisingHubCarrier")
    protected Boolean merchandisingHubCarrier;
    @XmlAttribute(name = "BookingRetrieve")
    protected TypeProviderSupplierCapabilityType bookingRetrieve;
    @XmlAttribute(name = "SegmentModify")
    protected TypeProviderSupplierCapabilityType segmentModify;
    @XmlAttribute(name = "OptionalServicesModify")
    protected TypeProviderSupplierCapabilityType optionalServicesModify;
    @XmlAttribute(name = "TravelerInfoModify")
    protected TypeProviderSupplierCapabilityType travelerInfoModify;
    @XmlAttribute(name = "AdditionalPayment")
    protected TypeProviderSupplierCapabilityType additionalPayment;
    @XmlAttribute(name = "BookingCancel")
    protected TypeProviderSupplierCapabilityType bookingCancel;
    @XmlAttribute(name = "SeatMap")
    protected TypeProviderSupplierCapabilityType seatMap;

    /**
     * Obtiene el valor de la propiedad providerCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProviderCode() {
        return providerCode;
    }

    /**
     * Define el valor de la propiedad providerCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProviderCode(String value) {
        this.providerCode = value;
    }

    /**
     * Obtiene el valor de la propiedad supplierCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupplierCode() {
        return supplierCode;
    }

    /**
     * Define el valor de la propiedad supplierCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupplierCode(String value) {
        this.supplierCode = value;
    }

    /**
     * Obtiene el valor de la propiedad agencyCredentials.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyCredentials() {
        return agencyCredentials;
    }

    /**
     * Define el valor de la propiedad agencyCredentials.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyCredentials(String value) {
        this.agencyCredentials = value;
    }

    /**
     * Obtiene el valor de la propiedad active.
     * 
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Define el valor de la propiedad active.
     * 
     */
    public void setActive(boolean value) {
        this.active = value;
    }

    /**
     * Obtiene el valor de la propiedad provisionable.
     * 
     */
    public boolean isProvisionable() {
        return provisionable;
    }

    /**
     * Define el valor de la propiedad provisionable.
     * 
     */
    public void setProvisionable(boolean value) {
        this.provisionable = value;
    }

    /**
     * Obtiene el valor de la propiedad merchandisingACHAdapter.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMerchandisingACHAdapter() {
        return merchandisingACHAdapter;
    }

    /**
     * Define el valor de la propiedad merchandisingACHAdapter.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMerchandisingACHAdapter(String value) {
        this.merchandisingACHAdapter = value;
    }

    /**
     * Obtiene el valor de la propiedad staticDataCarrier.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isStaticDataCarrier() {
        if (staticDataCarrier == null) {
            return false;
        } else {
            return staticDataCarrier;
        }
    }

    /**
     * Define el valor de la propiedad staticDataCarrier.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStaticDataCarrier(Boolean value) {
        this.staticDataCarrier = value;
    }

    /**
     * Obtiene el valor de la propiedad merchandisingACHCarrier.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isMerchandisingACHCarrier() {
        if (merchandisingACHCarrier == null) {
            return false;
        } else {
            return merchandisingACHCarrier;
        }
    }

    /**
     * Define el valor de la propiedad merchandisingACHCarrier.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMerchandisingACHCarrier(Boolean value) {
        this.merchandisingACHCarrier = value;
    }

    /**
     * Obtiene el valor de la propiedad merchandisingHubCarrier.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isMerchandisingHubCarrier() {
        if (merchandisingHubCarrier == null) {
            return false;
        } else {
            return merchandisingHubCarrier;
        }
    }

    /**
     * Define el valor de la propiedad merchandisingHubCarrier.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMerchandisingHubCarrier(Boolean value) {
        this.merchandisingHubCarrier = value;
    }

    /**
     * Obtiene el valor de la propiedad bookingRetrieve.
     * 
     * @return
     *     possible object is
     *     {@link TypeProviderSupplierCapabilityType }
     *     
     */
    public TypeProviderSupplierCapabilityType getBookingRetrieve() {
        if (bookingRetrieve == null) {
            return TypeProviderSupplierCapabilityType.YES;
        } else {
            return bookingRetrieve;
        }
    }

    /**
     * Define el valor de la propiedad bookingRetrieve.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeProviderSupplierCapabilityType }
     *     
     */
    public void setBookingRetrieve(TypeProviderSupplierCapabilityType value) {
        this.bookingRetrieve = value;
    }

    /**
     * Obtiene el valor de la propiedad segmentModify.
     * 
     * @return
     *     possible object is
     *     {@link TypeProviderSupplierCapabilityType }
     *     
     */
    public TypeProviderSupplierCapabilityType getSegmentModify() {
        if (segmentModify == null) {
            return TypeProviderSupplierCapabilityType.YES;
        } else {
            return segmentModify;
        }
    }

    /**
     * Define el valor de la propiedad segmentModify.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeProviderSupplierCapabilityType }
     *     
     */
    public void setSegmentModify(TypeProviderSupplierCapabilityType value) {
        this.segmentModify = value;
    }

    /**
     * Obtiene el valor de la propiedad optionalServicesModify.
     * 
     * @return
     *     possible object is
     *     {@link TypeProviderSupplierCapabilityType }
     *     
     */
    public TypeProviderSupplierCapabilityType getOptionalServicesModify() {
        if (optionalServicesModify == null) {
            return TypeProviderSupplierCapabilityType.YES;
        } else {
            return optionalServicesModify;
        }
    }

    /**
     * Define el valor de la propiedad optionalServicesModify.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeProviderSupplierCapabilityType }
     *     
     */
    public void setOptionalServicesModify(TypeProviderSupplierCapabilityType value) {
        this.optionalServicesModify = value;
    }

    /**
     * Obtiene el valor de la propiedad travelerInfoModify.
     * 
     * @return
     *     possible object is
     *     {@link TypeProviderSupplierCapabilityType }
     *     
     */
    public TypeProviderSupplierCapabilityType getTravelerInfoModify() {
        if (travelerInfoModify == null) {
            return TypeProviderSupplierCapabilityType.YES;
        } else {
            return travelerInfoModify;
        }
    }

    /**
     * Define el valor de la propiedad travelerInfoModify.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeProviderSupplierCapabilityType }
     *     
     */
    public void setTravelerInfoModify(TypeProviderSupplierCapabilityType value) {
        this.travelerInfoModify = value;
    }

    /**
     * Obtiene el valor de la propiedad additionalPayment.
     * 
     * @return
     *     possible object is
     *     {@link TypeProviderSupplierCapabilityType }
     *     
     */
    public TypeProviderSupplierCapabilityType getAdditionalPayment() {
        if (additionalPayment == null) {
            return TypeProviderSupplierCapabilityType.YES;
        } else {
            return additionalPayment;
        }
    }

    /**
     * Define el valor de la propiedad additionalPayment.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeProviderSupplierCapabilityType }
     *     
     */
    public void setAdditionalPayment(TypeProviderSupplierCapabilityType value) {
        this.additionalPayment = value;
    }

    /**
     * Obtiene el valor de la propiedad bookingCancel.
     * 
     * @return
     *     possible object is
     *     {@link TypeProviderSupplierCapabilityType }
     *     
     */
    public TypeProviderSupplierCapabilityType getBookingCancel() {
        if (bookingCancel == null) {
            return TypeProviderSupplierCapabilityType.YES;
        } else {
            return bookingCancel;
        }
    }

    /**
     * Define el valor de la propiedad bookingCancel.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeProviderSupplierCapabilityType }
     *     
     */
    public void setBookingCancel(TypeProviderSupplierCapabilityType value) {
        this.bookingCancel = value;
    }

    /**
     * Obtiene el valor de la propiedad seatMap.
     * 
     * @return
     *     possible object is
     *     {@link TypeProviderSupplierCapabilityType }
     *     
     */
    public TypeProviderSupplierCapabilityType getSeatMap() {
        if (seatMap == null) {
            return TypeProviderSupplierCapabilityType.YES;
        } else {
            return seatMap;
        }
    }

    /**
     * Define el valor de la propiedad seatMap.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeProviderSupplierCapabilityType }
     *     
     */
    public void setSeatMap(TypeProviderSupplierCapabilityType value) {
        this.seatMap = value;
    }

}
