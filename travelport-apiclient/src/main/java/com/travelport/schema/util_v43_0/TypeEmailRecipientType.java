
package com.travelport.schema.util_v43_0;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para typeEmailRecipientType.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;simpleType name="typeEmailRecipientType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="TO"/&gt;
 *     &lt;enumeration value="CC"/&gt;
 *     &lt;enumeration value="BCC"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "typeEmailRecipientType")
@XmlEnum
public enum TypeEmailRecipientType {

    TO,
    CC,
    BCC;

    public String value() {
        return name();
    }

    public static TypeEmailRecipientType fromValue(String v) {
        return valueOf(v);
    }

}
