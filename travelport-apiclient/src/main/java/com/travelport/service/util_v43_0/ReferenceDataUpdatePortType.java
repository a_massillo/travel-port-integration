package com.travelport.service.util_v43_0;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This class was generated by Apache CXF 3.2.1
 * 2018-01-17T20:07:41.013-03:00
 * Generated source version: 3.2.1
 * 
 */
@WebService(targetNamespace = "http://www.travelport.com/service/util_v43_0", name = "ReferenceDataUpdatePortType")
@XmlSeeAlso({com.travelport.schema.air_v43_0.ObjectFactory.class, com.travelport.schema.common_v43_0.ObjectFactory.class, com.travelport.schema.util_v43_0.ObjectFactory.class})
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface ReferenceDataUpdatePortType {

    @WebMethod(action = "http://localhost:8080/kestrel/UtilService")
    @WebResult(name = "ReferenceDataUpdateRsp", targetNamespace = "http://www.travelport.com/schema/util_v43_0", partName = "result")
    public com.travelport.schema.util_v43_0.ReferenceDataUpdateRsp service(
        @WebParam(partName = "parameters", name = "ReferenceDataUpdateReq", targetNamespace = "http://www.travelport.com/schema/util_v43_0")
        com.travelport.schema.util_v43_0.ReferenceDataUpdateReq parameters
    ) throws UtilFaultMessage;
}
