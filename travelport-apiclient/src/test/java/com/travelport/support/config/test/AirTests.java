/***********************************************************************************
 * CopyRight (c) Sep 18, 2016 Safarifone.com to present.
 * All rights reserved.
 * All materials in this project are the intellectual properties of Safarifone.com
 *
 ***********************************************************************************/
package com.travelport.support.config.test;

import java.util.Iterator;
import java.util.List;

import com.travelport.schema.air_v43_0.AirItinerary;
import com.travelport.schema.air_v43_0.AirPricePoint;
import com.travelport.schema.air_v43_0.AirPriceReq;
import com.travelport.schema.air_v43_0.AirPriceRsp;
import com.travelport.schema.air_v43_0.AirPricingCommand;
import com.travelport.schema.air_v43_0.AirPricingInfo;
import com.travelport.schema.air_v43_0.AirPricingSolution;
import com.travelport.schema.air_v43_0.BookingInfo;
import com.travelport.schema.air_v43_0.FareInfo;
import com.travelport.schema.air_v43_0.FlightOption;
import com.travelport.schema.air_v43_0.LowFareSearchReq;
import com.travelport.schema.air_v43_0.LowFareSearchRsp;
import com.travelport.schema.air_v43_0.Option;
import com.travelport.schema.air_v43_0.TypeBaseAirSegment;
import com.travelport.schema.common_v43_0.ActionStatus;
import com.travelport.schema.common_v43_0.SearchPassenger;
import com.travelport.schema.universal_v43_0.AirCreateReservationReq;
import com.travelport.schema.universal_v43_0.AirCreateReservationRsp;
import com.travelport.schema.universal_v43_0.UniversalRecordCancelReq;
import com.travelport.schema.universal_v43_0.UniversalRecordCancelRsp;
import com.travelport.service.universal_v43_0.UniversalRecordFaultMessage;
import com.travelport.support.Helper;
import com.travelport.support.Helper.AirSegmentMap;
import com.travelport.support.Helper.FareMap;
import com.travelport.support.Helper.FlightDetailsMap;
import com.travelport.support.TravelPortServicesHelper;
import com.travelport.support.config.PropertiesLoader;
import com.travelport.support.config.TravelPortConfig;
import com.travelport.support.request.AirBaseLowFareReq;
import com.travelport.support.request.SearchAirLegExt;
import com.travelport.support.request.enums.AirProvider;
import com.travelport.support.request.enums.EmailType;
import com.travelport.support.request.enums.PassengerType;
import com.travelport.support.request.enums.PhoneType;
import com.travelport.support.request.enums.TicketingType;
import com.travelport.support.request.params.TravelPortTraveler;


/**
 * Tests and sample usage of travelport api
 * @author amassillo
 *
 */

public class AirTests {

	private TravelPortServicesHelper services;

	private String airportFrom = "CHI";
	private String airportTo = "DCA";
	private String departureDate = Helper.daysInFuture(30);
	private String returnDate = Helper.daysInFuture(45);
	
	public AirTests() {
		try {
			TravelPortConfig lConfig = new PropertiesLoader<TravelPortConfig>().loadForInstance("travelport-helper.yml",TravelPortConfig.class );
			services = new TravelPortServicesHelper(lConfig);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	//variables moving among tests
	
	private TypeBaseAirSegment lSelectedSegment;
	private AirPriceRsp lPriceResp;
	private AirCreateReservationRsp tpAirRvs;

	public static void main(String[] args) {
		new AirTests().testScenarios();
	}
	
	public void testScenarios() {
		this.airLowFareTest();
//		this.airPricing();
//		this.airReservation();
//		this.airCancel();
	}
	/**
	 * one way itinerary, with possible connections
	 */
    public void airLowFareTest() {
		LowFareSearchRsp l = null;
		try {
			
			SearchAirLegExt lSearchAirLeg = new SearchAirLegExt();
			lSearchAirLeg.setBounds(this.airportFrom, this.airportTo);	
			lSearchAirLeg.addDepartureDate(departureDate,null);
		
			SearchAirLegExt lSearchAirLeg2 = new SearchAirLegExt();
			lSearchAirLeg2.setBounds(this.airportTo,this.airportFrom);	
			lSearchAirLeg2.addDepartureDate(returnDate,null);
			
			//helper 2
			AirBaseLowFareReq h1 = new AirBaseLowFareReq();
			h1.addPassengers(1, PassengerType.ADT);
			
			//setup request this objects created via helpers
			LowFareSearchReq r = new LowFareSearchReq();
			r.setTargetBranch(services.getTargetBranch());
			r.setBillingPointOfSaleInfo(services.getBillingPointOfSaleInfo());

			r.getSearchAirLeg().add(lSearchAirLeg);
			r.getSearchAirLeg().add(lSearchAirLeg2);
			
			r.setAirSearchModifiers(h1.getAirSearchModifiers());
			r.getSearchPassenger().addAll(h1.getSearchPassenger());
		
			//-------------------check availability----------------------------------------------------------
			l = this.services.getAirShop().get().service(r);			
				//select and prepare 1st returned itinerary to check air price
			lSelectedSegment = l.getAirSegmentList().getAirSegment().get(0);
			lSelectedSegment.setProviderCode(AirProvider.GALILEO.getCode());
			FlightDetailsMap lMap = Helper.createFlightDetailsMap(l.getFlightDetailsList().getFlightDetails());
			lSelectedSegment.getFlightDetails().addAll(lMap.getByRef(lSelectedSegment.getFlightDetailsRef()));
			//clear references
			lSelectedSegment.getFlightDetailsRef().clear();
			this.checkLowFareprices(l);
			System.out.println("low fare Sycn search done !!");
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    
	private void checkLowFareprices(LowFareSearchRsp l) {
		FareMap lFaresMap 		  = Helper.createFareInfoMap(l.getFareInfoList().getFareInfo());
		//using point list entrance point due to the need of returning prices, otherwise you can use the segments which have 
		//most of the details
		AirSegmentMap lSegmentMap = Helper.createAirSegmentMap(l.getAirSegmentList().getAirSegment());
		AirPricePoint lPricePoint = l.getAirPricePointList().getAirPricePoint().get(0);
		for (Iterator<AirPricingInfo> lPricingIt = lPricePoint.getAirPricingInfo().iterator(); 
													lPricingIt.hasNext();) {
			AirPricingInfo lPriceInfo = lPricingIt.next();
			List<FlightOption> lFlightOptions = lPriceInfo.getFlightOptionsList().getFlightOption();
			for (Iterator<FlightOption> lFlightOptionIt = lFlightOptions.iterator(); 
					lFlightOptionIt.hasNext();) {
				FlightOption lFlightOption = lFlightOptionIt.next();
				for (Iterator<Option> lFlightOption2It = lFlightOption.getOption().iterator(); 
						lFlightOption2It.hasNext();) {
					Option lOption = lFlightOption2It.next();
					for (Iterator<BookingInfo> lBookingInfo = lOption.getBookingInfo().iterator(); 
							lBookingInfo.hasNext();) {
						//fare and booking info references are translated into the lists
						BookingInfo lInfo = lBookingInfo.next();								
						FareInfo lFareInfo = lFaresMap.getByRef(lInfo.getFareInfoRef());
						TypeBaseAirSegment lSegment = lSegmentMap.getByRef(lInfo.getSegmentRef());
						//set fields extracted from fare and booking inforation -------------------------------------
						String lol = "From " + lSegment.getOrigin() + " TO " + lSegment.getDestination() + ":(" +
									lSegment.getFlightNumber() +" ) departing at: "+ lSegment.getDepartureTime() +"/"+
							//		 BigDecimal.valueOf(Helper.parseNumberWithCurrency(lFareInfo.getTaxAmount())) + " of taxes/ " +
									 Helper.parseNumberWithCurrency(lFareInfo.getAmount()) + " of amount/" +
									 lInfo.getCabinClass();
						System.out.println(lol);
						//---------------------------------------------------------------------------------------------
					}
				
				}
			}
		}	
	}
	/**
	 * price the first selected returned flight
	 * 
	 */
	public void airPricing () {
		if (lSelectedSegment ==null) return;
		AirPriceReq lReq = new AirPriceReq();
		lReq.setAirItinerary(new AirItinerary());
		//need to match refs in order to make it work
		lReq.getAirItinerary().getAirSegment().add(lSelectedSegment);
		lReq.setBillingPointOfSaleInfo(services.getBillingPointOfSaleInfo());
		lReq.setTargetBranch(services.getTargetBranch());
		SearchPassenger adult = new SearchPassenger();
		adult.setCode(PassengerType.ADT.name());
		adult.setKey("gr8AVWGCR064r57Jt0+8bA==");
		lReq.getSearchPassenger().add(adult);
		AirPricingCommand command = new AirPricingCommand();
		command.setCabinClass(lSelectedSegment.getCabinClass());
		lReq.getAirPricingCommand().add(command);
		try {
			services.getAirPrice().showXML(true);
			lPriceResp = services.getAirPrice().get().service(lReq);
			System.out.println ("pricing complete !!: " + lPriceResp.getAirPriceResult().size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * air reservation
	 */
	
	public void airReservation() {	
		if (lPriceResp == null) return; 
		//-------------------------make reservation-------------------------------------------------------
		AirCreateReservationReq lResrvReq = new AirCreateReservationReq();
		
		AirPricingSolution lsol = lPriceResp.getAirPriceResult().get(0).getAirPricingSolution().get(0);
		lsol.getAirPricingInfo().get(0).setPlatingCarrier(null);
		AirSegmentMap lSegments = Helper.createAirSegmentMap(lPriceResp.getAirItinerary().getAirSegment());
		lsol.getAirSegment().addAll(lSegments.getByRef(lsol.getAirSegmentRef()));
		lsol.getAirSegmentRef().clear(); 			//clean refs
	
		
		lResrvReq.setBillingPointOfSaleInfo(services.getBillingPointOfSaleInfo());
		
		lResrvReq.setTargetBranch(services.getTargetBranch());	
		
		
		ActionStatus lActionStatus = new ActionStatus();
		lActionStatus.setType(TicketingType.ACTIVE.name());
		//lActionStatus.setTicketDate("2018-02-01T13:33:03");
		lActionStatus.setTicketDate("T*");
		lActionStatus.setProviderCode(AirProvider.GALILEO.getCode());
		lResrvReq.getActionStatus().add(lActionStatus);
		lResrvReq.getAirPricingSolution().add(lsol);
		
		TravelPortTraveler lTraveler = new TravelPortTraveler("Alejandra","Massillo","Mr");
		lTraveler.setEmail("amassillo@gmail.com", EmailType.Home);
		lTraveler.setPhone("+54987777777", PhoneType.Mobile);
		lTraveler.setTravelerType("ADT");
//		DeliveryInfo dinfo = new DeliveryInfo();
//		DeliveryInfo.ShippingAddress value = new DeliveryInfo.ShippingAddress();
//		value.setAddressName("one");
//		value.setCity("dos");
//		value.setCountry("US");
//		value.setPostalCode("80206");
//		State lState = new State();
//		lState.setValue("CO");
//		value.setState(lState);
//		dinfo.setShippingAddress(value);
//				
//		lTraveler.getDeliveryInfo().add(dinfo);
		lResrvReq.getBookingTraveler().add(lTraveler);
		
		//String FORM_OF_PAYMENT_REF ="1239900";
		
		//req.getFormOfPayment().add(fop);
		
		//hook payment to credit card
//		Payment payment = new Payment();
//		payment.setType("TicketFee");
//		payment.setFormOfPaymentRef(FORM_OF_PAYMENT_REF);
//		payment.setAmount(lsol.getTotalPrice());
//		lResrvReq.getPayment().add(payment);
		System.out.println(lsol.getTotalPrice());
		//form of payment
//		FormOfPayment lFop = new FormOfPayment();
//		lFop.setType("Cash");
//		lFop.setKey(FORM_OF_PAYMENT_REF);
//		lResrvReq.getFormOfPayment().add(lFop);
		AirCreateReservationRsp tpAirRvs;
		try {
			services.getAirReserve().showXML(true);
			tpAirRvs = services.getAirReserve().get().service(lResrvReq);
//			AirPricingInfo lPriceInfo = tpAirRvs.getUniversalRecord().getAirReservation().get(0).getAirPricingInfo().get(0);
//			for (FareInfo lFareInfo : lPriceInfo.getFareInfo()) {
//				lFareInfo.getCommission();
//				lFareInfo.getBrand().getCarrier();
//				
//			}
			
			System.out.println("passed air reservation: "+ tpAirRvs.getUniversalRecord().getLocatorCode());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * cancel reservation
	 */
	public void airCancel () {
		UniversalRecordCancelReq lCancelReq = new UniversalRecordCancelReq();
		lCancelReq.setUniversalRecordLocatorCode(tpAirRvs.getUniversalRecord().getLocatorCode());
		lCancelReq.setVersion(tpAirRvs.getUniversalRecord().getVersion());
		//those are always necessary
		lCancelReq.setTargetBranch(services.getTargetBranch());
		lCancelReq.setBillingPointOfSaleInfo(services.getBillingPointOfSaleInfo());
		try {
			UniversalRecordCancelRsp lCancelRsp = services.getCancelJourney().get().service(lCancelReq);
			System.out.println("Cancellation is done" + lCancelRsp.getTransactionId());
		} catch (UniversalRecordFaultMessage e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
