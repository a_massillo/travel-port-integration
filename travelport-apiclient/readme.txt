#to generate clients
#Apache cfx 2.7.12

wsdl2java -client -d "src\main\java" "disk path to resources\uAPIs\Release-V17.4.0.36-V17.4\air_v43_0\Air.wsdl" 
wsdl2java -client -d "src\main\java" "disk path to resources\uAPIs\Release-V17.4.0.36-V17.4\system_v32_0\System.wsdl" 
wsdl2java -client -d "src\main\java"  "disk path to resources\uAPIs\Release-V17.4.0.36-V17.4\universal_v43_0\UniversalRecord.wsdl"
wsdl2java -client -d "src\main\java"  "disk path to resources\uAPIs\Release-V17.4.0.36-V17.4\util_v43_0\Util.wsdl"

#to run tests
update application.yml with proper travelport credentials
run as jUnit classes under com.travelport.support.config.test
